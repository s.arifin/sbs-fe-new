resource

    @PostMapping("/motors/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = motorService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


Service 
    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Async
    public void buildIndex() {
        motorSearchRepository.deleteAll();
        List<Motor> motors =  motorRepository.findAll();
        for (Motor m: motors) {
            motorSearchRepository.save(m);
            log.debug("Data motor save !...");
        }
    }


HTML untuk component
                        <button pButton type="submit"
                            icon="fa-th-list"
                            class="ui-button-primary btn-sm"
                            (click)="buildReindex()"
                            *jhiHasAnyAuthority="['ROLE_ADMIN']">
                        </button>   
====================================================================================================================
Sintaks Search

<div>
                        <form name="searchForm" class="form-inline">
                            <div class="input-group w-100 mt-3">
                                <input type="text" class="form-control" [(ngModel)]="currentSearch" id="currentSearch"
                                      name="currentSearch" placeholder="{{ 'mpmApp.motor.home.search' | translate }}">
                                <button class="input-group-addon btn btn-info" (click)="search(currentSearch)" *ngIf="currentSearch">
                                    <span class="fa fa-search"></span>
                                </button>
                                <button class="input-group-addon btn btn-danger" (click)="clear()" *ngIf="currentSearch">
                                    <span class="fa fa-trash-o"></span>
                                </button>
                            </div>
                        </form>
                    </div>
=======================================================================================================================-
- Yang digunakan

                    <ul class="nav justify-content-end">
                        <li class="nav-item"*jhiHasAnyAuthority="['ROLE_ADMIN']">
                            <a class="btn" (click)="buildReindex()" href="javascript:void(0)" style="cursor: pointer;"><span><i class="fa fa-th-list"></i> &nbsp;{{ 'entity.action.reindex' | translate }}</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="btn" [routerLink]="['/', { outlets: { popup: ['association-type-new'] } }]"><span><i class="fa fa-plus"></i> &nbsp;{{ 'entity.action.add' | translate }}</span></a>
                        </li>
                    </ul>
====================================================================================================================

Service .ts
    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

=================================================================================================================

component ts
    buildReindex() {
        this.motorService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
