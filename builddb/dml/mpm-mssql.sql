/*
 * ER/Studio Data Architect SQL Code Generation
 * Project :      mpm-model.DM1
 *
 * Date Created : Saturday, May 19, 2018 08:36:11
 * Target DBMS : Microsoft SQL Server 2012
 */

/* 
 * TABLE: accounting_calendar 
 */

CREATE TABLE accounting_calendar(
    idcalendar    DOM_IDENTITY    NOT NULL,
    idinternal    DOM_ID          NULL,
    PRIMARY KEY CLUSTERED (idcalendar)
)
go



/* 
 * TABLE: acctg_trans_detail 
 */

CREATE TABLE acctg_trans_detail(
    idtradet      DOM_LONG        NOT NULL,
    idacctra      DOM_LONG        NULL,
    idglacc       DOM_UUID        NULL,
    idinternal    DOM_ID          NULL,
    idcalendar    DOM_IDENTITY    NULL,
    amount        DOM_MONEY       NULL,
    dbcrflag      DOM_BOOLEAN     NULL,
    PRIMARY KEY CLUSTERED (idtradet)
)
go



/* 
 * TABLE: acctg_trans_type 
 */

CREATE TABLE acctg_trans_type(
    idacctratyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idacctratyp)
)
go



/* 
 * TABLE: acctg_transaction 
 */

CREATE TABLE acctg_transaction(
    idacctra         DOM_LONG           NOT NULL,
    idacctratyp      DOM_INTEGER        NULL,
    dttransaction    DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtentry          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description      DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idacctra)
)
go



/* 
 * TABLE: acctg_transaction_status 
 */

CREATE TABLE acctg_transaction_status(
    idstatus        DOM_UUID        NOT NULL,
    idacctra        DOM_LONG        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: agreement 
 */

CREATE TABLE agreement(
    idagreement    DOM_UUID           NOT NULL,
    idagrtyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idagreement)
)
go



/* 
 * TABLE: agreement_item 
 */

CREATE TABLE agreement_item(
    idagrite       DOM_UUID    NOT NULL,
    idagreement    DOM_UUID    NULL,
    PRIMARY KEY NONCLUSTERED (idagrite)
)
go



/* 
 * TABLE: agreement_motor_item 
 */

CREATE TABLE agreement_motor_item(
    idagrite     DOM_UUID    NOT NULL,
    idvehicle    DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idagrite)
)
go



/* 
 * TABLE: agreement_role 
 */

CREATE TABLE agreement_role(
    idrole         DOM_UUID        NOT NULL,
    idagreement    DOM_UUID        NULL,
    idroletype     DOM_INTEGER     NULL,
    idparty        DOM_UUID        NULL,
    username       DOM_NAME        NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: agreement_status 
 */

CREATE TABLE agreement_status(
    idstatus        DOM_UUID        NOT NULL,
    idagreement     DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: agreement_type 
 */

CREATE TABLE agreement_type(
    idagrtyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idagrtyp)
)
go



/* 
 * TABLE: approval 
 */

CREATE TABLE approval(
    idapproval      DOM_UUID        NOT NULL,
    idapptyp        DOM_INTEGER     NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreq           DOM_UUID        NULL,
    idordite        DOM_UUID        NULL,
    idmessage       DOM_IDENTITY    NULL,
    idleacom        DOM_UUID        NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idapproval)
)
go



/* 
 * TABLE: approval_stockopname_participant_tm 
 */

CREATE TABLE approval_stockopname_participant_tm(
    idapprovalstockopnameparticipant    DOM_IDENTITY    IDENTITY(1,1),
    participantuserid                   int             NULL,
    lastmodifieduserid                  int             NULL,
    lastmodifieddate                    datetime        NULL,
    PRIMARY KEY CLUSTERED (idapprovalstockopnameparticipant)
)
go



/* 
 * TABLE: approval_stockopname_status_tp 
 */

CREATE TABLE approval_stockopname_status_tp(
    idapprovalstockopnamestatus    int             NOT NULL,
    description                    nvarchar(50)    NULL,
    lastmodifieddate               datetime        NULL,
    lastmodifieduserid             int             NULL,
    PRIMARY KEY CLUSTERED (idapprovalstockopnamestatus)
)
go



/* 
 * TABLE: approval_stockopname_tm 
 */

CREATE TABLE approval_stockopname_tm(
    idapprovalstockopname          int              IDENTITY(1,1),
    requestnumber                  nvarchar(50)     NULL,
    idstockopname                  int              NULL,
    activityname                   nvarchar(50)     NULL,
    approvaldate                   datetime         NULL,
    idapprovaluser                 int              NULL,
    requestdate                    datetime         NULL,
    requestuserid                  int              NULL,
    idapprovalstockopnamestatus    int              NULL,
    notes                          nvarchar(200)    NULL,
    lastmodifieddate               datetime         NULL,
    lastmodifieduserid             int              NULL,
    PRIMARY KEY CLUSTERED (idapprovalstockopname)
)
go



/* 
 * TABLE: approval_type 
 */

CREATE TABLE approval_type(
    idapptyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idapptyp)
)
go



/* 
 * TABLE: association_type 
 */

CREATE TABLE association_type(
    idasstyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idasstyp)
)
go



/* 
 * TABLE: attendance 
 */

CREATE TABLE attendance(
    idattendance          DOM_IDENTITY    IDENTITY(1,1),
    idparrol              DOM_UUID        NULL,
    dateattendance        datetime        NULL,
    attendance            bit             NULL,
    attendinfo            varchar(20)     NULL,
    availability          bit             NULL,
    availinfo             varchar(20)     NULL,
    pdi                   bit             NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY NONCLUSTERED (idattendance)
)
go



/* 
 * TABLE: ax_purchaseorder_item 
 */

CREATE TABLE ax_purchaseorder_item(
    idpurchaseorderitem    DOM_INTEGER       NOT NULL,
    ponumber               nvarchar(30)      NULL,
    podate                 datetime          NULL,
    itemname               nvarchar(50)      NULL,
    merk                   nvarchar(30)      NULL,
    type                   nvarchar(30)      NULL,
    branchcode             nvarchar(20)      NULL,
    typecode               nvarchar(10)      NULL,
    price                  decimal(18, 2)    NULL,
    quantity               int               NULL,
    description            nvarchar(100)     NULL,
    PRIMARY KEY CLUSTERED (idpurchaseorderitem)
)
go



/* 
 * TABLE: base_calendar 
 */

CREATE TABLE base_calendar(
    idcalendar     DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    idcaltyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    dtkey          DOM_DATE           NULL,
    workday        DOM_BOOLEAN        NULL,
    seqnum         DOM_INTEGER        NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idcalendar)
)
go



/* 
 * TABLE: bill_to 
 */

CREATE TABLE bill_to(
    idbillto      DOM_ID         NOT NULL,
    idparty       DOM_UUID       NULL,
    idroletype    DOM_INTEGER    NULL,
    PRIMARY KEY NONCLUSTERED (idbillto)
)
go



/* 
 * TABLE: billing 
 */

CREATE TABLE billing(
    idbilling        DOM_UUID           NOT NULL,
    idbiltyp         DOM_IDENTITY       NULL,
    idinternal       DOM_ID             NULL,
    idbilto          DOM_ID             NULL,
    idbilfro         DOM_ID             NULL,
    billingnumber    DOM_ID             NULL,
    dtcreate         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtdue            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    printcounter     DOM_INTEGER        NULL,
    description      DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idbilling)
)
go



/* 
 * TABLE: billing_account 
 */

CREATE TABLE billing_account(
    idbilacc       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idbilacc)
)
go



/* 
 * TABLE: billing_acctg_trans 
 */

CREATE TABLE billing_acctg_trans(
    idacctra     DOM_LONG    NOT NULL,
    idbilling    DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idacctra)
)
go



/* 
 * TABLE: billing_disbursement 
 */

CREATE TABLE billing_disbursement(
    idbilling        DOM_UUID        NOT NULL,
    idbilacc         DOM_IDENTITY    NULL,
    idvendor         DOM_ID          NULL,
    vendorinvoice    DOM_ID          NULL,
    PRIMARY KEY CLUSTERED (idbilling)
)
go



/* 
 * TABLE: billing_issuance 
 */

CREATE TABLE billing_issuance(
    idslip      DOM_UUID    NOT NULL,
    idbilite    DOM_UUID    NULL,
    idinvite    DOM_UUID    NULL,
    idshipto    DOM_ID      NULL,
    qty         DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idslip)
)
go



/* 
 * TABLE: billing_item 
 */

CREATE TABLE billing_item(
    idbilite           DOM_UUID           NOT NULL,
    idbilling          DOM_UUID           NULL,
    idinvite           DOM_UUID           NULL,
    seq                DOM_INTEGER        NULL,
    idproduct          DOM_ID             NULL,
    idwetyp            DOM_INTEGER        NULL,
    idbilitetyp        DOM_IDENTITY       NULL,
    idfeature          DOM_IDENTITY       NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    qty                DOM_QTY            NULL,
    baseprice          DOM_MONEY          NULL,
    unitprice          DOM_MONEY          NULL,
    discount           DOM_MONEY          NULL,
    taxamount          DOM_MONEY          NULL,
    totalamount        DOM_MONEY          NULL,
    PRIMARY KEY NONCLUSTERED (idbilite)
)
go



/* 
 * TABLE: billing_item_type 
 */

CREATE TABLE billing_item_type(
    idbilitetyp    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idbilitetyp)
)
go



/* 
 * TABLE: billing_njb 
 */

CREATE TABLE billing_njb(
    idbilling             DOM_UUID          NOT NULL,
    nonjb                 varchar(30)       NOT NULL,
    totalbaseprice        decimal(18, 2)    NULL,
    totaltaxamount        decimal(18, 2)    NULL,
    mischarge             decimal(18, 2)    NULL,
    totalunitprice        decimal(18, 2)    NULL,
    totaldiscount         decimal(18, 2)    NULL,
    grandtotalamount      decimal(18, 2)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    PRIMARY KEY NONCLUSTERED (idbilling),
    UNIQUE (nonjb)
)
go



/* 
 * TABLE: billing_nsc 
 */

CREATE TABLE billing_nsc(
    idbilling             DOM_UUID          NOT NULL,
    nonsc                 varchar(30)       NOT NULL,
    totalbaseprice        decimal(18, 2)    NULL,
    totaltaxamount        decimal(18, 2)    NULL,
    mischarge             decimal(18, 2)    NULL,
    totalunitprice        decimal(18, 2)    NULL,
    totaldiscount         decimal(18, 2)    NULL,
    grandtotalamount      decimal(18, 2)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    PRIMARY KEY NONCLUSTERED (idbilling),
    UNIQUE (nonsc)
)
go



/* 
 * TABLE: billing_receipt 
 */

CREATE TABLE billing_receipt(
    idbilling    DOM_UUID        NOT NULL,
    idbilacc     DOM_IDENTITY    NULL,
    PRIMARY KEY CLUSTERED (idbilling)
)
go



/* 
 * TABLE: billing_role 
 */

CREATE TABLE billing_role(
    idrole        DOM_UUID        NOT NULL,
    idbilling     DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: billing_status 
 */

CREATE TABLE billing_status(
    idstatus        DOM_UUID        NOT NULL,
    idbilling       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: billing_type 
 */

CREATE TABLE billing_type(
    idbiltyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idbiltyp)
)
go



/* 
 * TABLE: booking 
 */

CREATE TABLE booking(
    idbooking             DOM_IDENTITY    IDENTITY(1,1),
    idbookingstatus       DOM_INTEGER     NULL,
    nobooking             varchar(4)      NOT NULL,
    customername          varchar(100)    NULL,
    vehiclenumber         varchar(20)     NULL,
    datebook              datetime        NULL,
    bookingtime           datetime        NULL,
    waitinglimit          datetime        NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY NONCLUSTERED (idbooking),
    UNIQUE (nobooking)
)
go



/* 
 * TABLE: booking_slot 
 */

CREATE TABLE booking_slot(
    idbooslo       DOM_UUID        NOT NULL,
    idcalendar     DOM_IDENTITY    NULL,
    idbooslostd    DOM_UUID        NULL,
    slotnumber     DOM_ID          NULL,
    PRIMARY KEY NONCLUSTERED (idbooslo)
)
go



/* 
 * TABLE: booking_slot_standard 
 */

CREATE TABLE booking_slot_standard(
    idbooslostd    DOM_UUID           NOT NULL,
    idboktyp       DOM_IDENTITY       NULL,
    idinternal     DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    capacity       DOM_INTEGER        NULL,
    starthour      DOM_INTEGER        NULL,
    startminute    DOM_INTEGER        NULL,
    endhour        DOM_INTEGER        NULL,
    endminute      DOM_INTEGER        NULL,
    PRIMARY KEY NONCLUSTERED (idbooslostd)
)
go



/* 
 * TABLE: booking_status 
 */

CREATE TABLE booking_status(
    idbookingstatus    DOM_INTEGER        NOT NULL,
    description        DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idbookingstatus)
)
go



/* 
 * TABLE: booking_type 
 */

CREATE TABLE booking_type(
    idboktyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idboktyp)
)
go



/* 
 * TABLE: branch 
 */

CREATE TABLE branch(
    idbranchcategory    DOM_IDENTITY    NULL,
    idinternal          DOM_ID          NOT NULL,
    PRIMARY KEY NONCLUSTERED (idinternal)
)
go



/* 
 * TABLE: broker_type 
 */

CREATE TABLE broker_type(
    idbrotyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idbrotyp)
)
go



/* 
 * TABLE: calendar_type 
 */

CREATE TABLE calendar_type(
    idcaltyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idcaltyp)
)
go



/* 
 * TABLE: carrier 
 */

CREATE TABLE carrier(
    idcarrier    DOM_INTEGER    NOT NULL,
    idvehicle    DOM_UUID       NULL,
    idparty      DOM_UUID       NULL,
    idreason     DOM_INTEGER    NULL,
    idreltyp     DOM_INTEGER    NULL,
    PRIMARY KEY CLUSTERED (idcarrier)
)
go



/* 
 * TABLE: category 
 */

CREATE TABLE category(
    idcategory     DOM_INTEGER        NOT NULL,
    idcattyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idcategory)
)
go



/* 
 * TABLE: category_asset_tp 
 */

CREATE TABLE category_asset_tp(
    idcategoryasset       int             IDENTITY(1,1),
    description           nvarchar(50)    NULL,
    statuscode            int             NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    PRIMARY KEY CLUSTERED (idcategoryasset)
)
go



/* 
 * TABLE: category_type 
 */

CREATE TABLE category_type(
    idcattyp       DOM_INTEGER        NOT NULL,
    idparent       DOM_INTEGER        NULL,
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idcattyp)
)
go



/* 
 * TABLE: charge_to_type 
 */

CREATE TABLE charge_to_type(
    idchargeto     int            NOT NULL,
    description    varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idchargeto)
)
go



/* 
 * TABLE: city 
 */

CREATE TABLE city(
    idgeobou    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



/* 
 * TABLE: claim_detail 
 */

CREATE TABLE claim_detail(
    idclaim               DOM_IDENTITY      IDENTITY(1,1),
    idcustomer            DOM_ID            NULL,
    idsympthomdatam       DOM_IDENTITY      NULL,
    iddatadamage          int               NULL,
    noclaim               int               NOT NULL,
    claimtype             varchar(5)        NULL,
    statusclaim           varchar(20)       NULL,
    noticket              varchar(20)       NULL,
    submissiongroup_      varchar(20)       NULL,
    nolkh                 varchar(30)       NULL,
    datelkh               datetime          NULL,
    noho                  datetime          NULL,
    dateho                date              NULL,
    notype                varchar(5)        NULL,
    marketname            varchar(30)       NULL,
    datebuy               datetime          NULL,
    noproduction          varchar(15)       NULL,
    brokendate            datetime          NULL,
    kmbroken              int               NULL,
    servicedate           datetime          NULL,
    rank                  char(1)           NULL,
    finishdate            datetime          NULL,
    analysisresult        varchar(100)      NULL,
    amount                decimal(18, 2)    NULL,
    mainpart              bit               NULL,
    dsubtotaljob          decimal(18, 2)    NULL,
    transferdate          datetime          NULL,
    nominaltransfer       decimal(18, 2)    NULL,
    nodocs                varchar(15)       NULL,
    replace_              varchar(10)       NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    PRIMARY KEY NONCLUSTERED (idclaim),
    UNIQUE (noclaim)
)
go



/* 
 * TABLE: communication_event 
 */

CREATE TABLE communication_event(
    idcomevt    DOM_UUID        NOT NULL,
    idparrel    DOM_UUID        NULL,
    idevetyp    DOM_IDENTITY    NULL,
    note        DOM_LONGDESC    NULL,
    dtfrom      DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru      DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idcomevt)
)
go



/* 
 * TABLE: communication_event_cdb 
 */

CREATE TABLE communication_event_cdb(
    idcomevt           DOM_UUID           NOT NULL,
    idcustomer         DOM_ID             NULL,
    idprovince         DOM_UUID           NULL,
    idcity             DOM_UUID           NULL,
    iddistrict         DOM_UUID           NULL,
    address            DOM_LONGDESC       NULL,
    hobby              DOM_DESCRIPTION    NULL,
    homestatus         DOM_DESCRIPTION    NULL,
    exponemonth        DOM_DESCRIPTION    NULL,
    job                DOM_DESCRIPTION    NULL,
    lasteducation      DOM_DESCRIPTION    NULL,
    owncellphone       DOM_DESCRIPTION    NULL,
    hondarefference    DOM_DESCRIPTION    NULL,
    ownvehiclebrand    DOM_DESCRIPTION    NULL,
    ownvehicletype     DOM_DESCRIPTION    NULL,
    buyfor             DOM_DESCRIPTION    NULL,
    vehicleuser        DOM_DESCRIPTION    NULL,
    facebook           DOM_DESCRIPTION    NULL,
    instagram          DOM_DESCRIPTION    NULL,
    twitter            DOM_DESCRIPTION    NULL,
    youtube            DOM_DESCRIPTION    NULL,
    email              DOM_DESCRIPTION    NULL,
    idreligion         DOM_INTEGER        NULL,
    iscityidadd        DOM_BOOLEAN        NULL,
    ismailadd          DOM_BOOLEAN        NULL,
    PRIMARY KEY CLUSTERED (idcomevt)
)
go



/* 
 * TABLE: communication_event_delivery 
 */

CREATE TABLE communication_event_delivery(
    idcomevt         DOM_UUID        NOT NULL,
    idinternal       DOM_ID          NULL,
    idcustomer       DOM_ID          NULL,
    bussinesscode    varchar(15)     NULL,
    b1               varchar(15)     NULL,
    b2               varchar(15)     NULL,
    b3               varchar(15)     NULL,
    b4               varchar(15)     NULL,
    c11rate          int             NULL,
    c11reason        varchar(150)    NULL,
    c11hope          varchar(150)    NULL,
    c12rate          int             NULL,
    c12reason        varchar(150)    NULL,
    c13hope          varchar(150)    NULL,
    c14rate          int             NULL,
    c14reason        varchar(150)    NULL,
    c14hope          varchar(150)    NULL,
    c15rate          int             NULL,
    c15reason        varchar(150)    NULL,
    c15hope          varchar(150)    NULL,
    c16rate          int             NULL,
    c16reason        varchar(150)    NULL,
    c16hope          varchar(150)    NULL,
    c17rate          int             NULL,
    c17reason        varchar(150)    NULL,
    c17hope          varchar(150)    NULL,
    c18rate          int             NULL,
    c18reason        varchar(150)    NULL,
    c18hope          varchar(150)    NULL,
    c19rate          int             NULL,
    c19reason        varchar(150)    NULL,
    c19hope          varchar(150)    NULL,
    c110rate         int             NULL,
    c110reason       varchar(150)    NULL,
    c110hope         varchar(150)    NULL,
    c2rate           int             NULL,
    c3option         varchar(150)    NULL,
    d1name           varchar(150)    NULL,
    d2gender         varchar(150)    NULL,
    d3age            int             NULL,
    d4phone          varchar(150)    NULL,
    d5vehicle        varchar(150)    NULL,
    d6education      varchar(150)    NULL,
    dealername       varchar(150)    NULL,
    dealeraddress    varchar(150)    NULL,
    c13rate          varchar(75)     NULL,
    c13reason        varchar(150)    NULL,
    c12hope          varchar(150)    NULL,
    PRIMARY KEY CLUSTERED (idcomevt)
)
go



/* 
 * TABLE: communication_event_prospect 
 */

CREATE TABLE communication_event_prospect(
    idcomevt        DOM_UUID        NOT NULL,
    idprospect      DOM_UUID        NULL,
    idsaletype      DOM_IDENTITY    NULL,
    idproduct       DOM_ID          NULL,
    idcolor         DOM_IDENTITY    NULL,
    interest        DOM_BOOLEAN     NULL,
    qty             DOM_QTY         NULL,
    purchaseplan    DOM_ID          NULL,
    connect         DOM_BOOLEAN     NULL,
    PRIMARY KEY CLUSTERED (idcomevt)
)
go



/* 
 * TABLE: communication_event_purpose 
 */

CREATE TABLE communication_event_purpose(
    idcomevt         DOM_UUID       NOT NULL,
    idpurposetype    DOM_INTEGER    NOT NULL,
    PRIMARY KEY CLUSTERED (idcomevt, idpurposetype)
)
go



/* 
 * TABLE: communication_event_role 
 */

CREATE TABLE communication_event_role(
    idrole        DOM_UUID        NOT NULL,
    idcomevt      DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrole)
)
go



/* 
 * TABLE: communication_event_status 
 */

CREATE TABLE communication_event_status(
    idstatus        DOM_UUID        NOT NULL,
    idcomevt        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: contact_mechanism 
 */

CREATE TABLE contact_mechanism(
    idcontact        DOM_UUID           NOT NULL,
    idcontacttype    DOM_INTEGER        NULL,
    description      DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idcontact)
)
go



/* 
 * TABLE: contact_purpose 
 */

CREATE TABLE contact_purpose(
    idcontact        DOM_UUID       NOT NULL,
    idpurposetype    DOM_INTEGER    NOT NULL,
    PRIMARY KEY CLUSTERED (idcontact, idpurposetype)
)
go



/* 
 * TABLE: container 
 */

CREATE TABLE container(
    idcontainer    DOM_UUID           NOT NULL,
    idfacility     DOM_UUID           NULL,
    idcontyp       DOM_IDENTITY       NULL,
    code           DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idcontainer)
)
go



/* 
 * TABLE: container_type 
 */

CREATE TABLE container_type(
    idcontyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idcontyp)
)
go



/* 
 * TABLE: correction_note 
 */

CREATE TABLE correction_note(
    idcorrectnote       DOM_INTEGER        NOT NULL,
    idproduct           DOM_ID             NULL,
    idspgclaim          int                NULL,
    idissue             int                NULL,
    correctionnumber    DOM_DESCRIPTION    NULL,
    qty                 DOM_QTY            NULL,
    dtcorrection        datetime           NULL,
    PRIMARY KEY CLUSTERED (idcorrectnote)
)
go



/* 
 * TABLE: customer 
 */

CREATE TABLE customer(
    idcustomer          DOM_ID          NOT NULL,
    idmarketcategory    DOM_IDENTITY    NULL,
    idparty             DOM_UUID        NULL,
    idroletype          DOM_INTEGER     NULL,
    idsegmen            DOM_INTEGER     NULL,
    idcustomertype      DOM_INTEGER     NULL,
    idmpm               DOM_ID          NULL,
    PRIMARY KEY NONCLUSTERED (idcustomer)
)
go



/* 
 * TABLE: customer_order 
 */

CREATE TABLE customer_order(
    idorder       DOM_UUID    NOT NULL,
    idinternal    DOM_ID      NULL,
    idbillto      DOM_ID      NULL,
    idcustomer    DOM_ID      NULL,
    idvehicle     DOM_UUID    NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: customer_quotation 
 */

CREATE TABLE customer_quotation(
    idquote       DOM_UUID    NOT NULL,
    idinternal    DOM_ID      NULL,
    idcustomer    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idquote)
)
go



/* 
 * TABLE: customer_relationship 
 */

CREATE TABLE customer_relationship(
    idparrel      DOM_UUID    NOT NULL,
    idinternal    DOM_ID      NULL,
    idcustomer    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idparrel)
)
go



/* 
 * TABLE: customer_request_item 
 */

CREATE TABLE customer_request_item(
    idreqitem     DOM_UUID     NOT NULL,
    idcustomer    DOM_ID       NULL,
    qty           DOM_QTY      NULL,
    unitprice     DOM_MONEY    NULL,
    bbn           DOM_MONEY    NULL,
    idframe       DOM_ID       NULL,
    idmachine     DOM_ID       NULL,
    PRIMARY KEY CLUSTERED (idreqitem)
)
go



/* 
 * TABLE: customer_type 
 */

CREATE TABLE customer_type(
    idcostumertype    DOM_INTEGER    NOT NULL,
    description       varchar(50)    NULL,
    PRIMARY KEY CLUSTERED (idcostumertype)
)
go



/* 
 * TABLE: data_damage 
 */

CREATE TABLE data_damage(
    iddatadamage    int            IDENTITY(1,1),
    descdamage      varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (iddatadamage)
)
go



/* 
 * TABLE: dealer_claim_type 
 */

CREATE TABLE dealer_claim_type(
    idclaimtype    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idclaimtype)
)
go



/* 
 * TABLE: dealer_reminder_type 
 */

CREATE TABLE dealer_reminder_type(
    idremindertype    DOM_INTEGER        NOT NULL,
    description       DOM_DESCRIPTION    NULL,
    messages          varchar(600)       NULL,
    PRIMARY KEY NONCLUSTERED (idremindertype)
)
go



/* 
 * TABLE: deliverable_type 
 */

CREATE TABLE deliverable_type(
    iddeltype      DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (iddeltype)
)
go



/* 
 * TABLE: delivery_order 
 */

CREATE TABLE delivery_order(
    invoicevendornumber    varchar(30)    NULL,
    idbilling              DOM_UUID       NOT NULL,
    PRIMARY KEY CLUSTERED (idbilling)
)
go



/* 
 * TABLE: delivery_order_status 
 */

CREATE TABLE delivery_order_status(
    iddeliveryorderstatus    int            NOT NULL,
    description              varchar(50)    NULL,
    PRIMARY KEY CLUSTERED (iddeliveryorderstatus)
)
go



/* 
 * TABLE: dimension 
 */

CREATE TABLE dimension(
    iddimension    DOM_ID             NOT NULL,
    iddimtyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (iddimension)
)
go



/* 
 * TABLE: dimension_type 
 */

CREATE TABLE dimension_type(
    iddimtyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (iddimtyp)
)
go



/* 
 * TABLE: disbursement 
 */

CREATE TABLE disbursement(
    idpayment    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idpayment)
)
go



/* 
 * TABLE: dispatch_status 
 */

CREATE TABLE dispatch_status(
    iddispatchstatus    DOM_INTEGER    NOT NULL,
    description         varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (iddispatchstatus)
)
go



/* 
 * TABLE: district 
 */

CREATE TABLE district(
    idgeobou    DOM_UUID    NOT NULL,
    PRIMARY KEY CLUSTERED (idgeobou)
)
go



/* 
 * TABLE: document_type 
 */

CREATE TABLE document_type(
    iddoctype      DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (iddoctype)
)
go



/* 
 * TABLE: documents 
 */

CREATE TABLE documents(
    iddocument    DOM_UUID           NOT NULL,
    iddoctype     DOM_IDENTITY       NULL,
    note          DOM_DESCRIPTION    NULL,
    dtcreate      DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY CLUSTERED (iddocument)
)
go



/* 
 * TABLE: driver 
 */

CREATE TABLE driver(
    idparrol      DOM_UUID       NOT NULL,
    idvendor      DOM_ID         NULL,
    iddriver      varchar(90)    NULL,
    isexternal    char(5)        NULL,
    PRIMARY KEY CLUSTERED (idparrol)
)
go



/* 
 * TABLE: electronic_address 
 */

CREATE TABLE electronic_address(
    idcontact    DOM_UUID       NOT NULL,
    address      DOM_ADDRESS    NULL,
    PRIMARY KEY NONCLUSTERED (idcontact)
)
go



/* 
 * TABLE: employee 
 */

CREATE TABLE employee(
    employeenumber    DOM_ID      NOT NULL,
    idparty           DOM_UUID    NULL,
    idparrol          DOM_UUID    NULL,
    PRIMARY KEY NONCLUSTERED (employeenumber)
)
go



/* 
 * TABLE: employee_customer_relationship 
 */

CREATE TABLE employee_customer_relationship(
    idparrel          DOM_UUID    NOT NULL,
    employeenumber    DOM_ID      NULL,
    idcustomer        DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idparrel)
)
go



/* 
 * TABLE: event_type 
 */

CREATE TABLE event_type(
    idevetyp       DOM_IDENTITY       IDENTITY(1,1),
    idprneve       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idevetyp)
)
go



/* 
 * TABLE: exchange_part 
 */

CREATE TABLE exchange_part(
    idspgclaimaction    int               NULL,
    idproduct           DOM_ID            NULL,
    idspgclaimdetail    DOM_IDENTITY      NOT NULL,
    qty                 decimal(14, 2)    NULL,
    dtexchangepart      datetime          NULL,
    PRIMARY KEY CLUSTERED (idspgclaimdetail)
)
go



/* 
 * TABLE: external_acctg_trans 
 */

CREATE TABLE external_acctg_trans(
    idacctra    DOM_LONG    NOT NULL,
    idparfro    DOM_UUID    NULL,
    idparto     DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idacctra)
)
go



/* 
 * TABLE: facility 
 */

CREATE TABLE facility(
    idfacility        DOM_UUID           NOT NULL,
    idfacilitytype    DOM_INTEGER        NULL,
    idpartof          DOM_UUID           NULL,
    idfa              DOM_IDENTITY       NULL,
    code              DOM_ID             NULL,
    description       DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idfacility)
)
go



/* 
 * TABLE: facility_contact_mechanism 
 */

CREATE TABLE facility_contact_mechanism(
    idconmecpur      DOM_UUID        NOT NULL,
    idfacility       DOM_UUID        NULL,
    idcontact        DOM_UUID        NULL,
    idpurposetype    DOM_INTEGER     NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idconmecpur)
)
go



/* 
 * TABLE: facility_status 
 */

CREATE TABLE facility_status(
    idstatus        DOM_UUID        NOT NULL,
    idfacility      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: facility_type 
 */

CREATE TABLE facility_type(
    idfacilitytype    DOM_INTEGER        NOT NULL,
    description       DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idfacilitytype)
)
go



/* 
 * TABLE: feature 
 */

CREATE TABLE feature(
    idfeature      DOM_IDENTITY       IDENTITY(1,1),
    idfeatyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    PRIMARY KEY NONCLUSTERED (idfeature)
)
go



/* 
 * TABLE: feature_applicable 
 */

CREATE TABLE feature_applicable(
    idapplicability    DOM_UUID        NOT NULL,
    idfeature          DOM_IDENTITY    NULL,
    idproduct          DOM_ID          NULL,
    idfeatyp           DOM_IDENTITY    NULL,
    idowner            DOM_UUID        NULL,
    boolvalue          DOM_BOOLEAN     NULL,
    dtfrom             DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru             DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idapplicability)
)
go



/* 
 * TABLE: feature_type 
 */

CREATE TABLE feature_type(
    idfeatyp       DOM_IDENTITY       IDENTITY(1,1),
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    PRIMARY KEY NONCLUSTERED (idfeatyp)
)
go



/* 
 * TABLE: fixed_asset 
 */

CREATE TABLE fixed_asset(
    idfa           DOM_IDENTITY       IDENTITY(1,1),
    idfatype       DOM_IDENTITY       NULL,
    iduom          DOM_ID             NULL,
    name           DOM_NAME           NULL,
    dtacquired     DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idfa)
)
go



/* 
 * TABLE: fixed_asset_type 
 */

CREATE TABLE fixed_asset_type(
    idfatype       DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idfatype)
)
go



/* 
 * TABLE: fixedasset_adjustment_th 
 */

CREATE TABLE fixedasset_adjustment_th(
    idfixedassetadjustment    int         IDENTITY(1,1),
    idfixedasset              int         NULL,
    adjusmentdate             datetime    NULL,
    adjustmentquantity        int         NULL,
    newquantity               int         NULL,
    lastmodifieddate          datetime    NULL,
    lastmodifieduserid        int         NULL,
    PRIMARY KEY CLUSTERED (idfixedassetadjustment)
)
go



/* 
 * TABLE: fixedasset_th 
 */

CREATE TABLE fixedasset_th(
    idfixedassetth        int         IDENTITY(1,1),
    idfixedasset          int         NULL,
    idlocationroom        int         NULL,
    idlocationfloor       int         NULL,
    idpicuser             int         NULL,
    lastmodifieddate      datetime    NULL,
    lastmodifieduserid    int         NULL,
    PRIMARY KEY CLUSTERED (idfixedassetth)
)
go



/* 
 * TABLE: fixedasset_tm 
 */

CREATE TABLE fixedasset_tm(
    idfixedasset                int              IDENTITY(1,1),
    idpurchaseorderreception    int              NULL,
    idpurchaseorder             int              NULL,
    idfloor                     int              NULL,
    idroom                      int              NULL,
    idcategoryasset             int              NULL,
    batchno                     int              NULL,
    assetid                     nvarchar(max)    NULL,
    idpicuser                   int              NULL,
    name                        nvarchar(50)     NULL,
    price                       money            NULL,
    orderquantity               int              NULL,
    acceptedquantity            int              NULL,
    currentquantity             int              NULL,
    unitmeasure                 nvarchar(50)     NULL,
    picname                     nvarchar(50)     NULL,
    lastmodifieddate            datetime         NULL,
    lastmodifieduserid          int              NULL,
    statuscode                  int              NULL,
    PRIMARY KEY CLUSTERED (idfixedasset)
)
go



/* 
 * TABLE: floor_tm 
 */

CREATE TABLE floor_tm(
    idfloor               int             IDENTITY(1,1),
    floorname             nvarchar(50)    NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY CLUSTERED (idfloor)
)
go



/* 
 * TABLE: gender 
 */

CREATE TABLE gender(
    idgender       DOM_FLAG           NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idgender)
)
go



/* 
 * TABLE: general_upload 
 */

CREATE TABLE general_upload(
    idgenupl            DOM_UUID           NOT NULL,
    idinternal          DOM_ID             NULL,
    idpurposetype       DOM_INTEGER        NULL,
    dtcreate            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description         DOM_DESCRIPTION    NULL,
    filename            varchar(255)       NULL,
    value               DOM_BLOB           NULL,
    valuecontenttype    DOM_LONGDESC       NULL,
    PRIMARY KEY CLUSTERED (idgenupl)
)
go



/* 
 * TABLE: general_upload_status 
 */

CREATE TABLE general_upload_status(
    idstatus        DOM_UUID        NOT NULL,
    idgenupl        DOM_UUID        NULL,
    idstatustype    int             NULL,
    idreason        int             NULL,
    reason          DOM_LONGDESC    NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: geo_boundary 
 */

CREATE TABLE geo_boundary(
    idgeobou        DOM_UUID           NOT NULL,
    idparent        DOM_UUID           NULL,
    idgeoboutype    DOM_INTEGER        NULL,
    geocode         DOM_ID             NULL,
    description     DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



/* 
 * TABLE: geo_boundary_type 
 */

CREATE TABLE geo_boundary_type(
    idgeoboutype    DOM_INTEGER        NOT NULL,
    description     DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idgeoboutype)
)
go



/* 
 * TABLE: gl_account 
 */

CREATE TABLE gl_account(
    idglacc        DOM_UUID           NOT NULL,
    idglacctyp     DOM_INTEGER        NULL,
    name           DOM_NAME           NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idglacc)
)
go



/* 
 * TABLE: gl_account_type 
 */

CREATE TABLE gl_account_type(
    idglacctyp     DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idglacctyp)
)
go



/* 
 * TABLE: gl_dimension 
 */

CREATE TABLE gl_dimension(
    idgldim       DOM_UUID        NOT NULL,
    idbillto      DOM_ID          NULL,
    idvendor      DOM_ID          NULL,
    idinternal    DOM_ID          NULL,
    idprocat      DOM_IDENTITY    NULL,
    idparcat      DOM_IDENTITY    NULL,
    dimension1    DOM_ID          NULL,
    dimension2    DOM_ID          NULL,
    dimension3    DOM_ID          NULL,
    dimension4    DOM_ID          NULL,
    dimension5    DOM_ID          NULL,
    dimension6    DOM_ID          NULL,
    PRIMARY KEY CLUSTERED (idgldim)
)
go



/* 
 * TABLE: good 
 */

CREATE TABLE good(
    idproduct     DOM_ID         NOT NULL,
    iduom         DOM_ID         NULL,
    serialized    DOM_BOOLEAN    NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: good_container 
 */

CREATE TABLE good_container(
    idgoocon       DOM_UUID        NOT NULL,
    idproduct      DOM_ID          NULL,
    idcontainer    DOM_UUID        NULL,
    idparty        DOM_UUID        NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idgoocon)
)
go



/* 
 * TABLE: good_promotion 
 */

CREATE TABLE good_promotion(
    idproduct    DOM_ID      NOT NULL,
    ispromo      bit         NULL,
    dtcreate     datetime    NULL,
    PRIMARY KEY CLUSTERED (idproduct)
)
go



/* 
 * TABLE: history_store 
 */

CREATE TABLE history_store(
    timemark        datetime         NOT NULL,
    table_name      nvarchar(50)     NOT NULL,
    pk_date_src     nvarchar(400)    NOT NULL,
    pk_date_dest    nvarchar(400)    NOT NULL,
    record_state    smallint         NOT NULL,
    PRIMARY KEY CLUSTERED (table_name, pk_date_dest)
)
go



/* 
 * TABLE: intern 
 */

CREATE TABLE intern(
    idintern       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idintern)
)
go



/* 
 * TABLE: internal 
 */

CREATE TABLE internal(
    idinternal      DOM_ID         NOT NULL,
    idroot          DOM_ID         NULL,
    idparent        DOM_ID         NULL,
    idroletype      DOM_INTEGER    NULL,
    idparty         DOM_UUID       NULL,
    selfsender      DOM_BOOLEAN    NULL,
    idmpm           DOM_ID         NULL,
    idahm           DOM_ID         NULL,
    iddealercode    DOM_ID         NULL,
    PRIMARY KEY NONCLUSTERED (idinternal)
)
go



/* 
 * TABLE: internal_acctg_trans 
 */

CREATE TABLE internal_acctg_trans(
    idacctra      DOM_LONG    NOT NULL,
    idinternal    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idacctra)
)
go



/* 
 * TABLE: internal_facility_purpose 
 */

CREATE TABLE internal_facility_purpose(
    idintfacpur      DOM_UUID        NOT NULL,
    idpurposetype    DOM_INTEGER     NULL,
    idfacility       DOM_UUID        NULL,
    idinternal       DOM_ID          NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idintfacpur)
)
go



/* 
 * TABLE: internal_order 
 */

CREATE TABLE internal_order(
    idorder     DOM_UUID    NOT NULL,
    idintto     DOM_ID      NULL,
    idintfro    DOM_ID      NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: inventory_adjustment 
 */

CREATE TABLE inventory_adjustment(
    idinvadj      DOM_UUID    NOT NULL,
    idinvite      DOM_UUID    NULL,
    idstopnite    DOM_UUID    NULL,
    qty           DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idinvadj)
)
go



/* 
 * TABLE: inventory_item 
 */

CREATE TABLE inventory_item(
    idinvite        DOM_UUID        NOT NULL,
    idowner         DOM_UUID        NULL,
    idproduct       DOM_ID          NULL,
    idfacility      DOM_UUID        NULL,
    idcontainer     DOM_UUID        NULL,
    idreceipt       DOM_UUID        NULL,
    idfeature       DOM_IDENTITY    NULL,
    idfa            DOM_IDENTITY    NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    qty             DOM_QTY         NULL,
    qtybooking      DOM_QTY         NULL,
    idmachine       DOM_ID          NULL,
    idframe         DOM_ID          NULL,
    yearassembly    DOM_INTEGER     NULL,
    PRIMARY KEY NONCLUSTERED (idinvite)
)
go



/* 
 * TABLE: inventory_item_status 
 */

CREATE TABLE inventory_item_status(
    idstatus        DOM_UUID        NOT NULL,
    idinvite        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: inventory_movement 
 */

CREATE TABLE inventory_movement(
    idslip      DOM_UUID        NOT NULL,
    dtcreate    DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    slipnum     DOM_ID          NULL,
    PRIMARY KEY NONCLUSTERED (idslip)
)
go



/* 
 * TABLE: inventory_movement_status 
 */

CREATE TABLE inventory_movement_status(
    idstatus        DOM_UUID        NOT NULL,
    idslip          DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: inventory_request_item 
 */

CREATE TABLE inventory_request_item(
    idinvite     DOM_UUID          NOT NULL,
    idreqitem    DOM_UUID          NOT NULL,
    qty          decimal(18, 0)    NULL,
    PRIMARY KEY CLUSTERED (idinvite, idreqitem)
)
go



/* 
 * TABLE: item_issuance 
 */

CREATE TABLE item_issuance(
    iditeiss    DOM_UUID    NOT NULL,
    idshiite    DOM_UUID    NULL,
    idinvite    DOM_UUID    NULL,
    idslip      DOM_UUID    NULL,
    qty         DOM_QTY     NULL,
    PRIMARY KEY NONCLUSTERED (iditeiss)
)
go



/* 
 * TABLE: job 
 */

CREATE TABLE job(
    idpkb                 DOM_IDENTITY    NOT NULL,
    idparrol              DOM_UUID        NULL,
    idjobstatus           int             NULL,
    starttime             datetime        NULL,
    complaint             varchar(255)    NULL,
    jobsuggest            varchar(255)    NULL,
    duration              int             NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY NONCLUSTERED (idpkb)
)
go



/* 
 * TABLE: job_dispatch 
 */

CREATE TABLE job_dispatch(
    idjobdispatch       DOM_IDENTITY    IDENTITY(1,1),
    iddispatchstatus    DOM_INTEGER     NULL,
    idpkb               DOM_IDENTITY    NULL,
    idparrol            DOM_UUID        NULL,
    idjobpriority       DOM_INTEGER     NULL,
    dispatchdate        datetime        NULL,
    PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



/* 
 * TABLE: job_dispatch_customer_type 
 */

CREATE TABLE job_dispatch_customer_type(
    idjobdispatch     DOM_IDENTITY    NOT NULL,
    idcostumertype    DOM_INTEGER     NULL,
    PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



/* 
 * TABLE: job_dispatch_group_service 
 */

CREATE TABLE job_dispatch_group_service(
    idjobdispatch    DOM_IDENTITY    NOT NULL,
    PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



/* 
 * TABLE: job_dispatch_pkb_type 
 */

CREATE TABLE job_dispatch_pkb_type(
    idjobdispatch    DOM_IDENTITY    NOT NULL,
    idtypepkb        DOM_INTEGER     NULL,
    PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



/* 
 * TABLE: job_dispatch_service 
 */

CREATE TABLE job_dispatch_service(
    idjobdispatch    DOM_IDENTITY    NOT NULL,
    idproduct        DOM_ID          NULL,
    PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



/* 
 * TABLE: job_history 
 */

CREATE TABLE job_history(
    idjobhistory       int             IDENTITY(1,1),
    idpkb              DOM_IDENTITY    NULL,
    idjobstatus        int             NULL,
    idpendingreason    int             NULL,
    idparrol           DOM_UUID        NULL,
    time               datetime        NULL,
    duration           int             NULL,
    PRIMARY KEY CLUSTERED (idjobhistory)
)
go



/* 
 * TABLE: job_priority 
 */

CREATE TABLE job_priority(
    idjobpriority        DOM_INTEGER    NOT NULL,
    idjobprioritytype    DOM_INTEGER    NULL,
    priorityname         varchar(30)    NULL,
    priority             int            NULL,
    dtcreate             datetime       NULL,
    PRIMARY KEY CLUSTERED (idjobpriority)
)
go



/* 
 * TABLE: job_priority_customer_type 
 */

CREATE TABLE job_priority_customer_type(
    idjobpriority     DOM_INTEGER    NOT NULL,
    idcostumertype    DOM_INTEGER    NULL,
    PRIMARY KEY CLUSTERED (idjobpriority)
)
go



/* 
 * TABLE: job_priority_group_service 
 */

CREATE TABLE job_priority_group_service(
    idjobpriority    DOM_INTEGER    NOT NULL,
    PRIMARY KEY CLUSTERED (idjobpriority)
)
go



/* 
 * TABLE: job_priority_pkb_type 
 */

CREATE TABLE job_priority_pkb_type(
    idjobpriority    DOM_INTEGER    NOT NULL,
    idtypepkb        DOM_INTEGER    NULL,
    PRIMARY KEY CLUSTERED (idjobpriority)
)
go



/* 
 * TABLE: job_priority_service 
 */

CREATE TABLE job_priority_service(
    idjobpriority    DOM_INTEGER    NOT NULL,
    idproduct        DOM_ID         NULL,
    PRIMARY KEY CLUSTERED (idjobpriority)
)
go



/* 
 * TABLE: job_priority_type 
 */

CREATE TABLE job_priority_type(
    idjobprioritytype    DOM_INTEGER    NOT NULL,
    jobtypename          varchar(30)    NULL,
    dtcreate             datetime       NULL,
    PRIMARY KEY CLUSTERED (idjobprioritytype)
)
go



/* 
 * TABLE: job_service 
 */

CREATE TABLE job_service(
    idjobservice    int             NOT NULL,
    idpkb           DOM_IDENTITY    NULL,
    idproduct       DOM_ID          NULL,
    PRIMARY KEY CLUSTERED (idjobservice)
)
go



/* 
 * TABLE: job_status 
 */

CREATE TABLE job_status(
    idjobstatus     DOM_INTEGER        NOT NULL,
    idstatustype    DOM_INTEGER        NULL,
    desc_           DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idjobstatus)
)
go



/* 
 * TABLE: job_suggest 
 */

CREATE TABLE job_suggest(
    idjobsuggest    int         IDENTITY(1,1),
    idproduct       DOM_ID      NULL,
    firstkm         int         NULL,
    lastkm          int         NULL,
    dtfrom          datetime    NULL,
    dtthru          datetime    NULL,
    PRIMARY KEY CLUSTERED (idjobsuggest)
)
go



/* 
 * TABLE: job_suggest_service 
 */

CREATE TABLE job_suggest_service(
    idjobsuggestservice    int         IDENTITY(1,1),
    idjobsuggest           int         NULL,
    idproduct              DOM_ID      NULL,
    dtfrom                 datetime    NULL,
    dtthru                 datetime    NULL,
    PRIMARY KEY CLUSTERED (idjobsuggestservice)
)
go



/* 
 * TABLE: leasing_company 
 */

CREATE TABLE leasing_company(
    idparrol        DOM_UUID    NOT NULL,
    idleacom        DOM_ID      NULL,
    idleasingahm    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idparrol)
)
go



/* 
 * TABLE: leasing_tenor_provide 
 */

CREATE TABLE leasing_tenor_provide(
    idlsgpro       DOM_UUID        NOT NULL,
    idleacom       DOM_UUID        NULL,
    idproduct      DOM_ID          NULL,
    tenor          DOM_INTEGER     NULL,
    installment    DOM_MONEY       NULL,
    downpayment    DOM_MONEY       NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idlsgpro)
)
go



/* 
 * TABLE: market_treatment 
 */

CREATE TABLE market_treatment(
    idmarkettreatment    int             IDENTITY(1,1),
    idvendor             DOM_ID          NULL,
    idbilling            DOM_UUID        NULL,
    name                 varchar(90)     NULL,
    description          varchar(300)    NULL,
    juklaknumber         varchar(30)     NULL,
    dtfirstperiod        datetime        NULL,
    dtlastperiod         datetime        NULL,
    PRIMARY KEY CLUSTERED (idmarkettreatment)
)
go



/* 
 * TABLE: market_treatment_status 
 */

CREATE TABLE market_treatment_status(
    idmarkettreatmentstatus    DOM_IDENTITY    IDENTITY(1,1),
    idstatustype               DOM_INTEGER     NULL,
    idmarkettreatment          int             NULL,
    dtfrom                     datetime        NULL,
    dtthru                     datetime        NULL,
    PRIMARY KEY CLUSTERED (idmarkettreatmentstatus)
)
go



/* 
 * TABLE: master_numbering 
 */

CREATE TABLE master_numbering(
    idnumbering    DOM_UUID           NOT NULL,
    idtag          DOM_DESCRIPTION    NULL,
    idvalue        DOM_DESCRIPTION    NULL,
    nextvalue      DOM_INTEGER        NULL,
    valuenumber    DOM_MONEY          NULL,
    valuestring    DOM_LONGDESC       NULL,
    PRIMARY KEY NONCLUSTERED (idnumbering)
)
go



/* 
 * TABLE: mechanic 
 */

CREATE TABLE mechanic(
    idparrol            DOM_UUID           NOT NULL,
    idvendor            DOM_ID             NULL,
    idmechanic          DOM_ID             NULL,
    isexternal          DOM_BOOLEAN        NULL,
    passwordmechanic    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idparrol)
)
go



/* 
 * TABLE: memo 
 */

CREATE TABLE memo(
    idmemo          DOM_UUID        NOT NULL,
    idbilling       DOM_UUID        NULL,
    idmemotype      DOM_IDENTITY    NULL,
    idinvite        DOM_UUID        NULL,
    idoldinvite     DOM_UUID        NULL,
    idinternal      DOM_ID          NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    memonumber      DOM_ID          NULL,
    bbn             DOM_MONEY       NULL,
    discount        DOM_MONEY       NULL,
    unitprice       DOM_MONEY       NULL,
    currbillnumb    DOM_ID          NULL,
    reqidfeature    DOM_INTEGER     NULL,
    PRIMARY KEY CLUSTERED (idmemo)
)
go



/* 
 * TABLE: memo_status 
 */

CREATE TABLE memo_status(
    idstatus        DOM_UUID        NOT NULL,
    idmemo          DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: memo_type 
 */

CREATE TABLE memo_type(
    idmemotype     DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idmemotype)
)
go



/* 
 * TABLE: motor 
 */

CREATE TABLE motor(
    idproduct     DOM_ID             NOT NULL,
    marketname    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: motor_due_reminder 
 */

CREATE TABLE motor_due_reminder(
    idreminder    DOM_IDENTITY    IDENTITY(1,1),
    idmotor       DOM_ID          NULL,
    nservice1     DOM_INTEGER     NULL,
    nservice2     DOM_INTEGER     NULL,
    nservice3     DOM_INTEGER     NULL,
    nservice4     DOM_INTEGER     NULL,
    nservice5     DOM_INTEGER     NULL,
    km            DOM_INTEGER     NULL,
    ndays         DOM_INTEGER     NULL,
    PRIMARY KEY NONCLUSTERED (idreminder)
)
go



/* 
 * TABLE: moving_slip 
 */

CREATE TABLE moving_slip(
    idslip             DOM_UUID    NOT NULL,
    idinviteto         DOM_UUID    NULL,
    idcontainerfrom    DOM_UUID    NULL,
    idcontainerto      DOM_UUID    NULL,
    idproduct          DOM_ID      NULL,
    idfacilityto       DOM_UUID    NULL,
    idfacilityfrom     DOM_UUID    NULL,
    idinvite           DOM_UUID    NULL,
    idreqitem          DOM_UUID    NULL,
    qty                DOM_QTY     NULL,
    PRIMARY KEY NONCLUSTERED (idslip)
)
go



/* 
 * TABLE: operation_code 
 */

CREATE TABLE operation_code(
    idoperationcode    DOM_ID             NOT NULL,
    description        DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idoperationcode)
)
go



/* 
 * TABLE: order_billing_item 
 */

CREATE TABLE order_billing_item(
    idordbilite    DOM_UUID     NOT NULL,
    idordite       DOM_UUID     NULL,
    idbilite       DOM_UUID     NULL,
    amount         DOM_MONEY    NULL,
    qty            DOM_QTY      NULL,
    PRIMARY KEY CLUSTERED (idordbilite)
)
go



/* 
 * TABLE: order_demand 
 */

CREATE TABLE order_demand(
    idorderdemand    char(10)          NOT NULL,
    qty              decimal(18, 4)    NULL,
    idproduct        DOM_ID            NULL,
    idorder          DOM_UUID          NULL,
    dtorderdemand    datetime          NULL,
    PRIMARY KEY CLUSTERED (idorderdemand)
)
go



/* 
 * TABLE: order_item 
 */

CREATE TABLE order_item(
    idordite           DOM_UUID           NOT NULL,
    idparordite        DOM_UUID           NULL,
    idorder            DOM_UUID           NULL,
    idproduct          DOM_ID             NULL,
    idfeature          DOM_IDENTITY       NULL,
    idshipto           DOM_ID             NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    qty                DOM_QTY            NULL,
    unitprice          DOM_MONEY          NULL,
    discount           DOM_MONEY          NULL,
    discountmd         DOM_MONEY          NULL,
    discountfin        DOM_MONEY          NULL,
    bbn                DOM_MONEY          NULL,
    discountatpm       DOM_MONEY          NULL,
    idframe            DOM_ID             NULL,
    idmachine          DOM_ID             NULL,
    taxamount          DOM_MONEY          NULL,
    totalamount        DOM_MONEY          NULL,
    PRIMARY KEY NONCLUSTERED (idordite)
)
go



/* 
 * TABLE: order_payment 
 */

CREATE TABLE order_payment(
    idordpayite    DOM_UUID     NOT NULL,
    idorder        DOM_UUID     NULL,
    idpayment      DOM_UUID     NULL,
    amount         DOM_MONEY    NULL,
    PRIMARY KEY NONCLUSTERED (idordpayite)
)
go



/* 
 * TABLE: order_request_item 
 */

CREATE TABLE order_request_item(
    idordreqite     DOM_UUID    NOT NULL,
    idordite        DOM_UUID    NULL,
    idreqitem       DOM_UUID    NULL,
    qty             DOM_QTY     NULL,
    qtydelivered    DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idordreqite)
)
go



/* 
 * TABLE: order_request_status 
 */

CREATE TABLE order_request_status(
    idorrestatus    int            IDENTITY(1,1),
    idrequest       DOM_UUID       NULL,
    idstatustype    DOM_INTEGER    NULL,
    idreason        int            NULL,
    reason          varchar(50)    NULL,
    dtfrom          datetime       NULL,
    dtthru          datetime       NULL,
    PRIMARY KEY CLUSTERED (idorrestatus)
)
go



/* 
 * TABLE: order_shipment_item 
 */

CREATE TABLE order_shipment_item(
    idordshiite    DOM_UUID    NOT NULL,
    idordite       DOM_UUID    NULL,
    idshiite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idordshiite)
)
go



/* 
 * TABLE: order_type 
 */

CREATE TABLE order_type(
    idordtyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idordtyp)
)
go



/* 
 * TABLE: orders 
 */

CREATE TABLE orders(
    idorder        DOM_UUID           NOT NULL,
    idordtyp       DOM_INTEGER        NULL,
    ordernumber    DOM_DESCRIPTION    NULL,
    dtentry        DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtorder        DOM_DATE           NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: orders_role 
 */

CREATE TABLE orders_role(
    idrole        DOM_UUID        NOT NULL,
    idorder       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: orders_status 
 */

CREATE TABLE orders_status(
    idstatus        DOM_UUID           NOT NULL,
    idorder         DOM_UUID           NULL,
    idstatustype    DOM_INTEGER        NULL,
    idreason        DOM_INTEGER        NULL,
    idordite        DOM_UUID           NULL,
    description     DOM_DESCRIPTION    NULL,
    reason          DOM_LONGDESC       NULL,
    dtfrom          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: organization 
 */

CREATE TABLE organization(
    idparty         DOM_UUID           NOT NULL,
    idpic           DOM_UUID           NULL,
    idposaddtdp     DOM_UUID           NULL,
    name            DOM_NAME           NULL,
    numbertdp       DOM_ID             NULL,
    dtestablised    DOM_DATE           NULL,
    officephone     DOM_ID             NULL,
    officefax       DOM_ID             NULL,
    taxidnumber     DOM_ID             NULL,
    officemail      DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idparty)
)
go



/* 
 * TABLE: organization_customer 
 */

CREATE TABLE organization_customer(
    idcustomer    DOM_ID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idcustomer)
)
go



/* 
 * TABLE: organization_gl_account 
 */

CREATE TABLE organization_gl_account(
    idorgglacc    DOM_IDENTITY    IDENTITY(1,1),
    idparent      DOM_IDENTITY    NULL,
    idglacc       DOM_UUID        NULL,
    idinternal    DOM_ID          NULL,
    idvendor      DOM_ID          NULL,
    idbillto      DOM_ID          NULL,
    idproduct     DOM_ID          NULL,
    idcategory    DOM_IDENTITY    NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idorgglacc)
)
go



/* 
 * TABLE: organization_prospect 
 */

CREATE TABLE organization_prospect(
    idprospect     DOM_UUID    NOT NULL,
    idparty        DOM_UUID    NULL,
    idpic          DOM_UUID    NULL,
    idposaddtdp    DOM_UUID    NULL,
    PRIMARY KEY NONCLUSTERED (idprospect)
)
go



/* 
 * TABLE: organization_prospect_detail 
 */

CREATE TABLE organization_prospect_detail(
    idetail          DOM_UUID        NOT NULL,
    idprospect       DOM_UUID        NULL,
    idpersonowner    DOM_UUID        NULL,
    idproduct        DOM_ID          NULL,
    idfeature        DOM_IDENTITY    NULL,
    unitprice        DOM_MONEY       NULL,
    discount         DOM_MONEY       NULL,
    PRIMARY KEY CLUSTERED (idetail)
)
go



/* 
 * TABLE: organization_suspect 
 */

CREATE TABLE organization_suspect(
    idsuspect    DOM_UUID    NOT NULL,
    idparty      DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idsuspect)
)
go



/* 
 * TABLE: package_receipt 
 */

CREATE TABLE package_receipt(
    idpackage         DOM_UUID           NOT NULL,
    idshifro          DOM_ID             NULL,
    documentnumber    DOM_DESCRIPTION    NULL,
    vendorinvoice     DOM_ID             NULL,
    PRIMARY KEY CLUSTERED (idpackage)
)
go



/* 
 * TABLE: packaging_content 
 */

CREATE TABLE packaging_content(
    idpaccon     DOM_UUID    NOT NULL,
    idpackage    DOM_UUID    NULL,
    idshiite     DOM_UUID    NULL,
    qty          DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idpaccon)
)
go



/* 
 * TABLE: parent_organization 
 */

CREATE TABLE parent_organization(
    idinternal    DOM_ID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idinternal)
)
go



/* 
 * TABLE: part_history 
 */

CREATE TABLE part_history(
    idparthistory              int               IDENTITY(1,1),
    idservicehistory           int               NULL,
    idvehicleservicehistory    DOM_IDENTITY      NULL,
    nopart                     varchar(30)       NULL,
    descpart                   varchar(30)       NULL,
    qty                        decimal(18, 4)    NULL,
    PRIMARY KEY CLUSTERED (idparthistory)
)
go



/* 
 * TABLE: part_issue 
 */

CREATE TABLE part_issue(
    idissue            int            IDENTITY(1,1),
    idpartissuetype    DOM_INTEGER    NULL,
    filingnumber       varchar(20)    NULL,
    filingdate         date           NULL,
    PRIMARY KEY CLUSTERED (idissue)
)
go



/* 
 * TABLE: part_issue_action 
 */

CREATE TABLE part_issue_action(
    idpartissueact    DOM_UUID       NOT NULL,
    idissue           int            NULL,
    claimto           varchar(90)    NULL,
    nobast            varchar(20)    NULL,
    receivedby        varchar(90)    NULL,
    releasedby        varchar(90)    NULL,
    approvedby        varchar(90)    NULL,
    dtcreate          date           NULL,
    PRIMARY KEY CLUSTERED (idpartissueact)
)
go



/* 
 * TABLE: part_issue_item 
 */

CREATE TABLE part_issue_item(
    idissueitem     DOM_IDENTITY    IDENTITY(1,1),
    idissue         int             NULL,
    idproduct       DOM_ID          NULL,
    idstatustype    DOM_INTEGER     NULL,
    description     varchar(50)     NULL,
    qty             int             NULL,
    PRIMARY KEY CLUSTERED (idissueitem)
)
go



/* 
 * TABLE: part_issue_status 
 */

CREATE TABLE part_issue_status(
    idstatus        int            IDENTITY(1,1),
    idissue         int            NULL,
    idstatustype    DOM_INTEGER    NULL,
    dtfrom          date           NULL,
    dtthru          date           NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: part_issue_type 
 */

CREATE TABLE part_issue_type(
    idpartissuetype    DOM_INTEGER     NOT NULL,
    description        varchar(255)    NULL,
    PRIMARY KEY CLUSTERED (idpartissuetype)
)
go



/* 
 * TABLE: part_sales_order 
 */

CREATE TABLE part_sales_order(
    idorder    DOM_UUID     NOT NULL,
    qty        DOM_QTY      NULL,
    total      DOM_MONEY    NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: party 
 */

CREATE TABLE party(
    idparty    DOM_UUID       NOT NULL,
    idtype     DOM_INTEGER    NULL,
    PRIMARY KEY NONCLUSTERED (idparty)
)
go



/* 
 * TABLE: party_category 
 */

CREATE TABLE party_category(
    idcategory     DOM_IDENTITY       IDENTITY(1,1),
    idcattyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    PRIMARY KEY CLUSTERED (idcategory)
)
go



/* 
 * TABLE: party_category_impl 
 */

CREATE TABLE party_category_impl(
    idparty       DOM_UUID        NOT NULL,
    idcategory    DOM_IDENTITY    NOT NULL,
    PRIMARY KEY CLUSTERED (idparty, idcategory)
)
go



/* 
 * TABLE: party_category_type 
 */

CREATE TABLE party_category_type(
    idcattyp       DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    PRIMARY KEY CLUSTERED (idcattyp)
)
go



/* 
 * TABLE: party_classification 
 */

CREATE TABLE party_classification(
    idclassification    DOM_UUID        NOT NULL,
    idparty             DOM_UUID        NULL,
    idowner             DOM_UUID        NULL,
    idcategory          DOM_IDENTITY    NULL,
    idcattyp            DOM_IDENTITY    NULL,
    dtfrom              DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru              DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idclassification)
)
go



/* 
 * TABLE: party_contact 
 */

CREATE TABLE party_contact(
    idparty      DOM_UUID    NOT NULL,
    idcontact    DOM_UUID    NOT NULL,
    PRIMARY KEY CLUSTERED (idparty, idcontact)
)
go



/* 
 * TABLE: party_contact_mechanism 
 */

CREATE TABLE party_contact_mechanism(
    idparcon         DOM_IDENTITY    IDENTITY(1,1),
    idparty          DOM_UUID        NULL,
    idcontact        DOM_UUID        NULL,
    idpurposetype    DOM_INTEGER     DEFAULT 1 NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idparcon)
)
go



/* 
 * TABLE: party_document 
 */

CREATE TABLE party_document(
    iddocument     DOM_UUID    NOT NULL,
    idparty        DOM_UUID    NULL,
    contenttype    DOM_ID      NULL,
    content        DOM_BLOB    NULL,
    PRIMARY KEY CLUSTERED (iddocument)
)
go



/* 
 * TABLE: party_facility_purpose 
 */

CREATE TABLE party_facility_purpose(
    idparfacpur      DOM_UUID        NOT NULL,
    idparty          DOM_UUID        NULL,
    idpurposetype    DOM_INTEGER     NULL,
    idfacility       DOM_UUID        NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idparfacpur)
)
go



/* 
 * TABLE: party_relationship 
 */

CREATE TABLE party_relationship(
    idparrel        DOM_UUID        NOT NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreltyp        DOM_INTEGER     NULL,
    idparrolfro     DOM_UUID        NULL,
    idparrolto      DOM_UUID        NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idparrel)
)
go



/* 
 * TABLE: party_role 
 */

CREATE TABLE party_role(
    idparrol      DOM_UUID        NOT NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    dtregister    DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idparrol)
)
go



/* 
 * TABLE: party_role_status 
 */

CREATE TABLE party_role_status(
    idstatus        DOM_UUID        NOT NULL,
    idparrol        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: party_role_type 
 */

CREATE TABLE party_role_type(
    idparty       DOM_UUID       NOT NULL,
    idroletype    DOM_INTEGER    NOT NULL,
    PRIMARY KEY CLUSTERED (idparty, idroletype)
)
go



/* 
 * TABLE: payment 
 */

CREATE TABLE payment(
    idpayment        DOM_UUID           NOT NULL,
    idpaytyp         DOM_IDENTITY       NULL,
    idpaymet         DOM_IDENTITY       NULL,
    idinternal       DOM_ID             NULL,
    idpaito          DOM_ID             NULL,
    idpaifro         DOM_ID             NULL,
    paymentnumber    DOM_ID             NULL,
    dtcreate         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    amount           DOM_MONEY          NULL,
    reffnum          DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idpayment)
)
go



/* 
 * TABLE: payment_acctg_trans 
 */

CREATE TABLE payment_acctg_trans(
    idacctra     DOM_LONG    NOT NULL,
    idpayment    DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idacctra)
)
go



/* 
 * TABLE: payment_application 
 */

CREATE TABLE payment_application(
    idpayapp         DOM_UUID        NOT NULL,
    idpayment        DOM_UUID        NULL,
    idbilacc         DOM_IDENTITY    NULL,
    idbilling        DOM_UUID        NULL,
    amountapplied    DOM_MONEY       NULL,
    PRIMARY KEY NONCLUSTERED (idpayapp)
)
go



/* 
 * TABLE: payment_method 
 */

CREATE TABLE payment_method(
    idpaymet         DOM_IDENTITY       IDENTITY(1,1),
    idpaymettyp      DOM_IDENTITY       NULL,
    refkey           DOM_ID             NULL,
    description      DOM_DESCRIPTION    NULL,
    accountnumber    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idpaymet)
)
go



/* 
 * TABLE: payment_method_type 
 */

CREATE TABLE payment_method_type(
    idpaymettyp    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    PRIMARY KEY NONCLUSTERED (idpaymettyp)
)
go



/* 
 * TABLE: payment_role 
 */

CREATE TABLE payment_role(
    idrole        DOM_UUID        NOT NULL,
    idpayment     DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: payment_status 
 */

CREATE TABLE payment_status(
    idstatus        DOM_UUID        NOT NULL,
    idpayment       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: payment_type 
 */

CREATE TABLE payment_type(
    idpaytyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idpaytyp)
)
go



/* 
 * TABLE: pendingreason 
 */

CREATE TABLE pendingreason(
    idpendingreason      int             NOT NULL,
    descpendingreason    varchar(250)    NULL,
    lastmodifieddate     datetime        NULL,
    statuscode           int             NULL,
    PRIMARY KEY CLUSTERED (idpendingreason)
)
go



/* 
 * TABLE: person 
 */

CREATE TABLE person(
    idparty             DOM_UUID           NOT NULL,
    fname               DOM_NAME           NULL,
    lname               DOM_NAME           NULL,
    pob                 DOM_NAME           NULL,
    dtob                DOM_DATE           NULL,
    bloodtype           DOM_SHORTID        NULL,
    gender              DOM_FLAG           NULL,
    personalidnumber    DOM_ID             NULL,
    familyidnumber      DOM_ID             NULL,
    taxidnumber         DOM_ID             NULL,
    idreligiontype      DOM_INTEGER        NULL,
    idworktype          DOM_INTEGER        NULL,
    cellphone1          DOM_DESCRIPTION    NULL,
    cellphone2          DOM_DESCRIPTION    NULL,
    privatemail         DOM_DESCRIPTION    NULL,
    phone               DOM_DESCRIPTION    NULL,
    username            DOM_NAME           NULL,
    PRIMARY KEY NONCLUSTERED (idparty)
)
go



/* 
 * TABLE: person_prospect 
 */

CREATE TABLE person_prospect(
    idprospect    DOM_UUID    NOT NULL,
    idparty       DOM_UUID    NULL,
    PRIMARY KEY NONCLUSTERED (idprospect)
)
go



/* 
 * TABLE: person_suspect 
 */

CREATE TABLE person_suspect(
    idsuspect    DOM_UUID    NOT NULL,
    idparty      DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idsuspect)
)
go



/* 
 * TABLE: person_tm 
 */

CREATE TABLE person_tm(
    idperson              int             NOT NULL,
    name                  nvarchar(50)    NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    PRIMARY KEY CLUSTERED (idperson)
)
go



/* 
 * TABLE: personal_customer 
 */

CREATE TABLE personal_customer(
    idcustomer    DOM_ID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idcustomer)
)
go



/* 
 * TABLE: personsosmed 
 */

CREATE TABLE personsosmed(
    idsosmed        int            IDENTITY(1,1),
    idsosmedtype    int            NULL,
    idparty         DOM_UUID       NULL,
    username        varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idsosmed)
)
go



/* 
 * TABLE: picking_slip 
 */

CREATE TABLE picking_slip(
    idslip              DOM_UUID       NOT NULL,
    idinvite            DOM_UUID       NULL,
    idordite            DOM_UUID       NULL,
    idshipto            DOM_ID         NULL,
    qty                 DOM_QTY        NULL,
    acc1                DOM_BOOLEAN    NULL,
    acc2                DOM_BOOLEAN    NULL,
    acc3                DOM_BOOLEAN    NULL,
    acc4                DOM_BOOLEAN    NULL,
    acc5                DOM_BOOLEAN    NULL,
    acc6                DOM_BOOLEAN    NULL,
    acc7                DOM_BOOLEAN    NULL,
    acc8                DOM_BOOLEAN    NULL,
    acctambah1          DOM_BOOLEAN    NULL,
    acctambah2          DOM_BOOLEAN    NULL,
    promat1             DOM_BOOLEAN    NULL,
    promat2             DOM_BOOLEAN    NULL,
    promatdirectgift    DOM_BOOLEAN    NULL,
    idinternalmecha     DOM_UUID       NULL,
    idexternalmecha     DOM_UUID       NULL,
    PRIMARY KEY NONCLUSTERED (idslip)
)
go



/* 
 * TABLE: pkb 
 */

CREATE TABLE pkb(
    idpkb                  DOM_IDENTITY      IDENTITY(1,1),
    idinternal             DOM_ID            NULL,
    idstatuspkbinternal    DOM_INTEGER       NULL,
    idstatuspkbcust        DOM_INTEGER       NULL,
    idcustomer             DOM_ID            NULL,
    idqueue                DOM_IDENTITY      NULL,
    idvehicle              DOM_UUID          NULL,
    idpaymettyp            DOM_IDENTITY      NULL,
    idtypepkb              DOM_INTEGER       NULL,
    idvehide               DOM_UUID          NULL,
    idpkbjobreturn         DOM_IDENTITY      NULL,
    nopkb                  varchar(15)       NOT NULL,
    pkbdate                datetime          NULL,
    lamentation            varchar(50)       NULL,
    lastmodifieddate       date              NULL,
    lastmodifieduserid     int               NULL,
    statuscode             int               NULL,
    subtotalservice        decimal(18, 2)    NULL,
    subtotalpart           decimal(18, 2)    NULL,
    reason                 varchar(255)      NULL,
    kilometer              decimal(14, 2)    NULL,
    nextservicedate        datetime          NULL,
    jobsuggest             varchar(50)       NULL,
    PRIMARY KEY NONCLUSTERED (idpkb),
    UNIQUE (nopkb)
)
go



/* 
 * TABLE: pkb_estimation 
 */

CREATE TABLE pkb_estimation(
    idpkb                DOM_IDENTITY      NOT NULL,
    estimatedprice       decimal(18, 0)    NULL,
    depositcust          decimal(18, 0)    NULL,
    dp                   decimal(18, 0)    NULL,
    beawaited            bit               NULL,
    estimatedduration    int               NULL,
    hoursregist          int               NULL,
    estimatedstart       int               NULL,
    estimatedfinish      int               NULL,
    PRIMARY KEY CLUSTERED (idpkb)
)
go



/* 
 * TABLE: pkb_inspection 
 */

CREATE TABLE pkb_inspection(
    idinspection    int            IDENTITY(1,1),
    description     varchar(60)    NULL,
    dtcreate        datetime       NULL,
    PRIMARY KEY CLUSTERED (idinspection)
)
go



/* 
 * TABLE: pkb_part 
 */

CREATE TABLE pkb_part(
    IdPkbPart             DOM_INTEGER       NOT NULL,
    idpkbservice          DOM_INTEGER       NULL,
    idproduct             varchar(90)       NULL,
    idpkb                 DOM_IDENTITY      NULL,
    idchargeto            int               NULL,
    nopart                varchar(300)      NULL,
    qty                   decimal(18, 4)    NULL,
    het                   decimal(18, 4)    NULL,
    discount              decimal(18, 4)    NULL,
    unitprice             decimal(18, 4)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    descpart              varchar(270)      NULL,
    totalamount           decimal(18, 4)    NULL,
    isrequest             tinyint           NULL,
    isclaimhotline        tinyint           NULL,
    taxamount             decimal(18, 4)    NULL,
    idcategory            int               NULL,
    sequence              int               NULL,
    qtyrequest            decimal(18, 4)    NULL,
    idinvite              varchar(192)      NULL,
    isdefaultpart         tinyint           NULL,
    PRIMARY KEY CLUSTERED (IdPkbPart)
)
go



/* 
 * TABLE: pkb_part_billing_item 
 */

CREATE TABLE pkb_part_billing_item(
    idbilite     DOM_UUID    NOT NULL,
    idpkbpart    int         NULL,
    idparrol     DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idbilite)
)
go



/* 
 * TABLE: pkb_part_request_item 
 */

CREATE TABLE pkb_part_request_item(
    idreqitem    DOM_UUID    NOT NULL,
    idpkbpart    int         NULL,
    PRIMARY KEY CLUSTERED (idreqitem)
)
go



/* 
 * TABLE: pkb_service 
 */

CREATE TABLE pkb_service(
    IdPkbService             DOM_INTEGER       NOT NULL,
    idpkb                    DOM_IDENTITY      NULL,
    idproduct                DOM_ID            NULL,
    idchargeto               int               NULL,
    codeservice              varchar(90)       NULL,
    descservice              varchar(270)      NULL,
    unitprice                decimal(18, 4)    NULL,
    discount                 decimal(18, 4)    NULL,
    flatrate                 decimal(5, 2)     NULL,
    chargeto                 varchar(60)       NULL,
    issuggestedbymechanic    tinyint           NULL,
    lastmodifieddate         date              NULL,
    lastmodifieduserid       int               NULL,
    statuscode               int               NULL,
    totalamount              decimal(18, 4)    NULL,
    isrequest                tinyint           NULL,
    taxamount                decimal(18, 4)    NULL,
    idcategory               int               NULL,
    sequence                 int               NULL,
    standardrate             decimal(18, 4)    NULL,
    isjobreturn              tinyint           NULL,
    baseprice                decimal(18, 4)    NULL,
    PRIMARY KEY CLUSTERED (IdPkbService)
)
go



/* 
 * TABLE: pkb_service_billing_item 
 */

CREATE TABLE pkb_service_billing_item(
    idbilite        DOM_UUID    NOT NULL,
    idpkbservice    int         NULL,
    idparrol        DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idbilite)
)
go



/* 
 * TABLE: po_detail_part 
 */

CREATE TABLE po_detail_part(
    idordite              DOM_UUID    NOT NULL,
    idpkbpart             int         NULL,
    lastmodifieddate      date        NULL,
    lastmodefieduserid    int         NULL,
    statuscode            int         NULL,
    PRIMARY KEY NONCLUSTERED (idordite)
)
go



/* 
 * TABLE: po_detail_service 
 */

CREATE TABLE po_detail_service(
    idordite              DOM_UUID    NOT NULL,
    idpkbservice          int         NULL,
    lastmodifieddate      date        NULL,
    lastmodifieduserid    int         NULL,
    statuscode            int         NULL,
    PRIMARY KEY NONCLUSTERED (idordite)
)
go



/* 
 * TABLE: po_status 
 */

CREATE TABLE po_status(
    idpostatus      DOM_INTEGER    NOT NULL,
    idstatustype    DOM_INTEGER    NULL,
    description     varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idpostatus)
)
go



/* 
 * TABLE: position_authority 
 */

CREATE TABLE position_authority(
    idpostyp    DOM_INTEGER    NOT NULL,
    name        DOM_NAME       NOT NULL,
    PRIMARY KEY CLUSTERED (idpostyp, name)
)
go



/* 
 * TABLE: position_fullfillment 
 */

CREATE TABLE position_fullfillment(
    idposfulfil    DOM_IDENTITY    IDENTITY(1,1),
    idposition     DOM_UUID        NULL,
    idperson       DOM_UUID        NULL,
    username       DOM_NAME        NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idposfulfil)
)
go



/* 
 * TABLE: position_reporting_structure 
 */

CREATE TABLE position_reporting_structure(
    idposrepstru    DOM_IDENTITY    IDENTITY(1,1),
    idposfro        DOM_UUID        NULL,
    idposto         DOM_UUID        NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idposrepstru)
)
go



/* 
 * TABLE: position_type 
 */

CREATE TABLE position_type(
    idpostyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    title          DOM_NAME           NULL,
    PRIMARY KEY NONCLUSTERED (idpostyp)
)
go



/* 
 * TABLE: positions 
 */

CREATE TABLE positions(
    idposition        DOM_UUID           NOT NULL,
    idpostyp          DOM_INTEGER        NULL,
    idorganization    DOM_UUID           NULL,
    idusrmed          DOM_UUID           NULL,
    idinternal        DOM_ID             NULL,
    seqnum            DOM_INTEGER        NULL,
    username          DOM_NAME           NULL,
    description       DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idposition)
)
go



/* 
 * TABLE: positions_status 
 */

CREATE TABLE positions_status(
    idstatus        DOM_UUID        NOT NULL,
    idposition      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: postal_address 
 */

CREATE TABLE postal_address(
    idcontact     DOM_UUID       NOT NULL,
    address       DOM_ADDRESS    NULL,
    address1      DOM_ADDRESS    NULL,
    address2      DOM_ADDRESS    NULL,
    idprovince    DOM_UUID       NULL,
    idcity        DOM_UUID       NULL,
    iddistrict    DOM_UUID       NULL,
    idvillage     DOM_UUID       NULL,
    idpostal      DOM_UUID       NULL,
    PRIMARY KEY NONCLUSTERED (idcontact)
)
go



/* 
 * TABLE: price_agreement_item 
 */

CREATE TABLE price_agreement_item(
    idagrite     DOM_UUID    NOT NULL,
    idproduct    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idagrite)
)
go



/* 
 * TABLE: price_component 
 */

CREATE TABLE price_component(
    idpricom          DOM_UUID        NOT NULL,
    idparty           DOM_UUID        NULL,
    idproduct         DOM_ID          NULL,
    idpricetype       DOM_IDENTITY    NULL,
    idparcat          DOM_IDENTITY    NULL,
    idprocat          DOM_IDENTITY    NULL,
    idgeobou          DOM_UUID        NULL,
    idagrite          DOM_UUID        NULL,
    price             DOM_MONEY       NULL,
    manfucterprice    DOM_MONEY       NULL,
    percentvalue      float           NULL,
    standardrate      DOM_MONEY       NULL,
    dtfrom            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru            DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idpricom)
)
go



/* 
 * TABLE: price_type 
 */

CREATE TABLE price_type(
    idpricetype    DOM_IDENTITY       IDENTITY(1,1),
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idpricetype)
)
go



/* 
 * TABLE: product 
 */

CREATE TABLE product(
    idproduct         DOM_ID             NOT NULL,
    idprotyp          DOM_IDENTITY       NULL,
    name              DOM_DESCRIPTION    NULL,
    description       DOM_DESCRIPTION    NULL,
    dtintroduction    DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtdiscontinue     DOM_DATE           NULL,
    istaxable         DOM_BOOLEAN        NULL,
    pricetype         DOM_INTEGER        NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: product_association 
 */

CREATE TABLE product_association(
    idproass         DOM_UUID        NOT NULL,
    idasstyp         DOM_IDENTITY    NULL,
    idproductto      DOM_ID          NULL,
    idproductfrom    DOM_ID          NULL,
    qty              DOM_QTY         NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idproass)
)
go



/* 
 * TABLE: product_category 
 */

CREATE TABLE product_category(
    idcategory     DOM_IDENTITY       IDENTITY(1,1),
    idcattyp       DOM_IDENTITY       NULL,
    refkey         DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idcategory)
)
go



/* 
 * TABLE: product_category_impl 
 */

CREATE TABLE product_category_impl(
    idcategory    DOM_IDENTITY    NOT NULL,
    idproduct     DOM_ID          NOT NULL,
    PRIMARY KEY CLUSTERED (idcategory, idproduct)
)
go



/* 
 * TABLE: product_category_type 
 */

CREATE TABLE product_category_type(
    idcattyp       DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    PRIMARY KEY CLUSTERED (idcattyp)
)
go



/* 
 * TABLE: product_classification 
 */

CREATE TABLE product_classification(
    idclassification    DOM_UUID        NOT NULL,
    idproduct           DOM_ID          NULL,
    idcategory          DOM_IDENTITY    NULL,
    idcattyp            DOM_IDENTITY    NULL,
    idowner             DOM_UUID        NULL,
    dtfrom              DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru              DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idclassification)
)
go



/* 
 * TABLE: product_document 
 */

CREATE TABLE product_document(
    iddocument     DOM_UUID    NOT NULL,
    idproduct      DOM_ID      NULL,
    contenttype    DOM_ID      NULL,
    content        DOM_BLOB    NULL,
    PRIMARY KEY CLUSTERED (iddocument)
)
go



/* 
 * TABLE: product_feature_impl 
 */

CREATE TABLE product_feature_impl(
    features_id    DOM_IDENTITY    NOT NULL,
    products_id    DOM_ID          NOT NULL,
    PRIMARY KEY CLUSTERED (features_id, products_id)
)
go



/* 
 * TABLE: product_package_receipt 
 */

CREATE TABLE product_package_receipt(
    idpackage         DOM_UUID           NOT NULL,
    idshifro          DOM_ID             NULL,
    documentnumber    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idpackage)
)
go



/* 
 * TABLE: product_purchase_order 
 */

CREATE TABLE product_purchase_order(
    idorder    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: product_shipment_incoming 
 */

CREATE TABLE product_shipment_incoming(
    idshipment    DOM_UUID    NOT NULL,
    PRIMARY KEY CLUSTERED (idshipment)
)
go



/* 
 * TABLE: product_shipment_receipt 
 */

CREATE TABLE product_shipment_receipt(
    idreceipt    DOM_UUID    NOT NULL,
    PRIMARY KEY CLUSTERED (idreceipt)
)
go



/* 
 * TABLE: product_type 
 */

CREATE TABLE product_type(
    idprotyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idprotyp)
)
go



/* 
 * TABLE: prospect 
 */

CREATE TABLE prospect(
    idprospect            DOM_UUID        NOT NULL,
    idbroker              DOM_UUID        NULL,
    idprosou              DOM_IDENTITY    NULL,
    idevetyp              DOM_IDENTITY    NULL,
    idfacility            DOM_UUID        NULL,
    idsalesman            DOM_UUID        NULL,
    idsuspect             DOM_UUID        NULL,
    idsalescoordinator    DOM_UUID        NULL,
    idinternal            DOM_ID          NULL,
    prospectnumber        DOM_ID          NULL,
    prospectcount         DOM_INTEGER     NULL,
    eveloc                varchar(600)    NULL,
    dtfollowup            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY NONCLUSTERED (idprospect)
)
go



/* 
 * TABLE: prospect_role 
 */

CREATE TABLE prospect_role(
    idrole        DOM_UUID        NOT NULL,
    idprospect    DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: prospect_source 
 */

CREATE TABLE prospect_source(
    idprosou       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idprosou)
)
go



/* 
 * TABLE: prospect_status 
 */

CREATE TABLE prospect_status(
    idstatus        DOM_UUID        NOT NULL,
    idprospect      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: province 
 */

CREATE TABLE province(
    idgeobou    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



/* 
 * TABLE: purchase_order 
 */

CREATE TABLE purchase_order(
    idorder    DOM_UUID        NOT NULL,
    reason     DOM_LONGDESC    NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: purchase_request 
 */

CREATE TABLE purchase_request(
    idreq         DOM_UUID    NOT NULL,
    idproduct     DOM_ID      NULL,
    idinternal    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idreq)
)
go



/* 
 * TABLE: purchaseorder_reception_tr 
 */

CREATE TABLE purchaseorder_reception_tr(
    idpurchaseorderreception    int               NOT NULL,
    idpurchaseorder             int               NULL,
    idcategoryasset             int               NULL,
    quantitypurchase            int               NULL,
    quantityaccepted            int               NULL,
    lastmodifieddate            datetime          NULL,
    lastmodifieduserid          int               NULL,
    itemname                    nvarchar(30)      NULL,
    price                       decimal(18, 2)    NULL,
    branchcode                  nvarchar(30)      NULL,
    typecode                    nvarchar(30)      NULL,
    merk                        nvarchar(30)      NULL,
    type                        nvarchar(30)      NULL,
    PRIMARY KEY CLUSTERED (idpurchaseorderreception)
)
go



/* 
 * TABLE: purchaseorder_status_tp 
 */

CREATE TABLE purchaseorder_status_tp(
    idpurchaseorderstatus    int             NOT NULL,
    description              nvarchar(50)    NULL,
    lastmodifieddate         datetime        NULL,
    lastmodifieduserid       int             NULL,
    PRIMARY KEY CLUSTERED (idpurchaseorderstatus)
)
go



/* 
 * TABLE: purchaseorder_tm 
 */

CREATE TABLE purchaseorder_tm(
    idpurchaseorder       int              IDENTITY(1,1),
    ponumber              nvarchar(15)     NULL,
    podate                datetime         NULL,
    spgdate               datetime         NULL,
    spgnumber             nvarchar(20)     NULL,
    vendorname            nvarchar(500)    NULL,
    status                int              NULL,
    lastmodifieddate      datetime         NULL,
    lastmodifieduserid    int              NULL,
    PRIMARY KEY CLUSTERED (idpurchaseorder)
)
go



/* 
 * TABLE: purpose_type 
 */

CREATE TABLE purpose_type(
    idpurposetype    DOM_INTEGER        NOT NULL,
    description      DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idpurposetype)
)
go



/* 
 * TABLE: queue 
 */

CREATE TABLE queue(
    idqueue               DOM_IDENTITY    IDENTITY(1,1),
    idqueuestatus         DOM_INTEGER     NULL,
    idqueuetype           DOM_INTEGER     NULL,
    idbooking             DOM_IDENTITY    NULL,
    noqueue               varchar(10)     NOT NULL,
    customername          varchar(100)    NULL,
    vehiclenumber         varchar(20)     NULL,
    datequeue             datetime        NULL,
    waitingduration       int             NULL,
    ismissed              bit             NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY NONCLUSTERED (idqueue),
    UNIQUE (noqueue)
)
go



/* 
 * TABLE: queue_status 
 */

CREATE TABLE queue_status(
    idqueuestatus    DOM_INTEGER    NOT NULL,
    description      varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idqueuestatus)
)
go



/* 
 * TABLE: queue_type 
 */

CREATE TABLE queue_type(
    idqueuetype    DOM_INTEGER    NOT NULL,
    type           varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idqueuetype)
)
go



/* 
 * TABLE: quote 
 */

CREATE TABLE quote(
    idquote        DOM_UUID           NOT NULL,
    dtissued       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idquote)
)
go



/* 
 * TABLE: quote_item 
 */

CREATE TABLE quote_item(
    idquoteitem    DOM_UUID     NOT NULL,
    idquote        DOM_UUID     NULL,
    idproduct      DOM_ID       NULL,
    unitprice      DOM_MONEY    NULL,
    qty            DOM_QTY      NULL,
    PRIMARY KEY CLUSTERED (idquoteitem)
)
go



/* 
 * TABLE: quote_role 
 */

CREATE TABLE quote_role(
    idrole        DOM_UUID        NOT NULL,
    idquote       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrole)
)
go



/* 
 * TABLE: quote_status 
 */

CREATE TABLE quote_status(
    idstatus        DOM_UUID        NOT NULL,
    idquote         DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: reason_type 
 */

CREATE TABLE reason_type(
    idreason       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idreason)
)
go



/* 
 * TABLE: receipt 
 */

CREATE TABLE receipt(
    idpayment    DOM_UUID    NOT NULL,
    idreq        DOM_UUID    NULL,
    PRIMARY KEY NONCLUSTERED (idpayment)
)
go



/* 
 * TABLE: regular_sales_order 
 */

CREATE TABLE regular_sales_order(
    idorder    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: reimbursement_part 
 */

CREATE TABLE reimbursement_part(
    idspgclaimaction    int               NULL,
    idproduct           DOM_ID            NULL,
    idspgclaimdetail    DOM_IDENTITY      NOT NULL,
    qty                 decimal(14, 2)    NULL,
    dtreimpart          datetime          NULL,
    PRIMARY KEY CLUSTERED (idspgclaimdetail)
)
go



/* 
 * TABLE: relation_type 
 */

CREATE TABLE relation_type(
    idreltyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idreltyp)
)
go



/* 
 * TABLE: religion_type 
 */

CREATE TABLE religion_type(
    idreligiontype    DOM_INTEGER        NOT NULL,
    description       DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idreligiontype)
)
go



/* 
 * TABLE: rem_part 
 */

CREATE TABLE rem_part(
    idproduct       DOM_ID     NOT NULL,
    minimumstock    DOM_QTY    NULL,
    qtysuggest      DOM_QTY    NULL,
    safetystock     DOM_QTY    NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: reminder_sent_status 
 */

CREATE TABLE reminder_sent_status(
    idsensta        DOM_UUID        NOT NULL,
    idreminder      DOM_IDENTITY    NULL,
    cellphone       DOM_ID          NULL,
    currentstate    DOM_INTEGER     NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY CLUSTERED (idsensta)
)
go



/* 
 * TABLE: request 
 */

CREATE TABLE request(
    idrequest      DOM_UUID           NOT NULL,
    idreqtype      DOM_IDENTITY       NULL,
    idinternal     DOM_ID             NULL,
    reqnum         DOM_ID             NULL,
    dtcreate       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtrequest      DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idrequest)
)
go



/* 
 * TABLE: request_item 
 */

CREATE TABLE request_item(
    idreqitem      DOM_UUID           NOT NULL,
    idrequest      DOM_UUID           NULL,
    idproduct      DOM_ID             NULL,
    idfeature      DOM_IDENTITY       NULL,
    seq            DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    qtyreq         DOM_QTY            NULL,
    qtytransfer    DOM_QTY            NULL,
    qtyorder       DOM_QTY            NULL,
    PRIMARY KEY CLUSTERED (idreqitem)
)
go



/* 
 * TABLE: request_product 
 */

CREATE TABLE request_product(
    idrequest         DOM_UUID    NOT NULL,
    idfacilityfrom    DOM_UUID    NULL,
    idfacilityto      DOM_UUID    NULL,
    PRIMARY KEY CLUSTERED (idrequest)
)
go



/* 
 * TABLE: request_requirement 
 */

CREATE TABLE request_requirement(
    idrequest        DOM_UUID    NOT NULL,
    idrequirement    DOM_UUID    NULL,
    qty              DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idrequest)
)
go



/* 
 * TABLE: request_role 
 */

CREATE TABLE request_role(
    idrole        DOM_UUID           NOT NULL,
    idrequest     DOM_UUID           NULL,
    idroletype    DOM_INTEGER        NULL,
    idparty       DOM_UUID           NULL,
    username      DOM_DESCRIPTION    NULL,
    dtfrom        DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrole)
)
go



/* 
 * TABLE: request_status 
 */

CREATE TABLE request_status(
    idstatus        DOM_UUID           NOT NULL,
    idrequest       DOM_UUID           NULL,
    idreason        DOM_INTEGER        NULL,
    idstatustype    DOM_INTEGER        NULL,
    reason          DOM_DESCRIPTION    NULL,
    dtfrom          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: request_type 
 */

CREATE TABLE request_type(
    idreqtype      DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idreqtype)
)
go



/* 
 * TABLE: request_unit_internal 
 */

CREATE TABLE request_unit_internal(
    idrequest       DOM_UUID    NOT NULL,
    idparent        DOM_UUID    NULL,
    idinternalto    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idrequest)
)
go



/* 
 * TABLE: requirement 
 */

CREATE TABLE requirement(
    idreq                DOM_UUID           NOT NULL,
    idfacility           DOM_UUID           NULL,
    idparentreq          DOM_UUID           NULL,
    idreqtyp             DOM_IDENTITY       NULL,
    requirementnumber    DOM_ID             NULL,
    description          DOM_DESCRIPTION    NULL,
    dtcreate             DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtrequired           DOM_DATE           NULL,
    budget               DOM_MONEY          NULL,
    qty                  DOM_QTY            NULL,
    PRIMARY KEY NONCLUSTERED (idreq)
)
go



/* 
 * TABLE: requirement_order_item 
 */

CREATE TABLE requirement_order_item(
    idreqordite    DOM_UUID    NOT NULL,
    idreq          DOM_UUID    NULL,
    idordite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idreqordite)
)
go



/* 
 * TABLE: requirement_payment 
 */

CREATE TABLE requirement_payment(
    idreqpayite    DOM_UUID     NOT NULL,
    idreq          DOM_UUID     NULL,
    idpayment      DOM_UUID     NULL,
    amount         DOM_MONEY    NULL,
    PRIMARY KEY NONCLUSTERED (idreqpayite)
)
go



/* 
 * TABLE: requirement_role 
 */

CREATE TABLE requirement_role(
    idrole        DOM_UUID        NOT NULL,
    idreq         DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idparty       DOM_UUID        NOT NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: requirement_status 
 */

CREATE TABLE requirement_status(
    idstatus        DOM_UUID        NOT NULL,
    idreq           DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: requirement_type 
 */

CREATE TABLE requirement_type(
    idreqtyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idreqtyp)
)
go



/* 
 * TABLE: return_purchase_order 
 */

CREATE TABLE return_purchase_order(
    idorder    DOM_UUID       NOT NULL,
    idissue    DOM_INTEGER    NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: return_sales_order 
 */

CREATE TABLE return_sales_order(
    idorder    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: role_type 
 */

CREATE TABLE role_type(
    idroletype     DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idroletype)
)
go



/* 
 * TABLE: room_tm 
 */

CREATE TABLE room_tm(
    idroom                int             IDENTITY(1,1),
    idfloor               int             NULL,
    roomname              nvarchar(50)    NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY CLUSTERED (idroom)
)
go



/* 
 * TABLE: rule_hot_item 
 */

CREATE TABLE rule_hot_item(
    idrule            DOM_IDENTITY    NOT NULL,
    idproduct         DOM_ID          NULL,
    mindownpayment    DOM_MONEY       NULL,
    PRIMARY KEY CLUSTERED (idrule)
)
go



/* 
 * TABLE: rule_indent 
 */

CREATE TABLE rule_indent(
    idrule        DOM_IDENTITY    NOT NULL,
    idproduct     DOM_ID          NULL,
    minpayment    DOM_MONEY       NULL,
    PRIMARY KEY CLUSTERED (idrule)
)
go



/* 
 * TABLE: rule_sales_discount 
 */

CREATE TABLE rule_sales_discount(
    idrule       DOM_IDENTITY    NOT NULL,
    maxamount    DOM_MONEY       NULL,
    name         DOM_NAME        NULL,
    PRIMARY KEY CLUSTERED (idrule)
)
go



/* 
 * TABLE: rule_type 
 */

CREATE TABLE rule_type(
    idrultyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idrultyp)
)
go



/* 
 * TABLE: rules 
 */

CREATE TABLE rules(
    idrule         DOM_IDENTITY       IDENTITY(1,1),
    idrultyp       DOM_IDENTITY       NULL,
    idinternal     DOM_ID             NULL,
    seqnum         DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrule)
)
go



/* 
 * TABLE: sale_type 
 */

CREATE TABLE sale_type(
    idsaletype     DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idsaletype)
)
go



/* 
 * TABLE: sales_agreement 
 */

CREATE TABLE sales_agreement(
    idagreement    DOM_UUID    NOT NULL,
    idinternal     DOM_ID      NULL,
    idcustomer     DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idagreement)
)
go



/* 
 * TABLE: sales_booking 
 */

CREATE TABLE sales_booking(
    idslsboo        DOM_UUID        NOT NULL,
    idinternal      DOM_ID          NULL,
    idreq           DOM_UUID        NULL,
    idproduct       DOM_ID          NULL,
    idfeature       DOM_IDENTITY    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    note            varchar(600)    NULL,
    yearassembly    DOM_INTEGER     NULL,
    onhand          DOM_BOOLEAN     NULL,
    intransit       DOM_BOOLEAN     NULL,
    PRIMARY KEY CLUSTERED (idslsboo)
)
go



/* 
 * TABLE: sales_broker 
 */

CREATE TABLE sales_broker(
    idparrol    DOM_UUID        NOT NULL,
    idbrotyp    DOM_IDENTITY    NULL,
    idbroker    DOM_ID          NULL,
    PRIMARY KEY NONCLUSTERED (idparrol)
)
go



/* 
 * TABLE: sales_order 
 */

CREATE TABLE sales_order(
    idorder       DOM_UUID        NOT NULL,
    idsaletype    DOM_IDENTITY    NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: sales_point 
 */

CREATE TABLE sales_point(
    idinternal    DOM_ID    NOT NULL,
    PRIMARY KEY CLUSTERED (idinternal)
)
go



/* 
 * TABLE: sales_unit_leasing 
 */

CREATE TABLE sales_unit_leasing(
    idsallea        DOM_UUID        NOT NULL,
    idreq           DOM_UUID        NULL,
    idleacom        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idorder         DOM_UUID        NULL,
    idlsgpro        DOM_UUID        NULL,
    ponumber        DOM_ID          NULL,
    dtpo            DOM_DATE        NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idsallea)
)
go



/* 
 * TABLE: sales_unit_requirement 
 */

CREATE TABLE sales_unit_requirement(
    idreq                       DOM_UUID        NOT NULL,
    idprospect                  DOM_UUID        NULL,
    idsaletype                  DOM_IDENTITY    NULL,
    idsalesman                  DOM_UUID        NULL,
    idproduct                   DOM_ID          NULL,
    idcolor                     DOM_IDENTITY    NULL,
    idsalesbroker               DOM_UUID        NULL,
    idowner                     DOM_UUID        NULL,
    idlsgpro                    DOM_UUID        NULL,
    idinternal                  DOM_ID          NULL,
    idbillto                    DOM_ID          NULL,
    idcustomer                  DOM_ID          NULL,
    idrequest                   DOM_UUID        NULL,
    downpayment                 DOM_MONEY       NULL,
    idframe                     DOM_ID          NULL,
    idmachine                   DOM_ID          NULL,
    subsfincomp                 DOM_MONEY       NULL,
    subsmd                      DOM_MONEY       NULL,
    subsown                     DOM_MONEY       NULL,
    subsahm                     DOM_MONEY       NULL,
    bbnprice                    DOM_MONEY       NULL,
    hetprice                    DOM_MONEY       NULL,
    note                        DOM_LONGDESC    NULL,
    unitprice                   DOM_MONEY       NULL,
    notepartner                 DOM_LONGDESC    NULL,
    shipmentnote                DOM_LONGDESC    NULL,
    requestpoliceid             DOM_ID          NULL,
    leasingpo                   DOM_ID          NULL,
    dtpoleasing                 DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    brokerfee                   DOM_MONEY       NULL,
    idprosou                    DOM_INTEGER     NULL,
    prodyear                    DOM_INTEGER     NULL,
    requestidmachine            DOM_ID          NULL,
    requestidframe              DOM_ID          NULL,
    requestidmachineandframe    DOM_ID          NULL,
    minpayment                  DOM_MONEY       NULL,
    isunithotitem               DOM_BOOLEAN     NULL,
    isunitindent                DOM_BOOLEAN     NULL,
    waitstnk                    DOM_BOOLEAN     NULL,
    creditinstallment           DOM_MONEY       NULL,
    creditdownpayment           DOM_MONEY       NULL,
    credittenor                 DOM_INTEGER     NULL,
    idleasing                   DOM_ID          NULL,
    podate                      DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    ponumber                    DOM_FLAG        NULL,
    PRIMARY KEY CLUSTERED (idreq)
)
go



/* 
 * TABLE: salesman 
 */

CREATE TABLE salesman(
    idparrol         DOM_UUID       NOT NULL,
    idcoordinator    DOM_UUID       NULL,
    idsalesman       DOM_ID         NULL,
    coordinator      DOM_BOOLEAN    NULL,
    idteamleader     DOM_UUID       NULL,
    teamleader       DOM_BOOLEAN    NULL,
    ahmcode          DOM_ID         NULL,
    PRIMARY KEY NONCLUSTERED (idparrol)
)
go



/* 
 * TABLE: service 
 */

CREATE TABLE service(
    idproduct            DOM_ID         NOT NULL,
    iduom                DOM_ID         NULL,
    frt                  DOM_INTEGER    NULL,
    idchargetopart       DOM_INTEGER    NULL,
    idchargetoservice    DOM_INTEGER    NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: service_agreement 
 */

CREATE TABLE service_agreement(
    idagreement    DOM_UUID    NOT NULL,
    idinternal     DOM_ID      NULL,
    idcustomer     DOM_ID      NULL,
    PRIMARY KEY NONCLUSTERED (idagreement)
)
go



/* 
 * TABLE: service_history 
 */

CREATE TABLE service_history(
    idservicehistory           int             IDENTITY(1,1),
    idvehicleservicehistory    DOM_IDENTITY    NULL,
    codeservice                varchar(30)     NULL,
    descservice                varchar(30)     NULL,
    PRIMARY KEY CLUSTERED (idservicehistory)
)
go



/* 
 * TABLE: service_kpb 
 */

CREATE TABLE service_kpb(
    idproduct            DOM_ID            NOT NULL,
    km                   decimal(12, 2)    NULL,
    expireddate          datetime          NULL,
    range_               decimal(12, 2)    NULL,
    daylimit             numeric(18, 0)    NULL,
    tolerancekm          decimal(12, 2)    NULL,
    tolerancedaylimit    numeric(18, 0)    NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: service_per_motor 
 */

CREATE TABLE service_per_motor(
    idproduct    DOM_ID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idproduct)
)
go



/* 
 * TABLE: service_reminder 
 */

CREATE TABLE service_reminder(
    idreminder        DOM_IDENTITY    IDENTITY(1,1),
    idclaimtype       DOM_IDENTITY    NULL,
    idremindertype    DOM_INTEGER     NULL,
    idvehide          DOM_UUID        NULL,
    days              DOM_INTEGER     NULL,
    PRIMARY KEY NONCLUSTERED (idreminder)
)
go



/* 
 * TABLE: service_reminder_event 
 */

CREATE TABLE service_reminder_event(
    idcomevt       DOM_UUID           NOT NULL,
    idreminder     DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idcomevt)
)
go



/* 
 * TABLE: ship_to 
 */

CREATE TABLE ship_to(
    idshipto      DOM_ID         NOT NULL,
    idparty       DOM_UUID       NULL,
    idroletype    DOM_INTEGER    NULL,
    PRIMARY KEY NONCLUSTERED (idshipto)
)
go



/* 
 * TABLE: shipment 
 */

CREATE TABLE shipment(
    idshipment        DOM_UUID           NOT NULL,
    idinternal        DOM_ID             NULL,
    idshityp          DOM_IDENTITY       NULL,
    idfacility        DOM_UUID           NULL,
    idposaddfro       DOM_UUID           NULL,
    idposaddto        DOM_UUID           NULL,
    idshito           DOM_ID             NULL,
    idshifro          DOM_ID             NULL,
    shipmentnumber    DOM_ID             NULL,
    description       DOM_DESCRIPTION    NULL,
    dtschedulle       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    reason            DOM_LONGDESC       NULL,
    PRIMARY KEY NONCLUSTERED (idshipment)
)
go



/* 
 * TABLE: shipment_billing_item 
 */

CREATE TABLE shipment_billing_item(
    idshibilite    DOM_UUID    NOT NULL,
    idshiite       DOM_UUID    NULL,
    idbilite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idshibilite)
)
go



/* 
 * TABLE: shipment_incoming 
 */

CREATE TABLE shipment_incoming(
    idshipment    DOM_UUID    NOT NULL,
    PRIMARY KEY CLUSTERED (idshipment)
)
go



/* 
 * TABLE: shipment_item 
 */

CREATE TABLE shipment_item(
    idshiite              DOM_UUID           NOT NULL,
    idshipment            DOM_UUID           NULL,
    idfeature             DOM_IDENTITY       NULL,
    idproduct             DOM_ID             NULL,
    itemdescription       DOM_DESCRIPTION    NULL,
    qty                   DOM_QTY            NULL,
    contentdescription    DOM_DESCRIPTION    NULL,
    idframe               DOM_DESCRIPTION    NULL,
    idmachine             DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idshiite)
)
go



/* 
 * TABLE: shipment_outgoing 
 */

CREATE TABLE shipment_outgoing(
    idshipment         DOM_UUID        NOT NULL,
    iddriver           DOM_UUID        NULL,
    dtbast             DOM_DATE        NULL,
    note               varchar(600)    NULL,
    othername          DOM_NAME        NULL,
    deliveryopt        DOM_LONGDESC    NULL,
    deliveryaddress    DOM_LONGDESC    NULL,
    printcount         DOM_INTEGER     NULL,
    PRIMARY KEY CLUSTERED (idshipment)
)
go



/* 
 * TABLE: shipment_package 
 */

CREATE TABLE shipment_package(
    idpackage      DOM_UUID        NOT NULL,
    idshipactyp    DOM_INTEGER     NULL,
    idinternal     DOM_ID          NULL,
    dtcreated      DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY NONCLUSTERED (idpackage)
)
go



/* 
 * TABLE: shipment_package_role 
 */

CREATE TABLE shipment_package_role(
    idrole        DOM_UUID        NOT NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    idpackage     DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrole)
)
go



/* 
 * TABLE: shipment_package_status 
 */

CREATE TABLE shipment_package_status(
    idstatus        DOM_UUID        NOT NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    idpackage       DOM_UUID        NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: shipment_package_type 
 */

CREATE TABLE shipment_package_type(
    idshipactyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idshipactyp)
)
go



/* 
 * TABLE: shipment_receipt 
 */

CREATE TABLE shipment_receipt(
    idreceipt          DOM_UUID           NOT NULL,
    idproduct          DOM_ID             NULL,
    idpackage          DOM_UUID           NULL,
    idfeature          DOM_IDENTITY       NULL,
    idordite           DOM_UUID           NULL,
    idshiite           DOM_UUID           NULL,
    code               DOM_ID             NULL,
    qtyaccept          DOM_QTY            NULL,
    qtyreject          DOM_QTY            NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    dtreceipt          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY NONCLUSTERED (idreceipt)
)
go



/* 
 * TABLE: shipment_receipt_role 
 */

CREATE TABLE shipment_receipt_role(
    idrole        DOM_UUID        NOT NULL,
    idreceipt     DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrole)
)
go



/* 
 * TABLE: shipment_receipt_status 
 */

CREATE TABLE shipment_receipt_status(
    idstatus        DOM_UUID        NOT NULL,
    idreceipt       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: shipment_status 
 */

CREATE TABLE shipment_status(
    idstatus        DOM_UUID        NOT NULL,
    idshipment      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: shipment_type 
 */

CREATE TABLE shipment_type(
    idshityp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idshityp)
)
go



/* 
 * TABLE: sosmedtype 
 */

CREATE TABLE sosmedtype(
    idsosmedtype    int            NOT NULL,
    description     varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idsosmedtype)
)
go



/* 
 * TABLE: spg_claim 
 */

CREATE TABLE spg_claim(
    idspgclaim    int     IDENTITY(1,1),
    dtclaim       date    NULL,
    PRIMARY KEY CLUSTERED (idspgclaim)
)
go



/* 
 * TABLE: spg_claim_action 
 */

CREATE TABLE spg_claim_action(
    idspgclaimaction    int            IDENTITY(1,1),
    claimto             varchar(90)    NULL,
    receivedby          varchar(90)    NULL,
    releasedby          varchar(90)    NULL,
    approvedby          varchar(90)    NULL,
    bastnumber          varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idspgclaimaction)
)
go



/* 
 * TABLE: spg_claim_detail 
 */

CREATE TABLE spg_claim_detail(
    idspgclaimdetail        DOM_IDENTITY      IDENTITY(1,1),
    idspgclaim              int               NULL,
    idproduct               DOM_ID            NULL,
    idreceipt               DOM_UUID          NULL,
    idspgclaimtype          int               NULL,
    idstatustype            DOM_INTEGER       NULL,
    idbilite                DOM_UUID          NULL,
    qty                     decimal(14, 2)    NULL,
    qtyreject               decimal(14, 2)    NULL,
    isnonactiveqty          bit               NULL,
    description             varchar(150)      NULL,
    isnonactiveqtyreject    bit               NULL,
    dtnonactivepart         datetime          NULL,
    PRIMARY KEY CLUSTERED (idspgclaimdetail)
)
go



/* 
 * TABLE: spg_claim_type 
 */

CREATE TABLE spg_claim_type(
    idspgclaimtype    int            IDENTITY(1,1),
    description       varchar(50)    NULL,
    PRIMARY KEY CLUSTERED (idspgclaimtype)
)
go



/* 
 * TABLE: standard_calendar 
 */

CREATE TABLE standard_calendar(
    idcalendar    DOM_IDENTITY    NOT NULL,
    idinternal    DOM_ID          NULL,
    PRIMARY KEY NONCLUSTERED (idcalendar)
)
go



/* 
 * TABLE: status_pkb 
 */

CREATE TABLE status_pkb(
    idstatuspkb           DOM_INTEGER    NOT NULL,
    idstatustype          DOM_INTEGER    NULL,
    descstatuspkb         varchar(30)    NULL,
    lastmodifieddate      date           NULL,
    lastmodifieduserid    int            NULL,
    statuscode            int            NULL,
    PRIMARY KEY NONCLUSTERED (idstatuspkb)
)
go



/* 
 * TABLE: status_type 
 */

CREATE TABLE status_type(
    idstatustype    DOM_INTEGER        NOT NULL,
    description     DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idstatustype)
)
go



/* 
 * TABLE: stock_opname 
 */

CREATE TABLE stock_opname(
    idstkop              DOM_UUID        NOT NULL,
    idstkopntyp          DOM_IDENTITY    NULL,
    idinternal           DOM_ID          NULL,
    idfacility           DOM_UUID        NULL,
    idcalendar           DOM_IDENTITY    NULL,
    dtcreated            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    stockopnamenumber    DOM_ID          NULL,
    PRIMARY KEY CLUSTERED (idstkop)
)
go



/* 
 * TABLE: stock_opname_inventory 
 */

CREATE TABLE stock_opname_inventory(
    idstkopninv    DOM_UUID    NOT NULL,
    idinvite       DOM_UUID    NULL,
    idstopnite     DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idstkopninv)
)
go



/* 
 * TABLE: stock_opname_item 
 */

CREATE TABLE stock_opname_item(
    idstopnite         DOM_UUID       NOT NULL,
    idstkop            DOM_UUID       NULL,
    idproduct          DOM_ID         NULL,
    idcontainer        DOM_UUID       NULL,
    itemdescription    DOM_NAME       NULL,
    qty                DOM_QTY        NULL,
    qtycount           DOM_QTY        NULL,
    tagnumber          DOM_ID         NULL,
    checked            DOM_BOOLEAN    NULL,
    het                DOM_MONEY      NULL,
    PRIMARY KEY CLUSTERED (idstopnite)
)
go



/* 
 * TABLE: stock_opname_status 
 */

CREATE TABLE stock_opname_status(
    idstatus        DOM_UUID        NOT NULL,
    idstkop         DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: stock_opname_type 
 */

CREATE TABLE stock_opname_type(
    idstkopntyp    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idstkopntyp)
)
go



/* 
 * TABLE: stockopname_item_tm 
 */

CREATE TABLE stockopname_item_tm(
    idstockopnameitem     int         IDENTITY(1,1),
    idstockopname         int         NULL,
    idfixedasset          int         NULL,
    idroom                int         NULL,
    idfloor               int         NULL,
    quantitysystem        int         NULL,
    quantityfound         int         NULL,
    bitfound              bit         NULL,
    lastmodifieddate      datetime    NULL,
    lastmodifieduserid    int         NULL,
    PRIMARY KEY CLUSTERED (idstockopnameitem)
)
go



/* 
 * TABLE: stockopname_status_tp 
 */

CREATE TABLE stockopname_status_tp(
    idstockopnamestatus    int             NOT NULL,
    description            nvarchar(10)    NULL,
    lastmodifieddate       datetime        NULL,
    lastmodifieduserid     int             NULL,
    PRIMARY KEY CLUSTERED (idstockopnamestatus)
)
go



/* 
 * TABLE: stockopname_tm 
 */

CREATE TABLE stockopname_tm(
    idstockopname         int             IDENTITY(1,1),
    stockopnamenumber     nvarchar(20)    NULL,
    stockopnamedate       datetime        NULL,
    status                int             NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    PRIMARY KEY CLUSTERED (idstockopname)
)
go



/* 
 * TABLE: suggest_part 
 */

CREATE TABLE suggest_part(
    idsuggestpart     int               IDENTITY(1,1),
    idproductmotor    DOM_ID            NULL,
    idproductpart     DOM_ID            NULL,
    km                decimal(12, 2)    NULL,
    ismultiply        bit               NULL,
    range_            decimal(12, 2)    NULL,
    PRIMARY KEY CLUSTERED (idsuggestpart)
)
go



/* 
 * TABLE: summary 
 */

CREATE TABLE summary(
    idsummary             int               IDENTITY(1,1),
    idpkb                 DOM_IDENTITY      NULL,
    totalunit             int               NULL,
    serviceqty            int               NULL,
    njbmechanic           decimal(18, 2)    NULL,
    nscmechanic           decimal(18, 2)    NULL,
    totalnjbmec           decimal(18, 2)    NULL,
    totalnscmec           decimal(18, 2)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    PRIMARY KEY NONCLUSTERED (idsummary)
)
go



/* 
 * TABLE: suspect 
 */

CREATE TABLE suspect(
    idsuspect             DOM_UUID           NOT NULL,
    suspectnumber         DOM_ID             NULL,
    idparty               DOM_UUID           NULL,
    idsalescoordinator    DOM_UUID           NULL,
    idsalesman            DOM_UUID           NULL,
    idaddress             DOM_UUID           NULL,
    idsuspecttyp          DOM_IDENTITY       NULL,
    iddealer              DOM_ID             NULL,
    salesdt               DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    marketname            DOM_DESCRIPTION    NULL,
    repurchaseprob        float              NULL,
    priority              float              NULL,
    idsaletype            DOM_INTEGER        NULL,
    idlsgpro              DOM_UUID           NULL,
    PRIMARY KEY NONCLUSTERED (idsuspect)
)
go



/* 
 * TABLE: suspect_role 
 */

CREATE TABLE suspect_role(
    idrole        DOM_UUID        NOT NULL,
    idsuspect     DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: suspect_status 
 */

CREATE TABLE suspect_status(
    idstatus        DOM_UUID        NOT NULL,
    idsuspect       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: suspect_type 
 */

CREATE TABLE suspect_type(
    idsuspecttyp    DOM_IDENTITY       IDENTITY(1,1),
    description     DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idsuspecttyp)
)
go



/* 
 * TABLE: sympthom_data_m 
 */

CREATE TABLE sympthom_data_m(
    idsympthomdatam       DOM_IDENTITY    IDENTITY(1,1),
    descsymptom           varchar(150)    NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    PRIMARY KEY NONCLUSTERED (idsympthomdatam)
)
go



/* 
 * TABLE: sysdiagrams 
 */

CREATE TABLE sysdiagrams(
    diagram_id      int               IDENTITY(1,1),
    name            DOM_NAME          NULL,
    principal_id    int               NOT NULL,
    version         int               NULL,
    definition      varbinary(max)    NULL,
    PRIMARY KEY CLUSTERED (diagram_id),
    UNIQUE (principal_id, name)
)
go



/* 
 * TABLE: telecomunication_number 
 */

CREATE TABLE telecomunication_number(
    idcontact    DOM_UUID    NOT NULL,
    number       DOM_ID      NULL,
    PRIMARY KEY NONCLUSTERED (idcontact)
)
go



/* 
 * TABLE: type_pkb 
 */

CREATE TABLE type_pkb(
    idtypepkb    DOM_INTEGER    NOT NULL,
    desctype     varchar(30)    NULL,
    PRIMARY KEY CLUSTERED (idtypepkb)
)
go



/* 
 * TABLE: unit_accesories_mapper 
 */

CREATE TABLE unit_accesories_mapper(
    iduntaccmap    DOM_UUID        NOT NULL,
    idmotor        DOM_ID          NULL,
    acc1           DOM_ID          NULL,
    qtyacc1        DOM_QTY         NULL,
    acc2           DOM_ID          NULL,
    qtyacc2        DOM_QTY         NULL,
    acc3           DOM_ID          NULL,
    qtyacc3        DOM_QTY         NULL,
    acc4           DOM_ID          NULL,
    qtyacc4        DOM_QTY         NULL,
    acc5           DOM_ID          NULL,
    qtyacc5        DOM_QTY         NULL,
    acc6           DOM_ID          NULL,
    qtyacc6        DOM_QTY         NULL,
    acc7           DOM_ID          NULL,
    qtyacc7        DOM_QTY         NULL,
    promat1        DOM_ID          NULL,
    qtypromat1     DOM_QTY         NULL,
    promat2        DOM_ID          NULL,
    qtypromat2     DOM_QTY         NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (iduntaccmap)
)
go



/* 
 * TABLE: unit_deliverable 
 */

CREATE TABLE unit_deliverable(
    iddeliverable     DOM_UUID           NOT NULL,
    idreq             DOM_UUID           NULL,
    iddeltype         DOM_IDENTITY       NULL,
    idvndstatyp       DOM_INTEGER        NULL,
    idconstatyp       DOM_INTEGER        NULL,
    description       DOM_DESCRIPTION    NULL,
    dtreceipt         DOM_DATE           NULL,
    dtdelivery        DOM_DATE           NULL,
    bastnumber        DOM_ID             NULL,
    name              DOM_NAME           NULL,
    identitynumber    DOM_ID             NULL,
    cellphone         DOM_ID             NULL,
    refnumber         DOM_DESCRIPTION    NULL,
    refdt             DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    receiptqty        DOM_INTEGER        NULL,
    receiptnominal    DOM_MONEY          NULL,
    PRIMARY KEY CLUSTERED (iddeliverable)
)
go



/* 
 * TABLE: unit_document_message 
 */

CREATE TABLE unit_document_message(
    idmessage       DOM_IDENTITY    IDENTITY(1,1),
    idparent        DOM_IDENTITY    NULL,
    idreq           DOM_UUID        NULL,
    idprospect      DOM_UUID        NULL,
    content         DOM_LONGDESC    NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    usernamefrom    DOM_NAME        NULL,
    usernameto      DOM_NAME        NULL,
    PRIMARY KEY CLUSTERED (idmessage)
)
go



/* 
 * TABLE: unit_preparation 
 */

CREATE TABLE unit_preparation(
    idslip         DOM_UUID    NOT NULL,
    idinternal     DOM_ID      NULL,
    idfacility     DOM_UUID    NULL,
    usermekanik    DOM_NAME    NULL,
    PRIMARY KEY CLUSTERED (idslip)
)
go



/* 
 * TABLE: unit_requirement 
 */

CREATE TABLE unit_requirement(
    idreq        DOM_UUID    NOT NULL,
    idproduct    DOM_ID      NULL,
    idbranch     DOM_ID      NULL,
    PRIMARY KEY NONCLUSTERED (idreq)
)
go



/* 
 * TABLE: unit_shipment_receipt 
 */

CREATE TABLE unit_shipment_receipt(
    idreceipt       DOM_UUID       NOT NULL,
    idframe         DOM_ID         NULL,
    idmachine       DOM_ID         NULL,
    yearassembly    DOM_INTEGER    NULL,
    unitprice       DOM_MONEY      NULL,
    acc1            DOM_BOOLEAN    NULL,
    acc2            DOM_BOOLEAN    NULL,
    acc3            DOM_BOOLEAN    NULL,
    acc4            DOM_BOOLEAN    NULL,
    acc5            DOM_BOOLEAN    NULL,
    acc6            DOM_BOOLEAN    NULL,
    acc7            DOM_BOOLEAN    NULL,
    isreceipt       DOM_BOOLEAN    NULL,
    PRIMARY KEY CLUSTERED (idreceipt)
)
go



/* 
 * TABLE: uom 
 */

CREATE TABLE uom(
    iduom           DOM_ID             NOT NULL,
    iduomtyp        DOM_IDENTITY       NULL,
    description     DOM_DESCRIPTION    NULL,
    abbreviation    DOM_NAME           NULL,
    PRIMARY KEY NONCLUSTERED (iduom)
)
go



/* 
 * TABLE: uom_conversion 
 */

CREATE TABLE uom_conversion(
    iduomconversion    DOM_IDENTITY    IDENTITY(1,1),
    iduomto            DOM_ID          NULL,
    iduomfro           DOM_ID          NULL,
    factor             DOM_QTY         NULL,
    PRIMARY KEY NONCLUSTERED (iduomconversion)
)
go



/* 
 * TABLE: uom_type 
 */

CREATE TABLE uom_type(
    iduomtyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (iduomtyp)
)
go



/* 
 * TABLE: user_extra 
 */

CREATE TABLE user_extra(
    iduser        DOM_LONG    NOT NULL,
    idinternal    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (iduser)
)
go



/* 
 * TABLE: user_mediator 
 */

CREATE TABLE user_mediator(
    idusrmed      DOM_UUID           NOT NULL,
    id            DOM_LONG           NULL,
    idperson      DOM_UUID           NULL,
    idinternal    DOM_ID             NULL,
    email         DOM_DESCRIPTION    NULL,
    username      DOM_NAME           NULL,
    firstname     DOM_NAME           NULL,
    lastname      DOM_NAME           NULL,
    PRIMARY KEY CLUSTERED (idusrmed)
)
go



/* 
 * TABLE: user_mediator_role 
 */

CREATE TABLE user_mediator_role(
    idrole        DOM_UUID        NOT NULL,
    idusrmed      DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idrole)
)
go



/* 
 * TABLE: user_mediator_status 
 */

CREATE TABLE user_mediator_status(
    idstatus        DOM_UUID        NOT NULL,
    idusrmed        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY CLUSTERED (idstatus)
)
go



/* 
 * TABLE: vehicle 
 */

CREATE TABLE vehicle(
    idvehicle            DOM_UUID           NOT NULL,
    idproduct            DOM_ID             NULL,
    idcolor              DOM_IDENTITY       NULL,
    idinternal           DOM_ID             NULL,
    idmachine            DOM_DESCRIPTION    NULL,
    idframe              DOM_DESCRIPTION    NULL,
    yearofass            DOM_INTEGER        NULL,
    fuel                 DOM_ID             NULL,
    merkoil              DOM_ID             NULL,
    servicebooknumber    DOM_ID             NULL,
    PRIMARY KEY NONCLUSTERED (idvehicle)
)
go



/* 
 * TABLE: vehicle_carrier 
 */

CREATE TABLE vehicle_carrier(
    idvehide    DOM_UUID       NOT NULL,
    idparty     DOM_UUID       NULL,
    idreason    DOM_INTEGER    NULL,
    idreltyp    DOM_INTEGER    NULL,
    PRIMARY KEY CLUSTERED (idvehide)
)
go



/* 
 * TABLE: vehicle_customer_request 
 */

CREATE TABLE vehicle_customer_request(
    idrequest        DOM_UUID    NOT NULL,
    idcustomer       DOM_ID      NULL,
    idsalesbroker    DOM_UUID    NULL,
    customerorder    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idrequest)
)
go



/* 
 * TABLE: vehicle_document_requirement 
 */

CREATE TABLE vehicle_document_requirement(
    idreq                  DOM_UUID           NOT NULL,
    idslsreq               DOM_UUID           NULL,
    idordite               DOM_UUID           NULL,
    idorganizationowner    DOM_UUID           NULL,
    idpersonowner          DOM_UUID           NULL,
    idvehregtyp            DOM_INTEGER        NULL,
    idsaletype             DOM_IDENTITY       NULL,
    idvehicle              DOM_UUID           NULL,
    idvendor               DOM_ID             NULL,
    idinternal             DOM_ID             NULL,
    idbillto               DOM_ID             NULL,
    idshipto               DOM_ID             NULL,
    atpmfaktur             DOM_ID             NULL,
    note                   DOM_LONGDESC       NULL,
    bbn                    DOM_MONEY          NULL,
    othercost              DOM_MONEY          NULL,
    policenumber           DOM_ID             NULL,
    atpmfakturdt           DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    submissionno           DOM_DESCRIPTION    NULL,
    submissiondt           DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    crossarea              DOM_INTEGER        NULL,
    dtfillname             DOM_DATE           NULL,
    registrationnumber     DOM_ID             NULL,
    statusfaktur           DOM_ID             NULL,
    waitstnk               DOM_BOOLEAN        NULL,
    reffnumber             DOM_ID             NULL,
    dtreff                 DOM_DATE           NULL,
    PRIMARY KEY CLUSTERED (idreq)
)
go



/* 
 * TABLE: vehicle_identification 
 */

CREATE TABLE vehicle_identification(
    idvehide          DOM_UUID           NOT NULL,
    idvehicle         DOM_UUID           NULL,
    idcustomer        DOM_ID             NULL,
    vehiclenumber     DOM_SHORTID        NULL,
    dealerpurchase    DOM_DESCRIPTION    NULL,
    dtpurchase        DOM_DATE           NULL,
    dtfrom            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru            DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idvehide)
)
go



/* 
 * TABLE: vehicle_purchase_order 
 */

CREATE TABLE vehicle_purchase_order(
    idorder    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: vehicle_registration_type 
 */

CREATE TABLE vehicle_registration_type(
    idvehregtyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idvehregtyp)
)
go



/* 
 * TABLE: vehicle_sales_billing 
 */

CREATE TABLE vehicle_sales_billing(
    idbilling        DOM_UUID        NOT NULL,
    idsaletype       DOM_IDENTITY    NULL,
    idcustomer       DOM_ID          NULL,
    axunitprice      DOM_MONEY       NULL,
    axbbn            DOM_MONEY       NULL,
    axsalesamount    DOM_MONEY       NULL,
    axdisc           DOM_MONEY       NULL,
    axprice          DOM_MONEY       NULL,
    axppn            DOM_MONEY       NULL,
    axaramt          DOM_MONEY       NULL,
    axadd1           DOM_MONEY       NULL,
    axadd2           DOM_MONEY       NULL,
    axadd3           DOM_MONEY       NULL,
    PRIMARY KEY CLUSTERED (idbilling)
)
go



/* 
 * TABLE: vehicle_sales_order 
 */

CREATE TABLE vehicle_sales_order(
    idorder                DOM_UUID           NOT NULL,
    idunitreq              DOM_UUID           NULL,
    idsaletype             DOM_IDENTITY       NULL,
    idleacom               DOM_UUID           NULL,
    shiptoowner            DOM_BOOLEAN        NULL,
    dtbastunit             DOM_DATE           NULL,
    dtcustreceipt          DOM_DATE           NULL,
    dtmachineswipe         DOM_DATE           NULL,
    dttakepicture          DOM_DATE           NULL,
    dtcovernote            DOM_DATE           NULL,
    idlsgpaystatus         DOM_INTEGER        NULL,
    dtadmslsverify         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtafcverify            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    admslsverifyval        DOM_INTEGER        NULL,
    afcverifyval           DOM_INTEGER        NULL,
    admslsnotapprovnote    DOM_LONGDESC       NULL,
    afcnotapprovnote       DOM_LONGDESC       NULL,
    vrnama1                DOM_LONGDESC       NULL,
    vrnama2                DOM_LONGDESC       NULL,
    vrnamamarket           DOM_LONGDESC       NULL,
    vrwarna                DOM_LONGDESC       NULL,
    vrtahunproduksi        DOM_LONG           NULL,
    vrangsuran             DOM_MONEY          NULL,
    vrtenor                DOM_INTEGER        NULL,
    vrleasing              DOM_LONGDESC       NULL,
    vrtglpengiriman        DOM_ID             NULL,
    vrjampengiriman        DOM_DESCRIPTION    NULL,
    vrtipepenjualan        DOM_DESCRIPTION    NULL,
    vrdpmurni              DOM_MONEY          NULL,
    vrtandajadi            DOM_MONEY          NULL,
    vrtglpembayaran        DOM_ID             NULL,
    vrsisa                 DOM_MONEY          NULL,
    vriscod                DOM_BOOLEAN        NULL,
    vrcatatantambahan      DOM_LONGDESC       NULL,
    vrisverified           DOM_BOOLEAN        NULL,
    vrcatatan              DOM_LONGDESC       NULL,
    kacabnote              DOM_LONGDESC       NULL,
    isreqtaxinv            DOM_BOOLEAN        NULL,
    isdonematching         DOM_BOOLEAN        NULL,
    vridsaletype           DOM_INTEGER        NULL,
    idsalesman             DOM_UUID           NULL,
    idbirojasa             DOM_ID             NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: vehicle_service_history 
 */

CREATE TABLE vehicle_service_history(
    idvehicleservicehistory    DOM_IDENTITY      IDENTITY(1,1),
    idvehicle                  DOM_UUID          NULL,
    location                   varchar(100)      NULL,
    km                         decimal(10, 2)    NULL,
    jobsuggest                 varchar(100)      NULL,
    servicedate                datetime          NULL,
    mechanicname               varchar(30)       NULL,
    nextservicedate            date              NULL,
    vehiclenumber              DOM_ID            NULL,
    idframe                    DOM_ID            NULL,
    idmachine                  DOM_ID            NULL,
    PRIMARY KEY CLUSTERED (idvehicleservicehistory)
)
go



/* 
 * TABLE: vehicle_work_requirement 
 */

CREATE TABLE vehicle_work_requirement(
    idreq            DOM_UUID       NOT NULL,
    idmechanic       DOM_UUID       NULL,
    idvehicle        DOM_UUID       NULL,
    idvehide         DOM_UUID       NULL,
    idcustomer       DOM_ID         NULL,
    vehiclenumber    DOM_SHORTID    NULL,
    PRIMARY KEY NONCLUSTERED (idreq)
)
go



/* 
 * TABLE: vendor 
 */

CREATE TABLE vendor(
    idvendor      DOM_ID         NOT NULL,
    idvndtyp      DOM_INTEGER    NULL,
    idroletype    DOM_INTEGER    NULL,
    idparty       DOM_UUID       NULL,
    PRIMARY KEY NONCLUSTERED (idvendor)
)
go



/* 
 * TABLE: vendor_order 
 */

CREATE TABLE vendor_order(
    idorder       DOM_UUID    NOT NULL,
    idvendor      DOM_ID      NULL,
    idinternal    DOM_ID      NULL,
    idbillto      DOM_ID      NULL,
    PRIMARY KEY NONCLUSTERED (idorder)
)
go



/* 
 * TABLE: vendor_product 
 */

CREATE TABLE vendor_product(
    idvenpro      DOM_UUID        NOT NULL,
    idproduct     DOM_ID          NULL,
    idvendor      DOM_ID          NULL,
    idinternal    DOM_ID          NULL,
    orderratio    DOM_INTEGER     NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idvenpro)
)
go



/* 
 * TABLE: vendor_relationship 
 */

CREATE TABLE vendor_relationship(
    idparrel      DOM_UUID    NOT NULL,
    idvendor      DOM_ID      NULL,
    idinternal    DOM_ID      NULL,
    PRIMARY KEY CLUSTERED (idparrel)
)
go



/* 
 * TABLE: vendor_type 
 */

CREATE TABLE vendor_type(
    idvndtyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY CLUSTERED (idvndtyp)
)
go



/* 
 * TABLE: village 
 */

CREATE TABLE village(
    idgeobou    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



/* 
 * TABLE: we_service_type 
 */

CREATE TABLE we_service_type(
    idwetyp      DOM_INTEGER    NOT NULL,
    idproduct    DOM_ID         NULL,
    frt          DOM_INTEGER    NULL,
    PRIMARY KEY NONCLUSTERED (idwetyp)
)
go



/* 
 * TABLE: we_type 
 */

CREATE TABLE we_type(
    idwetyp        DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idwetyp)
)
go



/* 
 * TABLE: we_type_good_standard 
 */

CREATE TABLE we_type_good_standard(
    idwegoostd    DOM_UUID       NOT NULL,
    idwetyp       DOM_INTEGER    NULL,
    idproduct     DOM_ID         NULL,
    qty           DOM_QTY        NULL,
    PRIMARY KEY NONCLUSTERED (idwegoostd)
)
go



/* 
 * TABLE: work_effort 
 */

CREATE TABLE work_effort(
    idwe           DOM_UUID           NOT NULL,
    idfacility     DOM_UUID           NULL,
    idwetyp        DOM_INTEGER        NULL,
    name           DOM_NAME           NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idwe)
)
go



/* 
 * TABLE: work_effort_payment 
 */

CREATE TABLE work_effort_payment(
    payments_id        DOM_UUID    NOT NULL,
    work_efforts_id    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (payments_id, work_efforts_id)
)
go



/* 
 * TABLE: work_effort_role 
 */

CREATE TABLE work_effort_role(
    idrole        DOM_UUID        NOT NULL,
    idwe          DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: work_effort_status 
 */

CREATE TABLE work_effort_status(
    idstatus        DOM_UUID        NOT NULL,
    idwe            DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: work_order 
 */

CREATE TABLE work_order(
    idreq    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idreq)
)
go



/* 
 * TABLE: work_order_booking 
 */

CREATE TABLE work_order_booking(
    idbooking        DOM_UUID        NOT NULL,
    idvehicle        DOM_UUID        NULL,
    idbooslo         DOM_UUID        NULL,
    idboktyp         DOM_IDENTITY    NULL,
    idevetyp         DOM_IDENTITY    NULL,
    idinternal       DOM_ID          NULL,
    idcustomer       DOM_ID          NULL,
    bookingnumber    DOM_ID          NULL,
    vehiclenumber    DOM_SHORTID     NULL,
    dtcreate         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    PRIMARY KEY NONCLUSTERED (idbooking)
)
go



/* 
 * TABLE: work_order_booking_role 
 */

CREATE TABLE work_order_booking_role(
    idrole        DOM_UUID        NOT NULL,
    idbooking     DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_INTEGER     NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idrole)
)
go



/* 
 * TABLE: work_order_booking_status 
 */

CREATE TABLE work_order_booking_status(
    idstatus        DOM_UUID        NOT NULL,
    idbooking       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    PRIMARY KEY NONCLUSTERED (idstatus)
)
go



/* 
 * TABLE: work_product_req 
 */

CREATE TABLE work_product_req(
    idreq    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idreq)
)
go



/* 
 * TABLE: work_requirement 
 */

CREATE TABLE work_requirement(
    idreq    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idreq)
)
go



/* 
 * TABLE: work_requirement_services 
 */

CREATE TABLE work_requirement_services(
    idwe     DOM_UUID    NOT NULL,
    idreq    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idwe, idreq)
)
go



/* 
 * TABLE: work_service_requirement 
 */

CREATE TABLE work_service_requirement(
    idwe    DOM_UUID    NOT NULL,
    PRIMARY KEY NONCLUSTERED (idwe)
)
go



/* 
 * TABLE: work_type 
 */

CREATE TABLE work_type(
    idworktype     DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    PRIMARY KEY NONCLUSTERED (idworktype)
)
go



/* 
 * INDEX: geo_boundary_code 
 */

CREATE UNIQUE INDEX geo_boundary_code ON geo_boundary(geocode)
go
/* 
 * INDEX: FK_JOB_STAT_REFERENCE_STATUS_T 
 */

CREATE INDEX FK_JOB_STAT_REFERENCE_STATUS_T ON job_status(idstatustype)
go
/* 
 * INDEX: leasing_company_id 
 */

CREATE UNIQUE INDEX leasing_company_id ON leasing_company(idleacom)
go
/* 
 * INDEX: mem_number 
 */

CREATE UNIQUE INDEX mem_number ON memo(memonumber)
go
/* 
 * INDEX: [nonclusteredindex-20180401-183606] 
 */

CREATE INDEX [nonclusteredindex-20180401-183606] ON package_receipt(documentnumber)
go
/* 
 * INDEX: party_category_refkey 
 */

CREATE INDEX party_category_refkey ON party_category(idcattyp, refkey)
go
/* 
 * INDEX: idpaytyp 
 */

CREATE INDEX idpaytyp ON payment(idpaytyp)
go
/* 
 * INDEX: person_id_user 
 */

CREATE INDEX person_id_user ON person(username)
go
/* 
 * INDEX: idordite 
 */

CREATE INDEX idordite ON picking_slip(idordite)
go
/* 
 * INDEX: idinvite 
 */

CREATE INDEX idinvite ON picking_slip(idinvite)
go
/* 
 * INDEX: FK_PKB_PART_REFERENCE_PKB 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_PKB ON pkb_part(idpkb)
go
/* 
 * INDEX: idinvite 
 */

CREATE UNIQUE INDEX idinvite ON pkb_part(idinvite)
go
/* 
 * INDEX: FK_PKB_PART_REFERENCE_CHARGE_T 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_CHARGE_T ON pkb_part(idchargeto)
go
/* 
 * INDEX: FK_PKB_PART_REFERENCE_PKB_SERV 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_PKB_SERV ON pkb_part(idpkbservice)
go
/* 
 * INDEX: FK_PKB_PART_REFERENCE_CATEGORY 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_CATEGORY ON pkb_part(idcategory)
go
/* 
 * INDEX: FK_PKB_PART_REFERENCE_GOOD 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_GOOD ON pkb_part(idproduct)
go
/* 
 * INDEX: FK_PKB_SERV_REFERENCE_CATEGORY 
 */

CREATE INDEX FK_PKB_SERV_REFERENCE_CATEGORY ON pkb_service(idcategory)
go
/* 
 * INDEX: FK_PKB_SERV_REFERENCE_SERVICE 
 */

CREATE INDEX FK_PKB_SERV_REFERENCE_SERVICE ON pkb_service(idproduct)
go
/* 
 * INDEX: FK_PKB_SERV_REFERENCE_CHARGE_T 
 */

CREATE INDEX FK_PKB_SERV_REFERENCE_CHARGE_T ON pkb_service(idchargeto)
go
/* 
 * INDEX: FK_PKB_SERV_RELATIONS_PKB 
 */

CREATE INDEX FK_PKB_SERV_RELATIONS_PKB ON pkb_service(idpkb)
go
/* 
 * INDEX: price_component_period 
 */

CREATE INDEX price_component_period ON price_component(dtfrom, dtthru)
go
/* 
 * INDEX: idprotyp 
 */

CREATE INDEX idprotyp ON product(idprotyp)
go
/* 
 * INDEX: product_category_refkey 
 */

CREATE INDEX product_category_refkey ON product_category(idcattyp, refkey)
go
/* 
 * INDEX: idproduct 
 */

CREATE INDEX idproduct ON sales_unit_requirement(idproduct)
go
/* 
 * INDEX: idowner 
 */

CREATE INDEX idowner ON sales_unit_requirement(idowner)
go
/* 
 * INDEX: idprospect 
 */

CREATE INDEX idprospect ON sales_unit_requirement(idprospect)
go
/* 
 * INDEX: idsalesman 
 */

CREATE INDEX idsalesman ON sales_unit_requirement(idsalesman)
go
/* 
 * INDEX: idcolor 
 */

CREATE INDEX idcolor ON sales_unit_requirement(idcolor)
go
/* 
 * INDEX: idsalesbroker 
 */

CREATE INDEX idsalesbroker ON sales_unit_requirement(idsalesbroker)
go
/* 
 * INDEX: idlsgpro 
 */

CREATE INDEX idlsgpro ON sales_unit_requirement(idlsgpro)
go
/* 
 * INDEX: idsaletype 
 */

CREATE INDEX idsaletype ON sales_unit_requirement(idsaletype)
go
/* 
 * INDEX: idcoordinator 
 */

CREATE INDEX idcoordinator ON salesman(idcoordinator)
go
/* 
 * INDEX: stk_op_item_tag 
 */

CREATE UNIQUE INDEX stk_op_item_tag ON stock_opname_item(tagnumber)
go
/* 
 * INDEX: idsalesman 
 */

CREATE INDEX idsalesman ON suspect(idsalesman)
go
/* 
 * INDEX: idsuspecttyp 
 */

CREATE INDEX idsuspecttyp ON suspect(idsuspecttyp)
go
/* 
 * INDEX: idparty 
 */

CREATE INDEX idparty ON suspect(idparty)
go
/* 
 * INDEX: idsalescoordinator 
 */

CREATE INDEX idsalescoordinator ON suspect(idsalescoordinator)
go
/* 
 * INDEX: idaddress 
 */

CREATE INDEX idaddress ON suspect(idaddress)
go
/* 
 * INDEX: iddeltype 
 */

CREATE INDEX iddeltype ON unit_deliverable(iddeltype)
go
/* 
 * INDEX: idvndstatyp 
 */

CREATE INDEX idvndstatyp ON unit_deliverable(idvndstatyp)
go
/* 
 * INDEX: idconstatyp 
 */

CREATE INDEX idconstatyp ON unit_deliverable(idconstatyp)
go
/* 
 * INDEX: idreq 
 */

CREATE INDEX idreq ON unit_deliverable(idreq)
go
/* 
 * INDEX: user_mediator_username 
 */

CREATE UNIQUE INDEX user_mediator_username ON user_mediator(username)
go
/* 
 * INDEX: idsaletype 
 */

CREATE INDEX idsaletype ON vehicle_document_requirement(idsaletype)
go
/* 
 * INDEX: idordite 
 */

CREATE INDEX idordite ON vehicle_document_requirement(idordite)
go
/* 
 * INDEX: idpersonowner 
 */

CREATE INDEX idpersonowner ON vehicle_document_requirement(idpersonowner)
go
/* 
 * INDEX: idvehregtyp 
 */

CREATE INDEX idvehregtyp ON vehicle_document_requirement(idvehregtyp)
go
/* 
 * INDEX: idvehicle 
 */

CREATE INDEX idvehicle ON vehicle_document_requirement(idvehicle)
go
/* 
 * INDEX: idslsreq 
 */

CREATE INDEX idslsreq ON vehicle_document_requirement(idslsreq)
go
/* 
 * INDEX: idsaletype 
 */

CREATE INDEX idsaletype ON vehicle_sales_order(idsaletype)
go
/* 
 * INDEX: idleacom 
 */

CREATE INDEX idleacom ON vehicle_sales_order(idleacom)
go
/* 
 * INDEX: idunitreq 
 */

CREATE INDEX idunitreq ON vehicle_sales_order(idunitreq)
go
/* 
 * INDEX: we_service_type_id_product 
 */

CREATE UNIQUE INDEX we_service_type_id_product ON we_service_type(idproduct)
go
/* 
 * TABLE: accounting_calendar 
 */

ALTER TABLE accounting_calendar ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
go

ALTER TABLE accounting_calendar ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: acctg_trans_detail 
 */

ALTER TABLE acctg_trans_detail ADD 
    FOREIGN KEY (idglacc)
    REFERENCES gl_account(idglacc)
go

ALTER TABLE acctg_trans_detail ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE acctg_trans_detail ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES accounting_calendar(idcalendar)
go

ALTER TABLE acctg_trans_detail ADD 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go


/* 
 * TABLE: acctg_transaction 
 */

ALTER TABLE acctg_transaction ADD 
    FOREIGN KEY (idacctratyp)
    REFERENCES acctg_trans_type(idacctratyp)
go


/* 
 * TABLE: acctg_transaction_status 
 */

ALTER TABLE acctg_transaction_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE acctg_transaction_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE acctg_transaction_status ADD 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go


/* 
 * TABLE: agreement 
 */

ALTER TABLE agreement ADD 
    FOREIGN KEY (idagrtyp)
    REFERENCES agreement_type(idagrtyp)
go


/* 
 * TABLE: agreement_item 
 */

ALTER TABLE agreement_item ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go


/* 
 * TABLE: agreement_motor_item 
 */

ALTER TABLE agreement_motor_item ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE agreement_motor_item ADD 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
go


/* 
 * TABLE: agreement_role 
 */

ALTER TABLE agreement_role ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE agreement_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE agreement_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: agreement_status 
 */

ALTER TABLE agreement_status ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE agreement_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE agreement_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: approval 
 */

ALTER TABLE approval ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE approval ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE approval ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE approval ADD 
    FOREIGN KEY (idapptyp)
    REFERENCES approval_type(idapptyp)
go

ALTER TABLE approval ADD 
    FOREIGN KEY (idmessage)
    REFERENCES unit_document_message(idmessage)
go


/* 
 * TABLE: approval_stockopname_tm 
 */

ALTER TABLE approval_stockopname_tm ADD 
    FOREIGN KEY (idstockopname)
    REFERENCES stockopname_tm(idstockopname)
go

ALTER TABLE approval_stockopname_tm ADD 
    FOREIGN KEY (idapprovalstockopnamestatus)
    REFERENCES approval_stockopname_status_tp(idapprovalstockopnamestatus)
go


/* 
 * TABLE: attendance 
 */

ALTER TABLE attendance ADD 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go


/* 
 * TABLE: base_calendar 
 */

ALTER TABLE base_calendar ADD 
    FOREIGN KEY (idcaltyp)
    REFERENCES calendar_type(idcaltyp)
go

ALTER TABLE base_calendar ADD 
    FOREIGN KEY (idparent)
    REFERENCES base_calendar(idcalendar)
go


/* 
 * TABLE: bill_to 
 */

ALTER TABLE bill_to ADD 
    FOREIGN KEY (idparty, idroletype)
    REFERENCES party_role_type(idparty, idroletype)
go


/* 
 * TABLE: billing 
 */

ALTER TABLE billing ADD 
    FOREIGN KEY (idbiltyp)
    REFERENCES billing_type(idbiltyp)
go

ALTER TABLE billing ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE billing ADD 
    FOREIGN KEY (idbilto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE billing ADD 
    FOREIGN KEY (idbilfro)
    REFERENCES bill_to(idbillto)
go


/* 
 * TABLE: billing_acctg_trans 
 */

ALTER TABLE billing_acctg_trans ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_acctg_trans ADD 
    FOREIGN KEY (idacctra)
    REFERENCES external_acctg_trans(idacctra)
go


/* 
 * TABLE: billing_disbursement 
 */

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
go


/* 
 * TABLE: billing_issuance 
 */

ALTER TABLE billing_issuance ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE billing_issuance ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

ALTER TABLE billing_issuance ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE billing_issuance ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go


/* 
 * TABLE: billing_item 
 */

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idbilitetyp)
    REFERENCES billing_item_type(idbilitetyp)
go

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go


/* 
 * TABLE: billing_njb 
 */

ALTER TABLE billing_njb ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: billing_nsc 
 */

ALTER TABLE billing_nsc ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: billing_receipt 
 */

ALTER TABLE billing_receipt ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_receipt ADD 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
go


/* 
 * TABLE: billing_role 
 */

ALTER TABLE billing_role ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE billing_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: billing_status 
 */

ALTER TABLE billing_status ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE billing_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: booking 
 */

ALTER TABLE booking ADD 
    FOREIGN KEY (idbookingstatus)
    REFERENCES booking_status(idbookingstatus)
go


/* 
 * TABLE: booking_slot 
 */

ALTER TABLE booking_slot ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
go

ALTER TABLE booking_slot ADD 
    FOREIGN KEY (idbooslostd)
    REFERENCES booking_slot_standard(idbooslostd)
go


/* 
 * TABLE: booking_slot_standard 
 */

ALTER TABLE booking_slot_standard ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE booking_slot_standard ADD 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
go


/* 
 * TABLE: branch 
 */

ALTER TABLE branch ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE branch ADD 
    FOREIGN KEY (idbranchcategory)
    REFERENCES party_category(idcategory)
go


/* 
 * TABLE: carrier 
 */

ALTER TABLE carrier ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE carrier ADD 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
go

ALTER TABLE carrier ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE carrier ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go


/* 
 * TABLE: category 
 */

ALTER TABLE category ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES category_type(idcattyp)
go


/* 
 * TABLE: category_type 
 */

ALTER TABLE category_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go

ALTER TABLE category_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES category_type(idcattyp)
go


/* 
 * TABLE: city 
 */

ALTER TABLE city ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: claim_detail 
 */

ALTER TABLE claim_detail ADD 
    FOREIGN KEY (iddatadamage)
    REFERENCES data_damage(iddatadamage)
go

ALTER TABLE claim_detail ADD 
    FOREIGN KEY (idsympthomdatam)
    REFERENCES sympthom_data_m(idsympthomdatam)
go

ALTER TABLE claim_detail ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: communication_event 
 */

ALTER TABLE communication_event ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go

ALTER TABLE communication_event ADD 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
go


/* 
 * TABLE: communication_event_cdb 
 */

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (idprovince)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (idcity)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (iddistrict)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: communication_event_delivery 
 */

ALTER TABLE communication_event_delivery ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE communication_event_delivery ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: communication_event_prospect 
 */

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
go

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go


/* 
 * TABLE: communication_event_purpose 
 */

ALTER TABLE communication_event_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go

ALTER TABLE communication_event_purpose ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go


/* 
 * TABLE: communication_event_role 
 */

ALTER TABLE communication_event_role ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE communication_event_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: communication_event_status 
 */

ALTER TABLE communication_event_status ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE communication_event_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: contact_purpose 
 */

ALTER TABLE contact_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go

ALTER TABLE contact_purpose ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go


/* 
 * TABLE: container 
 */

ALTER TABLE container ADD 
    FOREIGN KEY (idcontyp)
    REFERENCES container_type(idcontyp)
go

ALTER TABLE container ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: correction_note 
 */

ALTER TABLE correction_note ADD 
    FOREIGN KEY (idspgclaim)
    REFERENCES spg_claim(idspgclaim)
go

ALTER TABLE correction_note ADD 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go

ALTER TABLE correction_note ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: customer 
 */

ALTER TABLE customer ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE customer ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE customer ADD 
    FOREIGN KEY (idmarketcategory)
    REFERENCES party_category(idcategory)
go


/* 
 * TABLE: customer_order 
 */

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: customer_quotation 
 */

ALTER TABLE customer_quotation ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_quotation ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE customer_quotation ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go


/* 
 * TABLE: customer_relationship 
 */

ALTER TABLE customer_relationship ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE customer_relationship ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_relationship ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go


/* 
 * TABLE: customer_request_item 
 */

ALTER TABLE customer_request_item ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_request_item ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: delivery_order 
 */

ALTER TABLE delivery_order ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing_disbursement(idbilling)
go


/* 
 * TABLE: dimension 
 */

ALTER TABLE dimension ADD 
    FOREIGN KEY (iddimtyp)
    REFERENCES dimension_type(iddimtyp)
go


/* 
 * TABLE: disbursement 
 */

ALTER TABLE disbursement ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: district 
 */

ALTER TABLE district ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: document_type 
 */

ALTER TABLE document_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES document_type(iddoctype)
go


/* 
 * TABLE: documents 
 */

ALTER TABLE documents ADD 
    FOREIGN KEY (iddoctype)
    REFERENCES document_type(iddoctype)
go


/* 
 * TABLE: driver 
 */

ALTER TABLE driver ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE driver ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: electronic_address 
 */

ALTER TABLE electronic_address ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go


/* 
 * TABLE: employee 
 */

ALTER TABLE employee ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE employee ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go


/* 
 * TABLE: employee_customer_relationship 
 */

ALTER TABLE employee_customer_relationship ADD 
    FOREIGN KEY (employeenumber)
    REFERENCES employee(employeenumber)
go

ALTER TABLE employee_customer_relationship ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE employee_customer_relationship ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go


/* 
 * TABLE: event_type 
 */

ALTER TABLE event_type ADD 
    FOREIGN KEY (idprneve)
    REFERENCES event_type(idevetyp)
go


/* 
 * TABLE: exchange_part 
 */

ALTER TABLE exchange_part ADD 
    FOREIGN KEY (idspgclaimdetail)
    REFERENCES spg_claim_detail(idspgclaimdetail)
go

ALTER TABLE exchange_part ADD 
    FOREIGN KEY (idspgclaimaction)
    REFERENCES spg_claim_action(idspgclaimaction)
go

ALTER TABLE exchange_part ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: external_acctg_trans 
 */

ALTER TABLE external_acctg_trans ADD 
    FOREIGN KEY (idparfro)
    REFERENCES party(idparty)
go

ALTER TABLE external_acctg_trans ADD 
    FOREIGN KEY (idparto)
    REFERENCES party(idparty)
go

ALTER TABLE external_acctg_trans ADD 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go


/* 
 * TABLE: facility 
 */

ALTER TABLE facility ADD 
    FOREIGN KEY (idpartof)
    REFERENCES facility(idfacility)
go

ALTER TABLE facility ADD 
    FOREIGN KEY (idfacilitytype)
    REFERENCES facility_type(idfacilitytype)
go

ALTER TABLE facility ADD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
go


/* 
 * TABLE: facility_contact_mechanism 
 */

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: facility_status 
 */

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: feature 
 */

ALTER TABLE feature ADD 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
go


/* 
 * TABLE: feature_applicable 
 */

ALTER TABLE feature_applicable ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE feature_applicable ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE feature_applicable ADD 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
go

ALTER TABLE feature_applicable ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go


/* 
 * TABLE: feature_type 
 */

ALTER TABLE feature_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: fixed_asset 
 */

ALTER TABLE fixed_asset ADD 
    FOREIGN KEY (idfatype)
    REFERENCES fixed_asset_type(idfatype)
go

ALTER TABLE fixed_asset ADD 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
go


/* 
 * TABLE: fixed_asset_type 
 */

ALTER TABLE fixed_asset_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES fixed_asset_type(idfatype)
go


/* 
 * TABLE: fixedasset_adjustment_th 
 */

ALTER TABLE fixedasset_adjustment_th ADD 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
go


/* 
 * TABLE: fixedasset_th 
 */

ALTER TABLE fixedasset_th ADD 
    FOREIGN KEY (idlocationfloor)
    REFERENCES floor_tm(idfloor)
go

ALTER TABLE fixedasset_th ADD 
    FOREIGN KEY (idlocationroom)
    REFERENCES room_tm(idroom)
go

ALTER TABLE fixedasset_th ADD 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
go


/* 
 * TABLE: fixedasset_tm 
 */

ALTER TABLE fixedasset_tm ADD 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
go

ALTER TABLE fixedasset_tm ADD 
    FOREIGN KEY (idpurchaseorderreception)
    REFERENCES purchaseorder_reception_tr(idpurchaseorderreception)
go

ALTER TABLE fixedasset_tm ADD 
    FOREIGN KEY (idpurchaseorder)
    REFERENCES purchaseorder_tm(idpurchaseorder)
go

ALTER TABLE fixedasset_tm ADD 
    FOREIGN KEY (idroom)
    REFERENCES room_tm(idroom)
go

ALTER TABLE fixedasset_tm ADD 
    FOREIGN KEY (idcategoryasset)
    REFERENCES category_asset_tp(idcategoryasset)
go


/* 
 * TABLE: general_upload 
 */

ALTER TABLE general_upload ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go

ALTER TABLE general_upload ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: general_upload_status 
 */

ALTER TABLE general_upload_status ADD 
    FOREIGN KEY (idgenupl)
    REFERENCES general_upload(idgenupl)
go


/* 
 * TABLE: geo_boundary 
 */

ALTER TABLE geo_boundary ADD 
    FOREIGN KEY (idgeoboutype)
    REFERENCES geo_boundary_type(idgeoboutype)
go

ALTER TABLE geo_boundary ADD 
    FOREIGN KEY (idparent)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: gl_account 
 */

ALTER TABLE gl_account ADD 
    FOREIGN KEY (idglacctyp)
    REFERENCES gl_account_type(idglacctyp)
go


/* 
 * TABLE: gl_dimension 
 */

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (dimension1)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (dimension5)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (dimension4)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (dimension3)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (dimension2)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (dimension6)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (idprocat)
    REFERENCES product_category(idcategory)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (idparcat)
    REFERENCES party_category(idcategory)
go

ALTER TABLE gl_dimension ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: good 
 */

ALTER TABLE good ADD 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
go

ALTER TABLE good ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: good_container 
 */

ALTER TABLE good_container ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE good_container ADD 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE good_container ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: good_promotion 
 */

ALTER TABLE good_promotion ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: internal 
 */

ALTER TABLE internal ADD 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE internal ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE internal ADD 
    FOREIGN KEY (idparent)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal ADD 
    FOREIGN KEY (idroot)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: internal_acctg_trans 
 */

ALTER TABLE internal_acctg_trans ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal_acctg_trans ADD 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go


/* 
 * TABLE: internal_facility_purpose 
 */

ALTER TABLE internal_facility_purpose ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE internal_facility_purpose ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal_facility_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: internal_order 
 */

ALTER TABLE internal_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE internal_order ADD 
    FOREIGN KEY (idintfro)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal_order ADD 
    FOREIGN KEY (idintto)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: inventory_adjustment 
 */

ALTER TABLE inventory_adjustment ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE inventory_adjustment ADD 
    FOREIGN KEY (idstopnite)
    REFERENCES stock_opname_item(idstopnite)
go


/* 
 * TABLE: inventory_item 
 */

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
go


/* 
 * TABLE: inventory_item_status 
 */

ALTER TABLE inventory_item_status ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE inventory_item_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE inventory_item_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: inventory_movement_status 
 */

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: inventory_request_item 
 */

ALTER TABLE inventory_request_item ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go

ALTER TABLE inventory_request_item ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go

ALTER TABLE inventory_request_item ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go


/* 
 * TABLE: item_issuance 
 */

ALTER TABLE item_issuance ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go

ALTER TABLE item_issuance ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE item_issuance ADD 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
go


/* 
 * TABLE: job 
 */

ALTER TABLE job ADD 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go

ALTER TABLE job ADD 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go


/* 
 * TABLE: job_dispatch 
 */

ALTER TABLE job_dispatch ADD 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go

ALTER TABLE job_dispatch ADD 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE job_dispatch ADD 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go

ALTER TABLE job_dispatch ADD 
    FOREIGN KEY (iddispatchstatus)
    REFERENCES dispatch_status(iddispatchstatus)
go


/* 
 * TABLE: job_dispatch_customer_type 
 */

ALTER TABLE job_dispatch_customer_type ADD 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go

ALTER TABLE job_dispatch_customer_type ADD 
    FOREIGN KEY (idcostumertype)
    REFERENCES customer_type(idcostumertype)
go


/* 
 * TABLE: job_dispatch_group_service 
 */

ALTER TABLE job_dispatch_group_service ADD 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go


/* 
 * TABLE: job_dispatch_pkb_type 
 */

ALTER TABLE job_dispatch_pkb_type ADD 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
go

ALTER TABLE job_dispatch_pkb_type ADD 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go


/* 
 * TABLE: job_dispatch_service 
 */

ALTER TABLE job_dispatch_service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE job_dispatch_service ADD 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go


/* 
 * TABLE: job_history 
 */

ALTER TABLE job_history ADD 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE job_history ADD 
    FOREIGN KEY (idpendingreason)
    REFERENCES pendingreason(idpendingreason)
go

ALTER TABLE job_history ADD 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
go


/* 
 * TABLE: job_priority 
 */

ALTER TABLE job_priority ADD 
    FOREIGN KEY (idjobprioritytype)
    REFERENCES job_priority_type(idjobprioritytype)
go


/* 
 * TABLE: job_priority_customer_type 
 */

ALTER TABLE job_priority_customer_type ADD 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go

ALTER TABLE job_priority_customer_type ADD 
    FOREIGN KEY (idcostumertype)
    REFERENCES customer_type(idcostumertype)
go


/* 
 * TABLE: job_priority_group_service 
 */

ALTER TABLE job_priority_group_service ADD 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go


/* 
 * TABLE: job_priority_pkb_type 
 */

ALTER TABLE job_priority_pkb_type ADD 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
go

ALTER TABLE job_priority_pkb_type ADD 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go


/* 
 * TABLE: job_priority_service 
 */

ALTER TABLE job_priority_service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE job_priority_service ADD 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go


/* 
 * TABLE: job_service 
 */

ALTER TABLE job_service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go

ALTER TABLE job_service ADD 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
go


/* 
 * TABLE: job_suggest 
 */

ALTER TABLE job_suggest ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: job_suggest_service 
 */

ALTER TABLE job_suggest_service ADD 
    FOREIGN KEY (idjobsuggest)
    REFERENCES job_suggest(idjobsuggest)
go

ALTER TABLE job_suggest_service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: leasing_company 
 */

ALTER TABLE leasing_company ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go


/* 
 * TABLE: leasing_tenor_provide 
 */

ALTER TABLE leasing_tenor_provide ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE leasing_tenor_provide ADD 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
go


/* 
 * TABLE: market_treatment 
 */

ALTER TABLE market_treatment ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE market_treatment ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: market_treatment_status 
 */

ALTER TABLE market_treatment_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE market_treatment_status ADD 
    FOREIGN KEY (idmarkettreatment)
    REFERENCES market_treatment(idmarkettreatment)
go


/* 
 * TABLE: mechanic 
 */

ALTER TABLE mechanic ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE mechanic ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: memo 
 */

ALTER TABLE memo ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE memo ADD 
    FOREIGN KEY (idmemotype)
    REFERENCES memo_type(idmemotype)
go

ALTER TABLE memo ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE memo ADD 
    FOREIGN KEY (idoldinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE memo ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: memo_status 
 */

ALTER TABLE memo_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE memo_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE memo_status ADD 
    FOREIGN KEY (idmemo)
    REFERENCES memo(idmemo)
go


/* 
 * TABLE: motor 
 */

ALTER TABLE motor ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: motor_due_reminder 
 */

ALTER TABLE motor_due_reminder ADD 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
go


/* 
 * TABLE: moving_slip 
 */

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idinviteto)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idcontainerfrom)
    REFERENCES container(idcontainer)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idcontainerto)
    REFERENCES container(idcontainer)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: order_billing_item 
 */

ALTER TABLE order_billing_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE order_billing_item ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go


/* 
 * TABLE: order_demand 
 */

ALTER TABLE order_demand ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE order_demand ADD 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
go


/* 
 * TABLE: order_item 
 */

ALTER TABLE order_item ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE order_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE order_item ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE order_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE order_item ADD 
    FOREIGN KEY (idparordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: order_payment 
 */

ALTER TABLE order_payment ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE order_payment ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: order_request_item 
 */

ALTER TABLE order_request_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE order_request_item ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: order_request_status 
 */

ALTER TABLE order_request_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE order_request_status ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: order_shipment_item 
 */

ALTER TABLE order_shipment_item ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go

ALTER TABLE order_shipment_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: orders 
 */

ALTER TABLE orders ADD 
    FOREIGN KEY (idordtyp)
    REFERENCES order_type(idordtyp)
go


/* 
 * TABLE: orders_role 
 */

ALTER TABLE orders_role ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE orders_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE orders_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: orders_status 
 */

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: organization 
 */

ALTER TABLE organization ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE organization ADD 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
go

ALTER TABLE organization ADD 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
go


/* 
 * TABLE: organization_customer 
 */

ALTER TABLE organization_customer ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: organization_gl_account 
 */

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idparent)
    REFERENCES organization_gl_account(idorgglacc)
go

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
go

ALTER TABLE organization_gl_account ADD 
    FOREIGN KEY (idglacc)
    REFERENCES gl_account(idglacc)
go


/* 
 * TABLE: organization_prospect 
 */

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
go

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
go


/* 
 * TABLE: organization_prospect_detail 
 */

ALTER TABLE organization_prospect_detail ADD 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
go

ALTER TABLE organization_prospect_detail ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE organization_prospect_detail ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE organization_prospect_detail ADD 
    FOREIGN KEY (idprospect)
    REFERENCES organization_prospect(idprospect)
go


/* 
 * TABLE: organization_suspect 
 */

ALTER TABLE organization_suspect ADD 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE organization_suspect ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: package_receipt 
 */

ALTER TABLE package_receipt ADD 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE package_receipt ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go


/* 
 * TABLE: packaging_content 
 */

ALTER TABLE packaging_content ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go

ALTER TABLE packaging_content ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go


/* 
 * TABLE: parent_organization 
 */

ALTER TABLE parent_organization ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: part_history 
 */

ALTER TABLE part_history ADD 
    FOREIGN KEY (idvehicleservicehistory)
    REFERENCES vehicle_service_history(idvehicleservicehistory)
go

ALTER TABLE part_history ADD 
    FOREIGN KEY (idservicehistory)
    REFERENCES service_history(idservicehistory)
go


/* 
 * TABLE: part_issue 
 */

ALTER TABLE part_issue ADD 
    FOREIGN KEY (idpartissuetype)
    REFERENCES part_issue_type(idpartissuetype)
go


/* 
 * TABLE: part_issue_action 
 */

ALTER TABLE part_issue_action ADD 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go


/* 
 * TABLE: part_issue_item 
 */

ALTER TABLE part_issue_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE part_issue_item ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE part_issue_item ADD 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go


/* 
 * TABLE: part_issue_status 
 */

ALTER TABLE part_issue_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE part_issue_status ADD 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go


/* 
 * TABLE: part_sales_order 
 */

ALTER TABLE part_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
go


/* 
 * TABLE: party_category 
 */

ALTER TABLE party_category ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES party_category_type(idcattyp)
go


/* 
 * TABLE: party_category_impl 
 */

ALTER TABLE party_category_impl ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_category_impl ADD 
    FOREIGN KEY (idcategory)
    REFERENCES party_category(idcategory)
go


/* 
 * TABLE: party_category_type 
 */

ALTER TABLE party_category_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go

ALTER TABLE party_category_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES party_category_type(idcattyp)
go


/* 
 * TABLE: party_classification 
 */

ALTER TABLE party_classification ADD 
    FOREIGN KEY (idcategory)
    REFERENCES party_category(idcategory)
go

ALTER TABLE party_classification ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES party_category_type(idcattyp)
go

ALTER TABLE party_classification ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE party_classification ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_contact 
 */

ALTER TABLE party_contact ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE party_contact ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_contact_mechanism 
 */

ALTER TABLE party_contact_mechanism ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE party_contact_mechanism ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_contact_mechanism ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: party_document 
 */

ALTER TABLE party_document ADD 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
go

ALTER TABLE party_document ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_facility_purpose 
 */

ALTER TABLE party_facility_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go

ALTER TABLE party_facility_purpose ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE party_facility_purpose ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_relationship 
 */

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
go

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idparrolto)
    REFERENCES party_role(idparrol)
go

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idparrolfro)
    REFERENCES party_role(idparrol)
go


/* 
 * TABLE: party_role 
 */

ALTER TABLE party_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: party_role_status 
 */

ALTER TABLE party_role_status ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE party_role_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE party_role_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: party_role_type 
 */

ALTER TABLE party_role_type ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE party_role_type ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: payment 
 */

ALTER TABLE payment ADD 
    FOREIGN KEY (idpaytyp)
    REFERENCES payment_type(idpaytyp)
go

ALTER TABLE payment ADD 
    FOREIGN KEY (idpaymet)
    REFERENCES payment_method(idpaymet)
go

ALTER TABLE payment ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE payment ADD 
    FOREIGN KEY (idpaito)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE payment ADD 
    FOREIGN KEY (idpaifro)
    REFERENCES bill_to(idbillto)
go


/* 
 * TABLE: payment_acctg_trans 
 */

ALTER TABLE payment_acctg_trans ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE payment_acctg_trans ADD 
    FOREIGN KEY (idacctra)
    REFERENCES external_acctg_trans(idacctra)
go


/* 
 * TABLE: payment_application 
 */

ALTER TABLE payment_application ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE payment_application ADD 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
go

ALTER TABLE payment_application ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: payment_method 
 */

ALTER TABLE payment_method ADD 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
go


/* 
 * TABLE: payment_role 
 */

ALTER TABLE payment_role ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE payment_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE payment_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: payment_status 
 */

ALTER TABLE payment_status ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE payment_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE payment_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: person 
 */

ALTER TABLE person ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: person_prospect 
 */

ALTER TABLE person_prospect ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE person_prospect ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go


/* 
 * TABLE: person_suspect 
 */

ALTER TABLE person_suspect ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE person_suspect ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: personal_customer 
 */

ALTER TABLE personal_customer ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: personsosmed 
 */

ALTER TABLE personsosmed ADD 
    FOREIGN KEY (idsosmedtype)
    REFERENCES sosmedtype(idsosmedtype)
go

ALTER TABLE personsosmed ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go


/* 
 * TABLE: picking_slip 
 */

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: pkb 
 */

ALTER TABLE pkb ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idqueue)
    REFERENCES queue(idqueue)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idstatuspkbcust)
    REFERENCES status_pkb(idstatuspkb)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idstatuspkbinternal)
    REFERENCES status_pkb(idstatuspkb)
go

ALTER TABLE pkb ADD 
    FOREIGN KEY (idpkbjobreturn)
    REFERENCES pkb(idpkb)
go


/* 
 * TABLE: pkb_estimation 
 */

ALTER TABLE pkb_estimation ADD 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go


/* 
 * TABLE: pkb_part 
 */

ALTER TABLE pkb_part ADD 
    FOREIGN KEY (idchargeto)
    REFERENCES charge_to_type(idchargeto)
go

ALTER TABLE pkb_part ADD 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go

ALTER TABLE pkb_part ADD 
    FOREIGN KEY (idpkbservice)
    REFERENCES pkb_service(IdPkbService)
go


/* 
 * TABLE: pkb_part_billing_item 
 */

ALTER TABLE pkb_part_billing_item ADD 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE pkb_part_billing_item ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go


/* 
 * TABLE: pkb_part_request_item 
 */

ALTER TABLE pkb_part_request_item ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: pkb_service 
 */

ALTER TABLE pkb_service ADD 
    FOREIGN KEY (idchargeto)
    REFERENCES charge_to_type(idchargeto)
go

ALTER TABLE pkb_service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go

ALTER TABLE pkb_service ADD 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go


/* 
 * TABLE: pkb_service_billing_item 
 */

ALTER TABLE pkb_service_billing_item ADD 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE pkb_service_billing_item ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go


/* 
 * TABLE: po_detail_part 
 */

ALTER TABLE po_detail_part ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: po_detail_service 
 */

ALTER TABLE po_detail_service ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: po_status 
 */

ALTER TABLE po_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: position_authority 
 */

ALTER TABLE position_authority ADD 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
go


/* 
 * TABLE: position_fullfillment 
 */

ALTER TABLE position_fullfillment ADD 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
go

ALTER TABLE position_fullfillment ADD 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
go


/* 
 * TABLE: position_reporting_structure 
 */

ALTER TABLE position_reporting_structure ADD 
    FOREIGN KEY (idposfro)
    REFERENCES positions(idposition)
go

ALTER TABLE position_reporting_structure ADD 
    FOREIGN KEY (idposto)
    REFERENCES positions(idposition)
go


/* 
 * TABLE: positions 
 */

ALTER TABLE positions ADD 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
go

ALTER TABLE positions ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE positions ADD 
    FOREIGN KEY (idorganization)
    REFERENCES organization(idparty)
go

ALTER TABLE positions ADD 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
go


/* 
 * TABLE: positions_status 
 */

ALTER TABLE positions_status ADD 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
go

ALTER TABLE positions_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE positions_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: postal_address 
 */

ALTER TABLE postal_address ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE postal_address ADD 
    FOREIGN KEY (idprovince)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD 
    FOREIGN KEY (idcity)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD 
    FOREIGN KEY (iddistrict)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD 
    FOREIGN KEY (idpostal)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD 
    FOREIGN KEY (idvillage)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: price_agreement_item 
 */

ALTER TABLE price_agreement_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE price_agreement_item ADD 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
go


/* 
 * TABLE: price_component 
 */

ALTER TABLE price_component ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE price_component ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE price_component ADD 
    FOREIGN KEY (idpricetype)
    REFERENCES price_type(idpricetype)
go

ALTER TABLE price_component ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE price_component ADD 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
go

ALTER TABLE price_component ADD 
    FOREIGN KEY (idparcat)
    REFERENCES party_category(idcategory)
go

ALTER TABLE price_component ADD 
    FOREIGN KEY (idprocat)
    REFERENCES product_category(idcategory)
go


/* 
 * TABLE: price_type 
 */

ALTER TABLE price_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: product 
 */

ALTER TABLE product ADD 
    FOREIGN KEY (idprotyp)
    REFERENCES product_type(idprotyp)
go


/* 
 * TABLE: product_association 
 */

ALTER TABLE product_association ADD 
    FOREIGN KEY (idproductfrom)
    REFERENCES product(idproduct)
go

ALTER TABLE product_association ADD 
    FOREIGN KEY (idproductto)
    REFERENCES product(idproduct)
go

ALTER TABLE product_association ADD 
    FOREIGN KEY (idasstyp)
    REFERENCES association_type(idasstyp)
go


/* 
 * TABLE: product_category 
 */

ALTER TABLE product_category ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES product_category_type(idcattyp)
go


/* 
 * TABLE: product_category_impl 
 */

ALTER TABLE product_category_impl ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE product_category_impl ADD 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
go


/* 
 * TABLE: product_category_type 
 */

ALTER TABLE product_category_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go

ALTER TABLE product_category_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES product_category_type(idcattyp)
go


/* 
 * TABLE: product_classification 
 */

ALTER TABLE product_classification ADD 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
go

ALTER TABLE product_classification ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES product_category_type(idcattyp)
go

ALTER TABLE product_classification ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE product_classification ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_document 
 */

ALTER TABLE product_document ADD 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
go

ALTER TABLE product_document ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_feature_impl 
 */

ALTER TABLE product_feature_impl ADD 
    FOREIGN KEY (products_id)
    REFERENCES product(idproduct)
go

ALTER TABLE product_feature_impl ADD 
    FOREIGN KEY (features_id)
    REFERENCES feature(idfeature)
go


/* 
 * TABLE: product_package_receipt 
 */

ALTER TABLE product_package_receipt ADD 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE product_package_receipt ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go


/* 
 * TABLE: product_purchase_order 
 */

ALTER TABLE product_purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
go


/* 
 * TABLE: product_shipment_incoming 
 */

ALTER TABLE product_shipment_incoming ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go


/* 
 * TABLE: product_shipment_receipt 
 */

ALTER TABLE product_shipment_receipt ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: prospect 
 */

ALTER TABLE prospect ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idprosou)
    REFERENCES prospect_source(idprosou)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idbroker)
    REFERENCES sales_broker(idparrol)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
go


/* 
 * TABLE: prospect_role 
 */

ALTER TABLE prospect_role ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE prospect_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE prospect_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: prospect_status 
 */

ALTER TABLE prospect_status ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE prospect_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE prospect_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: province 
 */

ALTER TABLE province ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: purchase_order 
 */

ALTER TABLE purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
go


/* 
 * TABLE: purchase_request 
 */

ALTER TABLE purchase_request ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE purchase_request ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE purchase_request ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: purchaseorder_reception_tr 
 */

ALTER TABLE purchaseorder_reception_tr ADD 
    FOREIGN KEY (idpurchaseorder)
    REFERENCES purchaseorder_tm(idpurchaseorder)
go

ALTER TABLE purchaseorder_reception_tr ADD 
    FOREIGN KEY (idcategoryasset)
    REFERENCES category_asset_tp(idcategoryasset)
go


/* 
 * TABLE: purchaseorder_tm 
 */

ALTER TABLE purchaseorder_tm ADD 
    FOREIGN KEY (status)
    REFERENCES purchaseorder_status_tp(idpurchaseorderstatus)
go


/* 
 * TABLE: queue 
 */

ALTER TABLE queue ADD 
    FOREIGN KEY (idqueuestatus)
    REFERENCES queue_status(idqueuestatus)
go

ALTER TABLE queue ADD 
    FOREIGN KEY (idqueuetype)
    REFERENCES queue_type(idqueuetype)
go

ALTER TABLE queue ADD 
    FOREIGN KEY (idbooking)
    REFERENCES booking(idbooking)
go


/* 
 * TABLE: quote_item 
 */

ALTER TABLE quote_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE quote_item ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go


/* 
 * TABLE: quote_role 
 */

ALTER TABLE quote_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE quote_role ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go

ALTER TABLE quote_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: quote_status 
 */

ALTER TABLE quote_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE quote_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE quote_status ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go


/* 
 * TABLE: receipt 
 */

ALTER TABLE receipt ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE receipt ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: regular_sales_order 
 */

ALTER TABLE regular_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
go


/* 
 * TABLE: reimbursement_part 
 */

ALTER TABLE reimbursement_part ADD 
    FOREIGN KEY (idspgclaimdetail)
    REFERENCES spg_claim_detail(idspgclaimdetail)
go

ALTER TABLE reimbursement_part ADD 
    FOREIGN KEY (idspgclaimaction)
    REFERENCES spg_claim_action(idspgclaimaction)
go

ALTER TABLE reimbursement_part ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: rem_part 
 */

ALTER TABLE rem_part ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: reminder_sent_status 
 */

ALTER TABLE reminder_sent_status ADD 
    FOREIGN KEY (idreminder)
    REFERENCES service_reminder(idreminder)
go


/* 
 * TABLE: request 
 */

ALTER TABLE request ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE request ADD 
    FOREIGN KEY (idreqtype)
    REFERENCES request_type(idreqtype)
go


/* 
 * TABLE: request_item 
 */

ALTER TABLE request_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE request_item ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE request_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go


/* 
 * TABLE: request_product 
 */

ALTER TABLE request_product ADD 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
go

ALTER TABLE request_product ADD 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
go

ALTER TABLE request_product ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_requirement 
 */

ALTER TABLE request_requirement ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE request_requirement ADD 
    FOREIGN KEY (idrequirement)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: request_role 
 */

ALTER TABLE request_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE request_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE request_role ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_status 
 */

ALTER TABLE request_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE request_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE request_status ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_unit_internal 
 */

ALTER TABLE request_unit_internal ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: requirement 
 */

ALTER TABLE requirement ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE requirement ADD 
    FOREIGN KEY (idparentreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement ADD 
    FOREIGN KEY (idreqtyp)
    REFERENCES requirement_type(idreqtyp)
go


/* 
 * TABLE: requirement_order_item 
 */

ALTER TABLE requirement_order_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE requirement_order_item ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: requirement_payment 
 */

ALTER TABLE requirement_payment ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement_payment ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: requirement_role 
 */

ALTER TABLE requirement_role ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE requirement_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: requirement_status 
 */

ALTER TABLE requirement_status ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE requirement_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: return_purchase_order 
 */

ALTER TABLE return_purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
go


/* 
 * TABLE: return_sales_order 
 */

ALTER TABLE return_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
go


/* 
 * TABLE: room_tm 
 */

ALTER TABLE room_tm ADD 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
go


/* 
 * TABLE: rule_hot_item 
 */

ALTER TABLE rule_hot_item ADD 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
go

ALTER TABLE rule_hot_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: rule_indent 
 */

ALTER TABLE rule_indent ADD 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
go

ALTER TABLE rule_indent ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: rule_sales_discount 
 */

ALTER TABLE rule_sales_discount ADD 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
go


/* 
 * TABLE: rules 
 */

ALTER TABLE rules ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go

ALTER TABLE rules ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: sale_type 
 */

ALTER TABLE sale_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: sales_agreement 
 */

ALTER TABLE sales_agreement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE sales_agreement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idcustomer)
go

ALTER TABLE sales_agreement ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go


/* 
 * TABLE: sales_booking 
 */

ALTER TABLE sales_booking ADD 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
go

ALTER TABLE sales_booking ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE sales_booking ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE sales_booking ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: sales_broker 
 */

ALTER TABLE sales_broker ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE sales_broker ADD 
    FOREIGN KEY (idbrotyp)
    REFERENCES broker_type(idbrotyp)
go


/* 
 * TABLE: sales_order 
 */

ALTER TABLE sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
go

ALTER TABLE sales_order ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: sales_point 
 */

ALTER TABLE sales_point ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: sales_unit_leasing 
 */

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
go

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
go

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idorder)
    REFERENCES vehicle_sales_order(idorder)
go

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
go


/* 
 * TABLE: sales_unit_requirement 
 */

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
go

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idrequest)
    REFERENCES vehicle_customer_request(idrequest)
go


/* 
 * TABLE: salesman 
 */

ALTER TABLE salesman ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE salesman ADD 
    FOREIGN KEY (idcoordinator)
    REFERENCES salesman(idparrol)
go


/* 
 * TABLE: service 
 */

ALTER TABLE service ADD 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
go

ALTER TABLE service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: service_agreement 
 */

ALTER TABLE service_agreement ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE service_agreement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE service_agreement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idcustomer)
go


/* 
 * TABLE: service_history 
 */

ALTER TABLE service_history ADD 
    FOREIGN KEY (idvehicleservicehistory)
    REFERENCES vehicle_service_history(idvehicleservicehistory)
go


/* 
 * TABLE: service_kpb 
 */

ALTER TABLE service_kpb ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: service_per_motor 
 */

ALTER TABLE service_per_motor ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: service_reminder 
 */

ALTER TABLE service_reminder ADD 
    FOREIGN KEY (idclaimtype)
    REFERENCES dealer_claim_type(idclaimtype)
go

ALTER TABLE service_reminder ADD 
    FOREIGN KEY (idremindertype)
    REFERENCES dealer_reminder_type(idremindertype)
go

ALTER TABLE service_reminder ADD 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go


/* 
 * TABLE: service_reminder_event 
 */

ALTER TABLE service_reminder_event ADD 
    FOREIGN KEY (idreminder)
    REFERENCES service_reminder(idreminder)
go

ALTER TABLE service_reminder_event ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go


/* 
 * TABLE: ship_to 
 */

ALTER TABLE ship_to ADD 
    FOREIGN KEY (idparty, idroletype)
    REFERENCES party_role_type(idparty, idroletype)
go


/* 
 * TABLE: shipment 
 */

ALTER TABLE shipment ADD 
    FOREIGN KEY (idshityp)
    REFERENCES shipment_type(idshityp)
go

ALTER TABLE shipment ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE shipment ADD 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE shipment ADD 
    FOREIGN KEY (idshito)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE shipment ADD 
    FOREIGN KEY (idposaddfro)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE shipment ADD 
    FOREIGN KEY (idposaddto)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE shipment ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: shipment_billing_item 
 */

ALTER TABLE shipment_billing_item ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE shipment_billing_item ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go


/* 
 * TABLE: shipment_incoming 
 */

ALTER TABLE shipment_incoming ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go


/* 
 * TABLE: shipment_item 
 */

ALTER TABLE shipment_item ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go

ALTER TABLE shipment_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE shipment_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: shipment_outgoing 
 */

ALTER TABLE shipment_outgoing ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go

ALTER TABLE shipment_outgoing ADD 
    FOREIGN KEY (iddriver)
    REFERENCES driver(idparrol)
go


/* 
 * TABLE: shipment_package 
 */

ALTER TABLE shipment_package ADD 
    FOREIGN KEY (idshipactyp)
    REFERENCES shipment_package_type(idshipactyp)
go

ALTER TABLE shipment_package ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: shipment_package_role 
 */

ALTER TABLE shipment_package_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE shipment_package_role ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE shipment_package_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: shipment_package_status 
 */

ALTER TABLE shipment_package_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE shipment_package_status ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE shipment_package_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: shipment_receipt 
 */

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go


/* 
 * TABLE: shipment_receipt_role 
 */

ALTER TABLE shipment_receipt_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE shipment_receipt_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE shipment_receipt_role ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: shipment_receipt_status 
 */

ALTER TABLE shipment_receipt_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE shipment_receipt_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE shipment_receipt_status ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: shipment_status 
 */

ALTER TABLE shipment_status ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go

ALTER TABLE shipment_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE shipment_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: spg_claim_detail 
 */

ALTER TABLE spg_claim_detail ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE spg_claim_detail ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go

ALTER TABLE spg_claim_detail ADD 
    FOREIGN KEY (idspgclaim)
    REFERENCES spg_claim(idspgclaim)
go

ALTER TABLE spg_claim_detail ADD 
    FOREIGN KEY (idspgclaimtype)
    REFERENCES spg_claim_type(idspgclaimtype)
go

ALTER TABLE spg_claim_detail ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE spg_claim_detail ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go


/* 
 * TABLE: standard_calendar 
 */

ALTER TABLE standard_calendar ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE standard_calendar ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
go


/* 
 * TABLE: status_pkb 
 */

ALTER TABLE status_pkb ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: stock_opname 
 */

ALTER TABLE stock_opname ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE stock_opname ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES standard_calendar(idcalendar)
go

ALTER TABLE stock_opname ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE stock_opname ADD 
    FOREIGN KEY (idstkopntyp)
    REFERENCES stock_opname_type(idstkopntyp)
go


/* 
 * TABLE: stock_opname_inventory 
 */

ALTER TABLE stock_opname_inventory ADD 
    FOREIGN KEY (idstopnite)
    REFERENCES stock_opname_item(idstopnite)
go

ALTER TABLE stock_opname_inventory ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go


/* 
 * TABLE: stock_opname_item 
 */

ALTER TABLE stock_opname_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE stock_opname_item ADD 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE stock_opname_item ADD 
    FOREIGN KEY (idstkop)
    REFERENCES stock_opname(idstkop)
go


/* 
 * TABLE: stock_opname_status 
 */

ALTER TABLE stock_opname_status ADD 
    FOREIGN KEY (idstkop)
    REFERENCES stock_opname(idstkop)
go


/* 
 * TABLE: stockopname_item_tm 
 */

ALTER TABLE stockopname_item_tm ADD 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
go

ALTER TABLE stockopname_item_tm ADD 
    FOREIGN KEY (idroom)
    REFERENCES room_tm(idroom)
go

ALTER TABLE stockopname_item_tm ADD 
    FOREIGN KEY (idstockopname)
    REFERENCES stockopname_tm(idstockopname)
go

ALTER TABLE stockopname_item_tm ADD 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
go


/* 
 * TABLE: stockopname_tm 
 */

ALTER TABLE stockopname_tm ADD 
    FOREIGN KEY (status)
    REFERENCES stockopname_status_tp(idstockopnamestatus)
go


/* 
 * TABLE: suggest_part 
 */

ALTER TABLE suggest_part ADD 
    FOREIGN KEY (idproductmotor)
    REFERENCES motor(idproduct)
go

ALTER TABLE suggest_part ADD 
    FOREIGN KEY (idproductpart)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: summary 
 */

ALTER TABLE summary ADD 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
go


/* 
 * TABLE: suspect 
 */

ALTER TABLE suspect ADD 
    FOREIGN KEY (iddealer)
    REFERENCES internal(idinternal)
go

ALTER TABLE suspect ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE suspect ADD 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
go

ALTER TABLE suspect ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE suspect ADD 
    FOREIGN KEY (idaddress)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE suspect ADD 
    FOREIGN KEY (idsuspecttyp)
    REFERENCES suspect_type(idsuspecttyp)
go


/* 
 * TABLE: suspect_role 
 */

ALTER TABLE suspect_role ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go

ALTER TABLE suspect_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE suspect_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: suspect_status 
 */

ALTER TABLE suspect_status ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go

ALTER TABLE suspect_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE suspect_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: telecomunication_number 
 */

ALTER TABLE telecomunication_number ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go


/* 
 * TABLE: unit_accesories_mapper 
 */

ALTER TABLE unit_accesories_mapper ADD 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
go


/* 
 * TABLE: unit_deliverable 
 */

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_document_requirement(idreq)
go

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (idvndstatyp)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (idconstatyp)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (iddeltype)
    REFERENCES deliverable_type(iddeltype)
go


/* 
 * TABLE: unit_document_message 
 */

ALTER TABLE unit_document_message ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE unit_document_message ADD 
    FOREIGN KEY (idparent)
    REFERENCES unit_document_message(idmessage)
go

ALTER TABLE unit_document_message ADD 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
go


/* 
 * TABLE: unit_preparation 
 */

ALTER TABLE unit_preparation ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE unit_preparation ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE unit_preparation ADD 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
go


/* 
 * TABLE: unit_requirement 
 */

ALTER TABLE unit_requirement ADD 
    FOREIGN KEY (idbranch)
    REFERENCES branch(idinternal)
go

ALTER TABLE unit_requirement ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE unit_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: unit_shipment_receipt 
 */

ALTER TABLE unit_shipment_receipt ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: uom 
 */

ALTER TABLE uom ADD 
    FOREIGN KEY (iduomtyp)
    REFERENCES uom_type(iduomtyp)
go


/* 
 * TABLE: uom_conversion 
 */

ALTER TABLE uom_conversion ADD 
    FOREIGN KEY (iduomto)
    REFERENCES uom(iduom)
go

ALTER TABLE uom_conversion ADD 
    FOREIGN KEY (iduomfro)
    REFERENCES uom(iduom)
go


/* 
 * TABLE: user_mediator 
 */

ALTER TABLE user_mediator ADD 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
go

ALTER TABLE user_mediator ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: user_mediator_role 
 */

ALTER TABLE user_mediator_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE user_mediator_role ADD 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
go

ALTER TABLE user_mediator_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: user_mediator_status 
 */

ALTER TABLE user_mediator_status ADD 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
go

ALTER TABLE user_mediator_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE user_mediator_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: vehicle 
 */

ALTER TABLE vehicle ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE vehicle ADD 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
go

ALTER TABLE vehicle ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: vehicle_carrier 
 */

ALTER TABLE vehicle_carrier ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE vehicle_carrier ADD 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
go

ALTER TABLE vehicle_carrier ADD 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go

ALTER TABLE vehicle_carrier ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go


/* 
 * TABLE: vehicle_customer_request 
 */

ALTER TABLE vehicle_customer_request ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_customer_request ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE vehicle_customer_request ADD 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
go


/* 
 * TABLE: vehicle_document_requirement 
 */

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idslsreq)
    REFERENCES sales_unit_requirement(idreq)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idvehregtyp)
    REFERENCES vehicle_registration_type(idvehregtyp)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
go


/* 
 * TABLE: vehicle_identification 
 */

ALTER TABLE vehicle_identification ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_identification ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: vehicle_purchase_order 
 */

ALTER TABLE vehicle_purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
go


/* 
 * TABLE: vehicle_sales_billing 
 */

ALTER TABLE vehicle_sales_billing ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_sales_billing ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing_receipt(idbilling)
go

ALTER TABLE vehicle_sales_billing ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: vehicle_sales_order 
 */

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
go

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idunitreq)
    REFERENCES sales_unit_requirement(idreq)
go

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
go

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go


/* 
 * TABLE: vehicle_service_history 
 */

ALTER TABLE vehicle_service_history ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: vehicle_work_requirement 
 */

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
go

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idmechanic)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: vendor 
 */

ALTER TABLE vendor ADD 
    FOREIGN KEY (idvndtyp)
    REFERENCES vendor_type(idvndtyp)
go

ALTER TABLE vendor ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE vendor ADD 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go


/* 
 * TABLE: vendor_order 
 */

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go


/* 
 * TABLE: vendor_product 
 */

ALTER TABLE vendor_product ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_product ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE vendor_product ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: vendor_relationship 
 */

ALTER TABLE vendor_relationship ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_relationship ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE vendor_relationship ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go


/* 
 * TABLE: village 
 */

ALTER TABLE village ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: we_service_type 
 */

ALTER TABLE we_service_type ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go

ALTER TABLE we_service_type ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: we_type_good_standard 
 */

ALTER TABLE we_type_good_standard ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go

ALTER TABLE we_type_good_standard ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: work_effort 
 */

ALTER TABLE work_effort ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE work_effort ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go


/* 
 * TABLE: work_effort_payment 
 */

ALTER TABLE work_effort_payment ADD 
    FOREIGN KEY (work_efforts_id)
    REFERENCES work_effort(idwe)
go

ALTER TABLE work_effort_payment ADD 
    FOREIGN KEY (payments_id)
    REFERENCES payment_application(idpayapp)
go


/* 
 * TABLE: work_effort_role 
 */

ALTER TABLE work_effort_role ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
go

ALTER TABLE work_effort_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE work_effort_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: work_effort_status 
 */

ALTER TABLE work_effort_status ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
go

ALTER TABLE work_effort_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE work_effort_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: work_order 
 */

ALTER TABLE work_order ADD 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_work_requirement(idreq)
go


/* 
 * TABLE: work_order_booking 
 */

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idbooslo)
    REFERENCES booking_slot(idbooslo)
go

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
go

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
go

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES personal_customer(idcustomer)
go

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: work_order_booking_role 
 */

ALTER TABLE work_order_booking_role ADD 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
go

ALTER TABLE work_order_booking_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE work_order_booking_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: work_order_booking_status 
 */

ALTER TABLE work_order_booking_status ADD 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
go

ALTER TABLE work_order_booking_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE work_order_booking_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go


/* 
 * TABLE: work_product_req 
 */

ALTER TABLE work_product_req ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: work_requirement 
 */

ALTER TABLE work_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: work_requirement_services 
 */

ALTER TABLE work_requirement_services ADD 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
go

ALTER TABLE work_requirement_services ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_service_requirement(idwe)
go


/* 
 * TABLE: work_service_requirement 
 */

ALTER TABLE work_service_requirement ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
go


