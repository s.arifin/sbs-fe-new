--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      mpm-model.DM1
--
-- Date Created : Thursday, July 19, 2018 20:14:45
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: accounting_calendar 
--

CREATE TABLE accounting_calendar(
    idinternal    VARCHAR(30),
    idcalendar    INT            NOT NULL,
    PRIMARY KEY (idcalendar)
)ENGINE=INNODB
;



-- 
-- TABLE: acctg_trans_detail 
--

CREATE TABLE acctg_trans_detail(
    idtradet      INT               AUTO_INCREMENT,
    idacctra      INT,
    idglacc       BINARY(16),
    idinternal    VARCHAR(30),
    idcalendar    INT,
    amount        DECIMAL(18, 4),
    dbcrflag      VARCHAR(1),
    PRIMARY KEY (idtradet)
)ENGINE=INNODB
;



-- 
-- TABLE: acctg_trans_type 
--

CREATE TABLE acctg_trans_type(
    idacctratyp    INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idacctratyp)
)ENGINE=INNODB
;



-- 
-- TABLE: acctg_transaction 
--

CREATE TABLE acctg_transaction(
    idacctra         INT            AUTO_INCREMENT,
    idacctratyp      INT,
    dttransaction    DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtentry          DATETIME       DEFAULT CURRENT_TIMESTAMP,
    description      VARCHAR(60),
    PRIMARY KEY (idacctra)
)ENGINE=INNODB
;



-- 
-- TABLE: acctg_transaction_status 
--

CREATE TABLE acctg_transaction_status(
    idstatus        BINARY(16)      NOT NULL,
    idacctra        INT,
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: agreement 
--

CREATE TABLE agreement(
    idagreement    BINARY(16)     NOT NULL,
    idagrtyp       INT,
    description    VARCHAR(60),
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idagreement)
)ENGINE=INNODB
;



-- 
-- TABLE: agreement_item 
--

CREATE TABLE agreement_item(
    idagrite       BINARY(16)    NOT NULL,
    idagreement    BINARY(16),
    PRIMARY KEY (idagrite)
)ENGINE=INNODB
;



-- 
-- TABLE: agreement_motor_item 
--

CREATE TABLE agreement_motor_item(
    idagrite     BINARY(16)    NOT NULL,
    idvehicle    BINARY(16),
    PRIMARY KEY (idagrite)
)ENGINE=INNODB
;



-- 
-- TABLE: agreement_role 
--

CREATE TABLE agreement_role(
    idrole         BINARY(16)     NOT NULL,
    idagreement    BINARY(16),
    idroletype     INT,
    idparty        BINARY(16),
    username       VARCHAR(30),
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: agreement_status 
--

CREATE TABLE agreement_status(
    idstatus        BINARY(16)      NOT NULL,
    idagreement     BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: agreement_type 
--

CREATE TABLE agreement_type(
    idagrtyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idagrtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: approval 
--

CREATE TABLE approval(
    idapproval      BINARY(16)      NOT NULL,
    idapptyp        INT,
    idstatustype    INT,
    idreq           BINARY(16),
    idordite        BINARY(16),
    idmessage       INT,
    idleacom        BINARY(16),
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idapproval)
)ENGINE=INNODB
;



-- 
-- TABLE: approval_stockopname_participant_tm 
--

CREATE TABLE approval_stockopname_participant_tm(
    idapprovalstockopnameparticipant    INT         AUTO_INCREMENT,
    participantuserid                   INT,
    lastmodifieduserid                  INT,
    lastmodifieddate                    DATETIME,
    PRIMARY KEY (idapprovalstockopnameparticipant)
)ENGINE=INNODB
;



-- 
-- TABLE: approval_stockopname_status_tp 
--

CREATE TABLE approval_stockopname_status_tp(
    idapprovalstockopnamestatus    INT            NOT NULL,
    description                    VARCHAR(50),
    lastmodifieddate               DATETIME,
    lastmodifieduserid             INT,
    PRIMARY KEY (idapprovalstockopnamestatus)
)ENGINE=INNODB
;



-- 
-- TABLE: approval_stockopname_tm 
--

CREATE TABLE approval_stockopname_tm(
    idapprovalstockopname          INT             AUTO_INCREMENT,
    requestnumber                  VARCHAR(50),
    idstockopname                  INT,
    activityname                   VARCHAR(50),
    approvaldate                   DATETIME,
    idapprovaluser                 INT,
    requestdate                    DATETIME,
    requestuserid                  INT,
    idapprovalstockopnamestatus    INT,
    notes                          VARCHAR(200),
    lastmodifieddate               DATETIME,
    lastmodifieduserid             INT,
    PRIMARY KEY (idapprovalstockopname)
)ENGINE=INNODB
;



-- 
-- TABLE: approval_type 
--

CREATE TABLE approval_type(
    idapptyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idapptyp)
)ENGINE=INNODB
;



-- 
-- TABLE: association_type 
--

CREATE TABLE association_type(
    idasstyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idasstyp)
)ENGINE=INNODB
;



-- 
-- TABLE: attendance 
--

CREATE TABLE attendance(
    idattendance          INT            AUTO_INCREMENT,
    idparrol              BINARY(16),
    dateattendance        DATETIME,
    attendance            BIT(1),
    attendinfo            VARCHAR(20),
    availability          BIT(1),
    availinfo             VARCHAR(20),
    pdi                   BIT(1),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idattendance)
)ENGINE=INNODB
;



-- 
-- TABLE: ax_purchaseorder_item 
--

CREATE TABLE ax_purchaseorder_item(
    idpurchaseorderitem    INT               NOT NULL,
    ponumber               VARCHAR(30),
    podate                 DATETIME,
    itemname               VARCHAR(50),
    merk                   VARCHAR(30),
    type                   VARCHAR(30),
    branchcode             VARCHAR(20),
    typecode               VARCHAR(10),
    price                  DECIMAL(18, 2),
    quantity               INT,
    description            VARCHAR(100),
    PRIMARY KEY (idpurchaseorderitem)
)ENGINE=INNODB
;



-- 
-- TABLE: base_calendar 
--

CREATE TABLE base_calendar(
    idcalendar     INT            AUTO_INCREMENT,
    idparent       INT,
    idcaltyp       INT,
    description    VARCHAR(60),
    dtkey          DATE,
    workday        INT            DEFAULT 0,
    seqnum         INT,
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idcalendar)
)ENGINE=INNODB
;



-- 
-- TABLE: bill_to 
--

CREATE TABLE bill_to(
    idbillto      VARCHAR(30)    NOT NULL,
    idparty       BINARY(16),
    idroletype    INT,
    PRIMARY KEY (idbillto)
)ENGINE=INNODB
;



-- 
-- TABLE: billing 
--

CREATE TABLE billing(
    idbilling        BINARY(16)     NOT NULL,
    idbiltyp         INT,
    idinternal       VARCHAR(30),
    idbilto          VARCHAR(30),
    idbilfro         VARCHAR(30),
    billingnumber    VARCHAR(30),
    dtcreate         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtdue            DATETIME       DEFAULT CURRENT_TIMESTAMP,
    printcounter     INT,
    description      VARCHAR(60),
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_account 
--

CREATE TABLE billing_account(
    idbilacc       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idbilacc)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_acctg_trans 
--

CREATE TABLE billing_acctg_trans(
    idbilling    BINARY(16),
    idacctra     INT           NOT NULL,
    PRIMARY KEY (idacctra)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_disbursement 
--

CREATE TABLE billing_disbursement(
    idbilling        BINARY(16)     NOT NULL,
    idbilacc         INT,
    idvendor         VARCHAR(30),
    vendorinvoice    VARCHAR(30),
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_issuance 
--

CREATE TABLE billing_issuance(
    idslip      BINARY(16)        NOT NULL,
    idbilite    BINARY(16),
    idinvite    BINARY(16),
    idshipto    VARCHAR(30),
    qty         DECIMAL(14, 4),
    PRIMARY KEY (idslip)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_item 
--

CREATE TABLE billing_item(
    idbilite           BINARY(16)        NOT NULL,
    idbilling          BINARY(16),
    idinvite           BINARY(16),
    idproduct          VARCHAR(30),
    idwetyp            INT,
    idbilitetyp        INT,
    idfeature          INT,
    seq                INT,
    itemdescription    VARCHAR(60),
    qty                DECIMAL(14, 4),
    baseprice          DECIMAL(18, 4),
    unitprice          DECIMAL(18, 4),
    discount           DECIMAL(18, 4),
    taxamount          DECIMAL(18, 4),
    totalamount        DECIMAL(18, 4),
    PRIMARY KEY (idbilite)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_item_type 
--

CREATE TABLE billing_item_type(
    idbilitetyp    INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idbilitetyp)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_njb 
--

CREATE TABLE billing_njb(
    idbilling             BINARY(16)        NOT NULL,
    nonjb                 VARCHAR(30)       NOT NULL,
    totalbaseprice        DECIMAL(18, 2),
    totaltaxamount        DECIMAL(18, 2),
    mischarge             DECIMAL(18, 2),
    totalunitprice        DECIMAL(18, 2),
    totaldiscount         DECIMAL(18, 2),
    grandtotalamount      DECIMAL(18, 2),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_nsc 
--

CREATE TABLE billing_nsc(
    idbilling             BINARY(16)        NOT NULL,
    nonsc                 VARCHAR(30)       NOT NULL,
    totalbaseprice        DECIMAL(18, 2),
    totaltaxamount        DECIMAL(18, 2),
    mischarge             DECIMAL(18, 2),
    totalunitprice        DECIMAL(18, 2),
    totaldiscount         DECIMAL(18, 2),
    grandtotalamount      DECIMAL(18, 2),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_receipt 
--

CREATE TABLE billing_receipt(
    idbilling         BINARY(16)     NOT NULL,
    idbilacc          INT,
    proformanumber    VARCHAR(30),
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_role 
--

CREATE TABLE billing_role(
    idrole        BINARY(16)     NOT NULL,
    idbilling     BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_status 
--

CREATE TABLE billing_status(
    idstatus        BINARY(16)      NOT NULL,
    idbilling       BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: billing_type 
--

CREATE TABLE billing_type(
    idbiltyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idbiltyp)
)ENGINE=INNODB
;



-- 
-- TABLE: booking 
--

CREATE TABLE booking(
    idbooking             INT             AUTO_INCREMENT,
    idbookingstatus       INT,
    nobooking             VARCHAR(4)      NOT NULL,
    customername          VARCHAR(100),
    vehiclenumber         VARCHAR(20),
    datebook              DATETIME,
    bookingtime           DATETIME,
    waitinglimit          DATETIME,
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idbooking)
)ENGINE=INNODB
;



-- 
-- TABLE: booking_slot 
--

CREATE TABLE booking_slot(
    idbooslo       BINARY(16)     NOT NULL,
    idcalendar     INT,
    idbooslostd    BINARY(16),
    slotnumber     VARCHAR(30),
    PRIMARY KEY (idbooslo)
)ENGINE=INNODB
;



-- 
-- TABLE: booking_slot_standard 
--

CREATE TABLE booking_slot_standard(
    idbooslostd    BINARY(16)     NOT NULL,
    idboktyp       INT,
    idinternal     VARCHAR(30),
    description    VARCHAR(60),
    capacity       INT,
    starthour      INT,
    startminute    INT,
    endhour        INT,
    endminute      INT,
    PRIMARY KEY (idbooslostd)
)ENGINE=INNODB
;



-- 
-- TABLE: booking_status 
--

CREATE TABLE booking_status(
    idbookingstatus    INT            NOT NULL,
    description        VARCHAR(60),
    PRIMARY KEY (idbookingstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: booking_type 
--

CREATE TABLE booking_type(
    idboktyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idboktyp)
)ENGINE=INNODB
;



-- 
-- TABLE: branch 
--

CREATE TABLE branch(
    idbranchcategory    INT,
    idinternal          VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idinternal)
)ENGINE=INNODB
;



-- 
-- TABLE: broker_type 
--

CREATE TABLE broker_type(
    idbrotyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idbrotyp)
)ENGINE=INNODB
;



-- 
-- TABLE: calendar_type 
--

CREATE TABLE calendar_type(
    idcaltyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idcaltyp)
)ENGINE=INNODB
;



-- 
-- TABLE: carrier 
--

CREATE TABLE carrier(
    idcarrier    INT           NOT NULL,
    idvehicle    BINARY(16),
    idparty      BINARY(16),
    idreason     INT,
    idreltyp     INT,
    PRIMARY KEY (idcarrier)
)ENGINE=INNODB
;



-- 
-- TABLE: category_asset_tp 
--

CREATE TABLE category_asset_tp(
    idcategoryasset       INT            AUTO_INCREMENT,
    description           VARCHAR(50),
    statuscode            INT,
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    PRIMARY KEY (idcategoryasset)
)ENGINE=INNODB
;



-- 
-- TABLE: charge_to_type 
--

CREATE TABLE charge_to_type(
    idchargeto     INT            NOT NULL,
    description    VARCHAR(30),
    PRIMARY KEY (idchargeto)
)ENGINE=INNODB
;



-- 
-- TABLE: city 
--

CREATE TABLE city(
    idgeobou    BINARY(16)    NOT NULL,
    PRIMARY KEY (idgeobou)
)ENGINE=INNODB
;



-- 
-- TABLE: claim_detail 
--

CREATE TABLE claim_detail(
    idclaim               INT               AUTO_INCREMENT,
    idcustomer            VARCHAR(30),
    idsympthomdatam       INT,
    iddatadamage          INT,
    noclaim               INT               NOT NULL,
    claimtype             VARCHAR(5),
    statusclaim           VARCHAR(20),
    noticket              VARCHAR(20),
    submissiongroup_      VARCHAR(20),
    nolkh                 VARCHAR(30),
    datelkh               DATETIME,
    noho                  DATETIME,
    dateho                DATE,
    notype                VARCHAR(5),
    marketname            VARCHAR(30),
    datebuy               DATETIME,
    noproduction          VARCHAR(15),
    brokendate            DATETIME,
    kmbroken              INT,
    servicedate           DATETIME,
    rank                  CHAR(1),
    finishdate            DATETIME,
    analysisresult        VARCHAR(100),
    amount                DECIMAL(18, 2),
    mainpart              BIT(1),
    dsubtotaljob          DECIMAL(18, 2),
    transferdate          DATETIME,
    nominaltransfer       DECIMAL(18, 2),
    nodocs                VARCHAR(15),
    replace_              VARCHAR(10),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idclaim)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event 
--

CREATE TABLE communication_event(
    idcomevt    BINARY(16)      NOT NULL,
    idparrel    BINARY(16),
    idevetyp    INT,
    note        VARCHAR(300),
    dtfrom      DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru      DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idcomevt)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event_cdb 
--

CREATE TABLE communication_event_cdb(
    idcomevt           BINARY(16)      NOT NULL,
    idcustomer         VARCHAR(30),
    idprovince         BINARY(16),
    idcity             BINARY(16),
    iddistrict         BINARY(16),
    idreligion         INT,
    address            VARCHAR(300),
    addresssurat       VARCHAR(60),
    hobby              VARCHAR(60),
    homestatus         VARCHAR(60),
    exponemonth        VARCHAR(60),
    job                VARCHAR(60),
    lasteducation      VARCHAR(60),
    owncellphone       VARCHAR(60),
    hondarefference    VARCHAR(60),
    ownvehiclebrand    VARCHAR(60),
    ownvehicletype     VARCHAR(60),
    buyfor             VARCHAR(60),
    vehicleuser        VARCHAR(60),
    facebook           VARCHAR(60),
    instagram          VARCHAR(60),
    twitter            VARCHAR(60),
    youtube            VARCHAR(60),
    email              VARCHAR(60),
    iscityidadd        INT             DEFAULT 0,
    ismailadd          INT             DEFAULT 0,
    idprovincesurat    BINARY(16),
    idcitysurat        BINARY(16),
    iddistrictsurat    BINARY(16),
    idvillagesurat     BINARY(16),
    jenispembayaran    VARCHAR(60),
    groupcustomer      VARCHAR(60),
    keterangan         VARCHAR(60),
    PRIMARY KEY (idcomevt)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event_delivery 
--

CREATE TABLE communication_event_delivery(
    idcomevt         BINARY(16)      NOT NULL,
    idinternal       VARCHAR(30),
    idcustomer       VARCHAR(30),
    bussinesscode    VARCHAR(30),
    b1               VARCHAR(30),
    b2               VARCHAR(30),
    b3               VARCHAR(30),
    b4               VARCHAR(30),
    c11rate          INT,
    c11reason        VARCHAR(300),
    c11hope          VARCHAR(300),
    c12rate          INT,
    c12reason        VARCHAR(300),
    c13hope          VARCHAR(300),
    c14rate          INT,
    c14reason        VARCHAR(300),
    c14hope          VARCHAR(300),
    c15rate          INT,
    c15reason        VARCHAR(300),
    c15hope          VARCHAR(300),
    c16rate          INT,
    c16reason        VARCHAR(300),
    c16hope          VARCHAR(300),
    c17rate          INT,
    c17reason        VARCHAR(300),
    c17hope          VARCHAR(300),
    c18rate          INT,
    c18reason        VARCHAR(300),
    c18hope          VARCHAR(300),
    c19rate          INT,
    c19reason        VARCHAR(300),
    c19hope          VARCHAR(300),
    c110rate         INT,
    c110reason       VARCHAR(300),
    c110hope         VARCHAR(300),
    c2rate           INT,
    c3option         VARCHAR(300),
    d1name           VARCHAR(300),
    d2gender         VARCHAR(300),
    d3age            INT,
    d4phone          VARCHAR(300),
    d5vehicle        VARCHAR(300),
    d6education      VARCHAR(300),
    dealername       VARCHAR(300),
    dealeraddress    VARCHAR(300),
    c13rate          VARCHAR(300),
    c13reason        VARCHAR(300),
    c12hope          VARCHAR(300),
    PRIMARY KEY (idcomevt)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event_prospect 
--

CREATE TABLE communication_event_prospect(
    idcomevt        BINARY(16)        NOT NULL,
    idprospect      BINARY(16),
    idsaletype      INT,
    idproduct       VARCHAR(30),
    idcolor         INT,
    interest        INT               DEFAULT 0,
    qty             DECIMAL(14, 4),
    purchaseplan    VARCHAR(30),
    connect         INT               DEFAULT 0,
    PRIMARY KEY (idcomevt)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event_purpose 
--

CREATE TABLE communication_event_purpose(
    idcomevt         BINARY(16)    NOT NULL,
    idpurposetype    INT           NOT NULL,
    PRIMARY KEY (idcomevt, idpurposetype)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event_role 
--

CREATE TABLE communication_event_role(
    idrole        BINARY(16)     NOT NULL,
    idcomevt      BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: communication_event_status 
--

CREATE TABLE communication_event_status(
    idstatus        BINARY(16)      NOT NULL,
    idcomevt        BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: contact_mechanism 
--

CREATE TABLE contact_mechanism(
    idcontact        BINARY(16)     NOT NULL,
    idcontacttype    INT,
    description      VARCHAR(60),
    PRIMARY KEY (idcontact)
)ENGINE=INNODB
;



-- 
-- TABLE: contact_purpose 
--

CREATE TABLE contact_purpose(
    idcontact        BINARY(16)    NOT NULL,
    idpurposetype    INT           NOT NULL,
    PRIMARY KEY (idcontact, idpurposetype)
)ENGINE=INNODB
;



-- 
-- TABLE: container 
--

CREATE TABLE container(
    idcontainer    BINARY(16)     NOT NULL,
    idfacility     BINARY(16),
    idcontyp       INT,
    code           VARCHAR(30),
    description    VARCHAR(60),
    PRIMARY KEY (idcontainer)
)ENGINE=INNODB
;



-- 
-- TABLE: container_type 
--

CREATE TABLE container_type(
    idcontyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idcontyp)
)ENGINE=INNODB
;



-- 
-- TABLE: correction_note 
--

CREATE TABLE correction_note(
    idcorrectnote       INT               NOT NULL,
    idproduct           VARCHAR(30),
    idspgclaim          INT,
    idissue             INT,
    correctionnumber    VARCHAR(60),
    qty                 DECIMAL(14, 4),
    dtcorrection        DATETIME,
    PRIMARY KEY (idcorrectnote)
)ENGINE=INNODB
;



-- 
-- TABLE: customer 
--

CREATE TABLE customer(
    idcustomer            VARCHAR(30)    NOT NULL,
    idmarketcategory      INT,
    idparty               BINARY(16),
    idroletype            INT,
    idmpm                 VARCHAR(30),
    idsegmen              INT,
    idcustomertype        INT,
    useshipmentaddress    INT            DEFAULT 0,
    PRIMARY KEY (idcustomer)
)ENGINE=INNODB
;



-- 
-- TABLE: customer_order 
--

CREATE TABLE customer_order(
    idorder       BINARY(16)     NOT NULL,
    idvehicle     BINARY(16),
    idcustomer    VARCHAR(30),
    idinternal    VARCHAR(30),
    idbillto      VARCHAR(30),
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: customer_quotation 
--

CREATE TABLE customer_quotation(
    idquote            BINARY(16)     NOT NULL,
    idinternal         VARCHAR(30),
    idcustomer         VARCHAR(30),
    quotationnumber    VARCHAR(30),
    PRIMARY KEY (idquote)
)ENGINE=INNODB
;



-- 
-- TABLE: customer_relationship 
--

CREATE TABLE customer_relationship(
    idparrel      BINARY(16)     NOT NULL,
    idinternal    VARCHAR(30),
    idcustomer    VARCHAR(30),
    PRIMARY KEY (idparrel)
)ENGINE=INNODB
;



-- 
-- TABLE: customer_request_item 
--

CREATE TABLE customer_request_item(
    idreqitem     BINARY(16)        NOT NULL,
    idcustomer    VARCHAR(30),
    qty           DECIMAL(14, 4),
    unitprice     DECIMAL(18, 4),
    bbn           DECIMAL(18, 4),
    idframe       VARCHAR(30),
    idmachine     VARCHAR(30)       NOT NULL,
    PRIMARY KEY (idreqitem)
)ENGINE=INNODB
;



-- 
-- TABLE: customer_type 
--

CREATE TABLE customer_type(
    idcostumertype    INT            NOT NULL,
    description       VARCHAR(60),
    PRIMARY KEY (idcostumertype)
)ENGINE=INNODB
;



-- 
-- TABLE: data_damage 
--

CREATE TABLE data_damage(
    iddatadamage    INT            AUTO_INCREMENT,
    descdamage      VARCHAR(30),
    PRIMARY KEY (iddatadamage)
)ENGINE=INNODB
;



-- 
-- TABLE: dealer_claim_type 
--

CREATE TABLE dealer_claim_type(
    idclaimtype    INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idclaimtype)
)ENGINE=INNODB
;



-- 
-- TABLE: dealer_reminder_type 
--

CREATE TABLE dealer_reminder_type(
    idremindertype    INT             AUTO_INCREMENT,
    description       VARCHAR(60),
    messages          VARCHAR(300),
    PRIMARY KEY (idremindertype)
)ENGINE=INNODB
;



-- 
-- TABLE: deliverable_type 
--

CREATE TABLE deliverable_type(
    iddeltype      INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (iddeltype)
)ENGINE=INNODB
;



-- 
-- TABLE: delivery_order 
--

CREATE TABLE delivery_order(
    invoicevendornumber    VARCHAR(30),
    idbilling              BINARY(16)     NOT NULL,
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: delivery_order_status 
--

CREATE TABLE delivery_order_status(
    iddeliveryorderstatus    INT            NOT NULL,
    description              VARCHAR(50),
    PRIMARY KEY (iddeliveryorderstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: dimension 
--

CREATE TABLE dimension(
    iddimension    VARCHAR(30)    NOT NULL,
    iddimtyp       INT,
    description    VARCHAR(60),
    PRIMARY KEY (iddimension)
)ENGINE=INNODB
;



-- 
-- TABLE: dimension_type 
--

CREATE TABLE dimension_type(
    iddimtyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (iddimtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: disbursement 
--

CREATE TABLE disbursement(
    idpayment    BINARY(16)    NOT NULL,
    PRIMARY KEY (idpayment)
)ENGINE=INNODB
;



-- 
-- TABLE: dispatch_status 
--

CREATE TABLE dispatch_status(
    iddispatchstatus    INT            NOT NULL,
    description         VARCHAR(30),
    PRIMARY KEY (iddispatchstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: district 
--

CREATE TABLE district(
    idgeobou    BINARY(16)    NOT NULL,
    PRIMARY KEY (idgeobou)
)ENGINE=INNODB
;



-- 
-- TABLE: document_type 
--

CREATE TABLE document_type(
    iddoctype      INT            NOT NULL,
    idparent       INT,
    description    VARCHAR(60),
    PRIMARY KEY (iddoctype)
)ENGINE=INNODB
;



-- 
-- TABLE: documents 
--

CREATE TABLE documents(
    iddocument    BINARY(16)     NOT NULL,
    iddoctype     INT,
    note          VARCHAR(60),
    dtcreate      DATETIME       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (iddocument)
)ENGINE=INNODB
;



-- 
-- TABLE: driver 
--

CREATE TABLE driver(
    idparrol      BINARY(16)     NOT NULL,
    idvendor      VARCHAR(30),
    iddriver      VARCHAR(90),
    isexternal    CHAR(5),
    PRIMARY KEY (idparrol)
)ENGINE=INNODB
;



-- 
-- TABLE: electronic_address 
--

CREATE TABLE electronic_address(
    idcontact    BINARY(16)     NOT NULL,
    address      VARCHAR(60),
    PRIMARY KEY (idcontact)
)ENGINE=INNODB
;



-- 
-- TABLE: employee 
--

CREATE TABLE employee(
    employeenumber    VARCHAR(30)    NOT NULL,
    idparty           BINARY(16),
    idparrol          BINARY(16),
    PRIMARY KEY (employeenumber)
)ENGINE=INNODB
;



-- 
-- TABLE: employee_customer_relationship 
--

CREATE TABLE employee_customer_relationship(
    idparrel          BINARY(16)     NOT NULL,
    employeenumber    VARCHAR(30),
    idcustomer        VARCHAR(30),
    PRIMARY KEY (idparrel)
)ENGINE=INNODB
;



-- 
-- TABLE: event_type 
--

CREATE TABLE event_type(
    idevetyp       INT            AUTO_INCREMENT,
    idprneve       INT,
    description    VARCHAR(60),
    PRIMARY KEY (idevetyp)
)ENGINE=INNODB
;



-- 
-- TABLE: exchange_part 
--

CREATE TABLE exchange_part(
    idspgclaimaction    INT,
    idproduct           VARCHAR(30),
    idspgclaimdetail    INT               NOT NULL,
    qty                 DECIMAL(14, 2),
    dtexchangepart      DATETIME,
    PRIMARY KEY (idspgclaimdetail)
)ENGINE=INNODB
;



-- 
-- TABLE: external_acctg_trans 
--

CREATE TABLE external_acctg_trans(
    idacctra    INT           NOT NULL,
    idparfro    BINARY(16),
    idparto     BINARY(16),
    PRIMARY KEY (idacctra)
)ENGINE=INNODB
;



-- 
-- TABLE: facility 
--

CREATE TABLE facility(
    idfacility        BINARY(16)     NOT NULL,
    idfacilitytype    INT,
    idpartof          BINARY(16),
    idfa              INT,
    code              VARCHAR(30),
    description       VARCHAR(60),
    PRIMARY KEY (idfacility)
)ENGINE=INNODB
;



-- 
-- TABLE: facility_contact_mechanism 
--

CREATE TABLE facility_contact_mechanism(
    idconmecpur      BINARY(16)    NOT NULL,
    idfacility       BINARY(16),
    idcontact        BINARY(16),
    idpurposetype    INT,
    dtfrom           DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru           DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idconmecpur)
)ENGINE=INNODB
;



-- 
-- TABLE: facility_status 
--

CREATE TABLE facility_status(
    idstatus        BINARY(16)      NOT NULL,
    idfacility      BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: facility_type 
--

CREATE TABLE facility_type(
    idfacilitytype    INT            AUTO_INCREMENT,
    description       VARCHAR(60),
    PRIMARY KEY (idfacilitytype)
)ENGINE=INNODB
;



-- 
-- TABLE: feature 
--

CREATE TABLE feature(
    idfeature      INT            NOT NULL,
    idfeatyp       INT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idfeature)
)ENGINE=INNODB
;



-- 
-- TABLE: feature_applicable 
--

CREATE TABLE feature_applicable(
    idapplicability    BINARY(16)     NOT NULL,
    idfeature          INT,
    idproduct          VARCHAR(30),
    idfeatyp           INT,
    idowner            BINARY(16),
    boolvalue          INT            DEFAULT 0,
    dtfrom             DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru             DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idapplicability)
)ENGINE=INNODB
;



-- 
-- TABLE: feature_type 
--

CREATE TABLE feature_type(
    idfeatyp       INT            AUTO_INCREMENT,
    idrultyp       INT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idfeatyp)
)ENGINE=INNODB
;



-- 
-- TABLE: fixed_asset 
--

CREATE TABLE fixed_asset(
    idfa           INT            AUTO_INCREMENT,
    idfatype       INT,
    iduom          VARCHAR(30),
    name           VARCHAR(30),
    dtacquired     DATETIME       DEFAULT CURRENT_TIMESTAMP,
    description    VARCHAR(60),
    PRIMARY KEY (idfa)
)ENGINE=INNODB
;



-- 
-- TABLE: fixed_asset_type 
--

CREATE TABLE fixed_asset_type(
    idfatype       INT            NOT NULL,
    idparent       INT,
    description    VARCHAR(60),
    PRIMARY KEY (idfatype)
)ENGINE=INNODB
;



-- 
-- TABLE: fixedasset_adjustment_th 
--

CREATE TABLE fixedasset_adjustment_th(
    idfixedassetadjustment    INT         AUTO_INCREMENT,
    idfixedasset              INT,
    adjusmentdate             DATETIME,
    adjustmentquantity        INT,
    newquantity               INT,
    lastmodifieddate          DATETIME,
    lastmodifieduserid        INT,
    PRIMARY KEY (idfixedassetadjustment)
)ENGINE=INNODB
;



-- 
-- TABLE: fixedasset_th 
--

CREATE TABLE fixedasset_th(
    idfixedassetth        INT         AUTO_INCREMENT,
    idfixedasset          INT,
    idlocationroom        INT,
    idlocationfloor       INT,
    idpicuser             INT,
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    PRIMARY KEY (idfixedassetth)
)ENGINE=INNODB
;



-- 
-- TABLE: fixedasset_tm 
--

CREATE TABLE fixedasset_tm(
    idfixedasset                INT               AUTO_INCREMENT,
    idpurchaseorderreception    INT,
    idpurchaseorder             INT,
    idfloor                     INT,
    idroom                      INT,
    idcategoryasset             INT,
    batchno                     INT,
    assetid                     VARCHAR(30),
    idpicuser                   INT,
    name                        VARCHAR(30),
    price                       DECIMAL(18, 4),
    orderquantity               INT,
    acceptedquantity            INT,
    currentquantity             INT,
    unitmeasure                 VARCHAR(30),
    picname                     VARCHAR(50),
    lastmodifieddate            DATETIME,
    lastmodifieduserid          INT,
    statuscode                  INT,
    PRIMARY KEY (idfixedasset)
)ENGINE=INNODB
;



-- 
-- TABLE: floor_tm 
--

CREATE TABLE floor_tm(
    idfloor               INT            AUTO_INCREMENT,
    floorname             VARCHAR(50),
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idfloor)
)ENGINE=INNODB
;



-- 
-- TABLE: gender 
--

CREATE TABLE gender(
    idgender       VARCHAR(1)     NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idgender)
)ENGINE=INNODB
;



-- 
-- TABLE: general_upload 
--

CREATE TABLE general_upload(
    idgenupl            BINARY(16)      NOT NULL,
    idinternal          VARCHAR(30),
    idpurposetype       INT,
    dtcreate            DATETIME        DEFAULT CURRENT_TIMESTAMP,
    description         VARCHAR(60),
    filename            VARCHAR(255),
    value               BLOB,
    valuecontenttype    VARCHAR(300),
    PRIMARY KEY (idgenupl)
)ENGINE=INNODB
;



-- 
-- TABLE: general_upload_status 
--

CREATE TABLE general_upload_status(
    idstatus        BINARY(16)      NOT NULL,
    idgenupl        BINARY(16),
    idreason        INT,
    idstatustype    INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: geo_boundary 
--

CREATE TABLE geo_boundary(
    idgeobou        BINARY(16)     NOT NULL,
    idparent        BINARY(16),
    idgeoboutype    INT,
    geocode         VARCHAR(30),
    description     VARCHAR(60),
    PRIMARY KEY (idgeobou)
)ENGINE=INNODB
;



-- 
-- TABLE: geo_boundary_type 
--

CREATE TABLE geo_boundary_type(
    idgeoboutype    INT            AUTO_INCREMENT,
    description     VARCHAR(60),
    PRIMARY KEY (idgeoboutype)
)ENGINE=INNODB
;



-- 
-- TABLE: gl_account 
--

CREATE TABLE gl_account(
    idglacc        BINARY(16)     NOT NULL,
    idglacctyp     INT,
    name           VARCHAR(30),
    description    VARCHAR(60),
    PRIMARY KEY (idglacc)
)ENGINE=INNODB
;



-- 
-- TABLE: gl_account_type 
--

CREATE TABLE gl_account_type(
    idglacctyp     INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idglacctyp)
)ENGINE=INNODB
;



-- 
-- TABLE: gl_dimension 
--

CREATE TABLE gl_dimension(
    idgldim       BINARY(16)     NOT NULL,
    idbillto      VARCHAR(30),
    idvendor      VARCHAR(30),
    idinternal    VARCHAR(30),
    idprocat      INT,
    idparcat      INT,
    dimension1    VARCHAR(30),
    dimension2    VARCHAR(30),
    dimension3    VARCHAR(30),
    dimension4    VARCHAR(30),
    dimension5    VARCHAR(30),
    dimension6    VARCHAR(30),
    PRIMARY KEY (idgldim)
)ENGINE=INNODB
;



-- 
-- TABLE: good 
--

CREATE TABLE good(
    idproduct     VARCHAR(30)    NOT NULL,
    iduom         VARCHAR(30),
    serialized    INT            DEFAULT 0,
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: good_container 
--

CREATE TABLE good_container(
    idgoocon       BINARY(16)     NOT NULL,
    idproduct      VARCHAR(30),
    idcontainer    BINARY(16),
    idparty        BINARY(16),
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idgoocon)
)ENGINE=INNODB
;



-- 
-- TABLE: good_identification 
--

CREATE TABLE good_identification(
    idgooide                BINARY(16)     NOT NULL,
    idproduct               VARCHAR(30),
    ididentificationtype    INT,
    idvalue                 VARCHAR(30),
    PRIMARY KEY (idgooide)
)ENGINE=INNODB
;



-- 
-- TABLE: good_promotion 
--

CREATE TABLE good_promotion(
    idproduct    VARCHAR(30)    NOT NULL,
    ispromo      BIT(1),
    dtcreate     DATETIME,
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: history_store 
--

CREATE TABLE history_store(
    timemark        DATETIME        NOT NULL,
    table_name      VARCHAR(50)     NOT NULL,
    pk_date_src     VARCHAR(400)    NOT NULL,
    pk_date_dest    VARCHAR(400)    NOT NULL,
    record_state    SMALLINT        NOT NULL,
    PRIMARY KEY (table_name, pk_date_dest)
)ENGINE=INNODB
;



-- 
-- TABLE: identification_type 
--

CREATE TABLE identification_type(
    ididentificationtype    INT            NOT NULL,
    description             VARCHAR(60),
    PRIMARY KEY (ididentificationtype)
)ENGINE=INNODB
;



-- 
-- TABLE: intern 
--

CREATE TABLE intern(
    idintern       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idintern)
)ENGINE=INNODB
;



-- 
-- TABLE: internal 
--

CREATE TABLE internal(
    idinternal      VARCHAR(30)    NOT NULL,
    idroot          VARCHAR(30),
    idparent        VARCHAR(30),
    idroletype      INT,
    idparty         BINARY(16),
    selfsender      INT            DEFAULT 0,
    idmpm           VARCHAR(30),
    idahm           VARCHAR(30),
    iddealercode    VARCHAR(30),
    PRIMARY KEY (idinternal)
)ENGINE=INNODB
;



-- 
-- TABLE: internal_acctg_trans 
--

CREATE TABLE internal_acctg_trans(
    idacctra      INT            NOT NULL,
    idinternal    VARCHAR(30),
    PRIMARY KEY (idacctra)
)ENGINE=INNODB
;



-- 
-- TABLE: internal_facility_purpose 
--

CREATE TABLE internal_facility_purpose(
    idintfacpur      BINARY(16)     NOT NULL,
    idpurposetype    INT,
    idfacility       BINARY(16),
    idinternal       VARCHAR(30),
    dtfrom           DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru           DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idintfacpur)
)ENGINE=INNODB
;



-- 
-- TABLE: internal_order 
--

CREATE TABLE internal_order(
    idorder     BINARY(16)     NOT NULL,
    idintto     VARCHAR(30),
    idintfro    VARCHAR(30),
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: inventory_adjustment 
--

CREATE TABLE inventory_adjustment(
    idinvadj      BINARY(16)        NOT NULL,
    idinvite      BINARY(16),
    idstopnite    BINARY(16),
    qty           DECIMAL(14, 4),
    PRIMARY KEY (idinvadj)
)ENGINE=INNODB
;



-- 
-- TABLE: inventory_item 
--

CREATE TABLE inventory_item(
    idinvite        BINARY(16)        NOT NULL,
    idowner         BINARY(16),
    idproduct       VARCHAR(30),
    idfacility      BINARY(16),
    idcontainer     BINARY(16),
    idreceipt       BINARY(16),
    idfeature       INT,
    idfa            INT,
    dtcreated       DATETIME          DEFAULT CURRENT_TIMESTAMP,
    qty             DECIMAL(14, 4),
    qtybooking      DECIMAL(14, 4),
    idmachine       VARCHAR(60),
    idframe         VARCHAR(60),
    yearassembly    INT,
    PRIMARY KEY (idinvite)
)ENGINE=INNODB
;



-- 
-- TABLE: inventory_item_status 
--

CREATE TABLE inventory_item_status(
    idstatus        BINARY(16)      NOT NULL,
    idinvite        BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: inventory_movement 
--

CREATE TABLE inventory_movement(
    idslip      BINARY(16)     NOT NULL,
    slipnum     VARCHAR(30),
    dtcreate    DATETIME       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idslip)
)ENGINE=INNODB
;



-- 
-- TABLE: inventory_movement_status 
--

CREATE TABLE inventory_movement_status(
    idstatus        BINARY(16)      NOT NULL,
    idslip          BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(300),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: inventory_request_item 
--

CREATE TABLE inventory_request_item(
    idinvite     BINARY(16)        NOT NULL,
    idreqitem    BINARY(16)        NOT NULL,
    qty          DECIMAL(14, 4),
    PRIMARY KEY (idinvite, idreqitem)
)ENGINE=INNODB
;



-- 
-- TABLE: item_issuance 
--

CREATE TABLE item_issuance(
    iditeiss    BINARY(16)        NOT NULL,
    idshiite    BINARY(16),
    idinvite    BINARY(16),
    idslip      BINARY(16),
    qty         DECIMAL(14, 4),
    PRIMARY KEY (iditeiss)
)ENGINE=INNODB
;



-- 
-- TABLE: job 
--

CREATE TABLE job(
    idpkb                 INT             NOT NULL,
    idparrol              BINARY(16),
    idjobstatus           INT,
    starttime             DATETIME,
    complaint             VARCHAR(255),
    jobsuggest            VARCHAR(255),
    duration              INT,
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idpkb)
)ENGINE=INNODB
;



-- 
-- TABLE: job_dispatch 
--

CREATE TABLE job_dispatch(
    idjobdispatch       INT           AUTO_INCREMENT,
    iddispatchstatus    INT,
    idpkb               INT,
    idparrol            BINARY(16),
    idjobpriority       INT,
    dispatchdate        DATETIME,
    PRIMARY KEY (idjobdispatch)
)ENGINE=INNODB
;



-- 
-- TABLE: job_dispatch_customer_type 
--

CREATE TABLE job_dispatch_customer_type(
    idjobdispatch     INT    NOT NULL,
    idcostumertype    INT,
    PRIMARY KEY (idjobdispatch)
)ENGINE=INNODB
;



-- 
-- TABLE: job_dispatch_group_service 
--

CREATE TABLE job_dispatch_group_service(
    idjobdispatch    INT    NOT NULL,
    PRIMARY KEY (idjobdispatch)
)ENGINE=INNODB
;



-- 
-- TABLE: job_dispatch_pkb_type 
--

CREATE TABLE job_dispatch_pkb_type(
    idjobdispatch    INT    NOT NULL,
    idtypepkb        INT,
    PRIMARY KEY (idjobdispatch)
)ENGINE=INNODB
;



-- 
-- TABLE: job_dispatch_service 
--

CREATE TABLE job_dispatch_service(
    idjobdispatch    INT            NOT NULL,
    idproduct        VARCHAR(30),
    PRIMARY KEY (idjobdispatch)
)ENGINE=INNODB
;



-- 
-- TABLE: job_history 
--

CREATE TABLE job_history(
    idjobhistory       INT           AUTO_INCREMENT,
    idpkb              INT,
    idjobstatus        INT,
    idpendingreason    INT,
    idparrol           BINARY(16),
    time               DATETIME,
    duration           INT,
    PRIMARY KEY (idjobhistory)
)ENGINE=INNODB
;



-- 
-- TABLE: job_priority 
--

CREATE TABLE job_priority(
    idjobpriority        INT            NOT NULL,
    idjobprioritytype    INT,
    priorityname         VARCHAR(30),
    priority             INT,
    dtcreate             DATETIME,
    PRIMARY KEY (idjobpriority)
)ENGINE=INNODB
;



-- 
-- TABLE: job_priority_customer_type 
--

CREATE TABLE job_priority_customer_type(
    idjobpriority     INT    NOT NULL,
    idcostumertype    INT,
    PRIMARY KEY (idjobpriority)
)ENGINE=INNODB
;



-- 
-- TABLE: job_priority_group_service 
--

CREATE TABLE job_priority_group_service(
    idjobpriority    INT    NOT NULL,
    PRIMARY KEY (idjobpriority)
)ENGINE=INNODB
;



-- 
-- TABLE: job_priority_pkb_type 
--

CREATE TABLE job_priority_pkb_type(
    idjobpriority    INT    NOT NULL,
    idtypepkb        INT,
    PRIMARY KEY (idjobpriority)
)ENGINE=INNODB
;



-- 
-- TABLE: job_priority_service 
--

CREATE TABLE job_priority_service(
    idjobpriority    INT            NOT NULL,
    idproduct        VARCHAR(30),
    PRIMARY KEY (idjobpriority)
)ENGINE=INNODB
;



-- 
-- TABLE: job_priority_type 
--

CREATE TABLE job_priority_type(
    idjobprioritytype    INT            NOT NULL,
    jobtypename          VARCHAR(30),
    dtcreate             DATETIME,
    PRIMARY KEY (idjobprioritytype)
)ENGINE=INNODB
;



-- 
-- TABLE: job_service 
--

CREATE TABLE job_service(
    idjobservice    INT            NOT NULL,
    idpkb           INT,
    idproduct       VARCHAR(30),
    PRIMARY KEY (idjobservice)
)ENGINE=INNODB
;



-- 
-- TABLE: job_status 
--

CREATE TABLE job_status(
    idjobstatus     INT            NOT NULL,
    idstatustype    INT,
    desc_           VARCHAR(60),
    PRIMARY KEY (idjobstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: job_suggest 
--

CREATE TABLE job_suggest(
    idjobsuggest    INT            AUTO_INCREMENT,
    idproduct       VARCHAR(30),
    firstkm         INT,
    lastkm          INT,
    dtfrom          DATETIME,
    dtthru          DATETIME,
    PRIMARY KEY (idjobsuggest)
)ENGINE=INNODB
;



-- 
-- TABLE: job_suggest_service 
--

CREATE TABLE job_suggest_service(
    idjobsuggestservice    INT            AUTO_INCREMENT,
    idjobsuggest           INT,
    idproduct              VARCHAR(30),
    dtfrom                 DATETIME,
    dtthru                 DATETIME,
    PRIMARY KEY (idjobsuggestservice)
)ENGINE=INNODB
;



-- 
-- TABLE: leasing_company 
--

CREATE TABLE leasing_company(
    idparrol        BINARY(16)     NOT NULL,
    idleacom        VARCHAR(30),
    idleasingahm    VARCHAR(30),
    PRIMARY KEY (idparrol)
)ENGINE=INNODB
;



-- 
-- TABLE: leasing_tenor_provide 
--

CREATE TABLE leasing_tenor_provide(
    idlsgpro       BINARY(16)        NOT NULL,
    idproduct      VARCHAR(30),
    idleacom       BINARY(16),
    tenor          INT,
    installment    DECIMAL(18, 4),
    downpayment    DECIMAL(18, 4),
    dtfrom         DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME          DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idlsgpro)
)ENGINE=INNODB
;



-- 
-- TABLE: market_treatment 
--

CREATE TABLE market_treatment(
    idmarkettreatment    INT             AUTO_INCREMENT,
    idvendor             VARCHAR(30),
    idbilling            BINARY(16),
    name                 VARCHAR(90),
    description          VARCHAR(300),
    juklaknumber         VARCHAR(30),
    dtfirstperiod        DATETIME,
    dtlastperiod         DATETIME,
    PRIMARY KEY (idmarkettreatment)
)ENGINE=INNODB
;



-- 
-- TABLE: market_treatment_status 
--

CREATE TABLE market_treatment_status(
    idmarkettreatmentstatus    INT         AUTO_INCREMENT,
    idstatustype               INT,
    idmarkettreatment          INT,
    dtfrom                     DATETIME,
    dtthru                     DATETIME,
    PRIMARY KEY (idmarkettreatmentstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: master_numbering 
--

CREATE TABLE master_numbering(
    idnumbering    BINARY(16)        NOT NULL,
    idtag          VARCHAR(60),
    idvalue        VARCHAR(60),
    nextvalue      INT,
    valuenumber    DECIMAL(18, 4),
    valuestring    VARCHAR(300),
    PRIMARY KEY (idnumbering)
)ENGINE=INNODB
;



-- 
-- TABLE: mechanic 
--

CREATE TABLE mechanic(
    idparrol            BINARY(16)     NOT NULL,
    idvendor            VARCHAR(30),
    idmechanic          VARCHAR(30),
    isexternal          INT            DEFAULT 0,
    passwordmechanic    VARCHAR(60),
    PRIMARY KEY (idparrol)
)ENGINE=INNODB
;



-- 
-- TABLE: memo 
--

CREATE TABLE memo(
    idmemo          BINARY(16)        NOT NULL,
    idbilling       BINARY(16),
    idmemotype      INT,
    idinvite        BINARY(16),
    idoldinvite     BINARY(16),
    idinternal      VARCHAR(30),
    dtcreated       DATETIME          DEFAULT CURRENT_TIMESTAMP,
    memonumber      VARCHAR(30),
    bbn             DECIMAL(18, 4),
    discount        DECIMAL(18, 4),
    unitprice       DECIMAL(18, 4),
    currbillnumb    VARCHAR(30),
    reqidfeature    INT,
    ppn             DECIMAL(18, 4),
    dpp             DECIMAL(18, 4),
    salesamount     DECIMAL(18, 4),
    aramount        DECIMAL(18, 4),
    PRIMARY KEY (idmemo)
)ENGINE=INNODB
;



-- 
-- TABLE: memo_status 
--

CREATE TABLE memo_status(
    idstatus        BINARY(16)      NOT NULL,
    idmemo          BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: memo_type 
--

CREATE TABLE memo_type(
    idmemotype     INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idmemotype)
)ENGINE=INNODB
;



-- 
-- TABLE: motor 
--

CREATE TABLE motor(
    idproduct         VARCHAR(30)    NOT NULL,
    marketname        VARCHAR(60),
    dtdiscontinued    DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: motor_due_reminder 
--

CREATE TABLE motor_due_reminder(
    idreminder    INT            AUTO_INCREMENT,
    idmotor       VARCHAR(30),
    nservice1     INT,
    nservice2     INT,
    nservice3     INT,
    nservice4     INT,
    nservice5     INT,
    km            INT,
    ndays         INT,
    PRIMARY KEY (idreminder)
)ENGINE=INNODB
;



-- 
-- TABLE: moving_slip 
--

CREATE TABLE moving_slip(
    idslip             BINARY(16)        NOT NULL,
    idinviteto         BINARY(16),
    idcontainerfrom    BINARY(16),
    idcontainerto      BINARY(16),
    idproduct          VARCHAR(30),
    idfacilityto       BINARY(16),
    idfacilityfrom     BINARY(16),
    idinvite           BINARY(16),
    idreqitem          BINARY(16),
    qty                DECIMAL(14, 4),
    PRIMARY KEY (idslip)
)ENGINE=INNODB
;



-- 
-- TABLE: operation_code 
--

CREATE TABLE operation_code(
    idoperationcode    VARCHAR(30)    NOT NULL,
    description        VARCHAR(60),
    PRIMARY KEY (idoperationcode)
)ENGINE=INNODB
;



-- 
-- TABLE: order_billing_item 
--

CREATE TABLE order_billing_item(
    idordbilite    BINARY(16)        NOT NULL,
    idordite       BINARY(16),
    idbilite       BINARY(16),
    amount         DECIMAL(18, 4),
    qty            DECIMAL(14, 4),
    PRIMARY KEY (idordbilite)
)ENGINE=INNODB
;



-- 
-- TABLE: order_demand 
--

CREATE TABLE order_demand(
    idorderdemand    CHAR(10)          NOT NULL,
    qty              DECIMAL(18, 4),
    idproduct        VARCHAR(30),
    idorder          BINARY(16),
    dtorderdemand    DATETIME,
    PRIMARY KEY (idorderdemand)
)ENGINE=INNODB
;



-- 
-- TABLE: order_item 
--

CREATE TABLE order_item(
    idordite           BINARY(16)        NOT NULL,
    idparordite        BINARY(16),
    idorder            BINARY(16),
    idproduct          VARCHAR(30),
    idfeature          INT,
    idshipto           VARCHAR(30),
    itemdescription    VARCHAR(60),
    qty                DECIMAL(14, 4),
    unitprice          DECIMAL(18, 4),
    discount           DECIMAL(18, 4),
    discountmd         DECIMAL(18, 4),
    discountfin        DECIMAL(18, 4),
    bbn                DECIMAL(18, 4),
    discountatpm       DECIMAL(18, 4),
    idframe            VARCHAR(30),
    idmachine          VARCHAR(30),
    taxamount          DECIMAL(18, 4),
    totalamount        DECIMAL(18, 4),
    PRIMARY KEY (idordite)
)ENGINE=INNODB
;



-- 
-- TABLE: order_payment 
--

CREATE TABLE order_payment(
    idordpayite    BINARY(16)        NOT NULL,
    idorder        BINARY(16),
    idpayment      BINARY(16),
    amount         DECIMAL(18, 4),
    PRIMARY KEY (idordpayite)
)ENGINE=INNODB
;



-- 
-- TABLE: order_request_item 
--

CREATE TABLE order_request_item(
    idordreqite     BINARY(16)        NOT NULL,
    idordite        BINARY(16),
    idreqitem       BINARY(16),
    qty             DECIMAL(14, 4),
    qtydelivered    DECIMAL(14, 4),
    PRIMARY KEY (idordreqite)
)ENGINE=INNODB
;



-- 
-- TABLE: order_shipment_item 
--

CREATE TABLE order_shipment_item(
    idordshiite    BINARY(16)        NOT NULL,
    idordite       BINARY(16),
    idshiite       BINARY(16),
    qty            DECIMAL(14, 4),
    PRIMARY KEY (idordshiite)
)ENGINE=INNODB
;



-- 
-- TABLE: order_type 
--

CREATE TABLE order_type(
    idordtyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idordtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: orders 
--

CREATE TABLE orders(
    idorder        BINARY(16)     NOT NULL,
    idordtyp       INT,
    ordernumber    VARCHAR(30),
    dtentry        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtorder        DATE,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: orders_role 
--

CREATE TABLE orders_role(
    idrole        BINARY(16)     NOT NULL,
    idorder       BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: orders_status 
--

CREATE TABLE orders_status(
    idstatus        BINARY(16)      NOT NULL,
    idorder         BINARY(16),
    idstatustype    INT,
    idreason        INT,
    idordite        BINARY(16),
    description     VARCHAR(60),
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: organization 
--

CREATE TABLE organization(
    idparty         BINARY(16)     NOT NULL,
    idpic           BINARY(16),
    idposaddtdp     BINARY(16),
    name            VARCHAR(30),
    numbertdp       VARCHAR(30),
    dtestablised    DATE,
    officephone     VARCHAR(30),
    officefax       VARCHAR(30),
    taxidnumber     VARCHAR(30),
    officemail      VARCHAR(60),
    PRIMARY KEY (idparty)
)ENGINE=INNODB
;



-- 
-- TABLE: organization_customer 
--

CREATE TABLE organization_customer(
    idcustomer    VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idcustomer)
)ENGINE=INNODB
;



-- 
-- TABLE: organization_gl_account 
--

CREATE TABLE organization_gl_account(
    idorgglacc    BINARY(16)     NOT NULL,
    idparent      BINARY(16),
    idglacc       BINARY(16),
    idinternal    VARCHAR(30),
    idvendor      VARCHAR(30),
    idbillto      VARCHAR(30),
    idproduct     VARCHAR(30),
    idcategory    INT,
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idorgglacc)
)ENGINE=INNODB
;



-- 
-- TABLE: organization_prospect 
--

CREATE TABLE organization_prospect(
    idprospect     BINARY(16)    NOT NULL,
    idparty        BINARY(16),
    idpic          BINARY(16),
    idposaddtdp    BINARY(16),
    PRIMARY KEY (idprospect)
)ENGINE=INNODB
;



-- 
-- TABLE: organization_prospect_detail 
--

CREATE TABLE organization_prospect_detail(
    idetail          BINARY(16)        NOT NULL,
    idprospect       BINARY(16),
    idpersonowner    BINARY(16),
    idproduct        VARCHAR(30),
    idfeature        INT,
    unitprice        DECIMAL(18, 4),
    discount         DECIMAL(18, 4),
    PRIMARY KEY (idetail)
)ENGINE=INNODB
;



-- 
-- TABLE: organization_suspect 
--

CREATE TABLE organization_suspect(
    idsuspect    BINARY(16)    NOT NULL,
    idparty      BINARY(16),
    PRIMARY KEY (idsuspect)
)ENGINE=INNODB
;



-- 
-- TABLE: package_receipt 
--

CREATE TABLE package_receipt(
    idpackage         BINARY(16)     NOT NULL,
    idshifro          VARCHAR(30),
    documentnumber    VARCHAR(60),
    vendorinvoice     VARCHAR(30),
    PRIMARY KEY (idpackage)
)ENGINE=INNODB
;



-- 
-- TABLE: packaging_content 
--

CREATE TABLE packaging_content(
    idpaccon     BINARY(16)        NOT NULL,
    idpackage    BINARY(16),
    idshiite     BINARY(16),
    qty          DECIMAL(14, 4),
    PRIMARY KEY (idpaccon)
)ENGINE=INNODB
;



-- 
-- TABLE: parent_organization 
--

CREATE TABLE parent_organization(
    idinternal    VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idinternal)
)ENGINE=INNODB
;



-- 
-- TABLE: part_history 
--

CREATE TABLE part_history(
    idparthistory              INT               AUTO_INCREMENT,
    idservicehistory           INT,
    idvehicleservicehistory    INT,
    nopart                     VARCHAR(30),
    descpart                   VARCHAR(30),
    qty                        DECIMAL(18, 4),
    PRIMARY KEY (idparthistory)
)ENGINE=INNODB
;



-- 
-- TABLE: part_issue 
--

CREATE TABLE part_issue(
    idissue            INT            AUTO_INCREMENT,
    idpartissuetype    INT,
    filingnumber       VARCHAR(20),
    filingdate         DATE,
    PRIMARY KEY (idissue)
)ENGINE=INNODB
;



-- 
-- TABLE: part_issue_action 
--

CREATE TABLE part_issue_action(
    idpartissueact    BINARY(16)     NOT NULL,
    idissue           INT,
    claimto           VARCHAR(90),
    nobast            VARCHAR(20),
    receivedby        VARCHAR(90),
    releasedby        VARCHAR(90),
    approvedby        VARCHAR(90),
    dtcreate          DATE,
    PRIMARY KEY (idpartissueact)
)ENGINE=INNODB
;



-- 
-- TABLE: part_issue_item 
--

CREATE TABLE part_issue_item(
    idissueitem     INT            AUTO_INCREMENT,
    idissue         INT,
    idproduct       VARCHAR(30),
    idstatustype    INT,
    description     VARCHAR(50),
    qty             INT,
    PRIMARY KEY (idissueitem)
)ENGINE=INNODB
;



-- 
-- TABLE: part_issue_status 
--

CREATE TABLE part_issue_status(
    idstatus        INT     AUTO_INCREMENT,
    idissue         INT,
    idstatustype    INT,
    dtfrom          DATE,
    dtthru          DATE,
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: part_issue_type 
--

CREATE TABLE part_issue_type(
    idpartissuetype    INT             NOT NULL,
    description        VARCHAR(255),
    PRIMARY KEY (idpartissuetype)
)ENGINE=INNODB
;



-- 
-- TABLE: part_sales_order 
--

CREATE TABLE part_sales_order(
    idorder    BINARY(16)        NOT NULL,
    qty        DECIMAL(14, 4),
    total      DECIMAL(18, 4),
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: party 
--

CREATE TABLE party(
    idparty    BINARY(16)    NOT NULL,
    idtype     INT,
    PRIMARY KEY (idparty)
)ENGINE=INNODB
;



-- 
-- TABLE: party_category 
--

CREATE TABLE party_category(
    idcategory     INT            AUTO_INCREMENT,
    idcattyp       INT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idcategory)
)ENGINE=INNODB
;



-- 
-- TABLE: party_category_impl 
--

CREATE TABLE party_category_impl(
    idparty       BINARY(16)    NOT NULL,
    idcategory    INT           NOT NULL,
    PRIMARY KEY (idparty, idcategory)
)ENGINE=INNODB
;



-- 
-- TABLE: party_category_type 
--

CREATE TABLE party_category_type(
    idcattyp       INT            NOT NULL,
    idparent       INT,
    idrultyp       INT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idcattyp)
)ENGINE=INNODB
;



-- 
-- TABLE: party_classification 
--

CREATE TABLE party_classification(
    idclassification    BINARY(16)    NOT NULL,
    idparty             BINARY(16),
    idowner             BINARY(16),
    idcategory          INT,
    idcattyp            INT,
    dtfrom              DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru              DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idclassification)
)ENGINE=INNODB
;



-- 
-- TABLE: party_contact 
--

CREATE TABLE party_contact(
    idparty      BINARY(16)    NOT NULL,
    idcontact    BINARY(16)    NOT NULL,
    PRIMARY KEY (idparty, idcontact)
)ENGINE=INNODB
;



-- 
-- TABLE: party_contact_mechanism 
--

CREATE TABLE party_contact_mechanism(
    idparcon         INT           AUTO_INCREMENT,
    idparty          BINARY(16),
    idcontact        BINARY(16),
    idpurposetype    INT           DEFAULT 1,
    dtfrom           DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru           DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idparcon)
)ENGINE=INNODB
;



-- 
-- TABLE: party_document 
--

CREATE TABLE party_document(
    iddocument     BINARY(16)     NOT NULL,
    idparty        BINARY(16),
    contenttype    VARCHAR(60),
    content        BLOB,
    PRIMARY KEY (iddocument)
)ENGINE=INNODB
;



-- 
-- TABLE: party_facility_purpose 
--

CREATE TABLE party_facility_purpose(
    idparfacpur      BINARY(16)    NOT NULL,
    idparty          BINARY(16),
    idpurposetype    INT,
    idfacility       BINARY(16),
    dtfrom           DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru           DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idparfacpur)
)ENGINE=INNODB
;



-- 
-- TABLE: party_relationship 
--

CREATE TABLE party_relationship(
    idparrel        BINARY(16)    NOT NULL,
    idstatustype    INT,
    idreltyp        INT,
    idparrolfro     BINARY(16),
    idparrolto      BINARY(16),
    dtfrom          DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idparrel)
)ENGINE=INNODB
;



-- 
-- TABLE: party_role 
--

CREATE TABLE party_role(
    idparrol      BINARY(16)    NOT NULL,
    idroletype    INT,
    idparty       BINARY(16),
    dtregister    DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtfrom        DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idparrol)
)ENGINE=INNODB
;



-- 
-- TABLE: party_role_status 
--

CREATE TABLE party_role_status(
    idstatus        BINARY(16)      NOT NULL,
    idparrol        BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: party_role_type 
--

CREATE TABLE party_role_type(
    idparty       BINARY(16)    NOT NULL,
    idroletype    INT           NOT NULL,
    PRIMARY KEY (idparty, idroletype)
)ENGINE=INNODB
;



-- 
-- TABLE: payment 
--

CREATE TABLE payment(
    idpayment        BINARY(16)        NOT NULL,
    idpaytyp         INT,
    idpaymet         INT,
    idinternal       VARCHAR(30),
    idpaito          VARCHAR(30),
    idpaifro         VARCHAR(30),
    paymentnumber    VARCHAR(30),
    dtcreate         DATETIME          DEFAULT CURRENT_TIMESTAMP,
    amount           DECIMAL(18, 4),
    reffnum          VARCHAR(60),
    PRIMARY KEY (idpayment)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_acctg_trans 
--

CREATE TABLE payment_acctg_trans(
    idacctra     INT           NOT NULL,
    idpayment    BINARY(16),
    PRIMARY KEY (idacctra)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_application 
--

CREATE TABLE payment_application(
    idpayapp         BINARY(16)        NOT NULL,
    idpayment        BINARY(16),
    idbilacc         INT,
    idbilling        BINARY(16),
    amountapplied    DECIMAL(18, 4),
    PRIMARY KEY (idpayapp)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_method 
--

CREATE TABLE payment_method(
    idpaymet         INT            AUTO_INCREMENT,
    idpaymettyp      INT,
    refkey           VARCHAR(30),
    description      VARCHAR(60),
    accountnumber    VARCHAR(60),
    PRIMARY KEY (idpaymet)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_method_type 
--

CREATE TABLE payment_method_type(
    idpaymettyp    INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idpaymettyp)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_role 
--

CREATE TABLE payment_role(
    idrole        BINARY(16)     NOT NULL,
    idpayment     BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_status 
--

CREATE TABLE payment_status(
    idstatus        BINARY(16)      NOT NULL,
    idpayment       BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: payment_type 
--

CREATE TABLE payment_type(
    idpaytyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idpaytyp)
)ENGINE=INNODB
;



-- 
-- TABLE: pendingreason 
--

CREATE TABLE pendingreason(
    idpendingreason      INT             NOT NULL,
    descpendingreason    VARCHAR(250),
    lastmodifieddate     DATETIME,
    statuscode           INT,
    PRIMARY KEY (idpendingreason)
)ENGINE=INNODB
;



-- 
-- TABLE: person 
--

CREATE TABLE person(
    idparty             BINARY(16)     NOT NULL,
    fname               VARCHAR(30),
    lname               VARCHAR(30),
    pob                 VARCHAR(30),
    dtob                DATE,
    bloodtype           VARCHAR(10),
    gender              VARCHAR(1),
    personalidnumber    VARCHAR(30),
    familyidnumber      VARCHAR(30),
    taxidnumber         VARCHAR(30),
    idreligiontype      INT,
    idworktype          INT,
    cellphone1          VARCHAR(60),
    cellphone2          VARCHAR(60),
    privatemail         VARCHAR(60),
    phone               VARCHAR(60),
    username            VARCHAR(30),
    PRIMARY KEY (idparty)
)ENGINE=INNODB
;



-- 
-- TABLE: person_prospect 
--

CREATE TABLE person_prospect(
    idprospect      BINARY(16)     NOT NULL,
    idparty         BINARY(16),
    fname           VARCHAR(30),
    lname           VARCHAR(30),
    cellphone       VARCHAR(30),
    facilityname    VARCHAR(30),
    internname      VARCHAR(30),
    PRIMARY KEY (idprospect)
)ENGINE=INNODB
;



-- 
-- TABLE: person_suspect 
--

CREATE TABLE person_suspect(
    idsuspect    BINARY(16)    NOT NULL,
    idparty      BINARY(16),
    PRIMARY KEY (idsuspect)
)ENGINE=INNODB
;



-- 
-- TABLE: person_tm 
--

CREATE TABLE person_tm(
    idperson              INT            NOT NULL,
    name                  VARCHAR(50),
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    PRIMARY KEY (idperson)
)ENGINE=INNODB
;



-- 
-- TABLE: personal_customer 
--

CREATE TABLE personal_customer(
    idcustomer    VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idcustomer)
)ENGINE=INNODB
;



-- 
-- TABLE: personsosmed 
--

CREATE TABLE personsosmed(
    idsosmed        INT            AUTO_INCREMENT,
    idsosmedtype    INT,
    idparty         BINARY(16),
    username        VARCHAR(30),
    PRIMARY KEY (idsosmed)
)ENGINE=INNODB
;



-- 
-- TABLE: picking_slip 
--

CREATE TABLE picking_slip(
    idslip              BINARY(16)        NOT NULL,
    idinvite            BINARY(16),
    idordite            BINARY(16),
    idshipto            VARCHAR(30),
    qty                 DECIMAL(14, 4),
    acc1                INT               DEFAULT 0,
    acc2                INT               DEFAULT 0,
    acc3                INT               DEFAULT 0,
    acc4                INT               DEFAULT 0,
    acc5                INT               DEFAULT 0,
    acc6                INT               DEFAULT 0,
    acc7                INT               DEFAULT 0,
    acc8                INT               DEFAULT 0,
    acctambah1          INT               DEFAULT 0,
    acctambah2          INT               DEFAULT 0,
    promat1             INT               DEFAULT 0,
    promat2             INT               DEFAULT 0,
    promatdirectgift    INT               DEFAULT 0,
    idinternalmecha     BINARY(16),
    idexternalmecha     BINARY(16),
    PRIMARY KEY (idslip)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb 
--

CREATE TABLE pkb(
    idpkb                  INT               AUTO_INCREMENT,
    idinternal             VARCHAR(30),
    idstatuspkbinternal    INT,
    idstatuspkbcust        INT,
    idcustomer             VARCHAR(30),
    idqueue                INT,
    idvehicle              BINARY(16),
    idpaymettyp            INT,
    idtypepkb              INT,
    idvehide               BINARY(16),
    idpkbjobreturn         INT,
    nopkb                  VARCHAR(15)       NOT NULL,
    pkbdate                DATETIME,
    lamentation            VARCHAR(50),
    lastmodifieddate       DATE,
    lastmodifieduserid     INT,
    statuscode             INT,
    subtotalservice        DECIMAL(18, 2),
    subtotalpart           DECIMAL(18, 2),
    reason                 VARCHAR(255),
    kilometer              DECIMAL(14, 2),
    nextservicedate        DATETIME,
    jobsuggest             VARCHAR(50),
    PRIMARY KEY (idpkb)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_estimation 
--

CREATE TABLE pkb_estimation(
    idpkb                INT               NOT NULL,
    estimatedprice       DECIMAL(18, 0),
    depositcust          DECIMAL(18, 0),
    dp                   DECIMAL(18, 0),
    beawaited            BIT(1),
    estimatedduration    INT,
    hoursregist          INT,
    estimatedstart       INT,
    estimatedfinish      INT,
    PRIMARY KEY (idpkb)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_inspection 
--

CREATE TABLE pkb_inspection(
    idinspection    INT            AUTO_INCREMENT,
    description     VARCHAR(60),
    dtcreate        DATETIME,
    PRIMARY KEY (idinspection)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_part 
--

CREATE TABLE pkb_part(
    IdPkbPart             INT               NOT NULL,
    idpkbservice          INT,
    idproduct             VARCHAR(90),
    idpkb                 INT,
    idchargeto            INT,
    nopart                VARCHAR(300),
    qty                   DECIMAL(18, 4),
    het                   DECIMAL(18, 4),
    discount              DECIMAL(18, 4),
    unitprice             DECIMAL(18, 4),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    descpart              VARCHAR(270),
    totalamount           DECIMAL(18, 4),
    isrequest             TINYINT,
    isclaimhotline        TINYINT,
    taxamount             DECIMAL(18, 4),
    idcategory            INT,
    sequence              INT,
    qtyrequest            DECIMAL(18, 4),
    idinvite              VARCHAR(192),
    isdefaultpart         TINYINT,
    PRIMARY KEY (IdPkbPart)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_part_billing_item 
--

CREATE TABLE pkb_part_billing_item(
    idbilite     BINARY(16)    NOT NULL,
    idpkbpart    INT,
    idparrol     BINARY(16),
    PRIMARY KEY (idbilite)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_part_request_item 
--

CREATE TABLE pkb_part_request_item(
    idreqitem    BINARY(16)    NOT NULL,
    idpkbpart    INT,
    PRIMARY KEY (idreqitem)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_service 
--

CREATE TABLE pkb_service(
    IdPkbService             INT               NOT NULL,
    idpkb                    INT,
    idproduct                VARCHAR(30),
    idchargeto               INT,
    codeservice              VARCHAR(90),
    descservice              VARCHAR(270),
    unitprice                DECIMAL(18, 4),
    discount                 DECIMAL(18, 4),
    flatrate                 DECIMAL(5, 2),
    chargeto                 VARCHAR(60),
    issuggestedbymechanic    TINYINT,
    lastmodifieddate         DATE,
    lastmodifieduserid       INT,
    statuscode               INT,
    totalamount              DECIMAL(18, 4),
    isrequest                TINYINT,
    taxamount                DECIMAL(18, 4),
    idcategory               INT,
    sequence                 INT,
    standardrate             DECIMAL(18, 4),
    isjobreturn              TINYINT,
    baseprice                DECIMAL(18, 4),
    PRIMARY KEY (IdPkbService)
)ENGINE=INNODB
;



-- 
-- TABLE: pkb_service_billing_item 
--

CREATE TABLE pkb_service_billing_item(
    idbilite        BINARY(16)    NOT NULL,
    idparrol        BINARY(16),
    idpkbservice    INT,
    PRIMARY KEY (idbilite)
)ENGINE=INNODB
;



-- 
-- TABLE: po_detail_part 
--

CREATE TABLE po_detail_part(
    idordite              BINARY(16)    NOT NULL,
    idpkbpart             INT,
    lastmodifieddate      DATE,
    lastmodefieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idordite)
)ENGINE=INNODB
;



-- 
-- TABLE: po_detail_service 
--

CREATE TABLE po_detail_service(
    idordite              BINARY(16)    NOT NULL,
    idpkbservice          INT,
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idordite)
)ENGINE=INNODB
;



-- 
-- TABLE: po_status 
--

CREATE TABLE po_status(
    idpostatus      INT            NOT NULL,
    idstatustype    INT,
    description     VARCHAR(30),
    PRIMARY KEY (idpostatus)
)ENGINE=INNODB
;



-- 
-- TABLE: position_authority 
--

CREATE TABLE position_authority(
    idpostyp    INT            NOT NULL,
    name        VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idpostyp, name)
)ENGINE=INNODB
;



-- 
-- TABLE: position_fullfillment 
--

CREATE TABLE position_fullfillment(
    idposfulfil    INT            AUTO_INCREMENT,
    idposition     BINARY(16),
    idperson       BINARY(16),
    username       VARCHAR(30),
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idposfulfil)
)ENGINE=INNODB
;



-- 
-- TABLE: position_reporting_structure 
--

CREATE TABLE position_reporting_structure(
    idposrepstru    INT           AUTO_INCREMENT,
    idposfro        BINARY(16),
    idposto         BINARY(16),
    dtfrom          DATETIME      DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idposrepstru)
)ENGINE=INNODB
;



-- 
-- TABLE: position_type 
--

CREATE TABLE position_type(
    idpostyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    title          VARCHAR(30),
    PRIMARY KEY (idpostyp)
)ENGINE=INNODB
;



-- 
-- TABLE: positions 
--

CREATE TABLE positions(
    idposition        BINARY(16)     NOT NULL,
    idpostyp          INT,
    idorganization    BINARY(16),
    idusrmed          BINARY(16),
    idinternal        VARCHAR(30),
    seqnum            INT,
    username          VARCHAR(30),
    description       VARCHAR(60),
    PRIMARY KEY (idposition)
)ENGINE=INNODB
;



-- 
-- TABLE: positions_status 
--

CREATE TABLE positions_status(
    idstatus        BINARY(16)      NOT NULL,
    idposition      BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: postal_address 
--

CREATE TABLE postal_address(
    idcontact     BINARY(16)     NOT NULL,
    idprovince    BINARY(16),
    idcity        BINARY(16),
    iddistrict    BINARY(16),
    idvillage     BINARY(16),
    idpostal      BINARY(16),
    address       VARCHAR(60),
    address1      VARCHAR(60),
    address2      VARCHAR(60),
    PRIMARY KEY (idcontact)
)ENGINE=INNODB
;



-- 
-- TABLE: price_agreement_item 
--

CREATE TABLE price_agreement_item(
    idagrite     BINARY(16)     NOT NULL,
    idproduct    VARCHAR(30),
    PRIMARY KEY (idagrite)
)ENGINE=INNODB
;



-- 
-- TABLE: price_component 
--

CREATE TABLE price_component(
    idpricom          BINARY(16)        NOT NULL,
    idparty           BINARY(16),
    idproduct         VARCHAR(30),
    idpricetype       INT,
    idparcat          INT,
    idprocat          INT,
    idgeobou          BINARY(16),
    idagrite          BINARY(16),
    price             DECIMAL(18, 4),
    manfucterprice    DECIMAL(18, 4),
    percentvalue      DECIMAL(10, 4)    DEFAULT 0,
    standardrate      DECIMAL(18, 4),
    dtfrom            DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtthru            DATETIME          DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idpricom)
)ENGINE=INNODB
;



-- 
-- TABLE: price_type 
--

CREATE TABLE price_type(
    idpricetype    INT            NOT NULL,
    idrultyp       INT,
    description    VARCHAR(60),
    PRIMARY KEY (idpricetype)
)ENGINE=INNODB
;



-- 
-- TABLE: product 
--

CREATE TABLE product(
    idproduct         VARCHAR(30)    NOT NULL,
    idprotyp          INT,
    name              VARCHAR(30),
    description       VARCHAR(60),
    dtintroduction    DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtdiscontinue     DATE,
    istaxable         INT            DEFAULT 0,
    pricetype         INT,
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: product_association 
--

CREATE TABLE product_association(
    idproass         BINARY(16)        NOT NULL,
    idasstyp         INT,
    idproductto      VARCHAR(30),
    idproductfrom    VARCHAR(30),
    qty              DECIMAL(14, 4),
    dtfrom           DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtthru           DATETIME          DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idproass)
)ENGINE=INNODB
;



-- 
-- TABLE: product_category 
--

CREATE TABLE product_category(
    idcategory     INT            AUTO_INCREMENT,
    idcattyp       INT,
    refkey         VARCHAR(30),
    description    VARCHAR(60),
    PRIMARY KEY (idcategory)
)ENGINE=INNODB
;



-- 
-- TABLE: product_category_impl 
--

CREATE TABLE product_category_impl(
    idproduct     VARCHAR(30)    NOT NULL,
    idcategory    INT            NOT NULL,
    PRIMARY KEY (idproduct, idcategory)
)ENGINE=INNODB
;



-- 
-- TABLE: product_category_type 
--

CREATE TABLE product_category_type(
    idcattyp       INT            AUTO_INCREMENT,
    idparent       INT,
    idrultyp       INT,
    description    VARCHAR(60),
    refkey         VARCHAR(30),
    PRIMARY KEY (idcattyp)
)ENGINE=INNODB
;



-- 
-- TABLE: product_classification 
--

CREATE TABLE product_classification(
    idclassification    BINARY(16)     NOT NULL,
    idproduct           VARCHAR(30),
    idcategory          INT,
    idcattyp            INT,
    idowner             BINARY(16),
    dtfrom              DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru              DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idclassification)
)ENGINE=INNODB
;



-- 
-- TABLE: product_document 
--

CREATE TABLE product_document(
    iddocument     BINARY(16)     NOT NULL,
    idproduct      VARCHAR(30),
    contenttype    VARCHAR(60),
    content        BLOB,
    PRIMARY KEY (iddocument)
)ENGINE=INNODB
;



-- 
-- TABLE: product_feature_impl 
--

CREATE TABLE product_feature_impl(
    products_id    VARCHAR(30)    NOT NULL,
    features_id    INT            NOT NULL,
    PRIMARY KEY (products_id, features_id)
)ENGINE=INNODB
;



-- 
-- TABLE: product_package_receipt 
--

CREATE TABLE product_package_receipt(
    idpackage         BINARY(16)     NOT NULL,
    idshifro          VARCHAR(30),
    documentnumber    VARCHAR(30),
    PRIMARY KEY (idpackage)
)ENGINE=INNODB
;



-- 
-- TABLE: product_purchase_order 
--

CREATE TABLE product_purchase_order(
    idorder    BINARY(16)    NOT NULL,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: product_shipment_incoming 
--

CREATE TABLE product_shipment_incoming(
    idshipment    BINARY(16)    NOT NULL,
    PRIMARY KEY (idshipment)
)ENGINE=INNODB
;



-- 
-- TABLE: product_shipment_receipt 
--

CREATE TABLE product_shipment_receipt(
    idreceipt    BINARY(16)    NOT NULL,
    PRIMARY KEY (idreceipt)
)ENGINE=INNODB
;



-- 
-- TABLE: product_type 
--

CREATE TABLE product_type(
    idprotyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idprotyp)
)ENGINE=INNODB
;



-- 
-- TABLE: prospect 
--

CREATE TABLE prospect(
    idprospect            BINARY(16)      NOT NULL,
    idbroker              BINARY(16),
    idprosou              INT,
    idevetyp              INT,
    idfacility            BINARY(16),
    idsalesman            BINARY(16),
    idsuspect             BINARY(16),
    idsalescoordinator    BINARY(16),
    idinternal            VARCHAR(30),
    prospectnumber        VARCHAR(30),
    prospectcount         INT,
    eveloc                VARCHAR(300),
    dtfollowup            DATETIME        DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idprospect)
)ENGINE=INNODB
;



-- 
-- TABLE: prospect_role 
--

CREATE TABLE prospect_role(
    idrole        BINARY(16)     NOT NULL,
    idprospect    BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: prospect_source 
--

CREATE TABLE prospect_source(
    idprosou       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idprosou)
)ENGINE=INNODB
;



-- 
-- TABLE: prospect_status 
--

CREATE TABLE prospect_status(
    idstatus        BINARY(16)      NOT NULL,
    idprospect      BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: province 
--

CREATE TABLE province(
    idgeobou    BINARY(16)    NOT NULL,
    PRIMARY KEY (idgeobou)
)ENGINE=INNODB
;



-- 
-- TABLE: purchase_order 
--

CREATE TABLE purchase_order(
    idorder    BINARY(16)      NOT NULL,
    reason     VARCHAR(300),
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: purchase_request 
--

CREATE TABLE purchase_request(
    idreq         BINARY(16)     NOT NULL,
    idproduct     VARCHAR(30),
    idinternal    VARCHAR(30),
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: purchaseorder_reception_tr 
--

CREATE TABLE purchaseorder_reception_tr(
    idpurchaseorderreception    INT               NOT NULL,
    idpurchaseorder             INT,
    idcategoryasset             INT,
    quantitypurchase            INT,
    quantityaccepted            INT,
    lastmodifieddate            DATETIME,
    lastmodifieduserid          INT,
    itemname                    VARCHAR(30),
    price                       DECIMAL(18, 2),
    branchcode                  VARCHAR(30),
    typecode                    VARCHAR(30),
    merk                        VARCHAR(30),
    type                        VARCHAR(30),
    PRIMARY KEY (idpurchaseorderreception)
)ENGINE=INNODB
;



-- 
-- TABLE: purchaseorder_status_tp 
--

CREATE TABLE purchaseorder_status_tp(
    idpurchaseorderstatus    INT            NOT NULL,
    description              VARCHAR(50),
    lastmodifieddate         DATETIME,
    lastmodifieduserid       INT,
    PRIMARY KEY (idpurchaseorderstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: purchaseorder_tm 
--

CREATE TABLE purchaseorder_tm(
    idpurchaseorder       INT             AUTO_INCREMENT,
    ponumber              VARCHAR(15),
    podate                DATETIME,
    spgdate               DATETIME,
    spgnumber             VARCHAR(20),
    vendorname            VARCHAR(500),
    status                INT,
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    PRIMARY KEY (idpurchaseorder)
)ENGINE=INNODB
;



-- 
-- TABLE: purpose_type 
--

CREATE TABLE purpose_type(
    idpurposetype    INT            AUTO_INCREMENT,
    description      VARCHAR(60),
    PRIMARY KEY (idpurposetype)
)ENGINE=INNODB
;



-- 
-- TABLE: queue 
--

CREATE TABLE queue(
    idqueue               INT             AUTO_INCREMENT,
    idqueuestatus         INT,
    idqueuetype           INT,
    idbooking             INT,
    noqueue               VARCHAR(10)     NOT NULL,
    customername          VARCHAR(100),
    vehiclenumber         VARCHAR(20),
    datequeue             DATETIME,
    waitingduration       INT,
    ismissed              BIT(1),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idqueue)
)ENGINE=INNODB
;



-- 
-- TABLE: queue_status 
--

CREATE TABLE queue_status(
    idqueuestatus    INT            NOT NULL,
    description      VARCHAR(30),
    PRIMARY KEY (idqueuestatus)
)ENGINE=INNODB
;



-- 
-- TABLE: queue_type 
--

CREATE TABLE queue_type(
    idqueuetype    INT            NOT NULL,
    type           VARCHAR(30),
    PRIMARY KEY (idqueuetype)
)ENGINE=INNODB
;



-- 
-- TABLE: quote 
--

CREATE TABLE quote(
    idquote        BINARY(16)     NOT NULL,
    idquotyp       INT,
    description    VARCHAR(60),
    dtissued       DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    dteffective    DATETIME       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idquote)
)ENGINE=INNODB
;



-- 
-- TABLE: quote_item 
--

CREATE TABLE quote_item(
    idquoteitem    BINARY(16)        NOT NULL,
    idquote        BINARY(16),
    idproduct      VARCHAR(30),
    qty            DECIMAL(14, 4),
    unitprice      DECIMAL(18, 4),
    pricetype      INT,
    serialized     INT               DEFAULT 0,
    istaxable      INT               DEFAULT 0,
    PRIMARY KEY (idquoteitem)
)ENGINE=INNODB
;



-- 
-- TABLE: quote_role 
--

CREATE TABLE quote_role(
    idrole        BINARY(16)     NOT NULL,
    idquote       BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: quote_status 
--

CREATE TABLE quote_status(
    idstatus        BINARY(16)      NOT NULL,
    idquote         BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: quote_type 
--

CREATE TABLE quote_type(
    idquotyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idquotyp)
)ENGINE=INNODB
;



-- 
-- TABLE: reason_type 
--

CREATE TABLE reason_type(
    idreason       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idreason)
)ENGINE=INNODB
;



-- 
-- TABLE: receipt 
--

CREATE TABLE receipt(
    idpayment    BINARY(16)    NOT NULL,
    idreq        BINARY(16),
    PRIMARY KEY (idpayment)
)ENGINE=INNODB
;



-- 
-- TABLE: regular_sales_order 
--

CREATE TABLE regular_sales_order(
    idorder    BINARY(16)    NOT NULL,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: reimbursement_part 
--

CREATE TABLE reimbursement_part(
    idspgclaimaction    INT,
    idproduct           VARCHAR(30),
    idspgclaimdetail    INT               NOT NULL,
    qty                 DECIMAL(14, 2),
    dtreimpart          DATETIME,
    PRIMARY KEY (idspgclaimdetail)
)ENGINE=INNODB
;



-- 
-- TABLE: relation_type 
--

CREATE TABLE relation_type(
    idreltyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idreltyp)
)ENGINE=INNODB
;



-- 
-- TABLE: religion_type 
--

CREATE TABLE religion_type(
    idreligiontype    INT            AUTO_INCREMENT,
    description       VARCHAR(60),
    PRIMARY KEY (idreligiontype)
)ENGINE=INNODB
;



-- 
-- TABLE: rem_part 
--

CREATE TABLE rem_part(
    idproduct       VARCHAR(30)       NOT NULL,
    minimumstock    DECIMAL(14, 4),
    qtysuggest      DECIMAL(14, 4),
    safetystock     DECIMAL(14, 4),
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: reminder_sent_status 
--

CREATE TABLE reminder_sent_status(
    idsensta        BINARY(16)     NOT NULL,
    idreminder      INT,
    cellphone       VARCHAR(30),
    currentstate    INT,
    dtcreated       DATETIME       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idsensta)
)ENGINE=INNODB
;



-- 
-- TABLE: reorder_guideline 
--

CREATE TABLE reorder_guideline(
    idreogui      INT               AUTO_INCREMENT,
    idproduct     VARCHAR(30),
    idinternal    VARCHAR(30),
    qtyreorder    DECIMAL(14, 4),
    qtylevel      DECIMAL(14, 4),
    dtfrom        DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME          DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idreogui)
)ENGINE=INNODB
;



-- 
-- TABLE: request 
--

CREATE TABLE request(
    idrequest      BINARY(16)     NOT NULL,
    idreqtype      INT,
    idinternal     VARCHAR(30),
    reqnum         VARCHAR(30),
    dtcreate       DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtrequest      DATETIME       DEFAULT CURRENT_TIMESTAMP,
    description    VARCHAR(60),
    PRIMARY KEY (idrequest)
)ENGINE=INNODB
;



-- 
-- TABLE: request_item 
--

CREATE TABLE request_item(
    idreqitem      BINARY(16)        NOT NULL,
    idrequest      BINARY(16),
    idproduct      VARCHAR(30),
    idfeature      INT,
    seq            INT,
    description    VARCHAR(60),
    qtyreq         DECIMAL(14, 4),
    qtytransfer    DECIMAL(14, 4),
    qtyorder       DECIMAL(14, 4),
    PRIMARY KEY (idreqitem)
)ENGINE=INNODB
;



-- 
-- TABLE: request_product 
--

CREATE TABLE request_product(
    idrequest         BINARY(16)    NOT NULL,
    idfacilityfrom    BINARY(16),
    idfacilityto      BINARY(16),
    PRIMARY KEY (idrequest)
)ENGINE=INNODB
;



-- 
-- TABLE: request_requirement 
--

CREATE TABLE request_requirement(
    idrequest        BINARY(16)        NOT NULL,
    idrequirement    BINARY(16),
    qty              DECIMAL(14, 4),
    PRIMARY KEY (idrequest)
)ENGINE=INNODB
;



-- 
-- TABLE: request_role 
--

CREATE TABLE request_role(
    idrole        BINARY(16)     NOT NULL,
    idrequest     BINARY(16),
    idroletype    INT,
    idparty       BINARY(16),
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: request_status 
--

CREATE TABLE request_status(
    idstatus        BINARY(16)      NOT NULL,
    idrequest       BINARY(16),
    idreason        INT,
    idstatustype    INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: request_type 
--

CREATE TABLE request_type(
    idreqtype      INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idreqtype)
)ENGINE=INNODB
;



-- 
-- TABLE: request_unit_internal 
--

CREATE TABLE request_unit_internal(
    idrequest       BINARY(16)     NOT NULL,
    idparent        BINARY(16),
    idinternalto    VARCHAR(30),
    PRIMARY KEY (idrequest)
)ENGINE=INNODB
;



-- 
-- TABLE: requirement 
--

CREATE TABLE requirement(
    idreq                BINARY(16)        NOT NULL,
    idfacility           BINARY(16),
    idparentreq          BINARY(16),
    idreqtyp             INT,
    requirementnumber    VARCHAR(30),
    description          VARCHAR(60),
    dtcreate             DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtrequired           DATE,
    budget               DECIMAL(18, 4),
    qty                  DECIMAL(14, 4),
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: requirement_order_item 
--

CREATE TABLE requirement_order_item(
    idreqordite    BINARY(16)        NOT NULL,
    idreq          BINARY(16),
    idordite       BINARY(16),
    qty            DECIMAL(14, 4),
    PRIMARY KEY (idreqordite)
)ENGINE=INNODB
;



-- 
-- TABLE: requirement_payment 
--

CREATE TABLE requirement_payment(
    idreqpayite        BINARY(16)        NOT NULL,
    requirements_id    BINARY(16),
    idpayment          BINARY(16),
    amount             DECIMAL(18, 4),
    PRIMARY KEY (idreqpayite)
)ENGINE=INNODB
;



-- 
-- TABLE: requirement_role 
--

CREATE TABLE requirement_role(
    idrole        BINARY(16)     NOT NULL,
    idreq         BINARY(16),
    idroletype    INT,
    idparty       BINARY(16)     NOT NULL,
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: requirement_status 
--

CREATE TABLE requirement_status(
    idstatus        BINARY(16)      NOT NULL,
    idreq           BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: requirement_type 
--

CREATE TABLE requirement_type(
    idreqtyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idreqtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: return_purchase_order 
--

CREATE TABLE return_purchase_order(
    idorder    BINARY(16)    NOT NULL,
    idissue    INT,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: return_sales_order 
--

CREATE TABLE return_sales_order(
    idorder    BINARY(16)    NOT NULL,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: role_type 
--

CREATE TABLE role_type(
    idroletype     INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idroletype)
)ENGINE=INNODB
;



-- 
-- TABLE: room_tm 
--

CREATE TABLE room_tm(
    idroom                INT            AUTO_INCREMENT,
    idfloor               INT,
    roomname              VARCHAR(50),
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idroom)
)ENGINE=INNODB
;



-- 
-- TABLE: rule_hot_item 
--

CREATE TABLE rule_hot_item(
    idrule            INT               NOT NULL,
    idproduct         VARCHAR(30),
    mindownpayment    DECIMAL(18, 4),
    PRIMARY KEY (idrule)
)ENGINE=INNODB
;



-- 
-- TABLE: rule_indent 
--

CREATE TABLE rule_indent(
    idrule        INT               NOT NULL,
    idproduct     VARCHAR(30),
    minpayment    DECIMAL(18, 4),
    PRIMARY KEY (idrule)
)ENGINE=INNODB
;



-- 
-- TABLE: rule_sales_discount 
--

CREATE TABLE rule_sales_discount(
    idrule       INT               NOT NULL,
    maxamount    DECIMAL(18, 4),
    name         VARCHAR(30),
    PRIMARY KEY (idrule)
)ENGINE=INNODB
;



-- 
-- TABLE: rule_type 
--

CREATE TABLE rule_type(
    idrultyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idrultyp)
)ENGINE=INNODB
;



-- 
-- TABLE: rules 
--

CREATE TABLE rules(
    idrule         INT            AUTO_INCREMENT,
    idrultyp       INT,
    idinternal     VARCHAR(30),
    seqnum         INT,
    description    VARCHAR(60),
    dtfrom         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrule)
)ENGINE=INNODB
;



-- 
-- TABLE: sale_type 
--

CREATE TABLE sale_type(
    idsaletype     INT            NOT NULL,
    idparent       INT,
    description    VARCHAR(60),
    PRIMARY KEY (idsaletype)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_agreement 
--

CREATE TABLE sales_agreement(
    idagreement    BINARY(16)     NOT NULL,
    idinternal     VARCHAR(30),
    idcustomer     VARCHAR(30),
    PRIMARY KEY (idagreement)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_booking 
--

CREATE TABLE sales_booking(
    idslsboo        BINARY(16)      NOT NULL,
    idinternal      VARCHAR(30),
    idreq           BINARY(16),
    idproduct       VARCHAR(30),
    idfeature       INT,
    note            VARCHAR(300),
    yearassembly    INT,
    onhand          INT             DEFAULT 0,
    intransit       INT             DEFAULT 0,
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idslsboo)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_broker 
--

CREATE TABLE sales_broker(
    idparrol    BINARY(16)     NOT NULL,
    idbrotyp    INT,
    idbroker    VARCHAR(30),
    PRIMARY KEY (idparrol)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_order 
--

CREATE TABLE sales_order(
    idorder       BINARY(16)    NOT NULL,
    idsaletype    INT,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_point 
--

CREATE TABLE sales_point(
    idinternal    VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idinternal)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_unit_leasing 
--

CREATE TABLE sales_unit_leasing(
    idsallea        BINARY(16)     NOT NULL,
    idreq           BINARY(16),
    idstatustype    INT,
    idorder         BINARY(16),
    idlsgpro        BINARY(16),
    idleacom        BINARY(16),
    ponumber        VARCHAR(30),
    dtpo            DATE,
    dtfrom          DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idsallea)
)ENGINE=INNODB
;



-- 
-- TABLE: sales_unit_requirement 
--

CREATE TABLE sales_unit_requirement(
    idreq                       BINARY(16)        NOT NULL,
    idprospect                  BINARY(16),
    idsaletype                  INT,
    idsalesman                  BINARY(16),
    idproduct                   VARCHAR(30),
    idcolor                     INT,
    idsalesbroker               BINARY(16),
    idowner                     BINARY(16),
    idlsgpro                    BINARY(16),
    idinternal                  VARCHAR(30),
    idbillto                    VARCHAR(30),
    idcustomer                  VARCHAR(30),
    idrequest                   BINARY(16),
    idorganizationowner         BINARY(16),
    downpayment                 DECIMAL(18, 4),
    idframe                     VARCHAR(30),
    idmachine                   VARCHAR(30),
    subsfincomp                 DECIMAL(18, 4),
    subsmd                      DECIMAL(18, 4),
    subsown                     DECIMAL(18, 4),
    subsahm                     DECIMAL(18, 4),
    bbnprice                    DECIMAL(18, 4),
    hetprice                    DECIMAL(18, 4),
    note                        VARCHAR(300),
    unitprice                   DECIMAL(18, 4),
    notepartner                 VARCHAR(300),
    shipmentnote                VARCHAR(300),
    requestpoliceid             VARCHAR(30),
    leasingpo                   VARCHAR(30),
    dtpoleasing                 DATETIME          DEFAULT CURRENT_TIMESTAMP,
    brokerfee                   DECIMAL(18, 4),
    idprosou                    INT,
    prodyear                    INT,
    requestidmachine            VARCHAR(30),
    requestidframe              VARCHAR(30),
    requestidmachineandframe    VARCHAR(30),
    minpayment                  DECIMAL(18, 4),
    isunithotitem               INT               DEFAULT 0,
    isunitindent                INT               DEFAULT 0,
    waitstnk                    INT               DEFAULT 0,
    creditinstallment           DECIMAL(18, 4),
    creditdownpayment           DECIMAL(18, 4),
    credittenor                 INT,
    idleasing                   VARCHAR(30),
    podate                      DATETIME          DEFAULT CURRENT_TIMESTAMP,
    ponumber                    VARCHAR(30),
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: salesman 
--

CREATE TABLE salesman(
    idparrol         BINARY(16)     NOT NULL,
    idcoordinator    BINARY(16),
    idsalesman       VARCHAR(30),
    coordinator      INT            DEFAULT 0,
    idteamleader     BINARY(16),
    teamleader       INT            DEFAULT 0,
    ahmcode          VARCHAR(30),
    PRIMARY KEY (idparrol)
)ENGINE=INNODB
;



-- 
-- TABLE: service 
--

CREATE TABLE service(
    idproduct            VARCHAR(30)    NOT NULL,
    iduom                VARCHAR(30),
    frt                  INT,
    idchargetopart       INT,
    idchargetoservice    INT,
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: service_agreement 
--

CREATE TABLE service_agreement(
    idagreement    BINARY(16)     NOT NULL,
    idcustomer     VARCHAR(30),
    idinternal     VARCHAR(30),
    PRIMARY KEY (idagreement)
)ENGINE=INNODB
;



-- 
-- TABLE: service_history 
--

CREATE TABLE service_history(
    idservicehistory           INT            AUTO_INCREMENT,
    idvehicleservicehistory    INT,
    codeservice                VARCHAR(30),
    descservice                VARCHAR(30),
    PRIMARY KEY (idservicehistory)
)ENGINE=INNODB
;



-- 
-- TABLE: service_kpb 
--

CREATE TABLE service_kpb(
    idproduct            VARCHAR(30)       NOT NULL,
    km                   DECIMAL(12, 2),
    expireddate          DATETIME,
    range_               DECIMAL(12, 2),
    daylimit             DECIMAL(18, 0),
    tolerancekm          DECIMAL(12, 2),
    tolerancedaylimit    DECIMAL(18, 0),
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: service_per_motor 
--

CREATE TABLE service_per_motor(
    idproduct    VARCHAR(30)    NOT NULL,
    PRIMARY KEY (idproduct)
)ENGINE=INNODB
;



-- 
-- TABLE: service_reminder 
--

CREATE TABLE service_reminder(
    idreminder        INT           AUTO_INCREMENT,
    idclaimtype       INT,
    idremindertype    INT,
    idvehide          BINARY(16),
    days              INT,
    PRIMARY KEY (idreminder)
)ENGINE=INNODB
;



-- 
-- TABLE: service_reminder_event 
--

CREATE TABLE service_reminder_event(
    idcomevt       BINARY(16)     NOT NULL,
    idreminder     INT,
    description    VARCHAR(60),
    PRIMARY KEY (idcomevt)
)ENGINE=INNODB
;



-- 
-- TABLE: ship_to 
--

CREATE TABLE ship_to(
    idshipto      VARCHAR(30)    NOT NULL,
    idparty       BINARY(16),
    idroletype    INT,
    PRIMARY KEY (idshipto)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment 
--

CREATE TABLE shipment(
    idshipment        BINARY(16)      NOT NULL,
    idinternal        VARCHAR(30),
    idshito           VARCHAR(30),
    idshifro          VARCHAR(30),
    idshityp          INT,
    idfacility        BINARY(16),
    idposaddfro       BINARY(16),
    idposaddto        BINARY(16),
    shipmentnumber    VARCHAR(30),
    description       VARCHAR(60),
    dtschedulle       DATETIME        DEFAULT CURRENT_TIMESTAMP,
    reason            VARCHAR(255),
    PRIMARY KEY (idshipment)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_billing_item 
--

CREATE TABLE shipment_billing_item(
    idshibilite    BINARY(16)        NOT NULL,
    idshiite       BINARY(16),
    idbilite       BINARY(16),
    qty            DECIMAL(14, 4),
    PRIMARY KEY (idshibilite)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_incoming 
--

CREATE TABLE shipment_incoming(
    idshipment    BINARY(16)    NOT NULL,
    PRIMARY KEY (idshipment)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_item 
--

CREATE TABLE shipment_item(
    idshiite              BINARY(16)        NOT NULL,
    idshipment            BINARY(16),
    idfeature             INT,
    idproduct             VARCHAR(30),
    itemdescription       VARCHAR(60),
    qty                   DECIMAL(14, 4),
    contentdescription    VARCHAR(60),
    idframe               VARCHAR(60),
    idmachine             VARCHAR(60),
    PRIMARY KEY (idshiite)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_outgoing 
--

CREATE TABLE shipment_outgoing(
    idshipment         BINARY(16)      NOT NULL,
    dtbast             DATE,
    note               VARCHAR(300),
    othername          VARCHAR(30),
    deliveryopt        VARCHAR(300),
    deliveryaddress    VARCHAR(300),
    printcount         INT,
    idparrol           BINARY(16),
    PRIMARY KEY (idshipment)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_package 
--

CREATE TABLE shipment_package(
    idpackage      BINARY(16)     NOT NULL,
    idshipactyp    INT,
    idinternal     VARCHAR(30),
    dtcreated      DATETIME       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idpackage)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_package_role 
--

CREATE TABLE shipment_package_role(
    idrole        BINARY(16)     NOT NULL,
    idparty       BINARY(16),
    idroletype    INT,
    idpackage     BINARY(16),
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_package_status 
--

CREATE TABLE shipment_package_status(
    idstatus        BINARY(16)      NOT NULL,
    idstatustype    INT,
    idreason        INT,
    idpackage       BINARY(16),
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_package_type 
--

CREATE TABLE shipment_package_type(
    idshipactyp    INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idshipactyp)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_receipt 
--

CREATE TABLE shipment_receipt(
    idreceipt          BINARY(16)        NOT NULL,
    idproduct          VARCHAR(30),
    idpackage          BINARY(16),
    idfeature          INT,
    idordite           BINARY(16),
    idshiite           BINARY(16),
    code               VARCHAR(30),
    qtyaccept          DECIMAL(14, 4),
    qtyreject          DECIMAL(14, 4),
    itemdescription    VARCHAR(60),
    dtreceipt          DATETIME          DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idreceipt)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_receipt_role 
--

CREATE TABLE shipment_receipt_role(
    idrole        BINARY(16)     NOT NULL,
    idreceipt     BINARY(16),
    idparty       BINARY(16),
    idroletype    INT,
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_receipt_status 
--

CREATE TABLE shipment_receipt_status(
    idstatus        BINARY(16)      NOT NULL,
    idreceipt       BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_status 
--

CREATE TABLE shipment_status(
    idstatus        BINARY(16)      NOT NULL,
    idshipment      BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: shipment_type 
--

CREATE TABLE shipment_type(
    idshityp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idshityp)
)ENGINE=INNODB
;



-- 
-- TABLE: sosmedtype 
--

CREATE TABLE sosmedtype(
    idsosmedtype    INT            NOT NULL,
    description     VARCHAR(30),
    PRIMARY KEY (idsosmedtype)
)ENGINE=INNODB
;



-- 
-- TABLE: spg_claim 
--

CREATE TABLE spg_claim(
    idspgclaim    INT     AUTO_INCREMENT,
    dtclaim       DATE,
    PRIMARY KEY (idspgclaim)
)ENGINE=INNODB
;



-- 
-- TABLE: spg_claim_action 
--

CREATE TABLE spg_claim_action(
    idspgclaimaction    INT            AUTO_INCREMENT,
    claimto             VARCHAR(90),
    receivedby          VARCHAR(90),
    releasedby          VARCHAR(90),
    approvedby          VARCHAR(90),
    bastnumber          VARCHAR(30),
    PRIMARY KEY (idspgclaimaction)
)ENGINE=INNODB
;



-- 
-- TABLE: spg_claim_detail 
--

CREATE TABLE spg_claim_detail(
    idspgclaimdetail        INT               AUTO_INCREMENT,
    idspgclaim              INT,
    idproduct               VARCHAR(30),
    idreceipt               BINARY(16),
    idspgclaimtype          INT,
    idstatustype            INT,
    idbilite                BINARY(16),
    qty                     DECIMAL(14, 2),
    qtyreject               DECIMAL(14, 2),
    isnonactiveqty          BIT(1),
    description             VARCHAR(150),
    isnonactiveqtyreject    BIT(1),
    dtnonactivepart         DATETIME,
    PRIMARY KEY (idspgclaimdetail)
)ENGINE=INNODB
;



-- 
-- TABLE: spg_claim_type 
--

CREATE TABLE spg_claim_type(
    idspgclaimtype    INT            AUTO_INCREMENT,
    description       VARCHAR(50),
    PRIMARY KEY (idspgclaimtype)
)ENGINE=INNODB
;



-- 
-- TABLE: standard_calendar 
--

CREATE TABLE standard_calendar(
    idcalendar    INT            NOT NULL,
    idinternal    VARCHAR(30),
    PRIMARY KEY (idcalendar)
)ENGINE=INNODB
;



-- 
-- TABLE: status_pkb 
--

CREATE TABLE status_pkb(
    idstatuspkb           INT            NOT NULL,
    idstatustype          INT,
    descstatuspkb         VARCHAR(30),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idstatuspkb)
)ENGINE=INNODB
;



-- 
-- TABLE: status_type 
--

CREATE TABLE status_type(
    idstatustype    INT            NOT NULL,
    description     VARCHAR(60),
    PRIMARY KEY (idstatustype)
)ENGINE=INNODB
;



-- 
-- TABLE: stock_opname 
--

CREATE TABLE stock_opname(
    idstkop              BINARY(16)     NOT NULL,
    idstkopntyp          INT,
    idinternal           VARCHAR(30),
    idfacility           BINARY(16),
    idcalendar           INT,
    dtcreated            DATETIME       DEFAULT CURRENT_TIMESTAMP,
    stockopnamenumber    VARCHAR(30),
    PRIMARY KEY (idstkop)
)ENGINE=INNODB
;



-- 
-- TABLE: stock_opname_inventory 
--

CREATE TABLE stock_opname_inventory(
    idstkopninv    BINARY(16)        NOT NULL,
    idinvite       BINARY(16),
    idstopnite     BINARY(16),
    qty            DECIMAL(14, 4),
    PRIMARY KEY (idstkopninv)
)ENGINE=INNODB
;



-- 
-- TABLE: stock_opname_item 
--

CREATE TABLE stock_opname_item(
    idstopnite         BINARY(16)        NOT NULL,
    idstkop            BINARY(16),
    idproduct          VARCHAR(30),
    idcontainer        BINARY(16),
    itemdescription    VARCHAR(60),
    qty                DECIMAL(14, 4),
    qtycount           DECIMAL(14, 4),
    tagnumber          VARCHAR(30),
    checked            INT               DEFAULT 0,
    het                DECIMAL(18, 4),
    PRIMARY KEY (idstopnite)
)ENGINE=INNODB
;



-- 
-- TABLE: stock_opname_status 
--

CREATE TABLE stock_opname_status(
    idstatus        BINARY(16)      NOT NULL,
    idstkop         BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: stock_opname_type 
--

CREATE TABLE stock_opname_type(
    idstkopntyp    INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idstkopntyp)
)ENGINE=INNODB
;



-- 
-- TABLE: stockopname_item_tm 
--

CREATE TABLE stockopname_item_tm(
    idstockopnameitem     INT         AUTO_INCREMENT,
    idstockopname         INT,
    idfixedasset          INT,
    idroom                INT,
    idfloor               INT,
    quantitysystem        INT,
    quantityfound         INT,
    bitfound              BIT(1),
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    PRIMARY KEY (idstockopnameitem)
)ENGINE=INNODB
;



-- 
-- TABLE: stockopname_status_tp 
--

CREATE TABLE stockopname_status_tp(
    idstockopnamestatus    INT            NOT NULL,
    description            VARCHAR(10),
    lastmodifieddate       DATETIME,
    lastmodifieduserid     INT,
    PRIMARY KEY (idstockopnamestatus)
)ENGINE=INNODB
;



-- 
-- TABLE: stockopname_tm 
--

CREATE TABLE stockopname_tm(
    idstockopname         INT            AUTO_INCREMENT,
    stockopnamenumber     VARCHAR(20),
    stockopnamedate       DATETIME,
    status                INT,
    lastmodifieddate      DATETIME,
    lastmodifieduserid    INT,
    PRIMARY KEY (idstockopname)
)ENGINE=INNODB
;



-- 
-- TABLE: suggest_part 
--

CREATE TABLE suggest_part(
    idsuggestpart     INT               AUTO_INCREMENT,
    idproductmotor    VARCHAR(30),
    idproductpart     VARCHAR(30),
    km                DECIMAL(12, 2),
    ismultiply        BIT(1),
    range_            DECIMAL(12, 2),
    PRIMARY KEY (idsuggestpart)
)ENGINE=INNODB
;



-- 
-- TABLE: summary 
--

CREATE TABLE summary(
    idsummary             INT               AUTO_INCREMENT,
    idpkb                 INT,
    totalunit             INT,
    serviceqty            INT,
    njbmechanic           DECIMAL(18, 2),
    nscmechanic           DECIMAL(18, 2),
    totalnjbmec           DECIMAL(18, 2),
    totalnscmec           DECIMAL(18, 2),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idsummary)
)ENGINE=INNODB
;



-- 
-- TABLE: suspect 
--

CREATE TABLE suspect(
    idsuspect             BINARY(16)        NOT NULL,
    suspectnumber         VARCHAR(30),
    idparty               BINARY(16),
    idsalescoordinator    BINARY(16),
    idsalesman            BINARY(16),
    idaddress             BINARY(16),
    idsuspecttyp          INT,
    iddealer              VARCHAR(30),
    salesdt               DATETIME          DEFAULT CURRENT_TIMESTAMP,
    marketname            VARCHAR(30),
    repurchaseprob        DECIMAL(10, 4)    DEFAULT 0,
    priority              DECIMAL(10, 4)    DEFAULT 0,
    idsaletype            INT,
    idlsgpro              BINARY(16),
    PRIMARY KEY (idsuspect)
)ENGINE=INNODB
;



-- 
-- TABLE: suspect_role 
--

CREATE TABLE suspect_role(
    idrole        BINARY(16)     NOT NULL,
    idsuspect     BINARY(16),
    idparty       BINARY(16),
    idroletype    INT,
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: suspect_status 
--

CREATE TABLE suspect_status(
    idstatus        BINARY(16)      NOT NULL,
    idsuspect       BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: suspect_type 
--

CREATE TABLE suspect_type(
    idsuspecttyp    INT            NOT NULL,
    description     VARCHAR(60),
    PRIMARY KEY (idsuspecttyp)
)ENGINE=INNODB
;



-- 
-- TABLE: sympthom_data_m 
--

CREATE TABLE sympthom_data_m(
    idsympthomdatam       INT             AUTO_INCREMENT,
    descsymptom           VARCHAR(150),
    lastmodifieddate      DATE,
    lastmodifieduserid    INT,
    statuscode            INT,
    PRIMARY KEY (idsympthomdatam)
)ENGINE=INNODB
;



-- 
-- TABLE: sysdiagrams 
--

CREATE TABLE sysdiagrams(
    diagram_id      INT                AUTO_INCREMENT,
    name            VARCHAR(30),
    principal_id    INT                NOT NULL,
    version         INT,
    definition      VARBINARY(4000),
    PRIMARY KEY (diagram_id)
)ENGINE=INNODB
;



-- 
-- TABLE: telecomunication_number 
--

CREATE TABLE telecomunication_number(
    idcontact    BINARY(16)     NOT NULL,
    number       VARCHAR(30),
    PRIMARY KEY (idcontact)
)ENGINE=INNODB
;



-- 
-- TABLE: type_pkb 
--

CREATE TABLE type_pkb(
    idtypepkb    INT            NOT NULL,
    desctype     VARCHAR(30),
    PRIMARY KEY (idtypepkb)
)ENGINE=INNODB
;



-- 
-- TABLE: unit_accesories_mapper 
--

CREATE TABLE unit_accesories_mapper(
    iduntaccmap    BINARY(16)        NOT NULL,
    idmotor        VARCHAR(30),
    acc1           VARCHAR(30),
    qtyacc1        DECIMAL(14, 4),
    acc2           VARCHAR(30),
    qtyacc2        DECIMAL(14, 4),
    acc3           VARCHAR(30),
    qtyacc3        DECIMAL(14, 4),
    acc4           VARCHAR(30),
    qtyacc4        DECIMAL(14, 4),
    acc5           VARCHAR(30),
    qtyacc5        DECIMAL(14, 4),
    acc6           VARCHAR(30),
    qtyacc6        DECIMAL(14, 4),
    acc7           VARCHAR(30),
    qtyacc7        DECIMAL(14, 4),
    promat1        VARCHAR(30),
    qtypromat1     DECIMAL(14, 4),
    promat2        VARCHAR(30),
    qtypromat2     DECIMAL(14, 4),
    dtfrom         DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtthru         DATETIME          DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (iduntaccmap)
)ENGINE=INNODB
;



-- 
-- TABLE: unit_deliverable 
--

CREATE TABLE unit_deliverable(
    iddeliverable     BINARY(16)        NOT NULL,
    idreq             BINARY(16),
    iddeltype         INT,
    idvndstatyp       INT,
    idconstatyp       INT,
    description       VARCHAR(60),
    dtreceipt         DATE,
    dtdelivery        DATE,
    bastnumber        VARCHAR(30),
    name              VARCHAR(30),
    identitynumber    VARCHAR(30),
    cellphone         VARCHAR(30),
    refnumber         VARCHAR(60),
    refdt             DATETIME          DEFAULT CURRENT_TIMESTAMP,
    receiptqty        INT,
    receiptnominal    DECIMAL(18, 4),
    bpkbnumber        VARCHAR(60),
    PRIMARY KEY (iddeliverable)
)ENGINE=INNODB
;



-- 
-- TABLE: unit_document_message 
--

CREATE TABLE unit_document_message(
    idmessage       INT             AUTO_INCREMENT,
    idparent        INT,
    idreq           BINARY(16),
    idprospect      BINARY(16),
    content         VARCHAR(300),
    dtcreated       DATETIME        DEFAULT CURRENT_TIMESTAMP,
    usernamefrom    VARCHAR(50),
    usernameto      VARCHAR(50),
    PRIMARY KEY (idmessage)
)ENGINE=INNODB
;



-- 
-- TABLE: unit_preparation 
--

CREATE TABLE unit_preparation(
    idslip         BINARY(16)     NOT NULL,
    idinternal     VARCHAR(30),
    idfacility     BINARY(16),
    usermekanik    VARCHAR(50),
    PRIMARY KEY (idslip)
)ENGINE=INNODB
;



-- 
-- TABLE: unit_requirement 
--

CREATE TABLE unit_requirement(
    idreq        BINARY(16)     NOT NULL,
    idproduct    VARCHAR(30),
    idbranch     VARCHAR(30),
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: unit_shipment_receipt 
--

CREATE TABLE unit_shipment_receipt(
    idreceipt       BINARY(16)        NOT NULL,
    idframe         VARCHAR(30),
    idmachine       VARCHAR(30),
    yearassembly    INT,
    unitprice       DECIMAL(18, 4),
    acc1            INT               DEFAULT 0,
    acc2            INT               DEFAULT 0,
    acc3            INT               DEFAULT 0,
    acc4            INT               DEFAULT 0,
    acc5            INT               DEFAULT 0,
    acc6            INT               DEFAULT 0,
    acc7            INT               DEFAULT 0,
    isreceipt       INT               DEFAULT 0,
    PRIMARY KEY (idreceipt)
)ENGINE=INNODB
;



-- 
-- TABLE: uom 
--

CREATE TABLE uom(
    iduom           VARCHAR(30)    NOT NULL,
    iduomtyp        INT,
    description     VARCHAR(60),
    abbreviation    VARCHAR(60),
    PRIMARY KEY (iduom)
)ENGINE=INNODB
;



-- 
-- TABLE: uom_conversion 
--

CREATE TABLE uom_conversion(
    iduomconversion    INT               AUTO_INCREMENT,
    iduomto            VARCHAR(30),
    iduomfro           VARCHAR(30),
    factor             DECIMAL(14, 4),
    PRIMARY KEY (iduomconversion)
)ENGINE=INNODB
;



-- 
-- TABLE: uom_type 
--

CREATE TABLE uom_type(
    iduomtyp       INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (iduomtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: user_mediator 
--

CREATE TABLE user_mediator(
    idusrmed      BINARY(16)     NOT NULL,
    idperson      BINARY(16),
    idinternal    VARCHAR(30),
    id            BIGINT,
    email         VARCHAR(60),
    username      VARCHAR(30),
    firstname     VARCHAR(30),
    lastname      VARCHAR(30),
    PRIMARY KEY (idusrmed)
)ENGINE=INNODB
;



-- 
-- TABLE: user_mediator_role 
--

CREATE TABLE user_mediator_role(
    idrole        BINARY(16)     NOT NULL,
    idusrmed      BINARY(16),
    idparty       BINARY(16),
    idroletype    INT,
    username      VARCHAR(50),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: user_mediator_status 
--

CREATE TABLE user_mediator_status(
    idstatus        BINARY(16)      NOT NULL,
    idusrmed        BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle 
--

CREATE TABLE vehicle(
    idvehicle            BINARY(16)     NOT NULL,
    idproduct            VARCHAR(30),
    idcolor              INT,
    idinternal           VARCHAR(30),
    idmachine            VARCHAR(30),
    idframe              VARCHAR(30),
    servicebooknumber    VARCHAR(30),
    yearofass            INT,
    fuel                 VARCHAR(30),
    merkoil              VARCHAR(30),
    PRIMARY KEY (idvehicle)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_carrier 
--

CREATE TABLE vehicle_carrier(
    idvehide    BINARY(16)    NOT NULL,
    idparty     BINARY(16),
    idreason    INT,
    idreltyp    INT,
    PRIMARY KEY (idvehide)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_customer_request 
--

CREATE TABLE vehicle_customer_request(
    idrequest        BINARY(16)     NOT NULL,
    idcustomer       VARCHAR(30),
    idsalesbroker    BINARY(16),
    idsalesman       BINARY(16),
    customerorder    VARCHAR(30),
    PRIMARY KEY (idrequest)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_document_requirement 
--

CREATE TABLE vehicle_document_requirement(
    idreq                  BINARY(16)        NOT NULL,
    idslsreq               BINARY(16),
    idordite               BINARY(16),
    idorganizationowner    BINARY(16),
    idpersonowner          BINARY(16),
    idvehregtyp            INT,
    idsaletype             INT,
    idvehicle              BINARY(16),
    idvendor               VARCHAR(30),
    idinternal             VARCHAR(30),
    idbillto               VARCHAR(30),
    idshipto               VARCHAR(30),
    atpmfaktur             VARCHAR(30),
    note                   VARCHAR(300),
    bbn                    DECIMAL(18, 4),
    othercost              DECIMAL(18, 4),
    policenumber           VARCHAR(30),
    atpmfakturdt           DATETIME          DEFAULT CURRENT_TIMESTAMP,
    submissionno           VARCHAR(60),
    submissiondt           DATETIME          DEFAULT CURRENT_TIMESTAMP,
    crossarea              INT,
    dtfillname             DATE,
    registrationnumber     VARCHAR(30),
    statusfaktur           VARCHAR(30),
    waitstnk               INT               DEFAULT 0,
    reffnumber             VARCHAR(30),
    dtreff                 DATE,
    fname                  VARCHAR(30),
    lname                  VARCHAR(30),
    cellphone              VARCHAR(30),
    facilityname           VARCHAR(30),
    internname             VARCHAR(30),
    costhandling           DECIMAL(18, 4),
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_identification 
--

CREATE TABLE vehicle_identification(
    idvehide          BINARY(16)     NOT NULL,
    idvehicle         BINARY(16),
    idcustomer        VARCHAR(30),
    vehiclenumber     VARCHAR(10),
    dealerpurchase    VARCHAR(60),
    dtpurchase        DATE,
    dtfrom            DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru            DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idvehide)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_purchase_order 
--

CREATE TABLE vehicle_purchase_order(
    idorder    BINARY(16)    NOT NULL,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_registration_type 
--

CREATE TABLE vehicle_registration_type(
    idvehregtyp    INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idvehregtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_sales_billing 
--

CREATE TABLE vehicle_sales_billing(
    idbilling        BINARY(16)        NOT NULL,
    idsaletype       INT,
    idcustomer       VARCHAR(30),
    axunitprice      DECIMAL(18, 4),
    axbbn            DECIMAL(18, 4),
    axsalesamount    DECIMAL(18, 4),
    axdisc           DECIMAL(18, 4),
    axprice          DECIMAL(18, 4),
    axppn            DECIMAL(18, 4),
    axaramt          DECIMAL(18, 4),
    axadd1           DECIMAL(18, 4),
    axadd2           DECIMAL(18, 4),
    axadd3           DECIMAL(18, 4),
    PRIMARY KEY (idbilling)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_sales_order 
--

CREATE TABLE vehicle_sales_order(
    idorder                BINARY(16)        NOT NULL,
    idunitreq              BINARY(16),
    idsaletype             INT,
    idsalesman             BINARY(16),
    idsales                BINARY(16),
    idbirojasa             VARCHAR(30),
    issubsidifincoasdp     INT               DEFAULT 0,
    shiptoowner            INT               DEFAULT 0,
    dtbastunit             DATE,
    dtcustreceipt          DATE,
    dtmachineswipe         DATE,
    dttakepicture          DATE,
    dtcovernote            DATE,
    idlsgpaystatus         INT,
    dtadmslsverify         DATETIME          DEFAULT CURRENT_TIMESTAMP,
    dtafcverify            DATETIME          DEFAULT CURRENT_TIMESTAMP,
    admslsverifyval        INT,
    afcverifyval           INT,
    admslsnotapprovnote    VARCHAR(300),
    afcnotapprovnote       VARCHAR(300),
    vrnama1                VARCHAR(300),
    vrnama2                VARCHAR(300),
    vrnamamarket           VARCHAR(300),
    vrwarna                VARCHAR(300),
    vrtahunproduksi        BIGINT,
    vrangsuran             DECIMAL(18, 4),
    vrtenor                INT,
    vrleasing              VARCHAR(300),
    vrtglpengiriman        VARCHAR(30),
    vrjampengiriman        VARCHAR(60),
    vrtipepenjualan        VARCHAR(60),
    vrdpmurni              DECIMAL(18, 4),
    vrtandajadi            DECIMAL(18, 4),
    vrtglpembayaran        VARCHAR(30),
    vrsisa                 DECIMAL(18, 4),
    vriscod                INT               DEFAULT 0,
    vrcatatantambahan      VARCHAR(300),
    vrisverified           INT               DEFAULT 0,
    vrcatatan              VARCHAR(300),
    kacabnote              VARCHAR(300),
    isreqtaxinv            INT               DEFAULT 0,
    isdonematching         INT               DEFAULT 0,
    vridsaletype           INT,
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_service_history 
--

CREATE TABLE vehicle_service_history(
    idvehicleservicehistory    INT               AUTO_INCREMENT,
    idvehicle                  BINARY(16),
    location                   VARCHAR(100),
    km                         DECIMAL(10, 2),
    jobsuggest                 VARCHAR(100),
    servicedate                DATETIME,
    mechanicname               VARCHAR(30),
    nextservicedate            DATE,
    vehiclenumber              VARCHAR(30),
    idframe                    VARCHAR(30),
    idmachine                  VARCHAR(30),
    PRIMARY KEY (idvehicleservicehistory)
)ENGINE=INNODB
;



-- 
-- TABLE: vehicle_work_requirement 
--

CREATE TABLE vehicle_work_requirement(
    idreq            BINARY(16)     NOT NULL,
    idmechanic       BINARY(16),
    idvehicle        BINARY(16),
    idvehide         BINARY(16),
    idcustomer       VARCHAR(30),
    vehiclenumber    VARCHAR(30),
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: vendor 
--

CREATE TABLE vendor(
    idvendor        VARCHAR(30)    NOT NULL,
    idvndtyp        INT,
    idroletype      INT,
    idparty         BINARY(16),
    isbirojasa      INT            DEFAULT 0,
    ismaindealer    INT            DEFAULT 0,
    PRIMARY KEY (idvendor)
)ENGINE=INNODB
;



-- 
-- TABLE: vendor_order 
--

CREATE TABLE vendor_order(
    idorder       BINARY(16)     NOT NULL,
    idinternal    VARCHAR(30),
    idvendor      VARCHAR(30),
    idbillto      VARCHAR(30),
    PRIMARY KEY (idorder)
)ENGINE=INNODB
;



-- 
-- TABLE: vendor_product 
--

CREATE TABLE vendor_product(
    idvenpro      BINARY(16)     NOT NULL,
    idproduct     VARCHAR(30),
    idvendor      VARCHAR(30),
    idinternal    VARCHAR(30),
    orderratio    INT,
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idvenpro)
)ENGINE=INNODB
;



-- 
-- TABLE: vendor_quotation 
--

CREATE TABLE vendor_quotation(
    idquote        BINARY(16)     NOT NULL,
    idinternal     VARCHAR(30),
    idvendor       VARCHAR(30),
    vendorquote    VARCHAR(30),
    PRIMARY KEY (idquote)
)ENGINE=INNODB
;



-- 
-- TABLE: vendor_relationship 
--

CREATE TABLE vendor_relationship(
    idparrel      BINARY(16)     NOT NULL,
    idvendor      VARCHAR(30),
    idinternal    VARCHAR(30),
    PRIMARY KEY (idparrel)
)ENGINE=INNODB
;



-- 
-- TABLE: vendor_type 
--

CREATE TABLE vendor_type(
    idvndtyp       INT            NOT NULL,
    description    VARCHAR(60),
    PRIMARY KEY (idvndtyp)
)ENGINE=INNODB
;



-- 
-- TABLE: village 
--

CREATE TABLE village(
    idgeobou    BINARY(16)    NOT NULL,
    PRIMARY KEY (idgeobou)
)ENGINE=INNODB
;



-- 
-- TABLE: we_service_type 
--

CREATE TABLE we_service_type(
    idwetyp      INT            NOT NULL,
    idproduct    VARCHAR(30),
    frt          INT,
    PRIMARY KEY (idwetyp)
)ENGINE=INNODB
;



-- 
-- TABLE: we_type 
--

CREATE TABLE we_type(
    idwetyp        INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idwetyp)
)ENGINE=INNODB
;



-- 
-- TABLE: we_type_good_standard 
--

CREATE TABLE we_type_good_standard(
    idwegoostd    BINARY(16)        NOT NULL,
    idwetyp       INT,
    idproduct     VARCHAR(30),
    qty           DECIMAL(14, 4),
    PRIMARY KEY (idwegoostd)
)ENGINE=INNODB
;



-- 
-- TABLE: work_effort 
--

CREATE TABLE work_effort(
    idwe           BINARY(16)     NOT NULL,
    idfacility     BINARY(16),
    idwetyp        INT,
    name           VARCHAR(30),
    description    VARCHAR(60),
    PRIMARY KEY (idwe)
)ENGINE=INNODB
;



-- 
-- TABLE: work_effort_payment 
--

CREATE TABLE work_effort_payment(
    payments_id        BINARY(16)    NOT NULL,
    work_efforts_id    BINARY(16)    NOT NULL,
    PRIMARY KEY (payments_id, work_efforts_id)
)ENGINE=INNODB
;



-- 
-- TABLE: work_effort_role 
--

CREATE TABLE work_effort_role(
    idrole        BINARY(16)     NOT NULL,
    idwe          BINARY(16),
    idparty       BINARY(16),
    idroletype    INT,
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: work_effort_status 
--

CREATE TABLE work_effort_status(
    idstatus        BINARY(16)      NOT NULL,
    idwe            BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: work_order 
--

CREATE TABLE work_order(
    idreq    BINARY(16)    NOT NULL,
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: work_order_booking 
--

CREATE TABLE work_order_booking(
    idbooking        BINARY(16)     NOT NULL,
    idvehicle        BINARY(16),
    idbooslo         BINARY(16),
    idboktyp         INT,
    idevetyp         INT,
    idcustomer       VARCHAR(30),
    idinternal       VARCHAR(30),
    bookingnumber    VARCHAR(30),
    vehiclenumber    VARCHAR(10),
    dtcreate         DATETIME       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idbooking)
)ENGINE=INNODB
;



-- 
-- TABLE: work_order_booking_role 
--

CREATE TABLE work_order_booking_role(
    idrole        BINARY(16)     NOT NULL,
    idbooking     BINARY(16),
    idparty       BINARY(16),
    idroletype    INT,
    username      VARCHAR(30),
    dtfrom        DATETIME       DEFAULT CURRENT_TIMESTAMP,
    dtthru        DATETIME       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)ENGINE=INNODB
;



-- 
-- TABLE: work_order_booking_status 
--

CREATE TABLE work_order_booking_status(
    idstatus        BINARY(16)      NOT NULL,
    idbooking       BINARY(16),
    idstatustype    INT,
    idreason        INT,
    reason          VARCHAR(255),
    dtfrom          DATETIME        DEFAULT CURRENT_TIMESTAMP,
    dtthru          DATETIME        DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)ENGINE=INNODB
;



-- 
-- TABLE: work_product_req 
--

CREATE TABLE work_product_req(
    idreq    BINARY(16)    NOT NULL,
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: work_requirement 
--

CREATE TABLE work_requirement(
    idreq    BINARY(16)    NOT NULL,
    PRIMARY KEY (idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: work_requirement_services 
--

CREATE TABLE work_requirement_services(
    idwe     BINARY(16)    NOT NULL,
    idreq    BINARY(16)    NOT NULL,
    PRIMARY KEY (idwe, idreq)
)ENGINE=INNODB
;



-- 
-- TABLE: work_service_requirement 
--

CREATE TABLE work_service_requirement(
    idwe    BINARY(16)    NOT NULL,
    PRIMARY KEY (idwe)
)ENGINE=INNODB
;



-- 
-- TABLE: work_type 
--

CREATE TABLE work_type(
    idworktype     INT            AUTO_INCREMENT,
    description    VARCHAR(60),
    PRIMARY KEY (idworktype)
)ENGINE=INNODB
;



-- 
-- INDEX: billing_item_type_refkey 
--

CREATE UNIQUE INDEX billing_item_type_refkey ON billing_item_type(refkey)
;
-- 
-- INDEX: UQ__billing___F614937A2127871C 
--

CREATE UNIQUE INDEX UQ__billing___F614937A2127871C ON billing_njb(nonjb)
;
-- 
-- INDEX: UQ__billing___F614D9AE28D461C1 
--

CREATE UNIQUE INDEX UQ__billing___F614D9AE28D461C1 ON billing_nsc(nonsc)
;
-- 
-- INDEX: UQ__booking__7BD7FF987733DDE6 
--

CREATE UNIQUE INDEX UQ__booking__7BD7FF987733DDE6 ON booking(nobooking)
;
-- 
-- INDEX: UQ__claim_de__04CB47A9CFFED76E 
--

CREATE UNIQUE INDEX UQ__claim_de__04CB47A9CFFED76E ON claim_detail(noclaim)
;
-- 
-- INDEX: customer_id_mpm 
--

CREATE UNIQUE INDEX customer_id_mpm ON customer(idmpm)
;
-- 
-- INDEX: geo_boundary_code 
--

CREATE UNIQUE INDEX geo_boundary_code ON geo_boundary(geocode)
;
-- 
-- INDEX: good_identification_value 
--

CREATE UNIQUE INDEX good_identification_value ON good_identification(idvalue)
;
-- 
-- INDEX: FK_JOB_STAT_REFERENCE_STATUS_T 
--

CREATE INDEX FK_JOB_STAT_REFERENCE_STATUS_T ON job_status(idstatustype)
;
-- 
-- INDEX: leasing_company_id 
--

CREATE UNIQUE INDEX leasing_company_id ON leasing_company(idleacom)
;
-- 
-- INDEX: mem_number 
--

CREATE UNIQUE INDEX mem_number ON memo(memonumber)
;
-- 
-- INDEX: `nonclusteredindex-20180401-183606` 
--

CREATE INDEX `nonclusteredindex-20180401-183606` ON package_receipt(documentnumber)
;
-- 
-- INDEX: party_category_refkey 
--

CREATE INDEX party_category_refkey ON party_category(idcattyp, refkey)
;
-- 
-- INDEX: idpaytyp 
--

CREATE INDEX idpaytyp ON payment(idpaytyp)
;
-- 
-- INDEX: person_id_user 
--

CREATE INDEX person_id_user ON person(username)
;
-- 
-- INDEX: idordite 
--

CREATE INDEX idordite ON picking_slip(idordite)
;
-- 
-- INDEX: idinvite 
--

CREATE INDEX idinvite ON picking_slip(idinvite)
;
-- 
-- INDEX: UQ__pkb__F9AD522DC0CFD3E6 
--

CREATE UNIQUE INDEX UQ__pkb__F9AD522DC0CFD3E6 ON pkb(nopkb)
;
-- 
-- INDEX: FK_PKB_PART_REFERENCE_PKB 
--

CREATE INDEX FK_PKB_PART_REFERENCE_PKB ON pkb_part(idpkb)
;
-- 
-- INDEX: idinvite 
--

CREATE UNIQUE INDEX idinvite ON pkb_part(idinvite)
;
-- 
-- INDEX: FK_PKB_PART_REFERENCE_CHARGE_T 
--

CREATE INDEX FK_PKB_PART_REFERENCE_CHARGE_T ON pkb_part(idchargeto)
;
-- 
-- INDEX: FK_PKB_PART_REFERENCE_PKB_SERV 
--

CREATE INDEX FK_PKB_PART_REFERENCE_PKB_SERV ON pkb_part(idpkbservice)
;
-- 
-- INDEX: FK_PKB_PART_REFERENCE_CATEGORY 
--

CREATE INDEX FK_PKB_PART_REFERENCE_CATEGORY ON pkb_part(idcategory)
;
-- 
-- INDEX: FK_PKB_PART_REFERENCE_GOOD 
--

CREATE INDEX FK_PKB_PART_REFERENCE_GOOD ON pkb_part(idproduct)
;
-- 
-- INDEX: FK_PKB_SERV_REFERENCE_CATEGORY 
--

CREATE INDEX FK_PKB_SERV_REFERENCE_CATEGORY ON pkb_service(idcategory)
;
-- 
-- INDEX: FK_PKB_SERV_REFERENCE_SERVICE 
--

CREATE INDEX FK_PKB_SERV_REFERENCE_SERVICE ON pkb_service(idproduct)
;
-- 
-- INDEX: FK_PKB_SERV_REFERENCE_CHARGE_T 
--

CREATE INDEX FK_PKB_SERV_REFERENCE_CHARGE_T ON pkb_service(idchargeto)
;
-- 
-- INDEX: FK_PKB_SERV_RELATIONS_PKB 
--

CREATE INDEX FK_PKB_SERV_RELATIONS_PKB ON pkb_service(idpkb)
;
-- 
-- INDEX: price_component_period 
--

CREATE INDEX price_component_period ON price_component(dtfrom, dtthru)
;
-- 
-- INDEX: idprotyp 
--

CREATE INDEX idprotyp ON product(idprotyp)
;
-- 
-- INDEX: product_category_refkey 
--

CREATE INDEX product_category_refkey ON product_category(idcattyp, refkey)
;
-- 
-- INDEX: UQ__queue__292D8243407E81C5 
--

CREATE UNIQUE INDEX UQ__queue__292D8243407E81C5 ON queue(noqueue)
;
-- 
-- INDEX: idproduct 
--

CREATE INDEX idproduct ON sales_unit_requirement(idproduct)
;
-- 
-- INDEX: idowner 
--

CREATE INDEX idowner ON sales_unit_requirement(idowner)
;
-- 
-- INDEX: idprospect 
--

CREATE INDEX idprospect ON sales_unit_requirement(idprospect)
;
-- 
-- INDEX: idsalesman 
--

CREATE INDEX idsalesman ON sales_unit_requirement(idsalesman)
;
-- 
-- INDEX: idcolor 
--

CREATE INDEX idcolor ON sales_unit_requirement(idcolor)
;
-- 
-- INDEX: idsalesbroker 
--

CREATE INDEX idsalesbroker ON sales_unit_requirement(idsalesbroker)
;
-- 
-- INDEX: idlsgpro 
--

CREATE INDEX idlsgpro ON sales_unit_requirement(idlsgpro)
;
-- 
-- INDEX: idsaletype 
--

CREATE INDEX idsaletype ON sales_unit_requirement(idsaletype)
;
-- 
-- INDEX: idcoordinator 
--

CREATE INDEX idcoordinator ON salesman(idcoordinator)
;
-- 
-- INDEX: stk_op_item_tag 
--

CREATE UNIQUE INDEX stk_op_item_tag ON stock_opname_item(tagnumber)
;
-- 
-- INDEX: idsalesman 
--

CREATE INDEX idsalesman ON suspect(idsalesman)
;
-- 
-- INDEX: idsuspecttyp 
--

CREATE INDEX idsuspecttyp ON suspect(idsuspecttyp)
;
-- 
-- INDEX: idparty 
--

CREATE INDEX idparty ON suspect(idparty)
;
-- 
-- INDEX: idsalescoordinator 
--

CREATE INDEX idsalescoordinator ON suspect(idsalescoordinator)
;
-- 
-- INDEX: idaddress 
--

CREATE INDEX idaddress ON suspect(idaddress)
;
-- 
-- INDEX: UQ__sysdiagr__532EC1545CBAFF32 
--

CREATE UNIQUE INDEX UQ__sysdiagr__532EC1545CBAFF32 ON sysdiagrams(principal_id, name)
;
-- 
-- INDEX: iddeltype 
--

CREATE INDEX iddeltype ON unit_deliverable(iddeltype)
;
-- 
-- INDEX: idvndstatyp 
--

CREATE INDEX idvndstatyp ON unit_deliverable(idvndstatyp)
;
-- 
-- INDEX: idconstatyp 
--

CREATE INDEX idconstatyp ON unit_deliverable(idconstatyp)
;
-- 
-- INDEX: idreq 
--

CREATE INDEX idreq ON unit_deliverable(idreq)
;
-- 
-- INDEX: user_mediator_username 
--

CREATE UNIQUE INDEX user_mediator_username ON user_mediator(username)
;
-- 
-- INDEX: idsaletype 
--

CREATE INDEX idsaletype ON vehicle_document_requirement(idsaletype)
;
-- 
-- INDEX: idordite 
--

CREATE INDEX idordite ON vehicle_document_requirement(idordite)
;
-- 
-- INDEX: idpersonowner 
--

CREATE INDEX idpersonowner ON vehicle_document_requirement(idpersonowner)
;
-- 
-- INDEX: idvehregtyp 
--

CREATE INDEX idvehregtyp ON vehicle_document_requirement(idvehregtyp)
;
-- 
-- INDEX: idvehicle 
--

CREATE INDEX idvehicle ON vehicle_document_requirement(idvehicle)
;
-- 
-- INDEX: idslsreq 
--

CREATE INDEX idslsreq ON vehicle_document_requirement(idslsreq)
;
-- 
-- INDEX: idsaletype 
--

CREATE INDEX idsaletype ON vehicle_sales_order(idsaletype)
;
-- 
-- INDEX: idunitreq 
--

CREATE INDEX idunitreq ON vehicle_sales_order(idunitreq)
;
-- 
-- INDEX: we_service_type_id_product 
--

CREATE UNIQUE INDEX we_service_type_id_product ON we_service_type(idproduct)
;
-- 
-- TABLE: accounting_calendar 
--

ALTER TABLE accounting_calendar ADD CONSTRAINT Refbase_calendar1809 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
;

ALTER TABLE accounting_calendar ADD CONSTRAINT FK__accountin__idint__3FBB6990 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: acctg_trans_detail 
--

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idacc__438BFA74 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
;

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idcal__4297D63B 
    FOREIGN KEY (idcalendar)
    REFERENCES accounting_calendar(idcalendar)
;

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idgla__40AF8DC9 
    FOREIGN KEY (idglacc)
    REFERENCES gl_account(idglacc)
;

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idint__41A3B202 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: acctg_transaction 
--

ALTER TABLE acctg_transaction ADD CONSTRAINT FK__acctg_tra__idacc__44801EAD 
    FOREIGN KEY (idacctratyp)
    REFERENCES acctg_trans_type(idacctratyp)
;


-- 
-- TABLE: acctg_transaction_status 
--

ALTER TABLE acctg_transaction_status ADD CONSTRAINT FK__acctg_tra__idacc__475C8B58 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
;

ALTER TABLE acctg_transaction_status ADD CONSTRAINT FK__acctg_tra__idrea__4668671F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE acctg_transaction_status ADD CONSTRAINT FK__acctg_tra__idsta__457442E6 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: agreement 
--

ALTER TABLE agreement ADD CONSTRAINT FK__agreement__idagr__4850AF91 
    FOREIGN KEY (idagrtyp)
    REFERENCES agreement_type(idagrtyp)
;


-- 
-- TABLE: agreement_item 
--

ALTER TABLE agreement_item ADD CONSTRAINT FK__agreement__idagr__4944D3CA 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;


-- 
-- TABLE: agreement_motor_item 
--

ALTER TABLE agreement_motor_item ADD CONSTRAINT FK__agreement__idagr__4B2D1C3C 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
;

ALTER TABLE agreement_motor_item ADD CONSTRAINT FK__agreement__idveh__4A38F803 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: agreement_role 
--

ALTER TABLE agreement_role ADD CONSTRAINT FK__agreement__idagr__4C214075 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE agreement_role ADD CONSTRAINT FK__agreement__idpar__4E0988E7 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE agreement_role ADD CONSTRAINT FK__agreement__idrol__4D1564AE 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: agreement_status 
--

ALTER TABLE agreement_status ADD CONSTRAINT FK__agreement__idagr__4EFDAD20 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE agreement_status ADD CONSTRAINT FK__agreement__idrea__50E5F592 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE agreement_status ADD CONSTRAINT FK__agreement__idsta__4FF1D159 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: approval 
--

ALTER TABLE approval ADD CONSTRAINT FK__approval__idappt__54B68676 
    FOREIGN KEY (idapptyp)
    REFERENCES approval_type(idapptyp)
;

ALTER TABLE approval ADD CONSTRAINT FK__approval__idmess__55AAAAAF 
    FOREIGN KEY (idmessage)
    REFERENCES unit_document_message(idmessage)
;

ALTER TABLE approval ADD CONSTRAINT FK__approval__idordi__53C2623D 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE approval ADD CONSTRAINT FK__approval__idreq__52CE3E04 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE approval ADD CONSTRAINT FK__approval__idstat__51DA19CB 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: approval_stockopname_tm 
--

ALTER TABLE approval_stockopname_tm ADD CONSTRAINT FK__approval___idapp__5792F321 
    FOREIGN KEY (idapprovalstockopnamestatus)
    REFERENCES approval_stockopname_status_tp(idapprovalstockopnamestatus)
;

ALTER TABLE approval_stockopname_tm ADD CONSTRAINT FK__approval___idsto__569ECEE8 
    FOREIGN KEY (idstockopname)
    REFERENCES stockopname_tm(idstockopname)
;


-- 
-- TABLE: attendance 
--

ALTER TABLE attendance ADD CONSTRAINT FK__attendanc__idpar__5887175A 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
;


-- 
-- TABLE: base_calendar 
--

ALTER TABLE base_calendar ADD CONSTRAINT FK__base_cale__idcal__597B3B93 
    FOREIGN KEY (idcaltyp)
    REFERENCES calendar_type(idcaltyp)
;

ALTER TABLE base_calendar ADD CONSTRAINT FK__base_cale__idpar__5A6F5FCC 
    FOREIGN KEY (idparent)
    REFERENCES base_calendar(idcalendar)
;


-- 
-- TABLE: bill_to 
--

ALTER TABLE bill_to ADD CONSTRAINT FK__bill_to__5B638405 
    FOREIGN KEY (idparty, idroletype)
    REFERENCES party_role_type(idparty, idroletype)
;


-- 
-- TABLE: billing 
--

ALTER TABLE billing ADD CONSTRAINT FK__billing__idbilfr__5F3414E9 
    FOREIGN KEY (idbilfro)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE billing ADD CONSTRAINT FK__billing__idbilto__5E3FF0B0 
    FOREIGN KEY (idbilto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE billing ADD CONSTRAINT FK__billing__idbilty__5C57A83E 
    FOREIGN KEY (idbiltyp)
    REFERENCES billing_type(idbiltyp)
;

ALTER TABLE billing ADD CONSTRAINT FK__billing__idinter__5D4BCC77 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: billing_acctg_trans 
--

ALTER TABLE billing_acctg_trans ADD CONSTRAINT FK__billing_a__idacc__611C5D5B 
    FOREIGN KEY (idacctra)
    REFERENCES external_acctg_trans(idacctra)
;

ALTER TABLE billing_acctg_trans ADD CONSTRAINT FK__billing_a__idbil__60283922 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;


-- 
-- TABLE: billing_disbursement 
--

ALTER TABLE billing_disbursement ADD CONSTRAINT Refbilling1804 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_disbursement ADD CONSTRAINT FK__billing_d__idbil__63F8CA06 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
;

ALTER TABLE billing_disbursement ADD CONSTRAINT FK__billing_d__idven__6304A5CD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: billing_issuance 
--

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idbil__67C95AEA 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idinv__64ECEE3F 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idshi__66D536B1 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
;

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idsli__65E11278 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;


-- 
-- TABLE: billing_item 
--

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idbil__68BD7F23 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idbil__6C8E1007 
    FOREIGN KEY (idbilitetyp)
    REFERENCES billing_item_type(idbilitetyp)
;

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idfea__6D823440 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idinv__69B1A35C 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idpro__6AA5C795 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idwet__6B99EBCE 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;


-- 
-- TABLE: billing_njb 
--

ALTER TABLE billing_njb ADD CONSTRAINT FK__billing_n__idbil__6E765879 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;


-- 
-- TABLE: billing_nsc 
--

ALTER TABLE billing_nsc ADD CONSTRAINT FK__billing_n__idbil__6F6A7CB2 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;


-- 
-- TABLE: billing_receipt 
--

ALTER TABLE billing_receipt ADD CONSTRAINT Refbilling1803 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_receipt ADD CONSTRAINT FK__billing_r__idbil__7152C524 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
;


-- 
-- TABLE: billing_role 
--

ALTER TABLE billing_role ADD CONSTRAINT FK__billing_r__idbil__7246E95D 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_role ADD CONSTRAINT FK__billing_r__idpar__742F31CF 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE billing_role ADD CONSTRAINT FK__billing_r__idrol__733B0D96 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: billing_status 
--

ALTER TABLE billing_status ADD CONSTRAINT FK__billing_s__idbil__75235608 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_status ADD CONSTRAINT FK__billing_s__idrea__770B9E7A 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE billing_status ADD CONSTRAINT FK__billing_s__idsta__76177A41 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: booking 
--

ALTER TABLE booking ADD CONSTRAINT FK__booking__idbooki__77FFC2B3 
    FOREIGN KEY (idbookingstatus)
    REFERENCES booking_status(idbookingstatus)
;


-- 
-- TABLE: booking_slot 
--

ALTER TABLE booking_slot ADD CONSTRAINT FK__booking_s__idboo__79E80B25 
    FOREIGN KEY (idbooslostd)
    REFERENCES booking_slot_standard(idbooslostd)
;

ALTER TABLE booking_slot ADD CONSTRAINT FK__booking_s__idcal__78F3E6EC 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
;


-- 
-- TABLE: booking_slot_standard 
--

ALTER TABLE booking_slot_standard ADD CONSTRAINT FK__booking_s__idbok__7BD05397 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
;

ALTER TABLE booking_slot_standard ADD CONSTRAINT FK__booking_s__idint__7ADC2F5E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: branch 
--

ALTER TABLE branch ADD CONSTRAINT FK__branch__idbranch__7DB89C09 
    FOREIGN KEY (idbranchcategory)
    REFERENCES party_category(idcategory)
;

ALTER TABLE branch ADD CONSTRAINT FK__branch__idintern__7CC477D0 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: carrier 
--

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idparty__01892CED 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idreaso__7EACC042 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idrelty__7FA0E47B 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
;

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idvehic__009508B4 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: city 
--

ALTER TABLE city ADD CONSTRAINT Refgeo_boundary1812 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: claim_detail 
--

ALTER TABLE claim_detail ADD CONSTRAINT FK__claim_det__idcus__08362A7C 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE claim_detail ADD CONSTRAINT FK__claim_det__iddat__064DE20A 
    FOREIGN KEY (iddatadamage)
    REFERENCES data_damage(iddatadamage)
;

ALTER TABLE claim_detail ADD CONSTRAINT FK__claim_det__idsym__07420643 
    FOREIGN KEY (idsympthomdatam)
    REFERENCES sympthom_data_m(idsympthomdatam)
;


-- 
-- TABLE: communication_event 
--

ALTER TABLE communication_event ADD CONSTRAINT FK__communica__ideve__0A1E72EE 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
;

ALTER TABLE communication_event ADD CONSTRAINT FK__communica__idpar__092A4EB5 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;


-- 
-- TABLE: communication_event_cdb 
--

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refreligion_type3035 
    FOREIGN KEY (idreligion)
    REFERENCES religion_type(idreligiontype)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refprovince3036 
    FOREIGN KEY (idprovincesurat)
    REFERENCES province(idgeobou)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refdistrict3037 
    FOREIGN KEY (idcitysurat)
    REFERENCES district(idgeobou)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refdistrict3038 
    FOREIGN KEY (iddistrictsurat)
    REFERENCES district(idgeobou)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refvillage3039 
    FOREIGN KEY (idvillagesurat)
    REFERENCES village(idgeobou)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idcit__0DEF03D2 
    FOREIGN KEY (idcity)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idcom__0C06BB60 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idcus__0B129727 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__iddis__0EE3280B 
    FOREIGN KEY (iddistrict)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idpro__0CFADF99 
    FOREIGN KEY (idprovince)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: communication_event_delivery 
--

ALTER TABLE communication_event_delivery ADD CONSTRAINT FK__communica__idcus__10CB707D 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE communication_event_delivery ADD CONSTRAINT FK__communica__idint__0FD74C44 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: communication_event_prospect 
--

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idcol__149C0161 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
;

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idcom__1590259A 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idpro__11BF94B6 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idpro__13A7DD28 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idsal__12B3B8EF 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: communication_event_purpose 
--

ALTER TABLE communication_event_purpose ADD CONSTRAINT FK__communica__idcom__17786E0C 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_purpose ADD CONSTRAINT FK__communica__idpur__168449D3 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: communication_event_role 
--

ALTER TABLE communication_event_role ADD CONSTRAINT FK__communica__idcom__186C9245 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_role ADD CONSTRAINT FK__communica__idpar__1A54DAB7 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE communication_event_role ADD CONSTRAINT FK__communica__idrol__1960B67E 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: communication_event_status 
--

ALTER TABLE communication_event_status ADD CONSTRAINT FK__communica__idcom__1B48FEF0 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_status ADD CONSTRAINT FK__communica__idrea__1D314762 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE communication_event_status ADD CONSTRAINT FK__communica__idsta__1C3D2329 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: contact_purpose 
--

ALTER TABLE contact_purpose ADD CONSTRAINT FK__contact_p__idcon__1F198FD4 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE contact_purpose ADD CONSTRAINT FK__contact_p__idpur__1E256B9B 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: container 
--

ALTER TABLE container ADD CONSTRAINT FK__container__idcon__200DB40D 
    FOREIGN KEY (idcontyp)
    REFERENCES container_type(idcontyp)
;

ALTER TABLE container ADD CONSTRAINT FK__container__idfac__2101D846 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;


-- 
-- TABLE: correction_note 
--

ALTER TABLE correction_note ADD CONSTRAINT FK__correctio__idiss__22EA20B8 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
;

ALTER TABLE correction_note ADD CONSTRAINT FK__correctio__idpro__23DE44F1 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE correction_note ADD CONSTRAINT FK__correctio__idspg__21F5FC7F 
    FOREIGN KEY (idspgclaim)
    REFERENCES spg_claim(idspgclaim)
;


-- 
-- TABLE: customer 
--

ALTER TABLE customer ADD CONSTRAINT FK__customer__idmark__26BAB19C 
    FOREIGN KEY (idmarketcategory)
    REFERENCES party_category(idcategory)
;

ALTER TABLE customer ADD CONSTRAINT FK__customer__idpart__24D2692A 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE customer ADD CONSTRAINT FK__customer__idrole__25C68D63 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: customer_order 
--

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idbil__2A8B4280 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idcus__27AED5D5 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idint__28A2FA0E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idord__29971E47 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idveh__2B7F66B9 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: customer_quotation 
--

ALTER TABLE customer_quotation ADD CONSTRAINT Refquote1817 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE customer_quotation ADD CONSTRAINT FK__customer___idcus__2C738AF2 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE customer_quotation ADD CONSTRAINT FK__customer___idint__2D67AF2B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: customer_relationship 
--

ALTER TABLE customer_relationship ADD CONSTRAINT FK__customer___idcus__30441BD6 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE customer_relationship ADD CONSTRAINT FK__customer___idint__2F4FF79D 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE customer_relationship ADD CONSTRAINT FK__customer___idpar__3138400F 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;


-- 
-- TABLE: customer_request_item 
--

ALTER TABLE customer_request_item ADD CONSTRAINT FK__customer___idcus__322C6448 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE customer_request_item ADD CONSTRAINT FK__customer___idreq__33208881 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
;


-- 
-- TABLE: delivery_order 
--

ALTER TABLE delivery_order ADD CONSTRAINT FK__delivery___idbil__3414ACBA 
    FOREIGN KEY (idbilling)
    REFERENCES billing_disbursement(idbilling)
;


-- 
-- TABLE: dimension 
--

ALTER TABLE dimension ADD CONSTRAINT FK__dimension__iddim__3508D0F3 
    FOREIGN KEY (iddimtyp)
    REFERENCES dimension_type(iddimtyp)
;


-- 
-- TABLE: disbursement 
--

ALTER TABLE disbursement ADD CONSTRAINT FK__disbursem__idpay__35FCF52C 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: district 
--

ALTER TABLE district ADD CONSTRAINT Refgeo_boundary1810 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: document_type 
--

ALTER TABLE document_type ADD CONSTRAINT FK__document___idpar__37E53D9E 
    FOREIGN KEY (idparent)
    REFERENCES document_type(iddoctype)
;


-- 
-- TABLE: documents 
--

ALTER TABLE documents ADD CONSTRAINT FK__documents__iddoc__38D961D7 
    FOREIGN KEY (iddoctype)
    REFERENCES document_type(iddoctype)
;


-- 
-- TABLE: driver 
--

ALTER TABLE driver ADD CONSTRAINT Refparty_role1814 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;

ALTER TABLE driver ADD CONSTRAINT FK__driver__idvendor__3AC1AA49 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: electronic_address 
--

ALTER TABLE electronic_address ADD CONSTRAINT FK__electroni__idcon__3BB5CE82 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: employee 
--

ALTER TABLE employee ADD CONSTRAINT FK__employee__idparr__3CA9F2BB 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;

ALTER TABLE employee ADD CONSTRAINT FK__employee__idpart__3D9E16F4 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;


-- 
-- TABLE: employee_customer_relationship 
--

ALTER TABLE employee_customer_relationship ADD CONSTRAINT FK__employee___emplo__3E923B2D 
    FOREIGN KEY (employeenumber)
    REFERENCES employee(employeenumber)
;

ALTER TABLE employee_customer_relationship ADD CONSTRAINT FK__employee___idcus__3F865F66 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE employee_customer_relationship ADD CONSTRAINT FK__employee___idpar__407A839F 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;


-- 
-- TABLE: event_type 
--

ALTER TABLE event_type ADD CONSTRAINT FK__event_typ__idprn__416EA7D8 
    FOREIGN KEY (idprneve)
    REFERENCES event_type(idevetyp)
;


-- 
-- TABLE: exchange_part 
--

ALTER TABLE exchange_part ADD CONSTRAINT FK__exchange___idpro__444B1483 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE exchange_part ADD CONSTRAINT FK__exchange___idspg__4262CC11 
    FOREIGN KEY (idspgclaimdetail)
    REFERENCES spg_claim_detail(idspgclaimdetail)
;

ALTER TABLE exchange_part ADD CONSTRAINT FK__exchange___idspg__4356F04A 
    FOREIGN KEY (idspgclaimaction)
    REFERENCES spg_claim_action(idspgclaimaction)
;


-- 
-- TABLE: external_acctg_trans 
--

ALTER TABLE external_acctg_trans ADD CONSTRAINT Refacctg_transaction1802 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
;

ALTER TABLE external_acctg_trans ADD CONSTRAINT FK__external___idpar__453F38BC 
    FOREIGN KEY (idparfro)
    REFERENCES party(idparty)
;

ALTER TABLE external_acctg_trans ADD CONSTRAINT FK__external___idpar__46335CF5 
    FOREIGN KEY (idparto)
    REFERENCES party(idparty)
;


-- 
-- TABLE: facility 
--

ALTER TABLE facility ADD CONSTRAINT FK__facility__idfa__4A03EDD9 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
;

ALTER TABLE facility ADD CONSTRAINT FK__facility__idfaci__490FC9A0 
    FOREIGN KEY (idfacilitytype)
    REFERENCES facility_type(idfacilitytype)
;

ALTER TABLE facility ADD CONSTRAINT FK__facility__idpart__481BA567 
    FOREIGN KEY (idpartof)
    REFERENCES facility(idfacility)
;


-- 
-- TABLE: facility_contact_mechanism 
--

ALTER TABLE facility_contact_mechanism ADD CONSTRAINT FK__facility___idcon__4AF81212 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE facility_contact_mechanism ADD CONSTRAINT FK__facility___idfac__4BEC364B 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE facility_contact_mechanism ADD CONSTRAINT FK__facility___idpur__4CE05A84 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: facility_status 
--

ALTER TABLE facility_status ADD CONSTRAINT FK__facility___idfac__4DD47EBD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE facility_status ADD CONSTRAINT FK__facility___idrea__4FBCC72F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE facility_status ADD CONSTRAINT FK__facility___idsta__4EC8A2F6 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: feature 
--

ALTER TABLE feature ADD CONSTRAINT FK__feature__idfeaty__50B0EB68 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
;


-- 
-- TABLE: feature_applicable 
--

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idfea__538D5813 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
;

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idfea__54817C4C 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idown__529933DA 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idpro__51A50FA1 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: feature_type 
--

ALTER TABLE feature_type ADD CONSTRAINT FK__feature_t__idrul__5575A085 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: fixed_asset 
--

ALTER TABLE fixed_asset ADD CONSTRAINT FK__fixed_ass__idfat__5669C4BE 
    FOREIGN KEY (idfatype)
    REFERENCES fixed_asset_type(idfatype)
;

ALTER TABLE fixed_asset ADD CONSTRAINT FK__fixed_ass__iduom__575DE8F7 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: fixed_asset_type 
--

ALTER TABLE fixed_asset_type ADD CONSTRAINT FK__fixed_ass__idpar__58520D30 
    FOREIGN KEY (idparent)
    REFERENCES fixed_asset_type(idfatype)
;


-- 
-- TABLE: fixedasset_adjustment_th 
--

ALTER TABLE fixedasset_adjustment_th ADD CONSTRAINT FK__fixedasse__idfix__59463169 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
;


-- 
-- TABLE: fixedasset_th 
--

ALTER TABLE fixedasset_th ADD CONSTRAINT FK__fixedasse__idfix__5C229E14 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
;

ALTER TABLE fixedasset_th ADD CONSTRAINT FK__fixedasse__idloc__5A3A55A2 
    FOREIGN KEY (idlocationfloor)
    REFERENCES floor_tm(idfloor)
;

ALTER TABLE fixedasset_th ADD CONSTRAINT FK__fixedasse__idloc__5B2E79DB 
    FOREIGN KEY (idlocationroom)
    REFERENCES room_tm(idroom)
;


-- 
-- TABLE: fixedasset_tm 
--

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idcat__60E75331 
    FOREIGN KEY (idcategoryasset)
    REFERENCES category_asset_tp(idcategoryasset)
;

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idflo__5D16C24D 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
;

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idpur__5E0AE686 
    FOREIGN KEY (idpurchaseorderreception)
    REFERENCES purchaseorder_reception_tr(idpurchaseorderreception)
;

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idpur__5EFF0ABF 
    FOREIGN KEY (idpurchaseorder)
    REFERENCES purchaseorder_tm(idpurchaseorder)
;

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idroo__5FF32EF8 
    FOREIGN KEY (idroom)
    REFERENCES room_tm(idroom)
;


-- 
-- TABLE: general_upload 
--

ALTER TABLE general_upload ADD CONSTRAINT FK__general_u__idint__62CF9BA3 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE general_upload ADD CONSTRAINT FK__general_u__idpur__61DB776A 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: general_upload_status 
--

ALTER TABLE general_upload_status ADD CONSTRAINT Refreason_type1807 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE general_upload_status ADD CONSTRAINT Refstatus_type1808 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE general_upload_status ADD CONSTRAINT FK__general_u__idgen__63C3BFDC 
    FOREIGN KEY (idgenupl)
    REFERENCES general_upload(idgenupl)
;


-- 
-- TABLE: geo_boundary 
--

ALTER TABLE geo_boundary ADD CONSTRAINT FK__geo_bound__idgeo__64B7E415 
    FOREIGN KEY (idgeoboutype)
    REFERENCES geo_boundary_type(idgeoboutype)
;

ALTER TABLE geo_boundary ADD CONSTRAINT FK__geo_bound__idpar__65AC084E 
    FOREIGN KEY (idparent)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: gl_account 
--

ALTER TABLE gl_account ADD CONSTRAINT FK__gl_accoun__idgla__66A02C87 
    FOREIGN KEY (idglacctyp)
    REFERENCES gl_account_type(idglacctyp)
;


-- 
-- TABLE: gl_dimension 
--

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__679450C0 
    FOREIGN KEY (dimension1)
    REFERENCES dimension(iddimension)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__688874F9 
    FOREIGN KEY (dimension5)
    REFERENCES dimension(iddimension)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__697C9932 
    FOREIGN KEY (dimension4)
    REFERENCES dimension(iddimension)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__6A70BD6B 
    FOREIGN KEY (dimension3)
    REFERENCES dimension(iddimension)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__6B64E1A4 
    FOREIGN KEY (dimension2)
    REFERENCES dimension(iddimension)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__6C5905DD 
    FOREIGN KEY (dimension6)
    REFERENCES dimension(iddimension)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idbil__6D4D2A16 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idint__711DBAFA 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idpar__702996C1 
    FOREIGN KEY (idparcat)
    REFERENCES party_category(idcategory)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idpro__6F357288 
    FOREIGN KEY (idprocat)
    REFERENCES product_category(idcategory)
;

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idven__6E414E4F 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: good 
--

ALTER TABLE good ADD CONSTRAINT FK__good__idproduct__7306036C 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE good ADD CONSTRAINT FK__good__iduom__7211DF33 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: good_container 
--

ALTER TABLE good_container ADD CONSTRAINT FK__good_cont__idcon__74EE4BDE 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
;

ALTER TABLE good_container ADD CONSTRAINT FK__good_cont__idpar__75E27017 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE good_container ADD CONSTRAINT FK__good_cont__idpro__73FA27A5 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: good_identification 
--

ALTER TABLE good_identification ADD CONSTRAINT Refproduct1821 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE good_identification ADD CONSTRAINT Refidentification_type1822 
    FOREIGN KEY (ididentificationtype)
    REFERENCES identification_type(ididentificationtype)
;


-- 
-- TABLE: good_promotion 
--

ALTER TABLE good_promotion ADD CONSTRAINT FK__good_prom__idpro__76D69450 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: internal 
--

ALTER TABLE internal ADD CONSTRAINT FK__internal__idpare__79B300FB 
    FOREIGN KEY (idparent)
    REFERENCES internal(idinternal)
;

ALTER TABLE internal ADD CONSTRAINT FK__internal__idpart__77CAB889 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
;

ALTER TABLE internal ADD CONSTRAINT FK__internal__idrole__78BEDCC2 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE internal ADD CONSTRAINT FK__internal__idroot__7AA72534 
    FOREIGN KEY (idroot)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: internal_acctg_trans 
--

ALTER TABLE internal_acctg_trans ADD CONSTRAINT Refacctg_transaction1801 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
;

ALTER TABLE internal_acctg_trans ADD CONSTRAINT FK__internal___idint__7B9B496D 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: internal_facility_purpose 
--

ALTER TABLE internal_facility_purpose ADD CONSTRAINT FK__internal___idfac__7D8391DF 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE internal_facility_purpose ADD CONSTRAINT FK__internal___idint__7E77B618 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE internal_facility_purpose ADD CONSTRAINT FK__internal___idpur__7F6BDA51 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: internal_order 
--

ALTER TABLE internal_order ADD CONSTRAINT FK__internal___idint__015422C3 
    FOREIGN KEY (idintfro)
    REFERENCES internal(idinternal)
;

ALTER TABLE internal_order ADD CONSTRAINT FK__internal___idint__024846FC 
    FOREIGN KEY (idintto)
    REFERENCES internal(idinternal)
;

ALTER TABLE internal_order ADD CONSTRAINT FK__internal___idord__005FFE8A 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;


-- 
-- TABLE: inventory_adjustment 
--

ALTER TABLE inventory_adjustment ADD CONSTRAINT FK__inventory__idinv__033C6B35 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE inventory_adjustment ADD CONSTRAINT FK__inventory__idsto__04308F6E 
    FOREIGN KEY (idstopnite)
    REFERENCES stock_opname_item(idstopnite)
;


-- 
-- TABLE: inventory_item 
--

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory___idfa__0ADD8CFD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
;

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idcon__08012052 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
;

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idfac__070CFC19 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idfea__09E968C4 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idown__0524B3A7 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idpro__0618D7E0 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idrec__08F5448B 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;


-- 
-- TABLE: inventory_item_status 
--

ALTER TABLE inventory_item_status ADD CONSTRAINT FK__inventory__idinv__0BD1B136 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE inventory_item_status ADD CONSTRAINT FK__inventory__idrea__0DB9F9A8 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE inventory_item_status ADD CONSTRAINT FK__inventory__idsta__0CC5D56F 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: inventory_movement_status 
--

ALTER TABLE inventory_movement_status ADD CONSTRAINT FK__inventory__idrea__10966653 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE inventory_movement_status ADD CONSTRAINT FK__inventory__idsli__0EAE1DE1 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;

ALTER TABLE inventory_movement_status ADD CONSTRAINT FK__inventory__idsta__0FA2421A 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: inventory_request_item 
--

ALTER TABLE inventory_request_item ADD CONSTRAINT FK__inventory__idinv__1372D2FE 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE inventory_request_item ADD CONSTRAINT FK__inventory__idreq__118A8A8C 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
;

ALTER TABLE inventory_request_item ADD CONSTRAINT FK__inventory__idreq__127EAEC5 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
;


-- 
-- TABLE: item_issuance 
--

ALTER TABLE item_issuance ADD CONSTRAINT FK__item_issu__idinv__155B1B70 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE item_issuance ADD CONSTRAINT FK__item_issu__idshi__1466F737 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;

ALTER TABLE item_issuance ADD CONSTRAINT FK__item_issu__idsli__164F3FA9 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
;


-- 
-- TABLE: job 
--

ALTER TABLE job ADD CONSTRAINT FK__job__idparrol__1837881B 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
;

ALTER TABLE job ADD CONSTRAINT FK__job__idpkb__174363E2 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
;


-- 
-- TABLE: job_dispatch 
--

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__iddis__1C0818FF 
    FOREIGN KEY (iddispatchstatus)
    REFERENCES dispatch_status(iddispatchstatus)
;

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__idjob__192BAC54 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
;

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__idpar__1A1FD08D 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
;

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__idpkb__1B13F4C6 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
;


-- 
-- TABLE: job_dispatch_customer_type 
--

ALTER TABLE job_dispatch_customer_type ADD CONSTRAINT FK__job_dispa__idcos__1DF06171 
    FOREIGN KEY (idcostumertype)
    REFERENCES customer_type(idcostumertype)
;

ALTER TABLE job_dispatch_customer_type ADD CONSTRAINT FK__job_dispa__idjob__1CFC3D38 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
;


-- 
-- TABLE: job_dispatch_group_service 
--

ALTER TABLE job_dispatch_group_service ADD CONSTRAINT FK__job_dispa__idjob__1EE485AA 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
;


-- 
-- TABLE: job_dispatch_pkb_type 
--

ALTER TABLE job_dispatch_pkb_type ADD CONSTRAINT FK__job_dispa__idjob__20CCCE1C 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
;

ALTER TABLE job_dispatch_pkb_type ADD CONSTRAINT FK__job_dispa__idtyp__1FD8A9E3 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
;


-- 
-- TABLE: job_dispatch_service 
--

ALTER TABLE job_dispatch_service ADD CONSTRAINT FK__job_dispa__idjob__22B5168E 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
;

ALTER TABLE job_dispatch_service ADD CONSTRAINT FK__job_dispa__idpro__21C0F255 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: job_history 
--

ALTER TABLE job_history ADD CONSTRAINT FK__job_histo__idpar__23A93AC7 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
;

ALTER TABLE job_history ADD CONSTRAINT FK__job_histo__idpen__249D5F00 
    FOREIGN KEY (idpendingreason)
    REFERENCES pendingreason(idpendingreason)
;

ALTER TABLE job_history ADD CONSTRAINT FK__job_histo__idpkb__25918339 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
;


-- 
-- TABLE: job_priority 
--

ALTER TABLE job_priority ADD CONSTRAINT FK__job_prior__idjob__2685A772 
    FOREIGN KEY (idjobprioritytype)
    REFERENCES job_priority_type(idjobprioritytype)
;


-- 
-- TABLE: job_priority_customer_type 
--

ALTER TABLE job_priority_customer_type ADD CONSTRAINT FK__job_prior__idcos__286DEFE4 
    FOREIGN KEY (idcostumertype)
    REFERENCES customer_type(idcostumertype)
;

ALTER TABLE job_priority_customer_type ADD CONSTRAINT FK__job_prior__idjob__2779CBAB 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
;


-- 
-- TABLE: job_priority_group_service 
--

ALTER TABLE job_priority_group_service ADD CONSTRAINT FK__job_prior__idjob__2962141D 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
;


-- 
-- TABLE: job_priority_pkb_type 
--

ALTER TABLE job_priority_pkb_type ADD CONSTRAINT FK__job_prior__idjob__2B4A5C8F 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
;

ALTER TABLE job_priority_pkb_type ADD CONSTRAINT FK__job_prior__idtyp__2A563856 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
;


-- 
-- TABLE: job_priority_service 
--

ALTER TABLE job_priority_service ADD CONSTRAINT FK__job_prior__idjob__2D32A501 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
;

ALTER TABLE job_priority_service ADD CONSTRAINT FK__job_prior__idpro__2C3E80C8 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: job_service 
--

ALTER TABLE job_service ADD CONSTRAINT FK__job_servi__idpkb__2F1AED73 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
;

ALTER TABLE job_service ADD CONSTRAINT FK__job_servi__idpro__2E26C93A 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;


-- 
-- TABLE: job_suggest 
--

ALTER TABLE job_suggest ADD CONSTRAINT FK__job_sugge__idpro__300F11AC 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: job_suggest_service 
--

ALTER TABLE job_suggest_service ADD CONSTRAINT FK__job_sugge__idjob__310335E5 
    FOREIGN KEY (idjobsuggest)
    REFERENCES job_suggest(idjobsuggest)
;

ALTER TABLE job_suggest_service ADD CONSTRAINT FK__job_sugge__idpro__31F75A1E 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;


-- 
-- TABLE: leasing_company 
--

ALTER TABLE leasing_company ADD CONSTRAINT Refparty_role1815 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: leasing_tenor_provide 
--

ALTER TABLE leasing_tenor_provide ADD CONSTRAINT FK__leasing_t__idlea__34D3C6C9 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
;

ALTER TABLE leasing_tenor_provide ADD CONSTRAINT FK__leasing_t__idpro__33DFA290 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: market_treatment 
--

ALTER TABLE market_treatment ADD CONSTRAINT FK__market_tr__idbil__36BC0F3B 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE market_treatment ADD CONSTRAINT FK__market_tr__idven__35C7EB02 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: market_treatment_status 
--

ALTER TABLE market_treatment_status ADD CONSTRAINT FK__market_tr__idmar__38A457AD 
    FOREIGN KEY (idmarkettreatment)
    REFERENCES market_treatment(idmarkettreatment)
;

ALTER TABLE market_treatment_status ADD CONSTRAINT FK__market_tr__idsta__37B03374 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: mechanic 
--

ALTER TABLE mechanic ADD CONSTRAINT FK__mechanic__idparr__39987BE6 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;

ALTER TABLE mechanic ADD CONSTRAINT FK__mechanic__idvend__3A8CA01F 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: memo 
--

ALTER TABLE memo ADD CONSTRAINT FK__memo__idbilling__3B80C458 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE memo ADD CONSTRAINT FK__memo__idinternal__3F51553C 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE memo ADD CONSTRAINT FK__memo__idinvite__3D690CCA 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE memo ADD CONSTRAINT FK__memo__idmemotype__3C74E891 
    FOREIGN KEY (idmemotype)
    REFERENCES memo_type(idmemotype)
;

ALTER TABLE memo ADD CONSTRAINT FK__memo__idoldinvit__3E5D3103 
    FOREIGN KEY (idoldinvite)
    REFERENCES inventory_item(idinvite)
;


-- 
-- TABLE: memo_status 
--

ALTER TABLE memo_status ADD CONSTRAINT FK__memo_stat__idmem__422DC1E7 
    FOREIGN KEY (idmemo)
    REFERENCES memo(idmemo)
;

ALTER TABLE memo_status ADD CONSTRAINT FK__memo_stat__idrea__41399DAE 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE memo_status ADD CONSTRAINT FK__memo_stat__idsta__40457975 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: motor 
--

ALTER TABLE motor ADD CONSTRAINT FK__motor__idproduct__4321E620 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: motor_due_reminder 
--

ALTER TABLE motor_due_reminder ADD CONSTRAINT FK__motor_due__idmot__44160A59 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: moving_slip 
--

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idcon__46F27704 
    FOREIGN KEY (idcontainerfrom)
    REFERENCES container(idcontainer)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idcon__47E69B3D 
    FOREIGN KEY (idcontainerto)
    REFERENCES container(idcontainer)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idfac__49CEE3AF 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idfac__4AC307E8 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idinv__45FE52CB 
    FOREIGN KEY (idinviteto)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idinv__4BB72C21 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idpro__48DABF76 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idreq__4CAB505A 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
;

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idsli__450A2E92 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;


-- 
-- TABLE: order_billing_item 
--

ALTER TABLE order_billing_item ADD CONSTRAINT FK__order_bil__idbil__4E9398CC 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE order_billing_item ADD CONSTRAINT FK__order_bil__idord__4D9F7493 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;


-- 
-- TABLE: order_demand 
--

ALTER TABLE order_demand ADD CONSTRAINT FK__order_dem__idord__507BE13E 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
;

ALTER TABLE order_demand ADD CONSTRAINT FK__order_dem__idpro__4F87BD05 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: order_item 
--

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idfea__544C7222 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idord__51700577 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idpar__5540965B 
    FOREIGN KEY (idparordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idpro__526429B0 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idshi__53584DE9 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
;


-- 
-- TABLE: order_payment 
--

ALTER TABLE order_payment ADD CONSTRAINT FK__order_pay__idord__5634BA94 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE order_payment ADD CONSTRAINT FK__order_pay__idpay__5728DECD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: order_request_item 
--

ALTER TABLE order_request_item ADD CONSTRAINT FK__order_req__idord__581D0306 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE order_request_item ADD CONSTRAINT FK__order_req__idreq__5911273F 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
;


-- 
-- TABLE: order_shipment_item 
--

ALTER TABLE order_shipment_item ADD CONSTRAINT FK__order_shi__idord__5CE1B823 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE order_shipment_item ADD CONSTRAINT FK__order_shi__idshi__5BED93EA 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;


-- 
-- TABLE: orders 
--

ALTER TABLE orders ADD CONSTRAINT FK__orders__idordtyp__5DD5DC5C 
    FOREIGN KEY (idordtyp)
    REFERENCES order_type(idordtyp)
;


-- 
-- TABLE: orders_role 
--

ALTER TABLE orders_role ADD CONSTRAINT FK__orders_ro__idord__5ECA0095 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE orders_role ADD CONSTRAINT FK__orders_ro__idpar__60B24907 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE orders_role ADD CONSTRAINT FK__orders_ro__idrol__5FBE24CE 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: orders_status 
--

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idord__61A66D40 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idord__6482D9EB 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idrea__638EB5B2 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idsta__629A9179 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: organization 
--

ALTER TABLE organization ADD CONSTRAINT FK__organizat__idpar__6576FE24 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE organization ADD CONSTRAINT FK__organizat__idpic__666B225D 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
;

ALTER TABLE organization ADD CONSTRAINT FK__organizat__idpos__675F4696 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
;


-- 
-- TABLE: organization_customer 
--

ALTER TABLE organization_customer ADD CONSTRAINT FK__organizat__idcus__68536ACF 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;


-- 
-- TABLE: organization_gl_account 
--

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idbil__6C23FBB3 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idcat__6E0C4425 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
;

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idgla__6F00685E 
    FOREIGN KEY (idglacc)
    REFERENCES gl_account(idglacc)
;

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idint__6A3BB341 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idpar__69478F08 
    FOREIGN KEY (idparent)
    REFERENCES organization_gl_account(idorgglacc)
;

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idpro__6D181FEC 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idven__6B2FD77A 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: organization_prospect 
--

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpar__70E8B0D0 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
;

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpic__71DCD509 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
;

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpos__72D0F942 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpro__6FF48C97 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;


-- 
-- TABLE: organization_prospect_detail 
--

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idfea__75AD65ED 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idper__73C51D7B 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
;

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idpro__74B941B4 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idpro__76A18A26 
    FOREIGN KEY (idprospect)
    REFERENCES organization_prospect(idprospect)
;


-- 
-- TABLE: organization_suspect 
--

ALTER TABLE organization_suspect ADD CONSTRAINT FK__organizat__idpar__7795AE5F 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
;

ALTER TABLE organization_suspect ADD CONSTRAINT FK__organizat__idsus__7889D298 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: package_receipt 
--

ALTER TABLE package_receipt ADD CONSTRAINT FK__package_r__idpac__7A721B0A 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE package_receipt ADD CONSTRAINT FK__package_r__idshi__797DF6D1 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
;


-- 
-- TABLE: packaging_content 
--

ALTER TABLE packaging_content ADD CONSTRAINT FK__packaging__idpac__7C5A637C 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE packaging_content ADD CONSTRAINT FK__packaging__idshi__7B663F43 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;


-- 
-- TABLE: parent_organization 
--

ALTER TABLE parent_organization ADD CONSTRAINT FK__parent_or__idint__7D4E87B5 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: part_history 
--

ALTER TABLE part_history ADD CONSTRAINT FK__part_hist__idser__7F36D027 
    FOREIGN KEY (idservicehistory)
    REFERENCES service_history(idservicehistory)
;

ALTER TABLE part_history ADD CONSTRAINT FK__part_hist__idveh__7E42ABEE 
    FOREIGN KEY (idvehicleservicehistory)
    REFERENCES vehicle_service_history(idvehicleservicehistory)
;


-- 
-- TABLE: part_issue 
--

ALTER TABLE part_issue ADD CONSTRAINT FK__part_issu__idpar__002AF460 
    FOREIGN KEY (idpartissuetype)
    REFERENCES part_issue_type(idpartissuetype)
;


-- 
-- TABLE: part_issue_action 
--

ALTER TABLE part_issue_action ADD CONSTRAINT FK__part_issu__idiss__011F1899 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
;


-- 
-- TABLE: part_issue_item 
--

ALTER TABLE part_issue_item ADD CONSTRAINT FK__part_issu__idiss__03FB8544 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
;

ALTER TABLE part_issue_item ADD CONSTRAINT FK__part_issu__idpro__02133CD2 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE part_issue_item ADD CONSTRAINT FK__part_issu__idsta__0307610B 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: part_issue_status 
--

ALTER TABLE part_issue_status ADD CONSTRAINT FK__part_issu__idiss__05E3CDB6 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
;

ALTER TABLE part_issue_status ADD CONSTRAINT FK__part_issu__idsta__04EFA97D 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: part_sales_order 
--

ALTER TABLE part_sales_order ADD CONSTRAINT FK__part_sale__idord__06D7F1EF 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
;


-- 
-- TABLE: party_category 
--

ALTER TABLE party_category ADD CONSTRAINT FK__party_cat__idcat__07CC1628 
    FOREIGN KEY (idcattyp)
    REFERENCES party_category_type(idcattyp)
;


-- 
-- TABLE: party_category_impl 
--

ALTER TABLE party_category_impl ADD CONSTRAINT Refparty_category3065 
    FOREIGN KEY (idcategory)
    REFERENCES party_category(idcategory)
;

ALTER TABLE party_category_impl ADD CONSTRAINT Refparty3066 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: party_category_type 
--

ALTER TABLE party_category_type ADD CONSTRAINT FK__party_cat__idpar__0B9CA70C 
    FOREIGN KEY (idparent)
    REFERENCES party_category_type(idcattyp)
;

ALTER TABLE party_category_type ADD CONSTRAINT FK__party_cat__idrul__0AA882D3 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: party_classification 
--

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idcat__0C90CB45 
    FOREIGN KEY (idcategory)
    REFERENCES party_category(idcategory)
;

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idcat__0D84EF7E 
    FOREIGN KEY (idcattyp)
    REFERENCES party_category_type(idcattyp)
;

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idown__0E7913B7 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idpar__0F6D37F0 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: party_contact 
--

ALTER TABLE party_contact ADD CONSTRAINT FK__party_con__idcon__10615C29 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE party_contact ADD CONSTRAINT FK__party_con__idpar__11558062 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: party_contact_mechanism 
--

ALTER TABLE party_contact_mechanism ADD CONSTRAINT FK__party_con__idcon__1249A49B 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE party_contact_mechanism ADD CONSTRAINT FK__party_con__idpar__133DC8D4 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_contact_mechanism ADD CONSTRAINT FK__party_con__idpur__1431ED0D 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: party_document 
--

ALTER TABLE party_document ADD CONSTRAINT FK__party_doc__iddoc__15261146 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
;

ALTER TABLE party_document ADD CONSTRAINT FK__party_doc__idpar__161A357F 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: party_facility_purpose 
--

ALTER TABLE party_facility_purpose ADD CONSTRAINT FK__party_fac__idfac__18027DF1 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE party_facility_purpose ADD CONSTRAINT FK__party_fac__idpar__18F6A22A 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_facility_purpose ADD CONSTRAINT FK__party_fac__idpur__170E59B8 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: party_relationship 
--

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idpar__1BD30ED5 
    FOREIGN KEY (idparrolto)
    REFERENCES party_role(idparrol)
;

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idpar__1CC7330E 
    FOREIGN KEY (idparrolfro)
    REFERENCES party_role(idparrol)
;

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idrel__19EAC663 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
;

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idsta__1ADEEA9C 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: party_role 
--

ALTER TABLE party_role ADD CONSTRAINT FK__party_rol__idpar__1DBB5747 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_role ADD CONSTRAINT FK__party_rol__idrol__1EAF7B80 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: party_role_status 
--

ALTER TABLE party_role_status ADD CONSTRAINT FK__party_rol__idpar__1FA39FB9 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;

ALTER TABLE party_role_status ADD CONSTRAINT FK__party_rol__idrea__218BE82B 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE party_role_status ADD CONSTRAINT FK__party_rol__idsta__2097C3F2 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: party_role_type 
--

ALTER TABLE party_role_type ADD CONSTRAINT FK__party_rol__idpar__2374309D 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_role_type ADD CONSTRAINT FK__party_rol__idrol__22800C64 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: payment 
--

ALTER TABLE payment ADD CONSTRAINT FK__payment__idinter__26509D48 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpaifr__2838E5BA 
    FOREIGN KEY (idpaifro)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpaito__2744C181 
    FOREIGN KEY (idpaito)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpayme__255C790F 
    FOREIGN KEY (idpaymet)
    REFERENCES payment_method(idpaymet)
;

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpayty__246854D6 
    FOREIGN KEY (idpaytyp)
    REFERENCES payment_type(idpaytyp)
;


-- 
-- TABLE: payment_acctg_trans 
--

ALTER TABLE payment_acctg_trans ADD CONSTRAINT FK__payment_a__idacc__2A212E2C 
    FOREIGN KEY (idacctra)
    REFERENCES external_acctg_trans(idacctra)
;

ALTER TABLE payment_acctg_trans ADD CONSTRAINT FK__payment_a__idpay__292D09F3 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: payment_application 
--

ALTER TABLE payment_application ADD CONSTRAINT FK__payment_a__idbil__2C09769E 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
;

ALTER TABLE payment_application ADD CONSTRAINT FK__payment_a__idbil__2CFD9AD7 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE payment_application ADD CONSTRAINT FK__payment_a__idpay__2B155265 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: payment_method 
--

ALTER TABLE payment_method ADD CONSTRAINT FK__payment_m__idpay__2DF1BF10 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
;


-- 
-- TABLE: payment_role 
--

ALTER TABLE payment_role ADD CONSTRAINT FK__payment_r__idpar__30CE2BBB 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE payment_role ADD CONSTRAINT FK__payment_r__idpay__2EE5E349 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE payment_role ADD CONSTRAINT FK__payment_r__idrol__2FDA0782 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: payment_status 
--

ALTER TABLE payment_status ADD CONSTRAINT FK__payment_s__idpay__31C24FF4 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE payment_status ADD CONSTRAINT FK__payment_s__idrea__33AA9866 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE payment_status ADD CONSTRAINT FK__payment_s__idsta__32B6742D 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: person 
--

ALTER TABLE person ADD CONSTRAINT FK__person__idparty__349EBC9F 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: person_prospect 
--

ALTER TABLE person_prospect ADD CONSTRAINT FK__person_pr__idpar__36870511 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE person_prospect ADD CONSTRAINT FK__person_pr__idpro__3592E0D8 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;


-- 
-- TABLE: person_suspect 
--

ALTER TABLE person_suspect ADD CONSTRAINT FK__person_su__idpar__377B294A 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE person_suspect ADD CONSTRAINT FK__person_su__idsus__386F4D83 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: personal_customer 
--

ALTER TABLE personal_customer ADD CONSTRAINT FK__personal___idcus__396371BC 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;


-- 
-- TABLE: personsosmed 
--

ALTER TABLE personsosmed ADD CONSTRAINT FK__personsos__idpar__3B4BBA2E 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE personsosmed ADD CONSTRAINT FK__personsos__idsos__3A5795F5 
    FOREIGN KEY (idsosmedtype)
    REFERENCES sosmedtype(idsosmedtype)
;


-- 
-- TABLE: picking_slip 
--

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idinv__3D3402A0 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idord__3F1C4B12 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idshi__3C3FDE67 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
;

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idsli__3E2826D9 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;


-- 
-- TABLE: pkb 
--

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idcustomer__41F8B7BD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idinternal__40106F4B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idpaymettyp__42ECDBF6 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idpkbjobret__48A5B54C 
    FOREIGN KEY (idpkbjobreturn)
    REFERENCES pkb(idpkb)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idqueue__43E1002F 
    FOREIGN KEY (idqueue)
    REFERENCES queue(idqueue)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idstatuspkb__44D52468 
    FOREIGN KEY (idstatuspkbcust)
    REFERENCES status_pkb(idstatuspkb)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idstatuspkb__47B19113 
    FOREIGN KEY (idstatuspkbinternal)
    REFERENCES status_pkb(idstatuspkb)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idtypepkb__45C948A1 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idvehicle__46BD6CDA 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idvehide__41049384 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
;


-- 
-- TABLE: pkb_estimation 
--

ALTER TABLE pkb_estimation ADD CONSTRAINT FK__pkb_estim__idpkb__4999D985 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
;


-- 
-- TABLE: pkb_part 
--

ALTER TABLE pkb_part ADD CONSTRAINT FK__pkb_part__idchar__4A8DFDBE 
    FOREIGN KEY (idchargeto)
    REFERENCES charge_to_type(idchargeto)
;

ALTER TABLE pkb_part ADD CONSTRAINT FK__pkb_part__idpkb__4B8221F7 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
;

ALTER TABLE pkb_part ADD CONSTRAINT FK__pkb_part__idpkbs__4C764630 
    FOREIGN KEY (idpkbservice)
    REFERENCES pkb_service(IdPkbService)
;


-- 
-- TABLE: pkb_part_billing_item 
--

ALTER TABLE pkb_part_billing_item ADD CONSTRAINT FK__pkb_part___idbil__4E5E8EA2 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE pkb_part_billing_item ADD CONSTRAINT FK__pkb_part___idpar__4D6A6A69 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
;


-- 
-- TABLE: pkb_part_request_item 
--

ALTER TABLE pkb_part_request_item ADD CONSTRAINT FK__pkb_part___idreq__4F52B2DB 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
;


-- 
-- TABLE: pkb_service 
--

ALTER TABLE pkb_service ADD CONSTRAINT FK__pkb_servi__idcha__5046D714 
    FOREIGN KEY (idchargeto)
    REFERENCES charge_to_type(idchargeto)
;

ALTER TABLE pkb_service ADD CONSTRAINT FK__pkb_servi__idpkb__522F1F86 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
;

ALTER TABLE pkb_service ADD CONSTRAINT FK__pkb_servi__idpro__513AFB4D 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;


-- 
-- TABLE: pkb_service_billing_item 
--

ALTER TABLE pkb_service_billing_item ADD CONSTRAINT FK__pkb_servi__idbil__541767F8 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE pkb_service_billing_item ADD CONSTRAINT FK__pkb_servi__idpar__532343BF 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
;


-- 
-- TABLE: po_detail_part 
--

ALTER TABLE po_detail_part ADD CONSTRAINT FK__po_detail__idord__550B8C31 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;


-- 
-- TABLE: po_detail_service 
--

ALTER TABLE po_detail_service ADD CONSTRAINT FK__po_detail__idord__55FFB06A 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;


-- 
-- TABLE: po_status 
--

ALTER TABLE po_status ADD CONSTRAINT FK__po_status__idsta__56F3D4A3 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: position_authority 
--

ALTER TABLE position_authority ADD CONSTRAINT FK__position___idpos__57E7F8DC 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
;


-- 
-- TABLE: position_fullfillment 
--

ALTER TABLE position_fullfillment ADD CONSTRAINT FK__position___idper__59D0414E 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
;

ALTER TABLE position_fullfillment ADD CONSTRAINT FK__position___idpos__58DC1D15 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
;


-- 
-- TABLE: position_reporting_structure 
--

ALTER TABLE position_reporting_structure ADD CONSTRAINT FK__position___idpos__5AC46587 
    FOREIGN KEY (idposfro)
    REFERENCES positions(idposition)
;

ALTER TABLE position_reporting_structure ADD CONSTRAINT FK__position___idpos__5BB889C0 
    FOREIGN KEY (idposto)
    REFERENCES positions(idposition)
;


-- 
-- TABLE: positions 
--

ALTER TABLE positions ADD CONSTRAINT FK__positions__idint__5DA0D232 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE positions ADD CONSTRAINT FK__positions__idorg__5E94F66B 
    FOREIGN KEY (idorganization)
    REFERENCES organization(idparty)
;

ALTER TABLE positions ADD CONSTRAINT FK__positions__idpos__5CACADF9 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
;

ALTER TABLE positions ADD CONSTRAINT FK__positions__idusr__5F891AA4 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
;


-- 
-- TABLE: positions_status 
--

ALTER TABLE positions_status ADD CONSTRAINT FK__positions__idpos__607D3EDD 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
;

ALTER TABLE positions_status ADD CONSTRAINT FK__positions__idrea__6265874F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE positions_status ADD CONSTRAINT FK__positions__idsta__61716316 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: postal_address 
--

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idcit__6541F3FA 
    FOREIGN KEY (idcity)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idcon__6359AB88 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__iddis__66361833 
    FOREIGN KEY (iddistrict)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idpos__672A3C6C 
    FOREIGN KEY (idpostal)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idpro__644DCFC1 
    FOREIGN KEY (idprovince)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idvil__681E60A5 
    FOREIGN KEY (idvillage)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: price_agreement_item 
--

ALTER TABLE price_agreement_item ADD CONSTRAINT FK__price_agr__idagr__6A06A917 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
;

ALTER TABLE price_agreement_item ADD CONSTRAINT FK__price_agr__idpro__691284DE 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: price_component 
--

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idagr__6ECB5E34 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
;

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idgeo__6DD739FB 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpar__6AFACD50 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpar__6FBF826D 
    FOREIGN KEY (idparcat)
    REFERENCES party_category(idcategory)
;

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpri__6CE315C2 
    FOREIGN KEY (idpricetype)
    REFERENCES price_type(idpricetype)
;

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpro__6BEEF189 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpro__70B3A6A6 
    FOREIGN KEY (idprocat)
    REFERENCES product_category(idcategory)
;


-- 
-- TABLE: price_type 
--

ALTER TABLE price_type ADD CONSTRAINT FK__price_typ__idrul__71A7CADF 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: product 
--

ALTER TABLE product ADD CONSTRAINT FK__product__idproty__729BEF18 
    FOREIGN KEY (idprotyp)
    REFERENCES product_type(idprotyp)
;


-- 
-- TABLE: product_association 
--

ALTER TABLE product_association ADD CONSTRAINT FK__product_a__idass__75785BC3 
    FOREIGN KEY (idasstyp)
    REFERENCES association_type(idasstyp)
;

ALTER TABLE product_association ADD CONSTRAINT FK__product_a__idpro__73901351 
    FOREIGN KEY (idproductfrom)
    REFERENCES product(idproduct)
;

ALTER TABLE product_association ADD CONSTRAINT FK__product_a__idpro__7484378A 
    FOREIGN KEY (idproductto)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_category 
--

ALTER TABLE product_category ADD CONSTRAINT FK__product_c__idcat__766C7FFC 
    FOREIGN KEY (idcattyp)
    REFERENCES product_category_type(idcattyp)
;


-- 
-- TABLE: product_category_impl 
--

ALTER TABLE product_category_impl ADD CONSTRAINT Refproduct_category3059 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
;

ALTER TABLE product_category_impl ADD CONSTRAINT Refproduct3060 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_category_type 
--

ALTER TABLE product_category_type ADD CONSTRAINT FK__product_c__idpar__7A3D10E0 
    FOREIGN KEY (idparent)
    REFERENCES product_category_type(idcattyp)
;

ALTER TABLE product_category_type ADD CONSTRAINT FK__product_c__idrul__7948ECA7 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: product_classification 
--

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idcat__7B313519 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
;

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idcat__7C255952 
    FOREIGN KEY (idcattyp)
    REFERENCES product_category_type(idcattyp)
;

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idown__7D197D8B 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idpro__7E0DA1C4 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_document 
--

ALTER TABLE product_document ADD CONSTRAINT FK__product_d__iddoc__7F01C5FD 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
;

ALTER TABLE product_document ADD CONSTRAINT FK__product_d__idpro__7FF5EA36 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_feature_impl 
--

ALTER TABLE product_feature_impl ADD CONSTRAINT Reffeature3061 
    FOREIGN KEY (features_id)
    REFERENCES feature(idfeature)
;

ALTER TABLE product_feature_impl ADD CONSTRAINT Refproduct3062 
    FOREIGN KEY (products_id)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_package_receipt 
--

ALTER TABLE product_package_receipt ADD CONSTRAINT FK__product_p__idpac__03C67B1A 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE product_package_receipt ADD CONSTRAINT FK__product_p__idshi__02D256E1 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
;


-- 
-- TABLE: product_purchase_order 
--

ALTER TABLE product_purchase_order ADD CONSTRAINT FK__product_p__idord__04BA9F53 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
;


-- 
-- TABLE: product_shipment_incoming 
--

ALTER TABLE product_shipment_incoming ADD CONSTRAINT FK__product_s__idshi__05AEC38C 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: product_shipment_receipt 
--

ALTER TABLE product_shipment_receipt ADD CONSTRAINT FK__product_s__idrec__06A2E7C5 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;


-- 
-- TABLE: prospect 
--

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idbrok__097F5470 
    FOREIGN KEY (idbroker)
    REFERENCES sales_broker(idparrol)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idevet__0A7378A9 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idfaci__0B679CE2 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idinte__07970BFE 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idpros__088B3037 
    FOREIGN KEY (idprosou)
    REFERENCES prospect_source(idprosou)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idsale__0C5BC11B 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idsale__0E44098D 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
;

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idsusp__0D4FE554 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: prospect_role 
--

ALTER TABLE prospect_role ADD CONSTRAINT FK__prospect___idpar__11207638 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE prospect_role ADD CONSTRAINT FK__prospect___idpro__0F382DC6 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE prospect_role ADD CONSTRAINT FK__prospect___idrol__102C51FF 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: prospect_status 
--

ALTER TABLE prospect_status ADD CONSTRAINT FK__prospect___idpro__12149A71 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE prospect_status ADD CONSTRAINT FK__prospect___idrea__13FCE2E3 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE prospect_status ADD CONSTRAINT FK__prospect___idsta__1308BEAA 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: province 
--

ALTER TABLE province ADD CONSTRAINT Refgeo_boundary1813 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: purchase_order 
--

ALTER TABLE purchase_order ADD CONSTRAINT FK__purchase___idord__15E52B55 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
;


-- 
-- TABLE: purchase_request 
--

ALTER TABLE purchase_request ADD CONSTRAINT FK__purchase___idint__17CD73C7 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE purchase_request ADD CONSTRAINT FK__purchase___idpro__16D94F8E 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE purchase_request ADD CONSTRAINT FK__purchase___idreq__18C19800 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: purchaseorder_reception_tr 
--

ALTER TABLE purchaseorder_reception_tr ADD CONSTRAINT FK__purchaseo__idcat__1AA9E072 
    FOREIGN KEY (idcategoryasset)
    REFERENCES category_asset_tp(idcategoryasset)
;

ALTER TABLE purchaseorder_reception_tr ADD CONSTRAINT FK__purchaseo__idpur__19B5BC39 
    FOREIGN KEY (idpurchaseorder)
    REFERENCES purchaseorder_tm(idpurchaseorder)
;


-- 
-- TABLE: purchaseorder_tm 
--

ALTER TABLE purchaseorder_tm ADD CONSTRAINT FK__purchaseo__statu__1B9E04AB 
    FOREIGN KEY (status)
    REFERENCES purchaseorder_status_tp(idpurchaseorderstatus)
;


-- 
-- TABLE: queue 
--

ALTER TABLE queue ADD CONSTRAINT FK__queue__idbooking__1E7A7156 
    FOREIGN KEY (idbooking)
    REFERENCES booking(idbooking)
;

ALTER TABLE queue ADD CONSTRAINT FK__queue__idqueuest__1C9228E4 
    FOREIGN KEY (idqueuestatus)
    REFERENCES queue_status(idqueuestatus)
;

ALTER TABLE queue ADD CONSTRAINT FK__queue__idqueuety__1D864D1D 
    FOREIGN KEY (idqueuetype)
    REFERENCES queue_type(idqueuetype)
;


-- 
-- TABLE: quote 
--

ALTER TABLE quote ADD CONSTRAINT Refquote_type1820 
    FOREIGN KEY (idquotyp)
    REFERENCES quote_type(idquotyp)
;


-- 
-- TABLE: quote_item 
--

ALTER TABLE quote_item ADD CONSTRAINT FK__quote_ite__idpro__1F6E958F 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE quote_item ADD CONSTRAINT FK__quote_ite__idquo__2062B9C8 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;


-- 
-- TABLE: quote_role 
--

ALTER TABLE quote_role ADD CONSTRAINT FK__quote_rol__idpar__233F2673 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE quote_role ADD CONSTRAINT FK__quote_rol__idquo__224B023A 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE quote_role ADD CONSTRAINT FK__quote_rol__idrol__2156DE01 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: quote_status 
--

ALTER TABLE quote_status ADD CONSTRAINT FK__quote_sta__idquo__261B931E 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE quote_status ADD CONSTRAINT FK__quote_sta__idrea__25276EE5 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE quote_status ADD CONSTRAINT FK__quote_sta__idsta__24334AAC 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: receipt 
--

ALTER TABLE receipt ADD CONSTRAINT FK__receipt__idpayme__270FB757 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE receipt ADD CONSTRAINT FK__receipt__idreq__2803DB90 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: regular_sales_order 
--

ALTER TABLE regular_sales_order ADD CONSTRAINT FK__regular_s__idord__28F7FFC9 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
;


-- 
-- TABLE: reimbursement_part 
--

ALTER TABLE reimbursement_part ADD CONSTRAINT FK__reimburse__idpro__2BD46C74 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE reimbursement_part ADD CONSTRAINT FK__reimburse__idspg__29EC2402 
    FOREIGN KEY (idspgclaimdetail)
    REFERENCES spg_claim_detail(idspgclaimdetail)
;

ALTER TABLE reimbursement_part ADD CONSTRAINT FK__reimburse__idspg__2AE0483B 
    FOREIGN KEY (idspgclaimaction)
    REFERENCES spg_claim_action(idspgclaimaction)
;


-- 
-- TABLE: rem_part 
--

ALTER TABLE rem_part ADD CONSTRAINT FK__rem_part__idprod__2CC890AD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: reminder_sent_status 
--

ALTER TABLE reminder_sent_status ADD CONSTRAINT FK__reminder___idrem__2DBCB4E6 
    FOREIGN KEY (idreminder)
    REFERENCES service_reminder(idreminder)
;


-- 
-- TABLE: reorder_guideline 
--

ALTER TABLE reorder_guideline ADD CONSTRAINT Refgood1823 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE reorder_guideline ADD CONSTRAINT Refinternal1824 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: request 
--

ALTER TABLE request ADD CONSTRAINT FK__request__idinter__2EB0D91F 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE request ADD CONSTRAINT FK__request__idreqty__2FA4FD58 
    FOREIGN KEY (idreqtype)
    REFERENCES request_type(idreqtype)
;


-- 
-- TABLE: request_item 
--

ALTER TABLE request_item ADD CONSTRAINT FK__request_i__idfea__32816A03 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE request_item ADD CONSTRAINT FK__request_i__idpro__30992191 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE request_item ADD CONSTRAINT FK__request_i__idreq__318D45CA 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;


-- 
-- TABLE: request_product 
--

ALTER TABLE request_product ADD CONSTRAINT FK__request_p__idfac__33758E3C 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
;

ALTER TABLE request_product ADD CONSTRAINT FK__request_p__idfac__3469B275 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
;

ALTER TABLE request_product ADD CONSTRAINT FK__request_p__idreq__355DD6AE 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;


-- 
-- TABLE: request_requirement 
--

ALTER TABLE request_requirement ADD CONSTRAINT FK__request_r__idreq__3651FAE7 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;

ALTER TABLE request_requirement ADD CONSTRAINT FK__request_r__idreq__37461F20 
    FOREIGN KEY (idrequirement)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: request_role 
--

ALTER TABLE request_role ADD CONSTRAINT FK__request_r__idpar__392E6792 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE request_role ADD CONSTRAINT FK__request_r__idreq__3A228BCB 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;

ALTER TABLE request_role ADD CONSTRAINT FK__request_r__idrol__383A4359 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: request_status 
--

ALTER TABLE request_status ADD CONSTRAINT FK__request_s__idrea__3B16B004 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE request_status ADD CONSTRAINT FK__request_s__idreq__3CFEF876 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;

ALTER TABLE request_status ADD CONSTRAINT FK__request_s__idsta__3C0AD43D 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: request_unit_internal 
--

ALTER TABLE request_unit_internal ADD CONSTRAINT FK__request_u__idreq__3DF31CAF 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;


-- 
-- TABLE: requirement 
--

ALTER TABLE requirement ADD CONSTRAINT FK__requireme__idfac__3EE740E8 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE requirement ADD CONSTRAINT FK__requireme__idpar__3FDB6521 
    FOREIGN KEY (idparentreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE requirement ADD CONSTRAINT FK__requireme__idreq__40CF895A 
    FOREIGN KEY (idreqtyp)
    REFERENCES requirement_type(idreqtyp)
;


-- 
-- TABLE: requirement_order_item 
--

ALTER TABLE requirement_order_item ADD CONSTRAINT FK__requireme__idord__41C3AD93 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE requirement_order_item ADD CONSTRAINT FK__requireme__idreq__42B7D1CC 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: requirement_payment 
--

ALTER TABLE requirement_payment ADD CONSTRAINT FK__requireme__idpay__44A01A3E 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE requirement_payment ADD CONSTRAINT FK__requireme__idreq__43ABF605 
    FOREIGN KEY (requirements_id)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: requirement_role 
--

ALTER TABLE requirement_role ADD CONSTRAINT FK__requireme__idpar__477C86E9 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE requirement_role ADD CONSTRAINT FK__requireme__idreq__45943E77 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE requirement_role ADD CONSTRAINT FK__requireme__idrol__468862B0 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: requirement_status 
--

ALTER TABLE requirement_status ADD CONSTRAINT FK__requireme__idrea__4A58F394 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE requirement_status ADD CONSTRAINT FK__requireme__idreq__4870AB22 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE requirement_status ADD CONSTRAINT FK__requireme__idsta__4964CF5B 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: return_purchase_order 
--

ALTER TABLE return_purchase_order ADD CONSTRAINT Refvendor_order1806 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
;


-- 
-- TABLE: return_sales_order 
--

ALTER TABLE return_sales_order ADD CONSTRAINT Refcustomer_order1805 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
;


-- 
-- TABLE: room_tm 
--

ALTER TABLE room_tm ADD CONSTRAINT FK__room_tm__idfloor__4D35603F 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
;


-- 
-- TABLE: rule_hot_item 
--

ALTER TABLE rule_hot_item ADD CONSTRAINT FK__rule_hot___idpro__4F1DA8B1 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE rule_hot_item ADD CONSTRAINT FK__rule_hot___idrul__4E298478 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
;


-- 
-- TABLE: rule_indent 
--

ALTER TABLE rule_indent ADD CONSTRAINT FK__rule_inde__idpro__5105F123 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE rule_indent ADD CONSTRAINT FK__rule_inde__idrul__5011CCEA 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
;


-- 
-- TABLE: rule_sales_discount 
--

ALTER TABLE rule_sales_discount ADD CONSTRAINT FK__rule_sale__idrul__51FA155C 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
;


-- 
-- TABLE: rules 
--

ALTER TABLE rules ADD CONSTRAINT FK__rules__idinterna__53E25DCE 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE rules ADD CONSTRAINT FK__rules__idrultyp__52EE3995 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: sale_type 
--

ALTER TABLE sale_type ADD CONSTRAINT FK__sale_type__idpar__54D68207 
    FOREIGN KEY (idparent)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: sales_agreement 
--

ALTER TABLE sales_agreement ADD CONSTRAINT FK__sales_agr__idagr__57B2EEB2 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE sales_agreement ADD CONSTRAINT FK__sales_agr__idcus__56BECA79 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idcustomer)
;

ALTER TABLE sales_agreement ADD CONSTRAINT FK__sales_agr__idint__55CAA640 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: sales_booking 
--

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idfea__5A8F5B5D 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idint__5B837F96 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idpro__599B3724 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idreq__58A712EB 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
;


-- 
-- TABLE: sales_broker 
--

ALTER TABLE sales_broker ADD CONSTRAINT FK__sales_bro__idbro__5D6BC808 
    FOREIGN KEY (idbrotyp)
    REFERENCES broker_type(idbrotyp)
;

ALTER TABLE sales_broker ADD CONSTRAINT FK__sales_bro__idpar__5C77A3CF 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: sales_order 
--

ALTER TABLE sales_order ADD CONSTRAINT FK__sales_ord__idord__5E5FEC41 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
;

ALTER TABLE sales_order ADD CONSTRAINT FK__sales_ord__idsal__5F54107A 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: sales_point 
--

ALTER TABLE sales_point ADD CONSTRAINT FK__sales_poi__idint__604834B3 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: sales_unit_leasing 
--

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idlea__613C58EC 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
;

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idlsg__650CE9D0 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
;

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idord__6418C597 
    FOREIGN KEY (idorder)
    REFERENCES vehicle_sales_order(idorder)
;

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idreq__6324A15E 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
;

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idsta__62307D25 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: sales_unit_requirement 
--

ALTER TABLE sales_unit_requirement ADD CONSTRAINT Reforganization3031 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idbil__69D19EED 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idcol__6CAE0B98 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idcus__67E9567B 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idint__6DA22FD1 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idlsg__707E9C7C 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idown__6F8A7843 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idpro__68DD7AB4 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idpro__6BB9E75F 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idreq__66010E09 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idreq__7172C0B5 
    FOREIGN KEY (idrequest)
    REFERENCES vehicle_customer_request(idrequest)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idsal__66F53242 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idsal__6AC5C326 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idsal__6E96540A 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
;


-- 
-- TABLE: salesman 
--

ALTER TABLE salesman ADD CONSTRAINT FK__salesman__idcoor__735B0927 
    FOREIGN KEY (idcoordinator)
    REFERENCES salesman(idparrol)
;

ALTER TABLE salesman ADD CONSTRAINT FK__salesman__idparr__7266E4EE 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: service 
--

ALTER TABLE service ADD CONSTRAINT FK__service__idprodu__75435199 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE service ADD CONSTRAINT FK__service__iduom__744F2D60 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: service_agreement 
--

ALTER TABLE service_agreement ADD CONSTRAINT FK__service_a__idagr__763775D2 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE service_agreement ADD CONSTRAINT FK__service_a__idcus__781FBE44 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idcustomer)
;

ALTER TABLE service_agreement ADD CONSTRAINT FK__service_a__idint__772B9A0B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: service_history 
--

ALTER TABLE service_history ADD CONSTRAINT FK__service_h__idveh__7913E27D 
    FOREIGN KEY (idvehicleservicehistory)
    REFERENCES vehicle_service_history(idvehicleservicehistory)
;


-- 
-- TABLE: service_kpb 
--

ALTER TABLE service_kpb ADD CONSTRAINT FK__service_k__idpro__7A0806B6 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;


-- 
-- TABLE: service_per_motor 
--

ALTER TABLE service_per_motor ADD CONSTRAINT FK__service_p__idpro__7AFC2AEF 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;


-- 
-- TABLE: service_reminder 
--

ALTER TABLE service_reminder ADD CONSTRAINT FK__service_r__idcla__7BF04F28 
    FOREIGN KEY (idclaimtype)
    REFERENCES dealer_claim_type(idclaimtype)
;

ALTER TABLE service_reminder ADD CONSTRAINT FK__service_r__idrem__7CE47361 
    FOREIGN KEY (idremindertype)
    REFERENCES dealer_reminder_type(idremindertype)
;

ALTER TABLE service_reminder ADD CONSTRAINT FK__service_r__idveh__7DD8979A 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
;


-- 
-- TABLE: service_reminder_event 
--

ALTER TABLE service_reminder_event ADD CONSTRAINT FK__service_r__idcom__7FC0E00C 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE service_reminder_event ADD CONSTRAINT FK__service_r__idrem__7ECCBBD3 
    FOREIGN KEY (idreminder)
    REFERENCES service_reminder(idreminder)
;


-- 
-- TABLE: ship_to 
--

ALTER TABLE ship_to ADD CONSTRAINT FK__ship_to__00B50445 
    FOREIGN KEY (idparty, idroletype)
    REFERENCES party_role_type(idparty, idroletype)
;


-- 
-- TABLE: shipment 
--

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idfaci__029D4CB7 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idinte__076201D4 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idposa__0579B962 
    FOREIGN KEY (idposaddfro)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idposa__066DDD9B 
    FOREIGN KEY (idposaddto)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idshif__039170F0 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
;

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idshit__01A9287E 
    FOREIGN KEY (idshityp)
    REFERENCES shipment_type(idshityp)
;

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idshit__04859529 
    FOREIGN KEY (idshito)
    REFERENCES ship_to(idshipto)
;


-- 
-- TABLE: shipment_billing_item 
--

ALTER TABLE shipment_billing_item ADD CONSTRAINT FK__shipment___idbil__0856260D 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE shipment_billing_item ADD CONSTRAINT FK__shipment___idshi__094A4A46 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;


-- 
-- TABLE: shipment_incoming 
--

ALTER TABLE shipment_incoming ADD CONSTRAINT FK__shipment___idshi__0A3E6E7F 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: shipment_item 
--

ALTER TABLE shipment_item ADD CONSTRAINT FK__shipment___idfea__0C26B6F1 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE shipment_item ADD CONSTRAINT FK__shipment___idpro__0D1ADB2A 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE shipment_item ADD CONSTRAINT FK__shipment___idshi__0B3292B8 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: shipment_outgoing 
--

ALTER TABLE shipment_outgoing ADD CONSTRAINT FK__shipment___iddri__0F03239C 
    FOREIGN KEY (idparrol)
    REFERENCES driver(idparrol)
;

ALTER TABLE shipment_outgoing ADD CONSTRAINT FK__shipment___idshi__0E0EFF63 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: shipment_package 
--

ALTER TABLE shipment_package ADD CONSTRAINT FK__shipment___idint__10EB6C0E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE shipment_package ADD CONSTRAINT FK__shipment___idshi__0FF747D5 
    FOREIGN KEY (idshipactyp)
    REFERENCES shipment_package_type(idshipactyp)
;


-- 
-- TABLE: shipment_package_role 
--

ALTER TABLE shipment_package_role ADD CONSTRAINT FK__shipment___idpac__12D3B480 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE shipment_package_role ADD CONSTRAINT FK__shipment___idpar__13C7D8B9 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE shipment_package_role ADD CONSTRAINT FK__shipment___idrol__11DF9047 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: shipment_package_status 
--

ALTER TABLE shipment_package_status ADD CONSTRAINT FK__shipment___idpac__15B0212B 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE shipment_package_status ADD CONSTRAINT FK__shipment___idrea__14BBFCF2 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE shipment_package_status ADD CONSTRAINT FK__shipment___idsta__16A44564 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: shipment_receipt 
--

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idfea__1980B20F 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idord__1A74D648 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idpac__188C8DD6 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idpro__1798699D 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idshi__1B68FA81 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;


-- 
-- TABLE: shipment_receipt_role 
--

ALTER TABLE shipment_receipt_role ADD CONSTRAINT FK__shipment___idpar__1C5D1EBA 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE shipment_receipt_role ADD CONSTRAINT FK__shipment___idrec__1E45672C 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;

ALTER TABLE shipment_receipt_role ADD CONSTRAINT FK__shipment___idrol__1D5142F3 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: shipment_receipt_status 
--

ALTER TABLE shipment_receipt_status ADD CONSTRAINT FK__shipment___idrea__202DAF9E 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE shipment_receipt_status ADD CONSTRAINT FK__shipment___idrec__2121D3D7 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;

ALTER TABLE shipment_receipt_status ADD CONSTRAINT FK__shipment___idsta__1F398B65 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: shipment_status 
--

ALTER TABLE shipment_status ADD CONSTRAINT FK__shipment___idrea__23FE4082 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE shipment_status ADD CONSTRAINT FK__shipment___idshi__2215F810 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;

ALTER TABLE shipment_status ADD CONSTRAINT FK__shipment___idsta__230A1C49 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: spg_claim_detail 
--

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idbil__29B719D8 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idpro__24F264BB 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idrec__25E688F4 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idspg__26DAAD2D 
    FOREIGN KEY (idspgclaim)
    REFERENCES spg_claim(idspgclaim)
;

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idspg__27CED166 
    FOREIGN KEY (idspgclaimtype)
    REFERENCES spg_claim_type(idspgclaimtype)
;

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idsta__28C2F59F 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: standard_calendar 
--

ALTER TABLE standard_calendar ADD CONSTRAINT FK__standard___idcal__2B9F624A 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
;

ALTER TABLE standard_calendar ADD CONSTRAINT FK__standard___idint__2AAB3E11 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;


-- 
-- TABLE: status_pkb 
--

ALTER TABLE status_pkb ADD CONSTRAINT FK__status_pk__idsta__2C938683 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: stock_opname 
--

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idcal__2E7BCEF5 
    FOREIGN KEY (idcalendar)
    REFERENCES standard_calendar(idcalendar)
;

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idfac__2D87AABC 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idint__2F6FF32E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idstk__30641767 
    FOREIGN KEY (idstkopntyp)
    REFERENCES stock_opname_type(idstkopntyp)
;


-- 
-- TABLE: stock_opname_inventory 
--

ALTER TABLE stock_opname_inventory ADD CONSTRAINT FK__stock_opn__idinv__324C5FD9 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE stock_opname_inventory ADD CONSTRAINT FK__stock_opn__idsto__31583BA0 
    FOREIGN KEY (idstopnite)
    REFERENCES stock_opname_item(idstopnite)
;


-- 
-- TABLE: stock_opname_item 
--

ALTER TABLE stock_opname_item ADD CONSTRAINT FK__stock_opn__idcon__3434A84B 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
;

ALTER TABLE stock_opname_item ADD CONSTRAINT FK__stock_opn__idpro__33408412 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE stock_opname_item ADD CONSTRAINT FK__stock_opn__idstk__3528CC84 
    FOREIGN KEY (idstkop)
    REFERENCES stock_opname(idstkop)
;


-- 
-- TABLE: stock_opname_status 
--

ALTER TABLE stock_opname_status ADD CONSTRAINT Refstatus_type2419 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE stock_opname_status ADD CONSTRAINT Refreason_type2420 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE stock_opname_status ADD CONSTRAINT FK__stock_opn__idstk__361CF0BD 
    FOREIGN KEY (idstkop)
    REFERENCES stock_opname(idstkop)
;


-- 
-- TABLE: stockopname_item_tm 
--

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idfix__39ED81A1 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
;

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idflo__371114F6 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
;

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idroo__3805392F 
    FOREIGN KEY (idroom)
    REFERENCES room_tm(idroom)
;

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idsto__38F95D68 
    FOREIGN KEY (idstockopname)
    REFERENCES stockopname_tm(idstockopname)
;


-- 
-- TABLE: stockopname_tm 
--

ALTER TABLE stockopname_tm ADD CONSTRAINT FK__stockopna__statu__3AE1A5DA 
    FOREIGN KEY (status)
    REFERENCES stockopname_status_tp(idstockopnamestatus)
;


-- 
-- TABLE: suggest_part 
--

ALTER TABLE suggest_part ADD CONSTRAINT FK__suggest_p__idpro__3BD5CA13 
    FOREIGN KEY (idproductmotor)
    REFERENCES motor(idproduct)
;

ALTER TABLE suggest_part ADD CONSTRAINT FK__suggest_p__idpro__3CC9EE4C 
    FOREIGN KEY (idproductpart)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: summary 
--

ALTER TABLE summary ADD CONSTRAINT FK__summary__idpkb__3DBE1285 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
;


-- 
-- TABLE: suspect 
--

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idaddre__4282C7A2 
    FOREIGN KEY (idaddress)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__iddeale__3EB236BE 
    FOREIGN KEY (iddealer)
    REFERENCES internal(idinternal)
;

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idparty__3FA65AF7 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idsales__409A7F30 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
;

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idsales__418EA369 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idsuspe__4376EBDB 
    FOREIGN KEY (idsuspecttyp)
    REFERENCES suspect_type(idsuspecttyp)
;


-- 
-- TABLE: suspect_role 
--

ALTER TABLE suspect_role ADD CONSTRAINT FK__suspect_r__idpar__455F344D 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE suspect_role ADD CONSTRAINT FK__suspect_r__idrol__46535886 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE suspect_role ADD CONSTRAINT FK__suspect_r__idsus__446B1014 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: suspect_status 
--

ALTER TABLE suspect_status ADD CONSTRAINT FK__suspect_s__idrea__492FC531 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE suspect_status ADD CONSTRAINT FK__suspect_s__idsta__483BA0F8 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE suspect_status ADD CONSTRAINT FK__suspect_s__idsus__47477CBF 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: telecomunication_number 
--

ALTER TABLE telecomunication_number ADD CONSTRAINT FK__telecomun__idcon__4A23E96A 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: unit_accesories_mapper 
--

ALTER TABLE unit_accesories_mapper ADD CONSTRAINT FK__unit_acce__idmot__4B180DA3 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: unit_deliverable 
--

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__idcon__4DF47A4E 
    FOREIGN KEY (idconstatyp)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__iddel__4EE89E87 
    FOREIGN KEY (iddeltype)
    REFERENCES deliverable_type(iddeltype)
;

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__idreq__4C0C31DC 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_document_requirement(idreq)
;

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__idvnd__4D005615 
    FOREIGN KEY (idvndstatyp)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: unit_document_message 
--

ALTER TABLE unit_document_message ADD CONSTRAINT FK__unit_docu__idpar__50D0E6F9 
    FOREIGN KEY (idparent)
    REFERENCES unit_document_message(idmessage)
;

ALTER TABLE unit_document_message ADD CONSTRAINT FK__unit_docu__idpro__4FDCC2C0 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE unit_document_message ADD CONSTRAINT FK__unit_docu__idreq__51C50B32 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
;


-- 
-- TABLE: unit_preparation 
--

ALTER TABLE unit_preparation ADD CONSTRAINT FK__unit_prep__idfac__52B92F6B 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE unit_preparation ADD CONSTRAINT FK__unit_prep__idint__53AD53A4 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE unit_preparation ADD CONSTRAINT FK__unit_prep__idsli__54A177DD 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
;


-- 
-- TABLE: unit_requirement 
--

ALTER TABLE unit_requirement ADD CONSTRAINT FK__unit_requ__idbra__55959C16 
    FOREIGN KEY (idbranch)
    REFERENCES branch(idinternal)
;

ALTER TABLE unit_requirement ADD CONSTRAINT FK__unit_requ__idpro__5689C04F 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;

ALTER TABLE unit_requirement ADD CONSTRAINT FK__unit_requ__idreq__577DE488 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: unit_shipment_receipt 
--

ALTER TABLE unit_shipment_receipt ADD CONSTRAINT FK__unit_ship__idrec__587208C1 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;


-- 
-- TABLE: uom 
--

ALTER TABLE uom ADD CONSTRAINT FK__uom__iduomtyp__59662CFA 
    FOREIGN KEY (iduomtyp)
    REFERENCES uom_type(iduomtyp)
;


-- 
-- TABLE: uom_conversion 
--

ALTER TABLE uom_conversion ADD CONSTRAINT FK__uom_conve__iduom__5A5A5133 
    FOREIGN KEY (iduomto)
    REFERENCES uom(iduom)
;

ALTER TABLE uom_conversion ADD CONSTRAINT FK__uom_conve__iduom__5B4E756C 
    FOREIGN KEY (iduomfro)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: user_mediator 
--

ALTER TABLE user_mediator ADD CONSTRAINT FK__user_medi__idint__5D36BDDE 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE user_mediator ADD CONSTRAINT FK__user_medi__idper__5C4299A5 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
;


-- 
-- TABLE: user_mediator_role 
--

ALTER TABLE user_mediator_role ADD CONSTRAINT FK__user_medi__idpar__5E2AE217 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE user_mediator_role ADD CONSTRAINT FK__user_medi__idrol__60132A89 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE user_mediator_role ADD CONSTRAINT FK__user_medi__idusr__5F1F0650 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
;


-- 
-- TABLE: user_mediator_status 
--

ALTER TABLE user_mediator_status ADD CONSTRAINT FK__user_medi__idrea__62EF9734 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE user_mediator_status ADD CONSTRAINT FK__user_medi__idsta__61FB72FB 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE user_mediator_status ADD CONSTRAINT FK__user_medi__idusr__61074EC2 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
;


-- 
-- TABLE: vehicle 
--

ALTER TABLE vehicle ADD CONSTRAINT FK__vehicle__idcolor__64D7DFA6 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
;

ALTER TABLE vehicle ADD CONSTRAINT FK__vehicle__idinter__65CC03DF 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE vehicle ADD CONSTRAINT FK__vehicle__idprodu__63E3BB6D 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: vehicle_carrier 
--

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idpar__699C94C3 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idrea__66C02818 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idrel__67B44C51 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
;

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idveh__68A8708A 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
;


-- 
-- TABLE: vehicle_customer_request 
--

ALTER TABLE vehicle_customer_request ADD CONSTRAINT Refsalesman3040 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE vehicle_customer_request ADD CONSTRAINT FK__vehicle_c__idcus__6A90B8FC 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE vehicle_customer_request ADD CONSTRAINT FK__vehicle_c__idreq__6B84DD35 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
;

ALTER TABLE vehicle_customer_request ADD CONSTRAINT FK__vehicle_c__idsal__6C79016E 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
;


-- 
-- TABLE: vehicle_document_requirement 
--

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idbil__750E476F 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idint__713DB68B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idord__6F556E19 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idorg__77EAB41A 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idper__70499252 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idreq__6E6149E0 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idsal__7325FEFD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idshi__76F68FE1 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idsls__6D6D25A7 
    FOREIGN KEY (idslsreq)
    REFERENCES sales_unit_requirement(idreq)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idveh__7231DAC4 
    FOREIGN KEY (idvehregtyp)
    REFERENCES vehicle_registration_type(idvehregtyp)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idveh__741A2336 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idven__76026BA8 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: vehicle_identification 
--

ALTER TABLE vehicle_identification ADD CONSTRAINT FK__vehicle_i__idcus__78DED853 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE vehicle_identification ADD CONSTRAINT FK__vehicle_i__idveh__79D2FC8C 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: vehicle_purchase_order 
--

ALTER TABLE vehicle_purchase_order ADD CONSTRAINT FK__vehicle_p__idord__7AC720C5 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
;


-- 
-- TABLE: vehicle_sales_billing 
--

ALTER TABLE vehicle_sales_billing ADD CONSTRAINT FK__vehicle_s__idbil__7CAF6937 
    FOREIGN KEY (idbilling)
    REFERENCES billing_receipt(idbilling)
;

ALTER TABLE vehicle_sales_billing ADD CONSTRAINT FK__vehicle_s__idcus__7BBB44FE 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE vehicle_sales_billing ADD CONSTRAINT FK__vehicle_s__idsal__7DA38D70 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: vehicle_sales_order 
--

ALTER TABLE vehicle_sales_order ADD CONSTRAINT Refvendor3034 
    FOREIGN KEY (idbirojasa)
    REFERENCES vendor(idvendor)
;

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idlea__01741E54 
    FOREIGN KEY (idsales)
    REFERENCES leasing_company(idparrol)
;

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idord__7E97B1A9 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
;

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idsal__007FFA1B 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idsal__0268428D 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__iduni__7F8BD5E2 
    FOREIGN KEY (idunitreq)
    REFERENCES sales_unit_requirement(idreq)
;


-- 
-- TABLE: vehicle_service_history 
--

ALTER TABLE vehicle_service_history ADD CONSTRAINT FK__vehicle_s__idveh__035C66C6 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: vehicle_work_requirement 
--

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idcus__08211BE3 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
;

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idmec__0544AF38 
    FOREIGN KEY (idmechanic)
    REFERENCES mechanic(idparrol)
;

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idreq__04508AFF 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
;

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idveh__0638D371 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idveh__072CF7AA 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
;


-- 
-- TABLE: vendor 
--

ALTER TABLE vendor ADD CONSTRAINT FK__vendor__idparty__0AFD888E 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
;

ALTER TABLE vendor ADD CONSTRAINT FK__vendor__idrolety__0A096455 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE vendor ADD CONSTRAINT FK__vendor__idvndtyp__0915401C 
    FOREIGN KEY (idvndtyp)
    REFERENCES vendor_type(idvndtyp)
;


-- 
-- TABLE: vendor_order 
--

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idbil__0DD9F539 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
;

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idint__0BF1ACC7 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idord__0ECE1972 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idven__0CE5D100 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: vendor_product 
--

ALTER TABLE vendor_product ADD CONSTRAINT FK__vendor_pr__idint__0FC23DAB 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE vendor_product ADD CONSTRAINT FK__vendor_pr__idpro__10B661E4 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE vendor_product ADD CONSTRAINT FK__vendor_pr__idven__11AA861D 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: vendor_quotation 
--

ALTER TABLE vendor_quotation ADD CONSTRAINT Refquote1816 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE vendor_quotation ADD CONSTRAINT Refinternal1818 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE vendor_quotation ADD CONSTRAINT Refvendor1819 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: vendor_relationship 
--

ALTER TABLE vendor_relationship ADD CONSTRAINT FK__vendor_re__idint__129EAA56 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE vendor_relationship ADD CONSTRAINT FK__vendor_re__idpar__1486F2C8 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;

ALTER TABLE vendor_relationship ADD CONSTRAINT FK__vendor_re__idven__1392CE8F 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
;


-- 
-- TABLE: village 
--

ALTER TABLE village ADD CONSTRAINT Refgeo_boundary1811 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: we_service_type 
--

ALTER TABLE we_service_type ADD CONSTRAINT FK__we_servic__idpro__17635F73 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;

ALTER TABLE we_service_type ADD CONSTRAINT FK__we_servic__idwet__166F3B3A 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;


-- 
-- TABLE: we_type_good_standard 
--

ALTER TABLE we_type_good_standard ADD CONSTRAINT FK__we_type_g__idpro__194BA7E5 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE we_type_good_standard ADD CONSTRAINT FK__we_type_g__idwet__185783AC 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;


-- 
-- TABLE: work_effort 
--

ALTER TABLE work_effort ADD CONSTRAINT FK__work_effo__idfac__1A3FCC1E 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE work_effort ADD CONSTRAINT FK__work_effo__idwet__1B33F057 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;


-- 
-- TABLE: work_effort_payment 
--

ALTER TABLE work_effort_payment ADD CONSTRAINT FK__work_effo__payme__1D1C38C9 
    FOREIGN KEY (payments_id)
    REFERENCES payment_application(idpayapp)
;

ALTER TABLE work_effort_payment ADD CONSTRAINT FK__work_effo__work___1C281490 
    FOREIGN KEY (work_efforts_id)
    REFERENCES work_effort(idwe)
;


-- 
-- TABLE: work_effort_role 
--

ALTER TABLE work_effort_role ADD CONSTRAINT FK__work_effo__idpar__1F04813B 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE work_effort_role ADD CONSTRAINT FK__work_effo__idrol__1FF8A574 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE work_effort_role ADD CONSTRAINT FK__work_effor__idwe__1E105D02 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
;


-- 
-- TABLE: work_effort_status 
--

ALTER TABLE work_effort_status ADD CONSTRAINT FK__work_effo__idrea__22D5121F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE work_effort_status ADD CONSTRAINT FK__work_effo__idsta__21E0EDE6 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE work_effort_status ADD CONSTRAINT FK__work_effor__idwe__20ECC9AD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
;


-- 
-- TABLE: work_order 
--

ALTER TABLE work_order ADD CONSTRAINT FK__work_orde__idreq__23C93658 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_work_requirement(idreq)
;


-- 
-- TABLE: work_order_booking 
--

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idbok__25B17ECA 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
;

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idboo__24BD5A91 
    FOREIGN KEY (idbooslo)
    REFERENCES booking_slot(idbooslo)
;

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idcus__288DEB75 
    FOREIGN KEY (idcustomer)
    REFERENCES personal_customer(idcustomer)
;

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__ideve__26A5A303 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
;

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idint__2799C73C 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
;

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idveh__29820FAE 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: work_order_booking_role 
--

ALTER TABLE work_order_booking_role ADD CONSTRAINT FK__work_orde__idboo__2A7633E7 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
;

ALTER TABLE work_order_booking_role ADD CONSTRAINT FK__work_orde__idpar__2B6A5820 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE work_order_booking_role ADD CONSTRAINT FK__work_orde__idrol__2C5E7C59 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: work_order_booking_status 
--

ALTER TABLE work_order_booking_status ADD CONSTRAINT FK__work_orde__idboo__2D52A092 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
;

ALTER TABLE work_order_booking_status ADD CONSTRAINT FK__work_orde__idrea__2F3AE904 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE work_order_booking_status ADD CONSTRAINT FK__work_orde__idsta__2E46C4CB 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: work_product_req 
--

ALTER TABLE work_product_req ADD CONSTRAINT FK__work_prod__idreq__302F0D3D 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: work_requirement 
--

ALTER TABLE work_requirement ADD CONSTRAINT FK__work_requ__idreq__31233176 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: work_requirement_services 
--

ALTER TABLE work_requirement_services ADD CONSTRAINT FK__work_requ__idreq__321755AF 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
;

ALTER TABLE work_requirement_services ADD CONSTRAINT FK__work_requi__idwe__330B79E8 
    FOREIGN KEY (idwe)
    REFERENCES work_service_requirement(idwe)
;


-- 
-- TABLE: work_service_requirement 
--

ALTER TABLE work_service_requirement ADD CONSTRAINT FK__work_servi__idwe__33FF9E21 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
;


