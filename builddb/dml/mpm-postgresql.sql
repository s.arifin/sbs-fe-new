--
-- ER/Studio Data Architect 10.0 SQL Code Generation
-- Project :      mpm-model.DM1
--
-- Date Created : Friday, December 01, 2017 02:54:02
-- Target DBMS : PostgreSQL 9.x
--

-- 
-- TABLE: agreement 
--

CREATE TABLE agreement(
    idagreement    bit(16)        NOT NULL,
    idagrtyp       int4,
    description    varchar(50),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idagreement)
)
;



-- 
-- TABLE: agreement_item 
--

CREATE TABLE agreement_item(
    idagrite       bit(16)    NOT NULL,
    idagreement    bit(16),
    PRIMARY KEY (idagrite)
)
;



-- 
-- TABLE: agreement_motor_item 
--

CREATE TABLE agreement_motor_item(
    idagrite     bit(16)    NOT NULL,
    idvehicle    bit(16),
    PRIMARY KEY (idagrite)
)
;



-- 
-- TABLE: agreement_role 
--

CREATE TABLE agreement_role(
    idrole         bit(16)        NOT NULL,
    idagreement    bit(16),
    idroletype     int4,
    idparty        bit(16),
    username       varchar(30),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: agreement_status 
--

CREATE TABLE agreement_status(
    idstatus        bit(16)         NOT NULL,
    idagreement     bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: agreement_type 
--

CREATE TABLE agreement_type(
    idagrtyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idagrtyp)
)
;



-- 
-- TABLE: approval 
--

CREATE TABLE approval(
    idapproval      bit(16)      NOT NULL,
    idapptyp        int4,
    idstatustype    int4,
    idreq           bit(16),
    idordite        bit(16),
    dtfrom          timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp    DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idapproval)
)
;



-- 
-- TABLE: approval_type 
--

CREATE TABLE approval_type(
    idapptyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idapptyp)
)
;



-- 
-- TABLE: association_type 
--

CREATE TABLE association_type(
    idasstyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idasstyp)
)
;



-- 
-- TABLE: base_calendar 
--

CREATE TABLE base_calendar(
    idcalendar     int4           NOT NULL,
    idparent       int4,
    idcaltyp       int4,
    description    varchar(50),
    dtkey          date,
    workday        varchar(5),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idcalendar)
)
;



-- 
-- TABLE: bill_to 
--

CREATE TABLE bill_to(
    idparrol    bit(16)        NOT NULL,
    idbillto    varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: billing 
--

CREATE TABLE billing(
    idbilling        bit(16)        NOT NULL,
    idbiltyp         int4,
    billingnumber    varchar(30),
    dtcreate         timestamp      DEFAULT CURRENT_TIMESTAMP,
    printcounter     int4,
    PRIMARY KEY (idbilling)
)
;



-- 
-- TABLE: billing_account 
--

CREATE TABLE billing_account(
    idbilacc       int4           NOT NULL,
    description    varchar(50),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idbilacc)
)
;



-- 
-- TABLE: billing_disbursement 
--

CREATE TABLE billing_disbursement(
    idbilling     bit(16)    NOT NULL,
    idbilacc      int4,
    idinternal    bit(16),
    idbilfro      bit(16),
    idvendor      bit(16),
    PRIMARY KEY (idbilling)
)
;



-- 
-- TABLE: billing_item 
--

CREATE TABLE billing_item(
    idbilite           bit(16)           NOT NULL,
    idbilling          bit(16),
    idinvite           bit(16),
    idproduct          varchar(30),
    idwetyp            int4,
    idbilitetyp        int4,
    idfeature          int4,
    itemdescription    varchar(50),
    qty                decimal(14, 4),
    baseprice          decimal(18, 4),
    unitprice          decimal(18, 4),
    discount           decimal(18, 4),
    taxamount          decimal(18, 4),
    totalamount        decimal(18, 4),
    PRIMARY KEY (idbilite)
)
;



-- 
-- TABLE: billing_item_type 
--

CREATE TABLE billing_item_type(
    idbilitetyp    int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idbilitetyp)
)
;



-- 
-- TABLE: billing_payment 
--

CREATE TABLE billing_payment(
    billings_id    bit(16)    NOT NULL,
    payments_id    bit(16)    NOT NULL,
    PRIMARY KEY (billings_id, payments_id)
)
;



-- 
-- TABLE: billing_receipt 
--

CREATE TABLE billing_receipt(
    idbilling     bit(16)    NOT NULL,
    idbilacc      int4,
    idinternal    bit(16),
    idbilto       bit(16),
    PRIMARY KEY (idbilling)
)
;



-- 
-- TABLE: billing_role 
--

CREATE TABLE billing_role(
    idrole        bit(16)        NOT NULL,
    idbilling     bit(16),
    idroletype    int4,
    idparty       bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: billing_status 
--

CREATE TABLE billing_status(
    idstatus        bit(16)         NOT NULL,
    idbilling       bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: billing_type 
--

CREATE TABLE billing_type(
    idbiltyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idbiltyp)
)
;



-- 
-- TABLE: booking_slot 
--

CREATE TABLE booking_slot(
    idbooslo       bit(16)        NOT NULL,
    idcalendar     int4,
    idbooslostd    bit(16),
    slotnumber     varchar(30),
    PRIMARY KEY (idbooslo)
)
;



-- 
-- TABLE: booking_slot_standard 
--

CREATE TABLE booking_slot_standard(
    idbooslostd    bit(16)        NOT NULL,
    idinternal     bit(16),
    idboktyp       int4,
    description    varchar(50),
    capacity       int4,
    starthour      int4,
    startminute    int4,
    endhour        int4,
    endminute      int4,
    PRIMARY KEY (idbooslostd)
)
;



-- 
-- TABLE: booking_type 
--

CREATE TABLE booking_type(
    idboktyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idboktyp)
)
;



-- 
-- TABLE: branch 
--

CREATE TABLE branch(
    idparrol            bit(16)    NOT NULL,
    idbranchcategory    int4,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: broker_type 
--

CREATE TABLE broker_type(
    idbrotyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idbrotyp)
)
;



-- 
-- TABLE: calendar_type 
--

CREATE TABLE calendar_type(
    idcaltyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idcaltyp)
)
;



-- 
-- TABLE: category 
--

CREATE TABLE category(
    idcategory     int4           NOT NULL,
    idcattyp       int4,
    description    varchar(50),
    PRIMARY KEY (idcategory)
)
;



-- 
-- TABLE: category_party 
--

CREATE TABLE category_party(
    idcategory     int4           NOT NULL,
    idcattyp       int4,
    description    varchar(50),
    refkey         varchar(30),
    PRIMARY KEY (idcategory)
)
;



-- 
-- TABLE: category_product 
--

CREATE TABLE category_product(
    idcategory     int4           NOT NULL,
    idcattyp       int4,
    description    varchar(50),
    refkey         varchar(30),
    PRIMARY KEY (idcategory)
)
;



-- 
-- TABLE: category_type 
--

CREATE TABLE category_type(
    idcattyp       int4           NOT NULL,
    idparent       int4,
    idrultyp       int4,
    description    varchar(50),
    PRIMARY KEY (idcattyp)
)
;



-- 
-- TABLE: city 
--

CREATE TABLE city(
    idgeobou    varchar(36)    NOT NULL,
    PRIMARY KEY (idgeobou)
)
;



-- 
-- TABLE: communication_event 
--

CREATE TABLE communication_event(
    idcomevt    bit(16)         NOT NULL,
    idparrel    bit(16),
    idevetyp    int4,
    note        varchar(300),
    dtfrom      timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru      timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idcomevt)
)
;



-- 
-- TABLE: communication_event_cdb 
--

CREATE TABLE communication_event_cdb(
    idcomevt           bit(16)         NOT NULL,
    idcustomer         bit(16),
    hobby              varchar(300),
    homestatus         varchar(300),
    exponemonth        varchar(300),
    job                varchar(300),
    lasteducation      varchar(300),
    owncellphone       varchar(300),
    hondarefference    varchar(300),
    ownvehiclebrand    varchar(300),
    ownvehicletype     varchar(300),
    buyfor             varchar(300),
    vehicleuser        varchar(300),
    PRIMARY KEY (idcomevt)
)
;



-- 
-- TABLE: communication_event_prospect 
--

CREATE TABLE communication_event_prospect(
    idcomevt        bit(16)           NOT NULL,
    idprospect      bit(16),
    idsaletype      int4,
    idproduct       varchar(30),
    idcolor         int4,
    interest        varchar(5),
    qty             decimal(14, 4),
    purchaseplan    varchar(30),
    connect         varchar(5),
    PRIMARY KEY (idcomevt)
)
;



-- 
-- TABLE: communication_event_purpose 
--

CREATE TABLE communication_event_purpose(
    idcomevt         bit(16)    NOT NULL,
    idpurposetype    int4       NOT NULL,
    PRIMARY KEY (idcomevt, idpurposetype)
)
;



-- 
-- TABLE: communication_event_role 
--

CREATE TABLE communication_event_role(
    idcomevt      bit(16),
    idrole        bit(16)        NOT NULL,
    idroletype    int4,
    idparty       bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: communication_event_status 
--

CREATE TABLE communication_event_status(
    idstatus        bit(16)         NOT NULL,
    idcomevt        bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: contact_mechanism 
--

CREATE TABLE contact_mechanism(
    idcontact        bit(16)        NOT NULL,
    idcontacttype    int4,
    description      varchar(50),
    PRIMARY KEY (idcontact)
)
;



-- 
-- TABLE: contact_mechanism_purpose 
--

CREATE TABLE contact_mechanism_purpose(
    idconmecpur      bit(16)      NOT NULL,
    idparty          bit(16),
    idcontact        bit(16),
    idpurposetype    int4,
    dtfrom           timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtthru           timestamp    DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idconmecpur)
)
;



-- 
-- TABLE: contact_purpose 
--

CREATE TABLE contact_purpose(
    idcontact        bit(16)    NOT NULL,
    idpurposetype    int4       NOT NULL,
    PRIMARY KEY (idcontact, idpurposetype)
)
;



-- 
-- TABLE: container 
--

CREATE TABLE container(
    idcontainer    bit(16)        NOT NULL,
    idfacility     bit(16),
    idcontyp       int4,
    code           varchar(30),
    description    varchar(50),
    PRIMARY KEY (idcontainer)
)
;



-- 
-- TABLE: container_type 
--

CREATE TABLE container_type(
    idcontyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idcontyp)
)
;



-- 
-- TABLE: customer 
--

CREATE TABLE customer(
    idparrol            bit(16)        NOT NULL,
    idmarketcategory    int4,
    idsegmen            int4,
    idcustomer          varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: customer_bill_to 
--

CREATE TABLE customer_bill_to(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: customer_order 
--

CREATE TABLE customer_order(
    idorder       bit(16)    NOT NULL,
    idinternal    bit(16),
    idcustomer    bit(16),
    idbillto      bit(16),
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: customer_quotation 
--

CREATE TABLE customer_quotation(
    idquote       bit(16)    NOT NULL,
    idcustomer    bit(16),
    idinternal    bit(16),
    PRIMARY KEY (idquote)
)
;



-- 
-- TABLE: customer_relationship 
--

CREATE TABLE customer_relationship(
    idparrel      bit(16)    NOT NULL,
    idinternal    bit(16),
    idcustomer    bit(16),
    PRIMARY KEY (idparrel)
)
;



-- 
-- TABLE: customer_ship_to 
--

CREATE TABLE customer_ship_to(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: databasechangelog 
--

CREATE TABLE databasechangelog(
    "ID"             varchar(765)    NOT NULL,
    "AUTHOR"         varchar(765)    NOT NULL,
    "FILENAME"       varchar(765)    NOT NULL,
    "DATEEXECUTED"   timestamp       NOT NULL,
    "ORDEREXECUTED"  int4            NOT NULL,
    "EXECTYPE"       varchar(30)     NOT NULL,
    "MD5SUM"         varchar(105),
    "DESCRIPTION"    varchar(765),
    "COMMENTS"       varchar(765),
    "TAG"            varchar(765),
    "LIQUIBASE"      varchar(60),
    "CONTEXTS"       varchar(765),
    "LABELS"         varchar(765),
    "DEPLOYMENT_ID"  varchar(30)
)
;



-- 
-- TABLE: databasechangeloglock 
--

CREATE TABLE databasechangeloglock(
    "ID"           int4            NOT NULL,
    "LOCKED"       char(1)         NOT NULL,
    "LOCKGRANTED"  timestamp,
    "LOCKEDBY"     varchar(765),
    PRIMARY KEY ("ID")
)
;



-- 
-- TABLE: dealer_claim_type 
--

CREATE TABLE dealer_claim_type(
    idclaimtype    int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idclaimtype)
)
;



-- 
-- TABLE: dealer_reminder_type 
--

CREATE TABLE dealer_reminder_type(
    idremindertype    int4            NOT NULL,
    description       varchar(50),
    messages          varchar(600),
    PRIMARY KEY (idremindertype)
)
;



-- 
-- TABLE: deliverable_type 
--

CREATE TABLE deliverable_type(
    iddeltype      int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (iddeltype)
)
;



-- 
-- TABLE: disbursement 
--

CREATE TABLE disbursement(
    idpayment     bit(16)    NOT NULL,
    idinternal    bit(16),
    idpaito       bit(16),
    PRIMARY KEY (idpayment)
)
;



-- 
-- TABLE: district 
--

CREATE TABLE district(
    idgeobou    varchar(36)    NOT NULL,
    PRIMARY KEY (idgeobou)
)
;



-- 
-- TABLE: document_type 
--

CREATE TABLE document_type(
    iddoctype      int4           NOT NULL,
    idparent       int4,
    description    varchar(50),
    PRIMARY KEY (iddoctype)
)
;



-- 
-- TABLE: documents 
--

CREATE TABLE documents(
    iddocument    bit(16)        NOT NULL,
    iddoctype     int4,
    note          varchar(50),
    dtcreate      timestamp      DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (iddocument)
)
;



-- 
-- TABLE: driver 
--

CREATE TABLE driver(
    idparrol    bit(16)        NOT NULL,
    iddriver    varchar(30),
    external    varchar(5),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: electronic_address 
--

CREATE TABLE electronic_address(
    idcontact    bit(16)        NOT NULL,
    address      varchar(60),
    PRIMARY KEY (idcontact)
)
;



-- 
-- TABLE: employee 
--

CREATE TABLE employee(
    idparrol          bit(16)        NOT NULL,
    "employeeNumber"  varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: estimation 
--

CREATE TABLE estimation(
    idreq    bit(16)    NOT NULL,
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: event_type 
--

CREATE TABLE event_type(
    idevetyp       int4           NOT NULL,
    idprneve       int4,
    description    varchar(50),
    PRIMARY KEY (idevetyp)
)
;



-- 
-- TABLE: facility 
--

CREATE TABLE facility(
    idfacility        bit(16)        NOT NULL,
    idfacilitytype    int4,
    idpartof          bit(16),
    idfa              int4,
    code              varchar(30),
    description       varchar(50),
    PRIMARY KEY (idfacility)
)
;



-- 
-- TABLE: facility_contact 
--

CREATE TABLE facility_contact(
    idfacility    bit(16)    NOT NULL,
    idcontact     bit(16)    NOT NULL,
    PRIMARY KEY (idfacility, idcontact)
)
;



-- 
-- TABLE: facility_contact_mechanism 
--

CREATE TABLE facility_contact_mechanism(
    idconmecpur      bit(16)      NOT NULL,
    idfacility       bit(16),
    idcontact        bit(16),
    idpurposetype    int4,
    dtfrom           timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtthru           timestamp    DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idconmecpur)
)
;



-- 
-- TABLE: facility_status 
--

CREATE TABLE facility_status(
    idstatus        bit(16)         NOT NULL,
    idfacility      bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: facility_type 
--

CREATE TABLE facility_type(
    idfacilitytype    int4           NOT NULL,
    description       varchar(50),
    PRIMARY KEY (idfacilitytype)
)
;



-- 
-- TABLE: feature 
--

CREATE TABLE feature(
    idfeature      int4           NOT NULL,
    idfeatyp       int4,
    description    varchar(50),
    refkey         varchar(30),
    PRIMARY KEY (idfeature)
)
;



-- 
-- TABLE: feature_type 
--

CREATE TABLE feature_type(
    idfeatyp       int4           NOT NULL,
    idrultyp       int4,
    description    varchar(50),
    PRIMARY KEY (idfeatyp)
)
;



-- 
-- TABLE: fixed_asset 
--

CREATE TABLE fixed_asset(
    idfa           int4           NOT NULL,
    idfatype       int4,
    iduom          varchar(30),
    name           varchar(30),
    dtacquired     timestamp      DEFAULT CURRENT_TIMESTAMP,
    description    varchar(50),
    PRIMARY KEY (idfa)
)
;



-- 
-- TABLE: fixed_asset_type 
--

CREATE TABLE fixed_asset_type(
    idfatype       int4           NOT NULL,
    idparent       int4,
    description    varchar(50),
    PRIMARY KEY (idfatype)
)
;



-- 
-- TABLE: geo_boundary 
--

CREATE TABLE geo_boundary(
    idgeobou        varchar(36)    NOT NULL,
    idparent        varchar(36),
    idgeoboutype    int4,
    geocode         varchar(30),
    description     varchar(50),
    PRIMARY KEY (idgeobou)
)
;



-- 
-- TABLE: geo_boundary_type 
--

CREATE TABLE geo_boundary_type(
    idgeoboutype    int4           NOT NULL,
    description     varchar(50),
    PRIMARY KEY (idgeoboutype)
)
;



-- 
-- TABLE: good 
--

CREATE TABLE good(
    iduom        varchar(30),
    idproduct    varchar(30)    NOT NULL,
    PRIMARY KEY (idproduct)
)
;



-- 
-- TABLE: good_container 
--

CREATE TABLE good_container(
    idgoocon       bit(16)        NOT NULL,
    idproduct      varchar(30),
    idinternal     bit(16),
    idcontainer    bit(16),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idgoocon)
)
;



-- 
-- TABLE: intern 
--

CREATE TABLE intern(
    idintern       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idintern)
)
;



-- 
-- TABLE: internal 
--

CREATE TABLE internal(
    idparrol      bit(16)        NOT NULL,
    idparent      bit(16),
    idinternal    varchar(30),
    selfsender    varchar(5),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: internal_bill_to 
--

CREATE TABLE internal_bill_to(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: internal_order 
--

CREATE TABLE internal_order(
    idorder           bit(16)    NOT NULL,
    idinternalto      bit(16),
    idinternalfrom    bit(16),
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: internal_ship_to 
--

CREATE TABLE internal_ship_to(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: inventory_adjustment 
--

CREATE TABLE inventory_adjustment(
    idinvadj    bit(16)           NOT NULL,
    idinvite    bit(16),
    qty         decimal(14, 4),
    PRIMARY KEY (idinvadj)
)
;



-- 
-- TABLE: inventory_item 
--

CREATE TABLE inventory_item(
    idinvite        bit(16)           NOT NULL,
    idowner         bit(16),
    idproduct       varchar(30),
    idfacility      bit(16),
    idcontainer     bit(16),
    idreceipt       bit(16),
    idfeature       int4,
    idfa            int4,
    dtcreated       timestamp         DEFAULT CURRENT_TIMESTAMP,
    qty             decimal(14, 4),
    qtybooking      decimal(14, 4),
    idframe         varchar(50),
    idmachine       varchar(50),
    yearassembly    int4,
    PRIMARY KEY (idinvite)
)
;



-- 
-- TABLE: inventory_item_status 
--

CREATE TABLE inventory_item_status(
    idstatus        bit(16)         NOT NULL,
    idinvite        bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: inventory_movement 
--

CREATE TABLE inventory_movement(
    idslip      bit(16)      NOT NULL,
    dtcreate    timestamp    DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idslip)
)
;



-- 
-- TABLE: inventory_movement_status 
--

CREATE TABLE inventory_movement_status(
    idstatus        bit(16)         NOT NULL,
    idslip          bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: item_issuance 
--

CREATE TABLE item_issuance(
    iditeiss    bit(16)           NOT NULL,
    idshiite    bit(16),
    idinvite    bit(16),
    idslip      bit(16),
    qty         decimal(14, 4),
    PRIMARY KEY (iditeiss)
)
;



-- 
-- TABLE: jhi_authority 
--

CREATE TABLE jhi_authority(
    name    varchar(30)    NOT NULL,
    PRIMARY KEY (name)
)
;



-- 
-- TABLE: jhi_persistent_audit_event 
--

CREATE TABLE jhi_persistent_audit_event(
    event_id      int8            NOT NULL,
    principal     varchar(150)    NOT NULL,
    event_date    timestamp       DEFAULT CURRENT_TIMESTAMP,
    event_type    varchar(765),
    PRIMARY KEY (event_id)
)
;



-- 
-- TABLE: jhi_persistent_audit_evt_data 
--

CREATE TABLE jhi_persistent_audit_evt_data(
    event_id    int8            NOT NULL,
    name        varchar(450)    NOT NULL,
    value       varchar(765),
    PRIMARY KEY (event_id, name)
)
;



-- 
-- TABLE: jhi_user 
--

CREATE TABLE jhi_user(
    id                    int8            NOT NULL,
    login                 varchar(150)    NOT NULL,
    password_hash         varchar(180),
    first_name            varchar(150),
    last_name             varchar(150),
    email                 varchar(300),
    image_url             varchar(768),
    activated             char(1)         NOT NULL,
    lang_key              varchar(15),
    activation_key        varchar(60),
    reset_key             varchar(60),
    created_by            varchar(150)    NOT NULL,
    created_date          timestamp       DEFAULT CURRENT_TIMESTAMP,
    reset_date            timestamp       DEFAULT CURRENT_TIMESTAMP,
    last_modified_by      varchar(150),
    last_modified_date    timestamp       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
)
;



-- 
-- TABLE: jhi_user_authority 
--

CREATE TABLE jhi_user_authority(
    user_id           int8           NOT NULL,
    authority_name    varchar(30)    NOT NULL,
    PRIMARY KEY (user_id, authority_name)
)
;



-- 
-- TABLE: leasing_company 
--

CREATE TABLE leasing_company(
    idparrol    bit(16)        NOT NULL,
    idleacom    varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: leasing_tenor_provide 
--

CREATE TABLE leasing_tenor_provide(
    idlsgpro       bit(16)           NOT NULL,
    idleacom       bit(16),
    idproduct      varchar(30),
    tenor          int4,
    installment    decimal(18, 4),
    downpayment    decimal(18, 4),
    dtfrom         timestamp         DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp         DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idlsgpro)
)
;



-- 
-- TABLE: master_numbering 
--

CREATE TABLE master_numbering(
    idnumbering    bit(16)        NOT NULL,
    idtag          varchar(50),
    idvalue        varchar(50),
    nextvalue      int4,
    PRIMARY KEY (idnumbering)
)
;



-- 
-- TABLE: mechanic 
--

CREATE TABLE mechanic(
    idparrol      bit(16)        NOT NULL,
    idmechanic    varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: motor 
--

CREATE TABLE motor(
    idproduct    varchar(30)    NOT NULL,
    PRIMARY KEY (idproduct)
)
;



-- 
-- TABLE: motor_due_reminder 
--

CREATE TABLE motor_due_reminder(
    idreminder    int4           NOT NULL,
    idmotor       varchar(30),
    nservice1     int4,
    nservice2     int4,
    nservice3     int4,
    nservice4     int4,
    nservice5     int4,
    km            int4,
    ndays         int4,
    PRIMARY KEY (idreminder)
)
;



-- 
-- TABLE: moving_slip 
--

CREATE TABLE moving_slip(
    idslip             bit(16)           NOT NULL,
    idinviteto         bit(16),
    idcontainerfrom    bit(16),
    idcontainerto      bit(16),
    idproduct          varchar(30),
    idfacilityto       bit(16),
    idfacilityfrom     bit(16),
    qty                decimal(14, 4),
    PRIMARY KEY (idslip)
)
;



-- 
-- TABLE: oauth_access_token 
--

CREATE TABLE oauth_access_token(
    token_id             varchar(765),
    token                bytea,
    authentication_id    varchar(765)    NOT NULL,
    user_name            varchar(150),
    client_id            varchar(765),
    authentication       bytea,
    refresh_token        varchar(765),
    PRIMARY KEY (authentication_id)
)
;



-- 
-- TABLE: oauth_approvals 
--

CREATE TABLE oauth_approvals(
    "userId"          varchar(765),
    "clientId"        varchar(765),
    scope             varchar(765),
    status            varchar(765),
    "expiresAt"       timestamp       DEFAULT CURRENT_TIMESTAMP,
    "lastModifiedAt"  timestamp       DEFAULT CURRENT_TIMESTAMP
)
;



-- 
-- TABLE: oauth_client_details 
--

CREATE TABLE oauth_client_details(
    client_id                  varchar(765)    NOT NULL,
    resource_ids               varchar(765),
    client_secret              varchar(765),
    scope                      varchar(765),
    authorized_grant_types     varchar(765),
    web_server_redirect_uri    varchar(765),
    authorities                varchar(765),
    access_token_validity      int4,
    refresh_token_validity     int4,
    additional_information     bytea,
    autoapprove                bytea,
    PRIMARY KEY (client_id)
)
;



-- 
-- TABLE: oauth_client_token 
--

CREATE TABLE oauth_client_token(
    token_id             varchar(765),
    token                bytea,
    authentication_id    varchar(765),
    user_name            varchar(150),
    client_id            varchar(765)
)
;



-- 
-- TABLE: oauth_code 
--

CREATE TABLE oauth_code(
    code              varchar(765),
    authentication    bytea
)
;



-- 
-- TABLE: oauth_refresh_token 
--

CREATE TABLE oauth_refresh_token(
    token_id          varchar(765),
    token             bytea,
    authentication    bytea
)
;



-- 
-- TABLE: order_billing_item 
--

CREATE TABLE order_billing_item(
    idbilite    bit(16)    NOT NULL,
    idordite    bit(16)    NOT NULL,
    PRIMARY KEY (idbilite, idordite)
)
;



-- 
-- TABLE: order_item 
--

CREATE TABLE order_item(
    idordite           bit(16)           NOT NULL,
    idparordite        bit(16),
    idorder            bit(16),
    idproduct          varchar(30),
    idshipto           bit(16),
    idfeature          int4,
    itemdescription    varchar(50),
    qty                decimal(14, 4),
    unitprice          decimal(18, 4),
    discount           decimal(18, 4),
    taxamount          decimal(18, 4),
    totalamount        decimal(18, 4),
    PRIMARY KEY (idordite)
)
;



-- 
-- TABLE: order_item_requirement 
--

CREATE TABLE order_item_requirement(
    requirements_id    bit(16)    NOT NULL,
    order_items_id     bit(16)    NOT NULL,
    PRIMARY KEY (requirements_id, order_items_id)
)
;



-- 
-- TABLE: order_type 
--

CREATE TABLE order_type(
    idordtyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idordtyp)
)
;



-- 
-- TABLE: orders 
--

CREATE TABLE orders(
    idorder        bit(16)         NOT NULL,
    idordtyp       int4,
    ordernumber    varchar(765),
    dtentry        timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtorder        date,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: orders_payment 
--

CREATE TABLE orders_payment(
    payments_id    bit(16)    NOT NULL,
    orders_id      bit(16)    NOT NULL,
    PRIMARY KEY (payments_id, orders_id)
)
;



-- 
-- TABLE: orders_role 
--

CREATE TABLE orders_role(
    idrole        bit(16)        NOT NULL,
    idorder       bit(16),
    idroletype    int4,
    idparty       bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: orders_status 
--

CREATE TABLE orders_status(
    idstatus        bit(16)         NOT NULL,
    idorder         bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: organization 
--

CREATE TABLE organization(
    idparty         bit(16)        NOT NULL,
    idpic           bit(16),
    idposaddtdp     bit(16),
    name            varchar(30),
    numbertdp       varchar(30),
    dtestablised    date,
    PRIMARY KEY (idparty)
)
;



-- 
-- TABLE: organization_customer 
--

CREATE TABLE organization_customer(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: organization_prospect 
--

CREATE TABLE organization_prospect(
    idprospect     bit(16)    NOT NULL,
    idparty        bit(16),
    idpic          bit(16),
    idposaddtdp    bit(16),
    PRIMARY KEY (idprospect)
)
;



-- 
-- TABLE: organization_suspect 
--

CREATE TABLE organization_suspect(
    idsuspect    bit(16)    NOT NULL,
    idparty      bit(16),
    PRIMARY KEY (idsuspect)
)
;



-- 
-- TABLE: package_receipt 
--

CREATE TABLE package_receipt(
    idpackage         bit(16)        NOT NULL,
    idshifro          bit(16),
    documentnumber    varchar(50),
    PRIMARY KEY (idpackage)
)
;



-- 
-- TABLE: packaging_content 
--

CREATE TABLE packaging_content(
    idpaccon     bit(16)           NOT NULL,
    idpackage    bit(16),
    idshiite     bit(16),
    qty          decimal(14, 4),
    PRIMARY KEY (idpaccon)
)
;



-- 
-- TABLE: parent_organization 
--

CREATE TABLE parent_organization(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: part_sales_order 
--

CREATE TABLE part_sales_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: party 
--

CREATE TABLE party(
    idparty    bit(16)    NOT NULL,
    idtype     int4,
    PRIMARY KEY (idparty)
)
;



-- 
-- TABLE: party_category 
--

CREATE TABLE party_category(
    categories_id    int4       NOT NULL,
    parties_id       bit(16)    NOT NULL,
    PRIMARY KEY (categories_id, parties_id)
)
;



-- 
-- TABLE: party_contact 
--

CREATE TABLE party_contact(
    idparty      bit(16)    NOT NULL,
    idcontact    bit(16)    NOT NULL,
    PRIMARY KEY (idparty, idcontact)
)
;



-- 
-- TABLE: party_document 
--

CREATE TABLE party_document(
    iddocument     bit(16)        NOT NULL,
    idparty        bit(16),
    contenttype    varchar(30),
    content        bytea,
    PRIMARY KEY (iddocument)
)
;



-- 
-- TABLE: party_facility 
--

CREATE TABLE party_facility(
    facilities_id    bit(16)    NOT NULL,
    parties_id       bit(16)    NOT NULL,
    PRIMARY KEY (facilities_id, parties_id)
)
;



-- 
-- TABLE: party_relationship 
--

CREATE TABLE party_relationship(
    idparrel        bit(16)      NOT NULL,
    idstatustype    int4,
    idreltyp        int4,
    idparrolfro     bit(16),
    idparrolto      bit(16),
    dtfrom          timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp    DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idparrel)
)
;



-- 
-- TABLE: party_role 
--

CREATE TABLE party_role(
    idparrol      bit(16)      NOT NULL,
    idroletype    int4,
    idparty       bit(16),
    dtregister    timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtfrom        timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp    DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: party_role_status 
--

CREATE TABLE party_role_status(
    idstatus        bit(16)         NOT NULL,
    idparrol        bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: payment 
--

CREATE TABLE payment(
    idpayment        bit(16)           NOT NULL,
    idpaytyp         int4,
    idpaymettyp      int4,
    paymentnumber    varchar(30),
    dtcreate         timestamp         DEFAULT CURRENT_TIMESTAMP,
    amount           decimal(18, 4),
    PRIMARY KEY (idpayment)
)
;



-- 
-- TABLE: payment_application 
--

CREATE TABLE payment_application(
    idpayapp         bit(16)           NOT NULL,
    idpayment        bit(16),
    idbilacc         int4,
    idbilling        bit(16),
    amountapplied    decimal(18, 4),
    PRIMARY KEY (idpayapp)
)
;



-- 
-- TABLE: payment_method_type 
--

CREATE TABLE payment_method_type(
    idpaymettyp    int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idpaymettyp)
)
;



-- 
-- TABLE: payment_role 
--

CREATE TABLE payment_role(
    idrole        bit(16)        NOT NULL,
    idpayment     bit(16),
    idroletype    int4,
    idparty       bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: payment_status 
--

CREATE TABLE payment_status(
    idstatus        bit(16)         NOT NULL,
    idpayment       bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: payment_type 
--

CREATE TABLE payment_type(
    idpaytyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idpaytyp)
)
;



-- 
-- TABLE: person 
--

CREATE TABLE person(
    idparty             bit(16)        NOT NULL,
    fname               varchar(30),
    lname               varchar(30),
    pob                 varchar(30),
    dtob                date,
    bloodtype           varchar(10),
    gender              varchar(1),
    personalidnumber    varchar(30),
    familyidnumber      varchar(30),
    taxnumber           varchar(30),
    idreligiontype      int4,
    idworktype          int4,
    cellphone1          varchar(50),
    cellphone2          varchar(50),
    privatemail         varchar(50),
    phone               varchar(50),
    username            varchar(30),
    PRIMARY KEY (idparty)
)
;



-- 
-- TABLE: person_prospect 
--

CREATE TABLE person_prospect(
    idprospect    bit(16)    NOT NULL,
    idparty       bit(16),
    PRIMARY KEY (idprospect)
)
;



-- 
-- TABLE: person_suspect 
--

CREATE TABLE person_suspect(
    idsuspect    bit(16)    NOT NULL,
    idparty      bit(16),
    PRIMARY KEY (idsuspect)
)
;



-- 
-- TABLE: personal_customer 
--

CREATE TABLE personal_customer(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: picking_slip 
--

CREATE TABLE picking_slip(
    idslip      bit(16)           NOT NULL,
    idshipto    bit(16),
    idinvite    bit(16),
    idordite    bit(16),
    qty         decimal(14, 4),
    acc1        varchar(5),
    acc2        varchar(5),
    acc3        varchar(5),
    acc4        varchar(5),
    acc5        varchar(5),
    acc6        varchar(5),
    acc7        varchar(5),
    promat1     varchar(5),
    promat2     varchar(5),
    PRIMARY KEY (idslip)
)
;



-- 
-- TABLE: position_authority 
--

CREATE TABLE position_authority(
    name        varchar(30)    NOT NULL,
    idpostyp    int4           NOT NULL,
    PRIMARY KEY (name, idpostyp)
)
;



-- 
-- TABLE: position_fullfillment 
--

CREATE TABLE position_fullfillment(
    idposfulfil    int4           NOT NULL,
    idposition     bit(16),
    idperson       bit(16),
    username       varchar(30),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idposfulfil)
)
;



-- 
-- TABLE: position_reporting_structure 
--

CREATE TABLE position_reporting_structure(
    idposrepstru    int4         NOT NULL,
    idposfro        bit(16),
    idposto         bit(16),
    dtfrom          timestamp    DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp    DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idposrepstru)
)
;



-- 
-- TABLE: position_type 
--

CREATE TABLE position_type(
    idpostyp       int4           NOT NULL,
    description    varchar(50),
    title          varchar(30),
    PRIMARY KEY (idpostyp)
)
;



-- 
-- TABLE: positions 
--

CREATE TABLE positions(
    idposition        bit(16)        NOT NULL,
    idpostyp          int4,
    idinternal        bit(16),
    idorganization    bit(16),
    seqnum            int4,
    description       varchar(50),
    PRIMARY KEY (idposition)
)
;



-- 
-- TABLE: positions_status 
--

CREATE TABLE positions_status(
    idstatus        bit(16)         NOT NULL,
    idposition      bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: postal_address 
--

CREATE TABLE postal_address(
    idcontact    bit(16)        NOT NULL,
    address1     varchar(60),
    address2     varchar(60),
    district     varchar(60),
    village      varchar(60),
    city         varchar(60),
    province     varchar(60),
    PRIMARY KEY (idcontact)
)
;



-- 
-- TABLE: price_agreement_item 
--

CREATE TABLE price_agreement_item(
    idagrite     bit(16)        NOT NULL,
    idproduct    varchar(30),
    PRIMARY KEY (idagrite)
)
;



-- 
-- TABLE: price_component 
--

CREATE TABLE price_component(
    idpricom          bit(16)           NOT NULL,
    idparty           bit(16),
    idproduct         varchar(30),
    idpricetype       int4,
    idparcat          int4,
    idprocat          int4,
    idgeobou          varchar(36),
    idagrite          bit(16),
    price             decimal(18, 4),
    manfucterprice    decimal(18, 4),
    percent           float4,
    dtfrom            timestamp         DEFAULT CURRENT_TIMESTAMP,
    dtthru            timestamp         DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idpricom)
)
;



-- 
-- TABLE: price_type 
--

CREATE TABLE price_type(
    idpricetype    int4           NOT NULL,
    idrultyp       int4,
    description    varchar(50),
    PRIMARY KEY (idpricetype)
)
;



-- 
-- TABLE: product 
--

CREATE TABLE product(
    idproduct         varchar(30)    NOT NULL,
    idprotyp          int4,
    name              varchar(30),
    dtintroduction    timestamp      DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idproduct)
)
;



-- 
-- TABLE: product_association 
--

CREATE TABLE product_association(
    idproass         bit(16)           NOT NULL,
    idasstyp         int4,
    idproductto      varchar(30),
    idproductfrom    varchar(30),
    qty              decimal(14, 4),
    dtfrom           timestamp         DEFAULT CURRENT_TIMESTAMP,
    dtthru           timestamp         DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idproass)
)
;



-- 
-- TABLE: product_category 
--

CREATE TABLE product_category(
    categories_id    int4           NOT NULL,
    products_id      varchar(30)    NOT NULL,
    PRIMARY KEY (categories_id, products_id)
)
;



-- 
-- TABLE: product_document 
--

CREATE TABLE product_document(
    iddocument     bit(16)        NOT NULL,
    idproduct      varchar(30),
    contenttype    varchar(30),
    content        bytea,
    PRIMARY KEY (iddocument)
)
;



-- 
-- TABLE: product_feature 
--

CREATE TABLE product_feature(
    features_id    int4           NOT NULL,
    products_id    varchar(30)    NOT NULL,
    PRIMARY KEY (features_id, products_id)
)
;



-- 
-- TABLE: product_purchase_order 
--

CREATE TABLE product_purchase_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: product_type 
--

CREATE TABLE product_type(
    idprotyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idprotyp)
)
;



-- 
-- TABLE: prospect 
--

CREATE TABLE prospect(
    idprospect            bit(16)         NOT NULL,
    idbroker              bit(16),
    idinternal            bit(16),
    idprosou              int4,
    idevetyp              int4,
    idfacility            bit(16),
    idsalesman            bit(16),
    idsuspect             bit(16),
    idsalescoordinator    bit(16),
    prospectnumber        varchar(30),
    prospectcount         int4,
    eveloc                varchar(600),
    dtfollowup            timestamp       DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idprospect)
)
;



-- 
-- TABLE: prospect_role 
--

CREATE TABLE prospect_role(
    idrole        bit(16)        NOT NULL,
    idprospect    bit(16),
    idroletype    int4,
    idparty       bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: prospect_source 
--

CREATE TABLE prospect_source(
    idprosou       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idprosou)
)
;



-- 
-- TABLE: prospect_status 
--

CREATE TABLE prospect_status(
    idstatus        bit(16)         NOT NULL,
    idprospect      bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: province 
--

CREATE TABLE province(
    idgeobou    varchar(36)    NOT NULL,
    PRIMARY KEY (idgeobou)
)
;



-- 
-- TABLE: purchase_order 
--

CREATE TABLE purchase_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: purchase_request 
--

CREATE TABLE purchase_request(
    idreq         bit(16)        NOT NULL,
    idproduct     varchar(30),
    idinternal    bit(16),
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: purpose_type 
--

CREATE TABLE purpose_type(
    idpurposetype    int4           NOT NULL,
    description      varchar(50),
    PRIMARY KEY (idpurposetype)
)
;



-- 
-- TABLE: quote 
--

CREATE TABLE quote(
    idquote        bit(16)        NOT NULL,
    dtissued       timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    description    varchar(50),
    PRIMARY KEY (idquote)
)
;



-- 
-- TABLE: quote_item 
--

CREATE TABLE quote_item(
    idquoteitem    bit(16)           NOT NULL,
    idquote        bit(16),
    idproduct      varchar(30),
    unitprice      decimal(18, 4),
    qty            decimal(14, 4),
    PRIMARY KEY (idquoteitem)
)
;



-- 
-- TABLE: quote_role 
--

CREATE TABLE quote_role(
    idrole        varchar(10)    NOT NULL,
    idquote       bit(16),
    idroletype    int4,
    idparty       bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: quote_status 
--

CREATE TABLE quote_status(
    idstatus        bit(16)         NOT NULL,
    idquote         bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: reason_type 
--

CREATE TABLE reason_type(
    idreason       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idreason)
)
;



-- 
-- TABLE: receipt 
--

CREATE TABLE receipt(
    idpayment     bit(16)    NOT NULL,
    idinternal    bit(16),
    idpaifro      bit(16),
    idreq         bit(16),
    PRIMARY KEY (idpayment)
)
;



-- 
-- TABLE: regular_sales_order 
--

CREATE TABLE regular_sales_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: relation_type 
--

CREATE TABLE relation_type(
    idreltyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idreltyp)
)
;



-- 
-- TABLE: religion_type 
--

CREATE TABLE religion_type(
    idreligiontype    int4           NOT NULL,
    description       varchar(50),
    PRIMARY KEY (idreligiontype)
)
;



-- 
-- TABLE: rem_part 
--

CREATE TABLE rem_part(
    idproduct    varchar(30)    NOT NULL,
    PRIMARY KEY (idproduct)
)
;



-- 
-- TABLE: requirement 
--

CREATE TABLE requirement(
    idreq                bit(16)           NOT NULL,
    idfacility           bit(16),
    idparentreq          bit(16),
    idreqtyp             int4,
    requirementnumber    varchar(30),
    description          varchar(50),
    dtcreate             timestamp         DEFAULT CURRENT_TIMESTAMP,
    dtrequired           date,
    budget               decimal(18, 4),
    qty                  decimal(14, 4),
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: requirement_payment 
--

CREATE TABLE requirement_payment(
    payments_id        bit(16)    NOT NULL,
    requirements_id    bit(16)    NOT NULL,
    PRIMARY KEY (payments_id, requirements_id)
)
;



-- 
-- TABLE: requirement_role 
--

CREATE TABLE requirement_role(
    idrole        bit(16)        NOT NULL,
    idreq         bit(16),
    idroletype    int4,
    idparty       bit(16)        NOT NULL,
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: requirement_status 
--

CREATE TABLE requirement_status(
    idstatus        bit(16)         NOT NULL,
    idreq           bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: requirement_type 
--

CREATE TABLE requirement_type(
    idreqtyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idreqtyp)
)
;



-- 
-- TABLE: return_purchase_order 
--

CREATE TABLE return_purchase_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: return_sales_order 
--

CREATE TABLE return_sales_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: role_type 
--

CREATE TABLE role_type(
    idroletype     int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idroletype)
)
;



-- 
-- TABLE: rule_sales_discount 
--

CREATE TABLE rule_sales_discount(
    idrule       int4              NOT NULL,
    name         varchar(30),
    maxamount    decimal(18, 4),
    PRIMARY KEY (idrule)
)
;



-- 
-- TABLE: rule_type 
--

CREATE TABLE rule_type(
    idrultyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idrultyp)
)
;



-- 
-- TABLE: rules 
--

CREATE TABLE rules(
    idrule         int4           NOT NULL,
    idrultyp       int4,
    seqnum         int4,
    description    varchar(50),
    dtfrom         timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrule)
)
;



-- 
-- TABLE: sale_type 
--

CREATE TABLE sale_type(
    idsaletype     int4           NOT NULL,
    idparent       int4,
    description    varchar(50),
    PRIMARY KEY (idsaletype)
)
;



-- 
-- TABLE: sales_agreement 
--

CREATE TABLE sales_agreement(
    idagreement    bit(16)    NOT NULL,
    idinternal     bit(16),
    idcustomer     bit(16),
    PRIMARY KEY (idagreement)
)
;



-- 
-- TABLE: sales_broker 
--

CREATE TABLE sales_broker(
    idparrol    bit(16)        NOT NULL,
    idbrotyp    int4,
    idbroker    varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: sales_order 
--

CREATE TABLE sales_order(
    idorder       bit(16)    NOT NULL,
    idsaletype    int4,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: sales_point 
--

CREATE TABLE sales_point(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: sales_unit_leasing 
--

CREATE TABLE sales_unit_leasing(
    idsallea        bit(16)        NOT NULL,
    idreq           bit(16),
    idleacom        bit(16),
    idstatustype    int4,
    idorder         bit(16),
    idlsgpro        bit(16),
    ponumber        varchar(30),
    dtpo            date,
    dtfrom          timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idsallea)
)
;



-- 
-- TABLE: sales_unit_requirement 
--

CREATE TABLE sales_unit_requirement(
    idreq              bit(16)           NOT NULL,
    idprospect         bit(16),
    idsaletype         int4,
    idcustomer         bit(16),
    idbillto           bit(16),
    idsalesman         bit(16),
    idproduct          varchar(30),
    idcolor            int4,
    idinternal         bit(16),
    idsalesbroker      bit(16),
    idowner            bit(16),
    idlsgpro           bit(16),
    downpayment        decimal(18, 4),
    idframe            varchar(30),
    idmachine          varchar(30),
    subsfincomp        decimal(18, 4),
    subsmd             decimal(18, 4),
    subsown            decimal(18, 4),
    subsahm            decimal(18, 4),
    bbnprice           decimal(18, 4),
    hetprice           decimal(18, 4),
    note               varchar(300),
    unitprice          decimal(18, 4),
    notepartner        varchar(300),
    shipmentnote       varchar(300),
    requestpoliceid    varchar(30),
    leasingpo          varchar(30),
    dtpoleasing        timestamp         DEFAULT CURRENT_TIMESTAMP,
    brokerfee          decimal(18, 4),
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: salesman 
--

CREATE TABLE salesman(
    idparrol         bit(16)        NOT NULL,
    idcoordinator    bit(16),
    idsalesman       varchar(30),
    coordinator      varchar(5),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: service 
--

CREATE TABLE service(
    iduom        varchar(30),
    idproduct    varchar(30)    NOT NULL,
    PRIMARY KEY (idproduct)
)
;



-- 
-- TABLE: service_agreement 
--

CREATE TABLE service_agreement(
    idagreement    bit(16)    NOT NULL,
    idinternal     bit(16),
    idcustomer     bit(16),
    PRIMARY KEY (idagreement)
)
;



-- 
-- TABLE: service_per_motor 
--

CREATE TABLE service_per_motor(
    idproduct    varchar(30)    NOT NULL,
    PRIMARY KEY (idproduct)
)
;



-- 
-- TABLE: service_reminder 
--

CREATE TABLE service_reminder(
    idreminder        int4    NOT NULL,
    idclaimtype       int4,
    idremindertype    int4,
    days              int4,
    PRIMARY KEY (idreminder)
)
;



-- 
-- TABLE: ship_to 
--

CREATE TABLE ship_to(
    idparrol    bit(16)        NOT NULL,
    idshipto    varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: shipment 
--

CREATE TABLE shipment(
    idshipment        bit(16)         NOT NULL,
    idshityp          int4,
    idfacility        bit(16),
    idshifro          bit(16),
    idshito           bit(16),
    idposaddfro       bit(16),
    idposaddto        bit(16),
    shipmentnumber    varchar(30),
    description       varchar(50),
    dtschedulle       timestamp       DEFAULT CURRENT_TIMESTAMP,
    reason            varchar(300),
    PRIMARY KEY (idshipment)
)
;



-- 
-- TABLE: shipment_incoming 
--

CREATE TABLE shipment_incoming(
    idshipment    bit(16)    NOT NULL,
    PRIMARY KEY (idshipment)
)
;



-- 
-- TABLE: shipment_item 
--

CREATE TABLE shipment_item(
    idshiite              bit(16)           NOT NULL,
    idshipment            bit(16),
    idfeature             int4,
    idproduct             varchar(30),
    qty                   decimal(14, 4),
    contentdescription    varchar(50),
    idframe               varchar(50),
    idmachine             varchar(50),
    PRIMARY KEY (idshiite)
)
;



-- 
-- TABLE: shipment_item_order_item 
--

CREATE TABLE shipment_item_order_item(
    order_items_id       bit(16)    NOT NULL,
    shipment_items_id    bit(16)    NOT NULL,
    PRIMARY KEY (order_items_id, shipment_items_id)
)
;



-- 
-- TABLE: shipment_outgoing 
--

CREATE TABLE shipment_outgoing(
    idshipment    bit(16)         NOT NULL,
    dtbast        date,
    note          varchar(600),
    PRIMARY KEY (idshipment)
)
;



-- 
-- TABLE: shipment_package 
--

CREATE TABLE shipment_package(
    idpackage      bit(16)      NOT NULL,
    idshipactyp    int4,
    idinternal     bit(16),
    dtcreated      timestamp    DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idpackage)
)
;



-- 
-- TABLE: shipment_package_role 
--

CREATE TABLE shipment_package_role(
    idrole        bit(16)        NOT NULL,
    idparty       bit(16),
    idroletype    int4,
    idpackage     bit(16),
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: shipment_package_status 
--

CREATE TABLE shipment_package_status(
    idstatus        bit(16)         NOT NULL,
    idstatustype    int4,
    idreason        int4,
    idpackage       bit(16),
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: shipment_package_type 
--

CREATE TABLE shipment_package_type(
    idshipactyp    int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idshipactyp)
)
;



-- 
-- TABLE: shipment_receipt 
--

CREATE TABLE shipment_receipt(
    idreceipt          bit(16)           NOT NULL,
    idproduct          varchar(30),
    idpackage          bit(16),
    idfeature          int4,
    idordite           bit(16),
    code               varchar(30),
    qtyaccept          decimal(14, 4),
    qtyreject          decimal(14, 4),
    itemdescription    varchar(50),
    dtreceipt          timestamp         DEFAULT CURRENT_TIMESTAMP,
    idframe            varchar(50),
    idmachine          varchar(50),
    acc1               varchar(5),
    acc2               varchar(5),
    acc3               varchar(5),
    acc4               varchar(5),
    acc5               varchar(5),
    acc6               varchar(5),
    acc7               varchar(5),
    PRIMARY KEY (idreceipt)
)
;



-- 
-- TABLE: shipment_receipt_role 
--

CREATE TABLE shipment_receipt_role(
    idrole        bit(16)        NOT NULL,
    idreceipt     bit(16),
    idparty       bit(16),
    idroletype    int4,
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: shipment_receipt_shipment_item 
--

CREATE TABLE shipment_receipt_shipment_item(
    shipment_items_id       bit(16)    NOT NULL,
    shipment_receipts_id    bit(16)    NOT NULL,
    PRIMARY KEY (shipment_items_id, shipment_receipts_id)
)
;



-- 
-- TABLE: shipment_receipt_status 
--

CREATE TABLE shipment_receipt_status(
    idstatus        bit(16)         NOT NULL,
    idreceipt       bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: shipment_status 
--

CREATE TABLE shipment_status(
    idstatus        bit(16)         NOT NULL,
    idshipment      bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: shipment_type 
--

CREATE TABLE shipment_type(
    idshityp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idshityp)
)
;



-- 
-- TABLE: standard_calendar 
--

CREATE TABLE standard_calendar(
    idcalendar    int4       NOT NULL,
    idinternal    bit(16),
    PRIMARY KEY (idcalendar)
)
;



-- 
-- TABLE: status_type 
--

CREATE TABLE status_type(
    idstatustype    int4           NOT NULL,
    description     varchar(50),
    PRIMARY KEY (idstatustype)
)
;



-- 
-- TABLE: suspect 
--

CREATE TABLE suspect(
    idsuspect             bit(16)        NOT NULL,
    suspectnumber         varchar(30),
    iddealer              bit(16),
    idparty               bit(16),
    idsalescoordinator    bit(16),
    idsalesman            bit(16),
    idaddress             bit(16),
    idsuspecttyp          int4,
    PRIMARY KEY (idsuspect)
)
;



-- 
-- TABLE: suspect_role 
--

CREATE TABLE suspect_role(
    idrole        bit(16)        NOT NULL,
    idsuspect     bit(16),
    idparty       bit(16),
    idroletype    int4,
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: suspect_status 
--

CREATE TABLE suspect_status(
    idstatus        bit(16)         NOT NULL,
    idsuspect       bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: suspect_type 
--

CREATE TABLE suspect_type(
    idsuspecttyp    int4           NOT NULL,
    description     varchar(50),
    PRIMARY KEY (idsuspecttyp)
)
;



-- 
-- TABLE: telecomunication_number 
--

CREATE TABLE telecomunication_number(
    idcontact    bit(16)        NOT NULL,
    number       varchar(30),
    PRIMARY KEY (idcontact)
)
;



-- 
-- TABLE: unit_accesories_mapper 
--

CREATE TABLE unit_accesories_mapper(
    iduntaccmap    bit(16)           NOT NULL,
    idmotor        varchar(30),
    acc1           varchar(30),
    qtyacc1        decimal(14, 4),
    acc2           varchar(30),
    qtyacc2        decimal(14, 4),
    acc3           varchar(30),
    qtyacc3        decimal(14, 4),
    acc4           varchar(30),
    qtyacc4        decimal(14, 4),
    acc5           varchar(30),
    qtyacc5        decimal(14, 4),
    acc6           varchar(30),
    qtyacc6        decimal(14, 4),
    acc7           varchar(30),
    qtyacc7        decimal(14, 4),
    promat1        varchar(30),
    qtypromat1     decimal(14, 4),
    promat2        varchar(30),
    qtypromat2     decimal(14, 4),
    dtfrom         timestamp         DEFAULT CURRENT_TIMESTAMP,
    dtthru         timestamp         DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (iduntaccmap)
)
;



-- 
-- TABLE: unit_deliverable 
--

CREATE TABLE unit_deliverable(
    iddeliverable     int4           NOT NULL,
    idreq             bit(16),
    iddeltype         int4,
    idvndstatyp       int4,
    idconstatyp       int4,
    description       varchar(50),
    dtreceipt         date,
    dtdelivery        date,
    bastnumber        varchar(30),
    name              varchar(30),
    identitynumber    varchar(30),
    cellphone         varchar(30),
    PRIMARY KEY (iddeliverable)
)
;



-- 
-- TABLE: unit_requirement 
--

CREATE TABLE unit_requirement(
    idreq        bit(16)        NOT NULL,
    idbranch     bit(16),
    idproduct    varchar(30),
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: uom 
--

CREATE TABLE uom(
    iduom          varchar(30)    NOT NULL,
    iduomtyp       int4,
    description    varchar(50),
    PRIMARY KEY (iduom)
)
;



-- 
-- TABLE: uom_conversion 
--

CREATE TABLE uom_conversion(
    iduomconversion    int4              NOT NULL,
    iduomto            varchar(30),
    iduomfro           varchar(30),
    factor             decimal(14, 4),
    PRIMARY KEY (iduomconversion)
)
;



-- 
-- TABLE: uom_type 
--

CREATE TABLE uom_type(
    iduomtyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (iduomtyp)
)
;



-- 
-- TABLE: user_extra 
--

CREATE TABLE user_extra(
    iduser        int8           NOT NULL,
    idinternal    varchar(30),
    PRIMARY KEY (iduser)
)
;



-- 
-- TABLE: vehicle 
--

CREATE TABLE vehicle(
    idvehicle     bit(16)        NOT NULL,
    idproduct     varchar(30),
    idcolor       int4,
    idinternal    bit(16),
    idmachine     varchar(50),
    idframe       varchar(50),
    yearofass     int4,
    PRIMARY KEY (idvehicle)
)
;



-- 
-- TABLE: vehicle_document_requirement 
--

CREATE TABLE vehicle_document_requirement(
    idreq            bit(16)           NOT NULL,
    idslsreq         bit(16),
    idordite         bit(16),
    idpersonowner    bit(16),
    idinternal       bit(16),
    idvehregtyp      int4,
    idsaletype       int4,
    idvehicle        bit(16),
    idbillto         bit(16),
    idvendor         bit(16),
    idshipto         bit(16),
    note             varchar(300),
    atpmfaktur       varchar(30),
    bbn              decimal(18, 4),
    othercost        decimal(18, 4),
    policenumber     varchar(30),
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: vehicle_identification 
--

CREATE TABLE vehicle_identification(
    idvehide         bit(16)        NOT NULL,
    idvehicle        bit(16),
    idcustomer       bit(16),
    vehiclenumber    varchar(10),
    dtfrom           timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru           timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idvehide)
)
;



-- 
-- TABLE: vehicle_purchase_order 
--

CREATE TABLE vehicle_purchase_order(
    idorder    bit(16)    NOT NULL,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: vehicle_registration_type 
--

CREATE TABLE vehicle_registration_type(
    idvehregtyp    int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idvehregtyp)
)
;



-- 
-- TABLE: vehicle_sales_billing 
--

CREATE TABLE vehicle_sales_billing(
    idbilling     bit(16)    NOT NULL,
    idcustomer    bit(16),
    idsaletype    int4,
    PRIMARY KEY (idbilling)
)
;



-- 
-- TABLE: vehicle_sales_order 
--

CREATE TABLE vehicle_sales_order(
    idorder           bit(16)       NOT NULL,
    idunitreq         bit(16),
    idsaletype        int4,
    idleacom          bit(16),
    shiptoowner       varchar(5),
    dtbastunit        date,
    dtcustreceipt     date,
    dtmachineswipe    date,
    dttakepicture     date,
    dtcovernote       date,
    idlsgpaystatus    int4,
    dtadmslsverify    timestamp     DEFAULT CURRENT_TIMESTAMP,
    dtafcverify       timestamp     DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: vehicle_work_requirement 
--

CREATE TABLE vehicle_work_requirement(
    idreq            bit(16)        NOT NULL,
    idmechanic       bit(16),
    idvehicle        bit(16),
    idvehide         bit(16),
    idcustomer       bit(16),
    vehiclenumber    varchar(10),
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: vendor 
--

CREATE TABLE vendor(
    idparrol    bit(16)        NOT NULL,
    idvndtyp    int4,
    idvendor    varchar(30),
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: vendor_bill_to 
--

CREATE TABLE vendor_bill_to(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: vendor_order 
--

CREATE TABLE vendor_order(
    idorder       bit(16)    NOT NULL,
    idinternal    bit(16),
    idvendor      bit(16),
    idbillto      bit(16),
    PRIMARY KEY (idorder)
)
;



-- 
-- TABLE: vendor_product 
--

CREATE TABLE vendor_product(
    idvenpro      bit(16)        NOT NULL,
    idproduct     varchar(30),
    idparrol      bit(16),
    orderratio    int4,
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idvenpro)
)
;



-- 
-- TABLE: vendor_relationship 
--

CREATE TABLE vendor_relationship(
    idparrel      bit(16)    NOT NULL,
    idinternal    bit(16),
    idvendor      bit(16),
    PRIMARY KEY (idparrel)
)
;



-- 
-- TABLE: vendor_ship_to 
--

CREATE TABLE vendor_ship_to(
    idparrol    bit(16)    NOT NULL,
    PRIMARY KEY (idparrol)
)
;



-- 
-- TABLE: vendor_type 
--

CREATE TABLE vendor_type(
    idvndtyp       int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idvndtyp)
)
;



-- 
-- TABLE: village 
--

CREATE TABLE village(
    idgeobou    varchar(36)    NOT NULL,
    PRIMARY KEY (idgeobou)
)
;



-- 
-- TABLE: we_service_type 
--

CREATE TABLE we_service_type(
    idwetyp      int4           NOT NULL,
    idproduct    varchar(30),
    frt          int4,
    PRIMARY KEY (idwetyp)
)
;



-- 
-- TABLE: we_type 
--

CREATE TABLE we_type(
    idwetyp        int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idwetyp)
)
;



-- 
-- TABLE: we_type_good_standard 
--

CREATE TABLE we_type_good_standard(
    idwegoostd    bit(16)           NOT NULL,
    idwetyp       int4,
    idproduct     varchar(30),
    qty           decimal(14, 4),
    PRIMARY KEY (idwegoostd)
)
;



-- 
-- TABLE: work_effort 
--

CREATE TABLE work_effort(
    idwe           bit(16)        NOT NULL,
    idfacility     bit(16),
    idwetyp        int4,
    name           varchar(30),
    description    varchar(50),
    PRIMARY KEY (idwe)
)
;



-- 
-- TABLE: work_effort_payment 
--

CREATE TABLE work_effort_payment(
    payments_id        bit(16)    NOT NULL,
    work_efforts_id    bit(16)    NOT NULL,
    PRIMARY KEY (payments_id, work_efforts_id)
)
;



-- 
-- TABLE: work_effort_role 
--

CREATE TABLE work_effort_role(
    idrole        bit(16)        NOT NULL,
    idwe          bit(16),
    idparty       bit(16),
    idroletype    int4,
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: work_effort_status 
--

CREATE TABLE work_effort_status(
    idstatus        bit(16)         NOT NULL,
    idwe            bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: work_order 
--

CREATE TABLE work_order(
    idreq    bit(16)    NOT NULL,
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: work_order_booking 
--

CREATE TABLE work_order_booking(
    idbooking        bit(16)        NOT NULL,
    idvehicle        bit(16),
    idbooslo         bit(16),
    idboktyp         int4,
    idevetyp         int4,
    idinternal       bit(16),
    idcustomer       bit(16),
    bookingnumber    varchar(30),
    vehiclenumber    varchar(10),
    dtcreate         timestamp      DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (idbooking)
)
;



-- 
-- TABLE: work_order_booking_role 
--

CREATE TABLE work_order_booking_role(
    idrole        bit(16)        NOT NULL,
    idbooking     bit(16),
    idparty       bit(16),
    idroletype    int4,
    username      varchar(30),
    dtfrom        timestamp      DEFAULT CURRENT_TIMESTAMP,
    dtthru        timestamp      DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idrole)
)
;



-- 
-- TABLE: work_order_booking_status 
--

CREATE TABLE work_order_booking_status(
    idstatus        bit(16)         NOT NULL,
    idbooking       bit(16),
    idstatustype    int4,
    idreason        int4,
    reason          varchar(300),
    dtfrom          timestamp       DEFAULT CURRENT_TIMESTAMP,
    dtthru          timestamp       DEFAULT '9999-12-31 23:59:59',
    PRIMARY KEY (idstatus)
)
;



-- 
-- TABLE: work_product_req 
--

CREATE TABLE work_product_req(
    idreq    bit(16)    NOT NULL,
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: work_requirement 
--

CREATE TABLE work_requirement(
    idreq    bit(16)    NOT NULL,
    PRIMARY KEY (idreq)
)
;



-- 
-- TABLE: work_requirement_services 
--

CREATE TABLE work_requirement_services(
    idwe     bit(16)    NOT NULL,
    idreq    bit(16)    NOT NULL,
    PRIMARY KEY (idwe, idreq)
)
;



-- 
-- TABLE: work_service_requirement 
--

CREATE TABLE work_service_requirement(
    idwe    bit(16)    NOT NULL,
    PRIMARY KEY (idwe)
)
;



-- 
-- TABLE: work_type 
--

CREATE TABLE work_type(
    idworktype     int4           NOT NULL,
    description    varchar(50),
    PRIMARY KEY (idworktype)
)
;



-- 
-- INDEX: product_cat_ref_key 
--

CREATE UNIQUE INDEX product_cat_ref_key ON category_product(idcattyp, refkey)
;
-- 
-- INDEX: geo_boundary_code 
--

CREATE UNIQUE INDEX geo_boundary_code ON geo_boundary(geocode)
;
-- 
-- INDEX: inventory_item_machine_id 
--

CREATE INDEX inventory_item_machine_id ON inventory_item(idmachine)
;
-- 
-- INDEX: inventory_item_frame_id 
--

CREATE INDEX inventory_item_frame_id ON inventory_item(idframe)
;
-- 
-- INDEX: idx_persistent_audit_event 
--

CREATE INDEX idx_persistent_audit_event ON jhi_persistent_audit_event(event_date, principal)
;
-- 
-- INDEX: idx_persistent_audit_evt_data 
--

CREATE INDEX idx_persistent_audit_evt_data ON jhi_persistent_audit_evt_data(event_id)
;
-- 
-- INDEX: idx_user_login 
--

CREATE UNIQUE INDEX idx_user_login ON jhi_user(login)
;
-- 
-- INDEX: idx_user_email 
--

CREATE UNIQUE INDEX idx_user_email ON jhi_user(email)
;
-- 
-- INDEX: login 
--

CREATE UNIQUE INDEX login ON jhi_user(login)
;
-- 
-- INDEX: email 
--

CREATE UNIQUE INDEX email ON jhi_user(email)
;
-- 
-- INDEX: leasing_company_id 
--

CREATE UNIQUE INDEX leasing_company_id ON leasing_company(idleacom)
;
-- 
-- INDEX: package_receipt_doc_number 
--

CREATE UNIQUE INDEX package_receipt_doc_number ON package_receipt(documentnumber)
;
-- 
-- INDEX: person_id_user 
--

CREATE INDEX person_id_user ON person(username)
;
-- 
-- INDEX: price_component_period 
--

CREATE INDEX price_component_period ON price_component(dtfrom, dtthru)
;
-- 
-- INDEX: we_service_type_id_product 
--

CREATE UNIQUE INDEX we_service_type_id_product ON we_service_type(idproduct)
;
-- 
-- TABLE: agreement 
--

ALTER TABLE agreement ADD 
    FOREIGN KEY (idagrtyp)
    REFERENCES agreement_type(idagrtyp)
;


-- 
-- TABLE: agreement_item 
--

ALTER TABLE agreement_item ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;


-- 
-- TABLE: agreement_motor_item 
--

ALTER TABLE agreement_motor_item ADD 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
;

ALTER TABLE agreement_motor_item ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: agreement_role 
--

ALTER TABLE agreement_role ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE agreement_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE agreement_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: agreement_status 
--

ALTER TABLE agreement_status ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE agreement_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE agreement_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;


-- 
-- TABLE: approval 
--

ALTER TABLE approval ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE approval ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE approval ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE approval ADD 
    FOREIGN KEY (idapptyp)
    REFERENCES approval_type(idapptyp)
;


-- 
-- TABLE: base_calendar 
--

ALTER TABLE base_calendar ADD 
    FOREIGN KEY (idparent)
    REFERENCES base_calendar(idcalendar)
;

ALTER TABLE base_calendar ADD 
    FOREIGN KEY (idcaltyp)
    REFERENCES calendar_type(idcaltyp)
;


-- 
-- TABLE: bill_to 
--

ALTER TABLE bill_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: billing 
--

ALTER TABLE billing ADD 
    FOREIGN KEY (idbiltyp)
    REFERENCES billing_type(idbiltyp)
;


-- 
-- TABLE: billing_disbursement 
--

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idparrol)
;

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
;

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE billing_disbursement ADD 
    FOREIGN KEY (idbilfro)
    REFERENCES bill_to(idparrol)
;


-- 
-- TABLE: billing_item 
--

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idbilitetyp)
    REFERENCES billing_item_type(idbilitetyp)
;

ALTER TABLE billing_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;


-- 
-- TABLE: billing_payment 
--

ALTER TABLE billing_payment ADD 
    FOREIGN KEY (billings_id)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_payment ADD 
    FOREIGN KEY (payments_id)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: billing_receipt 
--

ALTER TABLE billing_receipt ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;

ALTER TABLE billing_receipt ADD 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
;

ALTER TABLE billing_receipt ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE billing_receipt ADD 
    FOREIGN KEY (idbilto)
    REFERENCES bill_to(idparrol)
;


-- 
-- TABLE: billing_role 
--

ALTER TABLE billing_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE billing_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE billing_role ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;


-- 
-- TABLE: billing_status 
--

ALTER TABLE billing_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE billing_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE billing_status ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;


-- 
-- TABLE: booking_slot 
--

ALTER TABLE booking_slot ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
;

ALTER TABLE booking_slot ADD 
    FOREIGN KEY (idbooslostd)
    REFERENCES booking_slot_standard(idbooslostd)
;


-- 
-- TABLE: booking_slot_standard 
--

ALTER TABLE booking_slot_standard ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE booking_slot_standard ADD 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
;


-- 
-- TABLE: branch 
--

ALTER TABLE branch ADD 
    FOREIGN KEY (idbranchcategory)
    REFERENCES category_party(idcategory)
;

ALTER TABLE branch ADD 
    FOREIGN KEY (idparrol)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: category 
--

ALTER TABLE category ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES category_type(idcattyp)
;


-- 
-- TABLE: category_party 
--

ALTER TABLE category_party ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES category_type(idcattyp)
;


-- 
-- TABLE: category_product 
--

ALTER TABLE category_product ADD 
    FOREIGN KEY (idcattyp)
    REFERENCES category_type(idcattyp)
;


-- 
-- TABLE: category_type 
--

ALTER TABLE category_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;

ALTER TABLE category_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES category_type(idcattyp)
;


-- 
-- TABLE: city 
--

ALTER TABLE city ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: communication_event 
--

ALTER TABLE communication_event ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;

ALTER TABLE communication_event ADD 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
;


-- 
-- TABLE: communication_event_cdb 
--

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;

ALTER TABLE communication_event_cdb ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;


-- 
-- TABLE: communication_event_prospect 
--

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;

ALTER TABLE communication_event_prospect ADD 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
;


-- 
-- TABLE: communication_event_purpose 
--

ALTER TABLE communication_event_purpose ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: communication_event_role 
--

ALTER TABLE communication_event_role ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE communication_event_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: communication_event_status 
--

ALTER TABLE communication_event_status ADD 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
;

ALTER TABLE communication_event_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE communication_event_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;


-- 
-- TABLE: contact_mechanism_purpose 
--

ALTER TABLE contact_mechanism_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;

ALTER TABLE contact_mechanism_purpose ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE contact_mechanism_purpose ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: contact_purpose 
--

ALTER TABLE contact_purpose ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE contact_purpose ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;


-- 
-- TABLE: container 
--

ALTER TABLE container ADD 
    FOREIGN KEY (idcontyp)
    REFERENCES container_type(idcontyp)
;

ALTER TABLE container ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;


-- 
-- TABLE: customer 
--

ALTER TABLE customer ADD 
    FOREIGN KEY (idmarketcategory)
    REFERENCES category_party(idcategory)
;

ALTER TABLE customer ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: customer_bill_to 
--

ALTER TABLE customer_bill_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES bill_to(idparrol)
;


-- 
-- TABLE: customer_order 
--

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idparrol)
;

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;

ALTER TABLE customer_order ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: customer_quotation 
--

ALTER TABLE customer_quotation ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;

ALTER TABLE customer_quotation ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE customer_quotation ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;


-- 
-- TABLE: customer_relationship 
--

ALTER TABLE customer_relationship ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;

ALTER TABLE customer_relationship ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE customer_relationship ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;


-- 
-- TABLE: customer_ship_to 
--

ALTER TABLE customer_ship_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES ship_to(idparrol)
;


-- 
-- TABLE: disbursement 
--

ALTER TABLE disbursement ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE disbursement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE disbursement ADD 
    FOREIGN KEY (idpaito)
    REFERENCES bill_to(idparrol)
;


-- 
-- TABLE: district 
--

ALTER TABLE district ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: document_type 
--

ALTER TABLE document_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES document_type(iddoctype)
;


-- 
-- TABLE: documents 
--

ALTER TABLE documents ADD 
    FOREIGN KEY (iddoctype)
    REFERENCES document_type(iddoctype)
;


-- 
-- TABLE: driver 
--

ALTER TABLE driver ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: electronic_address 
--

ALTER TABLE electronic_address ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: employee 
--

ALTER TABLE employee ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: estimation 
--

ALTER TABLE estimation ADD 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_work_requirement(idreq)
;


-- 
-- TABLE: event_type 
--

ALTER TABLE event_type ADD 
    FOREIGN KEY (idprneve)
    REFERENCES event_type(idevetyp)
;


-- 
-- TABLE: facility 
--

ALTER TABLE facility ADD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
;

ALTER TABLE facility ADD 
    FOREIGN KEY (idfacilitytype)
    REFERENCES facility_type(idfacilitytype)
;

ALTER TABLE facility ADD 
    FOREIGN KEY (idpartof)
    REFERENCES facility(idfacility)
;


-- 
-- TABLE: facility_contact 
--

ALTER TABLE facility_contact ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE facility_contact ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: facility_contact_mechanism 
--

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
;

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;


-- 
-- TABLE: facility_status 
--

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;


-- 
-- TABLE: feature 
--

ALTER TABLE feature ADD 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
;


-- 
-- TABLE: feature_type 
--

ALTER TABLE feature_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: fixed_asset 
--

ALTER TABLE fixed_asset ADD 
    FOREIGN KEY (idfatype)
    REFERENCES fixed_asset_type(idfatype)
;

ALTER TABLE fixed_asset ADD 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: fixed_asset_type 
--

ALTER TABLE fixed_asset_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES fixed_asset_type(idfatype)
;


-- 
-- TABLE: geo_boundary 
--

ALTER TABLE geo_boundary ADD 
    FOREIGN KEY (idgeoboutype)
    REFERENCES geo_boundary_type(idgeoboutype)
;

ALTER TABLE geo_boundary ADD 
    FOREIGN KEY (idparent)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: good 
--

ALTER TABLE good ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE good ADD 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: good_container 
--

ALTER TABLE good_container ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE good_container ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE good_container ADD 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
;


-- 
-- TABLE: internal 
--

ALTER TABLE internal ADD 
    FOREIGN KEY (idparent)
    REFERENCES internal(idparrol)
;

ALTER TABLE internal ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: internal_bill_to 
--

ALTER TABLE internal_bill_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES bill_to(idparrol)
;


-- 
-- TABLE: internal_order 
--

ALTER TABLE internal_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE internal_order ADD 
    FOREIGN KEY (idinternalto)
    REFERENCES internal(idparrol)
;

ALTER TABLE internal_order ADD 
    FOREIGN KEY (idinternalfrom)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: internal_ship_to 
--

ALTER TABLE internal_ship_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES ship_to(idparrol)
;


-- 
-- TABLE: inventory_adjustment 
--

ALTER TABLE inventory_adjustment ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;


-- 
-- TABLE: inventory_item 
--

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
;

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
;


-- 
-- TABLE: inventory_item_status 
--

ALTER TABLE inventory_item_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE inventory_item_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE inventory_item_status ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;


-- 
-- TABLE: inventory_movement_status 
--

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;


-- 
-- TABLE: item_issuance 
--

ALTER TABLE item_issuance ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;

ALTER TABLE item_issuance ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE item_issuance ADD 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
;


-- 
-- TABLE: jhi_persistent_audit_evt_data 
--

ALTER TABLE jhi_persistent_audit_evt_data ADD 
    FOREIGN KEY (event_id)
    REFERENCES jhi_persistent_audit_event(event_id)
;


-- 
-- TABLE: jhi_user_authority 
--

ALTER TABLE jhi_user_authority ADD 
    FOREIGN KEY (authority_name)
    REFERENCES jhi_authority(name)
;

ALTER TABLE jhi_user_authority ADD 
    FOREIGN KEY (user_id)
    REFERENCES jhi_user(id)
;


-- 
-- TABLE: leasing_company 
--

ALTER TABLE leasing_company ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: leasing_tenor_provide 
--

ALTER TABLE leasing_tenor_provide ADD 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
;

ALTER TABLE leasing_tenor_provide ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: mechanic 
--

ALTER TABLE mechanic ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: motor 
--

ALTER TABLE motor ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: motor_due_reminder 
--

ALTER TABLE motor_due_reminder ADD 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: moving_slip 
--

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idinviteto)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idcontainerfrom)
    REFERENCES container(idcontainer)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idcontainerto)
    REFERENCES container(idcontainer)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
;

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idinviteto)
    REFERENCES inventory_item(idinvite)
;


-- 
-- TABLE: order_billing_item 
--

ALTER TABLE order_billing_item ADD 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
;

ALTER TABLE order_billing_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;


-- 
-- TABLE: order_item 
--

ALTER TABLE order_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE order_item ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idparrol)
;

ALTER TABLE order_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE order_item ADD 
    FOREIGN KEY (idparordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE order_item ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;


-- 
-- TABLE: order_item_requirement 
--

ALTER TABLE order_item_requirement ADD 
    FOREIGN KEY (order_items_id)
    REFERENCES order_item(idordite)
;

ALTER TABLE order_item_requirement ADD 
    FOREIGN KEY (requirements_id)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: orders 
--

ALTER TABLE orders ADD 
    FOREIGN KEY (idordtyp)
    REFERENCES order_type(idordtyp)
;


-- 
-- TABLE: orders_payment 
--

ALTER TABLE orders_payment ADD 
    FOREIGN KEY (orders_id)
    REFERENCES orders(idorder)
;

ALTER TABLE orders_payment ADD 
    FOREIGN KEY (payments_id)
    REFERENCES payment_application(idpayapp)
;


-- 
-- TABLE: orders_role 
--

ALTER TABLE orders_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE orders_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE orders_role ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;


-- 
-- TABLE: orders_status 
--

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE orders_status ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;


-- 
-- TABLE: organization 
--

ALTER TABLE organization ADD 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
;

ALTER TABLE organization ADD 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE organization ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: organization_customer 
--

ALTER TABLE organization_customer ADD 
    FOREIGN KEY (idparrol)
    REFERENCES customer(idparrol)
;


-- 
-- TABLE: organization_prospect 
--

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
;

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
;

ALTER TABLE organization_prospect ADD 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
;


-- 
-- TABLE: organization_suspect 
--

ALTER TABLE organization_suspect ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;

ALTER TABLE organization_suspect ADD 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
;


-- 
-- TABLE: package_receipt 
--

ALTER TABLE package_receipt ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE package_receipt ADD 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idparrol)
;


-- 
-- TABLE: packaging_content 
--

ALTER TABLE packaging_content ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE packaging_content ADD 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
;


-- 
-- TABLE: parent_organization 
--

ALTER TABLE parent_organization ADD 
    FOREIGN KEY (idparrol)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: part_sales_order 
--

ALTER TABLE part_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
;


-- 
-- TABLE: party_category 
--

ALTER TABLE party_category ADD 
    FOREIGN KEY (categories_id)
    REFERENCES category_party(idcategory)
;

ALTER TABLE party_category ADD 
    FOREIGN KEY (parties_id)
    REFERENCES party(idparty)
;


-- 
-- TABLE: party_contact 
--

ALTER TABLE party_contact ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_contact ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: party_document 
--

ALTER TABLE party_document ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_document ADD 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
;


-- 
-- TABLE: party_facility 
--

ALTER TABLE party_facility ADD 
    FOREIGN KEY (facilities_id)
    REFERENCES facility(idfacility)
;

ALTER TABLE party_facility ADD 
    FOREIGN KEY (parties_id)
    REFERENCES party(idparty)
;


-- 
-- TABLE: party_relationship 
--

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
;

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idparrolfro)
    REFERENCES party_role(idparrol)
;

ALTER TABLE party_relationship ADD 
    FOREIGN KEY (idparrolto)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: party_role 
--

ALTER TABLE party_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE party_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: party_role_status 
--

ALTER TABLE party_role_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE party_role_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE party_role_status ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: payment 
--

ALTER TABLE payment ADD 
    FOREIGN KEY (idpaytyp)
    REFERENCES payment_type(idpaytyp)
;

ALTER TABLE payment ADD 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
;


-- 
-- TABLE: payment_application 
--

ALTER TABLE payment_application ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE payment_application ADD 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
;

ALTER TABLE payment_application ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
;


-- 
-- TABLE: payment_role 
--

ALTER TABLE payment_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE payment_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE payment_role ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: payment_status 
--

ALTER TABLE payment_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE payment_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE payment_status ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;


-- 
-- TABLE: person 
--

ALTER TABLE person ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: person_prospect 
--

ALTER TABLE person_prospect ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE person_prospect ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;


-- 
-- TABLE: person_suspect 
--

ALTER TABLE person_suspect ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;

ALTER TABLE person_suspect ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;


-- 
-- TABLE: personal_customer 
--

ALTER TABLE personal_customer ADD 
    FOREIGN KEY (idparrol)
    REFERENCES customer(idparrol)
;


-- 
-- TABLE: picking_slip 
--

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idparrol)
;

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
;

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
;

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;


-- 
-- TABLE: position_authority 
--

ALTER TABLE position_authority ADD 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
;

ALTER TABLE position_authority ADD 
    FOREIGN KEY (name)
    REFERENCES jhi_authority(name)
;


-- 
-- TABLE: position_fullfillment 
--

ALTER TABLE position_fullfillment ADD 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
;

ALTER TABLE position_fullfillment ADD 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
;


-- 
-- TABLE: position_reporting_structure 
--

ALTER TABLE position_reporting_structure ADD 
    FOREIGN KEY (idposfro)
    REFERENCES positions(idposition)
;

ALTER TABLE position_reporting_structure ADD 
    FOREIGN KEY (idposto)
    REFERENCES positions(idposition)
;


-- 
-- TABLE: positions 
--

ALTER TABLE positions ADD 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
;

ALTER TABLE positions ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE positions ADD 
    FOREIGN KEY (idorganization)
    REFERENCES organization(idparty)
;


-- 
-- TABLE: positions_status 
--

ALTER TABLE positions_status ADD 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
;

ALTER TABLE positions_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE positions_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;


-- 
-- TABLE: postal_address 
--

ALTER TABLE postal_address ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: price_agreement_item 
--

ALTER TABLE price_agreement_item ADD 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
;

ALTER TABLE price_agreement_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: price_component 
--

ALTER TABLE price_component ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE price_component ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE price_component ADD 
    FOREIGN KEY (idpricetype)
    REFERENCES price_type(idpricetype)
;

ALTER TABLE price_component ADD 
    FOREIGN KEY (idparcat)
    REFERENCES category_party(idcategory)
;

ALTER TABLE price_component ADD 
    FOREIGN KEY (idprocat)
    REFERENCES category_product(idcategory)
;

ALTER TABLE price_component ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;

ALTER TABLE price_component ADD 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
;


-- 
-- TABLE: price_type 
--

ALTER TABLE price_type ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: product 
--

ALTER TABLE product ADD 
    FOREIGN KEY (idprotyp)
    REFERENCES product_type(idprotyp)
;


-- 
-- TABLE: product_association 
--

ALTER TABLE product_association ADD 
    FOREIGN KEY (idasstyp)
    REFERENCES association_type(idasstyp)
;

ALTER TABLE product_association ADD 
    FOREIGN KEY (idproductfrom)
    REFERENCES product(idproduct)
;

ALTER TABLE product_association ADD 
    FOREIGN KEY (idproductto)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_category 
--

ALTER TABLE product_category ADD 
    FOREIGN KEY (categories_id)
    REFERENCES category_product(idcategory)
;

ALTER TABLE product_category ADD 
    FOREIGN KEY (products_id)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_document 
--

ALTER TABLE product_document ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE product_document ADD 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
;


-- 
-- TABLE: product_feature 
--

ALTER TABLE product_feature ADD 
    FOREIGN KEY (features_id)
    REFERENCES feature(idfeature)
;

ALTER TABLE product_feature ADD 
    FOREIGN KEY (products_id)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: product_purchase_order 
--

ALTER TABLE product_purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
;


-- 
-- TABLE: prospect 
--

ALTER TABLE prospect ADD 
    FOREIGN KEY (idbroker)
    REFERENCES sales_broker(idparrol)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE prospect ADD 
    FOREIGN KEY (idprosou)
    REFERENCES prospect_source(idprosou)
;


-- 
-- TABLE: prospect_role 
--

ALTER TABLE prospect_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE prospect_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE prospect_role ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;


-- 
-- TABLE: prospect_status 
--

ALTER TABLE prospect_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE prospect_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE prospect_status ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;


-- 
-- TABLE: province 
--

ALTER TABLE province ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: purchase_order 
--

ALTER TABLE purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
;


-- 
-- TABLE: purchase_request 
--

ALTER TABLE purchase_request ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE purchase_request ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE purchase_request ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: quote_item 
--

ALTER TABLE quote_item ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE quote_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;


-- 
-- TABLE: quote_role 
--

ALTER TABLE quote_role ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE quote_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE quote_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;


-- 
-- TABLE: quote_status 
--

ALTER TABLE quote_status ADD 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
;

ALTER TABLE quote_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE quote_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;


-- 
-- TABLE: receipt 
--

ALTER TABLE receipt ADD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
;

ALTER TABLE receipt ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE receipt ADD 
    FOREIGN KEY (idpaifro)
    REFERENCES bill_to(idparrol)
;

ALTER TABLE receipt ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: regular_sales_order 
--

ALTER TABLE regular_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
;


-- 
-- TABLE: rem_part 
--

ALTER TABLE rem_part ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: requirement 
--

ALTER TABLE requirement ADD 
    FOREIGN KEY (idreqtyp)
    REFERENCES requirement_type(idreqtyp)
;

ALTER TABLE requirement ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE requirement ADD 
    FOREIGN KEY (idparentreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: requirement_payment 
--

ALTER TABLE requirement_payment ADD 
    FOREIGN KEY (payments_id)
    REFERENCES payment_application(idpayapp)
;

ALTER TABLE requirement_payment ADD 
    FOREIGN KEY (requirements_id)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: requirement_role 
--

ALTER TABLE requirement_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE requirement_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE requirement_role ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: requirement_status 
--

ALTER TABLE requirement_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE requirement_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE requirement_status ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: return_purchase_order 
--

ALTER TABLE return_purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
;


-- 
-- TABLE: return_sales_order 
--

ALTER TABLE return_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
;


-- 
-- TABLE: rule_sales_discount 
--

ALTER TABLE rule_sales_discount ADD 
    FOREIGN KEY (name)
    REFERENCES jhi_authority(name)
;

ALTER TABLE rule_sales_discount ADD 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
;


-- 
-- TABLE: rules 
--

ALTER TABLE rules ADD 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
;


-- 
-- TABLE: sale_type 
--

ALTER TABLE sale_type ADD 
    FOREIGN KEY (idparent)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: sales_agreement 
--

ALTER TABLE sales_agreement ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE sales_agreement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE sales_agreement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idparrol)
;


-- 
-- TABLE: sales_broker 
--

ALTER TABLE sales_broker ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;

ALTER TABLE sales_broker ADD 
    FOREIGN KEY (idbrotyp)
    REFERENCES broker_type(idbrotyp)
;


-- 
-- TABLE: sales_order 
--

ALTER TABLE sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
;

ALTER TABLE sales_order ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: sales_point 
--

ALTER TABLE sales_point ADD 
    FOREIGN KEY (idparrol)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: sales_unit_leasing 
--

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
;

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
;

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idorder)
    REFERENCES vehicle_sales_order(idorder)
;

ALTER TABLE sales_unit_leasing ADD 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
;


-- 
-- TABLE: sales_unit_requirement 
--

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idparrol)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
;

ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
;


-- 
-- TABLE: salesman 
--

ALTER TABLE salesman ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;

ALTER TABLE salesman ADD 
    FOREIGN KEY (idcoordinator)
    REFERENCES salesman(idparrol)
;


-- 
-- TABLE: service 
--

ALTER TABLE service ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE service ADD 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: service_agreement 
--

ALTER TABLE service_agreement ADD 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
;

ALTER TABLE service_agreement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE service_agreement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idparrol)
;


-- 
-- TABLE: service_per_motor 
--

ALTER TABLE service_per_motor ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;


-- 
-- TABLE: service_reminder 
--

ALTER TABLE service_reminder ADD 
    FOREIGN KEY (idclaimtype)
    REFERENCES dealer_claim_type(idclaimtype)
;

ALTER TABLE service_reminder ADD 
    FOREIGN KEY (idremindertype)
    REFERENCES dealer_reminder_type(idremindertype)
;


-- 
-- TABLE: ship_to 
--

ALTER TABLE ship_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: shipment 
--

ALTER TABLE shipment ADD 
    FOREIGN KEY (idshityp)
    REFERENCES shipment_type(idshityp)
;

ALTER TABLE shipment ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE shipment ADD 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idparrol)
;

ALTER TABLE shipment ADD 
    FOREIGN KEY (idshito)
    REFERENCES ship_to(idparrol)
;

ALTER TABLE shipment ADD 
    FOREIGN KEY (idposaddfro)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE shipment ADD 
    FOREIGN KEY (idposaddto)
    REFERENCES postal_address(idcontact)
;


-- 
-- TABLE: shipment_incoming 
--

ALTER TABLE shipment_incoming ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: shipment_item 
--

ALTER TABLE shipment_item ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE shipment_item ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE shipment_item ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: shipment_item_order_item 
--

ALTER TABLE shipment_item_order_item ADD 
    FOREIGN KEY (order_items_id)
    REFERENCES order_item(idordite)
;

ALTER TABLE shipment_item_order_item ADD 
    FOREIGN KEY (shipment_items_id)
    REFERENCES shipment_item(idshiite)
;


-- 
-- TABLE: shipment_outgoing 
--

ALTER TABLE shipment_outgoing ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: shipment_package 
--

ALTER TABLE shipment_package ADD 
    FOREIGN KEY (idshipactyp)
    REFERENCES shipment_package_type(idshipactyp)
;

ALTER TABLE shipment_package ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: shipment_package_role 
--

ALTER TABLE shipment_package_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE shipment_package_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE shipment_package_role ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;


-- 
-- TABLE: shipment_package_status 
--

ALTER TABLE shipment_package_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE shipment_package_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE shipment_package_status ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;


-- 
-- TABLE: shipment_receipt 
--

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
;

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
;

ALTER TABLE shipment_receipt ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;


-- 
-- TABLE: shipment_receipt_role 
--

ALTER TABLE shipment_receipt_role ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;

ALTER TABLE shipment_receipt_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE shipment_receipt_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;


-- 
-- TABLE: shipment_receipt_shipment_item 
--

ALTER TABLE shipment_receipt_shipment_item ADD 
    FOREIGN KEY (shipment_items_id)
    REFERENCES shipment_item(idshiite)
;

ALTER TABLE shipment_receipt_shipment_item ADD 
    FOREIGN KEY (shipment_receipts_id)
    REFERENCES shipment_receipt(idreceipt)
;


-- 
-- TABLE: shipment_receipt_status 
--

ALTER TABLE shipment_receipt_status ADD 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
;

ALTER TABLE shipment_receipt_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE shipment_receipt_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;


-- 
-- TABLE: shipment_status 
--

ALTER TABLE shipment_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE shipment_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE shipment_status ADD 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
;


-- 
-- TABLE: standard_calendar 
--

ALTER TABLE standard_calendar ADD 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
;

ALTER TABLE standard_calendar ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;


-- 
-- TABLE: suspect 
--

ALTER TABLE suspect ADD 
    FOREIGN KEY (iddealer)
    REFERENCES internal(idparrol)
;

ALTER TABLE suspect ADD 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
;

ALTER TABLE suspect ADD 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
;

ALTER TABLE suspect ADD 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
;

ALTER TABLE suspect ADD 
    FOREIGN KEY (idaddress)
    REFERENCES postal_address(idcontact)
;

ALTER TABLE suspect ADD 
    FOREIGN KEY (idsuspecttyp)
    REFERENCES suspect_type(idsuspecttyp)
;


-- 
-- TABLE: suspect_role 
--

ALTER TABLE suspect_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE suspect_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE suspect_role ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: suspect_status 
--

ALTER TABLE suspect_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE suspect_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE suspect_status ADD 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
;


-- 
-- TABLE: telecomunication_number 
--

ALTER TABLE telecomunication_number ADD 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
;


-- 
-- TABLE: unit_accesories_mapper 
--

ALTER TABLE unit_accesories_mapper ADD 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: unit_deliverable 
--

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (iddeltype)
    REFERENCES deliverable_type(iddeltype)
;

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_document_requirement(idreq)
;

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (idvndstatyp)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE unit_deliverable ADD 
    FOREIGN KEY (idconstatyp)
    REFERENCES status_type(idstatustype)
;


-- 
-- TABLE: unit_requirement 
--

ALTER TABLE unit_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE unit_requirement ADD 
    FOREIGN KEY (idbranch)
    REFERENCES branch(idparrol)
;

ALTER TABLE unit_requirement ADD 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
;


-- 
-- TABLE: uom 
--

ALTER TABLE uom ADD 
    FOREIGN KEY (iduomtyp)
    REFERENCES uom_type(iduomtyp)
;


-- 
-- TABLE: uom_conversion 
--

ALTER TABLE uom_conversion ADD 
    FOREIGN KEY (iduomto)
    REFERENCES uom(iduom)
;

ALTER TABLE uom_conversion ADD 
    FOREIGN KEY (iduomfro)
    REFERENCES uom(iduom)
;


-- 
-- TABLE: vehicle 
--

ALTER TABLE vehicle ADD 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
;

ALTER TABLE vehicle ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE vehicle ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: vehicle_document_requirement 
--

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idslsreq)
    REFERENCES sales_unit_requirement(idreq)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idvehregtyp)
    REFERENCES vehicle_registration_type(idvehregtyp)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idparrol)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idparrol)
;

ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idparrol)
;


-- 
-- TABLE: vehicle_identification 
--

ALTER TABLE vehicle_identification ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;

ALTER TABLE vehicle_identification ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: vehicle_purchase_order 
--

ALTER TABLE vehicle_purchase_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
;


-- 
-- TABLE: vehicle_sales_billing 
--

ALTER TABLE vehicle_sales_billing ADD 
    FOREIGN KEY (idbilling)
    REFERENCES billing_receipt(idbilling)
;

ALTER TABLE vehicle_sales_billing ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;

ALTER TABLE vehicle_sales_billing ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;


-- 
-- TABLE: vehicle_sales_order 
--

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
;

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idunitreq)
    REFERENCES sales_unit_requirement(idreq)
;

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
;

ALTER TABLE vehicle_sales_order ADD 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
;


-- 
-- TABLE: vehicle_work_requirement 
--

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
;

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idmechanic)
    REFERENCES mechanic(idparrol)
;

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
;

ALTER TABLE vehicle_work_requirement ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idparrol)
;


-- 
-- TABLE: vendor 
--

ALTER TABLE vendor ADD 
    FOREIGN KEY (idvndtyp)
    REFERENCES vendor_type(idvndtyp)
;

ALTER TABLE vendor ADD 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
;


-- 
-- TABLE: vendor_bill_to 
--

ALTER TABLE vendor_bill_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES bill_to(idparrol)
;


-- 
-- TABLE: vendor_order 
--

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idparrol)
;

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
;

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE vendor_order ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idparrol)
;


-- 
-- TABLE: vendor_product 
--

ALTER TABLE vendor_product ADD 
    FOREIGN KEY (idparrol)
    REFERENCES internal(idparrol)
;

ALTER TABLE vendor_product ADD 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
;

ALTER TABLE vendor_product ADD 
    FOREIGN KEY (idparrol)
    REFERENCES vendor(idparrol)
;


-- 
-- TABLE: vendor_relationship 
--

ALTER TABLE vendor_relationship ADD 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
;

ALTER TABLE vendor_relationship ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE vendor_relationship ADD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idparrol)
;


-- 
-- TABLE: vendor_ship_to 
--

ALTER TABLE vendor_ship_to ADD 
    FOREIGN KEY (idparrol)
    REFERENCES ship_to(idparrol)
;


-- 
-- TABLE: village 
--

ALTER TABLE village ADD 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
;


-- 
-- TABLE: we_service_type 
--

ALTER TABLE we_service_type ADD 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
;

ALTER TABLE we_service_type ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;


-- 
-- TABLE: we_type_good_standard 
--

ALTER TABLE we_type_good_standard ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;

ALTER TABLE we_type_good_standard ADD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
;


-- 
-- TABLE: work_effort 
--

ALTER TABLE work_effort ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
;

ALTER TABLE work_effort ADD 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
;


-- 
-- TABLE: work_effort_payment 
--

ALTER TABLE work_effort_payment ADD 
    FOREIGN KEY (work_efforts_id)
    REFERENCES work_effort(idwe)
;

ALTER TABLE work_effort_payment ADD 
    FOREIGN KEY (payments_id)
    REFERENCES payment_application(idpayapp)
;


-- 
-- TABLE: work_effort_role 
--

ALTER TABLE work_effort_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE work_effort_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE work_effort_role ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
;


-- 
-- TABLE: work_effort_status 
--

ALTER TABLE work_effort_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE work_effort_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE work_effort_status ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
;


-- 
-- TABLE: work_order 
--

ALTER TABLE work_order ADD 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_work_requirement(idreq)
;


-- 
-- TABLE: work_order_booking 
--

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
;

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idcustomer)
    REFERENCES personal_customer(idparrol)
;

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
;

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idparrol)
;

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idbooslo)
    REFERENCES booking_slot(idbooslo)
;

ALTER TABLE work_order_booking ADD 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
;


-- 
-- TABLE: work_order_booking_role 
--

ALTER TABLE work_order_booking_role ADD 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
;

ALTER TABLE work_order_booking_role ADD 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
;

ALTER TABLE work_order_booking_role ADD 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
;


-- 
-- TABLE: work_order_booking_status 
--

ALTER TABLE work_order_booking_status ADD 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
;

ALTER TABLE work_order_booking_status ADD 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
;

ALTER TABLE work_order_booking_status ADD 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
;


-- 
-- TABLE: work_product_req 
--

ALTER TABLE work_product_req ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: work_requirement 
--

ALTER TABLE work_requirement ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
;


-- 
-- TABLE: work_requirement_services 
--

ALTER TABLE work_requirement_services ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_service_requirement(idwe)
;

ALTER TABLE work_requirement_services ADD 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
;


-- 
-- TABLE: work_service_requirement 
--

ALTER TABLE work_service_requirement ADD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
;


