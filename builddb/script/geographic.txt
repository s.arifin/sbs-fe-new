select concat("buildProvince(", a.idgeoboutype, ", '", a.geocode, ", '", a.description, "');") rslt
from geo_boundary a left join geo_boundary b on (a.idparent = b.idgeobou)
where a.idgeoboutype = 10;

select concat("buildCity(", a.idgeoboutype, ", '", b.geocode, ", '", a.geocode, ", '", a.description, "');") rslt
from geo_boundary a left join geo_boundary b on (a.idparent = b.idgeobou)
where a.idgeoboutype = 11 order by a.geocode;

select concat("buildDistrict(", a.idgeoboutype, ", '", b.geocode, ", '", a.geocode, ", '", a.description, "');") rslt
from geo_boundary a inner join geo_boundary b on (a.idparent = b.idgeobou)
where a.idgeoboutype = 12 order by a.geocode;

select concat("buildVillage(", a.idgeoboutype, ", '", b.geocode, ", '", a.geocode, ", '", a.description, "');") rslt
from geo_boundary a inner join geo_boundary b on (a.idparent = b.idgeobou)
where a.idgeoboutype = 13 order by a.geocode;
