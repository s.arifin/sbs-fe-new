import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import {HttpClient} from '@angular/common/http';
import { JobordersService } from '../entities/joborders/joborders.service';
import { Account, LoginModalService, Principal, ResponseWrapper } from '../shared';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    ipaddres: any;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private http: HttpClient,
        protected jobOrderService: JobordersService,

    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.http.get('https://api.ipify.org/?format=json').subscribe((res: any) => {
                        this.ipaddres = res.ip;
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
                if ( this.account === undefined ) {
                    console.log('belum login');
                } else {
                    const user_login = this.principal.getUserLogin();
                    const obj = {
                        login : user_login,
                        ip_address : this.ipaddres,
                    }
                    this.jobOrderService.createNew1(obj).subscribe(
                        (res: ResponseWrapper) =>
                            this.onSaveSuccess(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    )
                }
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
    hris() {
        window.location.href = 'https://hris.sindegroup.com/';
    }

    private onSaveSuccess(data, headers) {
    }

    private onSaveError(error) {
    }

    private onError(error) {
    }
}
