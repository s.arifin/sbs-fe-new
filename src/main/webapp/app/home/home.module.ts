import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild([ HOME_ROUTE ]),
        HttpClientModule
    ],
    declarations: [
        HomeComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmHomeModule {}
