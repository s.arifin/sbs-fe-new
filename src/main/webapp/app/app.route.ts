import { Routes } from '@angular/router';

import { NavbarComponent } from './layouts';

export const navbarRoute: Routes = [
  {
    path: '',
    component: NavbarComponent,
    outlet: 'navbar',
    data: {
      pageTitle: 'global.menu.home'
    }
  },
  {
    path: 'purchase-order',
    loadChildren: './entities/purchase-order/purchase-order.module#MpmPurchaseOrderModule'
  },
];
