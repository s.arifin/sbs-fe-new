import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Purchasing, PurchasingService, Valuta } from '../purchasing';
import { Invoice, InvoiceService, Pobbmlov } from '../invoice';
import { Account, Principal, ToasterService } from '../../shared';
import { TandaTerimaService } from '../tanda-terima';
import { DetailTandaTerima } from '../detail-tanda-terima';
import { LoadingService } from '../../layouts';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterSupplierService, MasterSupplier } from '../master-supplier';
import { InvoiceItem } from '../invoice-item';
import { Payment } from './payment.model';
import { PaymentItem } from '../payment-item';
import { PaymentService } from './payment.service';

@Component({
    selector: 'jhi-payment-new-pbl',
    templateUrl: './payment-new-pbl.component.html'
})
export class PaymentNewPBLComponent implements OnInit, OnDestroy {

    pay: Payment;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    payItems: PaymentItem[];
    payItem: PaymentItem;
    eventSubscriber: Subscription;
    checkPPN: boolean;
    isview: boolean;
    subscription: Subscription;
    dtDue: Date;
    filter: Purchasing[];
    invNO: string;
    filteredValuta: any[];
    newValuta: Valuta;
    currentAccount: Account;
    eventSubscriberPPLOV: Subscription;
    invoiceNumber: string
    isDiscPersenAtas: boolean;
    isDiscPersenBawah: boolean;
    nopo: string;
    ptaxinvno: string;
    pembulatan1: boolean;
    jnspembulatan1: boolean;
    lblpembualatan1: string;
    lbljnspembulatan1: string;
    saveBool: boolean;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private paymentService: PaymentService,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private principal: Principal,
        private tandaTerimaService: TandaTerimaService,
        private loadingService: LoadingService,
    ) {
        this.pay = new Payment();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.payItems = new Array<PaymentItem>();
        this.payItem = new PaymentItem;
        this.checkPPN = false;
        this.eventSubscriber = new Subscription();
        this.isview = false;
        this.dtDue = new Date();
        this.filter = new Array<Purchasing>();
        this.invNO = '';
        this.newValuta = new Valuta();
        this.currentAccount = new Account();
        this.invoiceNumber = '';
        this.isDiscPersenAtas = false;
        this.isDiscPersenBawah = false;
        this.nopo = '';
        this.ptaxinvno = '';
        this.pembulatan1 = false;
        this.lblpembualatan1 = 'Tidak';
        this.jnspembulatan1 = false;
        this.lbljnspembulatan1 = 'Bawah';
        this.saveBool = false;
    }
    ngOnInit() {
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            console.log('currentAccount == ', this.currentAccount);
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.saveBool = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.saveBool = false;
                    this.load(params['id']);
                }
            }
        });
        this.registerFromPaymentLOV();
        this.registerTandaTerima();
    }
    load(id) {
        this.paymentService.find(id).subscribe(
            (res: Payment) => {
                this.pay = res;
                this.payItems = res.payment_items;
                this.dtDue = new Date(res.dtdue);
                this.invNO = res.payment_items[0].proformainv;
                this.nopo = res.payment_items[0].nopo;
                this.ptaxinvno = res.payment_items[0].invtaxno;
                if (res.payment_items[0].proformainv === null || res.payment_items[0].proformainv === '') {
                    this.invNO = res.receiptno;
                }
                this.invoiceNumber = res.payment_items[0].invno;
                this.masterSupplierService.findByCode(res.payto).subscribe(
                    (resS: MasterSupplier) => this.newSuplier = resS
                );
                this.filterValutaSingleLoad(res.valuta);
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.currentAccount.idInternal).then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.currentAccount.idInternal).then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.pay.payto = this.newSuplier.kd_suppnew;
        this.pay.paytoname = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
    }

    createPay() {
        this.isview = true;
        this.saveBool = true;
        this.loadingService.loadingStart();
        // this.pay.paytype = 6;
        if (this.currentAccount.idInternal === 'MKT') {
            this.pay.paytype = 6;
        }
        if (this.currentAccount.idInternal === 'FIN') {
            this.pay.paytype = 7;
        }
        if (this.currentAccount.idInternal === 'PBL') {
            this.pay.paytype = 8;
        }
        this.pay.dtdue = this.dtDue;
        this.pay.receiptno = this.invNO;
        this.pay.payment_items.forEach(
            (e) => {
                e.proformainv = this.invNO;
            }
        );
        // if (this.pay.payto === null || this.pay.payto === undefined || this.pay.payto === '') {
        //     alert('Supplier Tidak Boleh Kosong');
        // } else
        if (this.pay.dtdue === null || this.pay.dtdue === undefined) {
            alert('Tanggal Jatuh Tempo Tidak Boleh Kosong');
            this.isview = false;
            this.saveBool = false;
            this.loadingService.loadingStop();
        } else if (this.pay.valuta === null || this.pay.valuta === undefined || this.pay.valuta === '') {
            alert('Valuta Wajib Di Isi');
            this.isview = false;
            this.saveBool = false;
            this.loadingService.loadingStop();
        } else {
            if (this.pay.idpay === undefined) {
                const item = new PaymentItem();
                item.invno = this.invoiceNumber;
                item.invtaxno = this.ptaxinvno;
                this.pay.payment_items.forEach(
                    (e) => {
                        e.invno = this.invoiceNumber;
                        e.invtaxno = this.ptaxinvno;
                    }
                );
                if (this.payItems.length <= 0) {
                    this.payItems = [... this.payItems, item];
                }
                this.pay.payment_items = this.payItems;
                this.paymentService.create(this.pay).subscribe(
                    (res) => {
                        this.isview = false;
                        this.saveBool = false;
                        this.loadingService.loadingStop();
                        this.backMainPage();
                    }
                )
            } else if (this.pay.idpay !== undefined) {
                this.pay.payment_items.forEach(
                    (e) => {
                        e.invno = this.invoiceNumber;
                        e.invtaxno = this.ptaxinvno;
                    }
                );
                // this.payItems = [... this.payItems, new PaymentItem()];
                this.pay.payment_items = this.payItems;
                this.paymentService.update(this.pay).subscribe(
                    (res) => {
                        this.isview = false;
                        this.saveBool = false;
                        this.loadingService.loadingStop();
                        this.backMainPage();
                    }
                )
            }
        }
    }

    backMainPage() {
        // this.router.navigate(['/payment']);
        window.history.back();
    }
    registerFromPaymentLOV() {
        this.eventSubscriber = this.eventManager.subscribe('POLovModification', () => this.regGetInvoice());
    }
    regGetInvoice(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.purchasingService.values.subscribe(
                    (res: Purchasing[]) => {
                        this.nopo = res[0].nopurchasing;
                        // if (this.pay.valuta !== undefined) {
                        //     if (this.pay.valuta.toLowerCase() !== res[0].valuta.toLowerCase()) {
                        //         this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                        //         return;
                        //     }
                        // }
                        // if (this.pay.valuta === undefined) {
                        //     this.pay.valuta = res[0].valuta;
                        // }
                        // if (this.newSuplier.kd_suppnew === undefined) {
                        //     console.log('get data values INVOICE == ', res);
                        //     console.log('get data values INVOICE == ', res);
                        //     res.forEach((e) => {
                        //         const _idinv = e.idpurchasing;
                        //         // const _invno = e.nopurchasing;
                        //         this.invNO = e.nopurchasing;
                        //         const _grandtotal = e.totalprice;
                        //         this.filter = this.payItems.filter(
                        //             function dataSama(items) {
                        //                 return (
                        //                     items.idinvoice === _idinv &&
                        //                     items.invno === this.invNO &&
                        //                     items.totalpay === _grandtotal
                        //                 )
                        //             }
                        //         );
                        //         if (this.filter.length <= 0) {
                        //             this.payItem = new PaymentItem();
                        //             this.payItem.invno = e.nopurchasing;
                        //             this.payItem.idinvoice = e.idpurchasing;
                        //             this.payItem.totalpay = e.totalprice;
                        //             this.payItems = [... this.payItems, this.payItem];
                        //         }
                        //     });
                        //     this.getSuplierData(res[0].supliercode);
                        // }
                        // if (this.newSuplier.kd_suppnew !== undefined) {
                        //     if (this.newSuplier.kd_suppnew.toLowerCase() === res[0].supliercode.toLowerCase()) {
                        //         console.log('get data values INVOICE == ', res);
                        //         res.forEach((e) => {
                        //             const _idinv = e.idpurchasing;
                        //             this.invNO = e.nopurchasing;
                        //             const _grandtotal = e.totalprice;
                        //             this.filter = this.payItems.filter(
                        //                 function dataSama(items) {
                        //                     return (
                        //                         items.idinvoice === _idinv &&
                        //                         items.invno === this.invNO &&
                        //                         items.totalpay === _grandtotal
                        //                     )
                        //                 }
                        //             );
                        //             if (this.filter.length <= 0) {
                        //                 this.payItem = new PaymentItem();
                        //                 this.payItem.invno = e.nopurchasing;
                        //                 this.payItem.idinvoice = e.idpurchasing;
                        //                 this.payItem.totalpay = e.totalprice;
                        //                 this.payItems = [... this.payItems, this.payItem];
                        //             }
                        //         });
                        //     }
                        //     if (res[0].supliercode.toLocaleLowerCase() !== this.newSuplier.kd_suppnew.toLocaleLowerCase()) {
                        //         this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
                        //     }
                        // }
                    });
                this.eventSubscriber.unsubscribe();
                resolvedt();
            }
        )
    }
    countPrice(): Promise<void> {
        return new Promise<void>(
            (resolveCP) => {
                console.log('cek data masuk hitung == ');
                // if (this.pay.totalpay === undefined || this.pay.totalpay === null) {
                this.pay.totalpay = 0;
                // }
                this.payItems.forEach(
                    (e) => {
                        this.pay.subtotal += e.totalpay;
                    }
                )
                resolveCP();
            }
        )
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
                this.pay.payto = this.newSuplier.kd_suppnew;
                this.pay.paytoname = this.newSuplier.nmSupplier;
            }
        )
    }
    countPPH() {
        if (this.pay.totalpay === undefined) {
            this.pay.totalpay = 0;
        }
        if (this.pay.pph === undefined) {
            this.pay.pph = 0;
        }
        if (this.pembulatan1 === false) {
            this.jnspembulatan1 = false
            this.lblpembualatan1 = 'Tidak';
        }
        if (this.pembulatan1 === true) {
            this.lblpembualatan1 = 'Ya';
        }
        if (this.jnspembulatan1 === false) {
            this.lbljnspembulatan1 = 'Bawah';
        }
        if (this.jnspembulatan1 === true) {
            this.lbljnspembulatan1 = 'Atas';
        }
        if (this.pay.sebelum_pph === undefined) {
            this.pay.sebelum_pph = 0;
        }

        if (this.pay.persen_pph === undefined) {
            this.pay.persen_pph = 0;
        }
        if (this.pembulatan1 === false) {
            this.pay.pph = this.pay.sebelum_pph * (this.pay.persen_pph / 100);
        }
        if (this.pembulatan1 === true) {
            if (this.jnspembulatan1 === false) {
                this.pay.pph = Math.floor(this.pay.sebelum_pph * (this.pay.persen_pph / 100)); // - _cutcost;
            }
            if (this.jnspembulatan1 === true) {
                this.pay.pph = Math.ceil(this.pay.sebelum_pph * (this.pay.persen_pph / 100)); // - _cutcost;
            }
        }
        const _topay = this.pay.subtotal;
        const _pph = this.pay.pph;
        const _biaya_lain_lain = this.pay.additionalcosts;
        if (this.pembulatan1 === false) {
            if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === true) {
                this.pay.ppn = 0;
                this.pay.totalpay = _topay - _pph + _biaya_lain_lain;
            }
            if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === false) {
                const _ppn = Math.ceil(_topay * 11 / 100);
                this.pay.ppn = _ppn;
                this.pay.totalpay = _topay + _ppn - _pph + _biaya_lain_lain;
            }
            if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === true) {
                const _ppn = Math.floor(_topay * 11 / 100);
                this.pay.ppn = _ppn;
                this.pay.totalpay = _topay + _ppn - _pph + _biaya_lain_lain;
            }
            if (this.isDiscPersenBawah === false && this.isDiscPersenAtas === false) {
                this.pay.ppn = 0;
                this.pay.totalpay = _topay - _pph + _biaya_lain_lain;
            }
        }

        if (this.pembulatan1 === true) {
            if (this.jnspembulatan1 === false) {
                if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === true) {
                    this.pay.ppn = 0;
                    this.pay.totalpay = Math.floor(_topay - _pph + _biaya_lain_lain);
                }
                if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === false) {
                    const _ppn = Math.ceil(_topay * 11 / 100);
                    this.pay.ppn = _ppn;
                    this.pay.totalpay = Math.floor(_topay + _ppn - _pph + _biaya_lain_lain);
                }
                if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === true) {
                    const _ppn = Math.floor(_topay * 11 / 100);
                    this.pay.ppn = _ppn;
                    this.pay.totalpay = Math.floor(_topay + _ppn - _pph + _biaya_lain_lain);
                }
                if (this.isDiscPersenBawah === false && this.isDiscPersenAtas === false) {
                    this.pay.ppn = 0;
                    this.pay.totalpay = Math.floor(_topay - _pph + _biaya_lain_lain);
                }
            }
            if (this.jnspembulatan1 === true) {
                if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === true) {
                    this.pay.ppn = 0;
                    this.pay.totalpay = Math.ceil(_topay - _pph + _biaya_lain_lain);
                }
                if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === false) {
                    const _ppn = Math.ceil(_topay * 11 / 100);
                    this.pay.ppn = _ppn;
                    this.pay.totalpay = Math.ceil(_topay + _ppn - _pph + _biaya_lain_lain);
                }
                if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === true) {
                    const _ppn = Math.floor(_topay * 11 / 100);
                    this.pay.ppn = _ppn;
                    this.pay.totalpay = Math.ceil(_topay + _ppn - _pph + _biaya_lain_lain);
                }
                if (this.isDiscPersenBawah === false && this.isDiscPersenAtas === false) {
                    this.pay.ppn = 0;
                    this.pay.totalpay = Math.ceil(_topay - _pph + _biaya_lain_lain);
                }
            }
        }

        if (this.pay.pph > 0) {
            this.pay.notepph = this.pay.persen_pph + '% X ' + 'Rp. ' + this.numberWithCommas(this.pay.sebelum_pph);
        } else {
            this.pay.notepph = '';
        }

    }
    filterValutaSingle(event) {
        const query = event.query;
        this.purchasingService.getAllValuta().then((valuta) => {
            this.filteredValuta = this.filterValuta(query, valuta);
        });
    }

    filterValuta(query, valuta: Valuta[], isEdit: Boolean = false): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < valuta.length; i++) {
            const valutas = valuta[i];
            if (valutas.valutacode.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(valutas);
                if (isEdit === true) {
                    this.newValuta = valutas;
                }
            }
        }
        return filtered;
    }

    public selectValuta(isSelect?: boolean): void {
        this.pay.valuta = this.newValuta.valutacode;
    }
    registerTandaTerima() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('TandaTerimaModified',
            () => {
                this.regTT();
            });
    }
    regTT(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.tandaTerimaService.values.subscribe(
                    (res: DetailTandaTerima) => {
                        console.log('Cek data detail Tanda Terima == ', res);
                        this.invNO = res.receiptno;
                        this.invoiceNumber = res.invno;
                        this.ptaxinvno = res.invtaxno;
                        this.masterSupplierService.findByCode(res.kdsupplier).subscribe(
                            (resSPL: MasterSupplier) => {
                                this.newSuplier = resSPL;
                                this.pay.payto = resSPL.kd_suppnew;
                                this.pay.paytoname = resSPL.nmSupplier;
                                this.pay.subtotal = res.total;
                                this.pay.additionalcosts = 0;
                                this.pay.totalpay = res.total + this.pay.additionalcosts;
                            }
                        )
                    }
                );
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }
    filterValutaSingleLoad(event) {
        const query = event;
        this.purchasingService.getAllValuta().then((valuta) => {
            this.filteredValuta = this.filterValuta(query, valuta, true);
        });
    }

    savenopo() {
        this.loadingService.loadingStart();
        this.paymentService.item_set_po(this.nopo, this.pay.idpay).subscribe(
            (res) => {
                this.backMainPage();
                this.loadingService.loadingStop();
            },
            (err) => {
                this.loadingService.loadingStop();
            }
        );
    }

    numberWithCommas(x) {
        return x.toLocaleString('en-US');
    }

}
