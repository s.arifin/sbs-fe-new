import { PaymentItem } from '../payment-item';
import { Retur } from '../retur';
// import { Retur } from '../retur';
import { BaseEntity } from './../../shared';
import { PaymentStatus } from './payment-status.model';

export class Payment implements BaseEntity {
    constructor(
        public id?: number,
        public idpay?: any,
        public nopay?: string,
        public payfrom?: string,
        public payto?: string,
        public paytoname?: string,
        public paytype?: number,
        public dtcreate?: Date,
        public createby?: string,
        public dtmodified?: Date,
        public modifiedby?: string,
        public dtdue?: Date,
        public totalpay?: number,
        public note?: string,
        public dtdeptissueby?: Date,
        public dtdeptappby?: Date,
        public deptissueby?: string,
        public deptappby?: string,
        public dtdirector?: Date,
        public director?: string,
        public dtacccodeby?: Date,
        public dtaccappby?: Date,
        public acccodeby?: string,
        public accappby?: string,
        public dtfincheckby?: Date,
        public dtfinappby?: Date,
        public dtfinpassforpay?: Date,
        public fincheckby?: string,
        public finappby?: string,
        public finpassforpay?: string,
        public payment_items?: PaymentItem[],
        public statuses?: PaymentStatus,
        public notespl?: string,
        public valuta?: string,
        public notepph?: string,
        public pph?: number,
        public payment_type?: PaymentType,
        public subtotal?: number,
        public dtfin?: Date,
        public dtacc?: Date,
        public dtregfin?: Date,
        public dtregacc?: Date,
        public receiptno?: string,
        public dtpay?: Date,
        public dtrelease?: Date,
        public nogiro?: string,
        public stype?: string,
        public additionalcosts?: number,
        public additionalcostsnote?: string,
        public bankcode?: string,
        public totalpaid?: number,
        public kurspay?: number,
        public kursppn?: number,
        public cutcost?: number,
        public notecutcost?: string,
        public ppn?: number,
        public paynoppn?: number,
        public totalpaidppn?: number,
        public totalpaidnoppn?: number,
        public nopayfin?: string,
        public ishold?: number,
        public userhold?: string,
        public holdreason?: string,
        public jenis?: string,
        public totalppn?: number,
        public cancelnote?: string,
        public additionalcosts2?: number,
        public additionalcostsnote2?: string,
        public cekByrPPN?: boolean,
        public cekByrPokok?: boolean,
        public cekByrtagihan?: boolean,
        public invno?: string,
        public invtaxno?: string,
        public retur?: number,
        public notaretur?: Retur[],
        public fixrebate?: number,
        public persen_pph?: number,
        public sebelum_pph?: number,
        public no_um?: string,
        public is_cn?: string,
        public brand?: string,
        public produk?: string,
        public is_minus?: number,
        public is_plus?: number,
        public idreceipt?: any,
        public harga_produk?: number,
        // public harga_total_produk?: number
        public reverse_note?: string,
        public dtreverse?: Date,
        public mgr_approve?: string,
        public dtapprove_revisi?: Date,
        public is_pv?: string,
        public grand_total_produk?: number,
        public jenis_klaim?: string,
    ) {
        this.payment_items = new Array<PaymentItem>();
        this.statuses = new PaymentStatus();
        // tslint:disable-next-line: no-use-before-declare
        this.payment_type = new PaymentType();
        this.notaretur = new Array<Retur>();
    }
}

export class PaymentType {
    constructor(
        public id?: number,
        public description?: string,
    ) {

    }
}

export class PaymentReport {
    constructor(
        public notandaterima?: string,
        public bayarkepada?: string,
        public jumlahbayar?: number,
        public nogir?: string,
        public pembuat?: string,
        public tanggalbuat?: Date,
        public tanggalserahacc?: Date,
        public tanggalterimaacc?: Date,
        public tanggalserahfin?: Date,
        public tanggalterimafin?: Date,
        public tanggalbayar?: Date,
        public tanggalrelease?: Date,
        public tanggaltempo?: Date,
        public status?: string,
        public noinvoice?: string,
        public nofaktur?: string,
        public jumlahtagihan?: number,
        public valuta?: string,
        public ppn?: number,
        public bayarppn?: number,
        public pokok?: number,
        public bayarpokok?: number,
        public totaljumlahtagihan?: number,
        public totaljumlahbayar?: number,
        public totalppn?: number,
        public totalbayarppn?: number,
        public totalpokok?: number,
        public totalbayarpokok?: number,

        public totaljumlahtagihanrp?: number,
        public totaljumlahbayarrp?: number,
    ) {
    }
}

export class FileAttachment {
    constructor(
        public idfile?: any,
        public no_bbm?: string,
        public filename?: string,
        public filedata?: any,
    ) { }
}

export class PaymentReportVM2 {
    constructor(
        public idpayment?: any,
        public valuta?: string,
        public nott?: string,
        public payto?: string,
        public paytoname?: string,
        public tagihan?: number,
        public bayar?: number,
        public kurang?: number,
        public tagihanppn?: number,
        public bayarppn?: number,
        public kurangppn?: number,
        public tagihanpokok?: number,
        public bayarpokok?: number,
        public kurangpokok?: number,
        public dtcreate?: number,
        public tagihanrp?: number,
        public bayarrp?: number,
        public kurangrp?: number,
        public additionalcosts2?: number,
        public additionalcostsnote2?: string,
        public dtpay?: Date,
        public diff?: number,
        public dtdue?: Date,
        public invno?: string,
        public taxno?: string,
        public diff2?: number,
        public dttandaterima?: Date
    ) { }
}
export class Listpaypo {
    constructor(
        public nopo?: string,
        public uangmuka?: number,
        public downpayment?: number,
        public totalbayar?: number,
        public sisapo?: number,
        public totalpo?: number,
        public dtcreate?: Date,
        public tanda_terima?: string,
        public pelunasan?: number,
        public productcode?: string,
        public nobbm?: string,
        public dtpay?: Date,
        public kasnegara?: number,
        public ppjk?: number,
    ) { }
}
