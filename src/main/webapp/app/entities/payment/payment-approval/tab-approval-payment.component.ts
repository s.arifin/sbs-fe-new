import { Component, OnInit, OnDestroy } from '@angular/core';
import { AccountService, Account } from '../../../shared';

@Component({
    selector: 'jhi-tab-approval-payment',
    templateUrl: './tab-approval-payment.component.html'
})
export class TabApprovalPaymentComponent implements OnInit, OnDestroy {
    account: Account;
    accMgr: boolean;
    constructor(
        protected accountService: AccountService
    ) {
        this.account = new Account();
        this.accMgr = false;
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    ngOnInit(): void {
        // throw new Error('Method not implemented.');
        this.accountService.get().subscribe((res) => {
            this.account = res;
            if (this.account.authorities.indexOf('ROLE_MGR_PRC') > -1) {
                this.accMgr = true;
                console.log(this.accMgr)
            } else {
                this.accMgr = false;
                console.log(this.accMgr)
            }
        })
    }
}
