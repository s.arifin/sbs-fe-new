import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';
import { ToasterService, ResponseWrapper } from '../../shared';
import { MasterSupplierService } from '../master-supplier/master-supplier.service';
import { MasterSupplier } from '../master-supplier';
import { Purchasing, PurchasingService } from '../purchasing';
import { InvoiceService } from '../invoice';
import { Payment } from './payment.model';
import { PaymentItem } from '../payment-item';
import { PaymentService } from './payment.service';
import { PurchasingItem } from '../purchasing-item';
import { FileAttachment, Listpaypo } from '../payment';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-payment-new-uangmuka',
    templateUrl: './payment-new-uangmuka.component.html'
})
export class PaymentNewUangMukaComponent implements OnInit, OnDestroy {

    pay: Payment;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    payItems: PaymentItem[];
    payItem: PaymentItem;
    eventSubscriber: Subscription;
    checkPPN: boolean;
    isview: boolean;
    subscription: Subscription;
    dtDue: Date;
    filter: Purchasing[];
    invNO: string;
    noUM: string;
    pinvno: string;
    ptaxinvno: string;
    ispelunasan: Boolean;
    idinv: any;
    itemPurchasing: PurchasingItem[];
    pembulatan: boolean;
    jnspembulatan: boolean;
    lblpembualatan: string;
    lbljnspembulatan: string;
    listpaypo: Listpaypo[];
    saveBool: boolean;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private paymentService: PaymentService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
    ) {
        this.pay = new Payment();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.payItems = new Array<PaymentItem>();
        this.payItem = new PaymentItem;
        this.checkPPN = false;
        this.eventSubscriber = new Subscription();
        this.isview = false;
        this.dtDue = new Date();
        this.filter = new Array<Purchasing>();
        this.itemPurchasing = new Array<PurchasingItem>();
        this.invNO = '';
        this.pinvno = '';
        this.ptaxinvno = '';
        this.ispelunasan = false;
        this.idinv = null;
        this.noUM = 'XXXX/XX/XXX/XX/XXXX';
        this.pembulatan = false;
        this.lblpembualatan = 'Tidak';
        this.jnspembulatan = false;
        this.lbljnspembulatan = 'Bawah';
        this.listpaypo = new Array<Listpaypo>();
        this.saveBool = false;
    }
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.saveBool = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.saveBool = false;
                    this.load(params['id']);
                }
            }
        });
        this.registerFromPaymentLOV();
    }
    load(id) {
        this.paymentService.find(id).subscribe(
            (res: Payment) => {
                this.pay = res;
                this.payItems = res.payment_items;
                this.dtDue = new Date(res.dtdue);
                this.invNO = res.payment_items[0].proformainv;
                this.pinvno = res.payment_items[0].invno;
                this.ptaxinvno = res.payment_items[0].invtaxno;
                this.noUM = res.no_um;
                if (res.payment_items[0].umlunas === 1) {
                    this.ispelunasan = true;
                }
                this.masterSupplierService.findByCode(res.payto).subscribe(
                    (resS: MasterSupplier) => this.newSuplier = resS
                )

                this.paymentService.FindPOBYNo1({ query: 'nopo:' + this.payItems[0].proformainv + '|type_inv:' + 1 }).subscribe((resfind: ResponseWrapper) => {
                    resfind.json.forEach((e: Listpaypo) => {
                        this.listpaypo = [... this.listpaypo, e];
                    });
                })
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.pay.payto = this.newSuplier.kd_suppnew;
        this.pay.paytoname = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
    }

    createPay() {
        this.saveBool = true;
        this.pay.paytype = 1;
        this.pay.dtdue = this.dtDue;
        this.pay.payment_items = this.payItems;
        this.pay.payment_items.forEach(
            (e) => {
                e.proformainv = this.invNO;
            }
        );
        if (this.pay.payto === null || this.pay.payto === undefined || this.pay.payto === '') {
            alert('Supplier Tidak Boleh Kosong');
            this.saveBool = false;
        } else if (this.pay.dtdue === null || this.pay.dtdue === undefined) {
            alert('Tanggal Jatuh Tempo Tidak Boleh Kosong');
            this.saveBool = false;
        } else {
            if (this.pay.idpay === undefined) {
                this.paymentService.create(this.pay).subscribe(
                    (res) => {
                        this.saveBool = false;
                        this.backMainPage();
                    }
                )
            } else if (this.pay.idpay !== undefined) {
                this.paymentService.update(this.pay).subscribe(
                    (res) => {
                        this.saveBool = false;
                        this.backMainPage();
                    }
                )
            }
        }
    }

    backMainPage() {
        // this.router.navigate(['/payment']);
        window.history.back();
    }
    registerFromPaymentLOV() {
        this.eventSubscriber = this.eventManager.subscribe('POLovModification', () => this.regGetInvoice());
    }
    regGetInvoice(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.purchasingService.values.subscribe(
                    (res: Purchasing[]) => {
                        if (this.pay.valuta !== undefined) {
                            if (this.pay.valuta.toLowerCase() !== res[0].valuta.toLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                                return;
                            }
                        }
                        if (this.pay.note === undefined) {
                            this.pay.note = '';
                        }

                        // GET NOTE
                        this.paymentService.getItemName(res[0].idpurchasing).subscribe((res1) => {
                            const productNames: string[] = res1.json.map((item: any) => item.nmBahan.replace(/\n/g, ' '));

                            // Mengonversi array productNames menjadi string
                            const productNamesStr: string = productNames.join(', ');

                            this.pay.note = productNamesStr;
                        });
                        // GET NOTE
                        if (this.pay.valuta === undefined) {
                            this.pay.valuta = res[0].valuta;
                        }
                        if (this.newSuplier.kd_suppnew === undefined) {
                            console.log('get data values INVOICE == ', res);
                            console.log('get data values INVOICE == ', res);
                            res.forEach((e) => {
                                const _idinv = e.idpurchasing;
                                // const _invno = e.nopurchasing;
                                this.invNO = e.nopurchasing;
                                const _grandtotal = e.totalprice;
                                this.filter = this.payItems.filter(
                                    function dataSama(items) {
                                        return (
                                            items.idinvoice === _idinv &&
                                            items.invno === this.invNO &&
                                            items.totalpay === _grandtotal
                                        )
                                    }
                                );
                                if (this.filter.length <= 0) {
                                    this.payItem = new PaymentItem();
                                    this.payItem.invno = e.nopurchasing;
                                    this.payItem.idinvoice = e.idpurchasing;
                                    this.payItem.totalpay = e.totalprice;
                                    this.payItems = [... this.payItems, this.payItem];
                                }
                            });
                            this.getSuplierData(res[0].supliercode);
                        }
                        if (this.newSuplier.kd_suppnew !== undefined) {
                            if (this.newSuplier.kd_suppnew.toLowerCase() === res[0].supliercode.toLowerCase()) {
                                console.log('get data values INVOICE == ', res);
                                res.forEach((e) => {
                                    const _idinv = e.idpurchasing;
                                    this.invNO = e.nopurchasing;
                                    const _grandtotal = e.totalprice;
                                    this.filter = this.payItems.filter(
                                        function dataSama(items) {
                                            return (
                                                items.idinvoice === _idinv &&
                                                items.invno === this.invNO &&
                                                items.totalpay === _grandtotal
                                            )
                                        }
                                    );
                                    if (this.filter.length <= 0) {
                                        this.payItem = new PaymentItem();
                                        this.payItem.invno = e.nopurchasing;
                                        this.payItem.idinvoice = e.idpurchasing;
                                        this.payItem.totalpay = e.totalprice;
                                        this.payItems = [... this.payItems, this.payItem];
                                    }
                                });
                            }
                            if (res[0].supliercode.toLocaleLowerCase() !== this.newSuplier.kd_suppnew.toLocaleLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
                            }
                        }
                    });
                this.eventSubscriber.unsubscribe();
                resolvedt();
            }
        )
    }
    countPrice(): Promise<void> {
        return new Promise<void>(
            (resolveCP) => {
                console.log('cek data masuk hitung == ');
                // if (this.pay.totalpay === undefined || this.pay.totalpay === null) {
                this.pay.totalpay = 0;
                // }
                this.payItems.forEach(
                    (e) => {
                        this.pay.subtotal += e.totalpay;
                    }
                )
                resolveCP();
            }
        )
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
                this.pay.payto = this.newSuplier.kd_suppnew;
                this.pay.paytoname = this.newSuplier.nmSupplier;
            }
        )
    }
    countPPH() {
        if (this.pembulatan === false) {
            this.jnspembulatan = false
            this.lblpembualatan = 'Tidak';
        }
        if (this.pembulatan === true) {
            this.lblpembualatan = 'Ya';
        }
        if (this.jnspembulatan === false) {
            this.lbljnspembulatan = 'Bawah';
        }
        if (this.jnspembulatan === true) {
            this.lbljnspembulatan = 'Atas';
        }

        if (this.pay.totalpay === undefined) {
            this.pay.totalpay = 0;
        }
        if (this.pay.pph === undefined) {
            this.pay.pph = 0;
        }
        if (this.pay.sebelum_pph === undefined) {
            this.pay.sebelum_pph = 0;
        }
        if (this.pay.persen_pph === undefined) {
            this.pay.persen_pph = 0;
        }
        if (this.pembulatan === false) {
            this.pay.pph = this.pay.sebelum_pph * (this.pay.persen_pph / 100);
        }

        if (this.pembulatan === true) {
            if (this.jnspembulatan === false) {
                this.pay.pph = Math.floor(this.pay.sebelum_pph * (this.pay.persen_pph / 100));
            }
            if (this.jnspembulatan === true) {
                this.pay.pph = Math.ceil(this.pay.sebelum_pph * (this.pay.persen_pph / 100));
            }
        }
        const _topay = this.pay.subtotal;
        const _pph = this.pay.pph;
        this.pay.totalpay = _topay - _pph;
        if (this.pay.pph > 0) {
            this.pay.notepph = this.pay.persen_pph + '% X ' + 'Rp. ' + this.numberWithCommas(this.pay.sebelum_pph);
        } else {
            this.pay.notepph = '';
        }

    }

    lihatDetail(data: PaymentItem) {
        this.idinv = data.idinvoice;
        // console.log('ini idinvoice => ' + this.idinv);
    }

    numberWithCommas(x) {
        return x.toLocaleString('en-US');
    }

    deleteItem(invItem: PaymentItem) {
        if (invItem) {
            if (invItem.idpaydet !== null && invItem.idpaydet !== undefined) {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin Hapus Data ??!!',
                    accept: () => {
                        this.paymentService.deletebyid(invItem).subscribe((e) => { alert('Berhasil Hapus Data') });
                        this.payItems = new Array<PaymentItem>();
                        this.newSuplier = new MasterSupplier();
                        this.invNO = '';
                        this.pay.note = '';
                        this.pinvno = '';
                    }
                });
            } else {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin Hapus Data ??!!',
                    accept: () => {
                        this.payItems = new Array<PaymentItem>();
                        this.newSuplier = new MasterSupplier();
                        this.invNO = '';
                        this.pay.note = '';
                        this.pinvno = '';
                    }
                });
            }
        }
    }

}
