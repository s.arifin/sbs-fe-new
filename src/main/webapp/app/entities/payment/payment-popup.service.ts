import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Payment } from './payment.model';
import { PaymentService } from './payment.service';

@Injectable()
export class PaymentPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private paymentService: PaymentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.paymentService.find(id).subscribe((payment) => {
                    // payment.dtcreate = this.datePipe
                    //     .transform(payment.dtcreate, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtmodified = this.datePipe
                    //     .transform(payment.dtmodified, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtdue = this.datePipe
                    //     .transform(payment.dtdue, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtdeptissueby = this.datePipe
                    //     .transform(payment.dtdeptissueby, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtdeptappby = this.datePipe
                    //     .transform(payment.dtdeptappby, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtdirector = this.datePipe
                    //     .transform(payment.dtdirector, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtacccodeby = this.datePipe
                    //     .transform(payment.dtacccodeby, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtaccappby = this.datePipe
                    //     .transform(payment.dtaccappby, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtfincheckby = this.datePipe
                    //     .transform(payment.dtfincheckby, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtfinappby = this.datePipe
                    //     .transform(payment.dtfinappby, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtfinpassforpay = this.datePipe
                    //     .transform(payment.dtfinpassforpay, 'yyyy-MM-ddTHH:mm:ss');
                    // payment.dtreceipt = this.datePipe
                    //     .transform(payment.dtreceipt, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.paymentModalRef(component, payment);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.paymentModalRef(component, new Payment());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    paymentModalRef(component: Component, payment: Payment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.payment = payment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
