import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { TabApprovalAccPaymentComponent } from './payment-acc-approval/tab-approval-acc-payment.component';
import { TabPaymentAccComponent } from './payment-acc/tab-payment-acc.component';
import { TabApprovalFinPaymentComponent } from './payment-fin-approval/tab-approval-fin-payment.component';
import { PaymentFinBayarComponent } from './payment-fin/payment-fin-bayar.component';
import { TabPaymentFinComponent } from './payment-fin/tab-payment-fin.component';
import { TabPaymentComponent } from './tab-payment.component';
import { PaymentDetailComponent } from './payment-detail.component';
import { PaymentNewComponent } from './payment-new.component';
import { TabApprovalPaymentComponent } from './payment-approval/tab-approval-payment.component';
import { PaymentNewUangMukaComponent } from './payment-new-uangmuka.component';
import { PaymentPopupComponent } from './payment-dialog.component';
import { PaymentDeletePopupComponent } from './payment-delete-dialog.component';
import { PaymentNewMktComponent } from './payment-mkt/payment-new-mkt.component';
import { PaymentFinBPComponent } from './payment-fin/payment-fin-bp.component';
import { PaymentFinBRComponent } from './payment-fin/payment-fin-br.component';
import { PaymentFinJurnalComponent } from './payment-fin/payment-fin-jurnal.component';
import { PaymentFinBayarUSDComponent } from './payment-fin/payment-fin-bayar-usd.component';
// import { TabGabunganPaymentFinComponent } from './payment-fin/gabung-payment/tab-gabungan-payment-fin.component';
import { PaymentNewMktFixRebateComponent } from './payment-mkt/payment-new-mkt-fix-rebate.component';
import { PaymentNewFinComponent } from './payment-fin/payment-new-fin.component';
import { PaymentNewPBLComponent } from './payment-new-pbl.component';
import { PaymentNonMKTComponent } from './payment-non-mkt.component';
import { PaymentNewMktNonComponent } from './payment-mkt/payment-new-mkt-non.component';
@Injectable()
export class PaymentResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const paymentRoute: Routes = [
    {
        path: 'payment',
        component: TabPaymentComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'mkt-non-payment',
        component: PaymentNonMKTComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home1.title1'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment/:id',
        component: PaymentDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-new',
        component: PaymentNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit/:id/:isview',
        component: PaymentNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-payment',
        component: TabApprovalPaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-new-uangmuka',
        component: PaymentNewUangMukaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit-uangmuka/:id/:isview',
        component: PaymentNewUangMukaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-acc',
        component: TabPaymentAccComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-fin',
        component: TabPaymentFinComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-fin-bayar/:id/:isview',
        component: PaymentFinBayarComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-fin-bayar-usd/:id/:isview',
        component: PaymentFinBayarUSDComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-payment-acc',
        component: TabApprovalAccPaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-payment-fin',
        component: TabApprovalFinPaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-new-mkt',
        component: PaymentNewMktComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-new-mkt-non',
        component: PaymentNewMktNonComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-new-fin',
        component: PaymentNewFinComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-new-pbl',
        component: PaymentNewPBLComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit-mkt/:id/:isview',
        component: PaymentNewMktComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit-mkt-non/:id/:isview',
        component: PaymentNewMktNonComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit-fin/:id/:isview',
        component: PaymentNewFinComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit-pbl/:id/:isview',
        component: PaymentNewPBLComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-fin-bp/:id/:isview',
        component: PaymentFinBPComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-fin-bp',
        component: PaymentFinBPComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-fin-br/:id/:isview',
        component: PaymentFinBRComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-fin-br',
        component: PaymentFinBRComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-fin-jurnal',
        component: PaymentFinJurnalComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //        path: 'gabung-payment-fin',
    //        component: TabGabunganPaymentFinComponent,
    //        resolve: {
    //            'pagingParams': PaymentResolvePagingParams
    //        },
    //        data: {
    //            authorities: ['ROLE_USER'],
    //            pageTitle: 'mpmApp.payment.home.title'
    //        },
    //        canActivate: [UserRouteAccessService]
    //    },
    {
        path: 'payment-new-mkt-fix-rebate',
        component: PaymentNewMktFixRebateComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-edit-mkt-fix-rebate/:id/:isview',
        component: PaymentNewMktFixRebateComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentPopupRoute: Routes = [
    // {
    //     path: 'payment-new',
    //     component: PaymentPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.payment.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    {
        path: 'payment/:id/edit',
        component: PaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment/:id/delete',
        component: PaymentDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
