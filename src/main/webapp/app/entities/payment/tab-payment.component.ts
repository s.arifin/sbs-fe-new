import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { PaginationConfig } from 'ngx-bootstrap';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Payment, PaymentService } from '.';
import { LoadingService } from '../../layouts';
import { Principal, ITEMS_PER_PAGE } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-tab-payment',
    templateUrl: './tab-payment.component.html'
})
export class TabPaymentComponent implements OnInit, OnDestroy {

    constructor(
    ) {
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    ngOnInit(): void {
        // throw new Error('Method not implemented.');
    }
}
