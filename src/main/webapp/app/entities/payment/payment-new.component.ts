import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { ConfirmationService } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { runInThisContext } from 'vm';
import { Payment, PaymentService } from '.';
import { LoadingService } from '../../layouts';
import { Account, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Invoice, InvoiceService } from '../invoice';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { PaymentItem } from '../payment-item';
import { Retur, ReturService, ReturVM } from '../retur';

@Component({
    selector: 'jhi-payment-new',
    templateUrl: './payment-new.component.html'
})
export class PaymentNewComponent implements OnInit, OnDestroy {

    @Input() idstatus: number;
    pay: Payment;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    payItems: PaymentItem[];
    payItem: PaymentItem;
    eventSubscriber: Subscription;
    checkPPN: boolean;
    isview: boolean;
    subscription: Subscription;
    dtDue: Date;
    idinv: any;
    pembulatan: boolean;
    jnspembulatan: boolean;
    lblpembualatan: string;
    lbljnspembulatan: string;
    pembulatan1: boolean;
    jnspembulatan1: boolean;
    lblpembualatan1: string;
    lbljnspembulatan1: string;
    filter: Invoice[];
    len: string;
    notebiayalain: string;
    returs: Retur[];
    retur: Retur;
    filterRetur: Retur[];
    currentAccount: Account;
    internal: string;
    payupd: Payment;
    saveBool: boolean;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private paymentService: PaymentService,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
        private returService: ReturService,
        private confirmationService: ConfirmationService,
        private reportUtilService: ReportUtilService,
        private principal: Principal,
    ) {
        this.pay = new Payment();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.payItems = new Array<PaymentItem>();
        this.payItem = new PaymentItem;
        this.checkPPN = false;
        this.eventSubscriber = new Subscription();
        this.isview = false;
        this.dtDue = new Date();
        this.idinv = '';
        this.filter = new Array<Invoice>();
        this.len = '';
        this.notebiayalain = 'Materai';
        this.returs = new Array<Retur>();
        this.retur = new Retur();
        this.filterRetur = new Array<Retur>();
        this.currentAccount = new Account();
        this.internal = '';
        this.payupd = new Payment();
        this.pembulatan = false;
        this.lblpembualatan = 'Tidak';
        this.jnspembulatan = false;
        this.lbljnspembulatan = 'Bawah';
        this.pembulatan1 = false;
        this.lblpembualatan1 = 'Tidak';
        this.jnspembulatan1 = false;
        this.lbljnspembulatan1 = 'Bawah';
        this.saveBool = false;
    }
    ngOnInit() {
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            this.internal = account.idInternal;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.saveBool = true;
                    this.load(params['id']);
                    this.returService.getReturByIdPay(
                        {
                            query: 'idpay:' + params['id']
                        }
                    ).subscribe(
                        (res: ResponseWrapper) => {
                            this.returs = res.json;
                            if (this.pay.retur === undefined || this.pay.retur === null) {
                                this.pay.retur = 0;
                            }
                            this.returs.forEach(
                                (x) => this.pay.retur += x.grandtot)
                        }
                    )
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.isview = false;
                    this.load(params['id']);
                    this.returService.getReturByIdPay(
                        {
                            query: 'idpay:' + params['id']
                        }
                    ).subscribe(
                        (res: ResponseWrapper) => {
                            this.returs = res.json;
                            if (this.pay.retur === undefined || this.pay.retur === null) {
                                this.pay.retur = 0;
                            }
                            this.returs.forEach(
                                (x) => this.pay.retur += x.grandtot)
                        }
                    )
                }
            }
        });
        this.registerFromReturLOV();
        this.registerFromPaymentLOV();
    }
    load(id) {
        this.paymentService.find(id).subscribe(
            (res: Payment) => {
                this.pay = res;
                this.payupd = res;
                this.payItems = res.payment_items;
                this.notebiayalain = this.pay.additionalcostsnote;
                this.dtDue = new Date();
                this.dtDue = new Date(res.dtdue);
                this.masterSupplierService.findByCode(res.payto).subscribe(
                    (resS: MasterSupplier) => this.newSuplier = resS
                )
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.pay.payto = this.newSuplier.kd_suppnew;
        this.pay.paytoname = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
    }

    createPay() {
        this.saveBool = true;
        this.pay.notaretur = this.returs;
        this.pay.additionalcostsnote = this.notebiayalain;
        this.pay.dtdue = this.dtDue;
        this.pay.payment_items = this.payItems;
        if (this.pay.payto === null || this.pay.payto === undefined || this.pay.payto === '') {
            alert('Supplier Tidak Boleh Kosong');
            this.saveBool = false;
        } else if (this.pay.dtdue === null || this.pay.dtdue === undefined) {
            alert('Tanggal Jatuh Tempo Tidak Boleh Kosong');
            this.saveBool = false;
            // } else if (this.pay.valuta !== 'Rp' && (this.pay.kursppn === 0 || this.pay.kursppn === undefined || this.pay.kursppn === null)) {
            //         alert('Kurs PPN tidak boleh kosong');
        } else {
            if (this.pay.idpay === undefined) {
                this.loadingService.loadingStart();
                this.paymentService.create(this.pay).subscribe(
                    (res) => {
                        this.saveBool = false;
                        this.backMainPage();
                        this.loadingService.loadingStop();
                    },
                    (err) => {
                        this.saveBool = false;
                        this.loadingService.loadingStop();
                    }
                )
            } else if (this.pay.idpay !== undefined) {
                this.loadingService.loadingStart();
                this.paymentService.update(this.pay).subscribe(
                    (res) => {
                        this.saveBool = false;
                        this.backMainPage();
                        this.loadingService.loadingStop();
                    },
                    (err) => {
                        this.saveBool = false;
                        this.loadingService.loadingStop();
                    }
                )
            }
        }
    }

    backMainPage() {
        // this.router.navigate(['/payment']);
        window.history.back();
    }
    registerFromPaymentLOV() {
        this.eventSubscriber = this.eventManager.subscribe('InvoiceModified', () => this.regGetInvoice());
    }
    regGetInvoice(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                console.log('cek data masuk atau ngga yah');
                this.invoiceService.valusInv.subscribe(
                    (res: Invoice[]) => {
                        console.log('get data values PAYMENT masuk suplier ada ngga yahh == ', this.pay);
                        console.log('get data values INVOICE masuk suplier ada ngga yahh == ', res);
                        console.log('get data values INVOICE masuk suplier ada ngga yahh == ', this.newSuplier);
                        if (this.pay.paytype === undefined) {
                            this.pay.paytype = res[0].invtype;
                        }
                        if (this.pay.paytype !== res[0].invtype) {
                            this.toasterService.showToaster('Info', 'Peringatan', 'Jenis Pembayaran Tidak Boleh Bebeda');
                            return;
                        }
                        if (this.pay.valuta !== undefined) {
                            if (this.pay.valuta.toLowerCase() !== res[0].valuta.toLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                                return;
                            }
                        }
                        if (this.pay.receiptno !== null && this.pay.receiptno !== '' && this.pay.receiptno !== undefined) {
                            if (this.pay.receiptno !== res[0].receiptno) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Nomor tanda terima berbeda');
                                return;
                            }
                        }
                        if (this.pay.valuta === undefined) {
                            this.pay.valuta = res[0].valuta;
                        }
                        if (this.newSuplier.kd_suppnew === undefined) {
                            console.log('get data values INVOICE == ', res);
                            res.forEach((e) => {
                                const _idinv = e.idinv;
                                const _invno = e.invno;
                                const _grandtotal = e.grandtotal;
                                const _invfrom = e.invfrom;
                                const _taxinvno = e.taxinvno;
                                const _receiptno = e.receiptno;
                                this.filter = this.payItems.filter(
                                    function dataSama(items) {
                                        return (
                                            items.idinvoice === _idinv &&
                                            items.invno === _invno &&
                                            items.totalpay === _grandtotal &&
                                            items.invfrom === _invfrom &&
                                            items.invtaxno === _taxinvno &&
                                            items.receiptno === _receiptno
                                        )
                                    }
                                );
                                if (this.filter.length <= 0) {
                                    if (this.pay.note === undefined) {
                                        this.pay.note = '';
                                    }
                                    this.payItem = new PaymentItem();
                                    this.payItem.idinvoice = e.idinv;
                                    this.payItem.invno = e.invno;
                                    this.payItem.totalpay = e.grandtotal;
                                    this.payItem.invfrom = e.invfrom;
                                    this.payItem.invtaxno = e.taxinvno;
                                    this.payItem.receiptno = e.receiptno;
                                    this.payItem.ppn = e.ppn;
                                    this.payItem.paynoppn = e.grandtotal - e.ppn;
                                    this.payItem.totalppn = e.totalppn;
                                    this.pay.note += e.note;
                                    this.payItems = [... this.payItems, this.payItem];
                                }
                            });
                            this.countPrice().then(() => this.countPPH());
                            this.getSuplierData(res[0].invfrom);
                        }
                        if (this.newSuplier.kd_suppnew !== undefined) {
                            console.log('get data values INVOICE masuk suplier ada == ', res);
                            if (this.newSuplier.kd_suppnew.toLowerCase() === res[0].invfrom.toLowerCase()) {
                                console.log('get data values INVOICE == ', res);
                                res.forEach((e) => {
                                    const _idinv = e.idinv;
                                    const _invno = e.invno;
                                    const _grandtotal = e.grandtotal;
                                    const _invfrom = e.invfrom;
                                    const _taxinvno = e.taxinvno;
                                    const _receiptno = e.receiptno;
                                    this.filter = this.payItems.filter(
                                        function dataSama(items) {
                                            return (
                                                items.idinvoice === _idinv &&
                                                items.invno === _invno &&
                                                items.totalpay === _grandtotal &&
                                                items.invfrom === _invfrom &&
                                                items.invtaxno === _taxinvno &&
                                                items.receiptno === _receiptno
                                            )
                                        }
                                    );
                                    if (this.filter.length <= 0) {
                                        if (this.pay.note === undefined) {
                                            this.pay.note = '';
                                        }
                                        this.payItem = new PaymentItem();
                                        this.payItem.idinvoice = e.idinv;
                                        this.payItem.invno = e.invno;
                                        this.payItem.totalpay = e.grandtotal;
                                        this.payItem.invfrom = e.invfrom;
                                        this.payItem.invtaxno = e.taxinvno;
                                        this.payItem.receiptno = e.receiptno;
                                        this.payItem.ppn = e.ppn;
                                        this.payItem.paynoppn = e.grandtotal - e.ppn;
                                        this.payItem.totalppn = e.totalppn;
                                        this.pay.note += e.note;
                                        this.payItems = [... this.payItems, this.payItem];
                                    }
                                });
                                this.countPrice().then(() => this.countPPH());
                                // this.getSuplierData(res[0].invfrom);
                            }
                            if (this.newSuplier.kd_suppnew.toLowerCase() !== res[0].invfrom.toLowerCase()) {
                                this.toasterService.showToaster('Info', 'Info', 'Supplier Tidak Boleh Berbeda')
                            }
                        }
                        this.pay.receiptno = this.payItems[0].receiptno;
                        res.forEach((e: Invoice) => {
                            this.invoiceService.findpopph(e).subscribe((resfind) => {
                                if (resfind.code === 200 && resfind.messages === 'Data Ada') {
                                    this.toasterService.showToaster('Info', 'Info', 'PO diinvoice ada potongan PPH  !!')
                                    alert('PO diinvoice ada potongan PPH  !!');
                                    return;
                                } else { }
                            });
                        })
                        // this.invoiceService.valusInv.observers = [];
                    }
                );
                this.invoiceService.valusInv.observers = [];
                this.eventSubscriber.unsubscribe();
                resolvedt();
            }
        )
    }
    countPrice(): Promise<void> {
        return new Promise<void>(
            (resolveCP) => {
                console.log('cek data masuk hitung == ');
                // if (this.pay.totalpay === undefined || this.pay.totalpay === null) {
                this.pay.subtotal = 0;
                this.pay.totalppn = 0;
                this.pay.ppn = 0;
                // }
                this.payItems.forEach(
                    (e) => {
                        // if (this.pay.valuta !== 'Rp') {
                        //     this.pay.subtotal += e.paynoppn;
                        // } else {
                        this.pay.subtotal += e.totalpay;
                        // }
                        if (this.pay.ppn === null || this.pay.ppn === undefined) {
                            this.pay.ppn = 0
                        }
                        if (this.pay.totalppn === null || this.pay.totalppn === undefined) {
                            this.pay.totalppn = 0
                        }
                        this.pay.totalppn += e.totalppn;
                        this.pay.ppn += e.ppn;
                    }
                )
                resolveCP();
            }
        )
    }
    lihatDetail(data: PaymentItem) {
        this.idinv = data.idinvoice;
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
                this.pay.payto = this.newSuplier.kd_suppnew;
                this.pay.paytoname = this.newSuplier.nmSupplier;
            }
        )
    }
    countPPH() {
        if (this.pay.cutcost === undefined) {
            this.pay.cutcost = 0;
        }
        if (this.pay.additionalcosts === undefined) {
            this.pay.additionalcosts = 0;
        }
        if (this.pay.subtotal === undefined) {
            this.pay.subtotal = 0;
        }
        if (this.pay.pph === undefined) {
            this.pay.pph = 0;
        }
        if (this.pembulatan === false) {
            this.jnspembulatan = false
            this.lblpembualatan = 'Tidak';
        }
        if (this.pembulatan === true) {
            this.lblpembualatan = 'Ya';
        }
        if (this.jnspembulatan === false) {
            this.lbljnspembulatan = 'Bawah';
        }
        if (this.jnspembulatan === true) {
            this.lbljnspembulatan = 'Atas';
        }
        if (this.pembulatan1 === false) {
            this.jnspembulatan1 = false
            this.lblpembualatan1 = 'Tidak';
        }
        if (this.pembulatan1 === true) {
            this.lblpembualatan1 = 'Ya';
        }
        if (this.jnspembulatan1 === false) {
            this.lbljnspembulatan1 = 'Bawah';
        }
        if (this.jnspembulatan1 === true) {
            this.lbljnspembulatan1 = 'Atas';
        }
        this.pay.retur = 0;
        this.returs.forEach(
            (e) => {
                this.pay.retur += e.grandtot;
            }
        );

        if (this.pay.sebelum_pph === undefined) {
            this.pay.sebelum_pph = 0;
        }

        if (this.pay.persen_pph === undefined) {
            this.pay.persen_pph = 0;
        }
        if (this.pembulatan1 === false) {
            this.pay.pph = this.pay.sebelum_pph * (this.pay.persen_pph / 100);
        }
        if (this.pembulatan1 === true) {
            if (this.jnspembulatan1 === false) {
                this.pay.pph = Math.floor(this.pay.sebelum_pph * (this.pay.persen_pph / 100)); // - _cutcost;
            }
            if (this.jnspembulatan1 === true) {
                this.pay.pph = Math.ceil(this.pay.sebelum_pph * (this.pay.persen_pph / 100)); // - _cutcost;
            }
        }
        const _topay = this.pay.subtotal;
        const _pph = this.pay.pph;
        const _additionalcosts = this.pay.additionalcosts;
        const _retur = this.pay.retur;
        // const _cutcost = this.pay.cutcost;
        const _ppn = this.pay.ppn
        if (this.pembulatan === false) {
            this.pay.totalpay = _topay - _pph + _additionalcosts - _retur; // - _cutcost;
        }
        if (this.pembulatan === true) {
            if (this.jnspembulatan === false) {
                this.pay.totalpay = Math.floor(_topay - _pph + _additionalcosts - _retur); // - _cutcost;
            }
            if (this.jnspembulatan === true) {
                this.pay.totalpay = Math.ceil(_topay - _pph + _additionalcosts - _retur); // - _cutcost;
            }
        }

        if (this.pay.pph > 0) {
            this.pay.notepph = this.pay.persen_pph + '% X ' + 'Rp. ' + this.numberWithCommas(this.pay.sebelum_pph);
        } else {
            this.pay.notepph = '';
        }
    }
    ceklen() {
        this.len = this.pay.note.length.toString();
        if (this.pay.note !== '' && this.pay.note !== undefined) {
            if (this.pay.note.length > 500) {
                alert('( jumlah karakter ' + this.pay.note.length + ') \nCatatan tidak boleh melebihi 500 karakter (termasuk spasi)');
            }
        }
    }
    registerFromReturLOV() {
        this.eventSubscriber = this.eventManager.subscribe('ReturModified', () => this.regGetRetur().then((x) => this.countPPH()));
    }
    regGetRetur(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.returService.values.subscribe(
                    (res: ReturVM[]) => {
                        res.forEach(
                            (e) => {
                                if (e.suppcode.toLocaleLowerCase() !== this.newSuplier.kd_suppnew.toLowerCase()) {
                                    alert('Supplier Tidak Boleh Berbeda !!')
                                } else {
                                    this.filterRetur = this.returs.filter(
                                        function dataSama(items) {
                                            return (
                                                items.idretur === e.idretur
                                            )
                                        }
                                    );
                                    if (this.filterRetur.length <= 0) {
                                        this.retur = new Retur();
                                        this.retur = e;
                                        this.returs = [... this.returs, this.retur];
                                        console.log('ini adalah data nota retur nya => ', res);
                                    }
                                }
                            }
                        )
                    }
                );
                this.returService.values.observers = [];
                this.eventSubscriber.unsubscribe();
                resolvedt();
            }
        )
    }
    public deleteListArray(rowData: Retur) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                this.returs = this.returs.filter(function dataSama(items) {
                    return (items.idretur !== rowData.idretur)
                });
                this.returService.deleteMapReturByID(rowData)
                    .subscribe(
                        (res) => { console.log('berhasil hapus') }
                    )
                this.countPPH();
            }
        });
    }

    public deleteListInvoice(rowData: PaymentItem) {
        if (this.payItems.length < 1) {
            alert('Invoice Tidak Boleh Kosong');
        } else {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Apakah Anda yakin akan menghapus data ini ??',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.paymentService.deleteInvoice(rowData.idinvoice).subscribe(
                        (res) => {
                            // Callback ini akan dijalankan jika permintaan berhasil
                            this.payItems = this.payItems.filter(function dataSama(items) {
                                return items.idinvoice !== rowData.idinvoice;
                            });
                            this.countPrice().then(() => this.countPPH());
                            alert('Berhasil Hapus Invoice dari PV');
                            this.loadingService.loadingStop();
                        },
                        (error) => {
                            // Callback ini akan dijalankan jika ada error
                            alert('Gagal Hapus Invoice dari PV');
                            this.loadingService.loadingStop();
                        },
                        () => {
                            this.loadingService.loadingStop();
                        }
                    );
                }
            });
        }
    }

    cetakRetur(retur: Retur) {
        // const pisah = retur.noretur.split('/');
        // const filter_data = 'noretur:' + pisah[0].toString() +
        //     '|pt:' + pisah[1].toString() +
        //     '|bulan:' + pisah[2].toString() +
        //     '|tahun:' + pisah[3].toString();
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
        const filter_data = 'idretur:' + retur.idretur;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }
    setdtdue() {
        this.payupd.dtdue = this.dtDue;
        this.paymentService.setdtdue(this.payupd).subscribe((res) => {
            window.location.reload();
        });
    }

    numberWithCommas(x) {
        return x.toLocaleString('en-US');
    }
}
