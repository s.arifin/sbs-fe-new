import { PaymentItem } from '../payment-item';
import { BaseEntity } from '../../shared';

export class PaymentStatus {
    constructor(
        public idstatus?: any,
        public idpay?: any,
        public statustype?: number,
        public dtfrom?: Date,
        public dtthru?: Date,
        public author?: string,
    ) {
    }
}
