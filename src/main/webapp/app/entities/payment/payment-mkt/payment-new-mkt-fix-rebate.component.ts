import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { MasterSupplierService } from '../../master-supplier/master-supplier.service';
import { MasterSupplier } from '../../master-supplier';
import { InvoiceItem } from '../../invoice-item';
import { Purchasing, PurchasingService, Valuta } from '../../purchasing';
import { Invoice, InvoiceService, Pobbmlov } from '../../invoice';
import { Payment } from '../payment.model';
import { PaymentItem } from '../../payment-item';
import { PaymentService } from '../payment.service';
import { Account, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { TandaTerima, TandaTerimaService } from '../../tanda-terima';
import { DetailTandaTerima } from '../../detail-tanda-terima';
import { LoadingService } from '../../../layouts';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-payment-new-mkt-fix-rebate',
    templateUrl: './payment-new-mkt-fix-rebate.component.html'
})
export class PaymentNewMktFixRebateComponent implements OnInit, OnDestroy {

    pay: Payment;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    payItems: PaymentItem[];
    payItem: PaymentItem;
    eventSubscriber: Subscription;
    checkPPN: boolean;
    isview: boolean;
    subscription: Subscription;
    dtDue: Date;
    filter: Purchasing[];
    invNO: string;
    filteredValuta: any[];
    newValuta: Valuta;
    currentAccount: Account;
    eventSubscriberPPLOV: Subscription;
    invoiceNumber: string;
    tandaTerimas: TandaTerima[];
    tandaTerima: TandaTerima;
    filterTT: TandaTerima[];
    dtrelease: Date;
    dtpay: Date;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private paymentService: PaymentService,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private principal: Principal,
        private tandaTerimaService: TandaTerimaService,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService
    ) {
        this.pay = new Payment();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.payItems = new Array<PaymentItem>();
        this.payItem = new PaymentItem;
        this.checkPPN = false;
        this.eventSubscriber = new Subscription();
        this.isview = false;
        this.dtDue = new Date();
        this.filter = new Array<Purchasing>();
        this.invNO = '';
        this.newValuta = new Valuta();
        this.currentAccount = new Account();
        this.invoiceNumber = '';
        this.tandaTerimas = new Array<TandaTerima>();
        this.tandaTerima = new TandaTerima();
        this.filterTT = new Array<TandaTerima>();
        this.dtrelease = new Date();
        this.dtpay = new Date();
    }
    ngOnInit() {
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                }
            }
        });
        this.registerFromPaymentLOV();
        this.registerTandaTerima();
    }
    load(id) {
        this.paymentService.find(id).subscribe(
            (res: Payment) => {
                this.pay = res;
                this.payItems = res.payment_items;
                this.dtDue = res.dtdue;
                this.invNO = res.payment_items[0].proformainv;
                this.invoiceNumber = res.payment_items[0].invno;
                this.masterSupplierService.findByCode(res.payto).subscribe(
                    (resS: MasterSupplier) => this.newSuplier = resS
                );
                this.tandaTerimaService.queryList({
                    query: 'nott:' + res.receiptno
                }).subscribe(
                    (restt) => {
                        this.tandaTerimas = restt.json;
                    }
                )
                this.filterValutaSingleLoad(res.valuta);
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.currentAccount.idInternal).then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.currentAccount.idInternal).then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.pay.payto = this.newSuplier.kd_suppnew;
        this.pay.paytoname = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
    }

    createPay() {
        this.setReceiptno();
        this.isview = true;
        this.loadingService.loadingStart();
        this.pay.paytype = 9;
        this.pay.dtdue = this.dtDue;
        this.pay.dtpay = this.dtpay;
        this.pay.dtrelease = this.dtrelease;
        // this.pay.receiptno = this.invNO;
        this.pay.payment_items.forEach(
            (e) => {
                e.proformainv = this.invNO;
            }
        );
        // if (this.pay.payto === null || this.pay.payto === undefined || this.pay.payto === '') {
        //     alert('Supplier Tidak Boleh Kosong');
        // } else
        if (this.pay.dtdue === null || this.pay.dtdue === undefined) {
            alert('Tanggal Jatuh Tempo Tidak Boleh Kosong');
        } else {
            if (this.pay.idpay === undefined) {
                const item = new PaymentItem();
                item.invno = this.invoiceNumber;
                this.payItems = [... this.payItems, item];
                this.pay.payment_items = this.payItems;
                this.paymentService.create(this.pay).subscribe(
                    (res) => {
                        this.isview = false;
                        this.loadingService.loadingStop();
                        this.backMainPage();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                    }
                )
            } else if (this.pay.idpay !== undefined) {
                this.payItems = [... this.payItems, new PaymentItem()];
                this.pay.payment_items = this.payItems;
                this.paymentService.update(this.pay).subscribe(
                    (res) => {
                        this.isview = false;
                        this.loadingService.loadingStop();
                        this.backMainPage();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                    }
                )
            }
        }
    }

    backMainPage() {
        // this.router.navigate(['/payment']);
        window.history.back();
    }
    registerFromPaymentLOV() {
        this.eventSubscriber = this.eventManager.subscribe('POLovModification', () => this.regGetInvoice());
    }
    regGetInvoice(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.purchasingService.values.subscribe(
                    (res: Purchasing[]) => {
                        if (this.pay.valuta !== undefined) {
                            if (this.pay.valuta.toLowerCase() !== res[0].valuta.toLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                                return;
                            }
                        }
                        if (this.pay.valuta === undefined) {
                            this.pay.valuta = res[0].valuta;
                        }
                        if (this.newSuplier.kd_suppnew === undefined) {
                            res.forEach((e) => {
                                const _idinv = e.idpurchasing;
                                // const _invno = e.nopurchasing;
                                this.invNO = e.nopurchasing;
                                const _grandtotal = e.totalprice;
                                this.filter = this.payItems.filter(
                                    function dataSama(items) {
                                        return (
                                            items.idinvoice === _idinv &&
                                            items.invno === this.invNO &&
                                            items.totalpay === _grandtotal
                                        )
                                    }
                                );
                                if (this.filter.length <= 0) {
                                    this.payItem = new PaymentItem();
                                    this.payItem.invno = e.nopurchasing;
                                    this.payItem.idinvoice = e.idpurchasing;
                                    this.payItem.totalpay = e.totalprice;
                                    this.payItems = [... this.payItems, this.payItem];
                                }
                            });
                            this.getSuplierData(res[0].supliercode);
                        }
                        if (this.newSuplier.kd_suppnew !== undefined) {
                            if (this.newSuplier.kd_suppnew.toLowerCase() === res[0].supliercode.toLowerCase()) {
                                res.forEach((e) => {
                                    const _idinv = e.idpurchasing;
                                    this.invNO = e.nopurchasing;
                                    const _grandtotal = e.totalprice;
                                    this.filter = this.payItems.filter(
                                        function dataSama(items) {
                                            return (
                                                items.idinvoice === _idinv &&
                                                items.invno === this.invNO &&
                                                items.totalpay === _grandtotal
                                            )
                                        }
                                    );
                                    if (this.filter.length <= 0) {
                                        this.payItem = new PaymentItem();
                                        this.payItem.invno = e.nopurchasing;
                                        this.payItem.idinvoice = e.idpurchasing;
                                        this.payItem.totalpay = e.totalprice;
                                        this.payItems = [... this.payItems, this.payItem];
                                    }
                                });
                            }
                            if (res[0].supliercode.toLocaleLowerCase() !== this.newSuplier.kd_suppnew.toLocaleLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
                            }
                        }
                    });
                this.eventSubscriber.unsubscribe();
                resolvedt();
            }
        )
    }
    countPrice(): Promise<void> {
        return new Promise<void>(
            (resolveCP) => {
                this.pay.totalpay = 0;
                this.payItems.forEach(
                    (e) => {
                        this.pay.subtotal += e.totalpay;
                    }
                )
                resolveCP();
            }
        )
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
                this.pay.payto = this.newSuplier.kd_suppnew;
                this.pay.paytoname = this.newSuplier.nmSupplier;
            }
        )
    }
    countPPH() {
        if (this.pay.totalpay === undefined) {
            this.pay.totalpay = 0;
        }
        if (this.pay.pph === undefined) {
            this.pay.pph = 0;
        }
        if (this.pay.fixrebate === undefined) {
            this.pay.fixrebate = 0;
        }
        const _topay = this.pay.subtotal;
        const _pph = this.pay.pph;
        const _fixrebate = this.pay.fixrebate;
        this.pay.totalpay = _topay - _pph - _fixrebate;
    }
    filterValutaSingle(event) {
        const query = event.query;
        this.purchasingService.getAllValuta().then((valuta) => {
            this.filteredValuta = this.filterValuta(query, valuta);
        });
    }

    filterValuta(query, valuta: Valuta[], isEdit: Boolean = false): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < valuta.length; i++) {
            const valutas = valuta[i];
            if (valutas.valutacode.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(valutas);
                if (isEdit === true) {
                    this.newValuta = valutas;
                }
            }
        }
        return filtered;
    }

    public selectValuta(isSelect?: boolean): void {
        this.pay.valuta = this.newValuta.valutacode;
    }
    registerTandaTerima() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('TandaTerimaModified',
            () => {
                this.regTT();
            });
    }
    regTT(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.tandaTerimaService.values.subscribe(
                    (res: DetailTandaTerima) => {
                        // this.invNO = res.receiptno;
                        this.filterTT = this.tandaTerimas.filter(
                            function dataSama(items) {
                                return (
                                    items.idreceipt === res.idreceipt
                                )
                            }
                        );
                        if (this.filterTT.length <= 0) {
                            this.tandaTerima = new TandaTerima();
                            this.tandaTerima.idreceipt = res.idreceipt;
                            this.tandaTerima.receiptno = res.receiptno;
                            this.tandaTerima.kdsup = res.kdsupplier;
                            this.tandaTerima.total = res.total;
                            this.tandaTerimas = [... this.tandaTerimas, this.tandaTerima];
                            if (this.pay.receiptno === undefined) {
                                this.pay.receiptno = '';
                            }
                            // this.pay.receiptno += res.receiptno + ', ';
                            this.tandaTerimaService.getinvSPL({
                                query: 'nott:' + res.receiptno
                            }).subscribe(
                                (resINV: ResponseWrapper) => {
                                    this.invoiceNumber += resINV.json;
                                }
                            )
                            this.masterSupplierService.findByCode(res.kdsupplier).subscribe(
                                (resSPL: MasterSupplier) => {
                                    this.newSuplier = resSPL;
                                    this.pay.payto = resSPL.kd_suppnew;
                                    this.pay.paytoname = resSPL.nmSupplier;
                                    if (this.pay.subtotal === undefined || this.pay.subtotal === null) {
                                        this.pay.subtotal = 0;
                                    }
                                    this.pay.subtotal += res.total;
                                    this.pay.additionalcosts = 0;
                                    if (this.pay.totalpay === null || this.pay.totalpay === undefined) {
                                        this.pay.totalpay = 0;
                                    }
                                    this.pay.totalpay += res.total + this.pay.additionalcosts;
                                }
                            )
                        }
                    }
                );
                this.setReceiptno();
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }
    filterValutaSingleLoad(event) {
        const query = event;
        this.purchasingService.getAllValuta().then((valuta) => {
            this.filteredValuta = this.filterValuta(query, valuta, true);
        });
    }
    public deleteListArray(rowData: TandaTerima) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                this.tandaTerimas = this.tandaTerimas.filter(function dataSama(items) {
                    return (items.idreceipt !== rowData.idreceipt)
                });
                this.tandaTerimas.forEach((x) => {
                    let nott = [];
                    nott = [...nott, x.receiptno];
                    this.pay.receiptno = nott.join();
                })
                this.countPPH();
            }
        });
    }
    public setReceiptno() {
        let nott = [];
        this.tandaTerimas.forEach((e) => {
            nott = [... nott, e.receiptno];
            this.pay.receiptno = nott.join();
        })
    }

}
