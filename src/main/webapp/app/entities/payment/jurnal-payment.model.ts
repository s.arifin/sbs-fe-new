import { BaseEntity } from './../../shared';

export class JurnalPayment implements BaseEntity {
    constructor(
        public id?: any,
        public jenistrans?: string,
        public kodebank?: string,
        public norek?: string,
        public kdsupplier?: string,
        public tahun?: number,
        public nobukti?: string,
        public nogiro?: string,
        public tanggal?: any,
        public debet?: number,
        public kredit?: number,
        public ket1?: string,
        public ket2?: string,
        public ket3?: string,
        public uangmuka?: number,
        public bulan?: number,
    ) {
    }
}
