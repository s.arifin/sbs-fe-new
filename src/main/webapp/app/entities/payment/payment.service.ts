import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Listpaypo, Payment } from './payment.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Invoice } from '../invoice';
import { JurnalPayment } from './jurnal-payment.model';
import { FileAttachment } from '.';
import { PurchasingItem } from '../purchasing-item';
import { PaymentItem } from '../payment-item';

@Injectable()
export class PaymentService {

    private resourceUrl = process.env.API_C_URL + '/api/payments';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/payments';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(payment: Payment): Observable<Payment> {
        const copy = this.convert(payment);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getItemName(idpo: number): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/getItem/${idpo}`).map((res: Response) => this.convertResponse(res));
    }

    update(payment: Payment): Observable<Payment> {
        const copy = this.convert(payment);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Payment> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findTT(id: string): Observable<Payment> {
        return this.http.get(`${this.resourceUrl}/findTT/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    deleteInvoice(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/deleteInvoice/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    deletebyid(invoiceItem: PaymentItem): Observable<PaymentItem> {
        const copy = this.convert1(invoiceItem);
        return this.http.post(this.resourceUrl + '/delete-items', copy).map((res: Response) => {
            return res.json();
        });
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        // entity.dtcreate = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtcreate);
        // entity.dtmodified = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtmodified);
        // entity.dtdue = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtdue);
        // entity.dtdeptissueby = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtdeptissueby);
        // entity.dtdeptappby = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtdeptappby);
        // entity.dtdirector = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtdirector);
        // entity.dtacccodeby = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtacccodeby);
        // entity.dtaccappby = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtaccappby);
        // entity.dtfincheckby = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtfincheckby);
        // entity.dtfinappby = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtfinappby);
        // entity.dtfinpassforpay = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtfinpassforpay);
        // entity.dtreceipt = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtreceipt);
    }

    private convert(payment: Payment): Payment {
        const copy: Payment = Object.assign({}, payment);
        return copy;
    }

    private convertArray(payment: Payment[]): Payment[] {
        const copy: Payment[] = Object.assign([], payment);
        return copy;
    }

    private convert1(invoice: PaymentItem): PaymentItem {
        const copy: PaymentItem = Object.assign({}, invoice);

        // copy.dtinv = this.dateUtils.toDate(invoice.dtinv);

        // copy.dtcreate = this.dateUtils.toDate(invoice.dtcreate);

        // copy.dtmodified = this.dateUtils.toDate(invoice.dtmodified);
        return copy;
    }

    setStatus(pay: Payment, idstatus: number): Observable<Payment> {
        const copy = this.convert(pay);
        return this.http.post(this.resourceUrl + '/set-status/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    setStatus1(pay: Payment, idstatus: number): Observable<Payment> {
        const copy = this.convert(pay);
        return this.http.post(this.resourceUrl + '/set-status1/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    setStatus2(pay: Payment, idstatus: number): Observable<Payment> {
        const copy = this.convert(pay);
        return this.http.post(this.resourceUrl + '/set-status2/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryApproval(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryByPurchasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/purchasing-history', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryOneByPurchasing(idpo: any): Observable<Payment> {
        return this.http.get(`${this.resourceUrl}/purchasing-payment/${idpo}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    setStatusBulk(payment: Payment[], id: number): Observable<Payment[]> {
        const copy = this.convertArray(payment);
        return this.http.post(`${this.resourceUrl}/update-statuses/${id}`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    cekPV(kdprogramX: string): Observable<PaymentItem> {
        return this.http.get(`${this.resourceUrl}/cekPV/${kdprogramX}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    bypasstofin(payment: Payment[]): Observable<Payment[]> {
        const copy = this.convertArray(payment);
        return this.http.post(`${this.resourceUrl}/bypass-to-fin`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    getPayACCFIN(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/tanda-terima', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPayACCFINByTT(id: string): Observable<Payment> {
        return this.http.get(this.resourceUrl + '/tanda-terima/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    updateSetBayar(pay: Payment, inv: Invoice[]): Observable<Payment> {
        const payment = this.convert(pay);
        const invs = this.convertArrINV(inv);
        return this.http.put(this.resourceUrl + '/set-bayar', { payment, invs }).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    private convertArrINV(invs: Invoice[]): Invoice[] {
        const copy: Invoice[] = Object.assign([], invs);
        return copy;
    }
    setHold(payment: Payment): Observable<Payment> {
        const copy = this.convert(payment);
        return this.http.put(this.resourceUrl + '/hold-pay', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createBPBR(payment: Payment): Observable<Payment> {
        const copy = this.convert(payment);
        return this.http.post(this.resourceUrl + '/bp-br', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryGetJurnal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-jurnal', options)
            .map((res: Response) => this.convertResponse(res));
    }
    // (id: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(req);
    //     return this.http.get(this.resourceUrl + '/get-jurnal', options)
    //         .map((res: Response) => this.convertResponse(res));
    // }
    queryGetJurnalById(id: number): Observable<Payment> {
        return this.http.get(this.resourceUrl + '/get-jurnal/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryGetJurnalBRById(id: number): Observable<JurnalPayment> {
        return this.http.get(this.resourceUrl + '/get-jurnal-br/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryReport(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/report', options)
            .map((res: Response) => this.convertResponse(res));
    }
    updateSetBayarUSD(pay: Payment, inv: Invoice[]): Observable<Payment> {
        const payment = this.convert(pay);
        const invs = this.convertArrINV(inv);
        return this.http.put(this.resourceUrl + '/set-bayar-usd', { payment, invs }).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    bbmTekAtt(req?: any): Observable<FileAttachment> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-att-bbm-tek', options).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryReportSPL(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/report-spl', options)
            .map((res: Response) => this.convertResponse(res));
    }

    setnott(notanter: string, idpayment: any, pinvno: string, ptaxinvno: string): Observable<Payment> {
        const json: Object = Object.assign({}, {
            nott: notanter,
            idpay: idpayment,
            invno: pinvno,
            taxinvno: ptaxinvno
        });
        return this.http.post(this.resourceUrl + '/set-nott', json).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryRealisasiReport(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/realisasi-report', options)
            .map((res: Response) => this.convertResponse(res));
    }
    item_set_po(nopox: any, idpayx: any): Observable<Payment> {
        const json: Object = Object.assign({}, {
            nopo: nopox,
            idpay: idpayx
        });
        return this.http.post(this.resourceUrl + '/item-set-po', json).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    FindPOBYNo(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/find-po-by-no', options)
            .map((res: Response) => this.convertResponse(res));
    }
    FindPOBYNo1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/find-po-by-no1', options)
            .map((res: Response) => this.convertResponse(res));
    }
    setdtdue(payment: any): Observable<Payment> {
        return this.http.post(this.resourceUrl + '/set-dtdue', payment).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

}
