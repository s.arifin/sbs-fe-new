import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PaymentService,
    PaymentPopupService,
    PaymentDetailComponent,
    PaymentDialogComponent,
    PaymentPopupComponent,
    PaymentDeletePopupComponent,
    PaymentDeleteDialogComponent,
    paymentRoute,
    paymentPopupRoute,
    PaymentResolvePagingParams,
    PaymentNewComponent,
    PaymentComponent,
    // TabGabunganPaymentFinComponent,
    // GabunganPaymentFinComponent,
    PaymentNewMktFixRebateComponent,
    PaymentNewFinComponent,
    PaymentNewPBLComponent,
    PaymentNonMKTComponent,
} from './';

import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    MultiSelectModule
} from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TabApprovalPaymentComponent } from './payment-approval/tab-approval-payment.component';
import { ApprovalPaymentComponent } from './payment-approval/approval-payment.component';
import { PaymentNewUangMukaComponent } from './payment-new-uangmuka.component';
import { MpmInvoiceItemModule } from '../invoice-item/invoice-item.module';
import { PaymentByPurchasingComponent } from './payment-by-purchasing.component';
import { TabApprovalAccPaymentComponent } from './payment-acc-approval/tab-approval-acc-payment.component';
import { ApprovalPaymentAccComponent } from './payment-acc-approval/approval-acc-payment.component';
import { ApprovalPaymentFinComponent } from './payment-fin-approval/approval-fin-payment.component';
import { TabApprovalFinPaymentComponent } from './payment-fin-approval/tab-approval-fin-payment.component';
import { PaymentAccComponent } from './payment-acc/payment-acc.component';
import { TabPaymentAccComponent } from './payment-acc/tab-payment-acc.component';
import { PaymentFinComponent } from './payment-fin/payment-fin.component';
import { PaymentFinBayarComponent } from './payment-fin/payment-fin-bayar.component';
import { TabPaymentFinComponent } from './payment-fin/tab-payment-fin.component';
import { TabPaymentComponent } from './tab-payment.component';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PaymentNewMktComponent } from './payment-mkt/payment-new-mkt.component';
import { PaymentFinBRComponent } from './payment-fin/payment-fin-br.component';
import { PaymentFinBPComponent } from './payment-fin/payment-fin-bp.component';
import { PaymentFinJurnalComponent } from './payment-fin/payment-fin-jurnal.component';
import { FinTandaTerimaComponent } from './payment-fin/fin-tanda-terima.component';
import { MpmPaymentHistoryModule } from '../payment-history/payment-history.module';
import { PaymentFinBayarUSDComponent } from './payment-fin/payment-fin-bayar-usd.component';
import { PaymentNewMktNonComponent } from './payment-mkt/payment-new-mkt-non.component';

const ENTITY_STATES = [
    ...paymentRoute,
    ...paymentPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        MpmInvoiceItemModule,
        MpmPaymentHistoryModule,
        MultiSelectModule
    ],
    declarations: [
        PaymentComponent,
        PaymentDetailComponent,
        PaymentDialogComponent,
        PaymentDeleteDialogComponent,
        PaymentPopupComponent,
        PaymentDeletePopupComponent,
        PaymentNewComponent,
        TabApprovalPaymentComponent,
        ApprovalPaymentComponent,
        PaymentNewUangMukaComponent,
        PaymentByPurchasingComponent,
        PaymentAccComponent,
        PaymentFinComponent,
        TabPaymentAccComponent,
        PaymentFinBayarComponent,
        TabApprovalAccPaymentComponent,
        ApprovalPaymentAccComponent,
        TabPaymentFinComponent,
        TabApprovalFinPaymentComponent,
        ApprovalPaymentFinComponent,
        TabPaymentComponent,
        PaymentNewMktComponent,
        PaymentNewMktNonComponent,
        PaymentFinBRComponent,
        PaymentFinBPComponent,
        PaymentFinJurnalComponent,
        FinTandaTerimaComponent,
        PaymentFinBayarUSDComponent,
        // TabGabunganPaymentFinComponent,
        // GabunganPaymentFinComponent,
        PaymentNewMktFixRebateComponent,
        PaymentNewFinComponent,
        PaymentNewPBLComponent,
        PaymentNonMKTComponent
    ],
    entryComponents: [
        PaymentComponent,
        PaymentDialogComponent,
        PaymentPopupComponent,
        PaymentDeleteDialogComponent,
        PaymentDeletePopupComponent,
        PaymentNewComponent,
        TabApprovalPaymentComponent,
        ApprovalPaymentComponent,
        PaymentNewUangMukaComponent,
        PaymentByPurchasingComponent,
        TabApprovalAccPaymentComponent,
        ApprovalPaymentAccComponent,
        TabPaymentFinComponent,
        TabApprovalFinPaymentComponent,
        ApprovalPaymentFinComponent,
        PaymentNewMktComponent,
        PaymentFinBRComponent,
        PaymentFinBPComponent,
        PaymentFinJurnalComponent,
        FinTandaTerimaComponent,
        PaymentFinBayarUSDComponent,
        // TabGabunganPaymentFinComponent,
        // GabunganPaymentFinComponent,
        PaymentNewMktFixRebateComponent,
        PaymentNewFinComponent,
        PaymentNewPBLComponent,
        PaymentNonMKTComponent, PaymentNewMktNonComponent
    ],
    providers: [
        PaymentService,
        PaymentPopupService,
        PaymentResolvePagingParams,
        PaginationConfig,
    ],
    exports: [
        PaymentByPurchasingComponent,
        FinTandaTerimaComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPaymentModule { }
