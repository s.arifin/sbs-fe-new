import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Payment, PaymentService } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { JurnalPayment } from '../jurnal-payment.model';
@Component({
    selector: 'jhi-payment-fin-jurnal',
    templateUrl: './payment-fin-jurnal.component.html'
})
export class PaymentFinJurnalComponent implements OnInit, OnDestroy {

    @Input() idstatus: number;
    currentAccount: any;
    jurnals: JurnalPayment[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Payment[];
    newModalNote: boolean;
    notehold: string;
    login: string;
    payment: Payment;
    sortF: any;
    sortT: any;
    filteredCodeSuppliers: any[];
    filteredSuppliers: any[];
    fSplCode: string;
    newSuplier: MasterSupplier;
    constructor(
        private paymentService: PaymentService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
        private confirmatinService: ConfirmationService,
        private masterSupplierService: MasterSupplierService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Payment>();
        this.newModalNote = false;
        this.notehold = '';
        this.login = '';
        this.payment = new Payment();
        this.sortF = '';
        this.sortT = '';
        this.fSplCode = '';
        this.newSuplier = new MasterSupplier();
    }

    loadAll() {
        // if (this.currentSearch) {
        //     this.paymentService.search({
        //         page: this.page,
        //         query: this.currentSearch,
        //         size: this.itemsPerPage,
        //         sort: this.sort()
        //     }).subscribe(
        //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //         (res: ResponseWrapper) => this.onError(res.json)
        //     );
        //     return;
        // }
        this.paymentService.queryGetJurnal({
            page: this.page,
            size: this.itemsPerPage,
            // query: 'sts:' + this.idstatus + '|sortF:' + this.sortF + '|sortT:' + this.sortT + '|cari:' + this.currentSearch +
            //         '|fpayto:' + this.fSplCode
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/payment'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/payment', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/payment', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPayments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Payment) {
        return item.id;
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.jurnals = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    print(data: Payment) {
        const filter_data = 'idpay:' + data.idpay;
        if (data.valuta !== 'Rp') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer_usd/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer/pdf', { filterData: filter_data });
        }
    }
    reqApp(payment: Payment) {
        this.confirmatinService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Request Approve Payment Ini ??',
            accept: () => {
                this.paymentService.setStatus(payment, 29).subscribe(
                    (res) => this.loadAll()
                );
            }
        });

    }
    serahafin() {
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 51)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                    this.eventManager.broadcast({ name: 'registerpaymentdone', content: 'OK' });
                },
                (err) => {this.loadingService.loadingStop()}
            )
    }
    registerChangeInPayments() {
        this.eventSubscriber = this.eventManager.subscribe('registerpaymentdone', (response) => this.loadAll());
    }
    terimaFIN() {
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 59)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                    this.eventManager.broadcast({ name: 'registerpaymentdone', content: 'OK' });
                },
                (err) => {this.loadingService.loadingStop()}
            )
    }
    reqapp(rowData: Payment) {
        this.selected.push(rowData);
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 55)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                },
                (err) => {this.loadingService.loadingStop()}
            )
    }
    modalholdPay() {
        this.payment.ishold = 1;
        this.payment.holdreason = this.notehold;
        this.paymentService.setHold(this.payment)
            .subscribe(
                (res) => {
                    this.newModalNote = false;
                }
            )
    }
    holdPay(rowData: Payment) {
        this.newModalNote = true;
        this.payment = rowData;
    }
    showHoldPay(rowData: Payment) {
        this.confirmatinService.confirm({
            header: 'Hold Payment Voucher oleh ' + rowData.userhold,
            message: 'Catatan : \r\n ' + rowData.holdreason,
            accept: () => {}
        });
    }
    changeSort(event) {
        // -1 = asc
        // 1 = desc
        // if (!event.order) {
          this.sortF = event.field;
          this.sortT = event.order;
          console.log('ini adalah sortir = ', event);
        // }
    }
    unHoldPay(rowData: Payment) {
        rowData.ishold = 0;
        this.paymentService.setHold(rowData)
            .subscribe(
                (res) => {
                    this.loadAll();
                }
            )
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.fSplCode = this.newSuplier.kd_suppnew;
    }
    Reset() {
        this.fSplCode = '';
        this.loadAll();
    }
}
