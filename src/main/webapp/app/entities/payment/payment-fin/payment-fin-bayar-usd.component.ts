import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDateUtils } from 'ng-jhipster';
import { ConfirmationService } from 'primeng/primeng';
import { Payment, PaymentService } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterBank, MasterBankService } from '../../master-bank';
import { Invoice, InvoiceService } from '../../invoice';
import { InvoiceItem, InvoiceItemService } from '../../invoice-item';
import { PaymentItem, PaymentItemService } from '../../payment-item';
import { PaymentHistory, PaymentHistoryService } from '../../payment-history';

@Component({
    selector: 'jhi-payment-fin-bayar-usd',
    templateUrl: './payment-fin-bayar-usd.component.html'
})
export class PaymentFinBayarUSDComponent implements OnInit, OnDestroy {

    currentAccount: any;
    payment: Payment;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Payment[];
    dtpay: Date;
    dtrelease: Date;
    private subscription: Subscription;
    isview: boolean;
    selectedBank: MasterBank;
    banks: MasterBank[];
    totalpaid: number;
    invs: Invoice[];
    inv: Invoice;
    invitems: InvoiceItem[];
    payItems: PaymentItem[];
    isAll: boolean;
    strAll: string;
    cekByrPPN: boolean;
    cekByrPokok: boolean;
    cekByrtagihan: boolean;
    paidPPn: number;
    paidPokok: number;
    idpay: any;
    reload: string;
    constructor(
        private paymentService: PaymentService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private route: ActivatedRoute,
        private dateUtils: JhiDateUtils,
        private masterBankService: MasterBankService,
        private toasterService: ToasterService,
        private invoiceService: InvoiceService,
        private invoiceItemServices: InvoiceItemService,
        private paymentItemService: PaymentItemService,
        private paymentHistoryService: PaymentHistoryService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Payment>();
        this.payment = new Payment();
        this.dtpay = new Date();
        this.dtrelease = new Date();
        this.isview = false;
        this.selectedBank = new MasterBank();
        this.banks = new Array<MasterBank>();
        this.totalpaid = 0;
        this.invs = new Array<Invoice>();
        this.inv = new Invoice();
        this.invitems = new Array<InvoiceItem>();
        this.payItems = new Array<PaymentItem>();
        this.isAll = true;
        this.strAll = '';
        this.cekByrPPN = true;
        this.cekByrPokok = true;
        this.cekByrtagihan = true;
        this.paidPPn = 0;
        this.paidPokok = 0;
        this.reload = '';
    }

    load(id) {
        this.paymentService.getPayACCFINByTT(id).subscribe(
            (res: Payment) => {
                this.idpay = res.idpay;
                this.paymentItemService.findInvByIdPayUSD(res.idpay).subscribe(
                    (resItem: Invoice[]) => {
                        this.invs = resItem;
                        this.invs.forEach(
                            (e) => {
                                e.paynoppn = e.grandtotal - e.ppn;
                                if (e.totalpaid === 0 || e.totalpaid === null) {
                                    e.totalpaid = e.grandtotal;
                                }
                            }
                        );
                        this.checkIsAll();
                    }
                )
                this.payment = res;
                // this.paidPokok = this.payment.paynoppn;
                // this.paidPPn = this.payment.totalppn;
                if (this.payment.kurspay === null) {
                    this.payment.kurspay = 0;
                }
                // this.dtrelease = this.dateUtils
                //     .convertDateTimeFromServer(res.dtrelease);
                // this.dtpay = this.dateUtils
                //     .convertDateTimeFromServer(res.dtpay);
            }
        );
    }
    ngOnInit() {
        if (this.payment.paynoppn === this.payment.totalpaidnoppn) {
            this.cekByrPokok = false;
            this.payment.cekByrPokok = false;
        }
        if (this.payment.totalppn === this.payment.totalpaidppn) {
            this.cekByrPPN = false;
            this.payment.cekByrPPN = false;
        }
        if (this.payment.totalpaid === this.payment.totalpay) {
            this.cekByrtagihan = false;
            this.payment.cekByrtagihan = false;
        }
        this.masterBankService.queryComboBox()
            .subscribe(
                (res: ResponseWrapper) => {
                    this.banks = res.json;
                }
            )
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
            if (params['isview'] === 'true') {
                this.isview = true;
            }
            if (params['isview'] === 'false') {
                this.isview = false;
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }
    print(data: Payment) {
        const filter_data = 'idpay:' + data.idpay;
        if (data.valuta !== 'Rp') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer_usd/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer/pdf', { filterData: filter_data });
        }
    }
    updatepay() {
        this.loadingService.loadingStart();
        if (this.payment.totalppn > 0 && this.cekByrPPN === false && this.cekByrPokok === false) {
            this.toasterService.showToaster('Error', 'Pilih PPN/POKOK', 'PPN atau Pokok Belum di Checklist');
            return;
        }
        if (this.cekByrPokok === false) {
            this.cekByrPokok = false;
            this.payment.cekByrPokok = false;
        }
        if (this.cekByrPokok === true) {
            this.cekByrPokok = true;
            this.payment.cekByrPokok = true;
        }
        if (this.cekByrPPN === false) {
            this.cekByrPPN = false;
            this.payment.cekByrPPN = false;
        }
        if (this.cekByrPPN === true) {
            this.cekByrPPN = true;
            this.payment.cekByrPPN = true;
        }
        if (this.cekByrtagihan === true) {
            this.cekByrtagihan = true;
            this.payment.cekByrtagihan = true;
        }
        if (this.cekByrtagihan === false) {
            this.cekByrtagihan = false;
            this.payment.cekByrtagihan = false;
        }
        if (this.payment.totalpaid > this.payment.totalpay) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Jumlah bayar lebih besar dari yang seharus nya. Apakah akan dilanjutkan ??',
                accept: () => {
                    // this.payment.totalpaid = this.totalpaid;
                    this.payment.dtpay = this.dtpay;
                    this.payment.dtrelease = this.dtrelease;
                    // this.payment.paynoppn = this.paidPokok;
                    this.paymentService.updateSetBayarUSD(this.payment, this.invs).subscribe(
                        (res) => {
                            this.reload = 'true';
                            this.backMainPage();
                            this.toasterService.showToaster('Error', 'Payment Dibayar', 'PV Dibayar');
                        }
                    )
                }
            });
        } else {
            // this.payment.totalpaid = this.totalpaid;
            this.payment.dtpay = this.dtpay;
            this.payment.dtrelease = this.dtrelease;
            this.paymentService.updateSetBayarUSD(this.payment, this.invs).subscribe(
                (res) => {
                    this.reloadChild('');
                    this.reload = 'true';
                    this.backMainPage();
                    this.toasterService.showToaster('Error', 'Payment Dibayar', 'PV Dibayar');
                }
            )
        }
        this.loadingService.loadingStop();
    }
    backMainPage() {
        // this.router.navigate(['/payment-fin']);
        // window.location.reload();
        this.loadingService.loadingStop();
        window.history.back();
    }
    backMainPage2() {
        window.history.back();
    }
    getInvoice(idInv: any) {
        this.invoiceService.findDistinct(idInv).subscribe(
            (res: Invoice) => {
                this.invs.push(res);
                console.log('ini adalah data inv push -- ', this.inv);
            }
        )
    }
    trackNopoItem(index, item: InvoiceItem) {
        return item.nopo;
    }
    checkIsAll() {
        if (this.isAll === true) {
            this.isAll = true;
            this.strAll = 'Yes'
            const _totalPay = this.payment.totalpay;
            const _totalPaid = this.payment.totalpaid;
            if (this.payment.additionalcosts2 === undefined) {
                this.payment.additionalcosts2 = 0;
            }
            const _addtionalcos =  this.payment.additionalcosts2;
            this.totalpaid = _totalPay - _totalPaid - _addtionalcos;
            if (this.invs !== null) {
                this.payment.paynoppn = 0;
                this.invs.forEach(
                    (e) => {
                        if (this.payment.valuta !== 'Rp') {
                            // if (this.payment.paynoppn === 0 || this.payment.paynoppn === null) {
                                this.payment.paynoppn += e.grandtotal - e.ppn;
                                // this.payment.totalppn += e.ppn;
                            // }
                        }
                    }
                )
            }
        }
        if (this.isAll === false) {
            this.isAll = false;
            this.strAll = 'No'
            this.payment.totalppn = 0;
            this.payment.paynoppn = 0;
            this.payINV(null);
        }
    }
    payINV(rowData: Invoice) {
        this.totalpaid = 0;
        if (this.payment.additionalcosts2 === undefined) {
            this.payment.additionalcosts2 = 0;
        }
        const _addtionalcos =  this.payment.additionalcosts2;
        this.invs.forEach(
            (e) => {
                this.totalpaid += e.totalpaid
            }
        );
        this.totalpaid = this.totalpaid - _addtionalcos;
    }
    cekppn(rowData: Invoice) {
        if (rowData !== null) {
            if (rowData.checkppn === true) {
                rowData.totalpaid += rowData.ppn;
            } else if (rowData.checkppn === false) {
                rowData.totalpaid -= rowData.ppn;
            }
            this.payINV(rowData);
            this.payment.totalppn = 0;
            this.invs.forEach(
                (e) => {
                    this.payment.totalppn += e.ppn;
                }
            )
        }
    }
    cekpokok(rowData: Invoice) {
        if (rowData !== null) {
            if (rowData.checkpokok === true) {
                rowData.totalpaid += rowData.paynoppn;
            } else if (rowData.checkpokok === false) {
                rowData.totalpaid -= rowData.paynoppn;
            }
            this.payINV(rowData);
            this.payment.paynoppn = 0;
            this.invs.forEach(
                (e) => {
                    this.payment.paynoppn += e.grandtotal - e.ppn;
                }
            )
        }
    }
    cekbayarppn() {
        if (this.cekByrPPN === false) {
            this.payment.cekByrPPN = false;
            this.totalpaid = this.payment.totalppn;
        }
        if (this.cekByrPPN === true) {
            this.payment.cekByrPPN = true;
            this.totalpaid = this.payment.totalppn;
        }
    }
    cekbayarpokok() {
        if (this.cekByrPokok === false) {
            this.payment.cekByrPokok = false;
            this.totalpaid = this.payment.totalppn;
        }
        if (this.cekByrPokok === true) {
            this.payment.cekByrPokok = true;
            this.totalpaid = this.payment.paynoppn;
        }
    }
    cekbayartagihan() {
        if (this.cekByrtagihan === false) {
            this.payment.cekByrtagihan = false;
            this.totalpaid = this.payment.totalpay;
        }
        if (this.cekByrtagihan === true) {
            this.payment.cekByrtagihan = true;
            this.totalpaid = this.payment.totalpay;
        }
    }
    update(paymentHistorie: PaymentHistory) {
        // console.log('ini adalah testing', paymentHistorie);
        this.paymentHistoryService.update(paymentHistorie).subscribe(
            (res) => {
                console.log('sukses');
                this.toasterService.showToaster('INFO', 'SUCCES', 'Data Berhasil diUpdate !!');
            }
        );
    }
    reloadChild(event) {
        console.log('sukses');
        this.toasterService.showToaster('INFO', 'SUCCES', 'Data Berhasil diUpdate !!');
    }
}
