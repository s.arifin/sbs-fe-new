import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { LoadingService } from '../../../layouts';
import { Account, Principal, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { TandaTerima, TandaTerimaService } from '../../tanda-terima';

@Component({
    selector: 'jhi-fin-tanda-terima',
    templateUrl: './fin-tanda-terima.component.html'
})
export class FinTandaTerimaComponent implements OnInit, OnDestroy {

    @Input() div: number
    currentAccount: Account;
    tandaTerimas: TandaTerima[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    dtfrom: Date;
    dtthru: Date;
    idInternal: string;
    radioValue: number;
    pembayarans: Array<object> = [
        { label: 'SEMUA', value: 0 },
        { label: 'Belum Bayar', value: 1 },
        { label: 'Sudah Bayar', value: 2 },
    ];
    constructor(
        private tandaTerimaService: TandaTerimaService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private masterSupplierService: MasterSupplierService,
        private loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.page = 0
        this.idInternal = '';
        this.radioValue = 0;
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.tandaTerimaService.queryMKT({
            page: this.page,
            size: this.itemsPerPage,
            query: 'cari:' + this.currentSearch + '|kdsup:' + this.kdsup + '|dtfrom:' + this.dtfrom.toISOString().split('T')[0] +
                    '|dtthru:' + this.dtthru.toISOString().split('T')[0] + '|jenis:' + this.radioValue
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/tanda-terima'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/tanda-terima', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/tanda-terima', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            this.idInternal = account.idInternal;
        });
        // this.registerChangeInTandaTerimas();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TandaTerima) {
        return item.id;
    }
    registerChangeInTandaTerimas() {
        this.eventSubscriber = this.eventManager.subscribe('tandaTerimaListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.tandaTerimas = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    printTT(rowData: TandaTerima) {
        const filter_data = 'idreceipt:' + rowData.idreceipt;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tandaterima/pdf', { filterData: filter_data });
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.kd_suppnew;
    }
    filter() {
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.kd_suppnew === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.kd_suppnew;
        }
        this.loadAll();
    }
    reset() {
        this.loadAll();
        this.kdsup = '';
    }
}
