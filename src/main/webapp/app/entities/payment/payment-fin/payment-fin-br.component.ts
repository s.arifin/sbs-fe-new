import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDateUtils } from 'ng-jhipster';
import { ConfirmationService } from 'primeng/primeng';
import { Payment, PaymentService } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterBank, MasterBankService } from '../../master-bank';
import { Invoice, InvoiceService } from '../../invoice';
import { InvoiceItem, InvoiceItemService } from '../../invoice-item';
import { PaymentItem, PaymentItemService } from '../../payment-item';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { JurnalPayment } from '../jurnal-payment.model';

@Component({
    selector: 'jhi-payment-fin-br',
    templateUrl: './payment-fin-br.component.html'
})
export class PaymentFinBRComponent implements OnInit, OnDestroy {

    currentAccount: any;
    payment: Payment;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Payment[];
    dtpay: Date;
    dtrelease: Date;
    private subscription: Subscription;
    isview: boolean;
    selectedBank: MasterBank;
    banks: MasterBank[];
    totalpaid: number;
    invs: Invoice[];
    inv: Invoice;
    invitems: InvoiceItem[];
    payItems: PaymentItem[];
    isAll: boolean;
    strAll: string;
    cekByrPPN: boolean;
    cekByrPokok: boolean;
    paidPPn: number;
    paidPokok: number;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    constructor(
        private paymentService: PaymentService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private route: ActivatedRoute,
        private dateUtils: JhiDateUtils,
        private masterBankService: MasterBankService,
        private toasterService: ToasterService,
        private invoiceService: InvoiceService,
        private invoiceItemServices: InvoiceItemService,
        private paymentItemService: PaymentItemService,
        private masterSupplierService: MasterSupplierService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Payment>();
        this.payment = new Payment();
        this.dtpay = new Date();
        this.dtrelease = new Date();
        this.isview = false;
        this.selectedBank = new MasterBank();
        this.banks = new Array<MasterBank>();
        this.totalpaid = 0;
        this.invs = new Array<Invoice>();
        this.inv = new Invoice();
        this.invitems = new Array<InvoiceItem>();
        this.payItems = new Array<PaymentItem>();
        this.isAll = true;
        this.strAll = '';
        this.cekByrPPN = true;
        this.cekByrPokok = true;
        this.paidPPn = 0;
        this.paidPokok = 0;
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
    }

    load(id) {
        this.paymentService.queryGetJurnalBRById(id)
        .subscribe(
            (res: JurnalPayment) => {
                this.payment.nopayfin = res.nobukti;
                this.payment.bankcode = res.kodebank;
                this.dtpay = res.tanggal;
                this.payment.nogiro = res.nogiro;
                this.totalpaid = res.kredit;
                this.masterSupplierService.findByCode(res.kdsupplier).subscribe(
                    (ress: MasterSupplier) => this.newSuplier = ress
                );
                this.payment.note = res.ket1;
            }
        )
    }
    ngOnInit() {
        this.masterBankService.queryComboBox()
            .subscribe(
                (res: ResponseWrapper) => {
                    this.banks = res.json;
                }
            )
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
            if (params['isview'] === 'true') {
                this.isview = true;
            }
            if (params['isview'] === 'false') {
                this.isview = false;
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }
    print(data: Payment) {
        const filter_data = 'idpay:' + data.idpay;
        if (data.valuta !== 'Rp') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer_usd/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer/pdf', { filterData: filter_data });
        }
    }
    updatepay() {
        if (this.payment.bankcode === null || this.payment.bankcode === undefined) {
            alert('Nama bank tidak boleh kosong')
        } else if (this.payment.payto === null || this.payment.payto === undefined) {
            alert('Nama relasi tidak boleh kosong')
        } else if (this.totalpaid === null || this.totalpaid === undefined) {
            alert('Total Nilai tidak boleh kosong')
        } else {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah yakin untuk simpan data ??',
                accept: () => {
                    this.payment.totalpaid = this.totalpaid;
                    this.payment.dtpay = this.dtpay;
                    this.payment.dtrelease = this.dtrelease;
                    this.payment.jenis = 'BR';
                    this.loadingService.loadingStart();
                    this.paymentService.createBPBR(this.payment).subscribe(
                        (res) => {
                            this.backMainPage();
                            this.loadingService.loadingStop();
                        }
                    )
                }
            });
        }
    }
    backMainPage() {
        this.router.navigate(['/payment-fin-jurnal']);
    }
    getInvoice(idInv: any) {
        this.invoiceService.findDistinct(idInv).subscribe(
            (res: Invoice) => {
                this.invs.push(res);
                console.log('ini adalah data inv push -- ', this.inv);
            }
        )
    }
    trackNopoItem(index, item: InvoiceItem) {
        return item.nopo;
    }
    checkIsAll() {
        if (this.isAll === true) {
            this.isAll = true;
            this.strAll = 'Yes'
            const _totalPay = this.payment.totalpay;
            const _totalPaid = this.payment.totalpaid;
            this.totalpaid = _totalPay - _totalPaid;
            if (this.invs !== null) {
                this.invs.forEach(
                    (e) => {
                        if (this.payment.valuta !== 'Rp') {
                                this.payment.totalpaidnoppn += e.grandtotal - e.ppn;
                                this.payment.totalpaidppn += e.ppn;
                        }
                    }
                )
            }
        }
        if (this.isAll === false) {
            this.isAll = false;
            this.strAll = 'No'
            this.payment.totalpaidppn = 0;
            this.payment.totalpaidnoppn = 0;
            this.payINV(null);
        }
    }
    payINV(rowData: Invoice) {
        this.totalpaid = 0;
        this.invs.forEach(
            (e) => {
                this.totalpaid += e.totalpaid
            }
        )
    }
    cekppn(rowData: Invoice) {
        if (rowData !== null) {
            if (rowData.checkppn === true) {
                rowData.totalpaid += rowData.ppn;
            } else if (rowData.checkppn === false) {
                rowData.totalpaid -= rowData.ppn;
            }
            this.payINV(rowData);
            this.payment.totalpaidppn = 0;
            this.invs.forEach(
                (e) => {
                    this.payment.totalpaidppn += e.ppn;
                }
            )
        }
    }
    cekpokok(rowData: Invoice) {
        if (rowData !== null) {
            if (rowData.checkpokok === true) {
                rowData.totalpaid += rowData.paynoppn;
            } else if (rowData.checkpokok === false) {
                rowData.totalpaid -= rowData.paynoppn;
            }
            this.payINV(rowData);
            this.payment.totalpaidnoppn = 0;
            this.invs.forEach(
                (e) => {
                    this.payment.totalpaidnoppn += e.grandtotal - e.ppn;
                }
            )
        }
    }
    cekbayarppn() {
        if (this.cekByrPPN === false) {
            this.totalpaid = this.totalpaid - this.payment.totalpaidppn;
        }
        if (this.cekByrPPN === true) {
            this.totalpaid = this.totalpaid + this.payment.totalpaidppn;
        }
    }
    cekbayarpokok() {
        if (this.cekByrPokok === false) {
            this.totalpaid = this.totalpaid - this.payment.totalpaidnoppn;
        }
        if (this.cekByrPokok === true) {
            this.totalpaid = this.totalpaid + this.payment.totalpaidnoppn;
        }
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.currentAccount.idInternal).then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.currentAccount.idInternal).then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.payment.payto = this.newSuplier.kd_suppnew;
        this.payment.paytoname = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
    }

}
