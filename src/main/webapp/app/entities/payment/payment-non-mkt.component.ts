import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Payment, PaymentService } from '.';
import { LoadingService } from '../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, Account } from '../../shared';
import { JhiAlertService } from 'ng-jhipster/src/service/alert.service';
import { JhiEventManager } from 'ng-jhipster/src/service/event-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';

@Component({
    selector: 'jhi-mkt-non-payment',
    templateUrl: './payment-non-mkt.component.html'
})
export class PaymentNonMKTComponent implements OnInit, OnDestroy {

    @Input() idstatus: number
    currentAccount: Account;
    payments: Payment[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Payment[];
    newModalNote: boolean;
    notehold: string;
    login: string;
    payment: Payment;
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    newModalCancelNote: boolean;
    cancelNote: string;
    types: Array<object> = [
        { label: 'all', value: 0 },
        { label: 'Uang Muka', value: 1 },
        { label: 'Downpayment', value: 2 },
        { label: 'Pelunasan', value: 3 },
        { label: 'kasnegara', value: 4 },
        { label: 'PPJK', value: 5 }
    ];
    statuses: Array<object> = [
        { value: 0, label: 'ALL' },
        { value: 10, label: 'New' },
        { value: 11, label: 'Approved Manager Departement' },
        { value: 12, label: 'Not Approved' },
        { value: 13, label: 'Canceled' },
        { value: 16, label: 'Done' },
        { value: 29, label: 'Request Approve' },
        { value: 53, label: 'Sudah Dibayar' },
        { value: 54, label: 'Request Approve Accounting' },
        { value: 56, label: 'Approve Accounting' },
        { value: 58, label: 'Terima Accounting' },
        { value: 59, label: 'Terima Finance' },
    ];
    selectedType: number;
    selectedStatus: number;
    constructor(
        private paymentService: PaymentService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
        private confirmatinService: ConfirmationService,
        private masterSupplierService: MasterSupplierService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Payment>();
        this.newModalNote = false;
        this.notehold = '';
        this.login = '';
        this.payment = new Payment();
        this.currentAccount = new Account();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.newModalCancelNote = false;
        this.cancelNote = '';
        this.selectedType = 0;
        this.selectedStatus = 0;
        if (this.idstatus === undefined || this.idstatus === null) {
            this.idstatus = 10;
        } else {
            this.idstatus = this.idstatus;
        }
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.paymentService.query({
            page: this.page,
            size: this.itemsPerPage,
            query: 'status:' + this.idstatus + '|cari:' + this.currentSearch + '|kdsup:' + this.kdsup
                + '|dtfrom:' + this.dtfrom.toISOString().split('T')[0] + '|dtthru:' + this.dtthru.toISOString().split('T')[0]
                + '|type:' + this.selectedType + '|statuses:' + this.selectedStatus
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/mkt-non-payment'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/mkt-non-payment', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/payment', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPayments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Payment) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;

        this.payments = data;
        this.payments = this.payments.filter((item) => item.payment_type.id === 61);
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    print(data: Payment) {
        const filter_data = 'idpay:' + data.idpay;
        if (data.valuta !== 'Rp') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer_usd/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer/pdf', { filterData: filter_data });
        }
    }
    // reqApp(payment: Payment) {
    //     this.confirmationService.confirm({
    //         header: 'Information',
    //         message: 'Apakah Yakin Untuk Request Approve Payment Ini ??',
    //         accept: () => {
    //             this.paymentService.setStatus(payment, 29).subscribe(
    //                 (res) => this.loadAll()
    //             );
    //         }
    //     });

    // }
    reqapp(rowData: Payment) {
        this.selected.push(rowData);
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 29)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                },
                (err) => { this.loadingService.loadingStop() }
            )
    }
    bypasFIN(rowData: Payment) {
        this.selected.push(rowData);
        this.loadingService.loadingStart();
        this.paymentService.bypasstofin(this.selected)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                },
                (err) => { this.loadingService.loadingStop() }
            )
    }
    serahacc() {
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 58)
            .subscribe(
                (res) => {
                    this.paymentService.bypasstofin(this.selected).subscribe((res2) => {
                        this.loadAll();
                        this.selected = new Array<Payment>();
                        this.loadingService.loadingStop();
                        this.eventManager.broadcast({ name: 'registerpaymentdone', content: 'OK' });
                    });
                },
                (err) => { this.loadingService.loadingStop() }
            )
    }
    serahfin() {
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 51)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                    this.eventManager.broadcast({ name: 'registerpaymentdone', content: 'OK' });
                },
                (err) => { this.loadingService.loadingStop() }
            )
    }
    sudahbayar() {
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 53)
            .subscribe(
                (res) => {
                    this.loadAll();
                    this.selected = new Array<Payment>();
                    this.loadingService.loadingStop();
                    this.eventManager.broadcast({ name: 'registerpaymentdone', content: 'OK' });
                },
                (err) => { this.loadingService.loadingStop() }
            )
    }
    registerChangeInPayments() {
        this.eventSubscriber = this.eventManager.subscribe('registerparegisterpaymentdoneymentdone', (response) => this.loadAll());
    }
    modalholdPay() {
        this.payment.ishold = 1;
        this.payment.holdreason = this.notehold;
        this.paymentService.setHold(this.payment)
            .subscribe(
                (res) => {
                    this.newModalNote = false;
                }
            )
    }
    holdPay(rowData: Payment) {
        this.newModalNote = true;
        this.payment = rowData;
    }
    showHoldPay(rowData: Payment) {
        this.confirmatinService.confirm({
            header: 'Hold Payment Voucher oleh ' + rowData.userhold,
            message: 'Catatan : \r\n ' + rowData.holdreason,
            accept: () => { }
        });
    }
    unHoldPay() {
        this.payment.ishold = 0;
        this.paymentService.setHold(this.payment)
            .subscribe(
                (res) => {
                    this.newModalNote = false;
                }
            )
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.kd_suppnew;
    }
    filter() {
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.kd_suppnew === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.kd_suppnew;
        }
        this.loadAll();
    }
    reset() {
        this.loadAll();
        this.kdsup = '';
    }
    cancelPay(rowData: Payment) {
        this.payment = rowData;
        this.newModalCancelNote = true;
    }
    modalCacelPay() {
        this.payment.cancelnote = this.cancelNote;
        this.confirmatinService.confirm({
            header: 'Cancel Payment',
            message: 'Apakah Anda Yakin Cancel Payment No Tanda Terima  ' + this.payment.receiptno,
            accept: () => {
                this.paymentService.setStatus(this.payment, 13)
                    .subscribe(
                        (res) => {
                            this.loadAll();
                            this.newModalCancelNote = false;
                            this.cancelNote = '';
                        }
                    );
            }
        });
    }
    cekCancelNote(rowData: Payment) {
        this.confirmatinService.confirm({
            header: 'Catatan Cancel Payment',
            message: rowData.cancelnote,
            accept: () => { }
        });
    }

    serahaccMKT() {
        this.loadingService.loadingStart();
        this.paymentService.setStatusBulk(this.selected, 29)
            .subscribe(
                (res) => {
                    this.paymentService.setStatusBulk(this.selected, 11).subscribe((res2) => {
                        this.loadAll();
                        this.selected = new Array<Payment>();
                        this.loadingService.loadingStop();
                        this.eventManager.broadcast({ name: 'registerpaymentdone', content: 'OK' });
                    });
                },
                (err) => { this.loadingService.loadingStop() }
            )
    }
}
