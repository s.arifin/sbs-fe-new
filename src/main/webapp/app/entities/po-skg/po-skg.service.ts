import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { PoSKG } from './po-skg.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PoSKGService {

    private resourceUrl = process.env.API_C_URL + '/api/po-skgs';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/po-skgs';

    constructor(private http: Http) { }

    create(poSKG: PoSKG): Observable<PoSKG> {
        const copy = this.convert(poSKG);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(poSKG: PoSKG): Observable<PoSKG> {
        const copy = this.convert(poSKG);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<PoSKG> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(poSKG: PoSKG): PoSKG {
        const copy: PoSKG = Object.assign({}, poSKG);
        return copy;
    }
    getDetail(donum: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/get-detail/' + donum)
            .map((res: Response) => this.convertResponse(res));
    }

    approve(poSKG: PoSKG): Observable<PoSKG> {
        const copy = this.convert(poSKG);
        return this.http.put(this.resourceUrl + '/approve', copy).map((res: Response) => {
            return res.json();
        });
    }
}
