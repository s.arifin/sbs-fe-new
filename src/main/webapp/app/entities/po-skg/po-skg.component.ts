import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PoSKG } from './po-skg.model';
import { PoSKGService } from './po-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { load } from '@amcharts/amcharts4/.internal/core/utils/Net';
import { LoadingService } from '../../layouts';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-po-skg',
    templateUrl: './po-skg.component.html'
})
export class PoSKGComponent implements OnInit, OnDestroy {

    @Input() jenis: string;
    currentAccount: any;
    poSKGS: PoSKG[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private poSKGService: PoSKGService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.jenis = '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.jenis !== '') {
            this.poSKGService.query({
                page: this.page - 1,
                size: this.itemsPerPage,
                query: 'jenis:' + this.jenis
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/po-skg'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                query: 'jenis:' + this.jenis
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/po-skg', {
            page: this.page,
            query: 'jenis:' + this.jenis
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/po-skg', {
            search: this.currentSearch,
            page: this.page,
            query: 'jenis:' + this.jenis
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPoSKGS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PoSKG) {
        return item.id;
    }
    registerChangeInPoSKGS() {
        this.eventSubscriber = this.eventManager.subscribe('poSKGListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.poSKGS = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop()
    }

    todetail(data: PoSKG) {
        this.router.navigate(['/po-skg/' + data.DO_NUM]);
    }

    approve(data: PoSKG) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Approve PO ' + data.sor + ' ??',
            accept: () => {
                this.loadingService.loadingStart();
                this.poSKGService.approve(data)
                    .subscribe(
                        (res: PoSKG) => {this.loadAll(), this.loadingService.loadingStop()},
                        (res: PoSKG) => {this.loadAll(), this.loadingService.loadingStop()},
                    )
            }
        });
    }
}
