import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PoSKGComponent } from './po-skg.component';
import { PoSKGDetailComponent } from './po-skg-detail.component';
import { PoSKGPopupComponent } from './po-skg-dialog.component';
import { PoSKGDeletePopupComponent } from './po-skg-delete-dialog.component';
import { TabPoSkgComponent } from './tab-po-skg.component';

@Injectable()
export class PoSKGResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const poSKGRoute: Routes = [
    {
        path: 'po-skg',
        component: PoSKGComponent,
        resolve: {
            'pagingParams': PoSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.poSKG.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'po-skg/:id',
        component: PoSKGDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.poSKG.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-po-skg',
        component: TabPoSkgComponent,
        resolve: {
            'pagingParams': PoSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.poSKG.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const poSKGPopupRoute: Routes = [
    {
        path: 'po-skg-new',
        component: PoSKGPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.poSKG.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'po-skg/:id/edit',
        component: PoSKGPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.poSKG.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'po-skg/:id/delete',
        component: PoSKGDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.poSKG.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
