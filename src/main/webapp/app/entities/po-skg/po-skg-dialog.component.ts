import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PoSKG } from './po-skg.model';
import { PoSKGPopupService } from './po-skg-popup.service';
import { PoSKGService } from './po-skg.service';

@Component({
    selector: 'jhi-po-skg-dialog',
    templateUrl: './po-skg-dialog.component.html'
})
export class PoSKGDialogComponent implements OnInit {

    poSKG: PoSKG;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private poSKGService: PoSKGService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.poSKG.id !== undefined) {
            this.subscribeToSaveResponse(
                this.poSKGService.update(this.poSKG));
        } else {
            this.subscribeToSaveResponse(
                this.poSKGService.create(this.poSKG));
        }
    }

    private subscribeToSaveResponse(result: Observable<PoSKG>) {
        result.subscribe((res: PoSKG) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PoSKG) {
        this.eventManager.broadcast({ name: 'poSKGListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-po-skg-popup',
    template: ''
})
export class PoSKGPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private poSKGPopupService: PoSKGPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.poSKGPopupService
                    .open(PoSKGDialogComponent as Component, params['id']);
            } else {
                this.poSKGPopupService
                    .open(PoSKGDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
