import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PoSKGService,
    PoSKGPopupService,
    PoSKGComponent,
    PoSKGDetailComponent,
    PoSKGDialogComponent,
    PoSKGPopupComponent,
    PoSKGDeletePopupComponent,
    PoSKGDeleteDialogComponent,
    poSKGRoute,
    poSKGPopupRoute,
    PoSKGResolvePagingParams,
} from './';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    DataListModule
} from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TabPoSkgComponent } from './tab-po-skg.component';

const ENTITY_STATES = [
    ...poSKGRoute,
    ...poSKGPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        DataListModule
    ],
    declarations: [
        PoSKGComponent,
        PoSKGDetailComponent,
        PoSKGDialogComponent,
        PoSKGDeleteDialogComponent,
        PoSKGPopupComponent,
        PoSKGDeletePopupComponent,
        TabPoSkgComponent
    ],
    entryComponents: [
        PoSKGComponent,
        PoSKGDialogComponent,
        PoSKGPopupComponent,
        PoSKGDeleteDialogComponent,
        PoSKGDeletePopupComponent,
        TabPoSkgComponent
    ],
    providers: [
        PoSKGService,
        PoSKGPopupService,
        PoSKGResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPoSKGModule {}
