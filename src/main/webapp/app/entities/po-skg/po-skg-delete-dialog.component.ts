import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PoSKG } from './po-skg.model';
import { PoSKGPopupService } from './po-skg-popup.service';
import { PoSKGService } from './po-skg.service';

@Component({
    selector: 'jhi-po-skg-delete-dialog',
    templateUrl: './po-skg-delete-dialog.component.html'
})
export class PoSKGDeleteDialogComponent {

    poSKG: PoSKG;

    constructor(
        private poSKGService: PoSKGService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.poSKGService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'poSKGListModification',
                content: 'Deleted an poSKG'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-po-skg-delete-popup',
    template: ''
})
export class PoSKGDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private poSKGPopupService: PoSKGPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.poSKGPopupService
                .open(PoSKGDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
