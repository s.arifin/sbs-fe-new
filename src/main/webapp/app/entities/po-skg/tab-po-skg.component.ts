import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'jhi-tab-po-skg',
    templateUrl: './tab-po-skg.component.html'
})
export class TabPoSkgComponent implements OnInit, OnDestroy {
    PUS: string;
    TMBH: string;
    constructor() {
        this.PUS = 'PUS';
        this.TMBH = 'TMBH';
    }
    ngOnInit(): void {
        // throw new Error('Method not implemented.');
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
}
