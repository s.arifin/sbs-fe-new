import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { DetailPOSkg, PoSKG } from './po-skg.model';
import { PoSKGService } from './po-skg.service';
import { ResponseWrapper } from '../../shared';
import { LoadingService } from '../../layouts';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-po-skg-detail',
    templateUrl: './po-skg-detail.component.html'
})
export class PoSKGDetailComponent implements OnInit, OnDestroy {

    detailPoSKG: DetailPOSkg[];
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    donum: string;
    constructor(
        private eventManager: JhiEventManager,
        private poSKGService: PoSKGService,
        private route: ActivatedRoute,
        private alertService: JhiAlertService,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService
    ) {
        this.detailPoSKG = new Array<DetailPOSkg>();
        this.donum = '';
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
            this.donum = params['id'];
        });
        this.registerChangeInPoSKGS();
    }

    load(id) {
        this.loadingService.loadingStart();
        this.poSKGService.getDetail(id).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPoSKGS() {
        // this.eventSubscriber = this.eventManager.subscribe(
        //     'poSKGListModification',
        //     (response) => this.load(this.poSKG.id)
        // );
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.detailPoSKG = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }
    back() {
        window.history.back();
    }
    // approve() {
    //     this.confirmationService.confirm({
    //         header: 'Information',
    //         message: 'Apakah Yakin Untuk Approve PO ??',
    //         accept: () => {
    //             this.loadingService.loadingStart();
    //             this.poSKGService.approve(this.donum)
    //                 .subscribe(
    //                     (res: PoSKG) => { this.back(), this.loadingService.loadingStop() },
    //                     (res: PoSKG) => { this.back(), this.loadingService.loadingStop() },
    //                 )
    //         }
    //     });
    // }
}
