import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { CalendarType } from './calendar-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class CalendarTypeService {
    protected itemValues: CalendarType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    // protected resourceUrl = process.env.API_C_URL + '/api/calendar-types';
    protected resourceUrl = process.env.API_C_URL + '/api/calendar-types';
    protected resourceSearchUrl = 'api/_search/calendar-types';

    constructor(protected http: Http) { }

    create(calendarType: CalendarType): Observable<CalendarType> {
        const copy = this.convert(calendarType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(calendarType: CalendarType): Observable<CalendarType> {
        const copy = this.convert(calendarType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<CalendarType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, calendarType: CalendarType): Observable<CalendarType> {
        const copy = this.convert(calendarType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, calendarTypes: CalendarType[]): Observable<CalendarType[]> {
        const copy = this.convertList(calendarTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(calendarType: CalendarType): CalendarType {
        if (calendarType === null || calendarType === {}) {
            return {};
        }
        // const copy: CalendarType = Object.assign({}, calendarType);
        const copy: CalendarType = JSON.parse(JSON.stringify(calendarType));
        return copy;
    }

    protected convertList(calendarTypes: CalendarType[]): CalendarType[] {
        const copy: CalendarType[] = calendarTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: CalendarType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
