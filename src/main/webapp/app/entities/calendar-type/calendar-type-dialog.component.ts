import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {CalendarType} from './calendar-type.model';
import {CalendarTypePopupService} from './calendar-type-popup.service';
import {CalendarTypeService} from './calendar-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-calendar-type-dialog',
    templateUrl: './calendar-type-dialog.component.html'
})
export class CalendarTypeDialogComponent implements OnInit {

    calendarType: CalendarType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected calendarTypeService: CalendarTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.calendarType.idCalendarType !== undefined) {
            this.subscribeToSaveResponse(
                this.calendarTypeService.update(this.calendarType));
        } else {
            this.subscribeToSaveResponse(
                this.calendarTypeService.create(this.calendarType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CalendarType>) {
        result.subscribe((res: CalendarType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: CalendarType) {
        this.eventManager.broadcast({ name: 'calendarTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'calendarType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'calendarType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-calendar-type-popup',
    template: ''
})
export class CalendarTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected calendarTypePopupService: CalendarTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.calendarTypePopupService
                    .open(CalendarTypeDialogComponent as Component, params['id']);
            } else {
                this.calendarTypePopupService
                    .open(CalendarTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
