import { BaseEntity } from './../../shared';

export class CalendarType implements BaseEntity {
    constructor(
        public id?: number,
        public idCalendarType?: number,
        public description?: string,
    ) {
    }
}
