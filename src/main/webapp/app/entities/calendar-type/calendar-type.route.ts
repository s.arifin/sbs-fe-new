import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CalendarTypeComponent } from './calendar-type.component';
import { CalendarTypePopupComponent } from './calendar-type-dialog.component';

@Injectable()
export class CalendarTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCalendarType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const calendarTypeRoute: Routes = [
    {
        path: 'calendar-type',
        component: CalendarTypeComponent,
        resolve: {
            'pagingParams': CalendarTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const calendarTypePopupRoute: Routes = [
    {
        path: 'calendar-type-new',
        component: CalendarTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'calendar-type/:id/edit',
        component: CalendarTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
