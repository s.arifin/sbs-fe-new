import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PurposeType} from './purpose-type.model';
import {PurposeTypePopupService} from './purpose-type-popup.service';
import {PurposeTypeService} from './purpose-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-purpose-type-dialog',
    templateUrl: './purpose-type-dialog.component.html'
})
export class PurposeTypeDialogComponent implements OnInit {

    purposeType: PurposeType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected purposeTypeService: PurposeTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.purposeType.idPurposeType !== undefined) {
            this.subscribeToSaveResponse(
                this.purposeTypeService.update(this.purposeType));
        } else {
            this.subscribeToSaveResponse(
                this.purposeTypeService.create(this.purposeType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PurposeType>) {
        result.subscribe((res: PurposeType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PurposeType) {
        this.eventManager.broadcast({ name: 'purposeTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'purposeType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'purposeType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-purpose-type-popup',
    template: ''
})
export class PurposeTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected purposeTypePopupService: PurposeTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.purposeTypePopupService
                    .open(PurposeTypeDialogComponent as Component, params['id']);
            } else {
                this.purposeTypePopupService
                    .open(PurposeTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
