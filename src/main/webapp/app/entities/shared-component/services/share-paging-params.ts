import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

@Injectable()
export class UnitDeliverableResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idDeliverable,asc';
        const sortref = route.queryParams['sortref'] ? route.queryParams['sortref'] : 'refNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort),
            predicateref: this.paginationUtil.parsePredicate(sortref),
            ascendingref: this.paginationUtil.parseAscending(sortref)
      };
    }
}
