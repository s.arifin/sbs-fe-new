import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    MasterBankService,
    MasterBankPopupService,
    MasterBankComponent,
    MasterBankDetailComponent,
    MasterBankDialogComponent,
    MasterBankPopupComponent,
    MasterBankDeletePopupComponent,
    MasterBankDeleteDialogComponent,
    masterBankRoute,
    masterBankPopupRoute,
    MasterBankResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...masterBankRoute,
    ...masterBankPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MasterBankComponent,
        MasterBankDetailComponent,
        MasterBankDialogComponent,
        MasterBankDeleteDialogComponent,
        MasterBankPopupComponent,
        MasterBankDeletePopupComponent,
    ],
    entryComponents: [
        MasterBankComponent,
        MasterBankDialogComponent,
        MasterBankPopupComponent,
        MasterBankDeleteDialogComponent,
        MasterBankDeletePopupComponent,
    ],
    providers: [
        MasterBankService,
        MasterBankPopupService,
        MasterBankResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterBankModule {}
