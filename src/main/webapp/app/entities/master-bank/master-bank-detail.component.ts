import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { MasterBank } from './master-bank.model';
import { MasterBankService } from './master-bank.service';

@Component({
    selector: 'jhi-master-bank-detail',
    templateUrl: './master-bank-detail.component.html'
})
export class MasterBankDetailComponent implements OnInit, OnDestroy {

    masterBank: MasterBank;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private masterBankService: MasterBankService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMasterBanks();
    }

    load(id) {
        this.masterBankService.find(id).subscribe((masterBank) => {
            this.masterBank = masterBank;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMasterBanks() {
        this.eventSubscriber = this.eventManager.subscribe(
            'masterBankListModification',
            (response) => this.load(this.masterBank.id)
        );
    }
}
