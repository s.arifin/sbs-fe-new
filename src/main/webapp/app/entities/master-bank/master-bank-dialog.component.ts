import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MasterBank } from './master-bank.model';
import { MasterBankPopupService } from './master-bank-popup.service';
import { MasterBankService } from './master-bank.service';

@Component({
    selector: 'jhi-master-bank-dialog',
    templateUrl: './master-bank-dialog.component.html'
})
export class MasterBankDialogComponent implements OnInit {

    masterBank: MasterBank;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private masterBankService: MasterBankService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.masterBank.id !== undefined) {
            this.subscribeToSaveResponse(
                this.masterBankService.update(this.masterBank));
        } else {
            this.subscribeToSaveResponse(
                this.masterBankService.create(this.masterBank));
        }
    }

    private subscribeToSaveResponse(result: Observable<MasterBank>) {
        result.subscribe((res: MasterBank) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: MasterBank) {
        this.eventManager.broadcast({ name: 'masterBankListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-master-bank-popup',
    template: ''
})
export class MasterBankPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private masterBankPopupService: MasterBankPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.masterBankPopupService
                    .open(MasterBankDialogComponent as Component, params['id']);
            } else {
                this.masterBankPopupService
                    .open(MasterBankDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
