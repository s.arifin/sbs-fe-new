import { BaseEntity } from './../../shared';

export class MasterBank implements BaseEntity {
    constructor(
        public id?: number,
        public bankcode?: string,
        public bankname?: string,
    ) {
    }
}
