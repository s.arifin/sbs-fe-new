import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MasterBankComponent } from './master-bank.component';
import { MasterBankDetailComponent } from './master-bank-detail.component';
import { MasterBankPopupComponent } from './master-bank-dialog.component';
import { MasterBankDeletePopupComponent } from './master-bank-delete-dialog.component';

@Injectable()
export class MasterBankResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const masterBankRoute: Routes = [
    {
        path: 'master-bank',
        component: MasterBankComponent,
        resolve: {
            'pagingParams': MasterBankResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterBank.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'master-bank/:id',
        component: MasterBankDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterBank.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const masterBankPopupRoute: Routes = [
    {
        path: 'master-bank-new',
        component: MasterBankPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterBank.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-bank/:id/edit',
        component: MasterBankPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterBank.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-bank/:id/delete',
        component: MasterBankDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterBank.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
