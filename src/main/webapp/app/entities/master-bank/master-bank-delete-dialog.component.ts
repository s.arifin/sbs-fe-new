import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MasterBank } from './master-bank.model';
import { MasterBankPopupService } from './master-bank-popup.service';
import { MasterBankService } from './master-bank.service';

@Component({
    selector: 'jhi-master-bank-delete-dialog',
    templateUrl: './master-bank-delete-dialog.component.html'
})
export class MasterBankDeleteDialogComponent {

    masterBank: MasterBank;

    constructor(
        private masterBankService: MasterBankService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.masterBankService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'masterBankListModification',
                content: 'Deleted an masterBank'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-master-bank-delete-popup',
    template: ''
})
export class MasterBankDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private masterBankPopupService: MasterBankPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.masterBankPopupService
                .open(MasterBankDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
