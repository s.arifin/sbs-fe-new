import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    PeriodTypeService,
} from './';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

@NgModule({
    imports: [
        MpmSharedModule,
    ],
    exports: [
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        PeriodTypeService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPeriodTypeModule {}
