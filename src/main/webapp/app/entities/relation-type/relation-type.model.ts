import { BaseEntity } from './../../shared';

export class RelationType implements BaseEntity {
    constructor(
        public id?: number,
        public idRelationType?: number,
        public description?: string,
    ) {
    }
}
