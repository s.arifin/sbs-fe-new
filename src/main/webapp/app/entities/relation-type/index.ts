export * from './relation-type.model';
export * from './relation-type-popup.service';
export * from './relation-type.service';
export * from './relation-type-dialog.component';
export * from './relation-type.component';
export * from './relation-type.route';
