import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    RelationTypeService,
    RelationTypePopupService,
    RelationTypeComponent,
    RelationTypeDialogComponent,
    RelationTypePopupComponent,
    relationTypeRoute,
    relationTypePopupRoute,
    RelationTypeResolvePagingParams,
} from './';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

const ENTITY_STATES = [
    ...relationTypeRoute,
    ...relationTypePopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        RelationTypeComponent,
    ],
    declarations: [
        RelationTypeComponent,
        RelationTypeDialogComponent,
        RelationTypePopupComponent,
    ],
    entryComponents: [
        RelationTypeComponent,
        RelationTypeDialogComponent,
        RelationTypePopupComponent,
    ],
    providers: [
        RelationTypeService,
        RelationTypePopupService,
        RelationTypeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmRelationTypeModule {}
