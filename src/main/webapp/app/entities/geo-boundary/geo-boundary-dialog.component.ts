import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {GeoBoundary} from './geo-boundary.model';
import {GeoBoundaryPopupService} from './geo-boundary-popup.service';
import {GeoBoundaryService} from './geo-boundary.service';
import {ToasterService} from '../../shared';
import { GeoBoundaryType, GeoBoundaryTypeService } from '../geo-boundary-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-geo-boundary-dialog',
    templateUrl: './geo-boundary-dialog.component.html'
})
export class GeoBoundaryDialogComponent implements OnInit {

    geoBoundary: GeoBoundary;
    isSaving: boolean;

    geoboundarytypes: GeoBoundaryType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected geoBoundaryService: GeoBoundaryService,
        protected geoBoundaryTypeService: GeoBoundaryTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.geoBoundaryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.geoboundarytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.geoBoundary.idGeobou !== undefined) {
            this.subscribeToSaveResponse(
                this.geoBoundaryService.update(this.geoBoundary));
        } else {
            this.subscribeToSaveResponse(
                this.geoBoundaryService.create(this.geoBoundary));
        }
    }

    protected subscribeToSaveResponse(result: Observable<GeoBoundary>) {
        result.subscribe((res: GeoBoundary) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: GeoBoundary) {
        this.eventManager.broadcast({ name: 'geoBoundaryListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'geoBoundary saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'geoBoundary Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackGeoBoundaryTypeById(index: number, item: GeoBoundaryType) {
        return item.idGeobouType;
    }
}

@Component({
    selector: 'jhi-geo-boundary-popup',
    template: ''
})
export class GeoBoundaryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected geoBoundaryPopupService: GeoBoundaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.geoBoundaryPopupService
                    .open(GeoBoundaryDialogComponent as Component, params['id']);
            } else {
                this.geoBoundaryPopupService
                    .open(GeoBoundaryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
