import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { BukuTelponComponent } from './buku-telpon.component';
import { BukuTelponNewComponent } from './buku-telpon-new.component';
import { BukuTelponEditComponent } from './buku-telpon-edit.component';

@Injectable()
export class BukuTelponResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const BukuTelponRoute: Routes = [
    {
        path: 'buku-telpon',
        component: BukuTelponComponent,
        resolve: {
            'pagingParams': BukuTelponResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bukutelpon.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'buku-telpon-new',
        component: BukuTelponNewComponent,
        resolve: {
            'pagingParams': BukuTelponResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bukutelpon.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'buku-telpon-Edit/:route/:page/:id/edit',
        component: BukuTelponEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bukutelpon.home.createOrEditLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'master-bahan-Edit',
    //     component: MasterBahanEditComponent,
    //     resolve: {
    //         'pagingParams': MasterBahanResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.picrole.home.createLabel'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
];

export const BukuTelponPopupRoute: Routes = [

    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
