import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { BukuTelpon } from './buku-telpon.model';
import { BukuTelponService } from './buku-telpon.service';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-buku-telpon-edit',
    templateUrl: './buku-telpon-edit.component.html'
})
export class BukuTelponEditComponent implements OnInit {

    protected subscription: Subscription;
    picRole: BukuTelpon;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;

    routeId: number;
    isSaving: boolean;
    nama: string;
    inisial: string;
    telp1: string;
    telp2: string;
    fax: string;
    email: string;
    alamat: string;
    keterangan: string;
    Inputemail: string;
    Inputketerangan: string;
    lain: string;
    jenis: Array<object> = [
        {label : 'Pegawai', value : 'Pegawai'},
        {label : 'Distributor / Agen', value : 'Distributor / Agen'},
        {label : 'Lain - lain', value: 'Lain - lain'}
    ];
    readonly: boolean;
    pic: any;
    kdbahan: string;
    kdlama: any;
    satuan: string;
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: BukuTelponService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        protected route: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
    ) {
        this.picRole = new BukuTelpon();
        this.routeId = 0;
        this.readonly = true;
    }
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
    }

    load(id) {
        this.masterBahanService.find1(id).subscribe(
            (res: BukuTelpon) => {
                this.picRole = res;
                this.picRole.nama = res.nama;
                this.picRole.inisial = res.inisial;
                this.picRole.telp1 = res.telp1;
                this.picRole.telp2 = res.telp2;
                this.picRole.fax = res.fax;
                this.picRole.email = res.email;
                this.picRole.alamat = res.alamat;
                this.picRole.keterangan = res.keterangan;
                this.picRole.lain = res.lain;
            }
        )
    }

    trackId(index: number, item: BukuTelpon) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    ver() {
        if (this.picRole.nama === null || this.picRole.telp1 === ' ') {
            this.toaster.showToaster('info', 'Kolom Kosong', 'Kolom Tidak Boleh Kosong');
        } else {
            const obj = {
                idtelp: this.picRole.idtelp,
                nama: this.picRole.nama,
                inisial: this.picRole.inisial,
                telp1: this.picRole.telp1,
                telp2: this.picRole.telp2,
                fax: this.picRole.fax,
                email: this.picRole.email,
                alamat: this.picRole.alamat,
                keterangan: this.picRole.keterangan,
                lain: this.picRole.lain,
            }
            this.loadingService.loadingStart();
            this.masterBahanService.saveEdit(obj)
            .subscribe(
                (res) =>
                this.onSaveSuccess(res),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    simpan() {
        this.nama = this.picRole.nama,
        this.inisial = this.picRole.inisial,
        this.telp1 = this.picRole.telp1,
        this.telp2 = this.picRole.telp2,
        this.fax =  this.picRole.fax,
        this.email =  this.picRole.email,
        this.alamat = this.picRole.alamat,
        this.keterangan = this.picRole.keterangan,
        this.lain = this.picRole.lain;
        this.ver();
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data) {
        this.eventManager.broadcast({ name: 'MasterBahanListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Telpon saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Buku Telpon Error', error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }
    previousState() {
            this.router.navigate(['/buku-telpon', { page: this.paramPage }]);
    }
    back() {
            this.router.navigate(['/buku-telpon'])
        }
}
