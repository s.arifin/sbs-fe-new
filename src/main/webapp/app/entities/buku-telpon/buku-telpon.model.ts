import { BaseEntity } from '../../shared';

export class BukuTelpon implements BaseEntity {
    constructor(
        public idtelp?: any,
        public id?: number,
        public nama?: string,
        public inisial?: string,
        public telp1?: string,
        public telp2?: string,
        public fax?: string,
        public email?: string,
        public alamat?: string,
        public keterangan?: string,
        public lain?: string,
    ) {
    }
}
