import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MpmSharedModule } from '../../shared';
import { BukuTelpon, BukuTelponService, BukuTelponResolvePagingParams, BukuTelponNewComponent, BukuTelponEditComponent } from '.';
import { BukuTelponComponent } from './buku-telpon.component';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { BukuTelponRoute } from './buku-telpon.route';

const ENTITY_STATES = [
    ...BukuTelponRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule
    ],
    declarations: [
        BukuTelponComponent,
        BukuTelponNewComponent,
        BukuTelponEditComponent
    ],
    entryComponents: [
        BukuTelponComponent,
        BukuTelponNewComponent,
        BukuTelponEditComponent
    ],
    providers: [
        BukuTelponService,
        BukuTelponResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmBukuTelponModule {}
