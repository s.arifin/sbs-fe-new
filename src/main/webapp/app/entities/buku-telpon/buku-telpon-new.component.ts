import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { BukuTelponService } from './buku-telpon.service';
import { BukuTelpon } from './buku-telpon.model';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-buku-telpon-new',
    templateUrl: './buku-telpon-new.component.html'
})
export class BukuTelponNewComponent implements OnInit {

    picRole: BukuTelpon;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;
    internals: any;

    routeId: number;
    isSaving: boolean;
    nama: string;
    inisial: string;
    telp1: string;
    telp2: string;
    fax: string;
    email: string;
    alamat: string;
    keterangan: string;
    Inputemail: string;
    Inputketerangan: string;
    lain: string;
    jenis: Array<object> = [
        {label : 'Pegawai', value : 'Pegawai'},
        {label : 'Distributor / Agen', value : 'Distributor / Agen'},
        {label : 'Lain - lain', value: 'Lain - lain'}
    ];
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: BukuTelponService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.picRole = new BukuTelpon();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.internals = this.principal.getIdInternal();
        console.log(this.internals);
    }
    trackId(index: number, item: BukuTelpon) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    simpan() {
        if (this.nama === undefined || this.telp1 === undefined) {
            this.toaster.showToaster('info', 'Error', 'Kolom Tidak Boleh Kosong');
        } else {
            const obj = {
                nama: this.nama,
                inisial: this.inisial,
                telp1: this.telp1,
                telp2: this.telp2,
                fax: this.fax,
                email: this.email,
                alamat: this.alamat,
                keterangan: this.keterangan,
                lain: this.lain
            }
            // console.log(obj)
            this.loadingService.loadingStart();
            this.masterBahanService.create(obj)
            .subscribe(
                (res) =>
                this.onSaveSuccess(res),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data) {
        this.eventManager.broadcast({ name: 'MasterBahanListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'No Telpon saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', error.message, error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }
    previousState() {
            this.router.navigate(['/buku-telpon', { page: this.paramPage }]);
    }
    back() {
            this.router.navigate(['/buku-telpon'])
        }
}
