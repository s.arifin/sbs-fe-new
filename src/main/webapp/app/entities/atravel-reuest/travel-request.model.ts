import { BaseEntity } from '../../shared';

export class TravelRequest implements BaseEntity {
    constructor(
        public id?: number,
        public idTravel?: any,
        public noDoc?: any,
        public name?: string,
        public div?: string,
        public loc?: string,
        public dtStart?: Date,
        public dtThru?: Date,
        public nik?: any,
        public jabatan?: any,
        public catatan?: any,
        public statustype?: any,
        public status?: any,
        public statDesc?: any,
        public type?: any,
        public ket?: any,
        public locationTo?: any,
        public modaTrans?: any,
        public jenisTrans?: any,
        public createdby?: any,
        public dtcreated?: any,
        public app_mgr?: any,
        public app_hse?: any,
        public app_mgr_psu?: any,
        public dtsubmit?: any,
        public dtIsoman?: any,
        public dtMSK?: any,
        public unKer?: any,
        public totaldata?: any

    ) {
    }
}
