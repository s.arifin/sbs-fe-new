import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { ApprovalTravelComponent } from './approval/approval-travel.component';
import { TravelRequestComponent } from './travel-request.component';
import { TravelRequestNewComponent } from './travel-request-new.component';
import { TravelRequestEditComponent } from './travel-request-edit.component';
import { TravelRequestReciveComponent } from './travel-request-recive.component';
@Injectable()
export class TravelRequestResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const TravelRequestRoute: Routes = [
    // {
    //     path: 'travel-request',
    //     component: TravelRequestComponent,
    //     resolve: {
    //         'pagingParams': TravelRequestResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.annual.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: 'travel-request-req',
        component: TravelRequestComponent,
        resolve: {
            'pagingParams': TravelRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.Travel.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'travel-request-rec',
        component: TravelRequestReciveComponent,
        resolve: {
            'pagingParams': TravelRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.Travel.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'travel-request-req/travel-request-new',
        component: TravelRequestNewComponent,
        resolve: {
            'pagingParams': TravelRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.Travel.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'annual-leave-idu-new',
    //     component: AnnualLeaveIduNewComponent,
    //     resolve: {
    //         'pagingParams': AnnualLeaveResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.annual.home.createLabel'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    // {
    //     path: 'annualleavereg/:route/:page/:id/edit',
    //     component: AnnualLeaveRegEditComponent,
    //     resolve: {
    //         'pagingParams': AnnualLeaveResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.annual.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: 'travel-request/approval',
        component: ApprovalTravelComponent,
        resolve: {
            'pagingParams': TravelRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.Travel.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'travel-request-edit',
        component: TravelRequestEditComponent,
        resolve: {
            'pagingParams': TravelRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.Travel.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const  TravelRequestPopupRoute: Routes = [
    // {
    //     path: 'travel-request/:route/:page/:id/edit',
    //     component: TravelRequestEditComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.Travel.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    {
        path: 'travel-request/:route/:page/:id/edit',
        component: TravelRequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.Travel.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
