import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import {JobordersService, Joborders, CustomJobOrders } from '../../joborders'
import { Subscription } from 'rxjs';
import {AnnualLeave } from '../../annual-leave'
import {TravelRequestService } from '../travel-request.service'
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { TravelRequest } from '../travel-request.model';

@Component({
    selector: 'jhi-tab-view-travel',
    templateUrl: './tab-view-travel.component.html'
})
export class TabViewTravelComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    currentAccount: any;
    // @Input() idOrder: any;
    @Input() idStatusType: number;
    @Input() idJobOrder: any;
    public jobOrders: Array<TravelRequest>;
    public jobOrder: TravelRequest;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selectedJob: Joborders;
    selected: any = [];
    appJobOrders: Joborders[];
    newModalNoteAtDetail: boolean;
    noteNotAppPP: string;

    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrderService: TravelRequestService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.jobOrders = new Array<TravelRequest>();
        this.jobOrder = new TravelRequest();
        this.newModalNoteAtDetail = false;

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    previousState() {
        window.history.back();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }

    loadData(): void {
        this.loadingService.loadingStart();
        this.jobOrderService.getDataByStatus({
            page: this.page - 1,
            idStatustype: this.idStatusType,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrders = res.json;
                this.jobOrder = res.json;
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/annualleave/approval', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/annualleave/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }
    srch() {
        if (this.currentSearch) {
            console.log('current : ', this.currentSearch)
            this.jobOrderService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                stat: 13,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            return;
        }
    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    // clear() {
    //     this.page = 0;
    //     this.currentSearch = '';
    //     this.router.navigate(['./', {
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadData();
    // }

    public trackId(index: number, item: CustomUnitDocumentMessage): string {
        return item.idRequirement;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
       // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
        // this.customUnitDocumentMessages = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idTravel') {
            result.push('idTravel');
        }
        return result;
    }

    gotoDetail(rowData)  {
        console.log('datadriverdetail', );
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailableAtDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk TIdak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                    this.notAvailable();
                this.loadingService.loadingStart();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            statustype: 12,
            catatan: this.noteNotAppPP,
            data: this.selected
        }
        this.jobOrderService.changeStatus(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalCutiListModification', content: 'OK'});
            this.toaster.showToaster('info', 'Save', 'Data Not Approved !');
            this.totalItems = data[0].Totaldata;
            this.newModalNoteAtDetail = false;
            this.noteNotAppPP = null;
            this.selected = [];
            this.queryCount = this.totalItems;
            this.appJobOrders = data;
            this.router.navigate(['/travel-request/approval'])
            this.loadingService.loadingStop();
            this.loadData();
        }
        // console.log('ini data internal', data);
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                    this.Approved();
                this.loadingService.loadingStart();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
        console.log('isi selected', this.selected);
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            statustype: 11,
            data: this.selected
        }
        this.jobOrderService.changeStatus(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalCutiListModification', content: 'OK'});
            this.toaster.showToaster('info', 'Save', 'Data Approved !');
            this.selected = [];
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.newModalNoteAtDetail = false;
            this.appJobOrders = data;
            this.router.navigate(['/travel-request/approval'])
            this.loadingService.loadingStop();
            this.loadData();
        }
        // console.log('ini data internal', data);
    }
    // unregPP() {
    //     this.newMoalNote = true;
    // }
    unregPPAtDetail() {
        this.newModalNoteAtDetail = true;
    }
    backappNotApp() {
        this.noteNotAppPP = null;
        this.newModalNoteAtDetail = false;
    }
    print(id: any) {
        const user = this.principal.getUserLogin();

        const filter_data = 'idDoc:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/TravelRequest/pdf', {filterData: filter_data});
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }
}
