import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../../unit-document-message';
import {AnnualLeave } from '../../annual-leave'
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import * as BaseConstant from '../../../shared/constants/base.constants';
import * as UnitDocumentMessageConstant from '../../../shared/constants/unit-document-message.constants';

import { ResponseWrapper, CommonUtilService, Principal, ITEMS_PER_PAGE, ToasterService} from '../../../shared';
// import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { TravelRequestService } from '../travel-request.service';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { TravelRequest } from '../travel-request.model';

@Component({
    selector: 'jhi-approval-travel',
    templateUrl: './approval-travel.component.html'
})
export class ApprovalTravelComponent implements OnInit, OnDestroy {
    first: number;
    public jobOrders: Array<TravelRequest>;
    public jobOrder: TravelRequest;
    public jobOrdersApp: Array<TravelRequest>;
    public jobOrderApp: TravelRequest;
    private eventSubscriber: Subscription;
    currentAccount: any;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    jobOrderApprove: any;
    jobOrderRejeced: any;
    itemsPerPage: any;
    routeData: any;
    totalItems: any;
    queryCount: any;

    selected: any = [];
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    idStatus: any;

    constructor(
        protected principal: Principal,
        protected router: Router,
        protected reportUtilService: ReportUtilService,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrdersService: TravelRequestService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        protected alertService: JhiAlertService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.jobOrder = new TravelRequest();
        this.jobOrdersApp = new Array<TravelRequest>();
        this.jobOrderApp = new TravelRequest();
        this.jobOrders = new Array<TravelRequest>();
        this.jobOrder = new TravelRequest();
        this.jobOrderApprove = 12;
        this.jobOrderRejeced = 11;

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
        this.registerChangeInApprovalJobOrder();
    }

    protected loadData(): void {
        this.loadingService.loadingStart();
        this.jobOrdersService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 12,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrders = res.json;
                this.jobOrder = res.json;
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );

        this.jobOrdersService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 11,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrdersApp = res.json;
                this.jobOrderApp = res.json;
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }
    srch() {
        if (this.currentSearch) {
            console.log('current : ', this.currentSearch)
            this.jobOrdersService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                stat: this.idStatus,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            return;
        }
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idTravel') {
            result.push('idTravel');
        }
        return result;
    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
         this.totalItems = headers.get('X-Total-Count');
         this.queryCount = this.totalItems;
         this.loadingService.loadingStop();
         // this.customUnitDocumentMessages = data;
     }

     private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/travel-request/approval', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 11
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/travel-request/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    search13(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 12;
        this.currentSearch = query;
        this.router.navigate(['/travel-request/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    protected reset(): void {
        this.jobOrders = new Array<TravelRequest>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    gotoDetail(rowData)  {
        // console.log('datadriverdetail', );
        // this.jobOrdersService.passingCustomData(rowData);
    }

    public submit(id: number, type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header : 'Confirmation',
                message : 'Do you want to approve this Job Order?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: TravelRequest) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderApprove,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                item: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        } else if (type === 'reject') {
            this.confirmationService.confirm({
                header : 'Confirmation',
                message : 'Do you want to reject this Job Order?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: TravelRequest) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderRejeced,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                items: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody , objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalJobOrder() {
        this.eventSubscriber = this.eventManager.subscribe('approvalCutiListModification', (response) => this.loadData());
        console.log('masuk pak eko +++');
    }
    print(id: any) {
        const user = this.principal.getUserLogin();

        const filter_data = 'idDoc:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/TravelRequest/pdf', {filterData: filter_data});
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }
}
