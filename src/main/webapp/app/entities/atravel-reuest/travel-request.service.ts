import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, Headers } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { TravelRequest } from './travel-request.model';

@Injectable()
export class TravelRequestService {
    protected itemValues: TravelRequest[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/travel-request';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/travel-request';
    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }
    saveEdit(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/saveEdit`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    create(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    changeReqAPPROVE(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeReqAPPROVE'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    getNewBarang() {
        return this.http.get(this.resourceUrl + '/getalldropdown').toPromise()
            .then((res) => <any[]> res.json())
            .then((data) => data);
    }

    createIDU(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/createIDU`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    public getDataByApprovalStatusAndRequirement(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/byStatus', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
            // return res.json();
        // });
    }
    changeStatus(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatus'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    public getDataByStatus(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-status', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
            // return res.json();
        // });
    }
    createHrd(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/psu-entry`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    update(picRole: TravelRequest): Observable<TravelRequest> {
        const copy = this.convert(picRole);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<TravelRequest> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // find1(id: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(id);
    //     return this.http.get(`${this.resourceUrl}/${id}/find`, options).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         return this.convertResponse(jsonResponse);
    //     });
    // }
    find1(id: any): Observable<TravelRequest> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // query(req?: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(req);
    //     return this.http.get(this.resourceUrl, options)
    //         .map((res: Response) => this.convertResponse(res));
    // }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryReg(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '-receipt', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }

    private convert(picRole: TravelRequest): TravelRequest {
        const copy: TravelRequest = Object.assign({}, picRole);
        return copy;
    }

    getPtype() {
        return this.http.get(this.resourceUrl + '/tipe-produk').toPromise()
            .then((res) => <any[]> res.json())
            .then((data) => data);
    }

}
