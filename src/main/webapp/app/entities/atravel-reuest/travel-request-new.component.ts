import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { TravelRequestService } from './travel-request.service';
import { TravelRequest } from './travel-request.model';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PersonalInfo } from '../master_bahan/master-bahan.model';
import { OvertimeService } from '../overtime';
import {RadioButtonModule} from 'primeng/primeng';

@Component({
    selector: 'jhi-travel-request-new',
    templateUrl: './travel-request-new.component.html'
})
export class TravelRequestNewComponent implements OnInit {

    picRole: TravelRequest;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;

    routeId: number;
    isSaving: boolean;
    inName: any;
    inNIK: any;
    inJabatan: any;
    injblReqCuti: any;
    dtStart: any;
    dtFinish: Date;
    inApproved: any;
    inDept: any;
    inLoc: any;
    inType: any;
    inKet: any;
    inLocTo: any;
    inModTran: any;
    inJnsKen: any;
    dtOrder: any;
    inUnKer: any;
    dtIsoman: Date;
    dtIsoman1: Date;
    dtMSK: Date;
    dtiso: Date = new Date();
    tgl: any;
    tgl1: any;

    non_rutin: boolean;
    newPersonal: any;
    newPersonals: any[];
    filteredPersonal: any[];
    public personal: PersonalInfo;
    public Personal: PersonalInfo[];
    public selectedPersonal: any;
    public listPersonal = [{label: 'Please Select or Insert', value: null}];
    tipe: Array<object> = [
        {label : 'SBS 1', value : 'SBS 1'},
        {label : 'SBS 2', value : 'SBS 2'},
        {label : 'SBS 3', value : 'SBS 3'},
        {label : 'SBS 4', value : 'SBS 4'},
        {label : 'SBS 5', value : 'SBS 5'}
    ];

    needs: Array<object> = [
        {label : 'Pribadi', value : 'Pribadi'},
        {label : 'Umum', value : 'Umum'}

    ];
    divitions: Array<object> = [
        {label : 'Raw Material', value : 'RM'},
        {label : 'Packaging Material', value : 'PM'},
        {label : 'General Item', value : 'GI'},
        {label : 'Sparepart', value : 'S'},
        {label : 'Maintenace', value : 'M'},
        {label : 'Project', value : 'P'},
        {label : 'Jasa', value : 'J'},
        {label : 'Fixed Asets', value : 'F'},
        {label : 'Other', value : 'Other'},
        {label : 'Alat Non Rutin LPM', value : 'AL'},
        {label : 'Glassware Mikro LPM', value : 'GM'},
        {label : 'Glassware FK LPM', value : 'GF'},
        {label : 'Reagent LPM', value : 'RE'},
        {label : 'media LPM', value : 'ME'},
        {label : 'Tool LPM', value : 'TL'},
    ];
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: TravelRequestService,
        private personalService: OvertimeService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.dtIsoman = new Date();
        this.dtIsoman1 = new Date();
        this.tgl = this.dtiso.getDay() + 3;
        // this.dtMSK  = this.dtiso.getDay() + 3;
        this.dtMSK = new Date();
        this.non_rutin = false;
        this.dtOrder = new Date();
        this.personal = new PersonalInfo();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtFinish = new Date();

        // this.tgl1 = this.dtFinish
        this.dtStart = new Date();
    }
    filterBarangSingle(event) {
        // console.log('data Tipe barang : ', this.pilihTipe);
        const query = event.query;
        this.masterBahanService.getNewBarang().then((newPersonals) => {
            this.filteredPersonal = this.filterBarang(query, newPersonals);
        });
    }

    filterBarang(query, newPersonals: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newPersonals.length; i++) {
            const newPersonal = newPersonals[i];
            if (newPersonal.nama_lengkap.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal);
            } else if (newPersonal.nik.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.personal.idpersonalinfo = this.newPersonal.idpersonalinfo;
        console.log('idmasbahan : ' + this.newPersonal.idpersonalinfo);
        this.personalService.findBarang(this.newPersonal.idpersonalinfo)
        .subscribe(
            (res: PersonalInfo) => {
                console.log('ini isi personal = ', res.nama_lengkap);
                this.inNIK = res.nik;
                this.inName = res.nama;
                this.inJabatan = res.jabatan;
                this.inDept = res.dept;
            },
            (res: PersonalInfo) => {
                this.inName = null;
                console.log('ini isi bahan eror = ', this.inName);
            }
        )
    }

    selectTglBthUmum() {
        if (this.inModTran === 'Pribadi') {
            this.non_rutin = true;
            console.log('isi non rutin', this.non_rutin)
        } else {
            this.non_rutin = false;
            console.log('isi non rutin', this.non_rutin)
        }
        // console.log('isi non rutin', this.non_rutin)
    }

    selectTglBth() {
        this.dtIsoman = new Date();
        this.dtMSK = new Date();
        if (this.dtFinish.setSeconds(59) >= this.dtStart) {
            if (this.non_rutin === true) {
                this.dtIsoman = this.dtFinish;
                this.dtMSK = this.dtFinish;
            } else {
                this.dtIsoman.setUTCDate(this.dtFinish.getDate() + this.tgl);
                // this.dtIsoman.setUTCMonth(this.dtIsoman.getMonth());
                // this.dtIsoman.setUTCFullYear(this.dtIsoman.getFullYear());
                this.dtMSK.setUTCDate(this.dtFinish.getDate() + 3);
                this.dtMSK.setUTCMonth(this.dtFinish.getMonth());
                this.dtMSK.setUTCFullYear(this.dtFinish.getFullYear());
                console.log('masuk ke select tgl: ' + this.dtIsoman);
                console.log('masuk ke select tglfinish: ' + this.dtFinish);
            }
        } else {
            alert('Tgl Selesai tidak boleh kurang dari Tgl Permintaan');
            this.dtFinish = new Date();
            this.dtStart = new Date();
            // this.ngOnInit();
        }
    }

    ngOnInit() {
        this.dtIsoman = this.dtIsoman1;
        this.picRole = new TravelRequest();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }
    trackId(index: number, item: TravelRequest) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    public selectpic(isSelect?: boolean): void {
        this.picRole.name = this.newpic.login;
    }

    filterpTypeSingle(event) {
        const query = event.query;
        this.masterBahanService.getPtype().then((newPtype) => {
            this.filteredpType = this.filterpType(query, newPtype);
        });
    }

    filterpType(query, newpType: any[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpType.length; i++) {
            const newpTypes = newpType[i];
            if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpTypes);
            }
        }
        return filtered;
    }
    // public selectpType(isSelect?: boolean): void {
    //     this.picRole.type = this.newpType.pType;
    // }
    simpan() {
        const obj = {
            name: this.inName,
            nik: this.inNIK,
            jabatan: this.inJabatan,
            jmbReqCuti: this.inJabatan,
            dtStart: this.dtStart,
            dtThru: this.dtFinish,
            div: this.inDept,
            loc: this.inLoc,
            type: this.inType,
            ket: this.inKet,
            locationTo: this.inLocTo,
            modaTrans: this.inModTran,
            jenisTrans: this.inJnsKen,
            dtIsoman: this.dtIsoman,
            dtMSK: this.dtMSK,
            unKer: this.inUnKer,
        }
        this.loadingService.loadingStart();
        this.masterBahanService.create(obj)
        .subscribe(
            (res: ResponseWrapper) =>
            this.onSaveSuccess(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Request saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Request Gagal', error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }
    previousState() {
            this.router.navigate(['/travel-request-req', { page: this.paramPage }]);
    }
    back() {
            this.router.navigate(['/travel-request-req'])
        }
}
