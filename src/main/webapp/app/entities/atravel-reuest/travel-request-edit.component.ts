import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { TravelRequestService } from './travel-request.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TravelRequest } from './travel-request.model';
import { PersonalInfo } from '../master_bahan';
import { OvertimeService } from '../overtime';

@Component({
    selector: 'jhi-travel-request-edit',
    templateUrl: './travel-request-edit.component.html'
})
export class TravelRequestEditComponent implements OnInit {

    protected subscription: Subscription;
    picRole: TravelRequest;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;

    routeId: number;
    isSaving: boolean;
    inKdBahan: any;
    inKdUmBahan: any;
    inNmBahan: any;
    inSatBahan: any;
    inTipeBahan: any;
    inPicBahan: any;

    divitions: Array<object> = [
        {label : 'Raw Material', value : 'RM'},
        {label : 'Packaging Material', value : 'PM'},
        {label : 'General Item', value : 'GI'},
        {label : 'Sparepart', value : 'S'},
        {label : 'Maintenace', value : 'M'},
        {label : 'Project', value : 'P'},
        {label : 'Jasa', value : 'J'},
        {label : 'Fixed Asets', value : 'F'},
        {label : 'Other', value : 'Other'},
        {label : 'Alat Non Rutin LPM', value : 'AL'},
        {label : 'Glassware Mikro LPM', value : 'GM'},
        {label : 'Glassware FK LPM', value : 'GF'},
        {label : 'Reagent LPM', value : 'RE'},
        {label : 'media LPM', value : 'ME'},
        {label : 'Tool LPM', value : 'TL'},
    ];
    tipe: Array<object> = [
        {label : 'SBS 1', value : 'SBS 1'},
        {label : 'SBS 2', value : 'SBS 2'},
        {label : 'SBS 3', value : 'SBS 3'},
        {label : 'SBS 4', value : 'SBS 4'},
        {label : 'SBS 5', value : 'SBS 5'}
    ];
    tipe1: Array<object> = [
        {label : 'Barang', value : 'Barang'},
        {label : 'Jasa', value : 'JASA'},
        {label : 'Packageing Material (PM)', value : 'PM'},
        {label : 'Raw Material (RM)', value : 'RM'},
        {label : 'General Item (GI)', value : 'GI'},
        {label : 'Sparepart', value : 'S'},
        {label : 'Maintenace', value : 'M'},
        {label : 'Project', value : 'P'},
        {label : 'Jasa', value : 'J'},
        {label : 'Fixed Asets', value : 'F'},
        {label : 'Other', value : 'Other'},
        {label : 'Alat Non Rutin LPM', value : 'AL'},
        {label : 'Glassware Mikro LPM', value : 'GM'},
        {label : 'Glassware FK LPM', value : 'GF'},
        {label : 'Reagent LPM', value : 'RE'},
        {label : 'media LPM', value : 'ME'},
        {label : 'Tool LPM', value : 'TL'},
    ];
    idMasBahan: any;
    readonly: boolean;
    pic: any;
    kdbahan: string;
    kdlama: any;
    satuan: string;
    dtFinish: Date;
    dtIsoman: Date;
    dtMSK: Date;
    dtStart: any;

    newPersonal: any;
    newPersonals: any[];
    filteredPersonal: any[];

    public personal: PersonalInfo;
    public Personal: PersonalInfo[];
    public selectedPersonal: any;
    dtCreated: Date;

    constructor(
        public activeModal: NgbActiveModal,
        private personalService: OvertimeService,
        private masterBahanService: TravelRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        protected route: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.picRole = new TravelRequest();
        this.routeId = 0;
        this.readonly = true;
    }
    ngOnInit() {
        this.picRole = new MasterBahan();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idMasBahan = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
    }

    selectTglBth() {
        this.dtIsoman = new Date();
        this.dtMSK = new Date();
        if (this.dtFinish.setSeconds(59) >= this.dtStart) {
            this.dtIsoman.setUTCDate(this.dtFinish.getDate() + 3);
            this.dtIsoman.setMonth(this.dtIsoman.getMonth());
            this.dtIsoman.setFullYear(this.dtIsoman.getFullYear());

            this.dtMSK.setDate(this.dtFinish.getDate() + 3);
            this.dtMSK.setMonth(this.dtFinish.getMonth());
            // this.dtIsoman = this.dtFinish.getDay() + 3;
            console.log('masuk ke select tgl: ' + this.dtIsoman);
            // this.ngOnInit();
        } else {
            alert('Tgl Selesai tidak boleh kurang dari Tgl Permintaan');
            this.dtFinish = new Date();
            this.dtStart = new Date();
            // this.ngOnInit();
        }
    }

    load() {
        this.masterBahanService.find1(this.idMasBahan).subscribe((masterBahan) => {
            console.log('masuk load : ', masterBahan)
            this.picRole = masterBahan;
            this.dtStart = new Date(this.picRole.dtStart);
            this.dtFinish = new Date(this.picRole.dtThru);
            this.dtIsoman = new Date(this.picRole.dtIsoman);
            this.dtCreated = new Date(this.picRole.dtcreated);
            this.dtMSK = new Date(this.picRole.dtMSK);
            // // this.inputTipe = this.purchaseOrder.tipe;
            // // this.inputKet = this.purchaseOrder.ket;
            // // this.currentDiusul = this.purchaseOrder.diusulkan;
            // // this.currentMGR = this.purchaseOrder.appDept;
            // // this.currentKBG = this.purchaseOrder.appPabrik;
            // console.log('diusul = ', this.picRole.idMasBahan);
            // console.log('MGR = ', this.picRole.kdBahan);
            // // console.log('KBG = ', this.currentKBG);
            // // console.log('Creatby = ', this.purchaseOrder.createdBy);
        });
    }

    filterBarangSingle(event) {
        // console.log('data Tipe barang : ', this.pilihTipe);
        const query = event.query;
        this.masterBahanService.getNewBarang().then((newPersonals) => {
            this.filteredPersonal = this.filterBarang(query, newPersonals);
        });
    }

    filterBarang(query, newPersonals: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newPersonals.length; i++) {
            const newPersonal = newPersonals[i];
            if (newPersonal.nama_lengkap.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal);
            } else if (newPersonal.nik.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.personal.idpersonalinfo = this.newPersonal.idpersonalinfo;
        console.log('idmasbahan : ' + this.newPersonal.idpersonalinfo);
        this.personalService.findBarang(this.newPersonal.idpersonalinfo)
        .subscribe(
            (res: PersonalInfo) => {
                console.log('ini isi personal = ', res.nama_lengkap);
                this.picRole.nik = res.nik;
                this.picRole.name = res.nama;
                this.picRole.jabatan = res.jabatan;
                this.picRole.div = res.dept;
            },
            (res: PersonalInfo) => {
                this.picRole.name = null;
                console.log('ini isi bahan eror = ', this.picRole.name);
            }
        )
    }

    trackId(index: number, item: MasterBahan) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    // public selectpic(isSelect?: boolean): void {
    //     this.picRole.name = this.newpic.login;
    // }

    filterpTypeSingle(event) {
        const query = event.query;
        this.masterBahanService.getPtype().then((newPtype) => {
            this.filteredpType = this.filterpType(query, newPtype);
        });
    }

    filterpType(query, newpType: any[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpType.length; i++) {
            const newpTypes = newpType[i];
            if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpTypes);
            }
        }
        return filtered;
    }
    // public selectpType(isSelect?: boolean): void {
    //     this.picRole.type = this.newpType.pType;
    // }
    ver() {
            // const obj = {
            //     idMasBahan: this.picRole.idMasBahan,
            //     tpBahan: this.picRole.tpBahan,
            //     kdUmum: this.picRole.kdUmum,
            //     kdBahanLama: this.picRole.kdBahanLama,
            //     kdbahan: this.picRole.kdBahan,
            //     nmbahan: this.picRole.nmBahan,
            //     satuan: this.picRole.satuan,
            //     pic : this.picRole.pic,
            //     div: this.principal.getIdInternal(),
            //     type: this.picRole.tpBahan,
            // }
            this.loadingService.loadingStart();
            this.masterBahanService.saveEdit(this.picRole)
            .subscribe(
                (res: ResponseWrapper) =>
                this.onSaveSuccess(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }
    simpan() {
        console.log (this.picRole)
        this.picRole.dtStart = this.dtStart;
        this.picRole.dtThru = this.dtFinish;
        this.picRole.dtIsoman = this.dtIsoman;
        this.picRole.dtMSK = this.dtMSK;
        this.ver();
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'data saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Bahan Error', error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }
    previousState() {
        this.router.navigate(['/travel-request-req', { page: this.paramPage }]);
    }
    back() {
            this.router.navigate(['/travel-request-req'])
    }
}
