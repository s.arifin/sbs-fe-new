import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaginatorModule,
    FieldsetModule,
    ScheduleModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import { JhiLanguageService } from 'ng-jhipster';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmSharedModule, JhiLanguageHelper } from '../../shared';
import { TravelRequestPopupRoute, TravelRequestResolvePagingParams } from './travel-request.route';
import { TravelRequestComponent } from './travel-request.component';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule, TabViewModule, } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TravelRequestRoute } from './travel-request.route';
import { TravelRequestService } from './travel-request.service';
import { TravelRequestNewComponent } from './travel-request-new.component';
import { TravelRequestReciveComponent } from './travel-request-recive.component';
import { TravelRequestEditComponent } from './travel-request-edit.component';
// import { AnnualLeaveRegComponent } from './annual-leave-reg.component';
import { ApprovalTravelDetailComponent } from './approval/approval-travel-detail.component';
import { ApprovalTravelComponent } from './approval/approval-travel.component';
import { TabViewTravelComponent } from './approval/tab-view-travel.component';
// import { AnnualLeaveIduNewComponent } from './annual-leave-idu-new.component';

const ENTITY_STATES = [
    ...TravelRequestRoute,
    ...TravelRequestPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        TabsModule.forRoot(),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        PaginatorModule,
        FieldsetModule,
        ScheduleModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        TabViewModule,
        InputTextareaModule,
        FileUploadModule,
        DataListModule,
        TooltipModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        SliderModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        TabViewModule,
        AccordionModule,
        DialogModule,
    ],
    declarations: [
        TravelRequestComponent,
        TravelRequestReciveComponent,
        TravelRequestNewComponent,
        TravelRequestEditComponent,
        // AnnualLeaveNewComponent,
        // AnnualLeaveIduNewComponent,
        // AnnualLeaveRegComponent,
        // AnnualLeaveRegEditComponent,
        ApprovalTravelComponent,
        ApprovalTravelDetailComponent,
        TabViewTravelComponent
    ],
    entryComponents: [
        TravelRequestComponent,
        TravelRequestNewComponent,
        TravelRequestReciveComponent,
        TravelRequestEditComponent,
        // AnnualLeaveNewComponent,
        // AnnualLeaveIduNewComponent,
        // AnnualLeaveRegComponent,
        // AnnualLeaveRegEditComponent,
        ApprovalTravelComponent,
        ApprovalTravelDetailComponent,
        TabViewTravelComponent
    ],
    providers: [
        TravelRequestService,
        TravelRequestResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmTravelRequestModule {}
