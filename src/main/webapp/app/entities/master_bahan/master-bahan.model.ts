import { BaseEntity } from '../../shared';

export class MasterBahan implements BaseEntity {
    constructor(
        public idMasBahan?: any,
        public id?: number,
        public tpBahan?: any,
        public role?: string,
        public div?: string,
        public type?: string,
        public name?: string,
        public dtFrom?: Date,
        public dtThru?: Date,
        public ket?: any,
        public pic?: any,
        public is_halal?: any,
        public kategori?: any,
        public tipe_wadah?: any,
        public satuan_2?: string,
        public nilai_2?: number,
        public satuan_1?: string,
        public nilai_1?: number,
    ) {
    }
}
export class PersonalInfo implements BaseEntity {
    constructor(
        public id?: number,
        public idpersonalinfo?: any,
        public jabatan?: string,
        public dept?: string,
        public nama_lengkap?: string,
        public nama?: string,
        public nik?: string,
    ) {
    }
}
