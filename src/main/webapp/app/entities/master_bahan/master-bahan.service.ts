import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, Headers } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
// import { MasterBahan } from './.model';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';

@Injectable()
export class MasterBahanService {
    protected itemValues: MasterBahan[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/master-bahan';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/master-bahan';
    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    // create(picRole: MasterBahan): Observable<MasterBahan> {
    //     const copy = this.convert(picRole);
    //     return this.http.post(this.resourceUrl, copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }
    create(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    getAllSatuan(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getSatuan', options)
            .map((res: Response) => this.convertResponse(res));
    }

    saveEdit(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/saveEdit`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    update(picRole: MasterBahan): Observable<MasterBahan> {
        const copy = this.convert(picRole);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    find1(id: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/filter`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    getKategoriBarang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getAllKategori', options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }

    private convert(picRole: MasterBahan): MasterBahan {
        const copy: MasterBahan = Object.assign({}, picRole);
        return copy;
    }

    getPtype() {
        return this.http.get(this.resourceUrl + '/tipe-produk').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

}
