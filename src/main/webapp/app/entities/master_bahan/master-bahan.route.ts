import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { MasterBahanComponent } from './master-bahan.component';
import { MasterBahanNewComponent } from './master-bahan-new.component';
import { MasterBahanEditComponent } from './master-bahan-edit.component';

@Injectable()
export class MasterBahanResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const MasterBahanRoute: Routes = [
    {
        path: 'master-bahan',
        component: MasterBahanComponent,
        resolve: {
            'pagingParams': MasterBahanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-bahan-new',
        component: MasterBahanNewComponent,
        resolve: {
            'pagingParams': MasterBahanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-bahan-Edit/:route/:page/:id/edit',
        component: MasterBahanEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'master-bahan-Edit',
    //     component: MasterBahanEditComponent,
    //     resolve: {
    //         'pagingParams': MasterBahanResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.picrole.home.createLabel'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
];

export const MasterBahanPopupRoute: Routes = [

    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
