import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterBahanService } from './master-bahan.service';
import { MasterBahan } from './master-bahan.model';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-master-bahan-new',
    templateUrl: './master-bahan-new.component.html'
})
export class MasterBahanNewComponent implements OnInit {

    picRole: MasterBahan;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType: any;
    internals: any;
    newProj: any;
    newProjs: any[];
    routeId: number;
    isSaving: boolean;
    inKdBahan: string;
    inKdUmBahan: any;
    inNmBahan: any;
    inSatBahan: any;
    inSatBahan2: any;
    inSatBahan3: any;
    inQtybahan2: any;
    inQtybahan3: any;
    inTipeBahan: any;
    inPicBahan: any;
    selectedStatus: any;
    inputKategori: any;
    inputWadah: any;
    idInternal: any;

    listSatuan: Array<object> = [
        { label: 'Pilih Satuan', value: null },
    ];

    listWadah: Array<object> = [
        { label: 'Pilih Wadah', value: null },
        { label: 'Wadah', value: 'Wadah' },
        { label: 'Pallet', value: 'Pallet' },
    ];

    tipe: Array<object> = [
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY_SKG', value: 'SVY_SKG' },
        { label: 'SVY_SMK', value: 'SVY_SMK' },
        { label: 'JTR_SMK', value: 'JTR_SMK' },
    ];
    tipe1: Array<object> = [
        { label: 'Pilih Tipe', value: null },
        { label: 'Packaging Material (PM)', value: 'PM' },
        { label: 'Raw Material (RM)', value: 'RM' },
        { label: 'General Item (GI)', value: 'GI' },
    ];
    tipe2: Array<object> = [
        { label: 'Pilih Kategori', value: null },
        { label: 'Barang', value: 'BARANG' },
        { label: 'Jasa', value: 'JASA' },
    ];
    status1: Array<object> = [
        { label: 'Pilih Status', value: null },
        { label: 'Aktif', value: 'Aktif' },
        { label: 'Non Aktif', value: 'Non Aktif' }
    ];
    inkdUmum: string;
    inputQr: string;
    inputHalal: string;
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: MasterBahanService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.inSatBahan = null;
        this.inSatBahan2 = null;
        this.inSatBahan3 = null;
        this.inQtybahan2 = null;
        this.inQtybahan3 = null;
        this.inTipeBahan = null;
        this.inputKategori = null;
        this.inputWadah = null;
        this.inputHalal = null;
        this.inputQr = null;
        this.idInternal = this.principal.getIdInternal();
        if (this.idInternal.includes('SKG')) {
            this.tipe = this.tipe.filter((pic) => pic['value'].includes('SKG'));
        } else if (this.idInternal.includes('SMK')) {
            this.tipe = this.tipe.filter((pic) => pic['value'].includes('SMK'));
        } else {
            this.tipe = this.tipe.filter((pic) => !pic['value'].includes('SMK') && !pic['value'].includes('SKG'));
        }
    }
    ngOnInit() {
        this.picRole = new MasterBahan();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.internals = this.principal.getIdInternal();
    }
    trackId(index: number, item: MasterBahan) {
        return item.id;
    }

    onKeyUp(x) { // appending the updated value to the variable
        if (this.internals === 'PPIC') {
            this.inkdUmum = x.target.value.slice(0, 6);
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    public selectpic(isSelect?: boolean): void {
        this.picRole.name = this.newpic.login;
    }

    filterpTypeSingle(event) {
        const query = event.query;
        this.masterBahanService.getPtype().then((newPtype) => {
            this.filteredpType = this.filterpType(query, newPtype);
        });
    }

    filterpType(query, newpType: any[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpType.length; i++) {
            const newpTypes = newpType[i];
            if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpTypes);
            }
        }
        return filtered;
    }
    public selectpType(isSelect?: boolean): void {
        this.picRole.type = this.newpType.pType;
    }
    simpan() {
        if (this.inPicBahan === null || this.inPicBahan === ' ' || this.inPicBahan === undefined) {
            this.toaster.showToaster('info', 'PIC Kosong', 'Pilih PIC Barang Terlebih Dahulu !');
        } else if (this.inKdBahan === null || this.inKdBahan === ' ' || this.inKdBahan === undefined) {
            this.toaster.showToaster('info', 'Kode Bahan Kosong', 'Isi Kode Barang Terlebih Dahulu !');
        } else if (this.inNmBahan === null || this.inNmBahan === ' ' || this.inNmBahan === undefined) {
            this.toaster.showToaster('info', 'Nama Bahan Kosong', 'Isi Nama Bahan Terlebih Dahulu !');
        } else if (this.inSatBahan === null || this.inSatBahan === ' ' || this.inSatBahan === undefined) {
            this.toaster.showToaster('info', 'Satuan Bahan Kosong', 'Isi Satuan Bahan Terlebih Dahulu !');
        } else if (this.inNmBahan.length > 255) {
            this.toaster.showToaster('info', 'Nama Bahan Terlalu Panjang', 'Nama Bahan Tidak Boleh Lebih Dari 255 Karakter');
        } else if (this.selectedStatus === '' || this.selectedStatus === null || this.selectedStatus === undefined) {
            this.toaster.showToaster('info', 'Status Barang Belum Dipilih', 'Status Barang Harus Dipilih');
        } else if (this.internals === 'PPIC' && (this.inTipeBahan === null || this.inTipeBahan === '' || this.inTipeBahan === undefined)) {
            this.toaster.showToaster('info', 'Tipe Barang Belum Dipilih', 'Tipe Barang Harus Dipilih');
        } else if (this.inputKategori === null || this.inputKategori === '' || this.inputKategori === undefined) {
            this.toaster.showToaster('info', 'Kategori Barang Belum Dipilih', 'Kategori Barang Harus Dipilih');
        } else {
            if (this.internals === 'PPIC') {
                const obj = {
                    tpbahan: this.inTipeBahan,
                    kdBahanLama: this.inKdUmBahan,
                    kdUmum: this.inkdUmum,
                    kdbahan: this.inKdBahan.replace(/\s/g, ''),
                    nmbahan: this.inNmBahan,
                    satuan: this.inSatBahan.toUpperCase(),
                    pic: this.inPicBahan,
                    div: this.principal.getIdInternal(),
                    type: this.inTipeBahan,
                    status: this.selectedStatus,
                    tipe_wadah: this.inputWadah,
                    is_halal: this.inputHalal,
                    kategori: this.inputKategori,
                    satuan_2: this.inSatBahan2.toUpperCase(),
                    satuan_1: this.inSatBahan3.toUpperCase(),
                    nilai_2: this.inQtybahan2,
                    nilai_1: this.inQtybahan3,
                }
                this.loadingService.loadingStart();
                this.masterBahanService.create(obj)
                    .subscribe(
                        (res: ResponseWrapper) =>
                            this.onSaveSuccess(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res)
                    );
            } else {
                const obj = {
                    tpbahan: this.inTipeBahan,
                    kdBahanLama: this.inKdUmBahan,
                    kdUmum: this.inkdUmum,
                    kdbahan: this.inKdBahan.replace(/\s/g, ''),
                    nmbahan: this.inNmBahan,
                    satuan: this.inSatBahan.toUpperCase(),
                    pic: this.inPicBahan,
                    div: this.principal.getIdInternal(),
                    type: this.inTipeBahan,
                    status: this.selectedStatus,
                    tipe_wadah: this.inputWadah,
                    is_halal: this.inputHalal,
                    kategori: this.inputKategori
                }
                this.loadingService.loadingStart();
                this.masterBahanService.create(obj)
                    .subscribe(
                        (res: ResponseWrapper) =>
                            this.onSaveSuccess(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res)
                    );
            }

        }
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'MasterBahanListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Bahan saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Bahan Error', error._body);
        this.alertService.error(error._body, null, null);
    }
    previousState() {
        this.router.navigate(['/master-bahan', { page: this.paramPage }]);
    }
    back() {
        this.router.navigate(['/master-bahan'])
    }
}
