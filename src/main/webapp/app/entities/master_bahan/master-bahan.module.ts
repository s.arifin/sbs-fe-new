import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { MasterBahan, MasterBahanService, MasterBahanResolvePagingParams, MasterBahanNewComponent, MasterBahanEditComponent } from '.';
import { MasterBahanComponent } from './master-bahan.component';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule, DropdownModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { MasterBahanRoute } from './master-bahan.route';
import { AccordionModule } from 'primeng/primeng';

const ENTITY_STATES = [
    ...MasterBahanRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        AccordionModule,
        DropdownModule
    ],
    declarations: [
        MasterBahanComponent,
        MasterBahanNewComponent,
        MasterBahanEditComponent,
        // MasterBahanEditStockComponent
    ],
    entryComponents: [
        MasterBahanComponent,
        MasterBahanNewComponent,
        MasterBahanEditComponent,
        // MasterBahanEditStockComponent
    ],
    providers: [
        MasterBahanService,
        MasterBahanResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterBahanModule {}
