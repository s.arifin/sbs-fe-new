import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterBahanService } from './master-bahan.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-master-bahan-edit',
    templateUrl: './master-bahan-edit.component.html'
})
export class MasterBahanEditComponent implements OnInit {

    protected subscription: Subscription;
    picRole: MasterBahan;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType: any;

    routeId: number;
    isSaving: boolean;
    inKdBahan: any;
    inKdUmBahan: any;
    inNmBahan: any;
    inSatBahan: any;
    inTipeBahan: any;
    inPicBahan: any;

    listSatuan: Array<object> = [
        { label: 'Pilih Satuan', value: null },
    ];

    listWadah: Array<object> = [
        { label: 'Pilih Wadah', value: null },
        { label: 'Wadah', value: 'Wadah' },
        { label: 'Pallet', value: 'Pallet' },
    ];

    tipe: Array<object> = [
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY_SKG', value: 'SVY_SKG' },
        { label: 'SVY_SMK', value: 'SVY_SMK' },
        { label: 'JTR_SMK', value: 'JTR_SMK' },
    ];
    tipe1: Array<object> = [
        { label: 'Pilih Tipe', value: null },
        { label: 'Packaging Material (PM)', value: 'PM' },
        { label: 'Raw Material (RM)', value: 'RM' },
        { label: 'General Item (GI)', value: 'GI' },
    ];
    tipe2: Array<object> = [
        { label: 'Pilih Kategori', value: null },
        { label: 'Barang', value: 'BARANG' },
        { label: 'Jasa', value: 'JASA' },
    ];
    status1: Array<object> = [
        { label: 'Pilih Status', value: null },
        { label: 'Aktif', value: 'Aktif' },
        { label: 'Non Aktif', value: 'Non Aktif' }
    ];
    idMasBahan: any;
    readonly: boolean;
    pic: any;
    kdbahan: string;
    kdlama: any;
    satuan: string;
    selectedStatus: any;
    newProj: any;
    newProjs: any[];
    internals: any;
    idInternal: any;
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: MasterBahanService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        protected route: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.picRole = new MasterBahan();
        this.routeId = 0;
        this.readonly = true;
        this.idInternal = this.principal.getIdInternal();
        if (this.idInternal.includes('SKG')) {
            this.tipe = this.tipe.filter((pic) => pic['value'].includes('SKG'));
        } else if (this.idInternal.includes('SMK')) {
            this.tipe = this.tipe.filter((pic) => pic['value'].includes('SMK'));
        } else {
            this.tipe = this.tipe.filter((pic) => !pic['value'].includes('SMK') && !pic['value'].includes('SKG'));
        }
    }
    ngOnInit() {
        this.picRole = new MasterBahan();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idMasBahan = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.internals = this.principal.getIdInternal();
    }

    load() {
        this.masterBahanService.find1(this.idMasBahan).subscribe((masterBahan) => {
            this.picRole = masterBahan;
            // this.dtOrder = new Date(this.purchaseOrder.dtOrder);
            // this.dtNecessity = new Date(this.purchaseOrder.dtNecessity);
            // this.inputTipe = this.purchaseOrder.tipe;
            // this.inputKet = this.purchaseOrder.ket;
            // this.currentDiusul = this.purchaseOrder.diusulkan;
            // this.currentMGR = this.purchaseOrder.appDept;
            // this.currentKBG = this.purchaseOrder.appPabrik;
            console.log('diusul = ', this.picRole.idMasBahan);
            console.log('MGR = ', this.picRole.kdBahan);
            // console.log('KBG = ', this.currentKBG);
            // console.log('Creatby = ', this.purchaseOrder.createdBy);
        });
    }

    trackId(index: number, item: MasterBahan) {
        return item.id;
    }

    onKeyUp(x) { // appending the updated value to the variable
        if (this.internals === 'PPIC') {
            this.picRole.kdUmum = x.target.value.slice(0, 6);
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    // public selectpic(isSelect?: boolean): void {
    //     this.picRole.name = this.newpic.login;
    // }

    filterpTypeSingle(event) {
        const query = event.query;
        this.masterBahanService.getPtype().then((newPtype) => {
            this.filteredpType = this.filterpType(query, newPtype);
        });
    }

    filterpType(query, newpType: any[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpType.length; i++) {
            const newpTypes = newpType[i];
            if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpTypes);
            }
        }
        return filtered;
    }
    // public selectpType(isSelect?: boolean): void {
    //     this.picRole.type = this.newpType.pType;
    // }
    ver() {
        if (this.picRole.pic === null || this.picRole.pic === ' ' || this.picRole.pic === undefined) {
            this.toaster.showToaster('info', 'PIC Kosong', 'Pilih PIC Barang Terlebih Dahulu !');
        } else if (this.picRole.kdBahan === null || this.picRole.kdBahan === ' ' || this.picRole.kdBahan === undefined) {
            this.toaster.showToaster('info', 'Kode Bahan Kosong', 'Isi Kode Barang Terlebih Dahulu !');
        } else if (this.picRole.nmBahan === null || this.picRole.nmBahan === ' ' || this.picRole.nmBahan === undefined) {
            this.toaster.showToaster('info', 'Nama Bahan Kosong', 'Isi Nama Bahan Terlebih Dahulu !');
        } else if (this.picRole.satuan === null || this.picRole.satuan === ' ' || this.picRole.satuan === undefined) {
            this.toaster.showToaster('info', 'Satuan Bahan Kosong', 'Isi Satuan Bahan Terlebih Dahulu !');
        } else if (this.picRole.nmBahan.length > 255) {
            this.toaster.showToaster('info', 'Nama Bahan Terlalu Panjang', 'Nama Bahan Tidak Boleh Lebih Dari 255 Karakter');
        } else if (this.picRole.status === null || this.picRole.status === '' || this.picRole.status === undefined) {
            this.toaster.showToaster('info', 'Status Barang Belum Dipilih', 'Status Barang Harus Dipilih');
        } else if (this.internals === 'PPIC' && (this.picRole.tpBahan === null || this.picRole.tpBahan === '' || this.picRole.tpBahan === undefined)) {
            this.toaster.showToaster('info', 'Tipe Barang Belum Dipilih', 'Tipe Barang Harus Dipilih');
        } else if (this.picRole.kategori === null || this.picRole.kategori === '' || this.picRole.kategori === undefined) {
            this.toaster.showToaster('info', 'Kategori Barang Belum Dipilih', 'Kategori Barang Harus Dipilih');
        } else {
            if (this.internals === 'PPIC') {
                const obj = {
                    idMasBahan: this.picRole.idMasBahan,
                    tpBahan: this.picRole.tpBahan,
                    kdUmum: this.picRole.kdUmum,
                    kdBahanLama: this.picRole.kdBahanLama,
                    kdbahan: this.picRole.kdBahan,
                    nmbahan: this.picRole.nmBahan,
                    satuan: this.picRole.satuan.toUpperCase(),
                    pic: this.picRole.pic,
                    div: this.principal.getIdInternal(),
                    type: this.picRole.tpBahan,
                    status: this.picRole.status,
                    tipe_wadah: this.picRole.tipe_wadah,
                    is_halal: this.picRole.is_halal,
                    kategori: this.picRole.kategori,
                    satuan_2: this.picRole.satuan_2,
                    satuan_1: this.picRole.satuan_1,
                    nilai_2: this.picRole.nilai_2,
                    nilai_1: this.picRole.nilai_1,
                }
                this.loadingService.loadingStart();
                this.masterBahanService.saveEdit(obj)
                    .subscribe(
                        (res: ResponseWrapper) =>
                            this.onSaveSuccess(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res)
                    );
            } else {
                const obj = {
                    idMasBahan: this.picRole.idMasBahan,
                    tpBahan: this.picRole.tpBahan,
                    kdUmum: this.picRole.kdUmum,
                    kdBahanLama: this.picRole.kdBahanLama,
                    kdbahan: this.picRole.kdBahan,
                    nmbahan: this.picRole.nmBahan,
                    satuan: this.picRole.satuan.toUpperCase(),
                    pic: this.picRole.pic,
                    div: this.principal.getIdInternal(),
                    type: this.picRole.tpBahan,
                    status: this.picRole.status,
                    tipe_wadah: this.picRole.tipe_wadah,
                    is_halal: this.picRole.is_halal,
                    kategori: this.picRole.kategori,
                    // satuan_2 : this.picRole.satuan_2,
                    // satuan_1 : this.picRole.satuan_1,
                    // nilai_2 : this.picRole.nilai_2,
                    // nilai_1 : this.picRole.nilai_1,
                }
                this.loadingService.loadingStart();
                this.masterBahanService.saveEdit(obj)
                    .subscribe(
                        (res: ResponseWrapper) =>
                            this.onSaveSuccess(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res)
                    );
            }

        }
    }
    simpan() {
        console.log(this.picRole)
        this.pic = this.picRole.pic;
        this.kdbahan = this.picRole.kdBahan;
        this.kdlama = this.picRole.kdBahanLama;
        this.satuan = this.picRole.satuan;
        this.selectedStatus = this.picRole.status;
        this.ver();
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'MasterBahanListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Bahan saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Bahan Error', error._body);
        this.alertService.error(error._body, null, null);
    }
    previousState() {
        this.router.navigate(['/master-bahan', { page: this.paramPage }]);
    }
    back() {
        this.router.navigate(['/master-bahan'])
    }
}
