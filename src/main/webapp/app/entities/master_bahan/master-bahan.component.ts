import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterBahanService } from './master-bahan.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { ConfirmationService, LazyLoadEvent, AccordionTab, DataTable } from 'primeng/primeng';
import { LoadingBarHttp } from '@ngx-loading-bar/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { ExcelService } from '../report/excel.service';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-master-bahan',
    templateUrl: './master-bahan.component.html'
})
export class MasterBahanComponent implements OnInit, OnDestroy {
    @ViewChild('table') table: DataTable;
    picRoles: MasterBahan[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter_data: string;
    idInternal: string;
    first: number;
    currentInternal: string;
    status_bahan: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Aktif', value: 'Aktif' },
        { label: 'Non Aktif', value: 'Non Aktif' },
    ];
    tp_bahan: Array<object> = [
        { label: 'Semua', value: '' },
    ];
    [x: string]: any;
    inputStatus: string;
    inputKategori: string;
    isFilter: boolean;
    constructor(
        private picRoleServices: MasterBahanService,
        protected confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toasterService: ToasterService,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        protected loadingService: LoadingService,
        private paginationConfig: PaginationConfig,
        protected reportUtilService: ReportUtilService,

    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.idInternal = this.principal.getIdInternal();
        this.isFilter = false;
    }

    loadAll() {
        if (this.principal.getIdInternal() !== undefined || this.principal.getIdInternal() !== null) {
            this.loadingService.loadingStart();
            if (this.isFilter) {
                this.picRoleServices.queryFilter({
                    query: 'idInternal:' + this.idInternal + '|kategori_bahan:' + this.inputKategori + '|status_bahan:' + this.inputStatus,
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            } else {
                if (this.currentSearch) {
                    this.picRoleServices.search({
                        internals: 'idInternal:' + this.principal.getIdInternal(),
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }

                this.picRoleServices.query({
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        } else {

        }
    }

    filter() {
        this.isFilter = true;
        this.loadAll();
    }

    reset() {
        this.isFilter = false;
        this.inputKategori = '';
        this.inputStatus = '';
        this.loadAll();
    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
        this.loadingService.loadingStop();
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/master-bahan'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/master-bahan', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/master-bahan', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchasingItems();
        this.picRoleServices.getKategoriBarang({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                if (element.createdBy !== '' && element.createdBy !== null) {
                    this.tp_bahan.push({
                        label: element.createdBy,
                        value: element.approvedBy
                    });
                }
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MasterBahan) {
        return item.id;
    }
    registerChangeInPurchasingItems() {
        this.eventSubscriber = this.eventManager.subscribe('MasterBahanListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        console.log('panjang data: ', data.length);
        if (data.length === 0) {
            this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        } else if (data !== null) {
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.picRoles = data;
        } else {
            this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        }
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Cancle?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.picRoleServices.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'MasterBahanListModification',
                        content: 'Deleted an Data'
                    });
                });
            }
        });
    }

    public export() {
        this.loadingService.loadingStart();
        if (this.currentSearch === null || this.currentSearch === '' || this.currentSearch === undefined) {
            this.currentSearch = '';
        } else {
            this.currentSearch = this.currentSearch;
        }
        const date = new Date();
        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();

        this.filter_data = 'cari:' + this.currentSearch + '|div:' + this.currentInternal + '|kategori_bahan:' + this.inputKategori + '|status_bahan:' + this.inputStatus;
        this.reportUtilService.downloadFileWithName(`Report Master Bahan ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/ReportMasterBahan/xlsx', { filterData: this.filter_data });
        this.loadingService.loadingStop();

    }

    // loadDataLazy(event: LazyLoadEvent) {
    //     this.itemsPerPage = event.rows;
    //     this.page = Math.ceil(event.first / this.itemsPerPage);

    //     if (event.sortField !== undefined) {
    //         this.predicate = event.sortField;
    //         this.reverse = event.sortOrder;
    //     }
    //     this.loadAll();
    // }
}
