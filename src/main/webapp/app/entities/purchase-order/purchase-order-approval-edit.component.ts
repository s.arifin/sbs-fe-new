import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderService } from './purchase-order.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

import { OrderItem } from '../order-item/order-item.model';
import { OrderItemService } from '../order-item/order-item.service';

@Component({
    selector: 'jhi-purchase-order-approval-edit',
    templateUrl: './purchase-order-approval-edit.component.html'
})
export class PurchaseOrderApprovalEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    purchaseOrder: PurchaseOrder;
    isSaving: boolean;

    vendors: any[];

    internals: Internal[];

    billtos: any[];

    orderItems: OrderItem[];
    itemsPerPage: any;
    totalItems: any;

    confirmApprove: boolean;
    confirmReject: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected purchaseOrderService: PurchaseOrderService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.purchaseOrder = new PurchaseOrder();
    }

    ngOnInit() {
        this.confirmApprove = false;
        this.confirmReject = false;
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        // this.vendorService.query()
        //     .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.billToService.query()
        //     .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.purchaseOrderService.find(id).subscribe((purchaseOrder) => {
            if (purchaseOrder.dtOrder) {
                purchaseOrder.dtOrder = new Date(purchaseOrder.dtOrder);
            }
            this.purchaseOrder = purchaseOrder;
        });
    }

    previousState() {
        this.router.navigate(['purchase-order-approval']);
    }

    save() {
        this.isSaving = true;
        if (this.purchaseOrder.idPurchaseOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.update(this.purchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.create(this.purchaseOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
        result.subscribe((res: PurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PurchaseOrder) {
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'purchaseOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'purchaseOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    // trackVendorById(index: number, item: Vendor) {
    //     return item.idVendor;
    // }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    // trackBillToById(index: number, item: BillTo) {
    //     return item.idBillTo;
    // }

    confirmApproveDialog() {
        this.confirmApprove = true;
    }

    confirmRejectDialog() {
        this.confirmReject = true;
    }

    approve() {
        this.isSaving = true;
        if (this.purchaseOrder.idPurchaseOrder !== undefined) {
            // this.subscribeToSaveResponse(this.purchaseOrderService.update(this.purchaseOrder));
            // this.purchaseOrderService.executeProcess(101, null, this.purchaseOrder).subscribe(
            //     (value) => console.log('this: ', value),
            //     (err) => console.log(err),
            //     () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
            // );
        }
        this.confirmApprove = false;
        this.previousState();
    }

    reject() {
        this.isSaving = true;
        if (this.purchaseOrder.idPurchaseOrder !== undefined) {
            this.subscribeToSaveResponse(this.purchaseOrderService.update(this.purchaseOrder));
            // this.purchaseOrderService.executeProcess(102, null, this.purchaseOrder).subscribe(
            //     (value) => console.log('this: ', value),
            //     (err) => console.log(err),
            //     () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
            // );
        }
        this.confirmReject = false;
        this.previousState();
    }
}
