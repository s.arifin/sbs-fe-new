import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderService } from './purchase-order.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import * as ConfigConstants from '../../shared/constants/config.constants';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item';
import { FileCompressionService } from '../joborders/file-compression.service';

@Component({
    selector: 'jhi-purchase-order-edit',
    templateUrl: './purchase-order-edit.component.html'
})
export class PurchaseOrderEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;
    minimumDate: Date = new Date();

    protected subscription: Subscription;
    purchaseOrder: PurchaseOrder;
    isSaving: boolean;
    idPurchaseOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: any;
    dtNecessity: any;
    inputTipe: any;
    inputKet: any;
    internal: any;
    idlocal: any;
    isloc: any;
    curAccount: any;
    currentDiusul: any;
    currentMGR: any;
    currentKBG: any;
    sizePDF: any;

    tipe: Array<object> = [
        { label: 'Spare Parts', value: 'SP' },
        { label: 'Maintenace', value: 'MT' },
        { label: 'Service', value: 'SR' },
        { label: 'Fixed Asets', value: 'FA' },
        { label: 'Project', value: 'PR' }
    ];

    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK', value: 'TEKNIK' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' },
        { label: 'SKG-IT', value: 'SKG-IT' },
        { label: 'SKG-LOG', value: 'SKG-LOG' },
        { label: 'SKG-HRG', value: 'SKG-HRG' },
        { label: 'SKG-SLS', value: 'SKG-SLS' },
    ];

    sifats: Array<object> = [
        { label: 'Biasa', value: 'Biasa' },
        { label: 'Segera', value: 'Segera' },
        { label: 'Penting', value: 'Penting' }

    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }

    ];
    listMGR = [{}];
    listUSL = [{}];
    listASSMGR = [{}];
    listAPP = [{}];
    listKBG = [{}];
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected purchaseOrderService: PurchaseOrderService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected dataUtils: JhiDataUtils,
        protected loadingService: LoadingService,
        private fileCompressionService: FileCompressionService
    ) {
        this.purchaseOrder = new PurchaseOrder();
        this.routeId = 0;
        this.readonly = true;
        this.inputTipe = '';
        this.inputKet = '';
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.contents = null;
        this.contentTypes = null;
        this.sizePDF = 0;
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 5242880 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
                this.sizePdf = event.target.files[0].size;
            } else if (bytes < 5242880) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
                this.sizePdf = event.target.files[0].size;
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
            alert('Ukuran File Melebihi Batas 10MB, Silahkan Input Attachment dibawah 10 MB')
            this.sizePdf = 0;
        }
    }

    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        // this.dataUtils.clearInputImage(this.joborders, this.elementRef, field, fieldContentType, idInput);
        this.purchaseOrder.content = null;
        this.purchaseOrder.contentContentType = null;
        this.sizePdf = 0;
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], { 'type': fieldContentType });

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
    ngOnInit() {
        this.internal = this.principal.getIdInternal();
        this.curAccount = this.principal.getUserLogin().toUpperCase();
        console.log('userLogin :', this.curAccount);
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.idlocal = account.loc;
            console.log('internal : ', this.idlocal)
            if (account.loc === 'pabrik') {
                this.inputAppPabrik = 'SWY';
            }
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPurchaseOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.purchaseOrderService.getAllLTS({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listAPP.push({
                    label: element.div + ' - ' + element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllMGR({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listMGR.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllASSMGR({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listASSMGR.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
        this.purchaseOrderService.getAllUSL({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listUSL.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllKBG({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listKBG.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.isSaving = false;
        // this.vendorService.query()
        //     .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.internalService.query()
        //     .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.billToService.query()
        //     .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    openPDF(content: any, contentType: any) {
        const byteCharacters = atob(this.purchaseOrder.content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': this.purchaseOrder.contentContentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const win = window.open(content);
        window.open(URL.createObjectURL(blobContent), '_blank');
    }

    load() {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe((purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
            this.dtOrder = new Date(this.purchaseOrder.dtOrder);
            this.dtNecessity = new Date(this.purchaseOrder.dtNecessity);
            this.inputTipe = this.purchaseOrder.tipe;
            this.inputKet = this.purchaseOrder.ket;
            this.currentDiusul = this.purchaseOrder.diusulkan;
            this.currentMGR = this.purchaseOrder.appDept;
            this.currentKBG = this.purchaseOrder.appPabrik;
        });
    }

    uploadFile(event) {
        if (event.target.value) {
            const file = event.target.files[0];
            const type = file.type;
            this.changeFile(file).then((base64: string): any => {
                this.fileBlob = this.b64toBlob(base64, type);
                this.purchaseOrder.content = base64;
                this.purchaseOrder.contentContentType = type;
                this.sizePdf = event.target.files[0].size;
            });
        } else {
            alert('Nothing')
        }
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['purchase-order', { page: this.paramPage }]);
        }
    }
    // || this.curAccount !==  || this.curAccount !== this.currentMGR || this.curAccount !==
    masukSave() {
        this.isSaving = true;
        const tmp = new Date;
        const dari = (tmp.getFullYear() - 10) + '-' + (tmp.getMonth() + 1) + '-' + tmp.getDate();
        const smp = new Date(dari);
        this.loadingService.loadingStart();
        this.purchaseOrder.dtOrder = this.dtOrder;
        if (this.dtNecessity > smp) {
            this.purchaseOrder.dtNecessity = this.dtNecessity;
        }
        this.purchaseOrder.ket = this.inputKet;
        // console.log('idPurchaseOrder = ', this.purchaseOrder.idPurchaseOrder)
        if (this.purchaseOrder.idPurchaseOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.update(this.purchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.create(this.purchaseOrder));
        }
    }
    save() {
        if (this.sizePDF > this.maxFileSize) {
            alert('File Attachment Terlalu Besar, Maximal hanya 10 MB !');
        } else if (this.curAccount === this.purchaseOrder.createdBy) {
            this.masukSave()
        } else if (this.curAccount === this.currentDiusul) {
            this.masukSave()
        } else if (this.curAccount === this.currentMGR) {
            this.masukSave()
        } else if (this.curAccount === this.currentKBG) {
            this.masukSave()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
        result.subscribe((res: PurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PurchaseOrder) {
        // this.print(result.idPurchaseOrder);
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Permintaan Pembelian saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    print(id: any) {
        const filter_data = 'idPruchaseOrder:' + id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'purchaseOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
