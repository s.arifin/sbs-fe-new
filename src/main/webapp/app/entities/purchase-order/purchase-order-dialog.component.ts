import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderPopupService } from './purchase-order-popup.service';
import { PurchaseOrderService } from './purchase-order.service';
import { ToasterService, CommonUtilService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';
import { ITEMS_PER_PAGE, Principal } from '../../shared';

@Component({
    selector: 'jhi-purchase-order-dialog',
    templateUrl: './purchase-order-dialog.component.html'
})
export class PurchaseOrderDialogComponent implements OnInit {

    @Input() readonly = false;

    currentAccount: any;
    purchaseOrder: PurchaseOrder;
    isSaving: boolean;

    divitions: Array<object> = [
        {label : 'PPIC', value : 'PPIC'},
        {label : 'PROD', value : 'PROD'},
        {label : 'LPM', value : 'LPM'},
        {label : 'Purchasing', value : 'Purchasing'},
        {label : 'RND', value : 'RND'},
        {label : 'Teknik', value : 'Teknik'},
        {label : 'Marketing', value : 'Marketing'},
        {label : 'FNA', value : 'FNA'},
        {label : 'HRD', value : 'HRD'},
        {label : 'IT', value : 'IT'}
    ];

    sifats: Array<object> = [
        {label : 'Biasa', value : 'Biasa'},
        {label : 'Segera', value : 'Segera'},
        {label : 'Penting', value : 'Penting'}

    ];

    needs: Array<object> = [
        {label : 'Rutin', value : 'Rutin'},
        {label : 'Non Rutin', value : 'Non Rutin'}

    ];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected purchaseOrderService: PurchaseOrderService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        // this.vendorService.query()
        //     .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.internalService.query()
        //     .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.billToService.query()
        //     .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;

        const objectBody: object = {
            item: this.purchaseOrder,
            username: this.principal.getUserLogin().replace(/\./g, '$')
        };

        const objectHeader: Object = {
            execute: 'BuildAndValidatePurchaseOrder'
        };

        if (this.purchaseOrder.dtOrder != null && this.purchaseOrder.divition != null) {
            this.purchaseOrder.createdBy = this.principal.getUserLogin();
            this.subscribeToSaveResponse(this.purchaseOrderService.create(this.purchaseOrder));
        } else {
            if (this.purchaseOrder.dtOrder == null) {
                this.toaster.showToaster('info', 'save', 'Please Select Tanggal Order!')
            }
            if (this.purchaseOrder.divition == null) {
                this.toaster.showToaster('info', 'save', 'Please Select Divition!')
            }
        }
    }

    protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
        result.subscribe((res: PurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PurchaseOrder) {
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'purchaseOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'purchaseOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}

@Component({
    selector: 'jhi-purchase-order-popup',
    template: ''
})
export class PurchaseOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected purchaseOrderPopupService: PurchaseOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.purchaseOrderPopupService
                    .open(PurchaseOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.purchaseOrderPopupService.parent = params['parent'];
                this.purchaseOrderPopupService
                    .open(PurchaseOrderDialogComponent as Component);
            } else {
                this.purchaseOrderPopupService
                    .open(PurchaseOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
