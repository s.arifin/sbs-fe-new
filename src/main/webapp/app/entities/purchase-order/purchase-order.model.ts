import { BaseEntity } from '../../shared';
import { PurchaseOrderItem } from '../purchase-order-item/purchase-order-item.model';
import { MappingPriorityPPVM } from '../purchasing';

export class PurchaseOrder implements BaseEntity {
    constructor(
        public id?: number,
        public idPurchaseOrder?: any,
        public idpurchasing?: any,
        public noPo?: any,
        public noPurchaseOrder?: string,
        public dtOrder?: any,
        public divition?: string,
        public sifat?: string,
        public needs?: string,
        public appPembelian?: string,
        public dtAppPembelian?: any,
        public appPabrik?: string,
        public dtAppPabrik?: any,
        public appDept?: string,
        public dtAppDept?: any,
        public createdBy?: string,
        public dtCreateBy?: any,
        public Totaldata?: any,
        public idPurOrdItem?: any,
        public codeProduk?: any,
        public produkName?: string,
        public qty?: number,
        public qtyPO?: number,
        public unit?: string,
        public dtOffer?: Date,
        public offerNumber?: Date,
        public supplyerName?: string,
        public supplyerPrice?: number,
        public necessity?: string,
        public dtNecessity?: Date,
        public dtcreate?: Date,
        public remainingStock?: string,
        public remainingPo?: any,
        public tipe?: any,
        public ket?: any,
        public mataUang?: any,
        public idStatusType?: any,
        public status?: any,
        public contentContentType?: string,
        public content?: any,
        public noUrut?: any,
        public pic?: any,
        public data?: [PurchaseOrderItem],
        public dtApproved?: Date,
        public diusulkan?: any,
        public app1?: any,
        public dtapp1?: any,
        public app2?: any,
        public dtapp2?: any,
        public satuan?: any,
        public ket_not_app?: any,
        public dateStatus?: any,
        public appasmen?: any,
        public dtAppAsmen?: any,
        public loc?: any,
        public rejectNote?: any,
        public noprior?: any,
        public dtSent?: any,
        public item?: PurchaseOrderItem,
        public mapp?: MappingPriorityPPVM,
        public komen?: any,
        public appManajemen?: string,
        public dtappManajemen?: Date,
        public is_attach?: any,
        public is_inject?: number,
    ) {
        this.item = new PurchaseOrderItem();
    }
}

export class CustomPurchaseOrderHeader implements BaseEntity {
    constructor(
        public id?: number,
        public idPurchaseOrder?: any,
        public noPurchaseOrder?: string,
        public dtOrder?: any,
        public divition?: string,
        public sifat?: string,
        public needs?: string,
        public appPembelian?: string,
        public dtAppPembelian?: any,
        public appPabrik?: string,
        public dtAppPabrik?: any,
        public appDept?: string,
        public dtAppDept?: any,
        public createBy?: string,
        public dtCreateBy?: any,
        public Totaldata?: any,
    ) {

    }
}

export class CustomSupplyer implements BaseEntity {
    constructor(
        public id?: number,
        public kdSupplier?: any,
        public nmSupplier?: any,
        public almtSupplier1?: any,
        public almtSupplier2?: any,
        public telp?: any,
        public fax?: any,
        public person1?: any,
        public jsup?: any,
        public person2?: any,
    ) {

    }
}

export class MasterBarang implements BaseEntity {
    constructor(
        public id?: number,
        public idMstrbrg?: any,
        public kd_barang?: any,
        public nama_barang?: any,
        public satuan?: any,
        public lokasi?: any,
        public karantina?: any,
        public saldo?: any,
        public seksi?: any,
        public mesin?: any,
        public jenis?: any,
        public merek?: any,
        public tgl_input?: any,
        public jam_input?: any,
        public iduser?: any,
        public foto?: any,
        public waktu_ubah?: any,
        public iduser_ubah?: any,
        public pic?: any,
        public saldo_pp?: any,
        public saldo_po?: any,
        public max_saldo?: any,
        public saldo_tg?: any
    ) {

    }
}

// export class MasterBarang implements BaseEntity {
//     constructor(
//         public idMstrbrg?: any,
//         public kd_barang?: any,
//         public nama_barang?: any,
//         public satuan?: any,
//         public lokasi?: any,
//         public karantina?: any,
//         public stock?: any,
//         public seksi?: any,
//         public mesin?: any,
//         public jenis?: any,
//         public merek?: any,
//         public tgl_input?: any,
//         public jam_input?: any,
//         public iduser?: any,
//         public foto?: any,
//         public waktu_ubah?: any,
//         public iduser_ubah?: any,
//     ) {

//     }
// }

export class MasterBahan implements BaseEntity {
    constructor(
        public id?: number,
        public idMasBahan?: any,
        public tpBahan?: any,
        public kdUmum?: string,
        public kdBahan?: string,
        public nmBahan?: string,
        public satuan?: string,
        public status?: string,
        public stok?: any,
        public karantina?: any,
        public tolak?: any,
        public harga?: any,
        public kdBahanLama?: any,
        public saldo?: any,
        public masuk?: any,
        public keluar?: any,
        public krtn?: any,
        public supp?: any,
        public lain?: any,
        public korr?: any,
        public expHalal?: Date,
        public pic?: any,
        public div?: any,
        public saldo_pp?: any,
        public saldo_po?: any,
        public max_saldo?: any,
        public kategori?: any,
        public tipe_wadah?: any,
        public is_halal?: any,
        public satuan_2?: string,
        public nilai_2?: number,
        public satuan_1?: string,
        public nilai_1?: number,
        public saldo_tg?: any
    ) {

    }
}

export class Budgeting implements BaseEntity {
    constructor(
        public id?: number,
        public idrequest?: any,
        public requestNumber?: any,
        public dtRequest?: any,
        public reqType?: any,
        public div?: any,
        public description?: any,
        public dtCreated?: any,
        public createdBy?: any,
        public idBudget?: any,
        public location?: any,
        public pcAstUdr5Th?: any,
        public lpAstUdr5Th?: any,
        public qtyAstPrntr?: any,
        public pcAstUpr5Th?: any,
        public lpAstUpr5Th?: any,
        public qtyPcReq?: any,
        public qtyLpReq?: any,
        public qtyPntrReq?: any,
        public idReqItem?: any,
        public kdBarang?: any,
        public nmBarang?: any,
        public qty?: any,
        public totalPrice?: any,
    ) {
    }
}

export class MasterBahanIt implements BaseEntity {
    constructor(
        public id?: number,
        public idMasBah?: any,
        public tipe?: any,
        public kdBahan?: string,
        public nmBahan?: string,
        public harga?: any,
        public ket?: any,
    ) {

    }
}
