import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderService } from './purchase-order.service';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PurchaseOrderNewComponent } from './purchase-order-new.component'
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PurchaseOrderDialogComponent } from './purchase-order-dialog.component';
import { PurchaseOrderEditComponent } from './purchase-order-edit.component';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Purchasing, PurchasingService, RegisterPPDetail } from '../purchasing';
import { LoadingService } from '../../layouts';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { PurchasingFilter } from '../purchasing/purchasing-filter.model';
import { HistoryReason } from '../history-reason/history-reason.model';
import { HistoryReasonService } from '../history-reason/history-reason.service';

@Component({
    selector: 'jhi-purchase-order',
    templateUrl: './purchase-order.component.html'
})
export class PurchaseOrderComponent implements OnInit, OnDestroy {

    currentAccount: any;
    purchaseOrders: PurchaseOrder[];
    purchaseOrders1: PurchaseOrder[];
    hr: HistoryReason;
    listHR: HistoryReason[];
    modalListHR: boolean;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;
    stat: any;
    purchasings: Purchasing[];
    modalLihatPO: boolean;

    // field filter
    filDtFrom: Date;
    filDtThru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    newSuplier: MasterSupplier;
    kdSPL: string;
    isFilter: boolean;
    isFilter1: boolean;
    isFilter2: boolean;
    selectedPIC: string;
    dtFilter: PurchasingFilter;
    // field filter
    loginName: string;

    hideButton: boolean;
    idpp: any;

    // Filter Kebutuhan PP
    inputNeeds: any;
    inputselectedPP: any;
    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }
    ];

    arrayPP: Array<object> = [
        { label: 'Sudah di Buat PO(Belum di kirim)', value: '1' },
        { label: 'Belum di Buat PO', value: '2' }
    ];

    dtneed: string;
    non_rutin: boolean;

    divitions: Array<object> = [
        { label: '', value: '' },
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'HSE', value: 'HSE' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK', value: 'TEKNIK' },
        { label: 'GPO', value: 'GPO' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' }
    ];
    curAccount: string;

    constructor(
        protected purchaseOrderService: PurchaseOrderService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected modalService: NgbModal,
        protected loadingService: LoadingService,
        protected purchasingService: PurchasingService,
        private masterSupplierService: MasterSupplierService,
        protected toaster: ToasterService,
        private historyReasonService: HistoryReasonService
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.purchasings = new Array<Purchasing>();
        this.modalLihatPO = false;
        this.filDtFrom = new Date();
        this.hideButton = false;
        this.filDtThru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdSPL = '';
        this.isFilter = false;
        this.selectedPIC = '';
        this.dtFilter = new PurchasingFilter();
        this.loginName = '';
        this.hr = new HistoryReason();
        this.listHR = new Array<HistoryReason>();
        this.modalListHR = false;
        this.non_rutin = false;
        this.inputNeeds = 'Rutin';
        this.inputselectedPP = '';
        this.isFilter2 = false;
    }

    openAdd() {
        const modalRef = this.modalService.open(PurchaseOrderNewComponent, { size: 'lg', backdrop: 'static' });
        // console.log('OpenAdd ' + modalRef);
        // modalRef.componentInstance.purchaseOrder = new PurchaseOrder();
        // modalRef.result.then((result) => {}, (reason) => {});
        // this.router.navigate(['../new'])
    }

    filter() {
        this.isFilter = true;
        this.currentSearch = '';
        this.loadAll()
    }

    filter1() {
        this.isFilter1 = true;
        this.currentSearch = '';
        this.loadAll()
    }

    filter2() {
        this.isFilter2 = true;
        this.currentSearch = '';
        this.loadAll()
    }

    kebutuhan() {
        // this.ngOnInit();
        if (this.inputNeeds === 'Rutin') {
            this.non_rutin = false;
        } else {
            this.non_rutin = true;
        }
        // console.log('isi non rutin', this.non_rutin)
    }

    reset() {
        this.isFilter = false;
        this.isFilter1 = false;
        this.isFilter2 = false;
        this.currentSearch = ''
        this.filDtFrom = new Date();
        this.filDtThru = new Date();
        this.selectedPIC = '';
        this.newSuplier = new MasterSupplier();
        this.inputNeeds = 'Rutin';
        this.inputselectedPP = '';
        this.loadAll()
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter === true) {
            console.log('isi filter ' + this.isFilter);
            this.dtFilter.pic = this.selectedPIC;
            this.dtFilter.from = this.filDtFrom;
            this.dtFilter.thru = this.filDtThru;
            this.dtFilter.kdspl = this.newSuplier.nmSupplier;
            this.purchaseOrderService.queryFilter({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort(),
                pic: this.selectedPIC,
                from: this.dtFilter.from.toJSON(),
                thru: this.dtFilter.thru.toJSON(),
                kdspl: this.newSuplier.kd_suppnew
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccessfilter(res.json, res.headers),
                (reserr: ResponseWrapper) => this.onErrorSearch(reserr.json),
            );
            return;
        } else if (this.isFilter1 === true) {
            console.log('isi filter ' + this.isFilter1);
            this.dtFilter.need = this.inputNeeds;
            console.log(this.dtFilter.need);
            this.purchaseOrderService.queryFilter1({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort(),
                need: this.dtFilter.need
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccessfilter(res.json, res.headers),
                (reserr: ResponseWrapper) => this.onErrorSearch(reserr.json),
            );
            return;
        } else if (this.isFilter2 === true) {
            if (this.inputselectedPP === '') {
                alert('Status PP untuk filter tidak boleh kosong');
                this.reset();
            } else {
                this.dtFilter.need = this.inputNeeds;
                console.log(this.dtFilter.need);
                this.purchaseOrderService.queryFilter2({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort(),
                    need: this.inputselectedPP
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccessfilter(res.json, res.headers),
                    (reserr: ResponseWrapper) => this.onErrorSearch(reserr.json),
                );
            }
            return;
        } else if (this.currentSearch && (this.isFilter === false || this.isFilter1 === false || this.isFilter2 === false)) {
            console.log('isi currentSearch ' + this.currentSearch);
            this.purchaseOrderService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onErrorSearch(res.json)
            );
            return;
        } else {
            this.purchaseOrderService.queryFilterByC({
                query: 'idInternal:' + this.principal.getIdInternal(),
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (reserr: ResponseWrapper) => this.onError(reserr.json),
            );
        }
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
        this.loadingService.loadingStop();
        this.isFilter = false;
        this.isFilter1 = false;
        this.isFilter2 = false;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/purchase-order'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.isFilter = false;
        this.router.navigate(['/purchase-order', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isFilter = false;
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    print(id: any, raw: PurchaseOrder) {
        if (raw.idStatusType === 10) {
            this.toasterService.showToaster('warning', 'Data Belum Di Approve', raw.noPurchaseOrder);
        } else if (raw.idStatusType === 13) {
            this.toasterService.showToaster('warning', 'Data Sudah Di Cancel', raw.noPurchaseOrder);
        } else {
            const idinternal = this.principal.getIdInternal();
            const filter_data = 'idPruchaseOrder:' + id;
            if (idinternal.includes('SKG')) {
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderSKG/pdf', { filterData: filter_data });
            } else if (idinternal.includes('SMK')) {
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderSMK/pdf', { filterData: filter_data });
            } else {
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
            }
        }
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    ngOnInit() {
        this.curAccount = this.principal.getUserLogin();
        this.principal.identity(true).then((account: Account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInPurchaseOrders();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    registerChangeInPurchaseOrders() {
        this.loadingService.loadingStart();
        this.eventSubscriber = this.eventManager.subscribe('purchaseOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'noPurchaseOrder') {
            result.push('noPurchaseOrder');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.purchaseOrders = data;
        this.loadingService.loadingStop();

    }

    protected onSuccessfilter(data, headers) {
        if (data.length > 0) {
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.purchaseOrders = data;
            this.loadingService.loadingStop();
        } else {
            alert('Data Tidak Ditemukan');
        }
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(item) {
        this.purchaseOrderService.executeProcess(item).subscribe(
            //    (value) => console.log('this: ', value),
            //    (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.purchaseOrderService.update(event.data)
                .subscribe((res: PurchaseOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.purchaseOrderService.create(event.data)
                .subscribe((res: PurchaseOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: PurchaseOrder) {
        this.toasterService.showToaster('info', 'PurchaseOrder Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Cancel?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.purchaseOrderService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'purchaseOrderListModification',
                        content: 'Deleted an purchaseOrder'
                    });
                });
            }
        });
    }

    reqAppoved(id: any) {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.idpp = id;
                this.loadingService.loadingStart();
                this.ApprovedAtDialogDetail();

                // this.loadingService.loadingStart();
            }
        });
    }

    ApprovedAtDialogDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            idPurchaseOrder: this.idpp
        }
        this.purchaseOrderService.changeReqAPPROVE(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        // console.log('isi selected', obj);
    }

    private onSuccessApproved(data, headers) {
        // console.log('masuk on sukes approve');
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Request Approve Succeed');
        this.idpp = null;
        if (data === null || data === '' || data === undefined) {

        } else {
            alert(data);
        }
        this.loadAll();
        // this.totalItems = data[0].Totaldata;
        // this.queryCount = this.totalItems;
        // this.loadingService.loadingStop();
    }

    buildReindex() {
        this.purchaseOrderService.process({ command: 'buildIndex' }).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
    // process() {
    //     this.purchaseOrderService.process({}).subscribe((r) => {
    //         console.log('result', r);
    //         this.toasterService.showToaster('info', 'Data Processed', 'processed.....');
    //     });
    // }

    lihatPO(idpp: any) {
        this.purchasingService.getPObyIdPPItem({
            idPurchaseOrder: idpp
        }).subscribe(
            (res: ResponseWrapper) => {
                this.purchaseOrders1 = res.json;
                // console.log('ini data PP to PO = ', this.purchaseOrders);
                this.modalLihatPO = true
            })
    }
    printPO(purchasing: PurchaseOrder) {
        const filter_data = 'idpurchaseorder:' + purchasing.idpurchasing;
        if (purchasing.idpurchasing == null) {
            alert('Data Belum Dibuatkan PO');
            this.toasterService.showToaster('info', 'Data Belum Dibuatkan PO', '');
        } else {
            if (purchasing.noPo.includes('SKG')) {
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHargaSKG/pdf', { filterData: filter_data });
            } else if (purchasing.noPo.includes('SMK')) {
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHargaSMK/pdf', { filterData: filter_data });
            } else {
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHarga/pdf', { filterData: filter_data });
            }
        }
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }

    cekReasonpp(idpp: any) {
        // console.log('idpp: ', idpp)
        this.loadingService.loadingStart();
        this.historyReasonService.getByForeign(idpp)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.listHR = res.json;
                    this.loadingService.loadingStop();
                    this.modalListHR = true;
                },
                (err) => {
                    this.loadingService.loadingStop();
                }
            )
    }

    openPDF(idpp: any) {
        this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }

    openAttach(idpp: any) {
        this.purchaseOrderService.getPP2(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }
}
