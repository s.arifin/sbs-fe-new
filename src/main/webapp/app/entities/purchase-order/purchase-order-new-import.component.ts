import { Component, OnInit, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderService } from './purchase-order.service';
import { ToasterService, Account } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
import { CustomPurchaseOrderHeader, MasterBahan, CustomSupplyer, MasterBarang } from './index';
import { Header, FileUpload } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/index';
import * as ConfigConstants from '../../shared/constants/config.constants';
import { FileUploadModule } from 'primeng/primeng';
import { forEach, isNull, isObjectLike } from 'lodash';
import { not } from '@angular/compiler/src/output/output_ast';
import { getDay } from 'ngx-bootstrap/chronos/utils/date-getters';
// import { Ng2Uploader } from 'ng2-file-uploader';
import { FileCompressionService } from '../joborders/file-compression.service';
import * as XLSX from 'xlsx';
import { parseDate } from 'ngx-bootstrap/chronos';

@Component({
    selector: 'jhi-purchase-order-new-import',
    templateUrl: './purchase-order-new-import.component.html'
})
export class PurchaseOrderNewImportComponent implements OnInit, OnDestroy {
    [x: string]: any;
    commontUtilService: any;

    @Input() readonly = false;
    selectedFile: File | null = null;
    protected subscription: Subscription;
    purchaseOrder: PurchaseOrder;
    header: CustomPurchaseOrderHeader;
    isSaving: boolean;
    idPurchaseOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: any;
    minimumDate: Date = new Date();
    inputPurchaseNumber: any;
    inputDtOrder: any;
    inputDivition: any;
    inputSifat: any;
    inputTipe: any;
    inputNeeds: any;
    inputKet: any;
    inputAppPembelian: any;
    inputAppPabrik: any;
    inputAppDept: any;

    selectedSLNumber: string;
    selectedshipmentPackageId: string;
    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputSupplyerName: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: Date;
    inputRemainingStock: any;
    inputRemainingPo: any;
    unitChecks: PurchaseOrder[];
    unitChecksCopy: PurchaseOrder[];
    hasilSave: PurchaseOrder;
    inputOfferNumber: any;

    internal: any;
    isOpen: Boolean = false;
    index: number;
    supplier: CustomSupplyer;
    Supplyer: CustomSupplyer[];
    selectedSupplier: any;
    listSupplier = [{ label: 'Please Select or Insert', value: null }];

    public barang: MasterBahan;
    public Barang: MasterBahan[];
    public selectedBarang: any;
    public listBarang = [{ label: 'Please Select or Insert', value: null }];

    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];

    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK', value: 'TEKNIK' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' }
    ];

    tipe: Array<object> = [
        { label: 'Spare Parts', value: 'SP' },
        { label: 'Maintenace', value: 'MT' },
        { label: 'Service', value: 'SR' },
        { label: 'Fixed Asets', value: 'FA' },
        { label: 'Project', value: 'PR' }
    ];

    sifats: Array<object> = [
        { label: 'Biasa', value: 'Biasa' },
        { label: 'Segera', value: 'Segera' },
        { label: 'Penting', value: 'Penting' }

    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }

    ];

    satHarga: Array<object> = [
        { label: 'RUPIAH - IDR', value: 'IDR' },
        { label: 'DOLAR AS - USD', value: 'USD' },
        { label: 'EURO - EUR', value: 'EUR' },
        { label: 'DOLAR SINGAPURA - SGD', value: 'SGD' },
        { label: 'DOLAR AUSTRALIA - AUD', value: 'AUD' },
        { label: 'YEN - JPY', value: 'JPY' },
        { label: 'POUND - GBP', value: 'GBP' },
    ];

    inputSatHarga: any;
    newSuplier: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];
    pilihTipe: any;
    newKdBarang: any;
    newKdBarangs: any[];
    filteredKdBarang: any[];
    pilihKdBarang: any;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    uploader: FileUpload;
    hasBaseDropZoneOver: boolean;
    hasAnotherDropZoneOver: boolean;
    response: string;
    non_rutin: boolean;
    newProj: any;
    newProjs: any[];
    listProj = [{}];
    listKBG = [{}];
    listMGR = [{}];
    listASSMGR = [{}];
    listUSL = [{}];
    listAPP = [{}];
    sizePdf: any;
    picBarang: any;
    inputSaldo: any;
    saldoPP: any;
    saldoPO: any;
    maxSaldo: any;
    inputSatuan: any;
    noUrut: any;
    inputDiusulkan: any;
    inputAppAssmen: any;
    inputApp1: any;
    inputApp2: any;
    isSelcBrg: boolean;
    idlocal: any;
    isloc: any;
    inputDtRequest: any;
    public selectedTglBth: Date;

    tglKebutuhanPlus: Date = new Date();
    tgl: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected dataUtils: JhiDataUtils,
        protected alertService: JhiAlertService,
        protected purchaseOrderService: PurchaseOrderService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        private fileCompressionService: FileCompressionService
    ) {
        this.tgl = this.tglKebutuhanPlus.getDay() + 2;
        this.purchaseOrder = new PurchaseOrder();
        this.routeId = 0;
        this.dtOrder = new Date();
        // this.inputDtOffer = new Date();
        this.inputDtNecessity = new Date();
        // this.inputDtNecessity = this.tglKebutuhanPlus.getDay() + 2;
        this.barang = new MasterBahan();
        this.kdbarang = new MasterBahan();
        this.isSelectSupplier = false;
        this.inputTipe = '';
        this.inputKet = '';
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.non_rutin = false;
        this.sizePdf = null;
        this.picBarang = null;
        this.inputSaldo = 0;
        this.saldoPO = 0;
        this.saldoPP = 0;
        this.maxSaldo = 0;
        this.inputSatuan = null;
        this.isSelcBrg = true;
        this.inputNeeds = 'Rutin';
        this.noUrut = 0;
        // this.inputDtRequest = new Date();
    }
    protected resetMe() {
        this.selectedBarang = null;
        this.selectedTglBth = null;
    }

    kebutuhan() {
        // this.ngOnInit();
        if (this.inputNeeds === 'Rutin') {
            this.non_rutin = false;
        } else {
            this.non_rutin = true;
        }
    }

    changeFile(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        });
    }

    uploadFile(event) {
        if (event.target.value) {
            const file = event.target.files[0];
            const type = file.type;
            this.changeFile(file).then((base64: string): any => {
                this.fileBlob = this.b64toBlob(base64, type);
                this.purchaseOrder.content = base64;
                this.purchaseOrder.contentContentType = type;
                this.sizePdf = event.target.files[0].size;
            });
        } else {
            alert('Nothing')
        }
    }

    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(this.purchaseOrder.content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], { 'type': this.purchaseOrder.contentContentType });

        if (navigator.msSaveBlob) {
            const filename = 'Image';
            navigator.msSaveBlob(this.fileBlob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(this.fileBlob);

            link.setAttribute('visibility', 'hidden');
            link.download = 'Image';

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    clearPdf() {
        this.purchaseOrder.content = null;
        this.purchaseOrder.contentContentType = null;
        this.sizePdf = null;
    }

    b64toBlob(dataURI, type) {

        const byteString = atob(dataURI.split(',')[1]);
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);

        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], { type: 'type' });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 5242880 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            } else if (bytes < 5242880) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
            this.sizePdf = event.target.files[0].size;
            alert('Ukuran File Melebihi Batas 10MB')
        }
    }

    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.purchaseOrder, this.elementRef, field, fieldContentType, idInput);
    }

    filterBarangSingle(kd_barang: string) {
        const query = kd_barang;
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.getNewBarangTeknik().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        } else {
            this.purchaseOrderService.getNewBarang().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        }
    }

    filterBarang(query, newBarangs: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.barang.idMasBahan = this.newBarang.idMasBahan;
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.newBarang.idMasBahan)
                .subscribe(
                    (res: MasterBarang) => {
                        this.inputCodeName = res.kd_barang;
                        this.inputProdukName = res.nama_barang;
                        this.newKdBarang = res.kd_barang;
                        this.picBarang = res.pic;
                        this.inputSaldo = res.saldo;
                        this.saldoPO = res.saldo_po;
                        this.saldoPP = res.saldo_pp;
                        this.maxSaldo = res.max_saldo;
                        this.inputSatuan = res.satuan;
                        this.isSelcBrg = true;
                    },
                    (res: MasterBahan) => {
                        this.inputCodeName = null;
                        this.isSelcBrg = false;
                    }
                )
        } else {
            this.purchaseOrderService.findBarang(this.newBarang.idMasBahan)
                .subscribe(
                    (res: MasterBahan) => {
                        if (res.kategori === null || res.kategori === '' || res.kategori === undefined) {
                            alert('Kategori Barang Kosong, Silahkan edit terlebih dahulu di Master Bahan sebelum membuat PP dengan item ini');
                        } else if (res.pic === null || res.pic === '' || res.pic === undefined) {
                            alert('PIC Item Kosong, Silahkan edit terlebih dahulu di Master Bahan sebelum membuat PP dengan item ini');
                        } else {
                            this.inputCodeName = res.kdBahan;
                            this.inputProdukName = res.nmBahan;
                            this.newKdBarang = res.kdBahan;
                            this.picBarang = res.pic;
                            this.inputSaldo = res.stok + res.karantina;
                            this.saldoPO = res.saldo_po;
                            this.saldoPP = res.saldo_pp;
                            this.maxSaldo = res.max_saldo;
                            this.inputSatuan = res.satuan;
                        }

                    },
                    (res: MasterBahan) => {
                        this.inputCodeName = null;
                    }
                )
        }
    }

    public selectTglBth(isSelect?: boolean): void {
        if (this.inputDtNecessity.setSeconds(59) >= this.dtOrder) {
            this.selectedTglBth = this.inputDtNecessity;
            this.inputDtRequest = this.inputDtNecessity;
        } else {
            alert('Tgl dibutuhkan tidak boleh kurang dari Tgl Permintaan');
            this.inputDtNecessity = new Date;
        }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.purchaseOrderService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inputSupplyerName = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
    }

    onSuccessBarang(data, headers) {
        this.inputCodeName = data.kdBahan;
    }

    onErrorBarang(error) {
        this.inputCodeName = null;
    }

    simpanTipe() {
        this.pilihTipe = (this.inputTipe);
    }

    openPDF(content: any, contentType: any) {
        const byteCharacters = atob(this.purchaseOrder.content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': this.purchaseOrder.contentContentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const win = window.open(content);
        window.open(URL.createObjectURL(blobContent), '_blank');
    }

    ngOnInit() {
        // this.inputDtNecessity = this.tglKebutuhanPlus.getFullYear() + '-' + this.tglKebutuhanPlus.getMinutes() +'-'+ this.tglKebutuhanPlus.getDay() + 2;
        this.inputDtNecessity.setDate(this.tglKebutuhanPlus.getDate() + (this.tgl));
        // this.tglKebutuhanPlus.setDate(this.tglKebutuhanPlus.getDate()+2);
        this.internal = this.principal.getIdInternal();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.idlocal = account.loc;
            if (account.loc === 'pabrik') {
                this.inputAppPabrik = 'SWY';
            }
        });

        this.purchaseOrderService.getAllLTS({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listAPP.push({
                    label: element.div + ' - ' + element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
        this.purchaseOrderService.getAllKBG({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listKBG.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllMGR({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listMGR.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllASSMGR({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listASSMGR.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllUSL({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listUSL.push({
                    label: element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.purchaseOrderService.getAllProj({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listProj.push({
                    label: element.nm_project,
                    value: element.nm_project
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPurchaseOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.pilihTipe = this.inputTipe;
        this.unitChecks = Array<PurchaseOrder>();
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.clearForm();
    }

    validateAdd() {
        if (this.selectedFile) {
            const reader = new FileReader();
            reader.onload = (e: any) => {
                const arrayBuffer = e.target.result;

                // Convert ArrayBuffer to binary string
                const binaryData = new Uint8Array(arrayBuffer).reduce((data, byte) => {
                    return data + String.fromCharCode(byte);
                }, '');

                const workbook = XLSX.read(binaryData, { type: 'binary' });
                const firstSheetName = workbook.SheetNames[0];
                const worksheet = workbook.Sheets[firstSheetName];
                const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

                this.headers = jsonData[0];
                this.tableData = jsonData.slice(1);

                // Proses setiap baris untuk cek stok
                this.tableData.forEach((row) => {
                    const noUrut = row[0]; // Asumsi Kode Barang ada di kolom ke-3 (indeks 2)
                    const kodeBarang = row[1]; // Asumsi Kode Barang ada di kolom ke-3 (indeks 2)
                    const qtyBarang = parseFloat(row[2]); // Asumsi Kode Barang ada di kolom ke-3 (indeks 2)
                    const tglKirim = row[3];
                    const tglPenawaran = row[4];
                    const noPenawaran = row[5];
                    const suppName = row[6];
                    const suppPrice = parseFloat(row[8]);
                    const valuta = row[7];
                    const catatanItem = row[10];
                    const peruntukan = row[9];
                    if (kodeBarang === null || kodeBarang === undefined || kodeBarang === '') {
                        alert(`Kode Barang Pada Baris Ke - ${noUrut} Kosong, Silahkan Tentukan Terlebih Dahulu Kodenya`);
                    } else {
                        // const barang = this.filterBarangSingle(kodeBarang);
                        if (this.internal === 'TEKNIK') {
                            this.purchaseOrderService.getNewBarangTeknik().then((newBarangs) => {
                                const data1 = this.filterBarang(kodeBarang, newBarangs);
                                this.purchaseOrderService.findBarangTeknik(data1[0].idMasBahan)
                                    .subscribe(
                                        (res: MasterBarang) => {
                                            if (tglKirim === undefined || tglKirim === null) {
                                                alert(`Tanggal Kirim Harus Diisi Pada Item No Urut ${noUrut}`);
                                            } else {
                                                if (res.pic === null || res.pic === undefined || res.pic === '') {
                                                    alert(`PIC Kosong, Edit Terlebih Dahulu di Master Barang Untuk Item ${kodeBarang}`);
                                                } else {
                                                    if (this.unitChecks.length < 1) {
                                                        this.addDataImportTeknik(res, noUrut, tglKirim, qtyBarang, tglPenawaran, noPenawaran, suppName, suppPrice, valuta, catatanItem, peruntukan);
                                                    } else if (this.unitChecks.length > 0) {
                                                        if (this.unitChecks[0].pic !== res.pic) {
                                                            alert(`PIC Item No Urut ${noUrut} berbeda dengan Item No Urut Awal`);
                                                        } else {
                                                            this.addDataImportTeknik(res, noUrut, tglKirim, qtyBarang, tglPenawaran, noPenawaran, suppName, suppPrice, valuta, catatanItem, peruntukan);
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        (res: MasterBarang) => {
                                            alert(`Kode Barang ${kodeBarang} Tidak Ditemukan`);
                                        }
                                    )
                            });
                        } else {
                            this.purchaseOrderService.getNewBarang().then((newBarangs) => {
                                const data1 = this.filterBarang(kodeBarang, newBarangs);
                                this.purchaseOrderService.findBarang(data1[0].idMasBahan)
                                    .subscribe(
                                        (res: MasterBahan) => {
                                            if (tglKirim === undefined || tglKirim === null) {
                                                alert(`Tanggal Kirim Harus Diisi Pada Item No Urut ${noUrut}`);
                                            } else {
                                                if (res.pic === null || res.pic === undefined || res.pic === '') {
                                                    alert(`PIC Kosong, Edit Terlebih Dahulu di Master Barang Untuk Item ${kodeBarang}`);
                                                } else {
                                                    if (this.unitChecks.length < 1) {
                                                        this.addDataImport(res, noUrut, tglKirim, qtyBarang, tglPenawaran, noPenawaran, suppName, suppPrice, valuta, catatanItem, peruntukan);
                                                    } else if (this.unitChecks.length > 0) {
                                                        if (this.unitChecks[0].pic !== res.pic) {
                                                            alert(`PIC Item No Urut ${noUrut} berbeda dengan Item No Urut Awal`);
                                                        } else {
                                                            this.addDataImport(res, noUrut, tglKirim, qtyBarang, tglPenawaran, noPenawaran, suppName, suppPrice, valuta, catatanItem, peruntukan);
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        (res: MasterBahan) => {
                                            alert(`Kode Barang ${kodeBarang} Tidak Ditemukan`);
                                        }
                                    )
                            });
                        }
                    }
                });
            };

            reader.readAsArrayBuffer(this.selectedFile);
        } else {
            alert('Silahkan Pilih File Excel Terlebih Dahulu');
        }
    }
    // const totalStock = this.inputSaldo + this.saldoPP + this.saldoPO + this.inputQty;
    // if (this.inputDtRequest === null || this.inputDtRequest === ' ' || this.inputDtRequest === undefined) {
    //     alert('Tanggal Kirim Belum Terisi, Isi Tgl Kirim Terlebih Dahulu');
    // } else {
    //     if (totalStock > this.maxSaldo) {
    //         alert('Jika Qty Yang Dimasukan ' + this.inputQty + ' Stok Akan Menjadi ' + totalStock + ', Stok Barang Akan Melebihi Rata-rata Stock');
    //         if (this.picBarang === null || this.picBarang === undefined || this.picBarang === '') {
    //             alert('Barang Belum Bisa Di Pilih Karena PIC Pembelian Barang Belum Ada. Mohon Untuk Menginfokan Kepada Departement Pembelian.');
    //         } else if (this.unitChecks.length < 1) {
    //             this.addData();
    //         } else {
    //             if (this.unitChecks[0].pic !== this.picBarang) {
    //                 alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.');
    //             } else {
    //                 this.addData();
    //             }
    //         }
    //     } else {
    //         if (this.picBarang === null || this.picBarang === undefined || this.picBarang === '') {
    //             alert('PIC Pembelian Barang Belum Ada. Mohon Untuk Menginfokan Kepada Departement Pembelian.');
    //         } else if (this.unitChecks.length < 1) {
    //             this.addData();
    //         } else {
    //             if (this.unitChecks[0].pic !== this.picBarang) {
    //                 alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.');
    //             } else {
    //                 this.addData();
    //             }
    //         }
    //     }
    // }

    addData(): void {
        const data = new PurchaseOrder();
        data.idPurchaseOrder = this.selectedshipmentPackageId;
        data.noUrut = this.noUrut;
        data.codeProduk = this.inputCodeName;
        if (this.inputCodeName == null) {
            data.produkName = this.newBarang;
        } else {
            data.produkName = this.inputProdukName;
        }
        if (this.isSelectSupplier === true) {
            data.supplyerName = this.newSuplier.nmSupplier;
        } else {
            data.supplyerName = this.newSuplier;
        }
        data.pic = this.picBarang;
        data.qty = this.inputQty;
        // data.satuan = this.inputSatuan;
        data.unit = this.inputSatuan;
        data.dtOffer = this.inputDtOffer;
        data.dtSent = this.inputDtRequest;
        data.offerNumber = this.inputOfferNumber;
        data.supplyerPrice = this.inputSupplyerPrice;
        data.necessity = this.inputNecessity;
        data.dtNecessity = this.inputDtNecessity;
        data.remainingStock = this.inputRemainingStock;
        data.remainingPo = this.inputRemainingPo;
        data.mataUang = this.inputSatHarga;
        if (data.qty == null) {
            this.toaster.showToaster('info', 'Save', 'Please Insert Qty!');
        } else if (data.codeProduk == null || data.produkName == null) {
            this.toaster.showToaster('info', 'Save', 'Please Insert Item!');
        } else {
            this.unitChecks = [...this.unitChecks, data];
            this.clearDataInput();
        }
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }

    }

    addDataImport(res: MasterBahan, nourut: number, tglKirim: Date, qty: number, tglPenawaran: Date, noPenawaran: any, supplyerName: string, supplyerPrice: number, valuta: string, catatanItem: string, peruntukan: string): void {
        const data = new PurchaseOrder();
        data.idPurchaseOrder = this.selectedshipmentPackageId;
        data.noUrut = nourut;
        data.codeProduk = res.kdBahan;
        data.produkName = res.nmBahan;
        data.supplyerName = supplyerName;
        data.pic = res.pic;
        data.qty = qty;
        data.unit = res.satuan;
        data.dtOffer = tglPenawaran;
        data.dtSent = tglKirim;
        data.offerNumber = noPenawaran;
        data.supplyerPrice = supplyerPrice;
        data.necessity = peruntukan;
        data.dtNecessity = this.inputDtNecessity;
        data.remainingStock = catatanItem;
        data.remainingPo = '';
        data.mataUang = valuta;
        if (data.qty == null || data.qty <= 0) {
            this.toaster.showToaster('info', 'Save', `Please Insert Qty! Pada Item No Urut ${nourut}`);
        } else if (data.codeProduk == null || data.produkName == null) {
            this.toaster.showToaster('info', 'Save', `Please Insert Item! Pada Item No Urut ${nourut}`);
        } else {
            this.unitChecks = [...this.unitChecks, data];
        }
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    addDataImportTeknik(res: MasterBarang, nourut: number, tglKirim: Date, qty: number, tglPenawaran: Date, noPenawaran: any, supplyerName: string, supplyerPrice: number, valuta: string, catatanItem: string, peruntukan: string): void {
        const data = new PurchaseOrder();
        data.idPurchaseOrder = this.selectedshipmentPackageId;
        data.noUrut = nourut;
        data.codeProduk = res.kd_barang;
        data.produkName = res.nama_barang;
        data.supplyerName = supplyerName;
        data.pic = res.pic;
        data.qty = qty;
        data.unit = res.satuan;
        data.dtOffer = tglPenawaran;
        data.dtSent = tglKirim;
        data.offerNumber = noPenawaran;
        data.supplyerPrice = supplyerPrice;
        data.necessity = peruntukan;
        data.dtNecessity = this.inputDtNecessity;
        data.remainingStock = catatanItem;
        data.remainingPo = '';
        data.mataUang = valuta;
        if (data.qty == null || data.qty <= 0) {
            this.toaster.showToaster('info', 'Save', `Please Insert Qty! Pada Item No Urut ${nourut}`);
        } else if (data.codeProduk == null || data.produkName == null) {
            this.toaster.showToaster('info', 'Save', `Please Insert Item! Pada Item No Urut ${nourut}`);
        } else {
            this.unitChecks = [...this.unitChecks, data];
        }
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    clearBack() {
        this.activeModal.dismiss('cancel');
        this.previousState();
    }

    clearDataInput(): void {
        this.picBarang = null;
        this.noUrut = 0;
        this.selectedKdBarang = null;
        this.inputCodeName = null;
        this.inputProdukName = null;
        this.inputQty = null;
        this.inputDtOffer = null;
        this.inputSupplyerName = null;
        this.inputSupplyerPrice = null;
        this.inputNecessity = null;
        this.inputRemainingStock = null;
        this.inputRemainingPo = null;
        this.selectedBarang = null;
        this.selectedSupplier = null;
        this.newSuplier = null;
        this.newBarang = null;
        this.isSelectSupplier = false;
        this.inputOfferNumber = null;
        this.inputSatHarga = null;
        this.inputSaldo = 0;
        this.maxSaldo = 0;
        this.saldoPO = 0;
        this.saldoPP = 0;
        this.inputSatuan = null;
        this.inputDtRequest = this.inputDtNecessity;
    }

    protected clearForm(): void {
        this.clearDataInput();
        this.selectedSLNumber = null;
        this.selectedshipmentPackageId = null;
        this.index = null;
    }

    addDataBtnDisable(): boolean {
        if ((this.selectedKdBarang !== '' &&
            this.selectedKdBarang !== undefined &&
            this.selectedKdBarang != null)) {
            return false;
        } else if ((this.newBarang !== '' &&
            this.newBarang !== undefined &&
            this.newBarang != null)) {
            return false;
        } else { return true; }
    }

    removeData(index: number): void {
        this.unitChecksCopy = Object.assign([], this.unitChecks);
        this.unitChecksCopy.splice(index, 1);
        this.unitChecks = new Array<PurchaseOrder>();
        this.unitChecks = this.unitChecksCopy;
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    load() {
        // this.filterBarangSingle(event);
        // this.filterKdBarangSingle(event);

    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['purchase-order', { page: this.paramPage }]);
        }
    }

    save() {
        if (this.unitChecks.length < 1) {
            this.toaster.showToaster('info', 'Barang Kosong', 'Barang Tidak Boleh Kosong');
        } else {
            const obj = {
                noPurchaseOrder: this.inputPurchaseNumber,
                dtOrder: this.dtOrder,
                divition: this.principal.getIdInternal(),
                tipe: this.inputTipe,
                sifat: this.inputSifat,
                needs: this.inputNeeds,
                ket: this.inputKet,
                appPembelian: this.inputAppPembelian,
                appPabrik: this.inputAppPabrik,
                appDept: this.inputAppDept,
                createdBy: this.principal.getUserLogin(),
                dtNecessity: this.inputDtNecessity,
                content: this.purchaseOrder.content,
                contentContentType: this.purchaseOrder.contentContentType,
                diusulkan: this.inputDiusulkan,
                app1: this.inputApp1,
                app2: this.inputApp2,
                appasmen: this.inputAppAssmen,
                data: this.unitChecks
            }
            if (this.dtOrder == null) {
                this.toaster.showToaster('info', 'Save', 'Please Select Date order');
            } else if (this.unitChecks.length < 1) {
                this.toaster.showToaster('info', 'Save', 'Please Insert Detail');
            } else if (obj.appDept == null) {
                this.toaster.showToaster('info', 'Save', 'Please Insert Diusulkan');
            } else if (obj.appPabrik == null) {
                this.toaster.showToaster('info', 'Save', 'Please Insert Kepala Bagian');
            } else if (this.sizePdf > this.maxFileSize) {
                alert('File Attachment Melebihi 10MB');
            } else {
                this.loadingService.loadingStart();
                this.purchaseOrderService.createNew(obj).subscribe(
                    (res: ResponseWrapper) =>
                        this.onSaveSuccess(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                )
            }
        }

    }

    // protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
    //     result.subscribe((res: PurchaseOrder) =>
    //         this.onSaveSuccess(res, headers), (res: Response) => this.onSaveError(res));
    // }

    print(id: any) {

        const user = this.principal.getUserLogin();

        const filter_data = 'idPruchaseOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'purchaseOrder saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Purchase Requisition error', error.message);
        alert('Purchase Requisition error.')
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    onFileSelected(event: any) {
        this.selectedFile = event.target.files[0];
    }

}
