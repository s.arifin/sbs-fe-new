import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { JobordersService, Joborders, CustomJobOrders } from '../../joborders'
import { Subscription } from 'rxjs';
import { PurchaseOrder, PurchaseOrderService } from '../../purchase-order'
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { Purchasing, PurchasingService } from '../../purchasing';
import { MasterBarang, MasterBahan } from '../purchase-order.model';

@Component({
    selector: 'jhi-tab-view-pp',
    templateUrl: './tab-view-pp.component.html'
})
export class TabViewPPComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    currentAccount: any;
    @Input() idOrder: any;
    @Input() idStatusType: number;
    @Input() idJobOrder: any;
    public jobOrders: Array<PurchaseOrder>;
    public jobOrder: Joborders;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selectedJob: Joborders;
    selected: PurchaseOrder[];
    appJobOrders: Joborders[];
    newModalNote: boolean;
    newModalNoteAtDetail: boolean;
    pp: PurchaseOrder;
    noteNotAppPP: string;
    currentInternal: string;
    purchasings: MasterBahan[];
    modalLihatPO: boolean;
    idDialogPP: any = [];

    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrderService: PurchaseOrderService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected purchasingService: PurchasingService,
        protected reportUtilService: ReportUtilService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.newModalNote = false;
        this.newModalNoteAtDetail = false;

        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.purchasings = new Array<Purchasing>();
        this.modalLihatPO = false;
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    previousState() {
        window.history.back();
    }
    print(id: any, raw: PurchaseOrder) {

        const user = this.principal.getUserLogin();

        const filter_data = 'idPruchaseOrder:' + id;
        if (raw.divition.includes('SKG')) {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderSKG/pdf', { filterData: filter_data });
        } else if (raw.divition.includes('SMK')) {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderSMK/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
        }
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }

    loadData(): void {
        this.loadingService.loadingStart();
        this.jobOrderService.getDataByStatus({
            page: this.page - 1,
            idStatustype: this.idStatusType,
            isApp: 11,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/purchase-order/approval', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }
    srch() {
        if (this.currentSearch) {
            console.log('current : ', this.currentSearch)
            this.jobOrderService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                stat: 13,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onErrorSearch(res.json)
            );
            return;
        }
    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    // clear() {
    //     this.page = 0;
    //     this.currentSearch = '';
    //     this.router.navigate(['./', {
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadData();
    // }

    public trackId(index: number, item: CustomUnitDocumentMessage): string {
        return item.idRequirement;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.jobOrders = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Not Approved??',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 12,
            ket: this.noteNotAppPP,
            data: this.selected
        }
        this.jobOrderService.changeStatusapp(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessNotAvailable(data, headers) {
        this.eventManager.broadcast({ name: 'approvalPPListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Purchase Requisition Approved !');
        this.idDialogPP = null;
        this.modalLihatPO = false;
        this.newModalNote = false;
        this.newModalNoteAtDetail = false;
        this.noteNotAppPP = null;
        this.selected = new Array<PurchaseOrder>();
        this.loadData();
        // console.log('ini data internal', data);
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved??',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.selected.forEach(
                    (e) => e.content = null
                );
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
        console.log('isi selected', this.selected);
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            data: this.selected
        }
        this.jobOrderService.changeStatusapp(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessApproved(data, headers) {
        this.eventManager.broadcast({ name: 'approvalPPListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'PP Approved !');
        this.idDialogPP = null;
        this.newModalNote = false;
        this.modalLihatPO = false;
        this.newModalNoteAtDetail = false;
        this.selected = new Array<PurchaseOrder>();
        this.loadData();
        // console.log('ini data internal', data);
    }

    download(idpp: any) {
        this.jobOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blob = new Blob([byteArray], { 'type': contenttype });

                if (navigator.msSaveBlob) {
                    const filename = contenttype;
                    navigator.msSaveBlob(blob, filename);
                } else {
                    const link = document.createElement('a');

                    link.href = URL.createObjectURL(blob);

                    link.setAttribute('visibility', 'hidden');
                    link.download = contenttype;

                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }

        });

    }

    openPDF(idpp: any) {
        this.jobOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }
    modalUnregPP() {
        this.pp.ket = this.noteNotAppPP;
        this.jobOrderService.unAppPP(this.pp).subscribe(
            (res) => this.processToNotAvailable()
        );
    }

    unregPP() {
        this.newModalNote = true;
    }

    lihatPO(idpp: any) {
        this.jobOrderService.getDetailStock({
            idPurchaseOrder: idpp
        }).subscribe(
            (res: ResponseWrapper) => {
                this.purchasings = res.json;
                this.modalLihatPO = true
                this.idDialogPP = idpp;
                console.log('ini iddialog = ', this.idDialogPP);
            })
    }
    createRequestAtDialogDetail() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.ApprovedAtDialogDetail();
                this.loadingService.loadingStart();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
        console.log('isi selected', this.selected);
    }
    ApprovedAtDialogDetail() {
        const obj = {
            // idinternal: this.principal.getIdInternal(),
            // createdBy: this.principal.getUserLogin(),
            // idStatusType: 11,
            // data: this.idDialogPP
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            ket: this.noteNotAppPP,
            idPurchaseOrder: this.idDialogPP
        }
        this.jobOrderService.changeStatusappDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }
    modalUnregPPAtDetail() {
        this.pp.ket = this.noteNotAppPP;
        this.jobOrderService.unAppPP(this.pp).subscribe(
            (res) => this.processToNotAvailableAtDetail()
        );
    }

    unregPPAtDetail() {
        this.newModalNoteAtDetail = true;
    }

    processToNotAvailableAtDetail() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Not Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.notAvailableAtDetail();
                this.loadingService.loadingStart();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    notAvailableAtDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 12,
            ket: this.noteNotAppPP,
            idPurchaseOrder: this.idDialogPP
        }
        this.jobOrderService.changeStatusappDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }
    backapp() {
        this.idDialogPP = null;
        this.modalLihatPO = false;
        console.log('masuk backapp: ', this.idDialogPP)
    }
    backappNotApp() {
        this.noteNotAppPP = null;
        this.newModalNoteAtDetail = false;
        console.log('masuk backapp: ', this.idDialogPP)
    }
}
