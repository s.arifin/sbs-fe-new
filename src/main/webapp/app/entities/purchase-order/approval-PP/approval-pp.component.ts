// import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute, Router } from '@angular/router';
// import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../../unit-document-message';
// // import {JobordersService, Joborders, CustomJobOrders } from '../../joborders'
// import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
// import * as BaseConstant from '../../../shared/constants/base.constants';
// import * as UnitDocumentMessageConstant from '../../../shared/constants/unit-document-message.constants';
// import { PurchaseOrder, PurchaseOrderService } from '../../purchase-order'
// import { ResponseWrapper, CommonUtilService, Principal, ITEMS_PER_PAGE, ToasterService} from '../../../shared';
// // import { ConfirmationService } from 'primeng/primeng';
// import { LoadingService } from '../../../layouts';
// import { Subscription } from 'rxjs';
// import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
// import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PurchaseOrder } from '../purchase-order.model';
import { Subscription } from 'rxjs';
import { Principal, ToasterService, CommonUtilService, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { UnitDocumentMessageService } from '../../unit-document-message';
import { PurchaseOrderService } from '..';
import { LoadingService } from '../../../layouts';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { thru } from 'lodash';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { PurchasingService, Purchasing } from '../../purchasing';

@Component({
    selector: 'jhi-approval-pp',
    templateUrl: './approval-pp.component.html'
})
export class ApprovalPPComponent implements OnInit, OnDestroy {
    first: number;
    public purchaseOrders: Array<PurchaseOrder>;
    public purchaseOrder: PurchaseOrder;
    public purchaseOrderApps: Array<PurchaseOrder>;
    public purchaseOrderApp: PurchaseOrder;
    private eventSubscriber: Subscription;
    currentAccount: any;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    jobOrderApprove: any;
    jobOrderRejeced: any;
    itemsPerPage: any;
    routeData: any;
    totalItems: any;
    queryCount: any;

    selected: any = [];
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    idStatus: any;
    purchasings: any;
    modalLihatPO: boolean;
    currentInternal: string;

    constructor(
        protected principal: Principal,
        protected router: Router,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected purchaseOrderService: PurchaseOrderService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        protected alertService: JhiAlertService,
        protected purchasingService: PurchasingService,
        protected reportUtilService: ReportUtilService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.purchaseOrders = new Array<PurchaseOrder>();
        this.purchaseOrder = new PurchaseOrder();
        this.jobOrderApprove = 12;
        this.jobOrderRejeced = 11;

        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
        this.registerChangeInApprovalJobOrder();
    }

    print(id: any, raw: PurchaseOrder) {

        const user = this.principal.getUserLogin();

        const filter_data = 'idPruchaseOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }
    download(idpp: any) {
        this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blob = new Blob([byteArray], { 'type': contenttype });

                if (navigator.msSaveBlob) {
                    const filename = contenttype;
                    navigator.msSaveBlob(blob, filename);
                } else {
                    const link = document.createElement('a');

                    link.href = URL.createObjectURL(blob);

                    link.setAttribute('visibility', 'hidden');
                    link.download = contenttype;

                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }

        });

    }

    openPDF(idpp: any) {
        this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }

    loadData(): void {
        this.loadingService.loadingStart();

        this.purchaseOrderService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 12,
            isApp: 12,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.purchaseOrders = res.json;
                this.purchaseOrder = res.json;
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );

        this.purchaseOrderService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 11,
            idInternal: this.principalService.getIdInternal(),
            isApp: 21,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.purchaseOrderApps = res.json;
                this.purchaseOrderApp = res.json;
                this.onSuccess(res.json, res.headers);
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                // this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }
    srch() {
        if (this.currentSearch) {
            console.log('isi currentSearch ' + this.currentSearch);
            this.purchaseOrderService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onErrorSearch(res.json)
            );
            return;
        }
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idPurchaseOrder') {
            result.push('idPurchaseOrder');
        }
        return result;
    }
    onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.loadingService.loadingStop();
        this.totalItems = data[0].Totaldata;
        this.queryCount = this.totalItems;
        console.log('isi res app: ', this.purchaseOrderApps);
        // this.customUnitDocumentMessages = data;
    }

    onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/purchase-order/approval', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 11
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    search13(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 12;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    protected reset(): void {
        this.purchaseOrders = new Array<PurchaseOrder>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    gotoDetail(rowData) {
        this.purchaseOrderService.passingCustomData(rowData);
    }

    public submit(id: number, type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header: 'Confirmation',
                message: 'Do you want to Approveed this Purchase Requisition?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: PurchaseOrder) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderApprove,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                item: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        } else if (type === 'reject') {
            this.confirmationService.confirm({
                header: 'Confirmation',
                message: 'Do you want to reject this Purchase Requisition?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: PurchaseOrder) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderRejeced,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                items: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalJobOrder() {
        this.eventSubscriber = this.eventManager.subscribe('approvalPPListModification', (response) => this.loadData());
        console.log('masuk pak eko +++');
    }
    lihatPO(idpp: any) {
        this.purchasingService.getPObyIdPP({
            idPurchaseOrder: idpp
        }).subscribe(
            (res: ResponseWrapper) => {
                this.purchasings = res.json;
                console.log('ini data PP to PO = ', this.purchasings);
                this.modalLihatPO = true
            })
    }
    printPO(purchasing: Purchasing) {
        const filter_data = 'idpurchaseorder:' + purchasing.idpurchasing;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHarga/pdf', { filterData: filter_data })
    }

    cancelApp(id: any, rowData: PurchaseOrder) {
        this.confirmationService.confirm({
            header: 'Confirmation',
            message: 'Apakah anda yakin ingin membatalkan PP yang sudah diapprove kembali ke approval anda ?',
            accept: () => {
                this.loadingService.loadingStart();
                this.purchaseOrderService.getCancelPP(rowData, id).subscribe(
                    (res: ResponseWrapper) => {
                        this.toasterService.showToaster('info', 'Data Proces', 'Approved PP Batal');
                        this.loadData();
                    })
                this.loadingService.loadingStop();

            }
        })
    }
    // private onSuccess(data, headers) {
    //     // this.links = this.parseLinks.parse(headers.get('link'));
    //     // this.totalItems = headers.get('X-Total-Count');
    //     // this.queryCount = this.totalItems;
    //     this.loadingService.loadingStop();
    //     // this.page = pagingParams.page;
    //     this.customUnitDocumentMessages = data;
    //     console.log('idreq di sales note ==', this.customUnitDocumentMessages[0].idreq)
    // }
}
