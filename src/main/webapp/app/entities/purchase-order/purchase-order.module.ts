import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule, JhiLanguageHelper } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import {
    PurchaseOrderService,
    PurchaseOrderComponent,
    PurchaseOrderDialogComponent,
    PurchaseOrderPopupComponent,
    purchaseOrderRoute,
    purchaseOrderPopupRoute,
    PurchaseOrderResolvePagingParams,
    PurchaseOrderEditComponent,
    ApprovalPPComponent,
    TabViewPPComponent,
    PurchaseOrderEditAppLintasComponent,
    TabViewPPLintasComponent,
    ApprovalPPLintasComponent
    // BudgetingItComponent
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextareaModule,
         PaginatorModule,
         ConfirmDialogModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         TabViewModule,
         LightboxModule,
         TooltipModule
} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmPurchaseOrderItemModule } from '../purchase-order-item/purchase-order-item.module';
import { PurchaseOrderItemService } from '../purchase-order-item/index';
import { PurchaseOrderNewComponent } from './purchase-order-new.component';
import { PurchaseOrderEditAppComponent } from './approval-PP/purchase-order-edit-app.component';
import { PurchaseOrderNewImportComponent } from './purchase-order-new-import.component';

const ENTITY_STATES = [
    ...purchaseOrderRoute,
    ...purchaseOrderPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        TooltipModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        TabViewModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        SliderModule,
        RadioButtonModule,
        MpmPurchaseOrderItemModule
    ],
    exports: [
        PurchaseOrderComponent,
        PurchaseOrderEditAppComponent,
        PurchaseOrderEditComponent,
        TabViewPPComponent,
        PurchaseOrderEditAppLintasComponent,
        TabViewPPLintasComponent
        // BudgetingItComponent
    ],
    declarations: [
        ApprovalPPComponent,
        PurchaseOrderComponent,
        PurchaseOrderDialogComponent,
        PurchaseOrderPopupComponent,
        PurchaseOrderEditComponent,
        PurchaseOrderNewComponent,
        PurchaseOrderEditAppComponent,
        TabViewPPComponent,
        ApprovalPPLintasComponent,
        TabViewPPLintasComponent,
        PurchaseOrderEditAppLintasComponent,
        PurchaseOrderNewImportComponent
        // BudgetingItComponent
    ],
    entryComponents: [
        ApprovalPPComponent,
        PurchaseOrderComponent,
        PurchaseOrderDialogComponent,
        PurchaseOrderPopupComponent,
        PurchaseOrderEditComponent,
        PurchaseOrderNewComponent,
        PurchaseOrderEditAppComponent,
        TabViewPPComponent,
        ApprovalPPLintasComponent,
        TabViewPPLintasComponent,
        PurchaseOrderEditAppLintasComponent,
        PurchaseOrderNewImportComponent
        // BudgetingItComponent
    ],
    providers: [
        ApprovalPPComponent,
        ApprovalPPLintasComponent,
        PurchaseOrderItemService,
        PurchaseOrderService,
        PurchaseOrderResolvePagingParams,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPurchaseOrderModule {
    constructor(protected languageService: JhiLanguageService, protected languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
    }
}
