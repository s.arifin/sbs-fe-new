import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { PurchaseOrder } from './purchase-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Header } from 'primeng/primeng';
import { MasterBahan } from './index';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { createRequestOptionPurchasingPTO } from '../purchasing/purchasing-parameter.util';
import { PurchaseOrderItem } from '../purchase-order-item';

@Injectable()
export class PurchaseOrderService {
    protected itemValues: PurchaseOrder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/PurcahseOrders';
    protected resourceSearchUrl = process.env.API_C_URL + 'api/_search/purchase-orders';
    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    getDetailStock(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-detail-stock', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllProj(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/GetProject', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getAllLTS(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautorityLTS', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getAllSatuan(req?: any): Observable<MasterBahan> {
            return this.http.get(`${this.resourceUrl}/satuanPPIC/${req}`).map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }
    getAllKBG(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautorityKBG', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllASSMGR(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautorityASSMGR', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getAllMGR(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautorityMGR', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getAllUSL(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautorityUSL', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatusapp(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatus'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    changeStatusappDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusappDetailKomen(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    changeReqAPPROVE(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeReqAPPROVE'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    changeStatusAppLintas(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusApp'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    changeStatusAppLintasDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusAppDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    public getDataByApprovalStatusAndRequirement(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/byStatus', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
        // return res.json();
        // });
    }

    public getDataByStatus(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-status', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
        // return res.json();
        // });
    }

    getCancelPP(pay: PurchaseOrder, id: number): Observable<PurchaseOrder> {
        const copy = this.convert(pay);
        return this.http.post(this.resourceUrl + '/set-status-cancel/' + id, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    public getDataByStatusApprove(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-status-approve', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
        // return res.json();
        // });
    }
    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    getAllSupplier(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallsupplier', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getNewSupplier() {
        return this.http.get(this.resourceUrl + '/getallsupplier').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getNewBarangTeknik() {
        return this.http.get(this.resourceUrl + '/getalldropdownteknik').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getNewBarang() {
        return this.http.get(this.resourceUrl + '/getalldropdown').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getNewKdBarang() {
        return this.http.get(this.resourceUrl + '/getalldropdownKdBarang').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getAllDropdownTeknik(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getalldropdownteknik', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllDropdown(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getalldropdown', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findBarangTeknik(id: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/bahanTeknik/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getLocal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getLocal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findBarang(id: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/bahan/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    execute(params?: any, req?: any): Observable<PurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, options).map((res: Response) => {
            return res.json();
        });
    }
    unAppPP(purcaseOrder: PurchaseOrder): Observable<PurchaseOrder> {
        const copy = this.convert(purcaseOrder);
        return this.http.post(this.resourceUrl + '/NoteUnApp-pp', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    create(purchaseOrder: PurchaseOrder): Observable<PurchaseOrder> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(`${this.resourceUrl}/create`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    createNew(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    getPP1(idpurchaseorder: any): Observable<PurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/find/${idpurchaseorder}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getPP2(idpurchaseorder: any): Observable<PurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/find_attach/${idpurchaseorder}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // update(purchaseOrder: any): Observable<ResponseWrapper> {
    //     const copy = JSON.stringify(purchaseOrder);
    //     const options: BaseRequestOptions = new BaseRequestOptions();
    //     options.headers = new Headers;
    //     options.headers.append('Content-Type', 'application/json');
    //     return this.http.post(this.resourceUrl + '/update', copy,options).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    update(purchaseOrder: PurchaseOrder): Observable<PurchaseOrder> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(this.resourceUrl + '/update', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<PurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    findItem(id: any): Observable<PurchaseOrderItem> {
        return this.http.get(`${this.resourceUrl}/${id}/findItem`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(purchaseOrder: PurchaseOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('loggSearch', options);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/filter1', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter2(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/filter2', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<PurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<PurchaseOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PurchaseOrder.
     */
    protected convertItemFromServer(json: any): PurchaseOrder {
        const entity: PurchaseOrder = Object.assign(new PurchaseOrder(), json);
        return entity;
    }

    /**
     * Convert a PurchaseOrder to a JSON which can be sent to the server.
     */
    protected convert(purchaseOrder: PurchaseOrder): PurchaseOrder {
        if (purchaseOrder === null || purchaseOrder === {}) {
            return {};
        }
        // const copy: PurchaseOrder = Object.assign({}, purchaseOrder);
        const copy: PurchaseOrder = JSON.parse(JSON.stringify(purchaseOrder));
        return copy;
    }

    protected convertList(purchaseOrders: PurchaseOrder[]): PurchaseOrder[] {
        const copy: PurchaseOrder[] = purchaseOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PurchaseOrder[]) {
        this.itemValues = new Array<PurchaseOrder>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    queryLOV(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov', options)
            .map((res: Response) => this.convertResponse(res));
    }
}
