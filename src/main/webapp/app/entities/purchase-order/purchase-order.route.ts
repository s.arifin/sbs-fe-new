import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PurchaseOrderComponent } from './purchase-order.component';
import { PurchaseOrderEditComponent } from './purchase-order-edit.component';
import { PurchaseOrderPopupComponent } from './purchase-order-dialog.component';
import { PurchaseOrderNewComponent } from './purchase-order-new.component';
// import { BudgetingItComponent } from './it-budgeting.component';
import { ApprovalPPComponent } from './approval-pp/approval-pp.component'
import { ApprovalPPLintasComponent } from './approval-pp-lintas-dept/approval-pp-lintas.component';
import { PurchaseOrderEditAppComponent } from './approval-PP/purchase-order-edit-app.component';
import { PurchaseOrderEditAppLintasComponent } from './approval-pp-lintas-dept/purchase-order-edit-app-lintas.component';
import { PurchaseOrderItemComponent } from '../purchase-order-item';
import { PurchaseOrderNewImportComponent } from './purchase-order-new-import.component';
// import { ApprovalPPLintasComponent } from '.';
// import { ApprovalPPLintasComponent } from './approval-pp-lintas-dept/approval-pp-lintas.component'

@Injectable()
export class PurchaseOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'orderNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const purchaseOrderRoute: Routes = [
    {
        path: '',
        component: PurchaseOrderComponent,
        resolve: {
            'pagingParams': PurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'purchase-order-item',
        component: PurchaseOrderItemComponent,
        resolve: {
            'pagingParams': PurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: '/approve/:route/:page/:id',
    //     component: PurchaseOrderEditAppComponent,
    //     resolve: {
    //         'pagingParams': PurchaseOrderResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.approvalKacabRequest.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    //  {
    //     path: 'purchase-orders',
    //     component: BudgetingItComponent,
    //     resolve: {
    //         'pagingParams': PurchaseOrderResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchaseOrder.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: 'new-purchase-order',
        component: PurchaseOrderNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },

];

export const purchaseOrderPopupRoute: Routes = [
    {
        path: 'popup-new',
        component: PurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'create-new',
        component: PurchaseOrderNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'create-new-import',
        component: PurchaseOrderNewImportComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'lintas',
        component: ApprovalPPLintasComponent,
        resolve: {
            'pagingParams': PurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.approve'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval',
        component: ApprovalPPComponent,
        resolve: {
            'pagingParams': PurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.approve'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: ':route/:page/:id/approve',
    //     component: PurchaseOrderEditAppLintasComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchaseOrder.home.approve'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: ':route/:page/:id/edit',
        component: PurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/popup-edit',
        component: PurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
