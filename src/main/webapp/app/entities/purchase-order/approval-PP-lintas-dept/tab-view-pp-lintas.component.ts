import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';

import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { JobordersService, Joborders, CustomJobOrders } from '../../joborders'
import { Subscription } from 'rxjs';
import { PurchaseOrder, PurchaseOrderService } from '..'
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { Principal, ITEMS_PER_PAGE, ToasterService, CommonUtilService, ResponseWrapper } from '../../../shared';
import { MasterBahan } from '../../master_bahan';
import { count } from 'console';

@Component({
    selector: 'jhi-tab-view-pp-lintas',
    templateUrl: './tab-view-pp-lintas.component.html'
})
export class TabViewPPLintasComponent implements OnInit, OnDestroy {

    // protected subscription: Subscription;
    currentAccount: any;
    @Input() idOrder: any;
    @Input() idStatusType: number;
    @Input() idJobOrder: any;
    public jobOrders: Array<PurchaseOrder>;
    public jobOrder: Joborders;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selectedJob: Joborders;
    selected: PurchaseOrder[];
    appJobOrders: Joborders[];
    newModalNote: boolean;
    pp: PurchaseOrder;
    noteNotAppPP: string;
    purchasings: MasterBahan[];
    modalLihatPO: boolean;
    idDialogPP: any;
    noteKomenPP: string;
    newModalNoteAtDetail: boolean;
    newModalKomen: boolean;
    newModalKomen1: boolean
    username: string;

    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrderService: PurchaseOrderService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.newModalKomen = false;
        this.newModalKomen1 = false;

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    previousState() {
        window.history.back();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }
    print(id: any, raw: PurchaseOrder) {

        const user = this.principal.getUserLogin();
        const filter_data = 'idPruchaseOrder:' + id;

        if (raw.divition.includes('SKG')) {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderSKG/pdf', { filterData: filter_data });
        } else if (raw.divition.includes('SMK')) {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderSMK/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
        }
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    loadData(): void {
        this.loadingService.loadingStart();
        this.jobOrderService.getDataByStatusApprove({
            page: this.page - 1,
            idStatustype: 10,
            isApp: 11,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrders = res.json;
                this.jobOrder = res.json;
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
        this.username = this.principal.getUserLogin();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/purchase-order/lintas', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order/lintas', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }
    srch() {
        if (this.currentSearch) {
            console.log('current : ', this.currentSearch)
            this.jobOrderService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                stat: 13,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onErrorSearch(res.json)
            );
            return;
        }
    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    // clear() {
    //     this.page = 0;
    //     this.currentSearch = '';
    //     this.router.navigate(['./', {
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadData();
    // }

    public trackId(index: number, item: CustomUnitDocumentMessage): string {
        return item.idRequirement;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
        // this.customUnitDocumentMessages = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Not Approved',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 12,
            ket: this.noteNotAppPP,
            data: this.selected
        }
        this.jobOrderService.changeStatusapp(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessNotAvailable(data, headers) {
        this.eventManager.broadcast({ name: 'approvalPPListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Purchase Requisition Approved !');
        this.idDialogPP = null;
        this.newModalNote = false;
        this.modalLihatPO = false;
        this.newModalKomen = false;
        this.noteKomenPP = null;
        this.newModalNoteAtDetail = false
        this.totalItems = data[0].Totaldata;
        this.queryCount = this.totalItems;
        this.appJobOrders = data;
        this.selected = new Array<PurchaseOrder>();
        this.loadData();
        this.loadingService.loadingStop();
        this.router.navigate(['/purchase-order/lintas'])
        // console.log('ini data internal', data);
    }

    createRequest() {

        if (this.selected.length > 1) {
            this.confirmationService.confirm({
                message: 'Are you Sure that you want to Approved?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.Approved();
                }
            });
        } else {
            this.CreateKomen1();
        }
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            data: this.selected
        }
        this.jobOrderService.changeStatusAppLintas(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessApproved(data, headers) {
        console.log('masuk on sukes approve');
        this.eventManager.broadcast({ name: 'approvalPPListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'PP Approved !');
        this.idDialogPP = null;
        this.newModalNote = false;
        this.modalLihatPO = false;
        this.noteKomenPP = null;
        this.newModalKomen = false;
        this.newModalNoteAtDetail = false
        this.totalItems = data[0].Totaldata;
        this.queryCount = this.totalItems;
        this.appJobOrders = data;
        this.selected = new Array<PurchaseOrder>();
        this.loadData();
        this.loadingService.loadingStop();
        this.router.navigate(['/purchase-order/lintas'])

        // console.log('ini data internal', data);
    }

    download(idpp: any) {
        this.jobOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blob = new Blob([byteArray], { 'type': contenttype });

                if (navigator.msSaveBlob) {
                    const filename = contenttype;
                    navigator.msSaveBlob(blob, filename);
                } else {
                    const link = document.createElement('a');

                    link.href = URL.createObjectURL(blob);

                    link.setAttribute('visibility', 'hidden');
                    link.download = contenttype;

                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }

        });
    }

    openPDF(idpp: any) {
        this.jobOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }
    modalUnregPP() {
        this.pp.ket = this.noteNotAppPP;
        this.jobOrderService.unAppPP(this.pp).subscribe(
            (res) => this.processToNotAvailable()
        );
    }
    // unregPP(pp: PurchaseOrder) {
    //     this.pp = pp;
    //     this.newModalNote = true;
    // }
    // note ini kena AOT kalau butuh di benerin beerin aja sat
    unregPP() {
        this.newModalNote = true;
    }
    lihatPO(idpp: any) {
        this.jobOrderService.getDetailStock({
            idPurchaseOrder: idpp
        }).subscribe(
            (res: ResponseWrapper) => {
                this.purchasings = res.json;
                this.modalLihatPO = true
                this.idDialogPP = idpp;
                console.log('ini iddialog = ', this.idDialogPP);
            })
    }
    createRequestAtDialogDetail() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.ApprovedAtDialogDetail();
                this.loadingService.loadingStart();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
        console.log('isi selected', this.selected);
    }
    ApprovedAtDialogDetail() {
        const obj = {
            // idinternal: this.principal.getIdInternal(),
            // createdBy: this.principal.getUserLogin(),
            // idStatusType: 11,
            // data: this.idDialogPP
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            ket: this.noteNotAppPP,
            idPurchaseOrder: this.idDialogPP
        }
        this.jobOrderService.changeStatusAppLintasDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }
    modalUnregPPAtDetail() {
        this.pp.ket = this.noteNotAppPP;
        this.jobOrderService.unAppPP(this.pp).subscribe(
            (res) => this.processToNotAvailableAtDetail()
        );
    }

    unregPPAtDetail() {
        this.newModalNoteAtDetail = true;
    }

    processToNotAvailableAtDetail() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Not Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.notAvailableAtDetail();
                this.loadingService.loadingStart();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    notAvailableAtDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 12,
            ket: this.noteNotAppPP,
            idPurchaseOrder: this.idDialogPP
        }
        this.jobOrderService.changeStatusappDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }
    backapp() {
        this.idDialogPP = null;
        this.modalLihatPO = false;
        console.log('masuk backapp: ', this.idDialogPP)
    }
    backappNotApp() {
        this.noteNotAppPP = null;
        this.newModalNoteAtDetail = false;
        console.log('masuk backapp: ', this.idDialogPP)
    }
    CreateKomen() {
        this.newModalKomen = true;
    }

    CreateKomen1() {
        this.newModalKomen1 = true;
    }

    processToAvailableAtDetailKomen() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.AvailableAtDetailKomen();
                this.loadingService.loadingStart();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    processToAvailableAtKomen() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.AvailableAtKomen();
                this.loadingService.loadingStart();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    AvailableAtDetailKomen() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            komen: this.noteKomenPP,
            idPurchaseOrder: this.idDialogPP
        }
        this.jobOrderService.changeStatusappDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    AvailableAtKomen() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            komen: this.noteKomenPP,
            idPurchaseOrder: this.selected[0].idPurchaseOrder
        }
        this.jobOrderService.changeStatusappDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', this.selected[0].idPurchaseOrder);
    }
}
