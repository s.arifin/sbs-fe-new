
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../../unit-document-message';
// import {JobordersService, Joborders, CustomJobOrders } from '../../joborders'\
import * as BaseConstant from '../../../shared/constants/base.constants';
import * as UnitDocumentMessageConstant from '../../../shared/constants/unit-document-message.constants';
import { PurchaseOrder, PurchaseOrderService } from '..'
import { ResponseWrapper, CommonUtilService, Principal, ITEMS_PER_PAGE, ToasterService} from '../../../shared';
// import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-approval-pp-lintas',
    templateUrl: './approval-pp-lintas.component.html'
})
export class ApprovalPPLintasComponent implements OnInit, OnDestroy {
    first: number;
    public purchaseOrders: Array<PurchaseOrder>;
    public purchaseOrder: PurchaseOrder;
    public purchaseOrderApps: Array<PurchaseOrder>;
    public purchaseOrderApp: PurchaseOrder;
    private eventSubscriber: Subscription;
    currentAccount: any;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    jobOrderApprove: any;
    jobOrderRejeced: any;
    itemsPerPage: any;
    routeData: any;
    totalItems: any;
    queryCount: any;

    selected: any = [];
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    idStatus: any;

    constructor(
        protected principal: Principal,
        protected router: Router,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected purchaseOrderService: PurchaseOrderService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        protected alertService: JhiAlertService,
        protected reportUtilService: ReportUtilService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.purchaseOrders = new Array<PurchaseOrder>();
        this.purchaseOrder = new PurchaseOrder();
        this.jobOrderApprove = 12;
        this.jobOrderRejeced = 11;

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }
    print(id: any, raw: PurchaseOrder) {

        const user = this.principal.getUserLogin();
        const filter_data = 'idPruchaseOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
       // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], {'type': fieldContentType});

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    openPDF(content: any, fieldContentType) {
        const byteCharacters = atob(content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], {'type': fieldContentType});
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const fileURL = URL.createObjectURL(blobContent);
        // window.open(fileURL, '_blank');
        const win = window.open(content);
        win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
    }
    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
        this.registerChangeInApprovalJobOrder();
    }

    loadData(): void {
        this.loadingService.loadingStart();

        this.purchaseOrderService.getDataByStatusApprove({
            page: this.page - 1,
            idStatustype: 12,
            isApp: 12,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.purchaseOrders = res.json;
                this.purchaseOrder = res.json;
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );

        this.purchaseOrderService.getDataByStatusApprove({
            page: this.page - 1,
            idStatustype: 21,
            isApp: 21,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => {
                this.purchaseOrderApps = res.json;
                this.purchaseOrderApp = res.json;
                this.onSuccess(res.json, res.headers);
                // this.totalItems = res.headers.get('X-Total-Count');
                // console.log(this.totalItems);
                // this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }
    srch() {
        if (this.currentSearch) {
            console.log('isi currentSearch ' + this.currentSearch);
            this.purchaseOrderService.search({
                internals: 'idInternal:' + this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            return;
        }
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idPurchaseOrder') {
            result.push('idPurchaseOrder');
        }
        return result;
    }
    onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
         this.totalItems = headers.get('X-Total-Count');
         this.queryCount = this.totalItems;
         this.loadingService.loadingStop();
         console.log('isi res app: ', this.purchaseOrderApps);
         // this.customUnitDocumentMessages = data;
     }

    onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/purchase-order/lintas', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 11
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order/lintas', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    search13(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 12;
        this.currentSearch = query;
        this.router.navigate(['/purchase-order/lintas', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    protected reset(): void {
        this.purchaseOrders = new Array<PurchaseOrder>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    gotoDetail(rowData)  {
        console.log('datadriverdetail', );
        this.purchaseOrderService.passingCustomData(rowData);
    }

    public submit(id: number, type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header : 'Confirmation',
                message : 'Do you want to Approveed this Purchase Requisition?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: PurchaseOrder) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderApprove,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                item: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        } else if (type === 'reject') {
            this.confirmationService.confirm({
                header : 'Confirmation',
                message : 'Do you want to reject this Purchase Requisition?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: PurchaseOrder) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderRejeced,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                items: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody , objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalJobOrder() {
        this.eventSubscriber = this.eventManager.subscribe('approvalPPListModification', (response) => this.loadData());
        console.log('masuk pak eko +++');
    }

    // private onSuccess(data, headers) {
    //     // this.links = this.parseLinks.parse(headers.get('link'));
    //     // this.totalItems = headers.get('X-Total-Count');
    //     // this.queryCount = this.totalItems;
    //     this.loadingService.loadingStop();
    //     // this.page = pagingParams.page;
    //     this.customUnitDocumentMessages = data;
    //     console.log('idreq di sales note ==', this.customUnitDocumentMessages[0].idreq)
    // }

    cancelApp(id: any, rowData: PurchaseOrder) {
        this.confirmationService.confirm({
            header: 'Confirmation',
            message: 'Apakah anda yakin ingin membatalkan PP yang sudah diapprove kembali ke approval anda ?',
            accept: () => {
                this.loadingService.loadingStart();
                this.purchaseOrderService.getCancelPP(rowData, id).subscribe(
                    (res: ResponseWrapper) => {
                        this.toasterService.showToaster('info', 'Data Proces', 'Approved PP Batal');
                        this.loadData();
                    })
                this.loadingService.loadingStop();

            }
        })
    }
}
