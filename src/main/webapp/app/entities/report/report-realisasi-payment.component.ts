import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { Purchasing, PurchasingService } from '../purchasing';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportService } from './report.service';
import { ConfirmationService, DataTable, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { SelectItem } from 'primeng/primeng';
import { Payment, PaymentReport, PaymentReportVM2, PaymentService } from '../payment';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
import * as XLSX from 'xlsx';
@Component({
    selector: 'jhi-report-realisasi-payment.component',
    templateUrl: './report-realisasi-payment.component.html'
})
export class ReportRealisasiPaymentComponent implements OnInit, OnDestroy {
    @ViewChild('table') table: DataTable;
    @Input() idstatus: number;
    currentAccount: Account;
    payments: PaymentReportVM2[];
    HpaymentReport: PaymentReport;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Payment[];
    newModalNote: boolean;
    notehold: string;
    login: string;
    payment: Payment;
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    radioValue: number;
    pembayarans: Array<object> = [
        { label: 'SEMUA', value: 0 },
        { label: 'Belum Lunas', value: 1 },
        { label: 'Sudah Lunas', value: 2 },
    ];
    cols = [
        { field: 'notandaterima', header: 'No Tanda Terima' },
        { field: 'bayarkepada', header: 'Dibayar Kepada' },
        { field: 'valuta', header: 'Mata Uang' },
        { field: 'jumlahtagihan', header: 'Jumlah Tagihan' },
        { field: 'jumlahbayar', header: 'Jumlah Bayar' },

        { field: 'ppn', header: 'Total PPN' },
        { field: 'bayappn', header: 'Jumlah Bayar PPN' },
        { field: 'pokok', header: 'Jumlah Pokok' },
        { field: 'bayarpokok', header: 'Jumlah Bayar Pokok' },

        { field: 'nogiro', header: 'Nomor Giro' },
        { field: 'pembuat', header: 'Pembuat' },
        { field: 'tanggalbuat', header: 'Tanggal Buat' },
        // { field: 'tanggalserahacc', header: 'Tanggal Serah ACC' },
        // { field: 'tanggalterimaacc', header: 'Tanggal Terima ACC' },
        // { field: 'tanggalserahfin', header: 'Tanggal Serah FIN' },
        // { field: 'tanggalterimafin', header: 'Tanggal Terima FIN' },
        { field: 'tanggalbayar', header: 'Tanggal Bayar' },
        { field: 'tanggalrelease', header: 'Tanggal Release' },
        { field: 'tanggaltempo', header: 'Tanggal Tempo' },
        // { field: 'status', header: 'Status' }
    ];
    columnOptions: SelectItem[];
    jumlahtagihan: any;
    jumlahbayar: any;
    ppn: any;
    bayarppn: any;
    pokok: any;
    bayarpokok: any;
    kurangbayar: any;
    kurangbayarppn: any;
    kurangbayarpokok: any;
    jumlahtagihanrp: any;
    jumlahbayarrp: any;
    kurangbayarrp: any;
    constructor(
        private paymentService: PaymentService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
        private confirmatinService: ConfirmationService,
        private masterSupplierService: MasterSupplierService,
        private excelService: ExcelService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Payment>();
        this.newModalNote = false;
        this.notehold = '';
        this.login = '';
        this.payment = new Payment();
        this.currentAccount = new Account();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.idstatus = 10;
        this.radioValue = 0;
        this.columnOptions = [];
        for (let i = 0; i < this.cols.length; i++) {
            const obj = { label: this.cols[i].header, value: this.cols[i] }
            this.columnOptions.push(obj);
        }
        this.HpaymentReport = new PaymentReport();
        this.jumlahtagihan = 0;
        this.jumlahbayar = 0;
        this.ppn = 0;
        this.bayarppn = 0;
        this.pokok = 0;
        this.bayarpokok = 0;
        this.kurangbayar = 0;
        this.kurangbayarppn = 0;
        this.kurangbayarpokok = 0;
        this.jumlahtagihanrp = 0;
        this.jumlahbayarrp = 0;
        this.kurangbayarrp = 0;
    }

    // loadAll() {
    //     this.paymentService.queryReport({
    //         query: 'status:' + this.idstatus + '|cari:' + this.currentSearch + '|kdsup:' + this.kdsup + '|dtfrom:' + this.dtfrom.toISOString().split('T')[0] + '|dtthru:' + this.dtthru.toISOString().split('T')[0]
    //     }).subscribe(
    //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    // }
    loadAllFilter() {
        this.loadingService.loadingStart();
        this.jumlahtagihan = 0;
        this.jumlahbayar = 0;
        this.ppn = 0;
        this.bayarppn = 0;
        this.pokok = 0;
        this.bayarpokok = 0;
        this.kurangbayar = 0;
        this.kurangbayarppn = 0;
        this.kurangbayarpokok = 0;
        this.jumlahtagihanrp = 0;
        this.jumlahbayarrp = 0;
        this.kurangbayarrp = 0;
        this.paymentService.queryReport({
            query: 'status:' + this.idstatus + '|cari:' + this.currentSearch + '|kdsup:' + this.kdsup +
                '|dtfrom:' + this.dtfrom.toISOString().split('T')[0] + '|dtthru:' + this.dtthru.toISOString().split('T')[0]
                + '|radioValue:' + this.radioValue + '|jenis:detail'
            // query: 'status:10|cari:|kdsup:UNITED CAN COMPANY PT.|dtfrom:2002-12-31|dtthru:2022-01-10|radioValue:0'
        }).subscribe(
            (res: ResponseWrapper) => {
                // this.HpaymentReport = JSON.parse(res.headers.get('dataheadervm'));
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/payment'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAllFilter();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/payment', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAllFilter();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/payment', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAllFilter();
    }
    ngOnInit() {
        // this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            console.log('currentAccount == ', this.currentAccount);
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Payment) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.payments = data;
        // this.payments.shift();
        this.payments.forEach(
            (e) => {
                this.jumlahtagihan += e.tagihan;
                console.log('this.jumlahtagihan = ', this.jumlahtagihan);
                this.jumlahbayar += e.bayar;
                console.log('this.jumlahbayar = ', this.jumlahbayar);
                this.ppn += e.tagihanppn;
                console.log('this.ppn = ', this.ppn);
                this.bayarppn += e.bayarppn;
                console.log('this.bayarppn = ', this.bayarppn);
                this.pokok += e.tagihanpokok;
                console.log('this.pokok = ', this.pokok);
                this.bayarpokok += e.bayarpokok;
                console.log('this.bayarpokok = ', this.bayarpokok);
                this.kurangbayar += e.kurang;
                console.log('this.kurangbayar = ', this.kurangbayar);
                this.kurangbayarppn += e.kurangppn;
                console.log('this.kurangbayarppn = ', this.kurangbayarppn);
                this.kurangbayarpokok += e.kurangpokok;
                console.log('this.kurangbayarpokok = ', this.kurangbayarpokok);
                this.jumlahtagihanrp += e.tagihanrp == null ? 0 : e.tagihanrp;
                console.log('this.jumlahtagihanrp = ', this.jumlahtagihanrp);
                this.jumlahbayarrp += e.bayarrp == null ? 0 : e.bayarrp;
                console.log('this.jumlahbayarrp = ', this.jumlahbayarrp);
                this.kurangbayarrp += e.kurangrp == null ? 0 : e.kurangrp;
                console.log('this.kurangbayarrp = ', this.kurangbayarrp);
            });
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllFilter();
    }
    print(data: Payment) {
        const filter_data = 'idpay:' + data.idpay;
        if (data.valuta !== 'Rp') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer_usd/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer/pdf', { filterData: filter_data });
        }
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.kd_suppnew;
    }
    filter() {
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.nmSupplier === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.kd_suppnew;
        }
        this.loadAllFilter();
    }
    reset() {
        this.loadAllFilter();
        this.kdsup = '';
    }
    exportExcel() {
        this.excelService.exportAsExcelFile(this.payments, 'report-realisasi-payment');
    }
    public export() {
        this.excelService.exportTableAsExcelFile(this.table.el.nativeElement, 'Report Realisasi Payment');
    }
}
