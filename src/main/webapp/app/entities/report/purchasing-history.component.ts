import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { ItemProduct, Purchasing, PurchasingService, PurchasingHistory } from '../purchasing';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportService } from './report.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'jhi-purchasing-history',
    templateUrl: './purchasing-history.component.html'
})
export class ReportPurchasingHistoryComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    pics: Array<object> = [
        { label: 'All', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    statuses: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Approved kepala Bagian', value: '21' },
        { label: 'Register', value: '14' },
        { label: 'Serah Management', value: '24' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Siap PO', value: '25' },
        { label: 'Sudah Di Buat PO', value: '22' },
        { label: 'Siap PO Sebagian', value: '27' },
        { label: 'Sudah PO Sebagian', value: '28' },
        { label: 'Request Approve', value: '29' },
        { label: 'Approve', value: '11' },
        { label: 'Not Approve', value: '12' },
        { label: 'Siap BBM', value: '26' }
    ];
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string;
    filteredBarang: ItemProduct[];
    newBarang: any;
    selectedKd: any;
    selectedSPL: any;
    filteredCodeSuppliers: any[];
    filteredSuppliers: any[];
    newSuplier: any;
    itemsPerPage: any;
    page: any;
    purchasings: PurchasingHistory[];
    purchasing: PurchasingHistory;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    idPrint: any;
    modalPilihCeteakn: boolean;
    pdfBlob: any;
    pilihBahasa: string;
    isTnpaHrg: boolean;
    isViewCetakan: boolean;
    columnOptions: SelectItem[];
    cols = [
        { field: 'nopurchasing', header: 'No PO' },
        { field: 'dtcreated', header: 'Tanggal PO' },
        { field: 'pic', header: 'PIC' },
        { field: 'tpbahan', header: 'Type Bahan' },
        { field: 'kd_suppnew', header: 'Kode Suplier' },
        { field: 'nm_supplier', header: 'Nama Suplier' },
        { field: 'productcode', header: 'Kode Produk' },
        { field: 'productname', header: 'Nama Produk' },
        { field: 'dtsent1', header: 'Tanggal Terima' },
        { field: 'qty', header: 'QTY' },
        { field: 'uom', header: 'Satuan' },
        { field: 'price', header: 'Harga' },
        { field: 'valuta', header: 'Valuta' },
        // {field: 'price', header: 'Jumlah'},
        { field: 'disc', header: 'Disc (%)' },
        { field: 'totaldisc', header: 'Total Disc' },
        { field: 'ppn', header: 'PPN' }
        // {field: 'totalprice', header: 'Total'}
    ];
    currentPlant: string;
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        private purchasingService: PurchasingService,
        private masterSupplierService: MasterSupplierService,
        protected reportService: ReportService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = '';
        this.selectedKd = '';
        this.selectedSPL = '';
        this.idPrint = '';
        this.modalPilihCeteakn = false;
        this.pilihBahasa = '';
        this.isTnpaHrg = false;
        this.isViewCetakan = false;
        this.columnOptions = [];
        for (let i = 0; i < this.cols.length; i++) {
            const obj = { label: this.cols[i].header, value: this.cols[i] }
            this.columnOptions.push(obj);
        }
        this.purchasings = new Array<PurchasingHistory>();
        this.idinternal = this.principal.getIdInternal();
        if (this.idinternal.includes('SKG')) {
            this.currentPlant = 'SKG';
        } else if (this.idinternal.includes('SMK')) {
            this.currentPlant = 'SMK';
        } else {
            this.currentPlant = 'SBS';
        }
    }

    loadAll() {
        if (this.isview === true) {
            this.loadingService.loadingStart();
            this.reportService.getHistPurchasing({
                page: this.page,
                size: this.itemsPerPage,
                from: this.tgl1.toJSON(),
                thru: this.tgl2.toJSON(),
                filter: 'spl:' + this.selectedSPL + '|kdbahan:' + this.selectedKd
            })
                .subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        }
    }
    ngOnInit() {
    }
    print(from: Date, thru: Date) {
        this.loadingService.loadingStart()
        this.waktu = '23:59:59';
        this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;
        if (this.newSuplier === undefined || this.newSuplier === '') {
            this.selectedSPL = ''
        } else {
            this.selectedSPL = this.newSuplier.kd_suppnew
        }
        if (this.newBarang === undefined || this.newBarang === '') {
            this.selectedKd = ''
        } else {
            this.selectedKd = this.newBarang.kdBahan;
        }
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|kdspl:' + this.selectedSPL + '|kdbrg:' + this.selectedKd + '|plant:' + this.currentPlant;
        // this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/histori_pembelian/pdf', { filterData: this.filter_data }).
        //     subscribe((response: any) => {
        //         const reader = new FileReader();
        //         reader.readAsDataURL(response.blob());
        //         this.isview = true;
        //         // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
        //         reader.onloadend = (e) => {
        //             this.pdfSrc = reader.result;
        //         };
        //         this.loadingService.loadingStop();
        //     });
        this.isview = true;
        this.loadAll();
        this.loadingService.loadingStop();
    }
    exportExcel(from: Date, thru: Date) {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;
        this.waktu = '23:59:59';
        this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;

        const currentYear = date.getFullYear();
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|kdspl:' + this.selectedSPL + '|kdbrg:' + this.selectedKd + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFileWithName(`Report History Pembelian ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/histori_pembelian/xlsx', { filterData: this.filter_data });
    }
    backMainPage() {
        this.isViewCetakan = false;
    }

    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierNew().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierNew().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.purchasings = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
    }
    public printPO(purchasing: PurchasingHistory) {
        this.idPrint = purchasing.idpurchasing;
        this.purchasing = purchasing;
        this.modalPilihCeteakn = true;
    }
    public pilihPrintPO(bahasa: any) {
        this.pilihBahasa = bahasa
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        this.pdfBlob = '';
        const filter_data = 'idpurchaseorder:' + this.purchasing.idpurchasing;
        if (bahasa === 'indonesia') {
            if (this.purchasing.nopurchasing.includes('SKG')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.nopurchasing.includes('SMK')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
        if (bahasa === 'inggris') {
            if (this.purchasing.nopurchasing.includes('SKG')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.nopurchasing.includes('SMK')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggris/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
    }
    downloadBlob() {
        FileSaver.saveAs(this.pdfBlob, this.purchasing.nopurchasing);
    }
    lihatHarga() {
        this.pilihPrintPO(this.pilihBahasa);
    }
    printtanpaharga() {
        this.isTnpaHrg = true;
        const bahasa = this.pilihBahasa;
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        const filter_data = 'idpurchaseorder:' + this.purchasing.idpurchasing;
        if (bahasa === 'indonesia') {
            if (this.purchasing.nopurchasing.includes('SMK')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHargaSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.nopurchasing.includes('SKG')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHargaSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHarga/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
        if (bahasa === 'inggris') {
            if (this.purchasing.nopurchasing.includes('SMK')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHargaSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.nopurchasing.includes('SKG')) {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHargaSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHarga/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isViewCetakan = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
    }
}
