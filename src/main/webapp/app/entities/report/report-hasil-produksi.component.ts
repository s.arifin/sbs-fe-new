import { Component, OnInit, OnDestroy } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { AccountService } from '../../shared/auth/account.service';
import { ItemProduct, PurchasingService } from '../purchasing';
import { ReportService } from './report.service';
import { JhiAlertService } from 'ng-jhipster';
import { formatDate } from 'ngx-bootstrap/chronos';
import { DashboardService } from '../dashboard';

@Component({
    selector: 'jhi-report-hasil-produksi',
    templateUrl: './report-hasil-produksi.component.html'
})
export class ReportHasilProduksiComponent implements OnInit {
    commontUtilService: any;
    internals: Internal[];
    internal: Internal;
    modalPilihCeteakn: boolean;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string
    divitions: Array<Object>;
    selectedDIV: string;
    filterDiv: boolean;
    account: Account;
    us_divisi: any;
    date: Date;
    user: any;
    rofo1: string;
    rofo2: string;
    rofo3: string;
    selectBulan: any;
    kd_produk: any;
    newBarang: any;
    selectTahun: any;
    selectedType: number;
    tahun1: Array<object> = [
        { label: 'Pilih Tahun', value: '' },
    ];
    mesin = [
        { label: 'Semua Mesin', value: 'all' },
    ];
    selectMesin: any;
    filteredBarang: ItemProduct[];
    bulans: Array<object> = [
        { label: 'Januari', value: '-01-01' },
        { label: 'Februari', value: '-02-01' },
        { label: 'Maret', value: '-03-01' },
        { label: 'April', value: '-04-01' },
        { label: 'Mei', value: '-05-01' },
        { label: 'Juni', value: '-06-01' },
        { label: 'Juli', value: '-07-01' },
        { label: 'Agustus', value: '-08-01' },
        { label: 'September', value: '-09-01' },
        { label: 'Oktober', value: '-10-01' },
        { label: 'November', value: '-11-01' },
        { label: 'Desember', value: '-12-01' },
    ];
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected userService: UserService,
        protected accountService: AccountService,
        private purchasingService: PurchasingService,
        private reportService: ReportService,
        private alertService: JhiAlertService,
        private dashboardService: DashboardService,
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.selectedType = 0;
        this.date = new Date();
        this.selectBulan = '';
        this.modalPilihCeteakn = false;
        this.selectTahun = '';
        this.mesin = [];
        this.selectMesin = '';
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.us_divisi = this.account.idInternal;
        });

        this.dashboardService.getTahun({
            page: 0,
            size: 10000,
            sort: ['approvedBy', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            res.json.forEach((element) => {
                this.tahun1.push({
                    label: element.approvedBy,
                    value: element.approvedBy
                });
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.dashboardService.getMesin({
            page: 0,
            size: 10000,
            sort: ['approvedBy', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            res.json.forEach((element) => {
                this.mesin.push({
                    label: element.approvedBy,
                    value: element.createdBy
                });
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

    }
    print() {
        if ((this.selectBulan === undefined || this.selectBulan === '' || this.selectBulan === null) || (this.selectTahun === undefined || this.selectTahun === '' || this.selectTahun === null)) {
            alert('Periode dan Tahun Harus Di Pilih');
        } else if (this.selectMesin === null || this.selectMesin === undefined || this.selectMesin === '') {
            alert('Mesin Harus Di Pilih');
        } else {
            const tgl_new = this.selectTahun + this.selectBulan;
            const tgl_awal = new Date(tgl_new);
            const tgl_akhir = new Date(tgl_awal.getFullYear(), tgl_awal.getMonth() + 1, 0)
            const firstDay = tgl_awal.getFullYear() + '-' + (tgl_awal.getMonth() + 1) + '-' + tgl_awal.getDate();
            const lastDay = tgl_akhir.getFullYear() + '-' + (tgl_akhir.getMonth() + 1) + '-' + tgl_akhir.getDate();
            this.user = this.principal.getUserLogin().toLowerCase();
            this.loadingService.loadingStart();
            this.reportService.getHasilProduksi({
                filter: 'from:' + firstDay + '|typeMesin:' + this.selectMesin
            }).subscribe(
                (res: ResponseWrapper) => {
                    const nm_mesin = this.findLabelByValue(this.mesin, this.selectMesin);
                    this.filter_data = 'dtFrom:' + firstDay + '|dtThru:' + lastDay + '|iduser:' + this.user + '|mesin:' + nm_mesin
                    this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/rpt_target_produksi_new/pdf', { filterData: this.filter_data }).
                        subscribe((response: any) => {
                            const reader = new FileReader();
                            reader.readAsDataURL(response.blob());
                            this.isview = true;
                            // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                            reader.onloadend = (e) => {
                                this.pdfSrc = reader.result;
                            };
                            this.loadingService.loadingStop();
                        })
                }
            );
        }
    }

    private onSuccess(data, headers) {
        this.loadingService.loadingStop();
    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    findLabelByValue(array, searchValue) {
        const result = array.find((item) => item.value === searchValue);
        return result ? result.label : 'Semua Mesin';
      }

    backMainPage() {
        this.isview = false;
        this.pdfSrc = '';
    }
    public printReport() {
        this.modalPilihCeteakn = true;
    }

    printExcel() {
        const nm_mesin = this.findLabelByValue(this.mesin, this.selectMesin);
        const tgl_new = this.selectTahun + this.selectBulan;
        const tgl_awal = new Date(tgl_new);
        const tgl_akhir = new Date(tgl_awal.getFullYear(), tgl_awal.getMonth() + 1, 0)
        const firstDay = tgl_awal.getFullYear() + '-' + (tgl_awal.getMonth() + 1) + '-' + tgl_awal.getDate();
        const lastDay = tgl_akhir.getFullYear() + '-' + (tgl_akhir.getMonth() + 1) + '-' + tgl_akhir.getDate();
        this.user = this.principal.getUserLogin().toLowerCase();
        this.filter_data = 'dtFrom:' + firstDay + '|dtThru:' + lastDay + '|iduser:' + this.user + '|mesin:' + nm_mesin
        this.loadingService.loadingStart()
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_target_produksi_new/xlsx', { filterData: this.filter_data });
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

    printPDF() {
        const nm_mesin = this.findLabelByValue(this.mesin, this.selectMesin);
        const tgl_new = this.selectTahun + this.selectBulan;
        const tgl_awal = new Date(tgl_new);
        const tgl_akhir = new Date(tgl_awal.getFullYear(), tgl_awal.getMonth() + 1, 0)
        const firstDay = tgl_awal.getFullYear() + '-' + (tgl_awal.getMonth() + 1) + '-' + tgl_awal.getDate();
        const lastDay = tgl_akhir.getFullYear() + '-' + (tgl_akhir.getMonth() + 1) + '-' + tgl_akhir.getDate();
        this.user = this.principal.getUserLogin().toLowerCase();
        this.filter_data = 'dtFrom:' + firstDay + '|dtThru:' + lastDay + '|iduser:' + this.user + '|mesin:' + nm_mesin
        this.loadingService.loadingStart()
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_target_produksi_new/pdf', { filterData: this.filter_data });
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

}
