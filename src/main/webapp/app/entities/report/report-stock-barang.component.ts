import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BbmPrint, ReportPenerimaanBarang, ReportStockBarang } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, DataTable, LazyLoadEvent } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
import { ItemProduct, PurchasingService } from '../purchasing';

@Component({
    selector: 'jhi-report-stock-barang',
    templateUrl: './report-stock-barang.component.html'
})

export class ReportStockBarangComponent implements OnInit {
    @ViewChild('table') table: DataTable;
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    datas: ReportStockBarang[];
    details: ReportStockBarang[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    newModalDetail: boolean;
    totalItemsDetail: any;
    queryCountDetail: any;
    dtfrom: Date;
    dtthru: Date;
    selectedKd: any;
    newBarang: any;
    selectedType: any;
    internal: any;
    nopp: string;
    nopo: string;
    satuan: string;
    qtyPP: number;
    qtyPO: number;
    nama_supplier: string;
    currentAccount: string;
    types = [
        { label: 'Semua', value: 'all' },
        { label: 'Bahan Baku', value: 'rm' },
        { label: 'Bahan Pengemas', value: 'pm' },
        { label: 'Lain-Lain', value: 'rmpm' },
        { label: 'Sparepart', value: 's' }
        // { label: 'Sparepart IDR', value: 9 },
    ];
    filteredBarang: ItemProduct[];
    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private excelService: ExcelService,
        private purchasingService: PurchasingService,
        private principal: Principal,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array<Pobbmlov>();
        this.datas = new Array<ReportPenerimaanBarang>();
        this.details = new Array<ReportPenerimaanBarang>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.selectedType = 'all';
        this.selectedKd = '';
        this.newModalDetail = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
    }
    ngOnInit(): void {
        // this.loadAll();
    }
    loadAll() {
        this.internal = this.principal.getIdInternal();
        this.currentAccount = this.principal.getUserLogin();
        const waktu = '23:59:59';
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.reportService.getStockBarang({
                page: this.page,
                size: this.itemsPerPage,
                filter: 'cari:' + this.currentSearch +
                    '|thru:' + sampai +
                    '|jenis:' + this.selectedType +
                    '|kdbahan:' + this.selectedKd
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
            return;
        }
        this.reportService.getStockBarang({
            page: this.page,
            size: this.itemsPerPage,
            filter: 'cari:' + this.currentSearch +
                '|thru:' + sampai +
                '|jenis:' + this.selectedType +
                '|kdbahan:' + this.selectedKd
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
            (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
        );
        this.selected = new Array<Pobbmlov>();
        console.log('jenis:', this.selectedType);
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.datas = data;
        this.loadingService.loadingStop();

    };
    private onSuccessDetail(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.details = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        // this.loadAll();
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.newBarang = '';
        this.selectedType = 'all';
        this.datas = undefined;
        this.dtthru = new Date();
        this.selectedKd = '';
        // this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    lihatDetail(data: ReportStockBarang) {
        if (data.nopurchasing === null || data.nopurchasing === '' || data.nopurchasing === undefined) {
            alert('Detail Tidak Bisa Dilihat Karena Belum Ada POnya');
        } else {
            const nopo = data.nopurchasing.replace(/\//g, '-');
            this.nopo = data.nopurchasing;
            this.nopp = data.nopurchaseorder;
            this.qtyPO = data.qtypo;
            this.qtyPP = data.qtypp
            this.satuan = data.satuan_pp;
            this.nama_supplier = data.nm_supplier;
            this.reportService.getStockBarangDetail(nopo, data.codeproduk)
                .subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessDetail(res.json, res.headers); this.loadingService.loadingStop();
                        this.newModalDetail = true;
                    },
                    (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
                );
        }
    }

    backapp() {
        this.nopo = '';
        this.nopp = '';
        this.qtyPO = 0;
        this.qtyPP = 0;
        this.satuan = '';
        this.nama_supplier = '';
        this.newModalDetail = false;
    }

    filter() {
        this.loadAll();
    }
    public export() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        // const filter_data = 'kdbahan:' + this.selectedKd + '|jenis:' + this.selectedType + '|divisi:' + this.internal;
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/rpt_stock/xlsx', { filterData: filter_data });
        if (this.selectedKd === '') {
            this.selectedKd = undefined;
        } else {
            this.selectedKd = this.selectedKd;
        }
        const filter_data = 'kdbahan:' + this.selectedKd + '|jenis:' + this.selectedType + '|divisi:' + this.internal + '|iduser:' + this.currentAccount;
        this.reportUtilService.downloadFileWithName(`Report Stock Barang ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_stock/xlsx', { filterData: filter_data });
    }
    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }

}
