import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';

@Component({
    selector: 'jhi-report-purchase-request',
    templateUrl: './report-purchase-request.component.html'
})
export class ReportPurchaseRequestComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{label: 'Please Select or Insert', value: null}];
    isSelcBrg: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];
    pilihTipe: any;
    idlocal: any;
    currentAccount: any;
    inputAppPabrik: string;
    public barang: MasterBahan;
    public Barang: MasterBahan[];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pic: string;
    selectedPIC: string;
    selectedStatus: string;
    status: string;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = ''
        this.barang = new MasterBahan();
        this.inputCodeName = 'ALL';
    }

    filterBarangSingle(event) {
        console.log('data Tipe barang : ', this.pilihTipe);
        const query = event.query;
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.getNewBarangTeknik().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        } else {
            this.purchaseOrderService.getNewBarang().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        }
    }

    filterBarang(query, newBarangs: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.barang.idMasBahan = this.newBarang.idMasBahan;
        console.log('idmasbahan : ' + this.newBarang.idMasBahan);
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.newBarang.idMasBahan)
            .subscribe(
                (res: MasterBarang) => {
                    console.log('ini isi bahan = ', res.kd_barang);
                    this.inputCodeName = res.kd_barang;
                    this.inputProdukName = res.nama_barang;
                    this.newKdBarang = res.kd_barang;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    this.isSelcBrg = false;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        } else {
            this.purchaseOrderService.findBarang(this.newBarang.idMasBahan)
            .subscribe(
                (res: MasterBahan) => {
                    console.log('ini isi bahan = ', res.kdBahan);
                    this.inputCodeName = res.kdBahan;
                    this.inputProdukName = res.nmBahan;
                    this.newKdBarang = res.kdBahan;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        }
    }

    ngOnInit() {
        // this.organizationService.query()
        //     .subscribe((res: ResponseWrapper) => { this.organizations = res.json});
        //     console.log('nc :', this.organization);

        // this.internalService.query({size: 999999})
        // .subscribe((res: ResponseWrapper) => {
        //     this.internals = res.json;
        //     console.log('internal :', this.internals)
        // });
        this.idinternal =  this.principal.getIdInternal();
        console.log('internal : ', this.internal)
        if (this.idinternal === 'TEKNIK') {
            this.purchaseOrderService.getAllDropdownTeknik({
                page: 0,
                size: 10000,
                sort: ['kdBahan', 'asc']
            }).subscribe((res: ResponseWrapper) => {
                this.newKdBarang = res.json;
                this.newKdBarang.forEach((element) => {
                    this.listKdBarang.push({
                        label: element.kdBahan + ' - ' + element.nmBahan,
                        value: element.idMasBahan });
                });
            },
                (res: ResponseWrapper) => {
                    this.commontUtilService.showError(res.json);
                }
            );
        } else {
            this.purchaseOrderService.getAllDropdown({
                page: 0,
                size: 10000,
                sort: ['kdBahan', 'asc']
            }).subscribe((res: ResponseWrapper) => {
                this.newKdBarang = res.json;
                this.newKdBarang.forEach((element) => {
                    this.listKdBarang.push({
                        label: element.kdBahan + ' - ' + element.nmBahan,
                        value: element.idMasBahan });
                });
            },
                (res: ResponseWrapper) => {
                    this.commontUtilService.showError(res.json);
                }
            );
        }
    }

    public selectKdBarang(isSelect?: boolean): void {
        console.log('idmasbahankd : ' + this.selectedKdBarang);
        this.kdbarang.idMasBahan = this.selectedKdBarang;
        if (this.principal.getIdInternal() === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.selectedKdBarang)
            .subscribe(
                (res: MasterBarang) => {
                    console.log('ini isi bahan = ', res.kd_barang);
                    this.inputCodeName = res.kd_barang;
                    this.inputProdukName = res.nama_barang;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        } else {
            this.purchaseOrderService.findBarang(this.selectedKdBarang)
            .subscribe(
                (res: MasterBahan) => {
                    console.log('ini isi bahan = ', res.kdBahan);
                    this.inputCodeName = res.kdBahan;
                    this.inputProdukName = res.nmBahan;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        }
    }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    print(from: Date, thru: Date, nd: any) {
        const waktu = '23:59:59.000';
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        console.log('nd all:', this.inputCodeName);
        if (this.inputCodeName === 'ALL') {
            const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|Div:' + this.idinternal;
            // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/Report_PP_PO/xlsx', { filterData: filter_data });
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/Report_PP_PO/pdf', { filterData: filter_data });
            console.log('nd all:', filter_data);
        } else {
            const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|kd_bahan:' + this.inputCodeName + '|Div:' + this.idinternal;
            // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/Report_PP_PO/xlsx', { filterData: filter_data });
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/Report_PP_PO_Non_All/pdf', { filterData: filter_data });
            console.log('nd :', filter_data);
        }
    }
}
