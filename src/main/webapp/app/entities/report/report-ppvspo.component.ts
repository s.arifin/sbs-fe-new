import { Component, OnInit, OnDestroy } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { AccountService } from '../../shared/auth/account.service';

@Component({
    selector: 'jhi-report-ppvspo',
    templateUrl: './report-ppvspo.component.html'
})
export class ReportPpVsPoComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    pics: Array<object> = [
        { label: 'All', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' },
        { label: 'SVY_SKG', value: 'SVY_SKG' },
        { label: 'SVY_SMK', value: 'SVY_SMK' },
        { label: 'JTR_SMK', value: 'JTR_SMK' },
    ];
    statuses: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Approved kepala Bagian', value: '21' },
        { label: 'Register', value: '14' },
        { label: 'Serah Management', value: '24' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Siap PO', value: '25' },
        { label: 'Sudah Di Buat PO', value: '22' },
        { label: 'Siap PO Sebagian', value: '27' },
        { label: 'Sudah PO Sebagian', value: '28' },
        { label: 'Request Approve', value: '29' },
        { label: 'Approve', value: '11' },
        { label: 'Not Approve', value: '12' },
        { label: 'Siap BBM', value: '26' }
    ];
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string
    divitions: Array<Object>;
    selectedDIV: string;
    filterDiv: boolean;
    account: Account;
    idInternal: string;
    userAccount: string;
    currentPlant: string;
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected userService: UserService,
        protected accountService: AccountService
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = '';
        this.divitions = new Array<object>();
        this.selectedDIV = 'ALL';
        this.filterDiv = false;
        this.account = new Account();
        this.idInternal = this.principal.getIdInternal();
        this.userAccount = this.principal.getUserLogin();
        if (!this.idInternal.includes('SMK') && !this.idInternal.includes('SKG')) {
            this.pics = this.pics.filter((pic) => (!pic['value'].includes('SMK') && !pic['value'].includes('SKG')) || pic['label'] === 'all');
            this.currentPlant = 'SBS';
        } else if (this.idInternal.includes('SMK')) {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SMK') || pic['label'] === 'All');
            this.currentPlant = 'SMK';
        } else {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SKG') || pic['label'] === 'All');
            this.currentPlant = 'SKG';
        }
    }

    ngOnInit() {
        this.getUserDiv();
        this.accountService.get().subscribe((res) => {
            this.account = res;
            if ((this.account.authorities.indexOf('ROLE_BOD') > -1) ||
                (this.account.authorities.indexOf('ROLE_MGR_PABRIK') > -1) ||
                (this.account.authorities.indexOf('ROLE_MGR_PRC') > -1) ||
                (this.account.authorities.indexOf('ROLE_ADMPO') > -1)) {
                this.filterDiv = true;
            }
        })

    }
    print(from: Date, thru: Date, nd: Internal) {
        if (this.filterDiv === false) {
            this.selectedDIV = this.account.idInternal;
        }
        this.loadingService.loadingStart()
        this.waktu = '23:59:59';
        this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;
        if (this.selectedPIC === '') {
            if (!this.idInternal.includes('SMK') && !this.idInternal.includes('SKG')) {
                this.pic = 'JTR;SVY;EVN;';
            } else if (this.idInternal.includes('SKG')) {
                this.pic = 'SVY_SKG;';
            } else {
                this.pic = 'SVY_SMK;JTR_SMK;';
            }
        } else {
            this.pic = this.selectedPIC;
        }

        if (this.selectedStatus === '') {
            this.status = '14;21;22;23;24;25;26;27;28';
        } else {
            this.status = this.selectedStatus;
        }
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|parampic:' + this.pic + '|stype:' + this.status + '|div:' + this.selectedDIV + '|plant:' + this.currentPlant;
        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/laporanppvspo/pdf', { filterData: this.filter_data }).
            subscribe((response: any) => {
                const reader = new FileReader();
                reader.readAsDataURL(response.blob());
                this.isview = true;
                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                reader.onloadend = (e) => {
                    this.pdfSrc = reader.result;
                };
                this.loadingService.loadingStop();
            });
    }
    exportExcel() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|parampic:' + this.pic + '|stype:' + this.status + '|div:' + this.selectedDIV + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFileWithName(`Report PP VS PO ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/laporanppvspo/xlsx', { filterData: this.filter_data });
    }
    backMainPage() {
        this.isview = false;
        this.pdfSrc = '';
    }

    getUserDiv() {
        let obj = new Object();
        obj = { label: 'All', value: 'ALL' };
        this.divitions.push(obj);
        this.userService.queryGetDiv()
            .subscribe(
                (res: ResponseWrapper) => {
                    console.log('ini data account == ', res.json);
                    res.json.forEach((e) => {
                        let nobj = new Object();
                        nobj = { label: e.idInternal, value: e.idInternal }
                        this.divitions.push(nobj);
                    });
                    if (this.userAccount === 'JNY') {

                    } else if (this.userAccount !== 'JNY') {
                        console.log(this.idInternal);
                        if (this.idInternal.includes('SKG')) {
                            this.divitions = this.divitions.filter((pic) => {
                                console.log('Cek pic:', pic); // Debug setiap elemen
                                return pic['value'].includes('SKG') || pic['label'] === 'All';
                            });
                            console.log('Hasil filter SKG:', this.divitions);
                        } else if (this.idInternal.includes('SMK')) {
                            this.divitions = this.divitions.filter((pic) => (pic['value'].includes('SMK') || pic['label'] === 'All'));
                        } else {
                            this.divitions = this.divitions.filter((pic) => ((!pic['value'].includes('SKG') && !pic['value'].includes('SMK')) || pic['label'] === 'All'));
                        }
                    }
                }
            )

    }
    getAccount() {
    }
}
