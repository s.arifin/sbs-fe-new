import { Component, OnInit, OnDestroy } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { AccountService } from '../../shared/auth/account.service';
import { ItemProduct, PurchasingService } from '../purchasing';
import { ReportService } from './report.service';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-report-kaleng',
    templateUrl: './report-kaleng.component.html'
})
export class ReportKalengComponent implements OnInit {
    commontUtilService: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string
    divitions: Array<Object>;
    selectedDIV: string;
    filterDiv: boolean;
    account: Account;
    us_divisi: any;
    date: Date;
    user: any;
    rofo1: string;
    rofo2: string;
    rofo3: string;
    selectBulan: any;
    modalPilihCeteakn: boolean;

    kd_produk: any;
    newBarang: any;
    selectedType: number;
    types = [
        { label: 'Semua', value: 0 },
        { label: 'Bahan Baku', value: 1 },
        { label: 'Bahan Pengemas', value: 2 },
        { label: 'Lain-Lain', value: 3 }
        // { label: 'Sparepart IDR', value: 9 },
    ];
    produks: Array<object> = [
        { label: 'Pilih Group Can Produk', value: '' },
        { label: 'Badak', value: '1' },
        { label: 'Lasegar', value: '2' },
        { label: 'Lain - Lain', value: '3' },
        { label: 'Keseluruhan Can', value: '4' },
    ];

    filteredBarang: ItemProduct[];

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected userService: UserService,
        protected accountService: AccountService,
        private purchasingService: PurchasingService,
        private reportService: ReportService,
        private alertService: JhiAlertService,

    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.selectedType = 0;
        this.date = new Date();
        this.rofo1 = '';
        this.rofo2 = '';
        this.rofo3 = '';
        this.selectBulan = '';
        this.modalPilihCeteakn = false;

    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.us_divisi = this.account.idInternal;
        });
    }
    print() {
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.loadingService.loadingStart();
        this.filter_data = 'dtFrom:' + dtFrom + '|dtThru:' + dtThru + '|idcan:' + this.selectBulan;
        if (this.selectBulan === '' || this.selectBulan === undefined || this.selectBulan === null) {
            alert('Pilih Group Can Terlebih Dahulu');
            this.loadingService.loadingStop();
        } else if (this.selectBulan === '4') {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/rpt_kaleng_new/pdf', { filterData: this.filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    };
                    this.loadingService.loadingStop();
                });
        } else {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/rpt_kaleng_single/pdf', { filterData: this.filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    };
                    this.loadingService.loadingStop();
                });
        }

    }

    private onSuccess(data, headers) {

        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    exportExcel() {
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.filter_data = 'dtFrom:' + dtFrom + '|dtThru:' + dtThru;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rptkaleng/xlsx', { filterData: this.filter_data });
    }
    backMainPage() {
        this.isview = false;
        this.pdfSrc = '';
    }
    public printReport() {
        this.modalPilihCeteakn = true;
    }

    printExcel() {
        this.loadingService.loadingStart()
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.filter_data = 'dtFrom:' + dtFrom + '|dtThru:' + dtThru + '|idcan:' + this.selectBulan;
        if (this.selectBulan === '4') {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_kaleng_new/xlsx', { filterData: this.filter_data });
        } else {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_kaleng_single/xlsx', { filterData: this.filter_data });
        }
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

    printPDF() {
        this.loadingService.loadingStart()
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.filter_data = 'dtFrom:' + dtFrom + '|dtThru:' + dtThru + '|idcan:' + this.selectBulan;
        if (this.selectBulan === '4') {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_kaleng_new/pdf', { filterData: this.filter_data });
        } else {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_kaleng_single/pdf', { filterData: this.filter_data });
        }
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;
    }

}
