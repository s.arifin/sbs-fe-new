import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';

@Component({
    selector: 'jhi-report-cancel-po',
    templateUrl: './report-cancel-po.component.html'
})
export class ReportCancelPOComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string;
    selectedType: number;
    pics: Array<object> = [
        { label: 'All', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' },
        { label: 'SVY_SKG', value: 'SVY_SKG' },
        { label: 'SVY_SMK', value: 'SVY_SMK' },
        { label: 'JTR_SMK', value: 'JTR_SMK' },
    ];
    idInternal: string;
    currentPlant: string;
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected loadingService: LoadingService
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = ''
        this.selectedType = 31;
        this.idInternal = this.principal.getIdInternal();
        if (!this.idInternal.includes('SMK') && !this.idInternal.includes('SKG')) {
            this.pics = this.pics.filter((pic) => (!pic['value'].includes('SMK') && !pic['value'].includes('SKG')) || pic['label'] === 'All');
            this.currentPlant = 'SBS';
        } else if (this.idInternal.includes('SMK')) {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SMK') || pic['label'] === 'All');
            this.currentPlant = 'SMK';
        } else {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SKG') || pic['label'] === 'All');
            this.currentPlant = 'SKG';
        }
    }

    ngOnInit() {
    }
    print() {
        this.loadingService.loadingStart()
        this.waktu = '23:59:59';
        this.dari = this.tgl1.getFullYear() + '-' + (this.tgl1.getMonth() + 1) + '-' + this.tgl1.getDate();
        this.sampai = this.tgl2.getFullYear() + '-' + (this.tgl2.getMonth() + 1) + '-' + this.tgl2.getDate() + ' ' + this.waktu;
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|pic:' + this.selectedPIC + '|plant:' + this.currentPlant;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/laporan_cancel_po/pdf', { filterData: this.filter_data });
        this.loadingService.loadingStop();
    }
    exportExcel() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        this.waktu = '23:59:59';
        this.dari = this.tgl1.getFullYear() + '-' + (this.tgl1.getMonth() + 1) + '-' + this.tgl1.getDate();
        this.sampai = this.tgl2.getFullYear() + '-' + (this.tgl2.getMonth() + 1) + '-' + this.tgl2.getDate() + ' ' + this.waktu;
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|pic:' + this.selectedPIC + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFileWithName(`Report Cancel PO ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/laporan_cancel_po/xlsx', { filterData: this.filter_data });
    }
    backMainPage() {
        this.isview = false;
    }
}
