import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';

@Component({
    selector: 'jhi-report-tt',
    templateUrl: './report-tt.component.html'
})
export class ReportTTComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    pics: Array<object> = [
        { label: 'All', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    statuses: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Approved kepala Bagian', value: '21' },
        { label: 'Register', value: '14' },
        { label: 'Serah Management', value: '24' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Siap PO', value: '25' },
        { label: 'Sudah Di Buat PO', value: '22' },
        { label: 'Siap PO Sebagian', value: '27' },
        { label: 'Sudah PO Sebagian', value: '28' },
        { label: 'Request Approve', value: '29' },
        { label: 'Approve', value: '11' },
        { label: 'Not Approve', value: '12' },
        { label: 'Siap BBM', value: '26' }
    ];
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected loadingService: LoadingService
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = ''
    }

    ngOnInit() {
    }
    print() {
        this.loadingService.loadingStart()
        this.waktu = '23:59:59.000';
        this.dari = this.tgl1.getFullYear() + '-' + (this.tgl1.getMonth() + 1) + '-' + this.tgl1.getDate();
        this.sampai = this.tgl2.getFullYear() + '-' + (this.tgl2.getMonth() + 1) + '-' + this.tgl2.getDate() + ' ' + this.waktu;
        // if (this.selectedPIC === '') {
        //     this.pic = 'JTR;SVY;EVN;';
        // } else {
        //     this.pic = this.selectedPIC;
        // }

        // if (this.selectedStatus === '') {
        //     this.status = '14;21;22;23;24;25';
        // } else {
        //     this.status = this.selectedStatus;
        // }
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|pic:' + this.selectedPIC;
        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/report_tanda_terima/xlsx', {filterData: this.filter_data}).
            subscribe((response: any) => {
                const reader = new FileReader();
                reader.readAsDataURL(response.blob());
                this.isview = true;
                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                reader.onloadend = (e) => {
                    this.pdfSrc = reader.result;
                };
                this.loadingService.loadingStop();
            });
    }
    exportExcel() {
        this.waktu = '23:59:59.000';
        this.dari = this.tgl1.getFullYear() + '-' + (this.tgl1.getMonth() + 1) + '-' + this.tgl1.getDate();
        this.sampai = this.tgl2.getFullYear() + '-' + (this.tgl2.getMonth() + 1) + '-' + this.tgl2.getDate() + ' ' + this.waktu;
        this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|pic:' + this.selectedPIC;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_tanda_terima/xlsx', {filterData: this.filter_data});
    }
    backMainPage() {
        this.isview = false;
    }
}
