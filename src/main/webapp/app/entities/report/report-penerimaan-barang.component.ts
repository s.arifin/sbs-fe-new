import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BbmPrint, ReportPenerimaanBarang, RealisasiPO, DetailReportRealisasiPO } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, LazyLoadEvent, DataTable } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterSupplierService } from '../master-supplier/master-supplier.service';
import { MasterSupplier } from '../master-supplier';
import { ItemProduct, PurchasingService } from '../purchasing';
import { PurchasingItem } from '../purchasing-item';
import { ExcelService } from './excel.service';
@Component({
    selector: 'jhi-report-penerimaan-barang',
    templateUrl: './report-penerimaan-barang.component.html'
})

export class ReportPenerimaanBarangComponent implements OnInit {
    @ViewChild('table') table: DataTable;
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    datas: RealisasiPO[];
    details: DetailReportRealisasiPO[];
    detailss: ReportPenerimaanBarang;
    detailsPO: PurchasingItem[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    newModalDetailPPIC: boolean;
    newModalDetailNonPPIC: boolean;
    newModalDetailPO: boolean;
    totalItemsDetail: any;
    queryCountDetail: any;
    totalItemsDetailPO: any;
    queryCountDetailPO: any;
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    kdSPL: string;
    is_reload: any;
    newSuplier: MasterSupplier;
    selectedStatus: string;
    first: any;
    selectedPIC: string;
    isFilter: number;
    routeData: any;
    previousPage: any;
    qtyPO: any;
    tgl_kirim_po: Date;
    nopo: any;
    alkumulasi_po: any;
    nourut: number;
    divisi: string;
    statuses: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Selesai', value: 'selesai' },
        { label: 'Terkirim Sebagian', value: 'sebagian' },
        { label: 'Belum Terkirim', value: 'belum' },
        { label: 'Overdue Tanggal Kirim', value: 'overdue' }
    ];
    pics: Array<object> = [
        { label: 'all', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' },
        { label: 'SVY_SKG', value: 'SVY_SKG' },
        { label: 'JTR_SMK', value: 'JTR_SMK' },
        { label: 'SVY_SMK', value: 'SVY_SMK' }
    ];
    selectedKd: any;
    newBarang: any;
    selectedType: number;
    types = [
        { label: 'Semua', value: 0 },
        { label: 'Bahan Baku', value: 1 },
        { label: 'Bahan Pengemas', value: 2 },
        { label: 'Lain-Lain', value: 3 },
        { label: 'Sparepart', value: 4 }
        // { label: 'Sparepart IDR', value: 9 },
    ];
    internals: any;
    filteredBarang: ItemProduct[];
    idInternal: string;
    currentPlant: string;
    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private masterSupplierService: MasterSupplierService,
        private purchasingService: PurchasingService,
        private excelService: ExcelService,
        private principal: Principal,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.selected = new Array<Pobbmlov>();
        this.datas = new Array<ReportPenerimaanBarang>();
        this.details = new Array<ReportPenerimaanBarang>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.newModalDetailPPIC = false;
        this.newModalDetailNonPPIC = false;
        this.newModalDetailPO = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.newSuplier = new MasterSupplier();
        this.isFilter = 0;
        this.selectedStatus = '';
        this.selectedPIC = '';
        this.selectedType = 0;
        this.selectedKd = '';
        this.is_reload = 0;
        this.nourut = 1;
        this.internals = this.principal.getIdInternal();
        if (!this.internals.includes('SMK') && !this.internals.includes('SKG')) {
            this.pics = this.pics.filter((pic) => (!pic['value'].includes('SMK') && !pic['value'].includes('SKG')) || pic['label'] === 'All');
            this.currentPlant = 'SBS';
        } else if (this.internals.includes('SMK')) {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SMK') || pic['label'] === 'All');
            this.currentPlant = 'SMK';
        } else {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SKG') || pic['label'] === 'All');
            this.currentPlant = 'SKG';
        }
    }
    ngOnInit(): void {
        // this.loadAll();
    }
    loadAll() {
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.reportService.getPenerimaanBarang({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch +
                    '|from:' + dari + '|thru:' + sampai +
                    '|kdspl:' + this.kdSPL + '|status:' + this.selectedStatus +
                    '|pic:' + this.selectedPIC + '|isfilter:' + this.isFilter +
                    '|jenis:' + this.selectedType +
                    '|kdbahan:' + this.selectedKd +
                    '|plant:' + this.currentPlant
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
            return;
        } else {
            this.reportService.getPenerimaanBarang({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch +
                    '|from:' + dari + '|thru:' + sampai +
                    '|kdspl:' + this.kdSPL + '|status:' + this.selectedStatus +
                    '|pic:' + this.selectedPIC + '|isfilter:' + this.isFilter +
                    '|jenis:' + this.selectedType +
                    '|kdbahan:' + this.selectedKd +
                    '|plant:' + this.currentPlant
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
        }

        this.selected = new Array<Pobbmlov>();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.datas = data;
        this.is_reload = 1;
        this.loadingService.loadingStop();
    };
    private onSuccessDetail(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.details = data;
        this.loadingService.loadingStop();
    };

    private onSuccessDetail1(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.detailss = data;
        this.qtyPO = this.detailss.qtypo;
        console.log(this.qtyPO);
        this.loadingService.loadingStop();

    };

    private onSuccessQty(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.details = data;
        this.loadingService.loadingStop();

    };

    private onSuccessDetailPO(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetailPO = headers.get('X-Total-Count');
        this.queryCountDetailPO = this.totalItemsDetailPO;
        this.detailsPO = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.is_reload = 0;
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.is_reload === 0) {

        } else {
            this.loadAll();
        }
        // if (this.datas === null) {

        // } else {

        // }
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        if (this.is_reload === 0) {

        } else {
            this.loadAll();
        }
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.datas = null;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selectedStatus = '';
        this.selectedPIC = '';
        this.selectedType = 0;
        this.selectedKd = '';
        this.is_reload = 0;
        this.nourut = 1;
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    lihatDetail(data: ReportPenerimaanBarang) {

        // this.tgl_kirim_po = data.tglkirimpo;
        this.nopo = data.nopo;
        this.nourut = data.nourut;
        let nopo = '';
        let idpo = '';
        if (data.idpo === undefined || data.idpo === null || data.idpo === '') {
            idpo = this.generateGUID();
        } else {
            idpo = data.idpo;
        }
        nopo = data.nopo.replace(/\//g, '-');
        this.reportService.getPenerimaanBarangDetail1(nopo, data.kdbahan, data.divisi, idpo)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessDetail(res.json, res.headers); this.loadingService.loadingStop();
                    this.newModalDetailNonPPIC = true;
                },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
    }

    lihatDetailPO(data: ReportPenerimaanBarang) {
        let nopo = '';
        nopo = data.nopo.replace(/\//g, '-');
        this.reportService.getPenerimaanBarangDetailPO(nopo)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessDetailPO(res.json, res.headers); this.loadingService.loadingStop();
                    this.newModalDetailPO = true;
                },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierNew().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    filter() {
        this.isFilter = 1
        this.loadAll();
    }
    reset() {
        this.isFilter = 0;
        this.is_reload = 0;
        this.page = 0;
        this.currentSearch = '';
        this.datas = null;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selectedStatus = '';
        this.selectedPIC = '';
        this.selectedType = 0;
        this.selectedKd = '';
    }
    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }

    public export() {
        const user = this.principal.getUserLogin();
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;

        const filter_data = 'iduser:' + user + '|dtfrom:' + dari + '|dtthru:' + sampai + '|pic:' + this.selectedPIC + '|plant:' + this.currentPlant + '|search:' + this.currentSearch;

        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();

        this.reportUtilService.downloadFileWithName(`Report Realisasi PO ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_realisasi_po/xlsx', { filterData: filter_data });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'nopo') {
            result.push('nopo');
        }
        return result;
    }

    generateGUID(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = Math.floor(Math.random() * 16);
            const v = c === 'x' ? r : (r % 4) + 8;
            return v.toString(16);
        });
    }

    backapp() {
        this.nopo = '';
        this.newModalDetailNonPPIC = false;
        this.nourut = 1;
    }

}
