import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ReportPurchaseRequestComponent } from './report-purchase-request.component';
import { ReportJobOrderComponent } from './report-job-order.component';
import { ReportPpVsPoComponent } from './report-ppvspo.component';
import { ReportPurchasingHistoryComponent } from './purchasing-history.component';
import { ReportUnregppComponent } from './report-unregpp.component';
import { ReportPOBComponent } from './report-pob.component';
import { PurchasingReportComponent } from './purchasing-report.component';
import { ReportCloseRejectItemComponent } from './report-close-reject-item.component';
import { ReportPOVSPPoComponent } from './report-povspp.component';
import { ReportPaymentComponent } from './report-payemt.component';
import { RikaReportComponent } from './rika-report.component';
import { ReportBBMKpiComponent } from './report-bbm-kpi.component';
import { ReportPaymentSplComponent } from './report-payemt-spl.component';
import { BbmPrintComponent } from './bbm-print.component';
import { LaporanTanterComponent } from './laporan-tanter.component';
import { ReportTTComponent } from './report-tt.component';
import { ReportPenerimaanBarangComponent } from './report-penerimaan-barang.component';
import { ReportMutasiBarangComponent } from './report-mutasi-barang.component';
import { ReportStockBarangComponent } from './report-stock-barang.component';
import { ReportMutasiBarang2Component } from './report-mutasi-barang2.component';
import { ReportDaftarSupplierComponent } from './report-daftar-supplier.component';
import { ReportPermintaanPembelianComponent } from './report-permintaan-pembelian.component';
import { ReportCancelPOComponent } from './report-cancel-po.component';
import { ReportPayment2Component } from './report-payment-2.component';
import { ReportProdukJadiComponent } from './report-produk-jadi.component';
import { ReportJobOrderRequestComponent } from './report-job-order-request.component';
import { ReportRealisasiPaymentComponent } from './report-realisasi-payment.component';
import { ReportStokFGComponent } from './report-stok-fg.component';
import { ReportBotolComponent } from './report-botol.component';
import { ReportKalengComponent } from './report-kaleng.component';
import { ReportPengirimanComponent } from './report-pengiriman.component';
import { ReportBBKComponent } from './report-bbk.component';
import { JhiPaginationUtil } from 'ng-jhipster';
import { ReportHasilProduksiComponent } from './report-hasil-produksi.component';
import { ReportKartuStockComponent } from './report-kartu-stock.component';
import { ReportPaymentMKTComponent } from './report-payment-mkt.component';

export class ReportResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'nopo,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}
export const reportRoute: Routes = [
    {
        path: 'report-purchase-request',
        component: ReportPurchaseRequestComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-payment-mkt',
        component: ReportPaymentMKTComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-produk-jadi',
        component: ReportProdukJadiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-job-order',
        component: ReportJobOrderComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
    , {
        path: 'report-request-job-order',
        component: ReportJobOrderRequestComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
    ,
    {
        path: 'report-ppvspo',
        component: ReportPpVsPoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-purchasing-history',
        component: ReportPurchasingHistoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-unregister-pp',
        component: ReportUnregppComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-pob',
        component: ReportPOBComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-pembelian',
        component: PurchasingReportComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-close-reject-item',
        component: ReportCloseRejectItemComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-povspp',
        component: ReportPOVSPPoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-payment',
        component: ReportPaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-pembelian2',
        component: RikaReportComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-bbm-kpi',
        component: ReportBBMKpiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-payment-spl',
        component: ReportPaymentSplComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'bbm-print',
        component: BbmPrintComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'laporan-tanter',
        component: LaporanTanterComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'laporan-tanda-terima',
        component: ReportTTComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-penerimaan-barang',
        component: ReportPenerimaanBarangComponent,
        // resolve: {
        //     'pagingParams': ReportResolvePagingParams
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-mutasi-barang',
        component: ReportMutasiBarangComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-stock-barang',
        component: ReportStockBarangComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-mutasi-barang2',
        component: ReportMutasiBarang2Component,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-daftar-supplier',
        component: ReportDaftarSupplierComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-permintaan-pembelian',
        component: ReportPermintaanPembelianComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-cancel-po',
        component: ReportCancelPOComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-pament-2',
        component: ReportPayment2Component,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-realisasi-payment',
        component: ReportRealisasiPaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-stok-fg',
        component: ReportStokFGComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-botol',
        component: ReportBotolComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-kaleng',
        component: ReportKalengComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'report-pengiriman',
        component: ReportPengirimanComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'report-bbk',
        component: ReportBBKComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'report-hasil-produksi',
        component: ReportHasilProduksiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'report-kartu-stock',
        component: ReportKartuStockComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
