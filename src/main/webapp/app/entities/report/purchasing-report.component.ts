import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { Purchasing, PurchasingService } from '../purchasing';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportService } from './report.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { ReportPurchasing } from './report.model';
import * as moment from 'moment';

@Component({
    selector: 'jhi-purchasing-report',
    templateUrl: './purchasing-report.component.html'
})
export class PurchasingReportComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    pics: Array<object> = [
        { label: 'All', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    statuses: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Approved kepala Bagian', value: '21' },
        { label: 'Register', value: '14' },
        { label: 'Serah Management', value: '24' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Siap PO', value: '25' },
        { label: 'Sudah Di Buat PO', value: '22' },
        { label: 'Siap PO Sebagian', value: '27' },
        { label: 'Sudah PO Sebagian', value: '28' },
        { label: 'Request Approve', value: '29' },
        { label: 'Approve', value: '11' },
        { label: 'Not Approve', value: '12' },
        { label: 'Siap BBM', value: '26' }
    ];
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string;
    filteredBarang: any[];
    newBarang: any;
    selectedKd: any;
    selectedSPL: any;
    filteredCodeSuppliers: any[];
    filteredSuppliers: any[];
    newSuplier: any;
    itemsPerPage: any;
    page: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    idPrint: any;
    modalPilihCeteakn: boolean;
    pdfBlob: any;
    pilihBahasa: string;
    isTnpaHrg: boolean;
    isViewCetakan: boolean;
    filter: any;
    reportPurchasing: ReportPurchasing[];
    // 1. bahan pengemas
    // 2. bahan pengemas PPN
    // 3. bahan baku
    // 4. bahan baku ppn
    // 5. bahan pengemas USD
    // 6 lain lain import
    // 7 lain lain import
    // 8 lain lain IDR
    // 9 sparepart IDR
    // 10 sparepart VALAS
    types = [
        { label: 'Bahan Baku Valas', value: 11 },
        { label: 'Bahan Baku IDR PPN', value: 4 },
        { label: 'Bahan Baku IDR Non PPN', value: 3 },
        { label: 'Bahan Pengemas Valas', value: 5 },
        { label: 'Bahan Pengemas IDR PPN', value: 2 },
        { label: 'Bahan Pengemas IDR Non PPN', value: 1 },
        { label: 'Import', value: 6 },
        { label: 'Lain-Lain Valas', value: 7 },
        { label: 'Lain-Lain IDR', value: 8 },
        { label: 'sparepart Valas', value: 10 },
        { label: 'Sparepart IDR', value: 9 },
    ];
    selectedType: number;
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        private purchasingService: PurchasingService,
        private masterSupplierService: MasterSupplierService,
        protected reportService: ReportService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = '';
        this.selectedKd = '';
        this.selectedSPL = '';
        this.idPrint = '';
        this.modalPilihCeteakn = false;
        this.pilihBahasa = '';
        this.isTnpaHrg = false;
        this.isViewCetakan = false;
        this.filter = '';
        this.reportPurchasing = new Array<ReportPurchasing>();
        this.selectedType = 1;
        this.tgl1 = new Date();
        this.tgl2 = new Date();
    }

    loadAll() {
        if (this.isview === true) {
            this.loadingService.loadingStart();
            this.reportService.getRptPurchasing({
                filter: this.filter
            })
                .subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        }
    }
    ngOnInit() {
    }
    print(from: Date, thru: Date) {
        this.loadingService.loadingStart();
        this.waktu = '23:59:59';
        this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;
        this.filter = 'dtFrom:' + this.dari + '|dtThru:' + this.sampai + '|type:' + this.selectedType + '|nmtype:' +
            this.types.filter((items) => items.value === this.selectedType).map((a) => a.label);
        this.filter_data = this.filter;
        this.isview = true;
        this.loadAll();
        this.loadingService.loadingStop();
    }
    exportExcel(from: Date, thru: Date) {
        this.loadingService.loadingStart();
        this.waktu = '23:59:59';
        this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;
        this.filter = 'username:SVY|dtFrom:' + this.dari + '|dtThru:' + this.sampai + '|type:' + this.selectedType + '|nmtype:' +
            this.types.filter((items) => items.value === this.selectedType).map((a) => a.label);
        let namaReport = '';
        this.types.filter(
            (items) => items.value === this.selectedType).map((a) => namaReport = a.label);
        this.filter_data = this.filter;
        this.isview = true;
        namaReport = '(Bu Juni) ' + namaReport +  '(' + this.getMonthYear(from) + ' s-d ' + this.getMonthYear(thru) + ')';
        if (this.selectedType === 5 || this.selectedType === 7 || this.selectedType === 10 || this.selectedType === 11) {
            this.reportUtilService.downloadFileWithName(namaReport, process.env.API_C_URL + '/api/report/rpt_lap_pembelian_valas/xlsx', { filterData: this.filter_data });
        } else {
            this.reportUtilService.downloadFileWithName(namaReport, process.env.API_C_URL + '/api/report/rpt_lap_pembelian/xlsx', { filterData: this.filter_data });
        }
        this.loadingService.loadingStop();
    }
    backMainPage() {
        this.isViewCetakan = false;
    }

    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.reportPurchasing = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
    }

    public getMonthYear(date: Date): String {
        const bulan = date.getMonth() + 1;
        let hasil = '';
        if (bulan === 1) {
            hasil = 'Jan - ' + date.getFullYear();
        } if (bulan === 2) {
            hasil = 'Feb - ' + date.getFullYear();
        } if (bulan === 3) {
            hasil = 'Mar - ' + date.getFullYear();
        } if (bulan === 4) {
            hasil = 'Apr - ' + date.getFullYear();
        } if (bulan === 5) {
            hasil = 'Mei - ' + date.getFullYear();
        } if (bulan === 6) {
            hasil = 'Jun - ' + date.getFullYear();
        } if (bulan === 7) {
            hasil = 'Jul - ' + date.getFullYear();
        } if (bulan === 8) {
            hasil = 'Agu - ' + date.getFullYear();
        } if (bulan === 9) {
            hasil = 'Sep - ' + date.getFullYear();
        } if (bulan === 10) {
            hasil = 'Okt - ' + date.getFullYear();
        } if (bulan === 11) {
            hasil = 'Nov - ' + date.getFullYear();
        } if (bulan === 12) {
            hasil = 'Des - ' + date.getFullYear();
        }

        return hasil;
    }
    setSampai() {
        // this.tgl2 = new Date();
        // console.log('plus 3 hari => ', this.tgl1.getDate() + 3);
        // this.tgl2.setDate(this.tgl1.getDate());
        // this.tgl2.setMonth(this.tgl1.getMonth());
        // this.tgl2.setFullYear(this.tgl1.getFullYear());
        // this.tgl2 = moment().add(3, 'days').toDate();
    }
}
