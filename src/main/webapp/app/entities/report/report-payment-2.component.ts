import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { DataTable, SelectItem, ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { LoadingService } from '../../layouts';
import { Account, Principal, ITEMS_PER_PAGE } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { PaymentReportVM2, PaymentReport, Payment, PaymentService } from '../payment';
import { ExcelService } from './excel.service';

@Component({
    selector: 'jhi-report-payment-2.component',
    templateUrl: './report-payment-2.component.html'
})
export class ReportPayment2Component implements OnInit, OnDestroy {
    currentAccount: Account;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    sortValue: number;
    cari: string;
    sorts: Array<object> = [
        { label: 'Nomor PO', value: 1 },
        { label: 'Nomor Tanda Terima', value: 2 },
    ];
    type: string;
    types: Array<object> = [
        { label: 'all', value: '' },
        { label: 'Uang Muka', value: '1' },
        { label: 'Downpayment', value: '2' },
        { label: 'Pelunasan', value: '3' },
        { label: 'kasnegara', value: '4' },
        { label: 'PPJK', value: '5' }
    ];
    status: string;
    statuses: Array<object> = [
        { value: '', label: 'ALL' },
        { value: '10', label: 'New' },
        { value: '11', label: 'Approved Manager Departement' },
        { value: '12', label: 'Not Approved' },
        { value: '13', label: 'Canceled' },
        { value: '16', label: 'Done' },
        { value: '29', label: 'Request Approve' },
        { value: '53', label: 'Sudah Dibayar' },
        { value: '54', label: 'Request Approve Accounting' },
        { value: '56', label: 'Approve Accounting' },
        { value: '58', label: 'Terima Accounting' },
        { value: '59', label: 'Terima Finance' },
    ];
    constructor(
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        private excelService: ExcelService
    ) {
        this.currentAccount = new Account();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.sortValue = 1;
        this.cari = '';
        this.type = '';
        this.status = '';
    }
    ngOnInit() {
        // this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            console.log('currentAccount == ', this.currentAccount);
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Payment) {
        return item.id;
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.nmSupplier;
    }
    filter() {
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.nmSupplier === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.kd_suppnew;
        }
    }
    public export() {
        // report_payment_voucher_2x
        // let filter_data = '';
        // filter_data = 'dtfrom:' + this.dtfrom.toISOString().split('T')[0] + '|dtthru:' + this.dtthru.toISOString().split('T')[0] + '|sort:' + this.sortValue;
        // this.reportUtilService.downloadFileWithName('Report Payment Voucher 2', process.env.API_C_URL + '/api/report/report_payment_voucher_2/xlsx', { filterData: filter_data });
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        let filter_data = '';
        filter_data = 'dtfrom:' + this.dtfrom.toISOString().split('T')[0] +
            '|dtthru:' + this.dtthru.toISOString().split('T')[0] +
            '|sort:' + this.sortValue +
            '|cari:' + this.cari +
            '|admin:' + this.currentAccount.login +
            '|nmsupp:' + this.kdsup +
            '|type:' + this.type +
            '|status:' + this.status;
        console.log('ini filter data');
        console.log(filter_data);
        this.reportUtilService.downloadFileWithName(`Report Payment Voucher 2 ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_payment_voucher_2x/xlsx', { filterData: filter_data });
    }
}
