import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { JobordersService } from '../joborders/joborders.service';
import { LoadingService } from '../../layouts';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';

@Component({
    selector: 'jhi-report-payment-mkt',
    templateUrl: './report-payment-mkt.component.html'
})
export class ReportPaymentMKTComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    currentAccount: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    inputDivReciver: any;
    inptSeksie: any;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    divitions: Array<object> = [
        { label: 'All', value: 3 },
        { label: 'Belum Bayar', value: 1 },
        { label: 'Sudah Bayar', value: 2 },
    ];
    brandList: Array<object> = [
        { label: 'All', value: '' },
        { label: 'BADAK', value: 'BDK' },
        { label: 'PISTOL', value: 'LSG' },
        { label: 'OTC', value: 'OTC' },
        { label: 'ACARAKI', value: 'ACA' },
    ];
    inputBrang: any;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        protected jobOrderService: JobordersService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        private masterSupplierService: MasterSupplierService,
        protected loadingService: LoadingService,
    ) {
        this.kdbarang = new MasterBahan();
        this.tgl1 = new Date();
        this.tgl2 = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.inputDivReciver = null;
        this.inputBrang = null;
    }

    ngOnInit() {
        this.idinternal = this.principal.getIdInternal();
        this.currentAccount = this.principal.getUserLogin();
    }

    print(from: Date, thru: Date) {
        this.loadingService.loadingStart();
        if (this.inputDivReciver === undefined || this.inputDivReciver === null || this.inputDivReciver === '') {
            alert('Status PV Harus Dipilih Terlebih Dahulu');
            this.loadingService.loadingStop();
        } else if (this.inputBrang === undefined || this.inputBrang === null) {
            alert('Brand Harus Dipilih Terlebih Dahulu');
            this.loadingService.loadingStop();
        } else {
            const waktu = '23:59:59.000';
            const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
            const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
            const filter_data = 'dtFrom:' + dari + '|dtThru:' + sampai + '|iduser:' + this.currentAccount + '|status1:' + this.inputDivReciver + '|kdsup:' + this.kdsup + '|brand:' + this.inputBrang;
            const date = new Date();
            const currentDay = date.getDate();
            const currentMonth = date.getMonth() + 1;
            const currentYear = date.getFullYear();
            this.reportUtilService.downloadFileWithName(`Report Payment MKT ${currentDay}/${currentMonth}/${currentYear}`, process.env.API_C_URL + '/api/report/rpt_mkt/xlsx', { filterData: filter_data });
            this.loadingService.loadingStop();
        }

    }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    reset() {
        this.loadingService.loadingStart();
        this.tgl1 = new Date();
        this.tgl2 = new Date();
        this.inputDivReciver = null;
        this.kdsup = '';
        this.inputBrang = null;
        this.newSuplier = new MasterSupplier();
        this.loadingService.loadingStop();
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.kd_suppnew;
    }
}
