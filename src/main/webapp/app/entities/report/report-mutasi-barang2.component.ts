import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BbmPrint, ReportMutasiBahan2, ReportMutasiBahan2Detail, ReportPenerimaanBarang } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, AccordionTab, LazyLoadEvent } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
import { ItemProduct, PurchasingService } from '../purchasing';

@Component({
    selector: 'jhi-report-mutasi-barang2',
    templateUrl: './report-mutasi-barang2.component.html'
})

export class ReportMutasiBarang2Component implements OnInit {
    @ViewChild('table') table: AccordionTab;
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    datas: ReportMutasiBahan2[];
    details: ReportMutasiBahan2[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    newModalDetail: boolean;
    totalItemsDetail: any;
    queryCountDetail: any;
    dtfrom: Date;
    dtthru: Date;
    selectedKd: any;
    newBarang: any;
    selectedType: number;
    types = [
        { label: 'Semua', value: 0 },
        { label: 'Bahan Baku', value: 1 },
        { label: 'Bahan Pengemas', value: 2 },
        { label: 'Lain-Lain', value: 3 },
        { label: 'Sparepart', value: 4 }
        // { label: 'Sparepart IDR', value: 9 },
    ];
    filteredBarang: ItemProduct[];
    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private excelService: ExcelService,
        private purchasingService: PurchasingService,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array<Pobbmlov>();
        this.datas = new Array<ReportPenerimaanBarang>();
        this.details = new Array<ReportPenerimaanBarang>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.newModalDetail = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selectedType = 0;
        this.selectedKd = '';
    }
    cari() {
        this.loadAll2();
    }
    ngOnInit(): void {
        // this.loadAll();
    }
    loadAll() {
        this.loadingService.loadingStart();
        this.reportService.getMutasiBarang2().subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
            (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
        );
        this.selected = new Array<Pobbmlov>();
    }
    loadAll2() {
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        this.loadingService.loadingStart();
        this.reportService.getMutasiBarang2({
            filter: 'from:' + dari +
                    '|thru:' + sampai +
                    '|jenis:' + this.selectedType +
                    '|kdbahan:' + this.selectedKd
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
            (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
        );
        this.selected = new Array<Pobbmlov>();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.datas = data;
        this.loadingService.loadingStop();

    };
    private onSuccessDetail(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.details = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        // this.loadAll();
    }
    clear() {
        this.page = 0;
        this.selectedType = 0;
        this.selectedKd = '';
        this.loadAll2();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.loadAll();
    }

    lihatDetail(data: ReportPenerimaanBarang) {
        console.log('masuk ngga');
        let nopo = '';
        nopo = data.nopo.replace(/\//g, '-');
        this.reportService.getPenerimaanBarangDetailMutasi(nopo, data.kdbahan)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessDetail(res.json, res.headers); this.loadingService.loadingStop();
                    this.newModalDetail = true;
                },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
    }
    filter() {
        this.loadAll();
    }
    public export() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        console.log('ini data excel ', this.table.accordion.el.nativeElement);
        this.excelService.exportTableAsExcelFile(this.table.accordion.el.nativeElement, `Mutasi Barang ${currentDay}_${currentMonth}_${currentYear}`);
    }
    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }
    openPdf(rowData: ReportMutasiBahan2Detail) {
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.loadingService.loadingStart();
        let bbms: string;
        let bbk: string;

        bbms = rowData.jenistrx.replace(/\//g, '-');
        const pecah = bbms.split('-');
        if (rowData.tipe === 'bbmnontek') {
                const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbm_non_tek_rpt/pdf', { filterData: filter_data });
                console.log('BBM NON TEKNIK');
        } else if (rowData.tipe === 'bbknontek') {
            bbk = pecah[0].substring(3, 4);
            if (bbk === 'P' || bbk === '1') {
                const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbk_produksi/pdf', { filterData: filter_data });
            } else if (bbk === '2') {
                const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbk_supplier/pdf', { filterData: filter_data });
            } else {
                const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
                this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbk_lain/pdf', { filterData: filter_data });
            }
        } else if (rowData.tipe === 'bbmtek') {
            const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbm_tek_rpt/pdf', { filterData: filter_data });
        }else if (rowData.tipe === 'bbktek') {
            const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbk_teknik/pdf', { filterData: filter_data });
        }
        this.loadingService.loadingStop();
    }

}
