import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BBKModel, BBMModel, BbmPrint, ReportPenerimaanBarang, ReportStockBarang } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, DataTable, LazyLoadEvent } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
import { ItemProduct, PurchasingService } from '../purchasing';

@Component({
    selector: 'jhi-report-bbk',
    templateUrl: './report-bbk.component.html'
})

export class ReportBBKComponent implements OnInit {
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    datas: BBKModel[];
    details: BBMModel[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    newModalDetail: boolean;
    totalItemsDetail: any;
    queryCountDetail: any;
    dtfrom: Date;
    dtthru: Date;
    selectedKd: any;
    newBarang: any;
    selectedType: any;
    internal: any;
    types = [
        { label: 'Pilih Jenis Bahan', value: '' },
        { label: 'Bahan Baku', value: 'rm' },
        { label: 'Bahan Pengemas', value: 'pm' },
    ];
    filteredBarang: ItemProduct[];

    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private excelService: ExcelService,
        private purchasingService: PurchasingService,
        private principal: Principal,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array<Pobbmlov>();
        this.datas = new Array<ReportPenerimaanBarang>();
        this.details = new Array<ReportPenerimaanBarang>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.selectedType = 'all';
        this.selectedKd = '';
        this.newModalDetail = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
    }
    ngOnInit(): void {
        // this.loadAll();
    }
    loadAll() {
        if (this.selectedType === '' || this.selectedType === undefined || this.selectedType === null) {
            alert('Jenis Bahan Harus Di Pilih');
        } else {
            this.internal = this.principal.getIdInternal();
            const waktu = '23:59:59';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            this.loadingService.loadingStart();
            if (this.currentSearch) {
                this.reportService.getBBK({
                    page: this.page,
                    size: this.itemsPerPage,
                    filter: 'cari:' + this.currentSearch + '|kdbahan:' + this.selectedKd +
                        '|from:' + dari +
                        '|thru:' + sampai +
                        '|jenis:' + this.selectedType
                }).subscribe(
                    (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                    (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
                );
                return;
            } else {
                this.reportService.getBBK({
                    page: this.page,
                    size: this.itemsPerPage,
                    filter: 'cari:' + this.currentSearch + '|kdbahan:' + this.selectedKd +
                        '|from:' + dari +
                        '|thru:' + sampai +
                        '|jenis:' + this.selectedType
                }).subscribe(
                    (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                    (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
                );
            }

            this.selected = new Array<Pobbmlov>();
        }
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.datas = data;
        this.loadingService.loadingStop();

    };
    private onSuccessDetail(data, headers) {
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.details = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        // this.loadAll();
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.selectedType = 'all';
        this.datas = undefined;
        this.dtthru = new Date();
        this.dtfrom = new Date();
        this.selectedKd = '';
        this.newBarang = '';
        // this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    lihatDetail(no_bbk: string, kdbahan: string) {
        const nopo = no_bbk.replace(/\//g, '-');
        this.reportService.getBBMSupplier(nopo, kdbahan)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessDetail(res.json, res.headers); this.loadingService.loadingStop();
                    this.newModalDetail = true;
                },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
    }

    filter() {
        this.loadAll();
    }

    filterBarangSingle(event) {
        const query = event.query;
        this.reportService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }

}
