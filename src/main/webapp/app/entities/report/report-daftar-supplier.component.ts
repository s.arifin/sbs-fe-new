import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { ItemProduct, Purchasing, PurchasingService } from '../purchasing';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportService } from './report.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'jhi-report-daftar-supplier',
    templateUrl: './report-daftar-supplier.component.html'
})
export class ReportDaftarSupplierComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;
    jenis: Array<object> = [
        { label: 'Pilih Jenis Laporan', value: '' },
        { label: 'Bahan Baku', value: 'RM' },
        { label: 'Bahan Kemas', value: 'PM' },
        { label: 'Sparepart', value: 'S' },
        { label: 'Lain - Lain', value: 'Lain' },
    ];
    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    jenis1: string;
    sampai1: string
    modalPilihCeteakn: boolean;
    idInternal: string;
    currentPlant: string;
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        private purchasingService: PurchasingService,
        private masterSupplierService: MasterSupplierService,
        protected reportService: ReportService,
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.sampai1 = '';
        this.modalPilihCeteakn = false;
        this.idInternal = this.principal.getIdInternal();
        if (!this.idInternal.includes('SMK') && !this.idInternal.includes('SKG')) {
            // this.jenis = this.jenis.filter((pic) => (!pic['value'].includes('SMK') && !pic['value'].includes('SKG')) || pic['label'] === 'All');
            this.currentPlant = 'SBS';
        } else if (this.idInternal.includes('SMK')) {
            this.jenis = this.jenis.filter((pic) => pic['value'].includes('Lain') || pic['label'] === 'Pilih Jenis Laporan');
            this.currentPlant = 'SMK';
        } else {
            this.jenis = this.jenis.filter((pic) => pic['value'].includes('Lain') || pic['label'] === 'Pilih Jenis Laporan');
            this.currentPlant = 'SKG';
        }
    }
    ngOnInit() {
    }

    public printReport() {
        if (this.jenis1 === undefined || this.jenis1 === '') {
            alert('Jenis Laporan Harus Dipilih');
        } else {
            this.modalPilihCeteakn = true;
        }
    }
    printExcel() {
        this.loadingService.loadingStart()
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        if (this.jenis1 === null || this.jenis1 === undefined || this.jenis1 === '') {
            alert('Jenis Laporan Harus Di Pilih');
        } else if (this.jenis1 === 'RM') {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_baku_xlsx/xlsx', { filterData: this.filter_data })
            this.loadingService.loadingStop();
        } else if (this.jenis1 === 'PM') {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_pengemas_xlsx/xlsx', { filterData: this.filter_data })
            this.loadingService.loadingStop();
        } else if (this.jenis1 === 'Lain') {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_lain_xlsx/xlsx', { filterData: this.filter_data })
            this.loadingService.loadingStop();
        } else {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_sparepart_xlsx/xlsx', { filterData: this.filter_data })
            this.loadingService.loadingStop();
        }

    }

    printPDF() {
        this.loadingService.loadingStart()
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();

        if (this.jenis1 === null || this.jenis1 === undefined || this.jenis1 === '') {
            alert('Jenis Laporan Harus Di Pilih');
        } else if (this.jenis1 === 'RM') {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_baku/pdf', { filterData: this.filter_data })
            this.modalPilihCeteakn = false;
            this.loadingService.loadingStop();
        } else if (this.jenis1 === 'PM') {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_pengemas/pdf', { filterData: this.filter_data })
            this.modalPilihCeteakn = false;
            this.loadingService.loadingStop();
        } else if (this.jenis1 === 'Lain') {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_lain/pdf', { filterData: this.filter_data })
            this.modalPilihCeteakn = false;
            this.loadingService.loadingStop();
        } else {
            this.filter_data = 'vjenis:' + this.jenis1 + '|plant:' + this.currentPlant;
            this.reportUtilService.downloadFileWithName(`Report Daftar Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_daftar_suplier_sparepart/pdf', { filterData: this.filter_data })
            this.modalPilihCeteakn = false;
            this.loadingService.loadingStop();
        }

    }
}
