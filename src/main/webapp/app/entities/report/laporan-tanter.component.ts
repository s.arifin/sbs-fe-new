import { Component, OnInit } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BbmPrint } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-laporan-tanter',
    templateUrl: './laporan-tanter.component.html'
})

export class LaporanTanterComponent implements OnInit {
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    pobbm: BbmPrint;
    pobbms: BbmPrint[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    tgl1: Date;
    tgl2: Date;
    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array<Pobbmlov>();
        this.pobbm = new Pobbmlov();
        this.pobbms = new Array<Pobbmlov>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.tgl1 = new Date();
        this.tgl2 = new Date();
    }
    ngOnInit(): void {
        this.loadAll();
    }
    loadAll() {
        // this.loadingService.loadingStart();
        // if (this.currentSearch) {
        //     this.reportService.getBBMPrint({
        //         query: 'nobbm:' + this.currentSearch,
        //         page: this.page,
        //         size: this.itemsPerPage,
        //     }).subscribe(
        //         (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
        //         (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
        //     );
        //     return;
        // }
        // this.reportService.getBBMPrint({
        //     query: 'nobbm:',
        //     page: this.page,
        //     size: this.itemsPerPage,
        // }).subscribe(
        //     (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
        //     (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
        // );
        // this.selected = new Array<Pobbmlov>();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.pobbms = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }
    exportExcel() {
        const waktu = '23:59:59';
        const dari = this.tgl1.getFullYear() + '-' + (this.tgl1.getMonth() + 1) + '-' + this.tgl1.getDate();
        const sampai = this.tgl2.getFullYear() + '-' + (this.tgl2.getMonth() + 1) + '-' + this.tgl2.getDate() + ' ' + waktu;
        const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_tanda_terima/xlsx', { filterData: filter_data });
    }

}
