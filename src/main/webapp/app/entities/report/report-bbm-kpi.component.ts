import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportService } from './report.service';
import { ConfirmationService, DataTable, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { SelectItem } from 'primeng/primeng';
import { Payment, PaymentReport, PaymentService } from '../payment';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
import * as XLSX from 'xlsx';
import { ReportBBMKpi } from './report.model';
import { DatePipe } from '@angular/common';
@Component({
    selector: 'jhi-report-bbm-kpi.component',
    templateUrl: './report-bbm-kpi.component.html'
})
export class ReportBBMKpiComponent implements OnInit, OnDestroy {
    @ViewChild('table') table: DataTable;
    @Input() idstatus: number;
    currentAccount: Account;
    payments: ReportBBMKpi[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Payment[];
    newModalNote: boolean;
    notehold: string;
    login: string;
    payment: Payment;
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    radioValue: number;
    pembayarans: Array<object> = [
        { label: 'SEMUA', value: 0 },
        { label: 'Belum Lunas', value: 1 },
        { label: 'Sudah Lunas', value: 2 },
    ];
    cols = [
        { field: 'tglpo', header: 'Tgl. PO' },
        { field: 'nopo', header: 'No. PO' },
        { field: 'no_bbm', header: 'No. BBM' },
        { field: 'tgl_bbm', header: 'Tgl. BBM' },
        { field: 'kodebahan', header: 'Kode Bahan' },
        { field: 'nmbahan', header: 'Nama Bahan' },
        { field: 'qtypo', header: 'QTY PO' },
        { field: 'satuanpo', header: 'Satuan PO' },
        { field: 'qtybbm', header: 'QTY BBM' },
        { field: 'satuanbbm', header: 'Satuan BBM' },
        { field: 'tglkirimpo', header: 'Tgl. Kirim PO' },
        { field: 'tglsj', header: 'Tgl. Surat Jalan' }
    ];
    columnOptions: SelectItem[];
    types = [
        { label: 'Semua Bahan', value: 0 },
        { label: 'Bahan Baku Valas', value: 11 },
        { label: 'Bahan Baku IDR PPN', value: 4 },
        { label: 'Bahan Baku IDR Non PPN', value: 3 },
        { label: 'Bahan Pengemas Valas', value: 5 },
        { label: 'Bahan Pengemas IDR PPN', value: 2 },
        { label: 'Bahan Pengemas IDR Non PPN', value: 1 },
        { label: 'Import', value: 6 },
        { label: 'Lain-Lain Valas', value: 7 },
        { label: 'Lain-Lain IDR', value: 8 },
        { label: 'sparepart Valas', value: 10 },
        { label: 'Sparepart IDR', value: 9 },
    ];
    idInternal: string;
    currentPlant: string;
    selectedType: number;
    constructor(
        private paymentService: PaymentService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
        private confirmatinService: ConfirmationService,
        private masterSupplierService: MasterSupplierService,
        private excelService: ExcelService,
        private reportService: ReportService,
        private datePipe: DatePipe,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Payment>();
        this.newModalNote = false;
        this.notehold = '';
        this.login = '';
        this.payment = new Payment();
        this.currentAccount = new Account();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.idstatus = 10;
        this.radioValue = 0;
        this.columnOptions = [];
        for (let i = 0; i < this.cols.length; i++) {
            const obj = { label: this.cols[i].header, value: this.cols[i] }
            this.columnOptions.push(obj);
        }
        this.selectedType = 0;
        this.idInternal = this.principal.getIdInternal();
        if (!this.idInternal.includes('SMK') && !this.idInternal.includes('SKG')) {
            // this.pics = this.pics.filter((pic) => (!pic['value'].includes('SMK') && !pic['value'].includes('SKG')) || pic['label'] === 'All');
            this.currentPlant = 'SBS';
        } else if (this.idInternal.includes('SMK')) {
            // this.pics = this.pics.filter((pic) => pic['value'].includes('SMK') || pic['label'] === 'All');
            this.currentPlant = 'SMK';
        } else {
            // this.pics = this.pics.filter((pic) => pic['value'].includes('SKG') || pic['label'] === 'All');
            this.currentPlant = 'SKG';
        }
    }
    loadAllFilter() {
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate();
        this.reportService.getBBMKPI({
            // filter: 'dtFrom:' + this.dtfrom.toISOString().split('T')[0] + '|dtThru:' + this.dtthru.toISOString().split('T')[0]
            filter: 'dtFrom:' + dari + '|dtThru:' + sampai + '|plant:' + this.currentPlant
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/payment'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAllFilter();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/payment', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAllFilter();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/payment', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAllFilter();
    }
    ngOnInit() {
        // this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            console.log('currentAccount == ', this.currentAccount);
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Payment) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.payments = data;
        // this.payments.forEach(
        //     (e) => {
        //         e.tglpo = new Date(this.datePipe.transform(e.tglpo, 'dd/MM/yyyy'));
        //         e.tgl_bbm = new Date(this.datePipe.transform(e.tgl_bbm, 'dd/MM/yyyy'));
        //         e.tglkirimpo = new Date(this.datePipe.transform(e.tglkirimpo, 'dd/MM/yyyy'));
        //         e.tglsj = new Date(this.datePipe.transform(e.tglsj, 'dd/MM/yyyy'));
        //     }
        // )
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllFilter();
    }
    print() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();

        this.loadingService.loadingStart();
        let namaReport = ''
        this.types.filter(
            (items) => items.value === this.selectedType).map((a) => namaReport = a.label + currentDay + '_' + currentMonth + '_' + currentYear);
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate();
        const filter_data = 'dtFrom:' + dari + '|dtThru:' + sampai + '|type:' + this.selectedType + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFileWithName(namaReport, process.env.API_C_URL + '/api/report/bbm_kpi/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();

    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.nmSupplier;
    }
    filter() {
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.nmSupplier === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.nmSupplier;
        }
        this.loadAllFilter();
    }
    reset() {
        this.loadAllFilter();
        this.kdsup = '';
    }
    exportExcel() {
        this.excelService.exportAsExcelFile(this.payments, 'report bbm kpi');
    }
    public export() {
        console.log('ini data excel ', this.table.el.nativeElement);
        this.excelService.exportTableAsExcelFile(this.table.el.nativeElement, 'report bbm kpi');
        // this.excelService.ExportTOExcel(this.table.el.nativeElement);
    }
}
