
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { BaseEntity } from './../../shared';

export class Report implements BaseEntity {
    constructor(
        public id?: any,
        public idType?: any,
        public description?: string,
    ) {
    }
}

export class ReportPurchasing {
    constructor(
        public nopo?: string,
        public tglpo?: Date,
        public qty?: number,
        public qtyPOB?: number,
        public pcode?: string,
        public pname?: string,
        public nmspl?: string,
        public dtreceive?: Date,
        public qtybbm?: number,
        public harga?: number,
        public jumlah?: number,
        public disc?: number,
        public totaldisc?: number,
        public ppn?: number,
        public total?: number,
        public nobbm?: string,
        public nosj?: string,
        public nott?: string,
        public uom?: string,
    ) {
    }
}
export class ReportBBMKpi {
    constructor(
        public tglpo?: Date,
        public nopo?: string,
        public no_bbm?: string,
        public tgl_bbm?: Date,
        public kodebahan?: string,
        public nmbahan?: string,
        public qtypo?: string,
        public satuanpo?: string,
        public qtybbm?: number,
        public satuanbbm?: string,
        public tglkirimpo?: Date,
        public tglsj?: Date
    ) {
    }
}
export class BbmPrint {
    constructor(
        public nobbm?: string,
        public nolhp?: string,
        public print?: number,
        public div?: string,
        public totalprint?: number,
    ) { }
}

export class ReportPenerimaanBarang {
    constructor(
        public id?: any,
        public nobbm?: string,
        public tglbbm?: Date,
        public kdbahan?: string,
        public nmbahan?: string,
        public nopo?: string,
        public tglkirimpo?: Date,
        public qtybbm?: number,
        public qtypo?: number,
        public kd_suppnew?: string,
        public nm_supplier?: string,
        public pic?: string,
        public status?: string,
        public outstanding?: number,
        public satuan?: string,
        public tgl_sj_supp?: Date,
        public status_kirim?: string,
        public divisi?: string,
        public idpo?: any,
        public div?: string,
        public nourut?: number
    ) {

    }
}

export class DetailReportRealisasiPO {
    constructor(
        public id?: any,
        public no_bbm?: string,
        public kdbahan?: string,
        public nmbahan?: string,
        public tglbbm?: Date,
        public qtypo?: number,
        public qtybbm?: number,
        public dtsent?: Date,
        public tgl_bbm?: Date,
        public tgl_sj?: Date,
        public satuan?: string,
        public tgl_exp?: Date,
        public tgl_exphal?: Date,
        public no_batch?: string,
    ) {

    }
}

export class RealisasiPO {
    constructor(
        public id?: any,
        public kdbahan?: string,
        public nmbahan?: string,
        public nopo?: string,
        public tglkirimpo?: Date,
        public qtypo?: number,
        public kd_suppnew?: string,
        public nm_supplier?: string,
        public pic?: string,
        public status?: string,
        public tglpo?: Date,
        public outstanding?: number,
        public satuan?: string,
        public divisi?: string,
        public idpo?: any,
        public iduser?: string,
        public tpbahan?: string,
        public nourut?: number

    ) {

    }
}

export class ReportStockBarang {
    constructor(
        public nopurchaseorder?: string,
        public nopurchasing?: string,
        public unit?: string,
        public codeproduk?: string,
        public produkname?: string,
        public qtypp?: number,
        public qtypo?: number,
        public otspp?: number,
        public otspo?: number,
        public tglpo?: Date,
        public tglbbm?: Date,
        public dtcreateby?: Date,
        public nm_supplier?: string,
        public satuan_pp?: string,
        public satuan_po?: string,
        public otstg?: number,

    ) {

    }
}

export class ReportMutasiBahan2 {
    constructor(
        public id?: number,
        public kdbahan?: string,
        public tanggal?: Date,
        public jenistrx?: string,
        public operan?: string,
        public qty?: any,
        public satuan?: string,
        public nmbahan?: string,
        public totalmasuk?: number,
        public totalkeluar?: number,
        public tipe?: string,
        public total?: number,
        public detail?: ReportMutasiBahan2Detail[],
    ) {
        this.detail = new Array<ReportMutasiBahan2Detail>();
    }
}

export class ReportMutasiBahan2Detail {
    constructor(
        public id?: number,
        public kdbahan?: string,
        public tanggal?: Date,
        public jenistrx?: string,
        public operan?: string,
        public qty?: any,
        public saldo?: number,
        public satuan?: string,
        public nmbahan?: string,
        public tipe?: string,
    ) {

    }
}

export class MasterProduk {
    constructor(
        public kd_produk?: string,
        public nm_produk?: string,
        public kd_group?: string,
    ) {
    }
}

export class MasterStokProduk {
    constructor(
        public kd_produk?: string,
        public kd_group?: string,
        public nama_produk?: string,
        public rilis?: any,
        public karantina?: any,
        public satuan?: string
    ) {

    }
}

export class BBKModel {
    constructor(
        public no_bbk?: string,
        public tgl_bbk?: Date,
        public iduser?: string,
        public kd_bahan?: string,
        public nmbahan?: string,
        public qty?: number,
        public no_lpm?: string,
        public satuan?: string,
        public supp?: string,
        public no_periksa?: string,
    ) {

    }
}

export class BBMModel {
    constructor(
        public no_bbm?: string,
        public tgl_bbm?: Date,
        public iduser?: string,
        public no_sj?: string,
        public tgl_sj_supplier?: Date,
        public qty?: number,
        public kd_bahan?: string,
        public nmbahan?: string,
        public satuan?: string,
        public keterangan?: string
    ) {

    }
}
