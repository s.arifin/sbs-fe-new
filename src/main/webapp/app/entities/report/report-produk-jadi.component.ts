import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper, Principal, Account } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { MasterProduk, MasterStokProduk } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, LazyLoadEvent, DataTable } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
@Component({
    selector: 'jhi-report-produk-jadi',
    templateUrl: './report-produk-jadi.component.html'
})

export class ReportProdukJadiComponent implements OnInit {
    // @ViewChild('table') table: DataTable;
    page: any;
    itemsPerPage: any;
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    kdSPL: string;
    selectedStatus: string;
    selectedGroup: any;
    first: any;
    selectedPIC: string;
    isFilter: number;
    routeData: any;
    previousPage: any;
    qtyPO: any;
    statuses: Array<object> = [
        { label: 'CRT', value: 'CRT' },
        { label: 'PCS', value: 'PCS' },
    ];
    selectedKd: any;
    newBarang: any;
    selectedType: number;
    filteredBarang: MasterProduk[];
    datas: MasterStokProduk[];
    user: any;
    currentAccount: Account;

    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private excelService: ExcelService,
        private principal: Principal,

    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data.pagingParams.page;
        //     this.previousPage = data.pagingParams.page;
        //     this.reverse = data.pagingParams.ascending;
        //     this.predicate = data.pagingParams.predicate;
        // });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
        this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
        this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.isFilter = 0;
        this.selectedStatus = 'CRT';
        this.selectedPIC = '';
        this.selectedType = 0;
        this.selectedKd = '';
        this.datas = new Array<MasterStokProduk>();

    }
    ngOnInit(): void {
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
        });
    }
    loadAll() {
        const waktu = '23:59:59';
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        this.user = this.principal.getUserLogin().toLowerCase();
        console.log(this.user);
        if (this.isFilter === 0) {

        } else {
            this.loadingService.loadingStart();
            this.reportService.getStockProduk({
                page: this.page,
                size: this.itemsPerPage,
                filter: 'thru:' + sampai +
                        '|jenis:' + this.selectedStatus +
                        '|produk:' + this.selectedKd +
                        '|group:' + this.selectedGroup
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
        }
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.datas = data;
        this.loadingService.loadingStop();
    };
    private onSuccessDetail(data, headers) {
        // // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItemsDetail = headers.get('X-Total-Count');
        // this.queryCountDetail = this.totalItemsDetail;
        // this.details = data;
        // this.alkumulasi_po = this.details.reduce((partialSum, a) => partialSum + a.qtybbm, 0);
        // this.loadingService.loadingStop();
    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }
    transition() {
        this.router.navigate(['/report-produk-jadi'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        // this.loadAll();
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    filterBarangSingle(event) {
        const query = event.query;
        this.reportService.getProduk().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: MasterProduk[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nm_produk.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kd_produk.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            } else if (newBarang.kd_group.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kd_produk;
        this.selectedGroup = this.newBarang.kd_group;
    }

    filter() {
        this.isFilter = 1;
        this.loadAll()
    }

    reset() {
        this.isFilter = 0;
        this.selectedKd = '';
        this.selectedGroup = '';
        this.dtthru = new Date();
        this.newBarang = new MasterProduk();
        this.datas = null;
        this.selectedStatus = 'CRT';
        this.loadAll();
    }

    export() {
        this.loadingService.loadingStart();
        if (this.selectedKd === 'undefined' || this.selectedKd === null || this.selectedKd === '') {
           const produk = 'all';
           const group = '';
           const filter_data = 'iduser:' + this.user + '|produk:' + produk + '|kd_group:' + group;
           this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_fg/xlsx', { filterData: filter_data });
            // alert(filter_data);
        } else {
            const produk = this.selectedKd;
            const group = this.selectedGroup;
            const filter_data = 'iduser:' + this.user + '|produk:' + produk + '|kd_group:' + group;
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_fg/xlsx', { filterData: filter_data });
            // alert(filter_data);
        }
        this.loadingService.loadingStop();
    }

}
