import { Component, OnInit, OnDestroy } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { AccountService } from '../../shared/auth/account.service';
import { ItemProduct, PurchasingService } from '../purchasing';
import { ReportService } from './report.service';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-report-stok-fg',
    templateUrl: './report-stok-fg.component.html'
})
export class ReportStokFGComponent implements OnInit {
    commontUtilService: any;
    internals: Internal[];
    internal: Internal;
    modalPilihCeteakn: boolean;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string
    divitions: Array<Object>;
    selectedDIV: string;
    filterDiv: boolean;
    account: Account;
    us_divisi: any;
    date: Date;
    user: any;
    rofo1: string;
    rofo2: string;
    rofo3: string;
    selectBulan: any;
    kd_produk: any;
    newBarang: any;
    selectedType: number;
    types = [
        { label: 'Semua', value: 0 },
        { label: 'Bahan Baku', value: 1 },
        { label: 'Bahan Pengemas', value: 2 },
        { label: 'Lain-Lain', value: 3 }
        // { label: 'Sparepart IDR', value: 9 },
    ];
    barangs: Array<object> = [
        { label: 'Pilih Produk', value: '' },
        { label: 'All Product', value: 'all' }
    ];

    filteredBarang: ItemProduct[];
    bulans: Array<object> = [
        { label: 'ROFO Awal', value: '1' },
        { label: 'ROFO Revisi 1', value: '2' },
        { label: 'ROFO Revisi 2', value: '3' },
        { label: 'ROFO Revisi 3', value: '4' },
        { label: 'ROFO Revisi 4', value: '5' },
        { label: 'ROFO Revisi 5', value: '6' },
        { label: 'ROFO Terakhir', value: '7' },
        { label: 'Keseluruhan Revisi ROFO', value: '8' },
    ];
    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected userService: UserService,
        protected accountService: AccountService,
        private purchasingService: PurchasingService,
        private reportService: ReportService,
        private alertService: JhiAlertService,

    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.selectedType = 0;
        this.date = new Date();
        this.rofo1 = '';
        this.rofo2 = '';
        this.rofo3 = '';
        this.selectBulan = '';
        this.modalPilihCeteakn = false;

    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.us_divisi = this.account.idInternal;
        });

        this.reportService.getProduk1({
            page: 0,
            size: 10000,
            sort: ['kd_group', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.barangs.push({
                    label: element.kd_group + ' ' + element.nm_produk,
                    value: element.kd_produk
                });
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

    }
    print() {
        if (this.selectBulan === '' || this.selectBulan === undefined || this.selectBulan === null) {
            alert('Pilih Data ROFOnya');
        } else {
            const dari = this.date.getFullYear() + '-' + (this.date.getMonth() + 1) + '-' + this.date.getDate();
            this.user = this.principal.getUserLogin().toLowerCase();
            this.loadingService.loadingStart();
            this.reportService.getStokFG({
                filter: 'from:' + dari
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.rofo1 = res.json.rofo1.replace(' ', '_');
                    this.rofo2 = res.json.rofo2.replace(' ', '_');
                    this.rofo3 = res.json.rofo3.replace(' ', '_');
                    if (this.selectBulan === '8') {
                        this.filter_data = 'iduser:' + this.user + '|rofo1:' + this.rofo1 + '|rofo2:' + this.rofo2 + '|rofo3:' + this.rofo3 + '|idrofo:' + this.selectBulan;
                        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/report_stok_fg_new1/pdf', { filterData: this.filter_data }).
                            subscribe((response: any) => {
                                const reader = new FileReader();
                                reader.readAsDataURL(response.blob());
                                this.isview = true;
                                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                                reader.onloadend = (e) => {
                                    this.pdfSrc = reader.result;
                                };
                                this.loadingService.loadingStop();
                            });
                    } else {
                        this.filter_data = 'iduser:' + this.user + '|rofo1:' + this.rofo1 + '|rofo2:' + this.rofo2 + '|rofo3:' + this.rofo3 + '|idrofo:' + this.selectBulan;
                        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/report_stok_fg_new_single1/pdf', { filterData: this.filter_data }).
                            subscribe((response: any) => {
                                const reader = new FileReader();
                                reader.readAsDataURL(response.blob());
                                this.isview = true;
                                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                                reader.onloadend = (e) => {
                                    this.pdfSrc = reader.result;
                                };
                                this.loadingService.loadingStop();
                            });
                    }

                }
            );

        }

    }

    private onSuccess(data, headers) {

        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    exportExcel() {
        // if (this.us_divisi === 'PPIC') {
        //     this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|divisi:' + this.account.idInternal + '|kdbahan:' + this.selectedKd + '|jenis:' + this.selectedType;
        if (this.selectBulan === '8') {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_stok_fg_new1/xls', { filterData: this.filter_data });
        } else {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_stok_fg_new_single/xls', { filterData: this.filter_data });
        }
        // } else {
        //     this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|divisi:' + this.account.idInternal;
        //     this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_permintaan_pembelian/xlsx', { filterData: this.filter_data });
        // }
    }
    backMainPage() {
        this.isview = false;
        this.pdfSrc = '';
    }
    public printReport() {
        this.modalPilihCeteakn = true;
    }

    printExcel() {
        this.filter_data = 'iduser:' + this.user + '|rofo1:' + this.rofo1 + '|rofo2:' + this.rofo2 + '|rofo3:' + this.rofo3 + '|idrofo:' + this.selectBulan;
        this.loadingService.loadingStart()
        if (this.selectBulan === '8') {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_stok_fg_new1/xls', { filterData: this.filter_data });

        } else {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_stok_fg_new_single1/xls', { filterData: this.filter_data });
        }
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

    printPDF() {
        this.loadingService.loadingStart()
        if (this.selectBulan === '8') {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_stok_fg_new1/pdf', { filterData: this.filter_data });

        } else {
            this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_stok_fg_new_single1/pdf', { filterData: this.filter_data });
        }
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

}
