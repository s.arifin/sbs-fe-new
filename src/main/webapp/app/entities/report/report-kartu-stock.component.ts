import { Component, OnInit, ViewChild } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BbmPrint, ReportPenerimaanBarang, ReportStockBarang } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, DataTable, LazyLoadEvent } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from './excel.service';
import { ItemProduct, PurchasingService } from '../purchasing';

@Component({
    selector: 'jhi-report-kartu-stock',
    templateUrl: './report-kartu-stock.component.html'
})

export class ReportKartuStockComponent implements OnInit {
    @ViewChild('table') table: DataTable;
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    datas: ReportStockBarang[];
    details: ReportStockBarang[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    newModalDetail: boolean;
    totalItemsDetail: any;
    queryCountDetail: any;
    dtfrom: Date;
    dtthru: Date;
    selectedKd: any;
    newBarang: any;
    selectedType: any;
    selectedBatch: any;
    internal: any;
    types = [
        { label: 'ALL', value: 'all' },
        { label: 'GM', value: 'L' },
        { label: 'PM', value: 'P' },
        { label: 'RM', value: 'M' },
    ];
    batchs = [];
    iduser: any;
    filteredBarang: ItemProduct[];
    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private excelService: ExcelService,
        private purchasingService: PurchasingService,
        private principal: Principal,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array<Pobbmlov>();
        this.datas = new Array<ReportPenerimaanBarang>();
        this.details = new Array<ReportPenerimaanBarang>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.selectedType = 'all';
        this.selectedKd = '';
        this.newModalDetail = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selectedBatch = '';
        this.iduser = '';
    }
    ngOnInit(): void {
        // this.loadAll();
    }
    loadAll() {
        this.internal = this.principal.getIdInternal();
        this.iduser = this.principal.getUserLogin();
        this.isview = false;
        this.pdfSrc = '';
        if (this.selectedKd === null || this.selectedKd === undefined || this.selectedKd === '') {
            alert('Kode Barang Harus Di Pilih');
        } else if ((this.dtfrom === null || this.dtfrom === undefined) || (this.dtthru === undefined || this.dtthru === null)) {
            alert('Tanggal Harus di Pilih');
        } else {
            const waktu = '23:59:59';
            const dari = this.dtfrom + '-01';
            const sampai = this.dtthru;
            this.loadingService.loadingStart();
            this.reportService.getStockData({
                page: this.page,
                size: this.itemsPerPage,
                filter: 'dtfrom:' + dari +
                    '|dtthru:' + sampai +
                    '|kdbahan:' + this.selectedKd +
                    '|batch:' + this.selectedBatch +
                    '|tpbahan:' + this.selectedType
            }).subscribe(
                (res: ResponseWrapper) => {
                    const filter_data = 'dtfrom:' + this.dtfrom + '-01' + '|dtthru:' + this.dtthru + '|iduser:' + this.iduser;
                    this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/kartu_stock/pdf', { filterData: filter_data }).
                        subscribe((response: any) => {
                            const reader = new FileReader();
                            reader.readAsDataURL(response.blob());
                            this.isview = true;
                            // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                            reader.onloadend = (e) => {
                                this.pdfSrc = reader.result;
                            };
                            this.loadingService.loadingStop();
                        });
                },
                (res: ResponseWrapper) => {
                    this.onError(res.json);
                    this.loadingService.loadingStop()
                }
            );
            return;
        }

    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.datas = data;
        this.loadingService.loadingStop();

    };
    private onSuccessDetail(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsDetail = headers.get('X-Total-Count');
        this.queryCountDetail = this.totalItemsDetail;
        this.details = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        // this.loadAll();
    }
    clear() {
        this.page = 0;
        this.newBarang = '';
        this.selectedType = 'all';
        this.dtthru = new Date();
        this.dtfrom = new Date();
        this.batchs = [];
        this.selectedKd = '';
        this.isview = false;
        this.pdfSrc = '';
        this.selectedBatch = '';
        // this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    lihatDetail(data: ReportStockBarang) {
        const nopo = data.nopurchasing.replace(/\//g, '-');
        this.reportService.getStockBarangDetail(nopo)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessDetail(res.json, res.headers); this.loadingService.loadingStop();
                    this.newModalDetail = true;
                },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
    }

    filter() {
        this.loadAll();
    }
    public export() {
        // const filter_data = 'kdbahan:' + this.selectedKd + '|jenis:' + this.selectedType + '|divisi:' + this.internal;
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/rpt_stock/xlsx', { filterData: filter_data });
        const filter_data = 'dtfrom:' + this.dtfrom + '-01' + '|dtthru:' + this.dtthru + '|iduser:' + this.iduser;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/kartu_stock/xlsx', { filterData: filter_data });
    }
    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }

    getData(date1: any, date2: any, kdbarang: any) {
        let tgl1 = '';
        let tgl2 = '';
        if (date1 === null || date1 === undefined) {
            const tgl_sekarang = new Date();
            tgl1 = tgl_sekarang.getFullYear() + '-' + (tgl_sekarang.getMonth() + 1) + '-' + '01';
        } else {
            tgl1 = date1 + '-01';
        }

        if (date2 === null || date2 === undefined) {
            const tgl_sekarang = new Date();
            tgl2 = tgl_sekarang.getFullYear() + '-' + (tgl_sekarang.getMonth() + 1) + '-' + tgl_sekarang.getDay();
        } else {
            tgl2 = date2;
        }
        if (this.batchs.length > 0) {
            this.batchs = [];
            this.reportService.getDataBatch({
                page: this.page,
                size: this.itemsPerPage,
                filter: 'tgl1:' + tgl1 +
                    '|tgl2:' + tgl2 +
                    '|kdbahan:' + kdbarang
            }).subscribe(
                (res: ResponseWrapper) => {
                    const newKdBarang = res.json;
                    newKdBarang.forEach((element) => {
                        this.batchs.push({
                            label: element.kd_produk,
                            value: element.kd_produk
                        });
                    });
                },
            );
        } else {
            this.reportService.getDataBatch({
                page: this.page,
                size: this.itemsPerPage,
                filter: 'tgl1:' + tgl1 +
                    '|tgl2:' + tgl2 +
                    '|kdbahan:' + kdbarang
            }).subscribe(
                (res: ResponseWrapper) => {
                    const newKdBarang = res.json;
                    newKdBarang.forEach((element) => {
                        this.batchs.push({
                            label: element.kd_produk,
                            value: element.kd_produk
                        });
                    });
                },
            );
        }
    }

}
