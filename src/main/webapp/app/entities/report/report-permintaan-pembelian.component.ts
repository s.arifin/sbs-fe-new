import { Component, OnInit, OnDestroy } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { AccountService } from '../../shared/auth/account.service';
import { ItemProduct, PurchasingService } from '../purchasing';

@Component({
    selector: 'jhi-report-permintaan-pembelian',
    templateUrl: './report-permintaan-pembelian.component.html'
})
export class ReportPermintaanPembelianComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string
    divitions: Array<Object>;
    selectedDIV: string;
    filterDiv: boolean;
    account: Account;
    us_divisi: any;

    selectedKd: any;
    newBarang: any;
    selectedType: number;
    types = [
        { label: 'Semua', value: 0 },
        { label: 'Bahan Baku', value: 1 },
        { label: 'Bahan Pengemas', value: 2 },
        { label: 'Lain-Lain', value: 3 }
        // { label: 'Sparepart IDR', value: 9 },
    ];
    filteredBarang: ItemProduct[];

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected userService: UserService,
        protected accountService: AccountService,
        private purchasingService: PurchasingService,

    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.selectedType = 0;
        this.selectedKd = '';
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.us_divisi = this.account.idInternal;
        });
    }
    print(from: Date, thru: Date) {

        this.loadingService.loadingStart()
        if (this.us_divisi === 'PPIC') {
            this.waktu = '23:59:59';
            this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
            this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;
            this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|divisi:' + this.account.idInternal + '|kdbahan:' + this.selectedKd + '|jenis:' + this.selectedType;
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/rpt_permintaan_pembelian_ppic/pdf', { filterData: this.filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    };
                    this.loadingService.loadingStop();
                });
        } else {
            this.waktu = '23:59:59';
            this.dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
            this.sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + this.waktu;
            this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|divisi:' + this.account.idInternal;
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/rpt_permintaan_pembelian/pdf', { filterData: this.filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    };
                    this.loadingService.loadingStop();
                });
        }

    }
    exportExcel() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        if (this.us_divisi === 'PPIC') {
            this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|divisi:' + this.account.idInternal + '|kdbahan:' + this.selectedKd + '|jenis:' + this.selectedType;
            this.reportUtilService.downloadFileWithName(`Report Realisasi PP ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_permintaan_pembelian_ppic/xlsx', { filterData: this.filter_data });
        } else {
            this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|divisi:' + this.account.idInternal;
            this.reportUtilService.downloadFileWithName(`Report Realisasi PP ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_permintaan_pembelian/xlsx', { filterData: this.filter_data });
        }
    }
    backMainPage() {
        this.isview = false;
    }

    getAccount() {
    }

    filterBarangSingle(event) {
        const query = event.query;
        this.purchasingService.getPoProduct(this.us_divisi).then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectedKd = this.newBarang.kdBahan;
    }
}
