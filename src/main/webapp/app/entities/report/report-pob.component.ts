import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';

@Component({
    selector: 'jhi-report-pob',
    templateUrl: './report-pob.component.html'
})
export class ReportPOBComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: any;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];
    public barang: MasterBahan;
    public Barang: MasterBahan[];
    public selectedBarang: any;
    public listBarang = [{label: 'Please Select or Insert', value: null}];
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    pics: Array<object> = [
        { label: 'All', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    statuses: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Approved kepala Bagian', value: '21' },
        { label: 'Register', value: '14' },
        { label: 'Serah Management', value: '24' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Siap PO', value: '25' },
        { label: 'Sudah Di Buat PO', value: '22' },
        { label: 'Siap BBM', value: '26' }
    ];
    selectedPIC: string;
    pic: string;
    selectedStatus: string;
    status: string;

    isSelcBrg: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];
    pilihTipe: any;
    idlocal: any;
    currentAccount: any;
    inputAppPabrik: string;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected loadingService: LoadingService
    ) {
        this.kdbarang = new MasterBahan();
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.pic = '';
        this.selectedPIC = '';
        this.selectedStatus = '';
        this.status = ''
        this.barang = new MasterBahan();
    }

    filterBarangSingle(event) {
        console.log('data Tipe barang : ', this.pilihTipe);
        const query = event.query;
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.getNewBarangTeknik().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        } else {
            this.purchaseOrderService.getNewBarang().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        }
    }

    filterBarang(query, newBarangs: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.barang.idMasBahan = this.newBarang.idMasBahan;
        console.log('idmasbahan : ' + this.newBarang.idMasBahan);
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.newBarang.idMasBahan)
            .subscribe(
                (res: MasterBarang) => {
                    console.log('ini isi bahan = ', res.kd_barang);
                    this.inputCodeName = res.kd_barang;
                    this.inputProdukName = res.nama_barang;
                    this.newKdBarang = res.kd_barang;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    this.isSelcBrg = false;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        } else {
            this.purchaseOrderService.findBarang(this.newBarang.idMasBahan)
            .subscribe(
                (res: MasterBahan) => {
                    console.log('ini isi bahan = ', res.kdBahan);
                    this.inputCodeName = res.kdBahan;
                    this.inputProdukName = res.nmBahan;
                    this.newKdBarang = res.kdBahan;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        }
    }

    ngOnInit() {
        this.internal =  this.principal.getIdInternal();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.idlocal = account.loc;
            console.log('internal : ', this.idlocal)
            if (account.loc === 'pabrik') {
                this.inputAppPabrik = 'SWY';
            }
        });
    }
    print() {
        // this.loadingService.loadingStart()
        this.waktu = '23:59:59.000';
        this.dari = this.tgl1.getFullYear() + '-' + (this.tgl1.getMonth() + 1) + '-' + this.tgl1.getDate();
        this.sampai = this.tgl2.getFullYear() + '-' + (this.tgl2.getMonth() + 1) + '-' + this.tgl2.getDate() + ' ' + this.waktu;
        // this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai +'|kd_bahan:' + this.inputCodeName +'|div:' + this.internal;
        const filter_data = 'dtFrom:' + this.dari + '|dtThru:' + this.sampai + '|kd_bahan:' + this.inputCodeName + '|div:' + this.internal;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/rptPP_PO_BBM/pdf', { filterData: filter_data });
        console.log('nd :', filter_data);
    }
    // exportExcel() {
    //     this.filter_data = 'dtfrom:' + this.dari + '|dtthru:' + this.sampai + '|pic:' + this.selectedPIC;
    //     this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/ppunregister/xlsx', {filterData: this.filter_data});
    // }
    // backMainPage() {
    //     this.isview = false;
    // }
}
