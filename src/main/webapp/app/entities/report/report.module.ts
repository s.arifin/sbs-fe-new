import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    ReportService,
    reportRoute,
    ReportJobOrderComponent,
    ReportPurchasingHistoryComponent,
    ReportPaymentComponent,
    LaporanTanterComponent,
    ReportKartuStockComponent,
    ReportPaymentMKTComponent
} from './';
import { CommonModule } from '@angular/common';
import { ReportResolvePagingParams } from './report.route'
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { BrowserModule } from '@angular/platform-browser';
import {
    CheckboxModule,
    CalendarModule,
    DropdownModule,
    ButtonModule,
    DataTableModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    DataGridModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    AutoCompleteModule,
    MultiSelectModule,
    SelectItem
} from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';

import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { ReportPurchaseRequestComponent } from './report-purchase-request.component';
import { JobordersService } from '../joborders/index';
import { ReportPpVsPoComponent } from './report-ppvspo.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ReportUnregppComponent } from './report-unregpp.component';
import { ReportPOBComponent } from './report-pob.component';
import { PurchasingReportComponent } from './purchasing-report.component';
import { ReportCloseRejectItemComponent } from './report-close-reject-item.component';
import { ReportPOVSPPoComponent } from './report-povspp.component';
import { ExcelService } from './excel.service';
import { RikaReportComponent } from './rika-report.component';
import { ReportBBMKpiComponent } from './report-bbm-kpi.component';
import { ReportPaymentSplComponent } from './report-payemt-spl.component';
import { BbmPrintComponent } from './bbm-print.component';
import { ReportTTComponent } from './report-tt.component';
import { ReportPenerimaanBarangComponent } from './report-penerimaan-barang.component';
import { ReportMutasiBarangComponent } from './report-mutasi-barang.component';
import { ReportStockBarangComponent } from './report-stock-barang.component';
import { ReportMutasiBarang2Component } from './report-mutasi-barang2.component';
import { ReportDaftarSupplierComponent } from './report-daftar-supplier.component';
import { ReportPermintaanPembelianComponent } from './report-permintaan-pembelian.component';
import { ReportCancelPOComponent } from './report-cancel-po.component';
import { ReportPayment2Component } from './report-payment-2.component';
import { ReportProdukJadiComponent } from './report-produk-jadi.component';
import { ReportJobOrderRequestComponent } from './report-job-order-request.component';
import { ReportRealisasiPaymentComponent } from './report-realisasi-payment.component';
import { ReportStokFGComponent } from './report-stok-fg.component';
import { ReportBotolComponent } from './report-botol.component';
import { ReportKalengComponent } from './report-kaleng.component';
import { ReportPengirimanComponent } from './report-pengiriman.component';
import { ReportBBKComponent } from './report-bbk.component';
import { ReportHasilProduksiComponent } from './report-hasil-produksi.component';
const ENTITY_STATES = [
    ...reportRoute,
];

@NgModule({
    imports: [
        BrowserModule,
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule,
        PdfViewerModule,
        AutoCompleteModule,
        MultiSelectModule,
    ],
    exports: [
    ],
    declarations: [
        ReportPurchaseRequestComponent,
        ReportJobOrderComponent,
        ReportPpVsPoComponent,
        ReportPurchasingHistoryComponent,
        ReportUnregppComponent,
        ReportPOBComponent,
        PurchasingReportComponent,
        ReportCloseRejectItemComponent,
        ReportPOVSPPoComponent,
        ReportPaymentComponent,
        RikaReportComponent,
        ReportBBMKpiComponent,
        ReportPaymentSplComponent,
        BbmPrintComponent,
        LaporanTanterComponent,
        ReportTTComponent,
        ReportPenerimaanBarangComponent,
        ReportMutasiBarangComponent,
        ReportStockBarangComponent,
        ReportMutasiBarang2Component,
        ReportDaftarSupplierComponent,
        ReportPermintaanPembelianComponent,
        ReportCancelPOComponent,
        ReportPayment2Component,
        ReportProdukJadiComponent,
        ReportJobOrderRequestComponent,
        ReportRealisasiPaymentComponent,
        ReportStokFGComponent,
        ReportBotolComponent,
        ReportKalengComponent,
        ReportPengirimanComponent, ReportBBKComponent, ReportHasilProduksiComponent, ReportKartuStockComponent, ReportPaymentMKTComponent

    ],
    entryComponents: [
        ReportPurchaseRequestComponent,
        ReportJobOrderComponent,
        ReportPpVsPoComponent,
        ReportPurchasingHistoryComponent,
        ReportCloseRejectItemComponent,
        ReportPOVSPPoComponent,
        ReportPaymentComponent,
        RikaReportComponent,
        ReportBBMKpiComponent,
        ReportPaymentSplComponent,
        BbmPrintComponent,
        LaporanTanterComponent,
        ReportTTComponent,
        ReportPenerimaanBarangComponent,
        ReportMutasiBarangComponent,
        ReportStockBarangComponent,
        ReportMutasiBarangComponent,
        ReportDaftarSupplierComponent,
        ReportPermintaanPembelianComponent,
        ReportCancelPOComponent,
        ReportPayment2Component,
        ReportProdukJadiComponent,
        ReportJobOrderRequestComponent,
        ReportRealisasiPaymentComponent,
        ReportStokFGComponent,
        ReportBotolComponent,
        ReportKalengComponent,
        ReportPengirimanComponent, ReportBBKComponent, ReportHasilProduksiComponent, ReportKartuStockComponent, ReportPaymentMKTComponent

    ],
    providers: [
        ReportService,
        PurchaseOrderService,
        JobordersService,
        ExcelService,
        // ReportResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmReportModule { }
