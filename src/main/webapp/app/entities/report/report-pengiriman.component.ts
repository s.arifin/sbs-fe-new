import { Component, OnInit, OnDestroy } from '@angular/core';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { LoadingService } from '../../layouts';
import { AccountService } from '../../shared/auth/account.service';
import { ItemProduct, PurchasingService } from '../purchasing';
import { ReportService } from './report.service';
import { JhiAlertService } from 'ng-jhipster';
import { RofoService } from '../rofo';
import { MasterProduk } from './report.model';
import { MultiSelectModule } from 'primeng/primeng';

@Component({
    selector: 'jhi-report-pengiriman',
    templateUrl: './report-pengiriman.component.html'
})
export class ReportPengirimanComponent implements OnInit {
    commontUtilService: any;
    internals: Internal[];
    internal: Internal;
    newProjs: any[];
    newProj: any;
    newProj1: any;
    listAPP = [{}];
    listAPP1 = [{}];
    idinternal: any;
    isview: boolean;
    filter_data: string;
    waktu: string;
    dari: string;
    sampai: string;
    pdfSrc: any;
    account: Account;
    us_divisi: any;
    date: Date;
    user: any;
    pilihKdGroup: any;
    kd_produk: any;
    newBarang: any;
    selectBulan: any;
    filteredBarang: ItemProduct[];
    isproduk: boolean;
    produkSelect: MasterProduk[] = [];
    modalPilihCeteakn: boolean;

    constructor(
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected userService: UserService,
        protected accountService: AccountService,
        private reportService: ReportService,
        private alertService: JhiAlertService,

    ) {
        this.isview = false;
        this.filter_data = '';
        this.waktu = '';
        this.dari = '';
        this.sampai = '';
        this.date = new Date();
        this.selectBulan = '';
        this.isproduk = false;
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.us_divisi = this.account.idInternal;
        });

        this.reportService.getKdGroup({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listAPP.push({
                    label: element.kd_group + ' - ' + element.nm_group,
                    value: element.kd_group
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
        this.listAPP.shift();
    }
    print() {
        if (this.selectBulan === '' && this.isproduk === false) {
            alert('Pilih Group Produk Terlebih Dahulu')
        } else if (this.selectBulan === '1' && this.isproduk === false) {
            const nama = this.principal.getUserLogin();
            const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
            const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
            const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
            const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
            this.loadingService.loadingStart();
            this.reportService.getPengirimanData({
                filter: 'from:' + dtFrom +
                    '|thru:' + dtThru +
                    '|cari:' + this.produkSelect +
                    '|isalone:' + 'Y'
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.filter_data = 'dtFrom:' + dtFrom + '|iduser:' + nama;
                    this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/ReportPengiriman/pdf', { filterData: this.filter_data }).
                        subscribe((response: any) => {
                            const reader = new FileReader();
                            reader.readAsDataURL(response.blob());
                            this.isview = true;
                            // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                            reader.onloadend = (e) => {
                                this.pdfSrc = reader.result;
                            };
                            this.loadingService.loadingStop();
                        });
                }
            );
        } else if (this.selectBulan !== '1' && this.isproduk === true) {
            if (this.produkSelect.length < 1) {
                alert('Produk Harus Di Pilih')
            } else {
                const nama = this.principal.getUserLogin();
                const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
                const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
                const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
                const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
                this.loadingService.loadingStart();
                this.reportService.getPengirimanData({
                    filter: 'from:' + dtFrom +
                        '|thru:' + dtThru +
                        '|cari:' + this.produkSelect +
                        '|isalone:' + 'N'
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.filter_data = 'dtFrom:' + dtFrom + '|iduser:' + nama;
                        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/ReportPengiriman/pdf', { filterData: this.filter_data }).
                            subscribe((response: any) => {
                                const reader = new FileReader();
                                reader.readAsDataURL(response.blob());
                                this.isview = true;
                                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                                reader.onloadend = (e) => {
                                    this.pdfSrc = reader.result;
                                };
                                this.loadingService.loadingStop();
                            });
                    }
                );
            }
        }
    }

    private onSuccess(data, headers) {

        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    exportExcel() {
        const nama = this.principal.getUserLogin();
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.filter_data = 'dtFrom:' + dtFrom + '|iduser:' + nama;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/ReportPengiriman/xlsx', { filterData: this.filter_data });
    }
    backMainPage() {
        this.isview = false;
        this.pdfSrc = '';
    }

    onChangeDate() {
        this.listAPP1 = [{}];
        this.produkSelect = new Array<MasterProduk>();
        if (this.selectBulan === '') {
            this.isproduk = false;
        } else if (this.selectBulan === '1') {
            this.isproduk = false;
        } else {
            this.isproduk = true;
            this.reportService.getProdukByID({
                kd_group: this.selectBulan
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.newProj1 = res.json;
                    this.newProj1.forEach((element) => {
                        this.listAPP1.push({
                            label: element.nm_produk,
                            value: element.kd_produk
                        });
                    });
                })
            this.listAPP1.shift();

        }
    }

    public printReport() {
        this.modalPilihCeteakn = true;
    }

    printExcel() {
        this.loadingService.loadingStart()
        const nama = this.principal.getUserLogin();
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.filter_data = 'dtFrom:' + dtFrom + '|iduser:' + nama;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/ReportPengiriman/xlsx', { filterData: this.filter_data });
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

    printPDF() {
        this.loadingService.loadingStart()
        const nama = this.principal.getUserLogin();
        const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
        const dtFrom = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getDate();
        const dtThru = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getDate();
        this.filter_data = 'dtFrom:' + dtFrom + '|iduser:' + nama;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/ReportPengiriman/pdf', { filterData: this.filter_data });
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;

    }

}
