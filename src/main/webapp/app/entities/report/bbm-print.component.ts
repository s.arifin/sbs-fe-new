import { Component, OnInit } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts';
import { ITEMS_PER_PAGE, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { InvoiceService, Pobbmlov } from '../invoice';
import { BbmPrint } from './report.model';
import { ReportService } from './report.service';
import * as FileSaver from 'file-saver';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-bbm-print',
    templateUrl: './bbm-print.component.html'
})

export class BbmPrintComponent implements OnInit {
    page: any;
    itemsPerPage: any;
    selected: BbmPrint[];
    pobbm: BbmPrint;
    pobbms: BbmPrint[];
    totalItems: any;
    queryCount: any;
    isview: boolean;
    pdfBlob: string;
    pdfSrc: any;
    predicate: any;
    reverse: any;
    currentSearch: string;
    constructor(
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
        private alertService: JhiAlertService,
        private reportService: ReportService,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array<Pobbmlov>();
        this.pobbm = new Pobbmlov();
        this.pobbms = new Array<Pobbmlov>();
        this.totalItems = 0;
        this.queryCount = 0;
        this.isview = false;
        this.pdfSrc = '';
        this.pdfBlob = '';
    }
    ngOnInit(): void {
        this.loadAll();
    }
    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.reportService.getBBMPrint({
                query: 'nobbm:' + this.currentSearch,
                page: this.page,
                size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
            );
            return;
        }
        this.reportService.getBBMPrint({
            query: 'nobbm:',
            page: this.page,
            size: this.itemsPerPage,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
            (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
        );
        this.selected = new Array<Pobbmlov>();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.pobbms = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    openPdf(rowData: BbmPrint) {
        this.pobbm = rowData;
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.loadingService.loadingStart();
        let bbms: string;
        bbms = rowData.nobbm.replace(/\//g, '-');
        if (rowData.div === 'nontek') {
            const pecah = bbms.split('-');
            const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString() +
                '|awallhp:' + pecah[0].toString() + '|tengahlhp:' + pecah[1].toString() + '|akhirlhp:' + pecah[2].toString();
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/bbm_non_tek/pdf', { filterData: filter_data })
                .subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // tslint:disable-next-line: space-before-function-paren
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                },
                    (err) => {
                        this.loadingService.loadingStop();
                    });
        }
        if (rowData.div === 'tek') {
            const pecah = bbms.split('-');
            let lhp: string;
            lhp = rowData.nolhp.replace(/\//g, '-');
            const pecahlhp = lhp.split('-');
            const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString() +
                '|awallhp:' + pecahlhp[0].toString() + '|tengahlhp:' + pecahlhp[1].toString() + '|akhirlhp:' + pecahlhp[2].toString();
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/bbm_tek/pdf', { filterData: filter_data })
                .subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // tslint:disable-next-line: space-before-function-paren
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                },
                    (err) => {
                        this.loadingService.loadingStop();
                    });
        }
        this.loadingService.loadingStop();
    }

    backMainPage() {
        this.isview = false;
    }

    print() {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Cetak Dokumen ? \r\nDokumen hanya bisa di cetak satu kali !!',
            accept: () => {
                this.reportService.setTotalPrint(this.pobbm).subscribe(
                    (res) => {
                        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                        const win = window.open(this.pdfSrc);
                        win.document.write('<iframe id="printf" src="' + this.pdfSrc + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
                    }
                )
            }
        });
    }

    downloadBlob() {
        FileSaver.saveAs(this.pdfBlob, this.pobbm.nobbm);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

}
