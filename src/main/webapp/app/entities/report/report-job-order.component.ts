import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { MasterBahan, MasterBarang } from '../purchase-order/purchase-order.model';
import { PurchaseOrderService } from '../purchase-order/purchase-order.service';
import { JobordersService } from '../joborders/joborders.service';

@Component({
    selector: 'jhi-report-job-order',
    templateUrl: './report-job-order.component.html'
})
export class ReportJobOrderComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    commontUtilService: any;
    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: Internal;

    idinternal: any;
    newKdBarang: any;
    newKdBarangs: any[];
    inputCodeName: string;
    inputProdukName: string;
    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{label: 'Please Select or Insert', value: null}];
    inputDivReciver: any;
    inptSeksie: any;

    divitions: Array<object> = [
        {label : 'ALL', value : 'ALL'},
        {label : 'PPIC', value : 'PPIC'},
        {label : 'PROD', value : 'PROD'},
        {label : 'LPM', value : 'LPM'},
        {label : 'PURCHASING', value : 'PURCHASING'},
        {label : 'RND', value : 'RND'},
        {label : 'TEKNIK', value : 'TEKNIK'},
        {label : 'MARKETING', value : 'MARKETING'},
        {label : 'FNA', value : 'FNA'},
        {label : 'PSU', value : 'PSU'},
        {label : 'IT', value : 'IT'},
        {label : 'HSE', value : 'HSE'}

    ];

    constructor(
        protected reportUtilService: ReportUtilService,
        protected purchaseOrderService: PurchaseOrderService,
        protected jobOrderService: JobordersService,
        private principal: Principal,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
    ) {
        this.kdbarang = new MasterBahan();
    }

    ngOnInit() {
        // this.organizationService.query()
        //     .subscribe((res: ResponseWrapper) => { this.organizations = res.json});
        //     console.log('nc :', this.organization);

        // this.internalService.query({size: 999999})
        // .subscribe((res: ResponseWrapper) => {
        //     this.internals = res.json;
        //     console.log('internal :', this.internals)
        // });
        this.idinternal =  this.principal.getIdInternal();
        console.log('internal : ', this.internal)
        // if (this.idinternal === 'TEKNIK') {
        //     this.purchaseOrderService.getAllDropdownTeknik({
        //         page: 0,
        //         size: 10000,
        //         sort: ['kdBahan', 'asc']
        //     }).subscribe((res: ResponseWrapper) => {
        //         this.newKdBarang = res.json;
        //         this.newKdBarang.forEach((element) => {
        //             this.listKdBarang.push({
        //                 label: element.kdBahan + ' - ' + element.nmBahan,
        //                 value: element.idMasBahan });
        //         });
        //     },
        //         (res: ResponseWrapper) => {
        //             this.commontUtilService.showError(res.json);
        //         }
        //     );
        // } else {
        //     this.purchaseOrderService.getAllDropdown({
        //         page: 0,
        //         size: 10000,
        //         sort: ['kdBahan', 'asc']
        //     }).subscribe((res: ResponseWrapper) => {
        //         this.newKdBarang = res.json;
        //         this.newKdBarang.forEach((element) => {
        //             this.listKdBarang.push({
        //                 label: element.kdBahan + ' - ' + element.nmBahan,
        //                 value: element.idMasBahan });
        //         });
        //     },
        //         (res: ResponseWrapper) => {
        //             this.commontUtilService.showError(res.json);
        //         }
        //     );
        // }
        this.jobOrderService.getAllSeksie({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.seksie + ' - ' + element.initial,
                    value: element.initial });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }

    print(from: Date, thru: Date, nd: any, ns: any) {
        const waktu = '23:59:59.000';
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        const filter_data = 'dtFrom:' + dari + '|dtThru:' + sampai + '|div:' + this.idinternal + '|divreq:' + nd + '|sek:' + ns;
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_buku_tamu/xlsx', { filterData: filter_data });
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rptJO_new/xlsx', { filterData: filter_data });
        console.log('nd :', filter_data);
    }

    // print(from: Date, thru: Date, nd: Internal) {
    //     const filter_data = 'kdBarang:' + this.inputCodeName;
    //     // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/report_buku_tamu/xlsx', { filterData: filter_data });
    //     this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/rptPP/pdf', { filterData: filter_data });
    //     console.log('nd :', nd);
    // }

    // public selectKdBarang(isSelect?: boolean): void {
    //     console.log('idmasbahankd : ' + this.selectedKdBarang);
    //     this.kdbarang.idMasBahan = this.selectedKdBarang;
    //     if (this.principal.getIdInternal() === 'TEKNIK') {
    //         this.purchaseOrderService.findBarangTeknik(this.selectedKdBarang)
    //         .subscribe(
    //             (res: MasterBarang) => {
    //                 console.log('ini isi bahan = ', res.kd_barang);
    //                 this.inputCodeName = res.kd_barang;
    //                 this.inputProdukName = res.nama_barang;
    //             },
    //             (res: MasterBahan) => {
    //                 this.inputCodeName = null;
    //                 console.log('ini isi bahan eror = ', this.inputCodeName);
    //             }
    //         )
    //     } else {
    //         this.purchaseOrderService.findBarang(this.selectedKdBarang)
    //         .subscribe(
    //             (res: MasterBahan) => {
    //                 console.log('ini isi bahan = ', res.kdBahan);
    //                 this.inputCodeName = res.kdBahan;
    //                 this.inputProdukName = res.nmBahan;
    //             },
    //             (res: MasterBahan) => {
    //                 this.inputCodeName = null;
    //                 console.log('ini isi bahan eror = ', this.inputCodeName);
    //             }
    //         )
    //     }
    // }

    // trackorganizationById(index: number, item: Organization) {
    //     return item.idParty;
    // }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
