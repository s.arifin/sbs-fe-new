import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { createRequestOptionPurchasingPTO } from '../purchasing/purchasing-parameter.util';
import { BbmPrint } from './report.model';

@Injectable()
export class ReportService {
    private resourceUrl = process.env.API_C_URL + '/api/reports';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    getHistPurchasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/get-view-history-po', options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }
    getRptPurchasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/get-purchasing-report', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPoProductCode() {
        return this.http.get(this.resourceUrl + '/po-product-code-new').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getBBMKPI(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/bbm-kpi', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getBBMPrint(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/bbm-print', options)
            .map((res: Response) => this.convertResponse(res));
    }
    setTotalPrint(bbmprint: BbmPrint): Observable<BbmPrint> {
        const copy = this.convert(bbmprint);
        return this.http.post(this.resourceUrl + '/bbm-print', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    private convert(bbmprint: BbmPrint): BbmPrint {
        const copy: BbmPrint = Object.assign({}, bbmprint);
        return copy;
    }
    getPenerimaanBarang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/penerimaan-barang', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getPenerimaanBarangDetailMutasi(nopo: string, kdbahan: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/penerimaan-barang/' + nopo + '/' + kdbahan)
            .map((res: Response) => this.convertResponse(res));
    }

    getPenerimaanBarangDetail1(nopo: string, kdbahan: string, divisi: string, idpo: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/penerimaan-barang/' + nopo + '/' + kdbahan + '/' + divisi + '/' + idpo)
            .map((res: Response) => this.convertResponse(res));
    }

    getPenerimaanBarangDetail2(nopo: string, kdbahan: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/penerimaan-barang/' + nopo + '/' + kdbahan)
            .map((res: Response) => this.convertResponse(res));
    }

    getQtyItem(nopo: string, kdbahan: string, idpo: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/penerimaan-barang1/' + nopo + '/' + kdbahan + '/' + idpo)
            .map((res: Response) => this.convertResponse(res));
    }
    // getMutasiBarang(req?: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(req);
    //     return this.http.get(this.resourceUrl + '/mutasi-barang', options)
    //         .map((res: Response) => this.convertResponse(res));
    // }
    getMutasiBarang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(process.env.API_C_URL + '/api/report-mutasi-bahan', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getMutasiBarang2(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/mutasi-barang2', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getStokFG(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/report-fg', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getHasilProduksi(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/report-hasil-produksi', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPengirimanData(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/report-pengiriman', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getProduk1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallproduk', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getStockBarang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/stock-barang', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getStockData(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/stock-data', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getDataBatch(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-batch', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAutority(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautority', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getStockProduk(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/stock-produk', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getStockBarangDetail(nopo?: string, kdbahan?: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/stock-barang/' + nopo + '/' + kdbahan)
            .map((res: Response) => this.convertResponse(res));
    }

    getBBMSupplier(nopo?: string, kdbahan?: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/get-bbm-bbk/' + nopo + '/' + kdbahan)
            .map((res: Response) => this.convertResponse(res));
    }

    getPenerimaanBarangDetailPO(nopo: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/penerimaan-barangPO/' + nopo)
            .map((res: Response) => this.convertResponse(res));
    }

    getProduk() {
        return this.http.get(this.resourceUrl + '/getProduk').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getKdGroup(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getKdGroup', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getProdukByID(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getProduk', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getBBK(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-bbk', options)
            .map((res: Response) => this.convertResponse(res));
    }
}
