import { Component, Input, OnInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ResponseWrapper } from '../../../shared';
import { ExcelService } from '../../report/excel.service';
import { DashboardKlaim } from '../dashboard.model';

@Component({
    selector: 'jhi-dashboard-klaim',
    templateUrl: './dashboard-klaim.component.html',

})
export class DashboardKlaimComponent implements OnInit {
    @Input() pilihBulan: any
    @Input() pilihTahun: any
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    dataPPPO: DashboardKlaim[];
    rayLSs: any[];
    rayBadaks: any[];
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private excelService: ExcelService,
        private alertService: JhiAlertService,
    ) {
        this.date = new Date();
    }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        this.loadingService.loadingStart();
        this.getAllDist();
        this.loadingService.loadingStop();
    }

    // Semua Distributor
    getAllDist() {
        this.dashboardService.findKlaimDist(this.pilihTahun)
            .subscribe(
                (res: any) => {
                    // this.setPODataPie(res);
                    this.setPODataBarAll(res)
                }
            );
            this.dashboardService.queryReportKlaim({
                query: 'tahun:' + this.pilihTahun
            }).subscribe(
                (res: ResponseWrapper) => {
                    // this.HpaymentReport = JSON.parse(res.headers.get('dataheadervm'));
                    this.onSuccess(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.dashboardService.findKlaimDistBadak(this.pilihTahun, 'BADAK')
            .subscribe(
                (res: any) => {
                    // this.setPODataPie(res);
                    this.setPODataBarBadak(res)
                }
            );
            this.dashboardService.findKlaimDistLasegar(this.pilihTahun, 'LASEGAR')
            .subscribe(
                (res: any) => {
                    // this.setPODataPie(res);
                    this.setPODataBarLasegar(res)
                }
            );
    }

    setPODataBarAll(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarALl', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const div = 'chartdivBarALl';
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulanvel';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'total_surat';
        series.dataFields.categoryX = 'bulanvel';
        series.clustered = false;
        series.tooltipText = 'Surat Klaim : [bold]{valueY}[/]';
        series.name = 'Total Surat Klaim';
        series.columns.template.fill = am4core.color('#3CB371'); // Target Kuning
        series.columns.template.width = am4core.percent(30);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'jml_klaim';
        series2.dataFields.categoryX = 'bulanvel';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah Klaim : [bold]{valueY}[/]';
        series2.name = 'Total Jumlah Klaim';
        series2.columns.template.fill = am4core.color('#0000CD'); // PO Biru
        series2.columns.template.width = am4core.percent(30);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'klm_diproses';
        series3.dataFields.categoryX = 'bulanvel';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'Klaim Diproses : [bold]{valueY}[/]';
        series3.name = 'Total Klaim Diproses';
        series3.columns.template.fill = am4core.color('#48D1CC'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'klm_realisasikan';
        series4.dataFields.categoryX = 'bulanvel';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Klaim Direalisasikan : [bold]{valueY}[/]';
        series4.name = 'Total Klaim Direalisasikan';
        series4.columns.template.fill = am4core.color('#6B8E23'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);
        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        // floating tooltip
        // series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                           <tr>
                                               <th style="text-align:right; background-color:#3CB371"><span style="color:black">{total_surat.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                                <th style="text-align:right; background-color:#0000CD"><span style="color:white">{jml_klaim.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                               <th style="text-align:right; background-color:#48D1CC"><span style="color:black">{klm_diproses.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                               <th style="text-align:right; background-color:#6B8E23"><span style="color:white">{klm_realisasikan.formatNumber("#.")}</span></th>
                                           </tr>
                                       </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.itemContainers.template.paddingBottom = 100;
        chart.legend.labels.template.truncate = true;
        chart.legend.itemContainers.template.tooltipText = '';
        chart.legend.itemContainers.template.clickable = false;
        chart.legend.itemContainers.template.focusable = false;
        chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
        // chart.legend.itemContainers.template.tooltip.disabled = true;

    }

    setPODataBarBadak(data: any) {
         // Create chart instance
         const chart = am4core.create('chartdivBarAllBadak', am4charts.XYChart);
         // Add percent sign to all numbers
         // chart.numberFormatter.numberFormat = '#.#\'%\'';

         // Add data
         chart.data = data;
         // Create axes
         const div = 'chartdivBarAllBadak';
         const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
         categoryAxis.dataFields.category = 'bulanvel';
         categoryAxis.renderer.grid.template.location = 0;
         categoryAxis.renderer.minGridDistance = 10;

         const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
         valueAxis.title.text = 'Jumlah';
         valueAxis.title.fontWeight = '800';
         valueAxis.strictMinMax = true;
         valueAxis.min = 0;

         // Create series
         const series = chart.series.push(new am4charts.ColumnSeries());
         series.dataFields.valueY = 'total_surat';
         series.dataFields.categoryX = 'bulanvel';
         series.clustered = false;
         series.tooltipText = 'Surat Klaim : [bold]{valueY}[/]';
         series.name = 'Total Surat Klaim';
         series.columns.template.fill = am4core.color('#3CB371'); // Target Kuning
         series.columns.template.width = am4core.percent(30);

         const columnTemplate1 = series.columns.template;
         columnTemplate1.strokeWidth = 2;
         columnTemplate1.strokeOpacity = 1;
         columnTemplate1.stroke = am4core.color('#FFFFFF');

         const series2 = chart.series.push(new am4charts.ColumnSeries());
         series2.dataFields.valueY = 'jml_klaim';
         series2.dataFields.categoryX = 'bulanvel';
         series2.clustered = false;
         // series2.columns.template.width = am4core.percent(75);
         series2.tooltipText = 'Jumlah Klaim : [bold]{valueY}[/]';
         series2.name = 'Total Jumlah Klaim';
         series2.columns.template.fill = am4core.color('#0000CD'); // PO Biru
         series2.columns.template.width = am4core.percent(30);

         const columnTemplate2 = series2.columns.template;
         columnTemplate2.strokeWidth = 2;
         columnTemplate2.strokeOpacity = 1;
         columnTemplate2.stroke = am4core.color('#FFFFFF');

         const series3 = chart.series.push(new am4charts.ColumnSeries());
         series3.dataFields.valueY = 'klm_diproses';
         series3.dataFields.categoryX = 'bulanvel';
         series3.clustered = false;
         // series3.columns.template.width = am4core.percent(50);
         series3.tooltipText = 'Klaim Diproses : [bold]{valueY}[/]';
         series3.name = 'Total Klaim Diproses';
         series3.columns.template.fill = am4core.color('#48D1CC'); // pengiriman Hijau
         series3.columns.template.width = am4core.percent(30);

         const columnTemplate3 = series3.columns.template;
         columnTemplate3.strokeWidth = 2;
         columnTemplate3.strokeOpacity = 1;
         columnTemplate3.stroke = am4core.color('#FFFFFF');

         const series4 = chart.series.push(new am4charts.ColumnSeries());
         series4.dataFields.valueY = 'klm_realisasikan';
         series4.dataFields.categoryX = 'bulanvel';
         series4.clustered = false;
         // series4.columns.template.width = am4core.percent(25);
         series4.tooltipText = 'Klaim Direalisasikan : [bold]{valueY}[/]';
         series4.name = 'Total Klaim Direalisasikan';
         series4.columns.template.fill = am4core.color('#6B8E23'); // Pembayaran Hitam
         series4.columns.template.width = am4core.percent(30);
         const columnTemplate4 = series4.columns.template;
         columnTemplate4.strokeWidth = 2;
         columnTemplate4.strokeOpacity = 1;
         columnTemplate4.stroke = am4core.color('#FFFFFF');

         // floating tooltip
         // series.tooltip.label.textAlign = 'end';
         series.tooltip.pointerOrientation = 'down';
         series.tooltip.dy = -5;
         series.tooltip.label.fontSize = 10;
         columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                                <th style="text-align:right; background-color:#3CB371"><span style="color:black">{total_surat.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                 <th style="text-align:right; background-color:#0000CD"><span style="color:white">{jml_klaim.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#48D1CC"><span style="color:black">{klm_diproses.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#6B8E23"><span style="color:white">{klm_realisasikan.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
         columnTemplate1.showTooltipOn = 'always';
         columnTemplate1.tooltipY = 0;
         series.tooltip.getFillFromObject = false;
         series.tooltip.background.fill = am4core.color('#ffffff00');
         series.tooltip.background.stroke = am4core.color('#ffffff00');
         // floating tooltip
         chart.cursor = new am4charts.XYCursor();
         chart.cursor.lineX.disabled = true;
         chart.cursor.lineY.disabled = true;
         // Create LEGEND
         chart.legend = new am4charts.Legend();
         chart.legend.position = 'top';
         chart.legend.itemContainers.template.paddingBottom = 100;
         chart.legend.labels.template.truncate = true;
         chart.legend.itemContainers.template.tooltipText = '';
         chart.legend.itemContainers.template.clickable = false;
         chart.legend.itemContainers.template.focusable = false;
         chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
        //  chart.legend.itemContainers.template.tooltip.disabled = true;
    }

    setPODataBarLasegar(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarAllLasegar', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const div = 'chartdivBarAllLasegar';
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulanvel';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'total_surat';
        series.dataFields.categoryX = 'bulanvel';
        series.clustered = false;
        series.tooltipText = 'Surat Klaim : [bold]{valueY}[/]';
        series.name = 'Total Surat Klaim';
        series.columns.template.fill = am4core.color('#3CB371'); // Target Kuning
        series.columns.template.width = am4core.percent(30);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'jml_klaim';
        series2.dataFields.categoryX = 'bulanvel';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah Klaim : [bold]{valueY}[/]';
        series2.name = 'Total Jumlah Klaim';
        series2.columns.template.fill = am4core.color('#0000CD'); // PO Biru
        series2.columns.template.width = am4core.percent(30);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'klm_diproses';
        series3.dataFields.categoryX = 'bulanvel';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'Klaim Diproses : [bold]{valueY}[/]';
        series3.name = 'Total Klaim Diproses';
        series3.columns.template.fill = am4core.color('#48D1CC'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'klm_realisasikan';
        series4.dataFields.categoryX = 'bulanvel';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Klaim Direalisasikan : [bold]{valueY}[/]';
        series4.name = 'Total Klaim Direalisasikan';
        series4.columns.template.fill = am4core.color('#6B8E23'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);
        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        // floating tooltip
        // series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                           <tr>
                                               <th style="text-align:right; background-color:#3CB371"><span style="color:black">{total_surat.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                                <th style="text-align:right; background-color:#0000CD"><span style="color:white">{jml_klaim.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                               <th style="text-align:right; background-color:#48D1CC"><span style="color:black">{klm_diproses.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                               <th style="text-align:right; background-color:#6B8E23"><span style="color:white">{klm_realisasikan.formatNumber("#.")}</span></th>
                                           </tr>
                                       </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.itemContainers.template.paddingBottom = 100;
        chart.legend.labels.template.truncate = true;
        chart.legend.itemContainers.template.tooltipText = '';
        chart.legend.itemContainers.template.clickable = false;
        chart.legend.itemContainers.template.focusable = false;
        chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
        // chart.legend.itemContainers.template.tooltip.disabled = true;

    }
    // Semua Distributor

    exportExcel() {
        this.excelService.exportAsExcelFile(this.dataPPPO, `Report Klaim ${this.pilihTahun}`);
    }

    private onSuccess(data, headers) {
        this.dataPPPO = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
        this.loadData();
    }

    toPercent() {
        this.router.navigate(['/dashboard-tagetvspo']);
    }

}
