import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { DashboardService } from '../dashboard.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-klaim-tab-dashboard',
    templateUrl: './klaim-tab-dashboard.component.html',

})
export class KlaimTabDashboardComponent implements OnInit {
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    pilihBulan: number;
    tahun: Array<object> = [
        // { label: '2021', value: 2021 },
        // { label: '2022', value: 2022 },
        // { label: '2023', value: 2023 },
        // { label: '2024', value: 2024 }
    ];
    pilihTahun: number;
    rayLSs: any[];
    rayBadaks: any[];
    isPercent: boolean;
    private subscription: Subscription;
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private route: ActivatedRoute,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 1 > 12) {
            this.pilihBulan = this.date.getMonth();
            this.pilihTahun = this.date.getFullYear() + 2;
        } else {
            this.pilihBulan = this.date.getMonth() + 2;
            this.pilihTahun = this.date.getFullYear();
        }
        this.isPercent = true;
        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahun.push({ label: year.toString(), value: year });
        }
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['bulan'] && params['tahun']) {
                this.pilihBulan = params['bulan'];
                this.pilihTahun = params['tahun'];
            }
        });
    }

    loadData() {
    }
    toValue() {
        // this.router.navigate(['/dashboard-tagetvspo']);
        if (this.isPercent === true) {
            this.isPercent = false;
        }
    }
    toPercent() {
        // this.router.navigate(['/dashboard-tagetvspo']);
        if (this.isPercent === false) {
            this.isPercent = true;
        }
    }
    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
    }

}
