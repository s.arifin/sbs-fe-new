import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { DashboardComponent } from './dashboard.component';
import { TabDashboardComponent } from './tab-dashboard.component';
import { DashboardRofoComponent } from './dashboard-rofo.component';
import { TrendTabDashboardComponent } from './dashboard-trend/trend-tab-dashboard.component';
import { PppoTabDashboardComponent } from './dashboard-pp-po/pppo-tab-dashboard.component';
import { KlaimTabDashboardComponent } from './dashboard-klaim/klaim-tab-dashboard.component';
import { DashboardPpPoPercentComponent } from './dashboard-pp-po/dashboard-pppo-percent.component';
import { DashboardPppoComponent } from './dashboard-pp-po/dashboard-pppo.component';
import { TabDashboardProduksiComponent } from './dashboard-produksi/tab-dashboard-produksi.component';

@Injectable()
export class DashboardResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const dashboardRoute: Routes = [
    {
        path: 'dashboard-tagetvspo',
        component: DashboardComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-percent-tagetvspo',
        component: TabDashboardComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-rofo',
        component: DashboardRofoComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-trend',
        component: TrendTabDashboardComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-pp-po',
        component: PppoTabDashboardComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-percent-tagetvspo/:bulan/:tahun',
        component: TabDashboardComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-klaim',
        component: KlaimTabDashboardComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-produksi',
        component: TabDashboardProduksiComponent,
        resolve: {
            'pagingParams': DashboardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dashboard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dashboardPopupRoute: Routes = [
];
