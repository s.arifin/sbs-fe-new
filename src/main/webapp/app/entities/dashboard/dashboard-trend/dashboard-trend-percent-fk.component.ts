import { Component, Input, OnInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { Router } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { DashboardService } from '../dashboard.service';
import { DasboardTrendNotes } from '../dashboard.model';
import { ExcelService } from '../../report/excel.service';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { DashboardTrendP } from '../dashboard.model';
import { ResponseWrapper, Principal } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-dashboard-trend-percent-fk',
    templateUrl: './dashboard-trend-percent-fk.component.html',

})
export class DashboardTrendPercentFkComponent implements OnInit {
    @Input() pilihBulan: any
    @Input() pilihTahun: any
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    notesChart: DasboardTrendNotes;
    // bulan: Array<object> = [
    //     { label: '01', value: 1 },
    //     { label: '02', value: 2 },
    //     { label: '03', value: 3 },
    //     { label: '04', value: 4 },
    //     { label: '05', value: 5 },
    //     { label: '06', value: 6 },
    //     { label: '07', value: 7 },
    //     { label: '08', value: 8 },
    //     { label: '09', value: 9 },
    //     { label: '10', value: 10 },
    //     { label: '11', value: 11 },
    //     { label: '12', value: 12 }
    // ];
    // pilihBulan: number;
    // tahun: Array<object> = [
    //     { label: '2012', value: 2012 },
    //     { label: '2013', value: 2013 },
    //     { label: '2014', value: 2014 },
    //     { label: '2015', value: 2015 },
    //     { label: '2016', value: 2016 },
    //     { label: '2017', value: 2017 },
    //     { label: '2018', value: 2018 },
    //     { label: '2019', value: 2019 },
    //     { label: '2020', value: 2020 },
    //     { label: '2021', value: 2021 },
    //     { label: '2022', value: 2022 },
    //     { label: '2023', value: 2023 }
    // ];
    // pilihTahun: number;
    rayLSs: any[];
    rayBadaks: any[];
    modalTrend: boolean;
    dataBulan: string;
    dataDIV: string;
    dataPPPO: DashboardTrendP[];
    currentAccount: any;
    currentGroupUSers: any;
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private excelService: ExcelService,
        private alertService: JhiAlertService,
        private reportUtilService: ReportUtilService,
        private principal: Principal,
    ) {
        this.date = new Date();
        this.modalTrend = false;
        this.dataBulan = '';
        this.dataDIV = '';
        this.notesChart = new DasboardTrendNotes();
        // if (this.date.getMonth() + 2 > 12) {
        //     this.pilihBulan = this.date.getMonth();
        //     this.pilihTahun = this.date.getFullYear() + 1;
        // } else {
        //     this.pilihBulan = this.date.getMonth() + 2;
        //     this.pilihTahun = this.date.getFullYear();
        // }
    }

    ngOnInit() {
        this.loadData();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    loadData() {
        this.currentGroupUSers = this.principal.getUserGroup();
        this.loadingService.loadingStart();
        this.getAllDist();
        this.getAllDistBadak();
        this.getAllDistLasegar();
        // this.getWilayahDistBadak();
        // this.getWilayahDistBLasegar();
        this.loadingService.loadingStop();
    }
    private onSuccess(data, headers) {
        this.dataPPPO = data;
        this.loadingService.loadingStop();
        console.log(this.dataPPPO);
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    exportExcel() {
        this.loadingService.loadingStart();
        const temp = this.pilihTahun;
        const filter_data = 'test:' + temp + '|sm:' + this.principal.getUserGroup();
        this.reportUtilService.downloadFileWithName('Report Target VS PO Trend', process.env.API_C_URL + '/api/report/dashboard_trend/xlsx', { filterData: filter_data });

        this.loadingService.loadingStop();
    }

    getWilayahDistBadak() {
        this.dashboardService.findWilayahByJenis(this.pilihBulan, this.pilihTahun, 'badak')
            .subscribe(
                (res) => {
                    this.setPODataBarWilayahByJenis('chartdivBarWilayahBadakfk', res);
                }
            )
    }

    getWilayahDistBLasegar() {
        this.dashboardService.findWilayahByJenis(this.pilihBulan, this.pilihTahun, 'lasegar')
            .subscribe(
                (res) => {
                    this.setPODataBarWilayahByJenis('chartdivBarWilayahLasegarfk', res);
                }
            )
    }
    getAllDist() {
        this.dashboardService.findTrendFK(this.pilihBulan, this.pilihTahun)
            .subscribe(
                (res: any) => {
                    // this.setPODataPie(res);
                    this.setPODataBarAll(res)
                }
            );
        // this.dashboardService.queryReport1({
        //     query: 'tahun:' + this.pilihTahun
        // }).subscribe(
        //     (res: ResponseWrapper) => {
        //         this.onSuccess(res.json, res.headers)
        //     },
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
    }

    getAllDistBadak() {
        this.dashboardService.findTrendJenisFK('badak', this.pilihBulan, this.pilihTahun)
            .subscribe(
                (res: any) => {
                    // this.setPOBadakDataPie(res);
                    this.setPODataBarAllBadakLasegar(res, 'chartdivBarAllBadakfk')
                }
            );
    }
    getAllDistLasegar() {
        this.dashboardService.findTrendJenisFK('lasegar', this.pilihBulan, this.pilihTahun)
            .subscribe(
                (res: any) => {
                    // this.setPOLasegarDataPie(res);
                    this.setPODataBarAllBadakLasegar(res, 'chartdivBarAllLasegarfk')
                }
            );
    }

    setPODataBarAll(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarALlfk', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const div = 'chartdivBarALlfk';
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulan';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'bulan';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(30);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'bulan';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(30);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfakturpercent';
        series3.dataFields.categoryX = 'bulan';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'bulan';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);
        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        // floating tooltip
        // series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        series.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series2.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series3.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series4.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                           <tr>
                                               <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                                <th style="text-align:right; background-color:#0000FF"><span style="color:white">{newpopercent.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                               <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimfakturpercent.formatNumber("#.")}</span></th>
                                           </tr>
                                           <tr>
                                               <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                           </tr>
                                       </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';
        // chart.legend.marginBottom = 70;
        // chart.legend.labels.template.truncate = true;
        // chart.legend.itemContainers.template.tooltipText = '';
        // chart.legend.itemContainers.template.clickable = false;
        // chart.legend.itemContainers.template.focusable = false;
        // chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
        // chart.legend.itemContainers.template.tooltip.disabled = true;

        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    setPODataBarAllBadakLasegar(data: any, div: string) {
        // Create chart instance
        const chart = am4core.create(div, am4charts.XYChart);

        // Add data
        chart.data = data;
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulan';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'bulan';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(30);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'bulan';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(30);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfakturpercent';
        series3.dataFields.categoryX = 'bulan';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'bulan';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        series.tooltip.label.interactionsEnabled = true;
        // series.tooltip.events.on('hit', function() { alert('fooooooooooooooooooo {bulan}')});
        series.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series2.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series3.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series4.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series.tooltip.keepTargetHover = true;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#0000FF"><span style="color:white">{newpopercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimfakturpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    setPODataBarAllLasegar(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarAllLasegarfk', am4charts.XYChart);
        // Add data
        chart.data = data
        // Create axes
        const div = 'chartdivBarAllLasegarfk'
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfakturpercent';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        series.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series2.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series3.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series4.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#0000FF"><span style="color:white">{newpopercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimfakturpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    getRayLs() {
        this.dashboardService.findRay('LSG', this.pilihTahun, this.pilihBulan)
            .subscribe(
                (res: any) => {
                    this.rayLSs = res;
                    this.rayLSs.forEach(
                        (e) => {
                            this.dashboardService.findBar(this.pilihBulan, this.pilihTahun, 'lasegar', e.cust_ray).subscribe(
                                (resLS: any) => {
                                    const chartDiv = 'chartdivBarLasegarfk' + e.ray_desc1;
                                    console.log('INI CHART DIV LASEGAR == ', chartDiv);
                                    this.setPODataBarAllWilayahWithLoop(resLS, chartDiv);
                                });
                        }
                    )
                    console.log('Ray LASAEGAR == ', res);
                }
            )
    }

    getRayBadak() {
        this.dashboardService.findRay('BDK', this.pilihTahun, this.pilihBulan)
            .subscribe(
                (res: any) => {
                    this.rayBadaks = res;
                    this.rayBadaks.forEach(
                        (e) => {
                            this.dashboardService.findBar(this.pilihBulan, this.pilihTahun, 'badak', e.cust_ray).subscribe(
                                (resBadak: any) => {
                                    const chartDiv = 'chartdivBarBadakfk' + e.ray_desc1;
                                    console.log('INI CHART DIV BADAK == ', chartDiv);
                                    this.setPODataBarAllWilayahWithLoop(resBadak, chartDiv);
                                });
                        }
                    )
                    console.log('Ray BADAK == ', res);
                }
            )
    }
    setPODataBarAllWilayahWithLoop(data: any[], chartdiv: any) {
        const chart = am4core.create(chartdiv, am4charts.XYChart);

        // Add data
        chart.data = data;

        // Create axes
        const div = chartdiv;
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle'
        categoryAxis.renderer.labels.template.horizontalCenter = 'right'

        // categoryAxis.tooltipText = '{category}: [bold]{valueY}[/]';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfakturpercent';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');
        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        series.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series2.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series3.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series4.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#0000FF"><span style="color:white">{popercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimfakturpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
        this.loadData();
    }

    setPODataBarWilayahByJenis(chartDiv: string, data: any) {
        // Create chart instance
        const chart = am4core.create(chartDiv, am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const div = chartDiv;
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 0;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle';
        categoryAxis.renderer.labels.template.horizontalCenter = 'right';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfakturpercent';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        series.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series2.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series3.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        series4.columns.template.events.on(
            'hit',
            (ev) => {
                this.cek(ev.target._dataItem.dataContext['bulan'], div);
                //   let a = ev.target;
                //   alert(ev.target._dataItem.dataContext['bulan']);
                //   console.log('clicked on ', ev.target._dataItem.dataContext['bulan']);
            },
            this
        );
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                            <th style="text-align:right; background-color:#0000FF"><span style="color:white">popercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimfakturpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';
    }
    toValue() {
        this.router.navigate(['/dashboard-tagetvspo']);
    }

    cek(data: any, div: string) {
        this.dataDIV = div;
        this.modalTrend = true;
        this.dataBulan = data;
        console.log(this.dataDIV);
        console.log(this.dataBulan);
        this.dashboardService.queryGetNotes(
            {
                query: 'bulan:' + data + '|namachart:' + div
            }
        ).subscribe(
            (res: DasboardTrendNotes) => {
                this.notesChart = res;
            }
        )
        // alert(data);
    }
    saveNote(data: any, div: string) {
        this.notesChart.trendbulan = data;
        this.notesChart.namachart = div;
        this.dashboardService.setNotes(this.notesChart)
            .subscribe((res) => {
                this.modalTrend = false;
                this.notesChart = new DasboardTrendNotes();
            });
    }
}
