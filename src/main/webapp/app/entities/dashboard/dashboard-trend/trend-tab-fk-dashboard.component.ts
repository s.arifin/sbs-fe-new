import { Component, OnInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_dataviz from '@amcharts/amcharts4/themes/dataviz';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Router } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { DashboardService } from '../dashboard.service';

@Component({
    selector: 'jhi-trend-tab-fk-dashboard',
    templateUrl: './trend-tab-fk-dashboard.component.html',

})
export class TrendTabFkDashboardComponent implements OnInit {
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    bulan: Array<object> = [
        { label: '01', value: 1 },
        { label: '02', value: 2 },
        { label: '03', value: 3 },
        { label: '04', value: 4 },
        { label: '05', value: 5 },
        { label: '06', value: 6 },
        { label: '07', value: 7 },
        { label: '08', value: 8 },
        { label: '09', value: 9 },
        { label: '10', value: 10 },
        { label: '11', value: 11 },
        { label: '12', value: 12 }
    ];
    pilihBulan: number;
    tahun: Array<object> = [
        // { label: '2020', value: 2020 },
        // { label: '2021', value: 2021 },
        // { label: '2022', value: 2022 },
        // { label: '2023', value: 2023 },
        // { label: '2024', value: 2024 }
    ];
    pilihTahun: number;
    rayLSs: any[];
    rayBadaks: any[];
    isPercent: boolean;
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 1 > 12) {
            this.pilihBulan = this.date.getMonth() + 1;
            this.pilihTahun = this.date.getFullYear() + 1;
        } else {
            this.pilihBulan = this.date.getMonth() + 1;
            this.pilihTahun = this.date.getFullYear();
        }
        this.isPercent = true;
        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahun.push({ label: year.toString(), value: year });
        }
    }

    ngOnInit() {
    }

    loadData() {
    }
    toValue() {
        // this.router.navigate(['/dashboard-tagetvspo']);
        if (this.isPercent === true) {
            this.isPercent = false;
        }
    }
    toPercent() {
        // this.router.navigate(['/dashboard-tagetvspo']);
        if (this.isPercent === false) {
            this.isPercent = true;
        }
    }
    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
    }
}
