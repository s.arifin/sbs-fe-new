import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { LoadingService } from '../../layouts';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Subscription } from 'rxjs';
import { ResponseWrapper, Principal } from '../../shared';

@Component({
    selector: 'jhi-tab-dashboard',
    templateUrl: './tab-dashboard.component.html',

})
export class TabDashboardComponent implements OnInit {
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    bulan: Array<object> = [
        { label: '01', value: 1 },
        { label: '02', value: 2 },
        { label: '03', value: 3 },
        { label: '04', value: 4 },
        { label: '05', value: 5 },
        { label: '06', value: 6 },
        { label: '07', value: 7 },
        { label: '08', value: 8 },
        { label: '09', value: 9 },
        { label: '10', value: 10 },
        { label: '11', value: 11 },
        { label: '12', value: 12 }
    ];
    pilihBulan: number;
    tahun: Array<object> = [
        // { label: '2012', value: 2012 },
        // { label: '2013', value: 2013 },
        // { label: '2014', value: 2014 },
        // { label: '2015', value: 2015 },
        // { label: '2016', value: 2016 },
        // { label: '2017', value: 2017 },
        // { label: '2018', value: 2018 },
        // { label: '2019', value: 2019 },
        // { label: '2020', value: 2020 },
        // { label: '2021', value: 2021 },
        // { label: '2022', value: 2022 },
        // { label: '2023', value: 2023 },
        // { label: '2024', value: 2024 },
    ];
    pilihTahun: number;
    rayLSs: any[];
    rayBadaks: any[];
    isPercent: boolean;
    currentAccount: any;
    currentGroupUSers: any
    private subscription: Subscription;
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private reportUtilService: ReportUtilService,
        private route: ActivatedRoute,
        private principal: Principal,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 1 > 12) {
            this.pilihBulan = this.date.getMonth() + 1;
            this.pilihTahun = this.date.getFullYear() + 1;
        } else {
            this.pilihBulan = this.date.getMonth() + 1;
            this.pilihTahun = this.date.getFullYear();
        }
        this.isPercent = true;
        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahun.push({ label: year.toString(), value: year });
        }
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['bulan'] && params['tahun']) {
                this.pilihBulan = params['bulan'];
                this.pilihTahun = params['tahun'];
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    loadData() {
    }
    toValue() {
        // this.router.navigate(['/dashboard-tagetvspo']);
        if (this.isPercent === true) {
            this.isPercent = false;
        }
    }
    toPercent() {
        // this.router.navigate(['/dashboard-tagetvspo']);
        if (this.isPercent === false) {
            this.isPercent = true;
        }
    }
    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
    }
    download() {
        this.loadingService.loadingStart();
        let bulan = '';
        let tahun = '';
        let strNamaBulan = ''
        if (this.pilihBulan === 1) { strNamaBulan = 'JANUARI'; }
        if (this.pilihBulan === 2) { strNamaBulan = 'FEBRUARY'; }
        if (this.pilihBulan === 3) { strNamaBulan = 'MARET'; }
        if (this.pilihBulan === 4) { strNamaBulan = 'APRIL'; }
        if (this.pilihBulan === 5) { strNamaBulan = 'MEI'; }
        if (this.pilihBulan === 6) { strNamaBulan = 'JUNI'; }
        if (this.pilihBulan === 7) { strNamaBulan = 'JULI'; }
        if (this.pilihBulan === 8) { strNamaBulan = 'AGUSTUS'; }
        if (this.pilihBulan === 9) { strNamaBulan = 'SEPTEMBER'; }
        if (this.pilihBulan === 10) { strNamaBulan = 'OKTOBER'; }
        if (this.pilihBulan === 11) { strNamaBulan = 'NOVEMBER'; }
        if (this.pilihBulan === 12) { strNamaBulan = 'DESEMBER'; }
        if (this.pilihBulan.toString().length === 1) {
            bulan = '0' + this.pilihBulan;
        }
        if (this.pilihBulan.toString().length > 1) {
            bulan = this.pilihBulan.toString();
        }
        tahun = this.pilihTahun.toString().substring(2);
        console.log('bulan : ', bulan);
        console.log('tahun : ', tahun);
        console.log('tahun : ', strNamaBulan);
        const filter_data = 'bulan:' + bulan + '|tahun:' + tahun + '|namabulan:' + strNamaBulan + '|sm:' + this.principal.getUserGroup();
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/dashboard/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
    }
}
