import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { BaseEntity } from '../../shared';

export class Dashboard implements BaseEntity {
    constructor(
        public id?: number,
    ) { }
}
export class DasboardTrendNotes {
    constructor(
        public id?: any,
        public trendbulan?: string,
        public createdby?: string,
        public dtcreated?: Date,
        public notes?: string,
        public namachart?: string
    ) {}
}
export class DashboardPPPO {
    constructor(
        public iddashboard?: any,
        public bulan?: number,
        public tahun?: number,
        public ppqty?: any,
        public poqty?: any,
        public bbmqty?: any,
        public paymentqty?: any,
        public kodeproduk?: string,
        public div?: string,
        public kdsuppnew?: string,
        public nopp?: string,
        public namaproduk?: string,
        public nopo?: string,
        public pic?: string
    ) {}
}

export class DashboardKlaim {
    constructor(
        public no_klaim?: any,
        public cust_code?: string,
        public cust_name?: string,
        public tgl_klaim?: any,
        public tglInput?: any,
        public bulan_program?: any,
        public nm_program?: any,
        public no_srt_skg?: any,
        public tgl_srt_skg?: any,
        public totalValue?: any,
        public totalValueACC?: string,
        public divisi?: string
    ) {}
}

export class DashboardRofo {
    constructor(
        public kode_group?: any,
        public kode_stok?: string,
        public nama_stok?: string,
        public jumlah_stok?: any,
        public harga_stok?: any,
        public total_keseluruhan?: any,
        public kd_mail?: string,
    ) {}
}
export class DashboardTrendP {
    constructor(
        public desc1: string,
        public ray?: string,
        public  sm?: string,
        public  cust_code?: string,
        public target_jan?: any,
        public po_jan?: any,
        public kirim_jan?: any,
        public kirimfaktur_jan?: any,
        public bayar_jan?: any,
        public target_feb?: any,
        public po_feb?: any,
        public kirim_feb?: any,
        public kirimfaktur_feb?: any,
        public bayar_feb?: any,
        public target_mar?: any,
        public po_mar?: any,
        public kirim_mar?: any,
        public kirimfaktur_mar?: any,
        public bayar_mar?: any,
        public target_apr?: any,
        public kirim_apr?: any,
        public kirimfaktur_apr?: any,
        public bayar_apr?: any,
        public target_mei?: any,
        public po_mei?: any,
        public kirim_mei?: any,
        public kirimfaktur_mei?: any,
        public bayar_mei?: any,
        public target_jun?: any,
        public po_jun?: any,
        public kirim_jun?: any,
        public kirimfaktur_jun?: any,
        public bayar_jun?: any,
        public target_jul?: any,
        public po_jul?: any,
        public kirim_jul?: any,
        public kirimfaktur_jul?: any,
        public bayar_jul?: any,
        public target_agu?: any,
        public po_agu?: any,
        public kirim_agu?: any,
        public kirimfaktur_agu?: any,
        public bayar_agu?: any,
        public target_sept?: any,
        public po_sept?: any,
        public kirim_sept?: any,
        public  bayar_sept?: any,
        public kirimfaktur_sept?: any,
        public target_okt?: any,
        public po_okt?: any,
        public kirim_okt?: any,
        public kirimfaktur_okt?: any,
        public bayar_okt?: any,
        public target_nov?: any,
        public po_nov?: any,
        public kirim_nov?: any,
        public kirimfaktur_nov?: any,
        public bayar_nov?: any,
        public target_des?: any,
        public po_des?: any,
        public kirim_des?: any,
        public kirimfaktur_des?: any,
        public bayar_des?: any,
    ) {}
}

export class DashboardProduksiDaily {
    constructor(
        public id: any,
        public kd_group?: any,
        public kd_produk?: string,
        public nm_produk?: string,
        public qty?: number,
        public tgl_palet?: any,
    ) {}
}
