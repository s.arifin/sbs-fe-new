import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { DasboardTrendNotes, DashboardPPPO } from './dashboard.model';

@Injectable()
export class DashboardService {

    private resourceUrl = process.env.API_C_URL + '/api/chart-datas';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    find(bulan: any, tahun: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findBadak(bulan: any, tahun: any, jenis: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist/${bulan}/${tahun}/${jenis}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findLasegar(bulan: any, tahun: any, jenis: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist/${bulan}/${tahun}/${jenis}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findBar(bulan: number, tahun: any, jenis: any, ray: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-bar/${bulan}/${tahun}/${jenis}/${ray}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtcreated = this.dateUtils
            .convertDateTimeFromServer(entity.dtcreated);
        entity.dtmodified = this.dateUtils
            .convertDateTimeFromServer(entity.dtmodified);
    }

    findRay(custsm: any, tahun: number, bulan: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-ray/${custsm}/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findWilayahByJenis(bulan: number, tahun: any, jenis: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-wilayah-by-jenis/${bulan}/${tahun}/${jenis}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    // untuk data rofo yahh
    findRofo(tahun: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-rofoB/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findRofo1(tahun: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-rofoP/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findRofo2(tahun: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-rofo/${tahun}/`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findBadakRofo(bulan: any, tahun: any, jenis: string, itemmail: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-rofo/${bulan}/${tahun}/${jenis}/${itemmail}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findLasegarRofo(bulan: any, tahun: any, jenis: string, itemmail: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-rofo/${bulan}/${tahun}/${jenis}/${itemmail}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findBarRofo(bulan: number, tahun: any, jenis: any, ray: any, itemmail: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-bar-rofo/${bulan}/${tahun}/${jenis}/${ray}/${itemmail}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findWilayahByJenisRofo(bulan: number, tahun: any, jenis: any, itemmail: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-wilayah-by-jenis-rofo/${bulan}/${tahun}/${jenis}/${itemmail}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findRayRofo(custsm: any, tahun: number, itemmail: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-ray-rofo/${custsm}/${tahun}/${itemmail}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findTrendFK(bulan: number, tahun: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-trends/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findTrendSJ(bulan: number, tahun: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-trends/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findPPPOEVN(tahun: number, pic: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-PpPo/${tahun}/${pic}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findPPPOJTR(tahun: number, pic: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-PpPo/${tahun}/${pic}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findPPPOSVY(tahun: number, pic: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-PpPo/${tahun}/${pic}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findPPPOOther(tahun: number, pic: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-PpPo/${tahun}/${pic}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findTrendJenisFK(jenis: string, bulan: number, tahun: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-trends/${jenis}/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findTrendJenisSJ(jenis: string, bulan: number, tahun: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-trends/${jenis}/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findItemMail(bulan: any, tahun: any, jenis: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-item-mail/${bulan}/${tahun}/${jenis}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findRofoJenisWilayah(bulan: any, tahun: any, descmail: string): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-dist-rofo-wilayah/${bulan}/${tahun}/${descmail}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryGetNotes(req?: any): Observable<DasboardTrendNotes> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-trens-notes', options).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getPPPO(bulan: number, tahun: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-item-PpPo/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    // queryGetNotes(req?: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(req);
    //     return this.http.get(this.resourceUrl + '/get-trens-notes', options)
    //         .map((res: Response) => this.convertResponse(res));
    // }
    setNotes(note: DasboardTrendNotes): Observable<DasboardTrendNotes> {
        const notes = this.convert(note);
        return this.http.post(this.resourceUrl + '/set-trens-notes', notes).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    private convert(note: DasboardTrendNotes): DasboardTrendNotes {
        const copy: DasboardTrendNotes = Object.assign({}, note);

        // copy.dtcreated = this.dateUtils.toDate(purchasing.dtcreated);

        // copy.dtmodified = this.dateUtils.toDate(purchasing.dtmodified);
        return copy;
    }

    queryReport(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getDataPPPO', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findKlaimDist(tahun: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-klaim/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryReportKlaim(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getDataKlaim', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryReportRofo(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getDataRofoS', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findKlaimDistBadak(tahun: number, jenis: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-klaim-jenis/${tahun}/${jenis}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findKlaimDistLasegar(tahun: number, jenis: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/get-all-data-klaim-jenis/${tahun}/${jenis}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryReport1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getPrintTrendSJ', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findProdukDaily(): Observable<any> {
        return this.http.get(`${this.resourceUrl}/getProdukDaily`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getTahun(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallyear', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getMesin(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallmesin', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findProdukDaily1(): Observable<any> {
        return this.http.get(`${this.resourceUrl}/getProdukDaily1`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findProdukDaily2(): Observable<any> {
        return this.http.get(`${this.resourceUrl}/getProdukDaily2`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findProdukMonthlyBdk(tgl: any): Observable<ResponseWrapper> {
        const jenis = 'Badak';
        return this.http.get(`${this.resourceUrl}/getProdukMonthly/${tgl}/${jenis}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }

    findProdukMonthlyLsg(tgl: any): Observable<ResponseWrapper> {
        const jenis = 'Lasegar';
        return this.http.get(`${this.resourceUrl}/getProdukMonthly/${tgl}/${jenis}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }

    findProdukMonthlyLain(tgl: any): Observable<ResponseWrapper> {
        const jenis = 'Lain';
        return this.http.get(`${this.resourceUrl}/getProdukMonthly/${tgl}/${jenis}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }

    findProdukPeriodBdk(tgl: any, tgl1: any): Observable<ResponseWrapper> {
        const jenis = 'Badak';
        return this.http.get(`${this.resourceUrl}/getProdukPeriod/${tgl}/${tgl1}/${jenis}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }

    findProdukPeriodLsg(tgl: any, tgl1: any): Observable<ResponseWrapper> {
        const jenis = 'Lasegar';
        return this.http.get(`${this.resourceUrl}/getProdukPeriod/${tgl}/${tgl1}/${jenis}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }

    findProdukPeriodLain(tgl: any, tgl1: any): Observable<ResponseWrapper> {
        const jenis = 'Lain';
        return this.http.get(`${this.resourceUrl}/getProdukPeriod/${tgl}/${tgl1}/${jenis}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }
}
