import { Component, Input, OnInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { ExcelService } from '../../report/excel.service';
import { DashboardPPPO } from '../dashboard.model';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ResponseWrapper, Principal } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
  selector: 'jhi-dashboard-pppo',
  templateUrl: './dashboard-pppo.component.html',
})
export class DashboardPppoComponent implements OnInit {
  @Input() pilihBulan: any;
  @Input() pilihTahun: any;
  public data: any;
  public dataBadak: any;
  public dataLasegar: any;
  public dataBarBadak: any;
  public dataBarLasegar: any;
  public date: Date;
  public options: any;
  public dataBarAll: any;
  public pic1: any;
  public pic2: any;
  public pic3: any;
  public user: any;
  periode: any;
  dataPPPO: DashboardPPPO[];
  idInternal: any;
  currentPlant: any;
  constructor(
    private dashboardService: DashboardService,
    private loadingService: LoadingService,
    private router: Router,
    private excelService: ExcelService,
    private alertService: JhiAlertService,
    private principal: Principal,
    protected reportUtilService: ReportUtilService
  ) {
    this.date = new Date();
    this.idInternal = this.principal.getIdInternal();
    if (this.idInternal.includes('SMK')) {
      this.currentPlant = 'SMK';
    } else if (this.idInternal.includes('SKG')) {
      this.currentPlant = 'SKG';
    } else {
      this.currentPlant = 'SBS';
    }
  }

  ngOnInit() {
    this.loadData();
    this.user = this.principal.getUserLogin();
    console.log(this.user);
  }

  loadData() {
    this.loadingService.loadingStart();
    this.getAllDist();
    this.loadingService.loadingStop();
  }

  getAllDist() {
    this.user = this.principal.getUserLogin();
    this.pic1 = 'EVN';
    // TARIK DATA DARI SBS
    this.dashboardService.findPPPOEVN(this.pilihTahun, 'EVN').subscribe(
      (res: any) => {
        // this.setPODataPie(res);
        this.setPODataBarEVN(res)
      }
    );

    this.dashboardService.findPPPOJTR(this.pilihTahun, 'JTR').subscribe(
      (res: any) => {
        // this.setPODataPie(res);
        this.setPODataBarJTR(res)
      }
    );

    this.dashboardService.findPPPOSVY(this.pilihTahun, 'SVY').subscribe(
      (res: any) => {
        // this.setPODataPie(res);
        this.setPODataBarSVY(res)
      }
    );
    // TARIK DATA DARI SBS

    // TARIK DATA DARI SKG
    this.dashboardService.findPPPOOther(this.pilihTahun, 'SVY_SKG').subscribe(
      (res: any) => {
        // this.setPODataPie(res);
        this.setPODataBarSVYSKG(res)
      }
    );
    // TARIK DATA DARI SKG

    // TARIK DATA DARI SMK
    this.dashboardService.findPPPOOther(this.pilihTahun, 'SVY_SMK').subscribe(
      (res: any) => {
        // this.setPODataPie(res);
        if (this.user === 'JTR_SMK') {
          this.setPODataBarJTRSMK(res)
        } else {
          this.setPODataBarSVYSMK(res)
        }
      }
    );

    this.dashboardService.findPPPOOther(this.pilihTahun, 'JTR_SMK').subscribe(
      (res: any) => {
        // this.setPODataPie(res);
        if (this.user === 'JTR_SMK') {
          this.setPODataBarJTRSMK(res)
        } else {
          this.setPODataBarSVYSMK(res)
        }
      }
    );
    // TARIK DATA DARI SMK
  }

  private onSuccess(data, headers) {
    this.dataPPPO = data;
    this.loadingService.loadingStop();
  }

  private onError(error) {
    this.alertService.error(error.message, null, null);
    this.loadingService.loadingStop();
  }

  exportExcel() {
    this.loadingService.loadingStart();
    if (this.currentPlant === 'SBS') {
      if (this.user === 'EVN' || this.user === 'JTR') {
        const jenis = 2;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + this.user + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      } else if (this.user === 'SVY') {
        const jenis = 3;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      } else {
        const jenis = 1;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      }
    } else if (this.currentPlant === 'SKG') {
      if (this.user === 'SVY_SKG') {
        const jenis = 3;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      } else {
        const jenis = 1;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      }
    } else if (this.currentPlant === 'SMK') {
      if (this.user === 'JTR_SMK') {
        const jenis = 2;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + this.user + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      } else if (this.user === 'SVY_SMK') {
        const jenis = 3;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      } else {
        const jenis = 1;
        const filter_data = 'tahun:' + this.pilihTahun + '|jenis:' + jenis + '|pic:' + '|plant:' + this.currentPlant;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/rpt_dashboard_ppvspo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
      }
    }
  }

  setPODataBarEVN(data: any) {
    // Create chart instance
    const chart = am4core.create('chartdivBarEVN', am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';
    // Add data
    chart.data = data;
    // Create axes
    const div = 'chartdivBarALl';
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'bulanvel';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 10;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '900';
    valueAxis.strictMinMax = true;
    valueAxis.min = 0;
    valueAxis.renderer.labels.template.adapter.add('text', function(text) {
      return text + '%';
    });

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 16;
    series.dataFields.valueY = 'ppqty';
    series.dataFields.categoryX = 'bulanvel';
    series.clustered = false;
    series.tooltipText = 'PP : [bold]{valueY}[/]%';
    series.tooltip.label.fontSize = 18;
    series.tooltip.label.wrap = true;
    series.name = 'PP';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning
    series.columns.template.width = am4core.percent(40);

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 16;
    series2.dataFields.valueY = 'poqty';
    series2.dataFields.categoryX = 'bulanvel';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltipText = 'PO : [bold]{valueY}[/]%';
    series2.tooltip.label.fontSize = 18;
    series2.name = 'PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru
    series2.columns.template.width = am4core.percent(40);

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.fontSize = 16;
    series3.dataFields.valueY = 'bbmqty';
    series3.dataFields.categoryX = 'bulanvel';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltipText = 'BBM : [bold]{valueY}[/]%';
    series3.tooltip.label.fontSize = 18;
    series3.name = 'BBM';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau
    series3.columns.template.width = am4core.percent(40);

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.fontSize = 16;
    series4.dataFields.valueY = 'paymentqty';
    series4.dataFields.categoryX = 'bulanvel';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltip.label.fontSize = 18;
    series4.tooltipText = 'Payment : [bold]{valueY}[/]%';
    series4.name = 'Payment';
    series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
    series4.columns.template.width = am4core.percent(40);
    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    // floating tooltip
    // series.tooltip.label.textAlign = 'end';
    series.tooltip.pointerOrientation = 'down';
    series.tooltip.dy = -5;
    series.tooltip.label.fontSize = 10;
    columnTemplate1.tooltipHTML = ` <table style="width:100%">
                            <tr>
                                <th style="text-align:right; background-color:#F0BF60"><span style="color:black; font-size:14px;">{ppqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#9CC2E5"><span style="color:white; font-size:14px;">{poqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#C4D79B"><span style="color:black; font-size:14px;">{bbmqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#000000"><span style="color:white; font-size:14px;">{paymentqty.formatNumber("#.")}</span></th>
                            </tr>
                        </table>`;
    columnTemplate1.showTooltipOn = 'always';
    columnTemplate1.tooltipY = 0;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color('#ffffff00');
    series.tooltip.background.stroke = am4core.color('#ffffff00');
    // floating tooltip
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    // Create LEGEND
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    chart.legend.itemContainers.template.paddingBottom = 120;
    chart.legend.labels.template.truncate = true;
    chart.legend.itemContainers.template.tooltipText = '';
    chart.legend.itemContainers.template.clickable = false;
    chart.legend.itemContainers.template.focusable = false;
    chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    chart.legend.itemContainers.template.tooltip.disabled = true;
  }

  setPODataBarJTRSMK(data: any) {
    // Create chart instance
    const chart = am4core.create('chartdivBarJTRSMK', am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';
    // Add data
    chart.data = data;
    // Create axes
    const div = 'chartdivBarALl';
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'bulanvel';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 10;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '900';
    valueAxis.strictMinMax = true;
    valueAxis.min = 0;
    valueAxis.renderer.labels.template.adapter.add('text', function(text) {
      return text + '%';
    });

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 16;
    series.dataFields.valueY = 'ppqty';
    series.dataFields.categoryX = 'bulanvel';
    series.clustered = false;
    series.tooltipText = 'PP : [bold]{valueY}[/]%';
    series.tooltip.label.fontSize = 18;
    series.name = 'PP';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning
    series.columns.template.width = am4core.percent(40);

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.fontSize = 16;
    series2.dataFields.valueY = 'poqty';
    series2.dataFields.categoryX = 'bulanvel';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltipText = 'PO : [bold]{valueY}[/]%';
    series2.tooltip.label.fontSize = 18;
    series2.name = 'PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru
    series2.columns.template.width = am4core.percent(40);

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.fontSize = 14;
    series3.dataFields.valueY = 'bbmqty';
    series3.dataFields.categoryX = 'bulanvel';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltip.label.fontSize = 18;
    series3.tooltipText = 'BBM : [bold]{valueY}[/]%';
    series3.name = 'BBM';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau
    series3.columns.template.width = am4core.percent(40);

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.fontSize = 14;
    series4.dataFields.valueY = 'paymentqty';
    series4.dataFields.categoryX = 'bulanvel';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltip.label.fontSize = 18;
    series4.tooltipText = 'Payment : [bold]{valueY}[/]%';
    series4.name = 'Payment';
    series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
    series4.columns.template.width = am4core.percent(30);
    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    // floating tooltip
    // series.tooltip.label.textAlign = 'end';
    series.tooltip.pointerOrientation = 'down';
    series.tooltip.dy = -5;
    series.tooltip.label.fontSize = 10;
    columnTemplate1.tooltipHTML = ` <table style="width:100%">
                            <tr>
                                <th style="text-align:right; background-color:#F0BF60"><span style="color:black; font-size:14px;">{ppqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#9CC2E5"><span style="color:white; font-size:14px;">{poqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#C4D79B"><span style="color:black; font-size:14px;">{bbmqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#000000"><span style="color:white; font-size:14px;">{paymentqty.formatNumber("#.")}</span></th>
                            </tr>
                        </table>`;
    columnTemplate1.showTooltipOn = 'always';
    columnTemplate1.tooltipY = 0;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color('#ffffff00');
    series.tooltip.background.stroke = am4core.color('#ffffff00');
    // floating tooltip
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    // Create LEGEND
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    chart.legend.itemContainers.template.paddingBottom = 120;
    chart.legend.labels.template.truncate = true;
    chart.legend.itemContainers.template.tooltipText = '';
    chart.legend.itemContainers.template.clickable = false;
    chart.legend.itemContainers.template.focusable = false;
    chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    chart.legend.itemContainers.template.tooltip.disabled = true;
  }

  setPODataBarJTR(data: any) {
    // Create chart instance
    const chart = am4core.create('chartdivBarJTR', am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';
    // Add data
    chart.data = data;
    // Create axes
    const div = 'chartdivBarALl';
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'bulanvel';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 10;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '900';
    valueAxis.strictMinMax = true;
    valueAxis.min = 0;
    valueAxis.renderer.labels.template.adapter.add('text', function(text) {
      return text + '%';
    });

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 14;
    series.dataFields.valueY = 'ppqty';
    series.dataFields.categoryX = 'bulanvel';
    series.clustered = false;
    series.tooltip.label.fontSize = 18;
    series.tooltipText = 'PP : [bold]{valueY}[/]%';
    series.name = 'PP';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning
    series.columns.template.width = am4core.percent(40);

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.fontSize = 14;
    series2.dataFields.valueY = 'poqty';
    series2.dataFields.categoryX = 'bulanvel';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltip.label.fontSize = 18;
    series2.tooltipText = 'PO : [bold]{valueY}[/]%';
    series2.name = 'PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru
    series2.columns.template.width = am4core.percent(40);

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.fontSize = 14;
    series3.dataFields.valueY = 'bbmqty';
    series3.dataFields.categoryX = 'bulanvel';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltip.label.fontSize = 18;
    series3.tooltipText = 'BBM : [bold]{valueY}[/]%';
    series3.name = 'BBM';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau
    series3.columns.template.width = am4core.percent(40);

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.fontSize = 14;
    series4.dataFields.valueY = 'paymentqty';
    series4.dataFields.categoryX = 'bulanvel';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltip.label.fontSize = 18;
    series4.tooltipText = 'Payment : [bold]{valueY}[/]%';
    series4.name = 'Payment';
    series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
    series4.columns.template.width = am4core.percent(40);
    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    // floating tooltip
    // series.tooltip.label.textAlign = 'end';
    series.tooltip.pointerOrientation = 'down';
    series.tooltip.dy = -5;
    series.tooltip.label.fontSize = 10;
    columnTemplate1.tooltipHTML = ` <table style="width:100%">
                            <tr>
                                <th style="text-align:right; background-color:#F0BF60"><span style="color:black; font-size:14px;">{ppqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#9CC2E5"><span style="color:white; font-size:14px;">{poqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#C4D79B"><span style="color:black; font-size:14px;">{bbmqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#000000"><span style="color:white; font-size:14px;">{paymentqty.formatNumber("#.")}</span></th>
                            </tr>
                        </table>`;
    columnTemplate1.showTooltipOn = 'always';
    columnTemplate1.tooltipY = 0;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color('#ffffff00');
    series.tooltip.background.stroke = am4core.color('#ffffff00');
    // floating tooltip
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    // Create LEGEND
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    chart.legend.itemContainers.template.paddingBottom = 120;
    chart.legend.labels.template.truncate = true;
    chart.legend.itemContainers.template.tooltipText = '';
    chart.legend.itemContainers.template.clickable = false;
    chart.legend.itemContainers.template.focusable = false;
    chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    chart.legend.itemContainers.template.tooltip.disabled = true;
  }

  setPODataBarSVY(data: any) {
    // Create chart instance
    const chart = am4core.create('chartdivBarSVY', am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';
    // Add data
    chart.data = data;
    // Create axes
    const div = 'chartdivBarALl';
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'bulanvel';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 10;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '900';
    valueAxis.strictMinMax = true;
    valueAxis.min = 0;
    valueAxis.renderer.labels.template.adapter.add('text', function(text) {
      return text + '%';
    });

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 14
    series.dataFields.valueY = 'ppqty';
    series.dataFields.categoryX = 'bulanvel';
    series.clustered = false;
    series.tooltip.label.fontSize = 18;
    series.tooltipText = 'PP : [bold]{valueY}[/]%';
    series.name = 'PP';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning
    series.columns.template.width = am4core.percent(40);

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.fontSize = 14;
    series2.dataFields.valueY = 'poqty';
    series2.dataFields.categoryX = 'bulanvel';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltip.label.fontSize = 18;
    series2.tooltipText = 'PO : [bold]{valueY}[/]%';
    series2.name = 'PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru
    series2.columns.template.width = am4core.percent(40);

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.fontSize = 14;
    series3.dataFields.valueY = 'bbmqty';
    series3.dataFields.categoryX = 'bulanvel';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltip.label.fontSize = 18;
    series3.tooltipText = 'BBM : [bold]{valueY}[/]%';
    series3.name = 'BBM';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau
    series3.columns.template.width = am4core.percent(40);

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.fontSize = 14;
    series4.dataFields.valueY = 'paymentqty';
    series4.dataFields.categoryX = 'bulanvel';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltip.label.fontSize = 18;
    series4.tooltipText = 'Payment : [bold]{valueY}[/]%';
    series4.name = 'Payment';
    series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
    series4.columns.template.width = am4core.percent(40);
    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    // floating tooltip
    // series.tooltip.label.textAlign = 'end';
    series.tooltip.pointerOrientation = 'down';
    series.tooltip.dy = -5;
    series.tooltip.label.fontSize = 10;
    columnTemplate1.tooltipHTML = ` <table style="width:100%">
                            <tr>
                                <th style="text-align:right; background-color:#F0BF60"><span style="color:black; font-size:14px;">{ppqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#9CC2E5"><span style="color:white; font-size:14px;">{poqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#C4D79B"><span style="color:black; font-size:14px;">{bbmqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#000000"><span style="color:white; font-size:14px;">{paymentqty.formatNumber("#.")}</span></th>
                            </tr>
                        </table>`;
    columnTemplate1.showTooltipOn = 'always';
    columnTemplate1.tooltipY = 0;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color('#ffffff00');
    series.tooltip.background.stroke = am4core.color('#ffffff00');
    // floating tooltip
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    // Create LEGEND
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    chart.legend.itemContainers.template.paddingBottom = 120;
    chart.legend.labels.template.truncate = true;
    chart.legend.itemContainers.template.tooltipText = '';
    chart.legend.itemContainers.template.clickable = false;
    chart.legend.itemContainers.template.focusable = false;
    chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    chart.legend.itemContainers.template.tooltip.disabled = true;
  }

  setPODataBarSVYSKG(data: any) {
    // Create chart instance
    const chart = am4core.create('chartdivBarSVYSKG', am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';
    // Add data
    chart.data = data;
    // Create axes
    const div = 'chartdivBarALl';
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'bulanvel';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 10;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '900';
    valueAxis.strictMinMax = true;
    valueAxis.min = 0;
    valueAxis.renderer.labels.template.adapter.add('text', function(text) {
      return text + '%';
    });

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 14
    series.dataFields.valueY = 'ppqty';
    series.dataFields.categoryX = 'bulanvel';
    series.clustered = false;
    series.tooltip.label.fontSize = 18;
    series.tooltipText = 'PP : [bold]{valueY}[/]%';
    series.name = 'PP';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning
    series.columns.template.width = am4core.percent(40);

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.fontSize = 14;
    series2.dataFields.valueY = 'poqty';
    series2.dataFields.categoryX = 'bulanvel';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltip.label.fontSize = 18;
    series2.tooltipText = 'PO : [bold]{valueY}[/]%';
    series2.name = 'PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru
    series2.columns.template.width = am4core.percent(40);

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.fontSize = 14;
    series3.dataFields.valueY = 'bbmqty';
    series3.dataFields.categoryX = 'bulanvel';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltip.label.fontSize = 18;
    series3.tooltipText = 'BBM : [bold]{valueY}[/]%';
    series3.name = 'BBM';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau
    series3.columns.template.width = am4core.percent(40);

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.fontSize = 14;
    series4.dataFields.valueY = 'paymentqty';
    series4.dataFields.categoryX = 'bulanvel';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltip.label.fontSize = 18;
    series4.tooltipText = 'Payment : [bold]{valueY}[/]%';
    series4.name = 'Payment';
    series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
    series4.columns.template.width = am4core.percent(40);
    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    // floating tooltip
    // series.tooltip.label.textAlign = 'end';
    series.tooltip.pointerOrientation = 'down';
    series.tooltip.dy = -5;
    series.tooltip.label.fontSize = 10;
    columnTemplate1.tooltipHTML = ` <table style="width:100%">
                            <tr>
                                <th style="text-align:right; background-color:#F0BF60"><span style="color:black; font-size:14px;">{ppqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#9CC2E5"><span style="color:white; font-size:14px;">{poqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#C4D79B"><span style="color:black; font-size:14px;">{bbmqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#000000"><span style="color:white; font-size:14px;">{paymentqty.formatNumber("#.")}</span></th>
                            </tr>
                        </table>`;
    columnTemplate1.showTooltipOn = 'always';
    columnTemplate1.tooltipY = 0;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color('#ffffff00');
    series.tooltip.background.stroke = am4core.color('#ffffff00');
    // floating tooltip
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    // Create LEGEND
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    chart.legend.itemContainers.template.paddingBottom = 120;
    chart.legend.labels.template.truncate = true;
    chart.legend.itemContainers.template.tooltipText = '';
    chart.legend.itemContainers.template.clickable = false;
    chart.legend.itemContainers.template.focusable = false;
    chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    chart.legend.itemContainers.template.tooltip.disabled = true;
  }

  setPODataBarSVYSMK(data: any) {
    // Create chart instance
    const chart = am4core.create('chartdivBarSVYSMK', am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';
    // Add data
    chart.data = data;
    // Create axes
    const div = 'chartdivBarALl';
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'bulanvel';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 10;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '900';
    valueAxis.strictMinMax = true;
    valueAxis.min = 0;
    valueAxis.renderer.labels.template.adapter.add('text', function(text) {
      return text + '%';
    });

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.fontSize = 14
    series.dataFields.valueY = 'ppqty';
    series.dataFields.categoryX = 'bulanvel';
    series.clustered = false;
    series.tooltip.label.fontSize = 18;
    series.tooltipText = 'PP : [bold]{valueY}[/]%';
    series.name = 'PP';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning
    series.columns.template.width = am4core.percent(40);

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.fontSize = 14;
    series2.dataFields.valueY = 'poqty';
    series2.dataFields.categoryX = 'bulanvel';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltip.label.fontSize = 18;
    series2.tooltipText = 'PO : [bold]{valueY}[/]%';
    series2.name = 'PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru
    series2.columns.template.width = am4core.percent(40);

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.fontSize = 14;
    series3.dataFields.valueY = 'bbmqty';
    series3.dataFields.categoryX = 'bulanvel';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltip.label.fontSize = 18;
    series3.tooltipText = 'BBM : [bold]{valueY}[/]%';
    series3.name = 'BBM';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau
    series3.columns.template.width = am4core.percent(40);

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.fontSize = 14;
    series4.dataFields.valueY = 'paymentqty';
    series4.dataFields.categoryX = 'bulanvel';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltip.label.fontSize = 18;
    series4.tooltipText = 'Payment : [bold]{valueY}[/]%';
    series4.name = 'Payment';
    series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
    series4.columns.template.width = am4core.percent(40);
    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    // floating tooltip
    // series.tooltip.label.textAlign = 'end';
    series.tooltip.pointerOrientation = 'down';
    series.tooltip.dy = -5;
    series.tooltip.label.fontSize = 10;
    columnTemplate1.tooltipHTML = ` <table style="width:100%">
                            <tr>
                                <th style="text-align:right; background-color:#F0BF60"><span style="color:black; font-size:14px;">{ppqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#9CC2E5"><span style="color:white; font-size:14px;">{poqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#C4D79B"><span style="color:black; font-size:14px;">{bbmqty.formatNumber("#.")}</span></th>
                            </tr>
                            <tr>
                                <th style="text-align:right; background-color:#000000"><span style="color:white; font-size:14px;">{paymentqty.formatNumber("#.")}</span></th>
                            </tr>
                        </table>`;
    columnTemplate1.showTooltipOn = 'always';
    columnTemplate1.tooltipY = 0;
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color('#ffffff00');
    series.tooltip.background.stroke = am4core.color('#ffffff00');
    // floating tooltip
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
    // Create LEGEND
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    chart.legend.itemContainers.template.paddingBottom = 120;
    chart.legend.labels.template.truncate = true;
    chart.legend.itemContainers.template.tooltipText = '';
    chart.legend.itemContainers.template.clickable = false;
    chart.legend.itemContainers.template.focusable = false;
    chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    chart.legend.itemContainers.template.tooltip.disabled = true;
  }

  filter() {
    console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
    console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
    this.loadData();
  }

  setPODataBarWilayahByJenis(chartDiv: string, data: any) {
    // Create chart instance
    const chart = am4core.create(chartDiv, am4charts.XYChart);
    // Add percent sign to all numbers
    // chart.numberFormatter.numberFormat = '#.#\'%\'';

    // Add data
    chart.data = data;
    // Create axes
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'dist';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 0;
    categoryAxis.renderer.labels.template.rotation = 300;
    categoryAxis.renderer.labels.template.verticalCenter = 'middle';
    categoryAxis.renderer.labels.template.horizontalCenter = 'right';

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = 'Jumlah';
    valueAxis.title.fontWeight = '800';
    valueAxis.min = 0;

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = 'target';
    series.dataFields.categoryX = 'dist';
    series.clustered = false;
    series.tooltipText = 'Target PO : [bold]{valueY}[/]';
    series.name = 'Target PO';
    series.columns.template.fill = am4core.color('#F0BF60'); // Target Kuning

    const columnTemplate1 = series.columns.template;
    columnTemplate1.strokeWidth = 2;
    columnTemplate1.strokeOpacity = 1;
    columnTemplate1.stroke = am4core.color('#FFFFFF');

    const series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.dataFields.valueY = 'po';
    series2.dataFields.categoryX = 'dist';
    series2.clustered = false;
    // series2.columns.template.width = am4core.percent(75);
    series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
    series2.name = 'Jumlah PO';
    series2.columns.template.fill = am4core.color('#9CC2E5'); // PO Biru

    const columnTemplate2 = series2.columns.template;
    columnTemplate2.strokeWidth = 2;
    columnTemplate2.strokeOpacity = 1;
    columnTemplate2.stroke = am4core.color('#FFFFFF');

    const series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.dataFields.valueY = 'kirim';
    series3.dataFields.categoryX = 'dist';
    series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
    series3.name = 'Pengiriman PO';
    series3.columns.template.fill = am4core.color('#C4D79B'); // pengiriman Hijau

    const columnTemplate3 = series3.columns.template;
    columnTemplate3.strokeWidth = 2;
    columnTemplate3.strokeOpacity = 1;
    columnTemplate3.stroke = am4core.color('#FFFFFF');

    const series4 = chart.series.push(new am4charts.ColumnSeries());
    series4.dataFields.valueY = 'bayar';
    series4.dataFields.categoryX = 'dist';
    series4.clustered = false;
    // series4.columns.template.width = am4core.percent(25);
    series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
    series4.name = 'Pembayaran PO';
    series4.columns.template.fill = am4core.color('#000000'); // Pembayaran Hitam

    const columnTemplate4 = series4.columns.template;
    columnTemplate4.strokeWidth = 2;
    columnTemplate4.strokeOpacity = 1;
    columnTemplate4.stroke = am4core.color('#FFFFFF');

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;
  }

  toPercent() {
    this.router.navigate(['/dashboard-tagetvspo']);
  }
}
