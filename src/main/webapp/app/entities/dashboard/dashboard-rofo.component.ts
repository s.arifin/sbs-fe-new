import { Component, Input, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_dataviz from '@amcharts/amcharts4/themes/dataviz';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { LoadingService } from '../../layouts';
import { ResponseWrapper, Principal } from '../../shared';
import { Router } from '@angular/router';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-dashboard-rofo',
    templateUrl: './dashboard-rofo.component.html',

})
export class DashboardRofoComponent implements OnInit {
    // @Input() pilihBulan: any
    // @Input() pilihTahun: any
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    // INDICATOR UNTUK LOADING SCREEN
    indicator: any;
    indicatorInterval: any;
    indicatorLabel: any;
    hourglass: any;
    indicator1: any;
    indicatorInterval1: any;
    indicatorLabel1: any;
    hourglass1: any;
    indicator2: any;
    indicatorInterval2: any;
    indicatorLabel2: any;
    hourglass2: any;
    // INDICATOR UNTUK LOADING SCREEN
    periode: any;
    div: string;
    produkLS: string;
    produkLP: string;
    rayLSs: any[];
    rayBadaks: any[];
    bulan: Array<object> = [
        { label: '01', value: 1 },
        { label: '02', value: 2 },
        { label: '03', value: 3 },
        { label: '04', value: 4 },
        { label: '05', value: 5 },
        { label: '06', value: 6 },
        { label: '07', value: 7 },
        { label: '08', value: 8 },
        { label: '09', value: 9 },
        { label: '10', value: 10 },
        { label: '11', value: 11 },
        { label: '12', value: 12 }
    ];
    pilihBulan: number;
    isBadak: boolean;
    isPistol: boolean;
    isNasional: boolean;
    tahun: Array<object> = [
        // { label: '2012', value: 2012 },
        // { label: '2013', value: 2013 },
        // { label: '2014', value: 2014 },
        // { label: '2015', value: 2015 },
        // { label: '2016', value: 2016 },
        // { label: '2017', value: 2017 },
        // { label: '2018', value: 2018 },
        // { label: '2019', value: 2019 },
        // { label: '2020', value: 2020 },
        // { label: '2021', value: 2021 },
        // { label: '2022', value: 2022 },
        // { label: '2023', value: 2023 },
        // { label: '2024', value: 2024 }
    ];
    pilihTahun: number;
    divs: Array<object> = [
        // { label: ' --- PILIH --- ', value: '' },
        { label: 'Lasegar', value: 'LS' },
        { label: 'Badak ', value: 'LP' },
        // { label: 'Lian - Lain ', value: 'XX' },
    ];
    produkLasegars: Array<object> = [
        { label: 'Lasegar botol 500', value: 'LSG50' },
        { label: 'Lasegar botol 200', value: 'LSG20' },
        { label: 'Lasegar kaleng 320', value: 'LSGXX' },
        { label: 'Lasegar kaleng 238', value: 'LSKDX' },
        { label: 'Lasegar sachet', value: 'LSPXX' },
        { label: 'Lasegar container', value: 'LSGX2' }
    ];
    produkLPs: Array<object> = [
        { label: 'Badak botol 500', value: 'LCKB5' },
        { label: 'Badak botol 200', value: 'LCKT2' },
        { label: 'Badak kaleng 320', value: 'LCKXX' },
        { label: 'Badak kaleng 238', value: 'LLPXX' },
        { label: 'Badak sachet', value: 'PLPXX' },
        { label: 'Badak container', value: 'LCKXX' }
    ];
    listProdukLS: string[];
    listProdukLP: string[];
    currentAccount: any;
    currentGroupUSers: any
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private reportUtilService: ReportUtilService,
        private principal: Principal,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 1 > 12) {
            this.pilihTahun = this.date.getFullYear() + 1;
        } else {
            this.pilihTahun = this.date.getFullYear();
        }
        this.div = '';
        this.produkLS = '';
        this.produkLP = '';
        this.listProdukLS = new Array<string>();
        this.listProdukLP = new Array<string>();
        this.isNasional = false;
        this.isBadak = false;
        this.isPistol = false;

        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahun.push({ label: year.toString(), value: year });
        }
    }

    ngOnInit() {
        this.loadData();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        // this.dummychart();
    }

    loadData() {
        this.currentGroupUSers = this.principal.getUserGroup();
        // this.loadingService.loadingStart();
        this.getAllDist();
        // this.getItemMailBadak();
        // this.getItemMailLasegar();
        // this.loadingService.loadingStop();
    }
    getAllDist() {

        // UNTUK BADAK
        const chart = am4core.create('chartdivBarBadak', am4charts.XYChart);
        chart.preloader.disabled = true;
        this.setPODataBarBadakLoadingStart(chart);
        this.dashboardService.findRofo(this.pilihTahun)
            .subscribe(
                (res: any) => {
                    if (this.indicator !== null || this.indicator !== '' || this.indicator !== undefined) {
                        this.indicator.hide();
                        clearInterval(this.indicatorInterval);
                    }
                    this.setPODataBarBadak(chart, res);
                }
            );
        // UNTUK BADAK

        // UNTUK PISTOL
        const chart1 = am4core.create('chartdivBarPistol', am4charts.XYChart);
        chart1.preloader.disabled = true;
        this.setPODataBarPistolLoadingStart(chart1);
        this.dashboardService.findRofo1(this.pilihTahun)
            .subscribe(
                (res: any) => {
                    if (this.indicator1 !== null || this.indicator1 !== '' || this.indicator1 !== undefined) {
                        this.indicator1.hide();
                        clearInterval(this.indicatorInterval1);
                    }
                    // this.setPODataPie(res);
                    this.setPODataBarPistol(chart1, res)
                }
            );
        // UNTUK PISTOL

        // UNTUK NASIONAL
        const chart2 = am4core.create('chartdivBarAll', am4charts.XYChart);
        chart2.preloader.disabled = true;
        this.setPODataBarAllLoadingStart(chart2);
        this.dashboardService.findRofo2(this.pilihTahun)
            .subscribe(
                (res: any) => {
                    if (this.indicator2 !== null || this.indicator2 !== '' || this.indicator2 !== undefined) {
                        this.indicator2.hide();
                        clearInterval(this.indicatorInterval2);
                    }
                    this.setPODataBarAll(chart2, res)
                }
            );
        // UNTUK NASIONAL

    }

    setPODataBarBadakLoadingStart(chart: any) {
        if (!this.indicator) {
            this.indicator = chart.tooltipContainer.createChild(am4core.Container);
            this.indicator.background.fill = am4core.color('#fff');
            this.indicator.background.fill = am4core.color('#fff');
            this.indicator.background.fillOpacity = 0.8;
            this.indicator.width = am4core.percent(100);
            this.indicator.height = am4core.percent(100);

            this.indicatorLabel = this.indicator.createChild(am4core.Label);
            this.indicatorLabel.text = 'Loading Data...';
            this.indicatorLabel.align = 'center';
            this.indicatorLabel.valign = 'middle';
            this.indicatorLabel.fontSize = 20;
            this.indicatorLabel.dy = 50;

            this.hourglass = this.indicator.createChild(am4core.Image);
            this.hourglass.href = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-160/hourglass.svg';
            this.hourglass.align = 'center';
            this.hourglass.valign = 'middle';
            this.hourglass.horizontalCenter = 'middle';
            this.hourglass.verticalCenter = 'middle';
            this.hourglass.scale = 0.7;
        }
        this.indicator.hide(0);
        this.indicator.show();
        clearInterval(this.indicatorInterval);
        this.indicatorInterval = setInterval(function() {
            this.hourglass.animate([{
                from: 0,
                to: 360,
                property: 'rotation'
            }], 2000);
        }, 3000);
    }

    setPODataBarPistolLoadingStart(chart: any) {
        if (!this.indicator1) {
            this.indicator1 = chart.tooltipContainer.createChild(am4core.Container);
            this.indicator1.background.fill = am4core.color('#fff');
            this.indicator1.background.fill = am4core.color('#fff');
            this.indicator1.background.fillOpacity = 0.8;
            this.indicator1.width = am4core.percent(100);
            this.indicator1.height = am4core.percent(100);

            this.indicatorLabel1 = this.indicator1.createChild(am4core.Label);
            this.indicatorLabel1.text = 'Loading Data...';
            this.indicatorLabel1.align = 'center';
            this.indicatorLabel1.valign = 'middle';
            this.indicatorLabel1.fontSize = 20;
            this.indicatorLabel1.dy = 50;

            this.hourglass1 = this.indicator1.createChild(am4core.Image);
            this.hourglass1.href = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-160/hourglass.svg';
            this.hourglass1.align = 'center';
            this.hourglass1.valign = 'middle';
            this.hourglass1.horizontalCenter = 'middle';
            this.hourglass1.verticalCenter = 'middle';
            this.hourglass1.scale = 0.7;
        }
        this.indicator1.hide(0);
        this.indicator1.show();
        clearInterval(this.indicatorInterval1);
        this.indicatorInterval1 = setInterval(function() {
            this.hourglass1.animate([{
                from: 0,
                to: 360,
                property: 'rotation'
            }], 2000);
        }, 3000);
    }

    setPODataBarAllLoadingStart(chart: any) {
        if (!this.indicator2) {
            this.indicator2 = chart.tooltipContainer.createChild(am4core.Container);
            this.indicator2.background.fill = am4core.color('#fff');
            this.indicator2.background.fill = am4core.color('#fff');
            this.indicator2.background.fillOpacity = 0.8;
            this.indicator2.width = am4core.percent(100);
            this.indicator2.height = am4core.percent(100);

            this.indicatorLabel2 = this.indicator2.createChild(am4core.Label);
            this.indicatorLabel2.text = 'Loading Data...';
            this.indicatorLabel2.align = 'center';
            this.indicatorLabel2.valign = 'middle';
            this.indicatorLabel2.fontSize = 20;
            this.indicatorLabel2.dy = 50;

            this.hourglass2 = this.indicator2.createChild(am4core.Image);
            this.hourglass2.href = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-160/hourglass.svg';
            this.hourglass2.align = 'center';
            this.hourglass2.valign = 'middle';
            this.hourglass2.horizontalCenter = 'middle';
            this.hourglass2.verticalCenter = 'middle';
            this.hourglass2.scale = 0.7;
        }
        this.indicator2.hide(0);
        this.indicator2.show();
        clearInterval(this.indicatorInterval2);
        this.indicatorInterval2 = setInterval(function() {
            this.hourglass2.animate([{
                from: 0,
                to: 360,
                property: 'rotation'
            }], 2000);
        }, 3000);
    }

    setPODataBarBadak(chart: any, data: any) {
        // Create chart instance
        // const chart = am4core.create('chartdivBarBadak', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';
        // Add data

        chart.data = data;
        // Create axes
        const div = 'chartdivBarALl';
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulanvel';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'bulanvel';
        series.clustered = false;
        series.tooltipText = 'Target : [bold]{valueY}[/]%';
        series.name = 'Target';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(30);
        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');
        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'mrofopercent';
        series2.dataFields.categoryX = 'bulanvel';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'ROFO : [bold]{valueY}[/]%';
        series2.name = 'ROFO';
        series2.columns.template.fill = am4core.color('#008B8B'); // PO Biru
        series2.columns.template.width = am4core.percent(30);
        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');
        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'popercent';
        series3.dataFields.categoryX = 'bulanvel';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'PO : [bold]{valueY}[/]%';
        series3.name = 'PO';
        series3.columns.template.fill = am4core.color('#0000FF'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);
        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');
        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'kirimpercent';
        series4.dataFields.categoryX = 'bulanvel';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Kirim : [bold]{valueY}[/]%';
        series4.name = 'Kirim';
        series4.columns.template.fill = am4core.color('#00FF00'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);
        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');
        // floating tooltip
        // series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style='width:100%'>
                                           <tr>
                                               <th style='text-align:right; background-color:#FFFF00'><span style='color:black'>{targetpercent.formatNumber('#.')}</span></th>
                                           </tr>
                                           <tr>
                                                <th style='text-align:right; background-color:#008B8B'><span style='color:white'>{mrofopercent.formatNumber('#.')}</span></th>
                                           </tr>
                                           <tr>
                                               <th style='text-align:right; background-color:#0000FF'><span style='color:white'>{newpopercent.formatNumber('#.')}</span></th>
                                           </tr>
                                           <tr>
                                               <th style='text-align:right; background-color:#00FF00'><span style='color:black'>{kirimpercent.formatNumber('#.')}</span></th>
                                           </tr>
                                       </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.itemContainers.template.paddingBottom = 100;
        chart.legend.labels.template.truncate = true;
        chart.legend.itemContainers.template.tooltipText = '';
        chart.legend.itemContainers.template.clickable = false;
        chart.legend.itemContainers.template.focusable = false;
        chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
        // chart.legend.itemContainers.template.tooltip.disabled = true;
    }

    setPODataBarPistol(chart: any, data: any) {
        // Create chart instance
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';
        // Add data
        chart.data = data;
        // Create axes
        const div = 'chartdivBarALl';
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulanvel';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'bulanvel';
        series.clustered = false;
        series.tooltipText = 'Target : [bold]{valueY}[/]%';
        series.name = 'Target';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(30);
        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');
        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'mrofopercent';
        series2.dataFields.categoryX = 'bulanvel';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'ROFO : [bold]{valueY}[/]%';
        series2.name = 'ROFO';
        series2.columns.template.fill = am4core.color('#008B8B'); // PO Biru
        series2.columns.template.width = am4core.percent(30);
        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');
        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'popercent';
        series3.dataFields.categoryX = 'bulanvel';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'PO : [bold]{valueY}[/]%';
        series3.name = 'PO';
        series3.columns.template.fill = am4core.color('#0000FF'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);
        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');
        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'kirimpercent';
        series4.dataFields.categoryX = 'bulanvel';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Kirim : [bold]{valueY}[/]%';
        series4.name = 'Kirim';
        series4.columns.template.fill = am4core.color('#00FF00'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);
        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');
        // floating tooltip
        // series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style='width:100%'>
                                           <tr>
                                               <th style='text-align:right; background-color:#FFFF00'><span style='color:black'>{targetpercent.formatNumber('#.')}</span></th>
                                           </tr>
                                           <tr>
                                                <th style='text-align:right; background-color:#008B8B'><span style='color:white'>{mrofopercent.formatNumber('#.')}</span></th>
                                           </tr>
                                           <tr>
                                               <th style='text-align:right; background-color:#0000FF'><span style='color:white'>{newpopercent.formatNumber('#.')}</span></th>
                                           </tr>
                                           <tr>
                                               <th style='text-align:right; background-color:#00FF00'><span style='color:black'>{kirimpercent.formatNumber('#.')}</span></th>
                                           </tr>
                                       </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.itemContainers.template.paddingBottom = 100;
        chart.legend.labels.template.truncate = true;
        chart.legend.itemContainers.template.tooltipText = '';
        chart.legend.itemContainers.template.clickable = false;
        chart.legend.itemContainers.template.focusable = false;
        chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;

    }

    setPODataBarAll(chart: any, data: any) {
        // Create chart instance
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';
        // Add data
        chart.data = data;
        // Create axes
        const div = 'chartdivBarALl';
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'bulanvel';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'bulanvel';
        series.clustered = false;
        series.tooltipText = 'Target : [bold]{valueY}[/]%';
        series.name = 'Target';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(30);
        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');
        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'mrofopercent';
        series2.dataFields.categoryX = 'bulanvel';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'ROFO : [bold]{valueY}[/]%';
        series2.name = 'ROFO';
        series2.columns.template.fill = am4core.color('#008B8B'); // PO Biru
        series2.columns.template.width = am4core.percent(30);
        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');
        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'popercent';
        series3.dataFields.categoryX = 'bulanvel';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'PO : [bold]{valueY}[/]%';
        series3.name = 'PO';
        series3.columns.template.fill = am4core.color('#0000FF'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(30);
        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');
        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'kirimpercent';
        series4.dataFields.categoryX = 'bulanvel';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Kirim : [bold]{valueY}[/]%';
        series4.name = 'Kirim';
        series4.columns.template.fill = am4core.color('#00FF00'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(30);
        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');
        // floating tooltip
        // series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style='width:100%'>
                                            <tr>
                                                <th style='text-align:right; background-color:#FFFF00'><span style='color:black'>{targetpercent.formatNumber('#.')}</span></th>
                                            </tr>
                                            <tr>
                                                 <th style='text-align:right; background-color:#008B8B'><span style='color:white'>{mrofopercent.formatNumber('#.')}</span></th>
                                            </tr>
                                            <tr>
                                                <th style='text-align:right; background-color:#0000FF'><span style='color:white'>{newpopercent.formatNumber('#.')}</span></th>
                                            </tr>
                                            <tr>
                                                <th style='text-align:right; background-color:#00FF00'><span style='color:black'>{kirimpercent.formatNumber('#.')}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.itemContainers.template.paddingBottom = 100;
        chart.legend.labels.template.truncate = true;
        chart.legend.itemContainers.template.tooltipText = '';
        chart.legend.itemContainers.template.clickable = false;
        chart.legend.itemContainers.template.focusable = false;
        chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.default;
    }

    filter() {
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
        this.loadData();
    }

    toPercent() {
        this.router.navigate(['/dashboard-tagetvspo']);
    }

    download1() {
        this.loadingService.loadingStart();
        let tahun = '';
        let pilih = '';
        tahun = this.pilihTahun.toString();
        pilih = 'UM';
        const filter_data = 'test:' + tahun + '|sm:' + this.principal.getUserGroup();
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/dashboardrofo_jenis/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
    }
}
