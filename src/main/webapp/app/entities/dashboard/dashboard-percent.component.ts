import { Component, Input, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { LoadingService } from '../../layouts';
import { Router } from '@angular/router';
import { ResponseWrapper, Principal } from '../../shared';

@Component({
    selector: 'jhi-dashboard-percent',
    templateUrl: './dashboard-percent.component.html',

})
export class DashboardPercentComponent implements OnInit {
    @Input() pilihBulan: any
    @Input() pilihTahun: any
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    // bulan: Array<object> = [
    //     { label: '01', value: 1 },
    //     { label: '02', value: 2 },
    //     { label: '03', value: 3 },
    //     { label: '04', value: 4 },
    //     { label: '05', value: 5 },
    //     { label: '06', value: 6 },
    //     { label: '07', value: 7 },
    //     { label: '08', value: 8 },
    //     { label: '09', value: 9 },
    //     { label: '10', value: 10 },
    //     { label: '11', value: 11 },
    //     { label: '12', value: 12 }
    // ];
    // pilihBulan: number;
    // tahun: Array<object> = [
    //     { label: '2012', value: 2012 },
    //     { label: '2013', value: 2013 },
    //     { label: '2014', value: 2014 },
    //     { label: '2015', value: 2015 },
    //     { label: '2016', value: 2016 },
    //     { label: '2017', value: 2017 },
    //     { label: '2018', value: 2018 },
    //     { label: '2019', value: 2019 },
    //     { label: '2020', value: 2020 },
    //     { label: '2021', value: 2021 },
    //     { label: '2022', value: 2022 },
    //     { label: '2023', value: 2023 }
    // ];
    // pilihTahun: number;
    rayLSs: any[];
    rayBadaks: any[];
    currentAccount: any;
    currentGroupUSers: any;
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private principal: Principal,
    ) {
        this.date = new Date();
        // if (this.date.getMonth() + 2 > 12) {
        //     this.pilihBulan = this.date.getMonth();
        //     this.pilihTahun = this.date.getFullYear() + 1;
        // } else {
        //     this.pilihBulan = this.date.getMonth() + 2;
        //     this.pilihTahun = this.date.getFullYear();
        // }

    }

    ngOnInit() {
        this.dummyChart();
        this.loadData();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    loadData() {
        this.currentGroupUSers = this.principal.getUserGroup();
        this.loadingService.loadingStart();
        this.getAllDist();
        this.getAllDistBadak();
        this.getAllDistLasegar();
        this.getRayBadak();
        this.getRayLs();
        this.getWilayahDistBadak();
        this.getWilayahDistBLasegar();
        this.loadingService.loadingStop();
    }

    getWilayahDistBadak() {
        this.dashboardService.findWilayahByJenis(this.pilihBulan, this.pilihTahun, 'badak')
            .subscribe(
                (res) => {
                    this.setPODataBarWilayahByJenis('chartdivBarWilayahBadak', res);
                }
            )
    }

    getWilayahDistBLasegar() {
        this.dashboardService.findWilayahByJenis(this.pilihBulan, this.pilihTahun, 'lasegar')
            .subscribe(
                (res) => {
                    this.setPODataBarWilayahByJenis('chartdivBarWilayahLasegar', res);
                }
            )
    }
    getAllDist() {
        this.dashboardService.find(this.pilihBulan, this.pilihTahun)
            .subscribe(
                (res: any) => {
                    // this.setPODataPie(res);
                    this.setPODataBarAll(res)
                }
            );
    }

    getAllDistBadak() {
        this.dashboardService.findBadak(this.pilihBulan, this.pilihTahun, 'badak')
            .subscribe(
                (res: any) => {
                    // this.setPOBadakDataPie(res);
                    this.setPODataBarAllBadak(res)
                }
            );
    }
    getAllDistLasegar() {
        this.dashboardService.findLasegar(this.pilihBulan, this.pilihTahun, 'lasegar')
            .subscribe(
                (res: any) => {
                    // this.setPOLasegarDataPie(res);
                    this.setPODataBarAllLasegar(res)
                }
            );
    }

    setPODataBarAll(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarALl', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = [{
            'dist': 'Semua Distributor',
            'target': data[5],
            'po': data[6],
            'kirim': data[7],
            'bayar': data[9],
            'newtargetpercent': data[10],
            'newpopercent': data[11],
            'newkirimpercent': data[12],
            'newbayarpercent': data[14],
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirim';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');
        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{target.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#0000FF"><span style="color:white">{newpopercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirim.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayar.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

    }

    setPODataBarAllBadak(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarAllBadak', am4charts.XYChart);

        // Add data
        chart.data = [{
            'dist': 'Semua Distributor Badak',
            'target': data[5],
            'po': data[6],
            'kirim': data[7],
            'bayar': data[9],
            'newtargetpercent': data[10],
            'newpopercent': data[11],
            'newkirimpercent': data[12],
            'newbayarpercent': data[14],
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirim';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
        <tr>
            <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{target.formatNumber("#.")}</span></th>
        </tr>
        <tr>
        <th style="text-align:right; background-color:#0000FF"><span style="color:white">{newpopercent.formatNumber("#.")}</span></th>
        </tr>
        <tr>
            <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirim.formatNumber("#.")}</span></th>
        </tr>
        <tr>
            <th style="text-align:right; background-color:#000000"><span style="color:white">{bayar.formatNumber("#.")}</span></th>
        </tr>
    </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');

    }

    setPODataBarAllLasegar(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarAllLasegar', am4charts.XYChart);
        // Add data
        chart.data = [{
            'dist': 'Semua Distributor Lasegar',
            'target': data[5],
            'po': data[6],
            'kirim': data[7],
            'bayar': data[9],
            'newtargetpercent': data[10],
            'newpopercent': data[11],
            'newkirimpercent': data[12],
            'newbayarpercent': data[14],
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirim';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                            <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{target.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                            <th style="text-align:right; background-color:#0000FF"><span style="color:white">{newpopercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirim.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayar.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');

    }

    setPODataBarAllWilayah(data: any[], chartdiv: any, bgTarget: any, bgPO: any) {
        const chart = am4core.create(chartdiv, am4charts.XYChart);

        // Add data
        chart.data = data;

        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle'
        categoryAxis.renderer.labels.template.horizontalCenter = 'right'

        // categoryAxis.tooltipText = '{category}: [bold]{valueY}[/]';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.columns.template.fill = am4core.color(bgTarget); // green fill

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');
        series.columns.template.fillOpacity = .8

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.columns.template.fill = am4core.color(bgPO); // green fill
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');
        series2.columns.template.fillOpacity = .8;

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    getRayLs() {
        this.dashboardService.findRay('LSG', this.pilihTahun, this.pilihBulan)
            .subscribe(
                (res: any) => {
                    this.rayLSs = res.filter((item) => item.ray_desc1 !== null);
                    this.rayLSs.forEach(
                        (e) => {
                            this.dashboardService.findBar(this.pilihBulan, this.pilihTahun, 'lasegar', e.cust_ray).subscribe(
                                (resLS: any) => {
                                    const chartDiv = 'chartdivBarLasegar' + e.ray_desc1;
                                    console.log('INI CHART DIV LASEGAR == ', chartDiv);
                                    this.setPODataBarAllWilayahWithLoop(resLS, chartDiv);
                                });
                        }
                    )
                    console.log('Ray LASAEGAR == ', res);
                }
            )
    }

    getRayBadak() {
        this.dashboardService.findRay('BDK', this.pilihTahun, this.pilihBulan)
            .subscribe(
                (res: any) => {
                    this.rayBadaks = res.filter((item) => item.ray_desc1 !== null);
                    this.rayBadaks.forEach(
                        (e) => {
                            this.dashboardService.findBar(this.pilihBulan, this.pilihTahun, 'badak', e.cust_ray).subscribe(
                                (resBadak: any) => {
                                    const chartDiv = 'chartdivBarBadak' + e.ray_desc1;
                                    console.log('INI CHART DIV BADAK == ', chartDiv);
                                    this.setPODataBarAllWilayahWithLoop(resBadak, chartDiv);
                                });
                        }
                    )
                    console.log('Ray BADAK == ', res);
                }
            )
    }
    setPODataBarAllWilayahWithLoop(data: any[], chartdiv: any) {
        const chart = am4core.create(chartdiv, am4charts.XYChart);

        // Add data
        chart.data = data;

        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle'
        categoryAxis.renderer.labels.template.horizontalCenter = 'right'
        categoryAxis.renderer.labels.template.adapter.add('textOutput', function(text) {
            return text.replace(/ \(.*/, '');
        });

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimpercent';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran';
        series4.columns.template.fill = am4core.color('#000000'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                            <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                            <th style="text-align:right; background-color:#0000FF"><span style="color:white">{popercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // floating tooltip

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
        this.loadData();
    }

    setPODataBarWilayahByJenis(chartDiv: string, data: any) {
        // Create chart instance
        const chart = am4core.create(chartDiv, am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 0;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle';
        categoryAxis.renderer.labels.template.horizontalCenter = 'right';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]%';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'popercent';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]%';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimpercent';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]%';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayarpercent';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]%';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // floating tooltip
        series.tooltip.label.textAlign = 'end';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = ` <table style="width:100%">
                                            <tr>
                                            <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                            <th style="text-align:right; background-color:#0000FF"><span style="color:white">{popercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{kirimpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:right; background-color:#000000"><span style="color:white">{bayarpercent.formatNumber("#.")}</span></th>
                                            </tr>
                                        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
    }
    toValue() {
        this.router.navigate(['/dashboard-tagetvspo']);
    }
    dummyChart() {
        const chart = am4core.create('chartdivDummy', am4charts.XYChart);
        // Add data
        chart.data = [{
            'dist': '',
            'target': 0,
            'po': 0,
            'kirim': 0,
            'bayar': 0
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirim';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        const legendContainer = am4core.create('legenddiv', am4core.Container);
        legendContainer.width = am4core.percent(100);
        legendContainer.height = am4core.percent(100);
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.parent = legendContainer;
    }
}
