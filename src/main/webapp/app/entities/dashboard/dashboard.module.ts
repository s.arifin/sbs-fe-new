import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    ChartModule,
} from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { dashboardPopupRoute, DashboardResolvePagingParams, dashboardRoute } from './dashboard.route';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { DashboardPercentComponent } from './dashboard-percent.component';
import { TabDashboardComponent } from './tab-dashboard.component';
import { DashboardRofoComponent } from './dashboard-rofo.component';
import { TrendTabDashboardComponent } from './dashboard-trend/trend-tab-dashboard.component';
import { DashboardTrendPercentComponent } from './dashboard-trend/dashboard-trend-percent.component';
import { DashboardTrendComponent } from './dashboard-trend/dashboard-trend.component';
import { PppoTabDashboardComponent } from './dashboard-pp-po/pppo-tab-dashboard.component';
import { DashboardPpPoPercentComponent } from './dashboard-pp-po/dashboard-pppo-percent.component';
import { DashboardPppoComponent } from './dashboard-pp-po/dashboard-pppo.component';
import { TrendTabFkDashboardComponent } from './dashboard-trend/trend-tab-fk-dashboard.component';
import { DashboardTrendPercentFkComponent } from './dashboard-trend/dashboard-trend-percent-fk.component';
import { DashboardPercentFkComponent } from './dashboard-percent-fk.component';
import { DashboardTrendFkComponent } from './dashboard-trend/dashboard-trend-fk.component';
import { DashboardFkComponent } from './dashboard-fk.component';
import { KlaimTabDashboardComponent } from './dashboard-klaim/klaim-tab-dashboard.component';
import { DashboardKlaimComponent } from './dashboard-klaim/dashboard-klaim.component';
import { DashboardKlaimPercentComponent } from './dashboard-klaim/dashboard-klaim-percent.component';
import { TabDashboardProduksiComponent } from './dashboard-produksi/tab-dashboard-produksi.component';
const ENTITY_STATES = [
    ...dashboardRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        ChartModule
    ],
    declarations: [
        DashboardComponent,
        DashboardPercentComponent,
        TabDashboardComponent,
        DashboardRofoComponent,
        TrendTabDashboardComponent,
        DashboardTrendPercentComponent,
        DashboardTrendComponent,
        PppoTabDashboardComponent,
        DashboardPpPoPercentComponent,
        DashboardPppoComponent,
        TrendTabFkDashboardComponent,
        DashboardTrendPercentFkComponent,
        DashboardPercentFkComponent,
        DashboardTrendFkComponent,
        DashboardFkComponent,
        KlaimTabDashboardComponent,
        DashboardKlaimComponent,
        DashboardKlaimPercentComponent,
        TabDashboardProduksiComponent
    ],
    entryComponents: [
        DashboardComponent,
        DashboardPercentComponent,
        TabDashboardComponent,
        DashboardRofoComponent,
        TrendTabDashboardComponent,
        DashboardTrendPercentComponent,
        DashboardTrendComponent,
        PppoTabDashboardComponent,
        DashboardPpPoPercentComponent,
        DashboardPppoComponent,
        TrendTabFkDashboardComponent,
        DashboardTrendPercentFkComponent,
        DashboardPercentFkComponent,
        DashboardTrendFkComponent,
        DashboardFkComponent,
        KlaimTabDashboardComponent,
        DashboardKlaimComponent,
        DashboardKlaimPercentComponent,
        TabDashboardProduksiComponent
    ],
    providers: [
        DashboardService,
        DashboardResolvePagingParams
    ],
    exports: [
        DashboardTrendPercentFkComponent,
        DashboardPercentFkComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDashboardModule { }
