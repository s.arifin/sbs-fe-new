import { Component, OnInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Router } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { DashboardService } from '../dashboard.service';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';

@Component({
    selector: 'jhi-tab-dashboard-produksi',
    templateUrl: './tab-dashboard-produksi.component.html',

})
export class TabDashboardProduksiComponent implements OnInit {
    tanggal: Date;
    bulan: Array<object> = [
        { label: 'Daily', value: '1' },
        { label: 'Monthly', value: '2' },
        { label: 'Period', value: '3' },
    ];
    commontUtilService: any;
    selectType: any;
    selectBulan: any;
    selectBulan1: any;
    selectBulan2: any;
    selectTahun: any;
    selectTahun1: any;
    tgl1: Date;
    tgl2: Date;
    typeDash: any;
    daysOfYear = [];
    daysofYear1 = [];
    daysOfYear2 = [];
    daysofYear3 = [];
    arrayBadak = [];
    arrayLasegar = [];
    arrayLain = [];
    arrayDaily = [];
    arrayDaily1 = [];
    arrayDaily2 = [];
    arrayDaily3 = [];
    mesin = [
        { label: 'Semua Mesin', value: 'all' },
    ];

    selectMesin: any;
    bulan1: Array<object> = [
        { label: 'Pilih Bulan', value: '' },
        { label: 'Januari', value: '-01-01' },
        { label: 'Februari', value: '-02-01' },
        { label: 'Maret', value: '-03-01' },
        { label: 'April', value: '-04-01' },
        { label: 'Mei', value: '-05-01' },
        { label: 'Juni', value: '-06-01' },
        { label: 'Juli', value: '-07-01' },
        { label: 'Agustus', value: '-08-01' },
        { label: 'September', value: '-09-01' },
        { label: 'Oktober', value: '-10-01' },
        { label: 'November', value: '-11-01' },
        { label: 'Desember', value: '-12-01' },
    ];

    tahun1: Array<object> = [
        { label: 'Pilih Tahun', value: '' },
    ];
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
    ) {
        this.tanggal = new Date();
        this.selectType = '1';
        this.tgl1 = new Date();
        this.tgl2 = new Date();
        this.selectBulan = '';
        this.selectBulan1 = '';
        this.selectBulan2 = '';
        this.typeDash = 'daily';
        this.selectTahun = '';
        this.selectTahun1 = '';
        this.selectMesin = 'all';
    }

    ngOnInit() {
        this.getDays();
        this.dashboardService.getTahun({
            page: 0,
            size: 10000,
            sort: ['approvedBy', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            res.json.forEach((element) => {
                this.tahun1.push({
                    label: element.approvedBy,
                    value: element.approvedBy
                });
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
        this.dashboardService.getMesin({
            page: 0,
            size: 10000,
            sort: ['approvedBy', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            res.json.forEach((element) => {
                this.mesin.push({
                    label: element.approvedBy,
                    value: element.createdBy
                });
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }

    getDays() {
        this.daysOfYear = [];
        this.daysofYear1 = [];
        this.daysOfYear2 = [];
        this.daysofYear3 = [];
        let firstDay = new Date();
        const firstDay1 = new Date();
        let day = new Date();
        let day2 = new Date();
        const newMonth = new Date(this.tanggal.getFullYear(), this.tanggal.getMonth(), 1)
        let dayDas = '';
        let dayDas1 = '';
        if (this.selectTahun === null || this.selectTahun === '' || this.selectTahun === undefined) {
            firstDay = new Date(this.tanggal.getFullYear(), this.tanggal.getMonth(), 1);
            firstDay1.setDate(firstDay.getDate() - 7); // Kurangi 7 hari
        } else {
            firstDay = new Date(this.selectTahun, this.tanggal.getMonth(), 1);
            firstDay1.setDate(firstDay.getDate() - 7); // Kurangi 7 hari
        }
        const lastDay = new Date(this.tanggal.getFullYear(), this.tanggal.getMonth() + 1, 0);
        const lastDay2 = new Date(this.tanggal.getFullYear(), this.tanggal.getMonth(), 1);
        lastDay2.setDate(lastDay2.getDate() - 1);

        for (day = firstDay; day < this.tanggal; day.setDate(day.getDate() + 1)) {
            // your day is here
            this.daysOfYear.push(new Date(day));
            dayDas = day.getFullYear() + '-' + (day.getMonth() + 1) + '-' + day.getDate();

            this.daysofYear1.push(dayDas);

        }

        for (day2 = firstDay1; day2 < newMonth; day2.setDate(day2.getDate() + 1)) {
            // your day is here
            this.daysOfYear2.push(new Date(day2));
            dayDas1 = day2.getFullYear() + '-' + (day2.getMonth() + 1) + '-' + day2.getDate();
            this.daysofYear3.push(dayDas1);
        }
        this.daysOfYear.pop();
        this.daysOfYear.reverse();
        this.daysofYear1.pop();
        this.daysofYear1.reverse();
        // this.daysOfYear2.pop();
        // this.daysOfYear2.reverse();
        // this.daysofYear3.pop();
        // this.daysofYear3.reverse();
        this.loadData();
    }

    loadData() {
        this.loadingService.loadingStart();

        // GET DAILY DATA NON LOOPING
        this.dashboardService
            .findProdukDaily()
            .subscribe({
                next: (res: any) => {
                    res.forEach((element: any) => {
                        this.tambahkanNilai(this.arrayDaily, element.nm_mesin, element.nm_produk, element.qty);
                    });
                    this.setChartDaily(this.arrayDaily, res);
                    this.loadingService.loadingStop();
                },
                error: (error: any) => {
                    // Handle error jika diperlukan
                    console.error('Error:', error);
                    this.loadingService.loadingStop();
                }
            });
        // GET DAILY DATA NON LOOPING

        // GET DAILY DATA LOOPING
        this.dashboardService
            .findProdukDaily1()
            .subscribe(
                (res: ResponseWrapper) => this.onSuccess(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        // GET DAILY DATA LOOPING

        // GET DAILY DATA LOOPING
        this.dashboardService
            .findProdukDaily2()
            .subscribe(
                (res: ResponseWrapper) => this.onSuccess2(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        // GET DAILY DATA LOOPING

    }

    private onSuccess(data, headers) {
        data.forEach((element) => {
            element.data.forEach((element1) => {
                this.tambahkanNilai(this.arrayDaily1, element1.nm_mesin, element1.nm_produk, element1.qty);
            });
            this.setChartDaily1(this.arrayDaily1, element.tgl_palet, element.data);
        });
        this.loadingService.loadingStop();
    }

    private onSuccess2(data, headers) {
        data.forEach((element) => {
            element.data.forEach((element1) => {
                this.tambahkanNilai(this.arrayDaily2, element1.nm_mesin, element1.nm_produk, element1.qty);
            });
            this.setChartDaily2(this.arrayDaily2, element.tgl_palet, element.data);
        });
        this.loadingService.loadingStop();
    }

    private onSuccessMonthly(data, headers) {
        data.forEach((element) => {
            this.tambahkanNilai(this.arrayBadak, element.nm_mesin, element.nm_produk, element.qty);
        });
        this.setChartMonthlyBadak(this.arrayBadak, data);
        this.loadingService.loadingStop();
    }
    private onSuccessMonthly2(data, headers) {
        data.forEach((element) => {
            this.tambahkanNilai(this.arrayLasegar, element.nm_mesin, element.nm_produk, element.qty);
        });
        this.setChartMonthlyLasegar(this.arrayLasegar, data);
        this.loadingService.loadingStop();
    }

    private onSuccessMonthly3(data, headers) {
        data.forEach((element) => {
            this.tambahkanNilai(this.arrayLain, element.nm_mesin, element.nm_produk, element.qty);
        });
        this.setChartMonthlyLain(this.arrayLain, data);
        this.loadingService.loadingStop();
    }

    private onSuccessPeriod(data, headers) {
        data.forEach((element) => {
            this.tambahkanNilai(this.arrayBadak, element.nm_mesin, element.nm_produk, element.qty);
        });
        this.setChartPeriodBadak(this.arrayBadak, data);
        this.loadingService.loadingStop();
    }
    private onSuccessPeriod2(data, headers) {
        data.forEach((element) => {
            this.tambahkanNilai(this.arrayLasegar, element.nm_mesin, element.nm_produk, element.qty);
        });
        this.setChartPeriodLasegar(this.arrayLasegar, data);
        this.loadingService.loadingStop();
    }

    private onSuccessPeriod3(data, headers) {
        data.forEach((element) => {
            this.tambahkanNilai(this.arrayLain, element.nm_mesin, element.nm_produk, element.qty);
        });
        this.setChartPeriodLain(this.arrayLain, data);
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.loadingService.loadingStop();
    }

    tambahkanNilai(arr: any[], kategori: string, prop: string, nilai: number) {
        const existingItem = arr.find((item) => item.nm_mesin === kategori);

        if (existingItem) {
            // Jika kategori sudah ada, tambahkan properti dan nilai baru ke dalam objek
            existingItem[prop] = nilai;
        } else {
            // Jika kategori belum ada, buat objek baru dan tambahkan ke dalam array
            const newObj: any = { nm_mesin: kategori };
            newObj[prop] = nilai;
            arr.push(newObj);
        }
    }

    reverseData() {
        if (this.selectType === '1') {
            this.selectBulan = '';
            this.selectBulan1 = '';
            this.selectBulan2 = '';
            this.selectTahun = '';
            this.selectTahun1 = '';
        } else if (this.selectType === '2') {
            this.selectBulan1 = '';
            this.selectBulan2 = '';
            this.selectTahun1 = '';
        } else {
            this.selectBulan = '';
            this.selectTahun = '';
        }
    }

    getData() {
        this.loadingService.loadingStart();
        if (this.selectType === '1') {

            this.typeDash = 'daily';
            this.getDays();

        } else if (this.selectType === '2') {
            if ((this.selectTahun === undefined || this.selectTahun === null || this.selectTahun === '')
                && (this.selectBulan === undefined || this.selectBulan === null || this.selectBulan === '')) {
                alert('BULAN DAN TAHUN HARUS DIPILIH');
                this.loadingService.loadingStop();
            } else {
                this.daysOfYear = [];
                this.daysofYear1 = [];
                this.daysOfYear2 = [];
                this.daysofYear3 = [];
                const periode = this.selectTahun + this.selectBulan
                this.typeDash = 'monthly';
                this.dashboardService
                    .findProdukMonthlyBdk(periode)
                    .subscribe(
                        (res: ResponseWrapper) => this.onSuccessMonthly(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );

                this.dashboardService
                    .findProdukMonthlyLsg(periode)
                    .subscribe(
                        (res: ResponseWrapper) => this.onSuccessMonthly2(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );

                this.dashboardService
                    .findProdukMonthlyLain(periode)
                    .subscribe(
                        (res: ResponseWrapper) => this.onSuccessMonthly3(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );
            }

        } else if (this.selectType === '3') {
            if ((this.selectTahun1 === undefined || this.selectTahun1 === null || this.selectTahun1 === '')
                || (this.selectBulan1 === undefined || this.selectBulan1 === null || this.selectBulan1 === '')
                || (this.selectBulan2 === undefined || this.selectBulan2 === null || this.selectBulan2 === '')) {
                alert('BULAN DARI DAN BULAN SAMPAI DAN TAHUN HARUS DIPILIH');
                this.loadingService.loadingStop();
            } else {
                this.daysOfYear = [];
                this.daysofYear1 = [];
                this.daysOfYear2 = [];
                this.daysofYear3 = [];
                const periode = this.selectTahun1 + this.selectBulan1;
                const periode1 = this.selectTahun1 + this.selectBulan2;
                this.typeDash = 'period';
                this.dashboardService
                    .findProdukPeriodBdk(periode, periode1)
                    .subscribe(
                        (res: ResponseWrapper) => this.onSuccessPeriod(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );

                this.dashboardService
                    .findProdukPeriodLsg(periode, periode1)
                    .subscribe(
                        (res: ResponseWrapper) => this.onSuccessPeriod2(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );

                this.dashboardService
                    .findProdukPeriodLain(periode, periode1)
                    .subscribe(
                        (res: ResponseWrapper) => this.onSuccessPeriod3(res, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );
            }
        }
    }

    setChartDaily(data: any, data1: any) {
        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivDaily', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }

    setChartDaily1(data: any, element: any, data1: any) {
        am4core.useTheme(am4themes_animated);

        // Create chart instance
        const chart = am4core.create(`chartDaily_${element}`, am4charts.XYChart);
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element1) => {
            createSeries(element1, element1);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        this.arrayDaily1 = [];
    }

    setChartDaily2(data: any, element: any, data1: any) {
        am4core.useTheme(am4themes_animated);

        // Create chart instance
        const chart = am4core.create(`chartDaily2_${element}`, am4charts.XYChart);
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element1) => {
            createSeries(element1, element1);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        this.arrayDaily2 = [];
    }

    setChartMonthlyBadak(data: any, data1: any) {

        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivBarMonthlyBdk', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }

    setChartMonthlyLasegar(data: any, data1: any) {

        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivBarMonthlyLsg', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }

    setChartMonthlyLain(data: any, data1: any) {

        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivBarMonthlyLain', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }

    setChartPeriodBadak(data: any, data1: any) {

        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivBarMonthlyBdk1', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }

    setChartPeriodLasegar(data: any, data1: any) {

        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivBarMonthlyLsg1', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }

    setChartPeriodLain(data: any, data1: any) {

        am4core.useTheme(am4themes_animated);
        // Themes end

        const chart = am4core.create('chartdivBarMonthlyLain1', am4charts.XYChart)
        chart.colors.step = 2;
        chart.data = data;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'bottom'
        chart.legend.labels.template.maxWidth = 300
        chart.legend.fontSize = 10;
        chart.legend.paddingTop = 10;

        const xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'nm_mesin'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 10;
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = 'right';
        xAxis.renderer.labels.template.verticalCenter = 'middle';
        const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.title.text = 'Jumlah';
        yAxis.title.fontWeight = '900';
        yAxis.strictMinMax = true;
        yAxis.min = 0;

        function createSeries(value, name) {
            const series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value;
            series.dataFields.categoryX = 'nm_mesin'
            series.name = name;
            series.fontSize = 14;
            series.tooltip.label.fontSize = 18;
            series.tooltipText = '{name} : [bold]{valueY}[/]';
            series.columns.template.width = am4core.percent(200);

            series.events.on('hidden', arrangeColumns);
            series.events.on('shown', arrangeColumns);

            const bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        // Menggunakan Set untuk menyimpan nilai unik dari nm_produk

        // Mengonversi Set kembali ke dalam array (jika diperlukan)
        const uniqueProdukTracker: { [key: string]: boolean } = {};
        const uniqueProdukArray: string[] = [];

        // Loop melalui records dan menyaring nilai unik
        data1.forEach((record) => {
            const { nm_produk } = record;
            if (!uniqueProdukTracker[nm_produk]) {
                uniqueProdukTracker[nm_produk] = true;
                uniqueProdukArray.push(nm_produk);
            }
        });

        uniqueProdukArray.forEach((element) => {
            createSeries(element, element);
        });
        function arrangeColumns() {

            const series = chart.series.getIndex(0);

            const w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                const x0 = xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
                const x1 = xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
                const delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    const middle = chart.series.length / 2;

                    let newIndex = 0;
                    chart.series.each(function(series1) {
                        if (!series1.isHidden && !series1.isHiding) {
                            series1.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series1.dummyData = chart.series.indexOf(series1);
                        }
                    })
                    const visibleCount = newIndex;
                    const newMiddle = visibleCount / 2;

                    chart.series.each(function(series2) {
                        const trueIndex = chart.series.indexOf(series2);
                        const newIndex1 = series2.dummyData;

                        const dx = (newIndex1 - trueIndex + middle - newMiddle) * delta

                        series2.animate({ property: 'dx', to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series2.bulletsContainer.animate({ property: 'dx', to: dx }, series2.interpolationDuration, series2.interpolationEasing);
                    })
                }
            }
        }

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
    }
}
