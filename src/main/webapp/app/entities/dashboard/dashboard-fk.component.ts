import { Component, Input, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_dataviz from '@amcharts/amcharts4/themes/dataviz';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { LoadingService } from '../../layouts';
import { ResponseWrapper, Principal } from '../../shared';
import { Router } from '@angular/router';

@Component({
    selector: 'jhi-dashboard-fk',
    templateUrl: './dashboard-fk.component.html',

})
export class DashboardFkComponent implements OnInit {
    @Input() pilihBulan: any
    @Input() pilihTahun: any
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    rayLSs: any[];
    rayBadaks: any[];
    currentAccount: any;
    currentGroupUSers: any;
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private principal: Principal,
    ) {
        this.date = new Date();
    }

    ngOnInit() {
        this.loadData();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    loadData() {
        this.currentGroupUSers = this.principal.getUserGroup();
        this.loadingService.loadingStart();
        this.getAllDist();
        this.getAllDistBadak();
        this.getAllDistLasegar();
        this.getRayBadak();
        this.getRayLs();
        this.getWilayahDistBadak();
        this.getWilayahDistBLasegar();
        this.loadingService.loadingStop();
    }

    getWilayahDistBadak() {
        this.dashboardService.findWilayahByJenis(this.pilihBulan, this.pilihTahun, 'badak')
            .subscribe(
                (res) => {
                    this.setPODataBarWilayahByJenis('chartdivBarWilayahBadakfk1', res);
                }
            )
    }

    getWilayahDistBLasegar() {
        this.dashboardService.findWilayahByJenis(this.pilihBulan, this.pilihTahun, 'lasegar')
            .subscribe(
                (res) => {
                    this.setPODataBarWilayahByJenis('chartdivBarWilayahLasegarfk1', res);
                }
            )
    }
    getAllDist() {
        this.dashboardService.find(this.pilihBulan, this.pilihTahun)
            .subscribe(
                (res: any) => {
                    // this.setPODataPie(res);
                    this.setPODataBarAll(res)
                }
            );
    }

    getAllDistBadak() {
        this.dashboardService.findBadak(this.pilihBulan, this.pilihTahun, 'badak')
            .subscribe(
                (res: any) => {
                    // this.setPOBadakDataPie(res);
                    this.setPODataBarAllBadak(res)
                }
            );
    }
    getAllDistLasegar() {
        this.dashboardService.findLasegar(this.pilihBulan, this.pilihTahun, 'lasegar')
            .subscribe(
                (res: any) => {
                    // this.setPOLasegarDataPie(res);
                    this.setPODataBarAllLasegar(res)
                }
            );
    }

    setPODataBarAll(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarALlfk1', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = [{
            'dist': 'Semua Distributor',
            'target': data[0],
            'po': data[1],
            'kirimfaktur': data[3],
            'bayar': data[4]
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.strictMinMax = true;
        valueAxis.min = 0;
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfaktur';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // // Create axes
        // const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        // categoryAxis.dataFields.category = 'dist';
        // // categoryAxis.title.text = 'Distributor Nasional';
        // categoryAxis.renderer.grid.template.location = 0;
        // categoryAxis.renderer.minGridDistance = 10;

        // const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        // valueAxis.title.text = 'Jumlah (%)';
        // valueAxis.calculateTotals = true;
        // valueAxis.min = 0;
        // valueAxis.max = 100;
        // valueAxis.strictMinMax = true;
        // valueAxis.renderer.labels.template.adapter.add('text', function (text) {
        //     return text + '%';
        // });

        // // Create series
        // const series = chart.series.push(new am4charts.ColumnSeries());
        // series.dataFields.valueY = 'target';
        // series.dataFields.valueYShow = 'totalPercent';
        // series.dataFields.categoryX = 'dist';
        // series.name = 'Target PO';
        // series.tooltipText = '{name}: [bold]{valueY}[/]';
        // // series.clustered = false;
        // series.stacked = false;
        // series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        // series.columns.template.width = am4core.percent(10);

        // const series2 = chart.series.push(new am4charts.ColumnSeries());
        // series2.dataFields.valueY = 'po';
        // series2.dataFields.valueYShow = 'totalPercent';
        // series2.dataFields.categoryX = 'dist';
        // series2.name = 'Jumlah PO';
        // series2.tooltipText = '{name}: [bold]{valueY}[/]';
        // // series2.clustered = false;
        // series2.stacked = false;
        // series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        // series2.columns.template.width = am4core.percent(10);

        // const series3 = chart.series.push(new am4charts.ColumnSeries());
        // series3.dataFields.valueY = 'kirimfaktur';
        // series3.dataFields.valueYShow = 'totalPercent';
        // series3.dataFields.categoryX = 'dist';
        // series3.name = 'Pengiriman PO';
        // series3.tooltipText = '{name}: [bold]{valueY}[/]';
        // // series3.clustered = false;
        // series3.stacked = false;
        // series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        // series3.columns.template.width = am4core.percent(10);

        // const series4 = chart.series.push(new am4charts.ColumnSeries());
        // series4.dataFields.valueY = 'bayar';
        // series4.dataFields.valueYShow = 'totalPercent';
        // series4.dataFields.categoryX = 'dist';
        // series4.name = 'Pembayaran PO';
        // series4.tooltipText = '{name}: [bold]{valueY}[/]';
        // // series4.clustered = false;
        // series4.stacked = false;
        // series4.columns.template.fill = am4core.color('#000000'); // Pembayaran Hitam
        // series4.columns.template.width = am4core.percent(10);
        // // Add cursor
        // chart.cursor = new am4charts.XYCursor();

        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';

    }

    setPODataBarAllBadak(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarAllBadakfk1', am4charts.XYChart);

        // Add data
        chart.data = [{
            'dist': 'Semua Distributor Badak',
            'target': data[0],
            'po': data[1],
            'kirimfaktur': data[3],
            'bayar': data[4]
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfaktur';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    setPODataBarAllLasegar(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarAllLasegarfk1', am4charts.XYChart);
        // Add data
        chart.data = [{
            'dist': 'Semua Distributor Badak',
            'target': data[0],
            'po': data[1],
            'kirimfaktur': data[3],
            'bayar': data[4]
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfaktur';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    setPODataBarAllWilayah(data: any[], chartdiv: any, bgTarget: any, bgPO: any) {
        const chart = am4core.create(chartdiv, am4charts.XYChart);

        // Add data
        chart.data = data;

        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle'
        categoryAxis.renderer.labels.template.horizontalCenter = 'right'

        // categoryAxis.tooltipText = '{category}: [bold]{valueY}[/]';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.columns.template.fill = am4core.color(bgTarget); // green fill

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');
        series.columns.template.fillOpacity = .8

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.columns.template.fill = am4core.color(bgPO); // green fill
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');
        series2.columns.template.fillOpacity = .8;

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    getRayLs() {
        this.dashboardService.findRay('LSG', this.pilihTahun, this.pilihBulan)
            .subscribe(
                (res: any) => {
                    this.rayLSs = res;
                    this.rayLSs.forEach(
                        (e) => {
                            this.dashboardService.findBar(this.pilihBulan, this.pilihTahun, 'lasegar', e.cust_ray).subscribe(
                                (resLS: any) => {
                                    const chartDiv = 'chartdivBarLasegarfk1' + e.ray_desc1;
                                    console.log('INI CHART DIV LASEGAR == ', chartDiv);
                                    this.setPODataBarAllWilayahWithLoop(resLS, chartDiv);
                                });
                        }
                    )
                    console.log('Ray LASAEGAR == ', res);
                }
            )
    }

    getRayBadak() {
        this.dashboardService.findRay('BDK', this.pilihTahun, this.pilihBulan)
            .subscribe(
                (res: any) => {
                    this.rayBadaks = res;
                    this.rayBadaks.forEach(
                        (e) => {
                            this.dashboardService.findBar(this.pilihBulan, this.pilihTahun, 'badak', e.cust_ray).subscribe(
                                (resBadak: any) => {
                                    const chartDiv = 'chartdivBarBadakfk1' + e.ray_desc1;
                                    console.log('INI CHART DIV BADAK == ', chartDiv);
                                    this.setPODataBarAllWilayahWithLoop(resBadak, chartDiv);
                                });
                        }
                    )
                    console.log('Ray BADAK == ', res);
                }
            )
    }
    setPODataBarAllWilayahWithLoop(data: any[], chartdiv: any) {
        const chart = am4core.create(chartdiv, am4charts.XYChart);

        // Add data
        chart.data = data;

        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle'
        categoryAxis.renderer.labels.template.horizontalCenter = 'right'
        categoryAxis.renderer.labels.template.adapter.add('textOutput', function(text) {
            return text.replace(/ \(.*/, '');
          });

        // categoryAxis.tooltipText = '{category}: [bold]{valueY}[/]';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;

        // // Create series
        // const series = chart.series.push(new am4charts.ColumnSeries3D());
        // series.dataFields.valueY = 'target';
        // series.dataFields.categoryX = 'dist';
        // series.name = 'Target PO';
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        // series.columns.template.fill = am4core.color(bgTarget); // green fill

        // const columnTemplate1 = series.columns.template;
        // columnTemplate1.strokeWidth = 2;
        // columnTemplate1.strokeOpacity = 1;
        // columnTemplate1.stroke = am4core.color('#FFFFFF');
        // series.columns.template.fillOpacity = .8

        // const series2 = chart.series.push(new am4charts.ColumnSeries3D());
        // series2.dataFields.valueY = 'po';
        // series2.dataFields.categoryX = 'dist';
        // series2.name = 'Jumlah PO';
        // series2.columns.template.fill = am4core.color(bgPO); // green fill
        // series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';

        // const columnTemplate2 = series2.columns.template;
        // columnTemplate2.strokeWidth = 2;
        // columnTemplate2.strokeOpacity = 1;
        // columnTemplate2.stroke = am4core.color('#FFFFFF');
        // series2.columns.template.fillOpacity = .8;

        // chart.cursor = new am4charts.XYCursor();
        // chart.cursor.lineX.disabled = true;
        // chart.cursor.lineY.disabled = true;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfaktur';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

    }

    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
        this.loadData();
    }

    dummyChart() {
        // let percenttarget: any;
        // let percentPO: any;
        // let percentKirim: any;
        // let percent: any;
        am4core.useTheme(am4themes_kelly);
        am4core.useTheme(am4themes_animated);
        // Create chart instance
        const chart = am4core.create('chartdivDummy', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = [{
            'dist': 'PUTRI AYU NUSANTARA,PT',
            'target': 212520000,
            'po': 37950000,
            'kirimfaktur': 37950000,
            'bayar': 17950000
        }, {
            'dist': 'ANUGERAH FAJAR,PD',
            'target': 724000000,
            'po': 724000000,
            'kirimfaktur': 724000000,
            'bayar': 424000000
        }, {
            'dist': 'BINTANG SARIMAS BATAM,PT',
            'target': 358248000,
            'po': 140070000,
            'kirimfaktur': 79350000,
            'bayarfaktur': 59350000
        }, {
            'dist': 'ALAMJAYA WIRASENTOSA,PT',
            'target': 382260000,
            'po': 69000000,
            'kirimfaktur': 69000000,
            'bayar': 23400000
        }];
        console.log('INI DATA DUMMY = ', chart.data);
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfaktur';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';

        am4core.unuseTheme(am4themes_kelly);
        am4core.unuseTheme(am4themes_animated);

    }

    setPODataBarWilayahByJenis(chartDiv: string, data: any) {
        // Create chart instance
        const chart = am4core.create(chartDiv, am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'dist';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 0;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle';
        categoryAxis.renderer.labels.template.horizontalCenter = 'right';

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'target';
        series.dataFields.categoryX = 'dist';
        series.clustered = false;
        series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'po';
        series2.dataFields.categoryX = 'dist';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        series2.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series2.name = 'Jumlah PO';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'kirimfaktur';
        series3.dataFields.categoryX = 'dist';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        series3.tooltipText = 'JUmlah Pengiriman : [bold]{valueY}[/]';
        series3.name = 'Pengiriman PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'bayar';
        series4.dataFields.categoryX = 'dist';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        series4.tooltipText = 'Pembayaran : [bold]{valueY}[/]';
        series4.name = 'Pembayaran PO';
        series4.columns.template.fill = am4core.color('#000000	'); // Pembayaran Hitam

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';
    }

    toPercent() {
        this.router.navigate(['/dashboard-tagetvspo']);
    }
}
