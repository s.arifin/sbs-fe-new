import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { DashboardService } from '../dashboard.service';

@Component({
    selector: 'jhi-dashboard-rofo',
    templateUrl: './dashboard-rofo.component.html',

})
export class DashboardRofoComponent implements OnInit {
    @Input() pilihBulan: any
    @Input() pilihTahun: any
    public data: any;
    public dataBadak: any;
    public dataLasegar: any;
    public dataBarBadak: any;
    public dataBarLasegar: any;
    public date: Date;
    public options: any;
    public dataBarAll: any;
    periode: any;
    div: string;
    produkLS: string;
    produkLP: string;
    rayLSs: any[];
    rayBadaks: any[];
    // bulan: Array<object> = [
    //     { label: '01', value: 1 },
    //     { label: '02', value: 2 },
    //     { label: '03', value: 3 },
    //     { label: '04', value: 4 },
    //     { label: '05', value: 5 },
    //     { label: '06', value: 6 },
    //     { label: '07', value: 7 },
    //     { label: '08', value: 8 },
    //     { label: '09', value: 9 },
    //     { label: '10', value: 10 },
    //     { label: '11', value: 11 },
    //     { label: '12', value: 12 }
    // ];
    // pilihBulan: number;
    // tahun: Array<object> = [
    //     { label: '2012', value: 2012 },
    //     { label: '2013', value: 2013 },
    //     { label: '2014', value: 2014 },
    //     { label: '2015', value: 2015 },
    //     { label: '2016', value: 2016 },
    //     { label: '2017', value: 2017 },
    //     { label: '2018', value: 2018 },
    //     { label: '2019', value: 2019 },
    //     { label: '2020', value: 2020 },
    //     { label: '2021', value: 2021 },
    //     { label: '2022', value: 2022 },
    //     { label: '2023', value: 2023 }
    // ];
    // pilihTahun: number;
    divs: Array<object> = [
        // { label: ' --- PILIH --- ', value: '' },
        { label: 'Lasegar', value: 'LS' },
        { label: 'Badak ', value: 'LP' },
        // { label: 'Lian - Lain ', value: 'XX' },
    ];
    produkLasegars: Array<object> = [
        { label: 'Lasegar botol 500', value: 'LSG50' },
        { label: 'Lasegar botol 200', value: 'LSG20' },
        { label: 'Lasegar kaleng 320', value: 'LSGXX' },
        { label: 'Lasegar kaleng 238', value: 'LSKDX' },
        { label: 'Lasegar sachet', value: 'LSPXX' },
        { label: 'Lasegar container', value: 'LSGX2' }
    ];
    produkLPs: Array<object> = [
        { label: 'Badak botol 500', value: 'LCKB5' },
        { label: 'Badak botol 200', value: 'LCKT2' },
        { label: 'Badak kaleng 320', value: 'LCKXX' },
        { label: 'Badak kaleng 238', value: 'LLPXX' },
        { label: 'Badak sachet', value: 'PLPXX' },
        { label: 'Badak container', value: 'LCKXX' }
    ];
    listProdukLS: string[];
    listProdukLP: string[];
    constructor(
        private dashboardService: DashboardService,
        private loadingService: LoadingService,
        private router: Router,
        private reportUtilService: ReportUtilService,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 2 > 12) {
            this.pilihBulan = this.date.getMonth();
            this.pilihTahun = this.date.getFullYear() + 1;
        } else {
            this.pilihBulan = this.date.getMonth() + 2;
            this.pilihTahun = this.date.getFullYear();
        }
        this.div = '';
        this.produkLS = '';
        this.produkLP = '';
        this.listProdukLS = new Array<string>();
        this.listProdukLP = new Array<string>();
    }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        this.loadingService.loadingStart();
        // this.getAllDist();
        // this.getItemMailBadak();
        // this.getItemMailLasegar();
        this.loadingService.loadingStop();
    }
    // getAllDist() {
    //     this.dashboardService.findRofo(this.pilihBulan, this.pilihTahun)
    //         .subscribe(
    //             (res: any) => {
    //                 // this.setPODataPie(res);
    //                 this.setPODataBarAll(res)
    //             }
    //         );
    // }

    setPODataBarAll(data: any) {
        // Create chart instance
        const chart = am4core.create('chartdivBarALl', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = data;
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'descmail';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'descmail';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'mrofopercent';
        series2.dataFields.categoryX = 'descmail';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah Rofo : [bold]{valueY}[/]';
        series2.name = 'Jumlah Rofo';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'popercent';
        series3.dataFields.categoryX = 'descmail';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah PO : [bold]{valueY}[/]';
        series3.name = 'Jumlah PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'kirimpercent';
        series4.dataFields.categoryX = 'descmail';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Jumlah Kirim : [bold]{valueY}[/]';
        series4.name = 'Jumlah Kirim';
        series4.columns.template.fill = am4core.color('#000000'); // Rofo Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        // floating tooltip
        series.tooltip.label.textAlign = 'start';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = `<table>
            <tr>
                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
            </tr>
            <tr>
                <th style="text-align:right; background-color:#0000FF"><span style="color:white">{mrofopercent.formatNumber("#.")}</span></th>
            </tr>
            <tr>
                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{popercent.formatNumber("#.")}</span></th>
            </tr>
            <tr>
                <th style="text-align:right; background-color:#000000"><span style="color:white">{kirimpercent.formatNumber("#.")}</span></th>
            </tr>
        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
        // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';

        // const legendContainer = am4core.create('legenddiv', am4core.Container);
        // legendContainer.width = am4core.percent(100);
        // legendContainer.height = am4core.percent(100);
        // // Create LEGEND
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = 'top';
        // chart.legend.parent = legendContainer;

    }
    setPODataBarAllWilayahWithLoop(data: any, chartdiv: any) {
        const chart = am4core.create(chartdiv, am4charts.XYChart);

        // Add data
        chart.data = data;

        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'ray';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;
        categoryAxis.renderer.labels.template.rotation = 300;
        categoryAxis.renderer.labels.template.verticalCenter = 'middle'
        categoryAxis.renderer.labels.template.horizontalCenter = 'right'

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });

        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'ray';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'mrofopercent';
        series2.dataFields.categoryX = 'ray';
        series2.clustered = false;
        // series2.tooltipText = 'Jumlah Rofo : [bold]{valueY}[/]';
        series2.name = 'Jumlah Rofo';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'popercent';
        series3.dataFields.categoryX = 'ray';
        series3.clustered = false;
        // series3.tooltipText = 'Jumlah PO : [bold]{valueY}[/]';
        series3.name = 'Jumlah PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series5 = chart.series.push(new am4charts.ColumnSeries());
        series5.dataFields.valueY = 'kirimpercent';
        series5.dataFields.categoryX = 'ray';
        series5.clustered = false;
        // series5.tooltipText = 'Jumlah Kirim : [bold]{valueY}[/]';
        series5.name = 'Jumlah Kirim';
        series5.columns.template.fill = am4core.color('#000000'); // Rofo Hitam

        const columnTemplate5 = series5.columns.template;
        columnTemplate5.strokeWidth = 2;
        columnTemplate5.strokeOpacity = 1;
        columnTemplate5.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // floating tooltip
        series.tooltip.label.textAlign = 'start';
        series.tooltip.pointerOrientation = 'down';
        series.tooltip.dy = -5;
        series.tooltip.label.fontSize = 10;
        columnTemplate1.tooltipHTML = `<table>
            <tr>
                <th style="text-align:right; background-color:#FFFF00"><span style="color:black">{targetpercent.formatNumber("#.")}</span></th>
            </tr>
            <tr>
            <th style="text-align:right; background-color:#0000FF"><span style="color:white">{mrofopercent.formatNumber("#.")}</span></th>
            </tr>
            <tr>
                <th style="text-align:right; background-color:#00FF00"><span style="color:black">{popercent.formatNumber("#.")}</span></th>
            </tr>
            <tr>
                <th style="text-align:right; background-color:#000000"><span style="color:white">{kirimpercent.formatNumber("#.")}</span></th>
            </tr>
        </table>`;
        columnTemplate1.showTooltipOn = 'always';
        columnTemplate1.tooltipY = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color('#ffffff00');
        series.tooltip.background.stroke = am4core.color('#ffffff00');
    }

    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
        this.loadData();
    }

    toPercent() {
        this.router.navigate(['/dashboard-tagetvspo']);
    }
    cek() {
        console.log('INI DATA DIV = ', this.div);
        console.log('INI DATA LS = ', this.produkLS);
        console.log('INI DATA LP = ', this.produkLP);
    }

    getItemMailBadak() {
        this.dashboardService.findItemMail(this.pilihBulan, this.pilihTahun, 'badak')
            .subscribe(
                (res: string[]) => {
                    this.listProdukLP = res;
                    console.log('ini adalah item mail Badak = ', this.listProdukLP)
                    this.listProdukLP.forEach(
                        (e) => {
                            this.dashboardService.findRofoJenisWilayah(this.pilihBulan, this.pilihTahun, e)
                                .subscribe(
                                    (resBadak: any) => {
                                        console.log('ini adalah rofo Badak = ', resBadak);
                                        this.setPODataBarAllWilayahWithLoop(resBadak, e);
                                    }
                                );
                        }
                    )
                }
            )
    }
    getItemMailLasegar() {
        this.dashboardService.findItemMail(this.pilihBulan, this.pilihTahun, 'lasegar')
            .subscribe(
                (res: string[]) => {
                    this.listProdukLS = res;
                    console.log('ini adalah item mail Lasegar = ', this.listProdukLS);
                    this.listProdukLS.forEach(
                        (e) => {
                            this.dashboardService.findRofoJenisWilayah(this.pilihBulan, this.pilihTahun, e)
                                .subscribe(
                                    (resLs: any) => {
                                        console.log('ini adalah rofo Lasegar = ', resLs);
                                        this.setPODataBarAllWilayahWithLoop(resLs, e);
                                    }
                                );
                        }
                    )
                }
            )
    }

    download() {
        this.loadingService.loadingStart();
        let bulan = '';
        let tahun = '';
        let strNamaBulan = ''
        if (this.pilihBulan === 1) { strNamaBulan = 'JANUARI'; }
        if (this.pilihBulan === 2) { strNamaBulan = 'FEBRUARY'; }
        if (this.pilihBulan === 3) { strNamaBulan = 'MARET'; }
        if (this.pilihBulan === 4) { strNamaBulan = 'APRIL'; }
        if (this.pilihBulan === 5) { strNamaBulan = 'MEI'; }
        if (this.pilihBulan === 6) { strNamaBulan = 'JUNI'; }
        if (this.pilihBulan === 7) { strNamaBulan = 'JULI'; }
        if (this.pilihBulan === 8) { strNamaBulan = 'AGUSTUS'; }
        if (this.pilihBulan === 9) { strNamaBulan = 'SEPTEMBER'; }
        if (this.pilihBulan === 10) { strNamaBulan = 'OKTOBER'; }
        if (this.pilihBulan === 11) { strNamaBulan = 'NOVEMBER'; }
        if (this.pilihBulan === 12) { strNamaBulan = 'DESEMBER'; }
        if (this.pilihBulan.toString().length === 1) {
            bulan = '0' + this.pilihBulan;
        }
        if (this.pilihBulan.toString().length > 1) {
            bulan = this.pilihBulan.toString();
        }
        tahun = this.pilihTahun.toString().substring(2);
        const filter_data = 'bulan:' + bulan + '|tahun:' + tahun + '|namabulan:' + strNamaBulan;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/dashboardrofo/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

    dummychart() {
        // Create chart instance
        const chart = am4core.create('chartdivBarALl', am4charts.XYChart);
        // Add percent sign to all numbers
        // chart.numberFormatter.numberFormat = '#.#\'%\'';

        // Add data
        chart.data = [{
            'item_mail': null,
            'descmail': 'All Product Category',
            'ray': null,
            'item_code': null,
            'target': 0,
            'po': 0,
            'kirim': 0,
            'mrofo': 0,
            'targetpercent': 0,
            'popercent': 0,
            'kirimpercent': 0,
            'mrofopercent': 0,
            'sm': null
        }];
        // Create axes
        const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'descmail';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = 'Jumlah';
        valueAxis.title.fontWeight = '800';
        valueAxis.min = 0;
        valueAxis.strictMinMax = true;
        valueAxis.renderer.labels.template.adapter.add('text', function(text) {
            return text + '%';
        });
        // Create series
        const series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = 'targetpercent';
        series.dataFields.categoryX = 'descmail';
        series.clustered = false;
        // series.tooltipText = 'Target PO : [bold]{valueY}[/]';
        series.name = 'Target PO';
        series.columns.template.fill = am4core.color('#FFFF00'); // Target Kuning
        series.columns.template.width = am4core.percent(10);

        const columnTemplate1 = series.columns.template;
        columnTemplate1.strokeWidth = 2;
        columnTemplate1.strokeOpacity = 1;
        columnTemplate1.stroke = am4core.color('#FFFFFF');

        const series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = 'mrofopercent';
        series2.dataFields.categoryX = 'descmail';
        series2.clustered = false;
        // series2.columns.template.width = am4core.percent(75);
        // series2.tooltipText = 'Jumlah Rofo : [bold]{valueY}[/]';
        series2.name = 'Jumlah Rofo';
        series2.columns.template.fill = am4core.color('#0000FF'); // PO Biru
        series2.columns.template.width = am4core.percent(10);

        const columnTemplate2 = series2.columns.template;
        columnTemplate2.strokeWidth = 2;
        columnTemplate2.strokeOpacity = 1;
        columnTemplate2.stroke = am4core.color('#FFFFFF');

        const series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.dataFields.valueY = 'popercent';
        series3.dataFields.categoryX = 'descmail';
        series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = 'JUmlah PO : [bold]{valueY}[/]';
        series3.name = 'Jumlah PO';
        series3.columns.template.fill = am4core.color('#00FF00'); // pengiriman Hijau
        series3.columns.template.width = am4core.percent(10);

        const columnTemplate3 = series3.columns.template;
        columnTemplate3.strokeWidth = 2;
        columnTemplate3.strokeOpacity = 1;
        columnTemplate3.stroke = am4core.color('#FFFFFF');

        const series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.dataFields.valueY = 'kirimpercent';
        series4.dataFields.categoryX = 'descmail';
        series4.clustered = false;
        // series4.columns.template.width = am4core.percent(25);
        // series4.tooltipText = 'Jumlah Kirim : [bold]{valueY}[/]';
        series4.name = 'Jumlah Kirim';
        series4.columns.template.fill = am4core.color('#000000'); // Rofo Hitam
        series4.columns.template.width = am4core.percent(10);

        const columnTemplate4 = series4.columns.template;
        columnTemplate4.strokeWidth = 2;
        columnTemplate4.strokeOpacity = 1;
        columnTemplate4.stroke = am4core.color('#FFFFFF');

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        const legendContainer = am4core.create('legenddiv', am4core.Container);
        legendContainer.width = am4core.percent(100);
        legendContainer.height = am4core.percent(100);
        // Create LEGEND
        chart.legend = new am4charts.Legend();
        chart.legend.position = 'top';
        chart.legend.parent = legendContainer;

    }

}
