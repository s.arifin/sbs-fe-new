import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    DetailBahanService,
    DetailBahanPopupService,
    DetailBahanComponent,
    DetailBahanDetailComponent,
    DetailBahanDialogComponent,
    DetailBahanPopupComponent,
    DetailBahanDeletePopupComponent,
    DetailBahanDeleteDialogComponent,
    detailBahanRoute,
    detailBahanPopupRoute,
    DetailBahanResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...detailBahanRoute,
    ...detailBahanPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DetailBahanComponent,
        DetailBahanDetailComponent,
        DetailBahanDialogComponent,
        DetailBahanDeleteDialogComponent,
        DetailBahanPopupComponent,
        DetailBahanDeletePopupComponent,
    ],
    entryComponents: [
        DetailBahanComponent,
        DetailBahanDialogComponent,
        DetailBahanPopupComponent,
        DetailBahanDeleteDialogComponent,
        DetailBahanDeletePopupComponent,
    ],
    providers: [
        DetailBahanService,
        DetailBahanPopupService,
        DetailBahanResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDetailBahanModule {}
