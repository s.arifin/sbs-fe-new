import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DetailBahan } from './detail-bahan.model';
import { DetailBahanPopupService } from './detail-bahan-popup.service';
import { DetailBahanService } from './detail-bahan.service';

@Component({
    selector: 'jhi-detail-bahan-dialog',
    templateUrl: './detail-bahan-dialog.component.html'
})
export class DetailBahanDialogComponent implements OnInit {

    detailBahan: DetailBahan;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private detailBahanService: DetailBahanService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.detailBahan.id !== undefined) {
            this.subscribeToSaveResponse(
                this.detailBahanService.update(this.detailBahan));
        } else {
            this.subscribeToSaveResponse(
                this.detailBahanService.create(this.detailBahan));
        }
    }

    private subscribeToSaveResponse(result: Observable<DetailBahan>) {
        result.subscribe((res: DetailBahan) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DetailBahan) {
        this.eventManager.broadcast({ name: 'detailBahanListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-detail-bahan-popup',
    template: ''
})
export class DetailBahanPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailBahanPopupService: DetailBahanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.detailBahanPopupService
                    .open(DetailBahanDialogComponent as Component, params['id']);
            } else {
                this.detailBahanPopupService
                    .open(DetailBahanDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
