export * from './detail-bahan.model';
export * from './detail-bahan-popup.service';
export * from './detail-bahan.service';
export * from './detail-bahan-dialog.component';
export * from './detail-bahan-delete-dialog.component';
export * from './detail-bahan-detail.component';
export * from './detail-bahan.component';
export * from './detail-bahan.route';
