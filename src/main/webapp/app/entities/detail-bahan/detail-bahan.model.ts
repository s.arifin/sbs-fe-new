import { BaseEntity } from './../../shared';

export class DetailBahan implements BaseEntity {
    constructor(
        public id?: number,
        public nobbm?: string,
        public kdsupp?: string,
        public kdbahan?: number,
        public qty?: number,
        public nobatch?: string,
        public tgl_exp?: any,
        public tgl_hal?: any,
        public tgl_msk?: any,
        public lokasi?: string,
        public nolpm?: string,
        public status?: string,
        public qty_rls?: string,
        public qty_tlk?: number,
        public qty_pakai?: number,
        public qty_karantina?: number,
        public kdbahan_lama?: string,
        public nolpm2?: string,
        public status2?: string,
        public qty_sisa?: number,
    ) {
    }
}
