import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DetailBahan } from './detail-bahan.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DetailBahanService {

    private resourceUrl = SERVER_API_URL + 'api/detail-bahans';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/detail-bahans';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(detailBahan: DetailBahan): Observable<DetailBahan> {
        const copy = this.convert(detailBahan);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(detailBahan: DetailBahan): Observable<DetailBahan> {
        const copy = this.convert(detailBahan);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<DetailBahan> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.tgl_exp = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_exp);
        entity.tgl_hal = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_hal);
        entity.tgl_msk = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_msk);
    }

    private convert(detailBahan: DetailBahan): DetailBahan {
        const copy: DetailBahan = Object.assign({}, detailBahan);

        copy.tgl_exp = this.dateUtils.toDate(detailBahan.tgl_exp);

        copy.tgl_hal = this.dateUtils.toDate(detailBahan.tgl_hal);

        copy.tgl_msk = this.dateUtils.toDate(detailBahan.tgl_msk);
        return copy;
    }
}
