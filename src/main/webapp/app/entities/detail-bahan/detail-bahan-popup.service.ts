import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DetailBahan } from './detail-bahan.model';
import { DetailBahanService } from './detail-bahan.service';

@Injectable()
export class DetailBahanPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private detailBahanService: DetailBahanService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.detailBahanService.find(id).subscribe((detailBahan) => {
                    detailBahan.tgl_exp = this.datePipe
                        .transform(detailBahan.tgl_exp, 'yyyy-MM-ddTHH:mm:ss');
                    detailBahan.tgl_hal = this.datePipe
                        .transform(detailBahan.tgl_hal, 'yyyy-MM-ddTHH:mm:ss');
                    detailBahan.tgl_msk = this.datePipe
                        .transform(detailBahan.tgl_msk, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.detailBahanModalRef(component, detailBahan);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.detailBahanModalRef(component, new DetailBahan());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    detailBahanModalRef(component: Component, detailBahan: DetailBahan): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.detailBahan = detailBahan;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
