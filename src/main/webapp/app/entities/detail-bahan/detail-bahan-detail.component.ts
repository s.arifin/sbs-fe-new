import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DetailBahan } from './detail-bahan.model';
import { DetailBahanService } from './detail-bahan.service';

@Component({
    selector: 'jhi-detail-bahan-detail',
    templateUrl: './detail-bahan-detail.component.html'
})
export class DetailBahanDetailComponent implements OnInit, OnDestroy {

    detailBahan: DetailBahan;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private detailBahanService: DetailBahanService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDetailBahans();
    }

    load(id) {
        this.detailBahanService.find(id).subscribe((detailBahan) => {
            this.detailBahan = detailBahan;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDetailBahans() {
        this.eventSubscriber = this.eventManager.subscribe(
            'detailBahanListModification',
            (response) => this.load(this.detailBahan.id)
        );
    }
}
