import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DetailBahanComponent } from './detail-bahan.component';
import { DetailBahanDetailComponent } from './detail-bahan-detail.component';
import { DetailBahanPopupComponent } from './detail-bahan-dialog.component';
import { DetailBahanDeletePopupComponent } from './detail-bahan-delete-dialog.component';

@Injectable()
export class DetailBahanResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const detailBahanRoute: Routes = [
    {
        path: 'detail-bahan',
        component: DetailBahanComponent,
        resolve: {
            'pagingParams': DetailBahanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBahan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'detail-bahan/:id',
        component: DetailBahanDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBahan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const detailBahanPopupRoute: Routes = [
    {
        path: 'detail-bahan-new',
        component: DetailBahanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBahan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-bahan/:id/edit',
        component: DetailBahanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBahan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-bahan/:id/delete',
        component: DetailBahanDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBahan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
