import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DetailBahan } from './detail-bahan.model';
import { DetailBahanPopupService } from './detail-bahan-popup.service';
import { DetailBahanService } from './detail-bahan.service';

@Component({
    selector: 'jhi-detail-bahan-delete-dialog',
    templateUrl: './detail-bahan-delete-dialog.component.html'
})
export class DetailBahanDeleteDialogComponent {

    detailBahan: DetailBahan;

    constructor(
        private detailBahanService: DetailBahanService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.detailBahanService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'detailBahanListModification',
                content: 'Deleted an detailBahan'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-detail-bahan-delete-popup',
    template: ''
})
export class DetailBahanDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailBahanPopupService: DetailBahanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.detailBahanPopupService
                .open(DetailBahanDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
