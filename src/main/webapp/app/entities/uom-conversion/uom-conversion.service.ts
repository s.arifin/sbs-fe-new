import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { UomConversion } from './uom-conversion.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UomConversionService {
   protected itemValues: UomConversion[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  process.env.API_C_URL + 'api/uom-conversions';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/uom-conversions';

   constructor(protected http: Http) { }

   create(uomConversion: UomConversion): Observable<UomConversion> {
       const copy = this.convert(uomConversion);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(uomConversion: UomConversion): Observable<UomConversion> {
       const copy = this.convert(uomConversion);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<UomConversion> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: UomConversion, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: UomConversion[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to UomConversion.
    */
   protected convertItemFromServer(json: any): UomConversion {
       const entity: UomConversion = Object.assign(new UomConversion(), json);
       return entity;
   }

   /**
    * Convert a UomConversion to a JSON which can be sent to the server.
    */
   protected convert(uomConversion: UomConversion): UomConversion {
       if (uomConversion === null || uomConversion === {}) {
           return {};
       }
       // const copy: UomConversion = Object.assign({}, uomConversion);
       const copy: UomConversion = JSON.parse(JSON.stringify(uomConversion));
       return copy;
   }

   protected convertList(uomConversions: UomConversion[]): UomConversion[] {
       const copy: UomConversion[] = uomConversions;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: UomConversion[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
