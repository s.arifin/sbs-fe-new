import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UomConversionComponent } from './uom-conversion.component';
import { UomConversionPopupComponent } from './uom-conversion-dialog.component';

@Injectable()
export class UomConversionResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idUomConversion,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const uomConversionRoute: Routes = [
    {
        path: 'uom-conversion',
        component: UomConversionComponent,
        resolve: {
            'pagingParams': UomConversionResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomConversion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const uomConversionPopupRoute: Routes = [
    {
        path: 'uom-conversion-popup-new-list/:parent',
        component: UomConversionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomConversion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'uom-conversion-new',
        component: UomConversionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomConversion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'uom-conversion/:id/edit',
        component: UomConversionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomConversion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
