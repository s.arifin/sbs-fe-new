import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OrderItem } from './order-item.model';
import { OrderItemService } from './order-item.service';

@Injectable()
export class OrderItemPopupService {
    protected ngbModalRef: NgbModalRef;

    idOrders: any;
    idShipFrom: any;
    idShipTo: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected orderItemService: OrderItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.orderItemService.find(id).subscribe((data: OrderItem) => {
                    this.ngbModalRef = this.orderItemModalRef(component, data);
                    this.ngbModalRef.componentInstance.idOrders = data.ordersId;
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new OrderItem();
                    data.ordersId = this.idOrders;
                    this.ngbModalRef = this.orderItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    orderItemModalRef(component: Component, orderItem: OrderItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderItem = orderItem;
        modalRef.componentInstance.idOrders = this.idOrders;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.orderItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    orderItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idOrders = this.idOrders;
        modalRef.componentInstance.idShipFrom = this.idShipFrom;
        modalRef.componentInstance.idShipTo = this.idShipTo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
