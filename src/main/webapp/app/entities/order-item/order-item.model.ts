import { BaseEntity } from './../../shared';

export class OrderItem implements BaseEntity {
    constructor(
        public id?: any,
        public idOrderItem?: any,
        public idProduct?: string,
        public itemDescription?: string,
        public idFeature?: number,
        public idShipTo?: string,
        public qty?: number,
        public unitPrice?: number,
        public discount?: number,
        public discountMD?: number,
        public discountFinCoy?: number,
        public discountatpm?: number,
        public bbn?: number,
        public taxAmount?: number,
        public totalAmount?: number,
        public ordersId?: any,
        public shipmentItems?: any,
        public billingItems?: any,
        public orderPayments?: any,
        public qtyReceipt?: number,
        public qtyFilled?: number,
        public idmachine?: string,
        public idframe?: string,
        public shipToName?: string,
        public cogs?: number,
        public TotalData?: number,
    ) {
    }
}

export class UnitOrderItem extends OrderItem {
    constructor(
        public customerId?: string,
        public customerName?: string,
        public personOwnerName?: string,
        public dateOrder?: Date,
        public orderNumber?: string,
        public salesmanName?: string,
        public salesmanId?: string,
        public currentStatus?: number,
        public adminSalesVerifyValue?: number,
        public afcVerifyValue?: number,
        public TotalData?: number,
    ) {
        super();
    }
}
