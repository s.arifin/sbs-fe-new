import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OrderItem } from './order-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OrderItemService {
   protected itemValues: OrderItem[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  process.env.API_C_URL + '/api/order-items';
   protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/order-items';
   protected resourceUnitSearchUrl = process.env.API_C_URL + '/api/_search/unit-order-items';
   protected resourceCUrl = process.env.API_C_URL + '/api/order-items';

   constructor(protected http: Http) { }

   create(orderItem: OrderItem): Observable<OrderItem> {
       const copy = this.convert(orderItem);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(orderItem: OrderItem): Observable<OrderItem> {
       const copy = this.convert(orderItem);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<OrderItem> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryUnitFilterBy(req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    return this.http.get(this.resourceUrl + '/unitFilterBy', options)
        .map((res: Response) => this.convertResponse(res));
    }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcessData(id: Number, param: String, orderItem: OrderItem): Observable<OrderItem> {
    const copy = this.convert(orderItem);
    return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
        return res.json();
    });
}

   executeProcess(item?: OrderItem, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: OrderItem[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   searchUnit(req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    return this.http.get(this.resourceUnitSearchUrl, options)
        .map((res: any) => this.convertResponse(res));
}

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to OrderItem.
    */
   protected convertItemFromServer(json: any): OrderItem {
       const entity: OrderItem = Object.assign(new OrderItem(), json);
       return entity;
   }

   /**
    * Convert a OrderItem to a JSON which can be sent to the server.
    */
   protected convert(orderItem: OrderItem): OrderItem {
       if (orderItem === null || orderItem === {}) {
           return {};
       }
       // const copy: OrderItem = Object.assign({}, orderItem);
       const copy: OrderItem = JSON.parse(JSON.stringify(orderItem));
       return copy;
   }

   protected convertList(orderItems: OrderItem[]): OrderItem[] {
       const copy: OrderItem[] = orderItems;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: OrderItem[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }

   queryFilterByC(req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    return this.http.get(this.resourceCUrl + '/filterBy', options)
        .map((res: Response) => this.convertResponse(res));
}

    executeProcessDataC(id: Number, param: String, orderItem: OrderItem): Observable<OrderItem> {
        const copy = this.convert(orderItem);
        return this.http.post(`${this.resourceCUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }
    queryUnitFilterByC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/unitFilterBy', options)
            .map((res: Response) => this.convertResponse(res));
        }
}
