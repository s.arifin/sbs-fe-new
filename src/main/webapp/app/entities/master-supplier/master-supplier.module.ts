import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextareaModule,
         PaginatorModule,
         ConfirmDialogModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         TooltipModule,
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { masterSupplierRoute, masterSupplierPopupRoute, MasterSupplierResolvePagingParams } from './master-supplier.route';
import { MasterSupplierComponent } from './master-supplier.component';
import { MasterSupplierDialogComponent, MasterSupplierPopupComponent } from './master-supplier-dialog.component';
import { MasterSupplierService } from './master-supplier.service';
import { MasterSupplierPopupService } from './master-supplier-popup.service';
import { MasterSupplierNewComponent } from './master-supplier-new.component';
import { MasterSupplierAsListComponent } from './master-supplier-as-list.component';

const ENTITY_STATES = [
    ...masterSupplierRoute,
    ...masterSupplierPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        TabsModule.forRoot(),
        TooltipModule,
        InputTextareaModule,
        ButtonModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        MasterSupplierComponent,
        MasterSupplierPopupComponent,
        MasterSupplierNewComponent,
        MasterSupplierAsListComponent
    ],
    declarations: [
        MasterSupplierComponent,
        MasterSupplierDialogComponent,
        MasterSupplierPopupComponent,
        MasterSupplierNewComponent,
        MasterSupplierAsListComponent
    ],
    entryComponents: [
        MasterSupplierComponent,
        MasterSupplierDialogComponent,
        MasterSupplierDialogComponent,
        MasterSupplierNewComponent,
        MasterSupplierAsListComponent
    ],
    providers: [
        MasterSupplierService,
        MasterSupplierPopupService,
        MasterSupplierResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterSupplierModule {}
