import { BaseEntity } from '../../shared';

export class MasterSupplier implements BaseEntity {
    constructor(
        public id?: number,
        public idSupplier?: any,
        public kdSupplier?: string,
        public nmSupplier?: string,
        public almtSupplier1?: any,
        public almtSupplier2?: any,
        public telp?: string ,
        public fax?: string,
        public person1?: string,
        public jsup?: string,
        public person2?: string,
        public alamat1?: string,
        public alamat2?: string,
        public kd_suppnew?: string,
        public tenor?: number,
        public email?: string,
        public npwp?: string,
        public mappdiv?: string,
        public isactive?: string,
        public kd_comp?: string
        // public ksup?: string
    ) {
    }
}
