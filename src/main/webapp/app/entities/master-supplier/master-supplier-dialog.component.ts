import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ToasterService } from '../../shared';
import { Uom, UomService } from '../uom';
import { ResponseWrapper } from '../../shared';
import { MasterSupplier, MasterSupplierService } from '.';
import { MasterSupplierPopupService } from './master-supplier-popup.service';

@Component({
    selector: 'jhi-master-supplier-dialog',
    templateUrl: './master-supplier-dialog.component.html'
})
export class MasterSupplierDialogComponent implements OnInit {

    masterSupplier: MasterSupplier;
    isSaving: boolean;
    idUomTo: any;
    idUomFrom: any;

    uoms: Uom[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected masterSupplierService: MasterSupplierService,
        protected uomService: UomService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomService.query()
            .subscribe((res: ResponseWrapper) => { this.uoms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.masterSupplier.idSupplier !== undefined) {
            this.subscribeToSaveResponse(
                this.masterSupplierService.update(this.masterSupplier));
        } else {
            this.subscribeToSaveResponse(
                this.masterSupplierService.create(this.masterSupplier));
        }
    }

    protected subscribeToSaveResponse(result: Observable<MasterSupplier>) {
        result.subscribe((res: MasterSupplier) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: MasterSupplier) {
        this.eventManager.broadcast({ name: 'masterSuplierListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'masterSuplier saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'masterSupplier Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUomById(index: number, item: Uom) {
        return item.idUom;
    }
}

@Component({
    selector: 'jhi-master-supplier-popup',
    template: ''
})
export class MasterSupplierPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected masterSupplierPopupService: MasterSupplierPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
        //     if ( params['id'] ) {
        //         this.masterSupplierPopupService
        //             .open(MasterSupplierDialogComponent as Component, params['id']);
        //     } else if ( params['parent'] ) {
        //         // this.uomConversionPopupService.parent = params['parent'];
        //         this.masterSupplierPopupService
        //             .open(MasterSupplierDialogComponent as Component);
        //     } else {
        //         this.masterSupplierPopupService
        //             .open(MasterSupplierDialogComponent as Component);
        //     }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
