import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MasterSupplierComponent } from './master-supplier.component';
import { MasterSupplierPopupComponent } from './master-supplier-dialog.component';
import { MasterSupplierNewComponent } from './master-supplier-new.component';

@Injectable()
export class MasterSupplierResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSupplier,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const masterSupplierRoute: Routes = [
    {
        path: 'master-supplier',
        component: MasterSupplierComponent,
        resolve: {
            'pagingParams': MasterSupplierResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterSupplier.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-supplier-new',
        component: MasterSupplierNewComponent,
        resolve: {
            'pagingParams': MasterSupplierResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterSupplier.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-supplier-edit/:id',
        component: MasterSupplierNewComponent,
        resolve: {
            'pagingParams': MasterSupplierResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterSupplier.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const masterSupplierPopupRoute: Routes = [
    {
        path: 'master-supplier/:id/edit',
        component: MasterSupplierPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterSupplier.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
