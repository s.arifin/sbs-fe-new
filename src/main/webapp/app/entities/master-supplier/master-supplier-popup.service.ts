import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MasterSupplierService, MasterSupplier } from '.';

@Injectable()
export class MasterSupplierPopupService {
    // protected ngbModalRef: NgbModalRef;
    // idSupplier: any;
    // // idUomFrom: any;

    // constructor(
    //     protected modalService: NgbModal,
    //     protected router: Router,
    //     protected masterSupplierService: MasterSupplierService

    // ) {
    //     this.ngbModalRef = null;
    // }

    // open(component: Component, id?: number | any): Promise<NgbModalRef> {
    //     return new Promise<NgbModalRef>((resolve, reject) => {
    //         const isOpen = this.ngbModalRef !== null;
    //         if (isOpen) {
    //             resolve(this.ngbModalRef);
    //         }

    //         if (id) {
    //             this.masterSupplierService.find(id).subscribe((data) => {
    //                 this.ngbModalRef = this.masterSupplierModalRef(component, data);
    //                 resolve(this.ngbModalRef);
    //             });
    //         } else {
    //             // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
    //             setTimeout(() => {
    //                 const data = new MasterSupplier();
    //                 // data.uomToId = this.idUomTo;
    //                 // data.uomFromId = this.idUomFrom;
    //                 this.ngbModalRef = this.masterSupplierModalRef(component, data);
    //                 resolve(this.ngbModalRef);
    //             }, 0);
    //         }
    //     });
    // }

    // masterSupplierModalRef(component: Component, masterSupplier: MasterSupplier): NgbModalRef {
    //     const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
    //     modalRef.componentInstance.masterSupplier = masterSupplier;
    //     modalRef.componentInstance.idSupplier = this.idSupplier;
    //     modalRef.result.then((result) => {
    //         this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
    //         this.ngbModalRef = null;
    //     }, (reason) => {
    //         this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
    //         this.ngbModalRef = null;
    //     });
    //     return modalRef;
    // }

    // load(component: Component): Promise<NgbModalRef> {
    //     return new Promise<NgbModalRef>((resolve, reject) => {
    //         const isOpen = this.ngbModalRef !== null;
    //         if (isOpen) {
    //             resolve(this.ngbModalRef);
    //         }

    //         // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
    //         setTimeout(() => {
    //             this.ngbModalRef = this.uomConversionLoadModalRef(component);
    //             resolve(this.ngbModalRef);
    //         }, 0);
    //     });
    // }

    // uomConversionLoadModalRef(component: Component): NgbModalRef {
    //     const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
    //     modalRef.componentInstance.idUomTo = this.idUomTo;
    //     modalRef.componentInstance.idUomFrom = this.idUomFrom;
    //     modalRef.result.then((result) => {
    //         this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
    //         this.ngbModalRef = null;
    //     }, (reason) => {
    //         this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
    //         this.ngbModalRef = null;
    //     });
    //     return modalRef;
    // }
}
