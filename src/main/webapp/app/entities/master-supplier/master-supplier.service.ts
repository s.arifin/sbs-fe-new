import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { MasterSupplier } from '.';
import { UomConversion } from '../uom-conversion';

@Injectable()
export class MasterSupplierService {
    protected itemValues: MasterSupplier[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl = process.env.API_C_URL + '/api/master-supliers';
    protected resourceSearchUrl = process.env.API_C_URL + 'api/_search/api/master-supliers';

    constructor(protected http: Http) { }

    create(masterSupplier: MasterSupplier): Observable<MasterSupplier> {
        const copy = this.convert(masterSupplier);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(masterSupplier: MasterSupplier): Observable<MasterSupplier> {
        const copy = this.convert(masterSupplier);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<MasterSupplier> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(item?: UomConversion, req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, item, options);
    }

    executeListProcess(items?: UomConversion[], req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UomConversion.
     */
    protected convertItemFromServer(json: any): MasterSupplier {
        const entity: MasterSupplier = Object.assign(new MasterSupplier(), json);
        return entity;
    }

    /**
     * Convert a UomConversion to a JSON which can be sent to the server.
     */
    protected convert(masterSupplier: MasterSupplier): MasterSupplier {
        if (masterSupplier === null || masterSupplier === {}) {
            return {};
        }
        // const copy: UomConversion = Object.assign({}, uomConversion);
        const copy: MasterSupplier = JSON.parse(JSON.stringify(masterSupplier));
        return copy;
    }

    protected convertList(uomConversions: UomConversion[]): UomConversion[] {
        const copy: UomConversion[] = uomConversions;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: UomConversion[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    getNewSupplier() {
        return this.http.get(this.resourceUrl + '/getallsupplier').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getNewSupplierNew() {
        return this.http.get(this.resourceUrl + '/getallsupplier1').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    findByCode(code: any): Observable<MasterSupplier> {
        return this.http.get(this.resourceUrl + '/get-by-code/' + code).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
    findByName(name: any): Observable<MasterSupplier> {
        return this.http.get(this.resourceUrl + '/get-by-name/' + name).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
    getNewSupplierDIV(div: string) {
        return this.http.get(this.resourceUrl + '/getallsupplier/div/' + div).toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getNewSupplierALL() {
        return this.http.get(this.resourceUrl + '/getallsupplier/all').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }
}
