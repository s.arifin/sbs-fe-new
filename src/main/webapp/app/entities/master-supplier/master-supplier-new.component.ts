import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { Account, AccountService, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { MasterSupplier, MasterSupplierService } from '.';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-master-supplier-new',
    templateUrl: './master-supplier-new.component.html'
})
export class MasterSupplierNewComponent implements OnInit, OnDestroy {

    currentAccount: Account;
    masterSuppliers: MasterSupplier[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    masterSupplier: MasterSupplier;
    isEdit: Boolean = false;
    private subscription: Subscription;
    jsup: Array<object> = [
        { label: 'Pilih Jenis Supplier', value: '' },
        { label: 'Lokal', value: 'Lokal' },
        { label: 'Luar', value: 'Luar' }
    ];
    compList: Array<object> = [
        { label: 'Pilih Perusahaan', value: '' },
        { label: 'SKG', value: 'SKG' },
        { label: 'SBS', value: 'SBS' }
    ];
    active: Array<object> = [
        { label: 'Pilih Status Supplier', value: null },
        { label: 'Aktif', value: '1' },
        { label: 'Non Aktif', value: '0' }
    ];
    // ksup: Array<object> = [
    //     { label: 'Pilih Kategori Supplier', value: null },
    //     { label: 'Bahan Baku', value: 'rm' },
    //     { label: 'Bahan Pengemas', value: 'pm' },
    //     { label: 'Bahan Lain - Lain', value: 'lain' },
    //     { label: 'Spareparts', value: 's' }
    // ]
    selectedjsup: string;
    selectedActive: string;
    selectedComp: string;
    // selectedKategori: string;
    isExist: boolean;
    existDesc: string;

    constructor(
        protected masterSupplierService: MasterSupplierService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        private route: ActivatedRoute,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.masterSupplier = new MasterSupplier();
        this.selectedjsup = '';
        this.isExist = false;
        this.existDesc = '';
        this.selectedComp = '';
        this.currentAccount = new Account();
    }

    loadAll() {
        if (this.currentSearch) {
            this.masterSupplierService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.masterSupplierService.query({
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/uom-conversion'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/uom-conversion', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/uom-conversion', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    load(id) {
        this.loadingService.loadingStart();
        this.masterSupplierService.find(id)
            .subscribe(
                (res: MasterSupplier) => {
                    this.masterSupplier = res;
                    this.selectedjsup = this.masterSupplier.jsup;
                    this.selectedActive = this.masterSupplier.isactive;
                    // this.selectedKategori = this.masterSupplier.ksup;
                    this.loadingService.loadingStop();
                }

            )
    }

    ngOnInit() {
        this.principal.identity(true).then((account: Account) => {
            this.currentAccount = account;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
                this.isEdit = true;
            }
            if (params['id'] === null && params['id'] === undefined) {
                this.masterSupplier = new MasterSupplier;
            }
        });
    }

    ngOnDestroy() {
        //    this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MasterSupplier) {
        return item.idSupplier;
    }

    registerChangeInUomConversions() {
        this.eventSubscriber = this.eventManager.subscribe('uomConversionListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idUomConversion') {
            result.push('idUomConversion');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        //    this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.masterSuppliers = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(item) {
        this.masterSupplierService.executeProcess(item).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.masterSupplierService.update(event.data)
                .subscribe((res: MasterSupplier) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.masterSupplierService.create(event.data)
                .subscribe((res: MasterSupplier) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: MasterSupplier) {
        this.toaster.showToaster('info', 'UomConversion Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.loadingService.loadingStart();
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.masterSupplierService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'uomConversionListModification',
                        content: 'Deleted an uomConversion'
                    });
                    this.loadingService.loadingStop();
                });
            }
        });
    }

    process() {
        this.masterSupplierService.process({}).subscribe((r) => {
            console.log('result', r);
            this.toaster.showToaster('info', 'Data Processed', 'processed.....');
        });
    }

    save() {
        this.loadingService.loadingStart();
        this.masterSupplier.jsup = this.selectedjsup;
        this.masterSupplier.isactive = this.selectedActive;
        // this.masterSupplier.ksup = this.selectedKategori;
        if (this.currentAccount.idInternal !== 'RCP') {
            this.masterSupplier.mappdiv = this.currentAccount.idInternal;
        }
        if (this.masterSupplier.kdSupplier === null || this.masterSupplier.kdSupplier === '' || this.masterSupplier.kdSupplier === undefined) {
            alert('Kode Supplier Tidak boleh Kosong');
            this.loadingService.loadingStop();
        } else if (this.masterSupplier.nmSupplier === null || this.masterSupplier.nmSupplier === '' || this.masterSupplier.nmSupplier === undefined) {
            alert('Nama Supplier Tidak boleh Kosong');
            this.loadingService.loadingStop()
        } else if (this.masterSupplier.isactive === null || this.masterSupplier.isactive === undefined || this.masterSupplier.isactive === '') {
            alert('Status Supplier Harus Di Pilih');
            this.loadingService.loadingStop()
        } else {
            this.masterSupplierService.create(this.masterSupplier)
                .subscribe(
                    (e) => { this.router.navigate(['/master-supplier']), this.loadingService.loadingStop() }
                );
        }
    }
    update() {
        this.masterSupplier.jsup = this.selectedjsup;
        this.masterSupplier.isactive = this.selectedActive;
        // this.masterSupplier.ksup = this.selectedKategori;
        this.masterSupplierService.update(this.masterSupplier)
            .subscribe(
                (e) => { this.router.navigate(['/master-supplier']), this.loadingService.loadingStop() }
            );
    }

    clearBack() {
        this.router.navigate(['/master-supplier']);
    }

    checkExist() {
        if (this.masterSupplier.kdSupplier.length > 6 || this.masterSupplier.kdSupplier.length < 6) {
            this.isExist = true;
            this.existDesc = 'Panjang Kode Supplier HARUS 6 dan tidak boleh lebih dan kurang dari 6';
        }
        if (this.masterSupplier.kdSupplier.length === 6) {
            this.loadingService.loadingStart();
            this.masterSupplierService.findByCode(this.masterSupplier.kdSupplier)
                .subscribe(
                    (res: MasterSupplier) => {
                        if (res.kd_suppnew != null && res.nmSupplier != null) {
                            this.isExist = true;
                            this.existDesc = 'Kode Supplier ' + res.kdSupplier + ' Sudah Ada dengan Nama Supplier = ' + res.nmSupplier;
                            // this.masterSupplier = new MasterSupplier();
                        } else {
                            this.isExist = false;
                            this.existDesc = '';
                        };
                        this.loadingService.loadingStop();
                    })
        }
    }

}
