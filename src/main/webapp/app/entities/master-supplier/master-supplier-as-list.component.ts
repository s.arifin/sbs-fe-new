import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { MasterSupplier, MasterSupplierService } from '.';

@Component({
    selector: 'jhi-master-supplier-as-list',
    templateUrl: './master-supplier-as-list.component.html'
})
export class MasterSupplierAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idUomTo: any;
    @Input() idUomFrom: any;

    currentAccount: any;
    masterSuppliers: MasterSupplier[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected uomConversionService: MasterSupplierService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idUomConversion';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.uomConversionService.queryFilterBy({
            idUomTo: this.idUomTo,
            idUomFrom: this.idUomFrom,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/uom-conversion', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUomConversions();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idUomTo']) {
            this.loadAll();
        }
        if (changes['idUomFrom']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MasterSupplier) {
        return item.idSupplier;
    }

    registerChangeInUomConversions() {
        this.eventSubscriber = this.eventManager.subscribe('uomConversionListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idUomConversion') {
            result.push('idUomConversion');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.masterSuppliers = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.uomConversionService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.uomConversionService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.uomConversionService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<MasterSupplier>) {
        result.subscribe((res: MasterSupplier) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: MasterSupplier) {
        this.eventManager.broadcast({ name: 'uomConversionListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'uomConversion saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.uomConversionService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'uomConversionListModification',
                        content: 'Deleted an uomConversion'
                    });
                });
            }
        });
    }
}
