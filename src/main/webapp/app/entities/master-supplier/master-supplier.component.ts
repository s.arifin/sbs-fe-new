import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LoadingService } from '../../layouts';
import { ReportUtilService } from '../../shared/report/report-util.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { MasterSupplier, MasterSupplierService } from '.';

@Component({
    selector: 'jhi-master-supplier',
    templateUrl: './master-supplier.component.html'
})
export class MasterSupplierComponent implements OnInit, OnDestroy {

    currentAccount: any;
    masterSuppliers: MasterSupplier[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    inputNeeds: any;
    isFilter: boolean;
    needs: Array<object> = [
        { label: 'Pilih Status', value: '' },
        { label: 'Aktif', value: '1' },
        { label: 'Non Aktif', value: '0' }
    ];
    currentInternal: string;
    filter_data: string;
    modalPilihCeteakn: boolean;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected masterSupplierService: MasterSupplierService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
    ) {
        this.currentInternal = principal.getIdInternal();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.isFilter = false;
        this.modalPilihCeteakn = false;
        this.filter_data = '';

    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter) {
            if (this.currentSearch) {
                this.masterSupplierService.queryFilter({
                    page: this.page - 1,
                    filter: 'fill:' + this.inputNeeds + ' |search:' + this.currentSearch,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    // (res: ResponseWrapper) => this.onError(res.json)
                );
            } else {
                this.masterSupplierService.queryFilter({
                    page: this.page - 1,
                    filter: 'fill:' + this.inputNeeds + ' |search:',
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    // (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        } else {
            if (this.currentSearch) {
                this.masterSupplierService.query({
                    page: this.page - 1,
                    filter: 'fill:' + this.currentSearch,
                    size: this.itemsPerPage,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    // (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
            this.masterSupplierService.query({
                page: this.page - 1,
                filter: 'fill:',
                size: this.itemsPerPage
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                // (res: ResponseWrapper) => this.onError(res.json)
            );
        }

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filter() {
        if (this.inputNeeds === null || this.inputNeeds === undefined || this.inputNeeds === '') {
            alert('Pilih Status Supplier Terlebih Dahulu');
        } else {
            this.isFilter = true;
            this.currentSearch = '';
            this.loadAll()
        }

    }

    reset() {
        this.isFilter = false;
        this.inputNeeds = null;
        this.loadAll();
    }

    transition() {
        this.router.navigate(['/uom-conversion'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUomConversions();
    }

    ngOnDestroy() {
        //    this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MasterSupplier) {
        return item.idSupplier;
    }

    registerChangeInUomConversions() {
        this.eventSubscriber = this.eventManager.subscribe('uomConversionListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idUomConversion') {
            result.push('idUomConversion');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        //    this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.masterSuppliers = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(item) {
        this.masterSupplierService.executeProcess(item).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.masterSupplierService.update(event.data)
                .subscribe((res: MasterSupplier) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.masterSupplierService.create(event.data)
                .subscribe((res: MasterSupplier) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: MasterSupplier) {
        this.toaster.showToaster('info', 'UomConversion Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.masterSupplierService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'uomConversionListModification',
                        content: 'Deleted an uomConversion'
                    });
                });
            }
        });
    }

    process() {
        this.masterSupplierService.process({}).subscribe((r) => {
            console.log('result', r);
            this.toaster.showToaster('info', 'Data Processed', 'processed.....');
        });
    }

    public download() {
        this.modalPilihCeteakn = true;
    }

    printExcel() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();

        this.loadingService.loadingStart();
        this.filter_data = 'divisi:' + this.currentInternal + '|status:' + this.inputNeeds;
        this.reportUtilService.downloadFileWithName(`Report Master Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_msupplier/xlsx', { filterData: this.filter_data })
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;
    }

    printPDF() {
        this.loadingService.loadingStart();
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        this.filter_data = 'divisi:' + this.currentInternal + '|status:' + this.inputNeeds;
        this.reportUtilService.downloadFileWithName(`Report Master Supplier ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_msupplier/pdf', { filterData: this.filter_data })
        this.loadingService.loadingStop();
        this.modalPilihCeteakn = false;
    }

}
