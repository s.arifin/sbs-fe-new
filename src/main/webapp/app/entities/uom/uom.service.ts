import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Uom } from './uom.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UomService {
    protected itemValues: Uom[];
    values: Subject<any> = new Subject<any>();

    //    protected resourceUrl =  SERVER_API_URL + 'api/uoms';
    protected resourceUrl = process.env.API_C_URL + '/api/uoms';
    protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/uoms';

    constructor(protected http: Http) { }

    create(uom: Uom): Observable<Uom> {
        const copy = this.convert(uom);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(uom: Uom): Observable<Uom> {
        const copy = this.convert(uom);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<Uom> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(item?: Uom, req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, item, options);
    }

    executeListProcess(items?: Uom[], req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
        * Convert a returned JSON object to Uom.
        */
    protected convertItemFromServer(json: any): Uom {
        const entity: Uom = Object.assign(new Uom(), json);
        return entity;
    }

    /**
        * Convert a Uom to a JSON which can be sent to the server.
        */
    protected convert(uom: Uom): Uom {
        if (uom === null || uom === {}) {
            return {};
        }
        // const copy: Uom = Object.assign({}, uom);
        const copy: Uom = JSON.parse(JSON.stringify(uom));
        return copy;
    }

    protected convertList(uoms: Uom[]): Uom[] {
        const copy: Uom[] = uoms;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Uom[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
