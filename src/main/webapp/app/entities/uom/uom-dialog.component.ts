import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Uom } from './uom.model';
import { UomPopupService } from './uom-popup.service';
import { UomService } from './uom.service';
import { ToasterService } from '../../shared';
import { UomType, UomTypeService } from '../uom-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-uom-dialog',
    templateUrl: './uom-dialog.component.html'
})
export class UomDialogComponent implements OnInit {

    uom: Uom;
    isSaving: boolean;
    idUomType: any;

    uomtypes: UomType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected uomService: UomService,
        protected uomTypeService: UomTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.uomtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.uom.idUom !== undefined) {
            this.subscribeToSaveResponse(
                this.uomService.update(this.uom));
        } else {
            this.subscribeToSaveResponse(
                this.uomService.create(this.uom));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Uom>) {
        result.subscribe((res: Uom) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Uom) {
        this.eventManager.broadcast({ name: 'uomListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'uom saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'uom Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUomTypeById(index: number, item: UomType) {
        return item.idUomType;
    }
}

@Component({
    selector: 'jhi-uom-popup',
    template: ''
})
export class UomPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected uomPopupService: UomPopupService
    ) {}

    ngOnInit() {
        this.uomPopupService.idUomType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.uomPopupService
                    .open(UomDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.uomPopupService.idUomType = params['parent'];
                this.uomPopupService
                    .open(UomDialogComponent as Component);
            } else {
                this.uomPopupService
                    .open(UomDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
