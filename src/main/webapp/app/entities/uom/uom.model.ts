import { BaseEntity } from './../../shared';

export class Uom implements BaseEntity {
    constructor(
        public id?: number,
        public idUom?: number,
        public description?: string,
        public abbreviation?: string,
        public uomTypeId?: any,
    ) {
    }
}
