import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {Uom} from './uom.model';
import {UomService} from './uom.service';

@Component({
    selector: 'jhi-uom-detail',
    templateUrl: './uom-detail.component.html'
})
export class UomDetailComponent implements OnInit, OnDestroy {

    uom: Uom;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected uomService: UomService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUoms();
    }

    load(id) {
        this.uomService.find(id).subscribe((uom) => {
            this.uom = uom;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUoms() {
        this.eventSubscriber = this.eventManager.subscribe(
            'uomListModification',
            (response) => this.load(this.uom.idUom)
        );
    }
}
