import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { BaseCalendar } from './base-calendar.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class BaseCalendarService {

    protected resourceUrl = process.env.API_C_URL + '/api/base-calendars';
    protected resourceSearchUrl = 'api/_search/base-calendars';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(baseCalendar: BaseCalendar): Observable<BaseCalendar> {
        const copy = this.convert(baseCalendar);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(baseCalendar: BaseCalendar): Observable<BaseCalendar> {
        const copy = this.convert(baseCalendar);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<BaseCalendar> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(baseCalendar: any, id: Number): Observable<String> {
        const copy = this.convert(baseCalendar);
        return this.http.post(`${this.resourceUrl}/execute/${id}`, copy).map((res: Response) => {
            return res.json();
        })
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateKey) {
            entity.dateKey = new Date(entity.dateKey);
        }
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(baseCalendar: BaseCalendar): BaseCalendar {
        const copy: BaseCalendar = Object.assign({}, baseCalendar);

        // copy.dateKey = this.dateUtils.toDate(baseCalendar.dateKey);

        // copy.dateFrom = this.dateUtils.toDate(baseCalendar.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(baseCalendar.dateThru);
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
