import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BaseCalendar} from './base-calendar.model';
import {BaseCalendarPopupService} from './base-calendar-popup.service';
import {BaseCalendarService} from './base-calendar.service';
import {ToasterService} from '../../shared';
import { CalendarType, CalendarTypeService } from '../calendar-type';
import { StandardCalendar, StandardCalendarService } from '../standard-calendar';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-base-calendar-dialog',
    templateUrl: './base-calendar-dialog.component.html'
})
export class BaseCalendarDialogComponent implements OnInit {

    baseCalendar: BaseCalendar;
    isSaving: boolean;

    calendartypes: CalendarType[];

    standardcalendars: StandardCalendar[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected baseCalendarService: BaseCalendarService,
        protected calendarTypeService: CalendarTypeService,
        protected standardCalendarService: StandardCalendarService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.calendarTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.calendartypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.standardCalendarService.query()
            .subscribe((res: ResponseWrapper) => { this.standardcalendars = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.baseCalendar.idCalendar !== undefined) {
            this.subscribeToSaveResponse(
                this.baseCalendarService.update(this.baseCalendar));
        } else {
            this.subscribeToSaveResponse(
                this.baseCalendarService.create(this.baseCalendar));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BaseCalendar>) {
        result.subscribe((res: BaseCalendar) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BaseCalendar) {
        this.eventManager.broadcast({ name: 'baseCalendarListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'baseCalendar saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'baseCalendar Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackCalendarTypeById(index: number, item: CalendarType) {
        return item.idCalendarType;
    }

    trackStandardCalendarById(index: number, item: StandardCalendar) {
        return item.idCalendar;
    }
}

@Component({
    selector: 'jhi-base-calendar-popup',
    template: ''
})
export class BaseCalendarPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected baseCalendarPopupService: BaseCalendarPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.baseCalendarPopupService
                    .open(BaseCalendarDialogComponent as Component, params['id']);
            } else {
                this.baseCalendarPopupService
                    .open(BaseCalendarDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
