import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BaseCalendarComponent } from './base-calendar.component';
import { BaseCalendarPopupComponent } from './base-calendar-dialog.component';

@Injectable()
export class BaseCalendarResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCalendar,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const baseCalendarRoute: Routes = [
    {
        path: 'base-calendar',
        component: BaseCalendarComponent,
        resolve: {
            'pagingParams': BaseCalendarResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.baseCalendar.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const baseCalendarPopupRoute: Routes = [
    {
        path: 'base-calendar-new',
        component: BaseCalendarPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.baseCalendar.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'base-calendar/:id/edit',
        component: BaseCalendarPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.baseCalendar.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
