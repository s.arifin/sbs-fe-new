import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { Village } from './village.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class VillageService {
    private itemValues: Village[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/villages';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/villages';
    private resourceCUrl = process.env.API_C_URL + '/api/villages';

    constructor(private http: Http) { }

    create(village: Village): Observable<Village> {
        const copy = this.convert(village);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(village: Village): Observable<Village> {
        const copy = this.convert(village);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<Village> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryByDistrict(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-district', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getByDistrict(id?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/by-district/' + id)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, village: Village): Observable<Village> {
        const copy = this.convert(village);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, villages: Village[]): Observable<Village[]> {
        const copy = this.convertList(villages);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(village: Village): Village {
        if (village === null || village === {}) {
            return {};
        }
        // const copy: Village = Object.assign({}, village);
        const copy: Village = JSON.parse(JSON.stringify(village));
        return copy;
    }

    private convertList(villages: Village[]): Village[] {
        const copy: Village[] = villages;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Village[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
