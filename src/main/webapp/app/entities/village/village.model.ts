import { BaseEntity } from './../../shared';

export class Village implements BaseEntity {
    constructor(
        public id?: any,
        public idGeobou?: any,
        public description?: any
    ) {
    }
}
