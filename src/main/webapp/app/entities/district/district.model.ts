import { BaseEntity } from './../../shared';

export class District implements BaseEntity {
    constructor(
        public id?: any,
        public idGeobou?: any,
        public description?: any
    ) {
    }
}
