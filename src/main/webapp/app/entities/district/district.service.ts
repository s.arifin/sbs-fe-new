import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { District } from './district.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class DistrictService {
    protected itemValues: District[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/districts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/districts';
    private resourceCUrl = process.env.API_C_URL + '/api/districts';

    constructor(protected http: Http) { }

    create(district: District): Observable<District> {
        const copy = this.convert(district);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(district: District): Observable<District> {
        const copy = this.convert(district);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<District> {
        return this.http.get(this.resourceUrl + '/' + id).map((res: Response) => {
            return res.json();
        });
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/getAll')
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryByCity(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-city', options)
            .map((res: Response) => this.convertResponse(res));
    }

    GetRing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-internal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getByCity(id?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/by-city/' + id)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(this.resourceUrl + '/' + id);
    }

    executeProcess(id: Number, param: String, district: District): Observable<District> {
        const copy = this.convert(district);
        return this.http.post(this.resourceUrl + '/execute/' + id + '/' + param , copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, districts: District[]): Observable<District[]> {
        const copy = this.convertList(districts);
        return this.http.post(this.resourceUrl + '/execute-list/' + 'id' + '/' + param, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(district: District): District {
        if (district === null || district === {}) {
            return {};
        }
        // const copy: District = Object.assign({}, district);
        const copy: District = JSON.parse(JSON.stringify(district));
        return copy;
    }

    protected convertList(districts: District[]): District[] {
        const copy: District[] = districts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: District[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
