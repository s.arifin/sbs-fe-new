import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {District} from './district.model';
import {DistrictPopupService} from './district-popup.service';
import {DistrictService} from './district.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-district-dialog',
    templateUrl: './district-dialog.component.html'
})
export class DistrictDialogComponent implements OnInit {

    district: District;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected districtService: DistrictService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.district.idGeobou !== undefined) {
            this.subscribeToSaveResponse(
                this.districtService.update(this.district));
        } else {
            this.subscribeToSaveResponse(
                this.districtService.create(this.district));
        }
    }

    protected subscribeToSaveResponse(result: Observable<District>) {
        result.subscribe((res: District) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: District) {
        this.eventManager.broadcast({ name: 'districtListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'district saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'district Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-district-popup',
    template: ''
})
export class DistrictPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected districtPopupService: DistrictPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.districtPopupService
                    .open(DistrictDialogComponent as Component, params['id']);
            } else {
                this.districtPopupService
                    .open(DistrictDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
