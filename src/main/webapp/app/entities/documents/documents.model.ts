import { BaseEntity } from './../../shared';

export class Documents implements BaseEntity {
    constructor(
        public id?: any,
        public idDocument?: any,
        public note?: string,
        public dateCreate?: any,
        public documentTypeId?: any,
        public documentTypeDescription?: string
    ) {
    }
}
