import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Documents} from './documents.model';
import {DocumentsPopupService} from './documents-popup.service';
import {DocumentsService} from './documents.service';
import {ToasterService} from '../../shared';
import { DocumentType, DocumentTypeService } from '../document-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-documents-dialog',
    templateUrl: './documents-dialog.component.html'
})
export class DocumentsDialogComponent implements OnInit {

    documents: Documents;
    isSaving: boolean;

    documenttypes: DocumentType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected documentsService: DocumentsService,
        protected documentTypeService: DocumentTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.documentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.documenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.documents.idDocument !== undefined) {
            this.subscribeToSaveResponse(
                this.documentsService.update(this.documents));
        } else {
            this.subscribeToSaveResponse(
                this.documentsService.create(this.documents));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Documents>) {
        result.subscribe((res: Documents) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Documents) {
        this.eventManager.broadcast({ name: 'documentsListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'documents saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'documents Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackDocumentTypeById(index: number, item: DocumentType) {
        return item.idDocumentType;
    }
}

@Component({
    selector: 'jhi-documents-popup',
    template: ''
})
export class DocumentsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected documentsPopupService: DocumentsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.documentsPopupService
                    .open(DocumentsDialogComponent as Component, params['id']);
            } else {
                this.documentsPopupService
                    .open(DocumentsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
