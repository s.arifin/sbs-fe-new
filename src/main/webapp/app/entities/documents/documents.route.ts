import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DocumentsComponent } from './documents.component';
import { DocumentsPopupComponent } from './documents-dialog.component';

@Injectable()
export class DocumentsResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idDocument,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const documentsRoute: Routes = [
    {
        path: 'documents',
        component: DocumentsComponent,
        resolve: {
            'pagingParams': DocumentsResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.documents.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const documentsPopupRoute: Routes = [
    {
        path: 'documents-new',
        component: DocumentsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.documents.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'documents/:id/edit',
        component: DocumentsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.documents.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
