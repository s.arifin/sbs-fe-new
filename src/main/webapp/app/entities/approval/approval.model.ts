import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { BaseEntity } from './../../shared';

export class ApprovalBBM implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: any,
        public tgl_bbm?: any,
        public kd_supplier?: any,
        public no_sj_supplier?: any,
        public tgl_sj_supplier?: any,
        public nm_supplier?: any,
        public no_po?: any,
        public kd_div?: any,
        public id_app?: any,
        public tg_app?: any,
        public jm_app?: any,
        public iduser?: any,
    ) {
    }
}
export class ApprovalBBMTeknik implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: any,
        public tgl_bbm?: any,
        public no_sj?: any,
        public supplier?: any,
        public tgl_sj?: any,
        public no_po?: any,
        public no_pp?: any,
        public status1?: any,
    ) {
    }
}

export class DetailBBMTeknik implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: any,
        public kd_barang?: any,
        public nama_barang?: any,
        public satuan?: any,
        public qty?: any,
        public qty_po?: any,
        public lokasi?: any,
        public ket?: any,
        public exp_date?: any,
        public exp_halal?: any,
    ) {
    }
}

export class DetailBBM implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: any,
        public kd_bahan?: any,
        public nmbahan?: any,
        public satuan?: any,
        public qty?: any,
        public hasil?: any,
        public no_batch?: any,
        public tgl_exp?: any,
        public tgl_exphal?: any,
        public keterangan?: any,
    ) {
    }
}

export class ApprovalBPB implements BaseEntity {
    constructor(
        public id?: number,
        public no_bpb?: any,
        public tgl_bpb?: any,
        public jns_mutasi?: any,
        public diterima?: any,
        public jns_prod?: any,
        public id_app1?: any,
        public id_app2?: any,
        public iduser?: any,

    ) { }
}

export class DetailBPB implements BaseEntity {
    constructor(
        public id?: number,
        public kd_produk?: any,
        public kd_group?: any,
        public nm_produk?: any,
        public qty?: any,
        public satuan?: any,
        public no_batch?: any,
        public no_palet?: any,
        public tgl_ub?: any,
        public tgl_palet?: any,
    ) {
    }
}

export class ApprovalLHP implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: any,
        public no_periksa?: any,
        public tgl_masuk?: any,
        public nm_pemeriksa?: any,
        public tgl_bbm ?: any,
        public no_po?: any,
        public kd_suppnew?: any,
        public nm_supplier?: any,
        public no_sj_supplier?: any,
        public tgl_sj_supplier?: any,
        public iduser?: any,
        public approval?: any,
    ) { }
}

export class ApprovalDetailLHP implements BaseEntity {
    constructor(
        public id?: number,
        public kd_bahan?: any,
        public nmbahan?: any,
        public qty_trm?: any,
        public qty_rls?: any,
        public qty_tlk?: any,
        public satuan?: any,
        public hasil?: any,
        public keterangan?: any,
        public tgl_exphal?: any,
        public tgl_exp?: any,
        public no_batch?: any,
        public no_periksa?: any,
    ) { }
}

export class ApprovalDisposisi implements BaseEntity {
    constructor(
        public id?: number,
        public no_dsp?: any,
        public tgl_dsp?: any,
        public status?: any,
        public iduser?: any
    ) {
    }
}

export class DetailDisposisi implements BaseEntity {
    constructor(
        public id?: number,
        public no_dsp?: any,
        public kd_group?: any,
        public kd_produk?: any,
        public nm_produk?: any,
        public satuan?: any,
        public qty?: any,
        public no_batch?: any,
        public no_palet?: any,
        public no_prod?: any,
        public tgl_ub?: any,
        public tgl_palet?: any,
        public keterangan?:  any,
    ) {
    }
}
