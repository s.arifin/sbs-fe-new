import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import * as BaseConstant from '../../../shared/constants/base.constants';
import * as UnitDocumentMessageConstant from '../../../shared/constants/unit-document-message.constants';
import { ResponseWrapper, CommonUtilService, Principal, ITEMS_PER_PAGE, ToasterService } from '../../../shared';
// import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ApprovalDisposisi, DetailDisposisi } from '../approval.model';
import { ApprovalService } from '../approval.service';

@Component({
    selector: 'jhi-approval-disposisi',
    templateUrl: './approval-disposisi.component.html'
})
export class ApprovalDisposisiComponent implements OnInit, OnDestroy {
    first: number;
    joborders: ApprovalDisposisi[];
    jobordersManager: ApprovalDisposisi[];
    private eventSubscriber: Subscription;
    detailDisposisi: DetailDisposisi[];
    currentAccount: any;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    jobOrderApprove: any;
    jobOrderRejeced: any;
    itemsPerPage: any;
    routeData: any;
    totalItems: any;
    queryCount: any;
    idOrder: any;
    paramPage: number;
    selected: any = [];
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    idStatus: any;
    internal: any;
    userlogin: any;
    loginuser: any;
    // noteCancelJO: string;
    modalLihatDisposisi: boolean;

    constructor(
        protected principal: Principal,
        protected router: Router,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrdersService: ApprovalService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        protected alertService: JhiAlertService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.jobOrderApprove = 12;
        this.jobOrderRejeced = 11;
        this.modalLihatDisposisi = false;
        // this.noteCancelJO = '';
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
        this.registerChangeInApprovalJobOrder();
    }

    protected loadData(): void {
        this.loadingService.loadingStart();
        this.internal = this.principal.getIdInternal();
        this.loginuser = this.principal.getUserLogin().toUpperCase();
        console.log(this.loginuser);
        if (this.currentSearch) {
            if (this.idStatus === 12) {
                this.jobOrdersService.searchDisposisi({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat : 12,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            } else {
                this.jobOrdersService.searchDisposisi({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat : 11,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessManager(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            }

        } else {
                this.jobOrdersService.getDataDisposisi({
                    page: this.page - 1,
                    stat : 12,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrdersService.getDataDisposisi({
                    page: this.page - 1,
                    stat : 11,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessManager(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_dsp') {
            result.push('no_dsp');
        }
        return result;
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.joborders = data;
        this.loadingService.loadingStop();
    }

    private onSuccessManager(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.jobordersManager = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-disposisi', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search9(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 12;
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['approval-disposisi', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search11(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 11;
        this.currentSearch = query;
        this.router.navigate(['/approval-disposisi', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected reset(): void {
        this.joborders = new Array<ApprovalDisposisi>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    gotoDetail(rowData) {
        // console.log('datadriverdetail',);
        this.jobOrdersService.passingCustomData(rowData);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalJobOrder() {
        this.eventSubscriber = this.eventManager.subscribe('approvalJobOrderListModification', (response) => this.loadData());
        // console.log('masuk pak eko +++');
    }

    lihatPO(rowData: ApprovalDisposisi) {
        let bbms: string;

        bbms = rowData.no_dsp.replace(/\//g, '-');
        this.jobOrdersService.getDisposisiSearch({
            no_dsp: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailDisposisi = res.json;
                this.modalLihatDisposisi = true;
            })
    }
}
