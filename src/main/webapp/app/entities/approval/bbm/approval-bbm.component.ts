import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import * as BaseConstant from '../../../shared/constants/base.constants';
import * as UnitDocumentMessageConstant from '../../../shared/constants/unit-document-message.constants';
import { ResponseWrapper, CommonUtilService, Principal, ITEMS_PER_PAGE, ToasterService } from '../../../shared';
// import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ApprovalBBM, ApprovalBBMTeknik, DetailBBMTeknik, DetailBBM } from '../approval.model';
import { ApprovalService } from '../approval.service';

@Component({
    selector: 'jhi-approval-bbm',
    templateUrl: './approval-bbm.component.html'
})
export class ApprovalBBMComponent implements OnInit, OnDestroy {
    first: number;
    joborders: ApprovalBBM[];
    joborders1: ApprovalBBMTeknik[];
    joborders2: ApprovalBBMTeknik[];
    public jobOrders: Array<ApprovalBBM>;
    public jobOrders1: Array<ApprovalBBMTeknik>;
    public jobOrder: ApprovalBBM;
    public jobOrder1: ApprovalBBMTeknik;
    public jobOrdersApps: Array<ApprovalBBMTeknik>;
    public jobOrdersApp: ApprovalBBMTeknik;
    public jobOrdersApps1: Array<ApprovalBBMTeknik>;
    public jobOrdersApp1: ApprovalBBMTeknik;
    private eventSubscriber: Subscription;
    detailBBMT: DetailBBMTeknik[];
    detailBBM: DetailBBM[];
    currentAccount: any;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    jobOrderApprove: any;
    jobOrderRejeced: any;
    itemsPerPage: any;
    routeData: any;
    totalItems: any;
    queryCount: any;
    idOrder: any;
    paramPage: number;
    selected: any = [];
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    idStatus: any;
    internal: any;
    userlogin: any;
    loginuser: any;
    // noteCancelJO: string;
    modalLihatBBM2: boolean;
    modalLihatBBM1: boolean;
    public date: Date;
    tahun: Array<object> = [
        // { label: '2019', value: 2019 },
        // { label: '2020', value: 2020 },
        // { label: '2021', value: 2021 },
        // { label: '2022', value: 2022 },
        // { label: '2023', value: 2023 }
    ];
    pilihTahun: number;

    constructor(
        protected principal: Principal,
        protected router: Router,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrdersService: ApprovalService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        protected alertService: JhiAlertService,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 1 > 12) {
            this.pilihTahun = this.date.getFullYear() + 1;
        } else {
            this.pilihTahun = this.date.getFullYear();
        }
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.jobOrders = new Array<ApprovalBBM>();
        this.jobOrder = new ApprovalBBM();
        this.jobOrderApprove = 12;
        this.jobOrderRejeced = 11;
        this.modalLihatBBM2 = false;
        this.modalLihatBBM1 = false;
        // this.noteCancelJO = '';
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahun.push({ label: year.toString(), value: year });
        }
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
        this.registerChangeInApprovalJobOrder();
    }

    protected loadData(): void {
        this.loadingService.loadingStart();
        this.internal = this.principal.getIdInternal();
        this.loginuser = this.principal.getUserLogin();
        if (this.currentSearch) {
            if (this.idStatus === 11) {
                if (this.internal === 'TEKNIK') {
                    this.jobOrdersService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal(),
                        stat : 11,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessTeknik(res.json, res.headers);
                            this.jobOrders1 = res.json;
                            this.jobOrders1 = res.json;
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else {
                    this.jobOrdersService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal(),
                        stat : 11,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccess(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            } else {
                if (this.internal === 'TEKNIK') {
                    this.jobOrdersService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal(),
                        stat : 12,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessTeknik(res.json, res.headers);
                            this.jobOrders1 = res.json;
                            this.jobOrders1 = res.json;
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            }
        } else {
            if (this.internal === 'TEKNIK') {
                this.jobOrdersService.getDataBBMTeknik({
                    page: this.page - 1,
                    stat : 11,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessTeknik(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrdersService.getDataBBMTeknik({
                    page: this.page - 1,
                    stat : 12,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessTeknik1(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            } else {
                this.jobOrdersService.getDataBBM({
                    page: this.page - 1,
                    stat : 11,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            }
        }
    }

private onSuccessTeknik(data, headers) {
    this.totalItems = headers.get('X-Total-Count');
    this.queryCount = this.totalItems;
    this.joborders1 = data;
    this.loadingService.loadingStop();
}

private onSuccessTeknik1(data, headers) {
    this.totalItems = headers.get('X-Total-Count');
    this.queryCount = this.totalItems;
    this.joborders2 = data;
    this.loadingService.loadingStop();
}

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_bbm') {
            result.push('no_bbm');
        }
        return result;
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.joborders = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-bbm', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search9(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 11;
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['approval-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search13(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 12;
        this.currentSearch = query;
        this.router.navigate(['/approval-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected reset(): void {
        this.jobOrders = new Array<ApprovalBBM>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    gotoDetail(rowData) {
        // console.log('datadriverdetail',);
        this.jobOrdersService.passingCustomData(rowData);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalJobOrder() {
        this.eventSubscriber = this.eventManager.subscribe('approvalJobOrderListModification', (response) => this.loadData());
        // console.log('masuk pak eko +++');
    }

    lihatPO(rowData: ApprovalBBMTeknik) {
        let bbms: string;

        bbms = rowData.no_bbm.replace(/\//g, '-');
        this.jobOrdersService.getBBMNO({
            no_bbm: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBBMT = res.json;
                this.modalLihatBBM2 = true;
            })
    }

    lihatPO1(rowData: ApprovalBBM) {
        let bbms: string;

        bbms = rowData.no_bbm.replace(/\//g, '-');
        this.jobOrdersService.getBBMNO({
            no_bbm: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBBM = res.json;
                this.modalLihatBBM1 = true;
            })
    }
}
