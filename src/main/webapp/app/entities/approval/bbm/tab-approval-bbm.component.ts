import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ApprovalService } from '../approval.service';
import { Subscription } from 'rxjs';
import { ApprovalBBM, ApprovalBBMTeknik, DetailBBMTeknik, DetailBBM} from '../approval.model';

@Component({
    selector: 'jhi-tab-approval-bbm',
    templateUrl: './tab-approval-bbm.component.html'
})
export class TabApprovalBBMComponent implements OnInit, OnDestroy {
    @Input() idStatusType: number;
    protected subscription: Subscription;
    currentAccount: any;
    joborders: ApprovalBBM[];
    joborders1: ApprovalBBMTeknik[];
    detailBBMT: DetailBBMTeknik[];
    detailBBM: DetailBBM[];
    public jobOrders: Array<ApprovalBBM>;
    public jobOrders1: Array<ApprovalBBMTeknik>;
    public jobOrder: ApprovalBBM;
    public jobOrder1: ApprovalBBMTeknik;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selectedJob: ApprovalBBM;
    selected: any = [];
    selectedJob1: ApprovalBBMTeknik;
    appJobOrders: ApprovalBBM[];
    appJobOrders1: ApprovalBBMTeknik[];
    internal: any;
    statusidtype: number;
    loginuser: any;
    kategori: any;
    divreciver: any;
    modalLihatBBM1: boolean;
    modalLihatBBM2: boolean;
    bbmT: ApprovalBBMTeknik;
    bbm: ApprovalBBM;
    noBBMT: any;
    noBBM: any;
    idDialogPP: any = [];
    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrderService: ApprovalService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.modalLihatBBM1 = false;
        this.modalLihatBBM2 = false;

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        }

    previousState() {
        window.history.back();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }

    loadData(): void {
        this.loadingService.loadingStart();
        this.internal = this.principal.getIdInternal();
        this.loginuser = this.principal.getUserLogin();
        if (this.currentSearch) {
            if (this.internal === 'TEKNIK') {
                this.jobOrderService.searchBBM({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat : 10,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessTeknik(res.json, res.headers);
                        this.jobOrders1 = res.json;
                        this.jobOrders1 = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            } else {
                this.jobOrderService.searchBBM({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat : 10,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            }
        } else {
            if (this.internal === 'TEKNIK') {
                this.jobOrderService.getDataBBMTeknik({
                    page: this.page - 1,
                    stat : this.idStatusType,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessTeknik(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            } else {
                this.jobOrderService.getDataBBM({
                    page: this.page - 1,
                    stat : this.idStatusType,
                    query: 'idInternal:' + this.principal.getIdInternal(),
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/approval-bbm'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-bbm', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/approval-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    public trackId(index: number, item: ApprovalBBMTeknik): string {
        return item.no_bbm;
    }

    UnregPPAtDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailableDetail();
            }
        });
    }

    createRequestAtDialogDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.ApprovedDetail();
            }
        });
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.joborders = data;
        this.loadingService.loadingStop();
    }

    private onSuccessTeknik(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.joborders1 = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_bbm') {
            result.push('no_bbm');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            data: this.selected
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknik(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailableTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatus(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    notAvailableDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            no_bbm: this.idDialogPP
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknikDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailableTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatusDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Not Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatBBM1 = false;
            this.loadData();
            this.loadingService.loadingStop();
            this.router.navigate(['/approval-bbm'])
        }
        // console.log('ini data internal', data);
    }

    private onSuccessNotAvailableTeknik(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Not Approved !');
            this.modalLihatBBM2 = false;
            this.idDialogPP = null;
            this.selected = null;
            this.loadData();
            this.loadingService.loadingStop();
            this.router.navigate(['/approval-bbm'])
        }
        // console.log('ini data internal', data);
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
        // console.log('isi selected', this.selected);
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.selected,
            status: 11
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknik(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApprovedTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatus(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        // // console.log('isi selected', obj);
        // // console.log(this.internal);
        // // console.log(this.loginuser);
    }

    ApprovedDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            no_bbm: this.idDialogPP,
            status: 11
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknikDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApprovedTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatusDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        // // console.log('isi selected', obj);
        // // console.log(this.internal);
        // // console.log(this.loginuser);
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            // console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatBBM1 = false;
            this.loadData();
            this.router.navigate(['/approval-bbm']);
            this.loadingService.loadingStop();

        }
        // console.log('ini data internal', data);
    }

    private onSuccessApprovedTeknik(data, headers) {
        if (data !== null) {
            // console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Approved !');
            this.modalLihatBBM2 = false;
            this.idDialogPP = null;
            this.selected = null;
            this.loadData();
            this.router.navigate(['/approval-bbm']);
            this.loadingService.loadingStop();
        }
        // console.log('ini data internal', data);
    }

    lihatPO(rowData: ApprovalBBMTeknik) {
        let bbms: string;

        bbms = rowData.no_bbm.replace(/\//g, '-');
        this.jobOrderService.getBBMNO({
            no_bbm: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBBMT = res.json;
                this.modalLihatBBM2 = true;
                this.idDialogPP = rowData.no_bbm;
            })
    }

    lihatPO1(rowData: ApprovalBBM) {
        let bbms: string;

        bbms = rowData.no_bbm.replace(/\//g, '-');
        this.jobOrderService.getBBMNO({
            no_bbm: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBBM = res.json;
                this.modalLihatBBM1 = true;
                this.idDialogPP = rowData.no_bbm;
            })
    }

    backapp() {
        this.idDialogPP = null;
        this.modalLihatBBM1 = false;
    }

    backapp1() {
        this.idDialogPP = null;
        this.modalLihatBBM2 = false;
    }

}
