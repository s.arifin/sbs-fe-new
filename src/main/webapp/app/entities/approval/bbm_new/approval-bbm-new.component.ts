import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { Subscription } from 'rxjs';
import { ApprovalBBM, ApprovalBBMTeknik, DetailBBMTeknik, DetailBBM } from '../approval.model';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ApprovalService } from '../approval.service';

@Component({
    selector: 'jhi-approval-bbm-new',
    templateUrl: './approval-bbm-new.component.html',
})
export class ApprovalBBMNewComponent implements OnInit {
    @Input() pilihTahun: any
    public data: any;
    public date: Date;
    public options: any;
    public user: any;
    periode: any;
    newBBM: ApprovalBBM[];
    approveBBM: ApprovalBBM[];
    newBBMTeknik: ApprovalBBMTeknik[];
    approveBBMTeknik: ApprovalBBMTeknik[];
    notapporveBBMTeknik: ApprovalBBMTeknik[];
    detailBBMT: DetailBBMTeknik[];
    detailBBM: DetailBBM[];
    routeData: any;
    links: any;
    totalItemsNew: any;
    totalItems: any;
    queryCount: any;
    totalItemsApprove: any;
    totalItemsNotApprove: any;
    queryCountNew: any;
    queryCountApprove: any;
    queryCountNotApprove: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selected: any = [];
    internal: any;
    loginuser: any;
    total: any;
    modalLihatBBMT: boolean;
    modalLihatBBM: boolean;
    bbmT: ApprovalBBMTeknik;
    bbm: ApprovalBBM;
    idDialogPP: any = [];
    currentAccount: any;
    idsearch: any;
    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected jobOrderService: ApprovalService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
    ) {
        this.modalLihatBBMT = false;
        this.modalLihatBBM = false;
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.total = 0;
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }

    loadData() {
        this.loadingService.loadingStart();
        if (this.pilihTahun === undefined || this.pilihTahun === '' || this.pilihTahun === null) {
            this.date = new Date();
            if (this.date.getMonth() + 1 > 12) {
                this.pilihTahun = this.date.getFullYear() + 1;
            } else {
                this.pilihTahun = this.date.getFullYear();
            }
        } else {
            this.pilihTahun = this.pilihTahun;
        }
        this.internal = this.principal.getIdInternal();
        this.loginuser = this.principal.getUserLogin();
        if (this.currentSearch) {
            if (this.internal === 'TEKNIK') {
                if (this.idsearch === 10) {
                    this.jobOrderService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 10,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNewTeknik(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else if (this.idsearch === 11) {
                    this.jobOrderService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 11,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApproveTeknik(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else {
                    this.jobOrderService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 12,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNotApproveTeknik(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            } else {
                if (this.idsearch === 10) {
                    this.jobOrderService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 10,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else if (this.idsearch === 11) {
                    this.jobOrderService.searchBBM({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 11,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            }
        } else {
            if (this.internal === 'TEKNIK') {
                this.jobOrderService.getDataBBMTeknik({
                    page: this.page - 1,
                    stat: 10,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessNewTeknik(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrderService.getDataBBMTeknik({
                    page: this.page - 1,
                    stat: 11,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessApproveTeknik(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrderService.getDataBBMTeknik({
                    page: this.page - 1,
                    stat: 12,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessNotApproveTeknik(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            } else {
                this.jobOrderService.getDataBBM({
                    page: this.page - 1,
                    stat: 10,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessNew(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrderService.getDataBBM({
                    page: this.page - 1,
                    stat: 11,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessApprove(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            }
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/approval-bbm'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-bbm', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchNew(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.idsearch = 10;
        this.router.navigate(['/approval-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchApprove(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.idsearch = 11;
        this.router.navigate(['/approval-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchNotApprove(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.idsearch = 12;
        this.router.navigate(['/approval-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }

    public trackId(index: number, item: ApprovalBBMTeknik): string {
        return item.no_bbm;
    }

    UnregPPAtDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailableDetail();
            }
        });
    }

    createRequestAtDialogDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.ApprovedDetail();
            }
        });
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccessNew(data, headers) {
        this.totalItemsNew = headers.get('X-Total-Count');
        this.queryCountNew = this.totalItemsNew;
        this.newBBM = data;
        this.loadingService.loadingStop();
    }

    private onSuccessApprove(data, headers) {
        this.totalItemsApprove = headers.get('X-Total-Count');
        this.queryCountApprove = this.totalItemsApprove;
        this.approveBBM = data;
        this.loadingService.loadingStop();
    }

    private onSuccessNewTeknik(data, headers) {
        this.totalItemsNew = headers.get('X-Total-Count');
        this.queryCountNew = this.totalItemsNew;
        this.newBBMTeknik = data;
        this.loadingService.loadingStop();
    }

    private onSuccessApproveTeknik(data, headers) {
        this.totalItemsApprove = headers.get('X-Total-Count');
        this.queryCountApprove = this.totalItemsApprove;
        this.approveBBMTeknik = data;
        this.loadingService.loadingStop();
    }

    private onSuccessNotApproveTeknik(data, headers) {
        this.totalItemsNotApprove = headers.get('X-Total-Count');
        this.queryCountNotApprove = this.totalItemsNotApprove;
        this.notapporveBBMTeknik = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_bbm') {
            result.push('no_bbm');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            data: this.selected
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknik(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailableTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatus(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    notAvailableDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            no_bbm: this.idDialogPP
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknikDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailableTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatusDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Not Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatBBM = false;
            this.loadData();
            this.loadingService.loadingStop();
            this.router.navigate(['/approval-bbm'])
        }
        // console.log('ini data internal', data);
    }

    private onSuccessNotAvailableTeknik(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Not Approved !');
            this.modalLihatBBMT = false;
            this.idDialogPP = null;
            this.selected = null;
            this.loadData();
            this.loadingService.loadingStop();
            this.router.navigate(['/approval-bbm'])
        }
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.selected,
            status: 11
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknik(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApprovedTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatus(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    ApprovedDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            no_bbm: this.idDialogPP,
            status: 11
        }
        if (this.internal === 'TEKNIK') {
            this.jobOrderService.changeStatusTeknikDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApprovedTeknik(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.jobOrderService.changeStatusDetail(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            // console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatBBM = false;
            this.loadData();
            this.router.navigate(['/approval-bbm']);
            this.loadingService.loadingStop();

        }
        // console.log('ini data internal', data);
    }

    private onSuccessApprovedTeknik(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BBM Approved !');
            this.modalLihatBBMT = false;
            this.idDialogPP = null;
            this.selected = null;
            this.loadData();
            this.router.navigate(['/approval-bbm']);
            this.loadingService.loadingStop();
        }
        // console.log('ini data internal', data);
    }

    lihatBBM(rowData: ApprovalBBM) {
        let bbms: string;

        bbms = rowData.no_bbm.replace(/\//g, '-');
        this.jobOrderService.getBBMNO({
            no_bbm: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBBM = res.json;
                this.modalLihatBBM = true;
                this.idDialogPP = rowData.no_bbm;
                this.detailBBM.forEach((e) => this.total += e.qty)
            });
    }

    lihatBBMT(rowData: ApprovalBBMTeknik) {
        let bbms: string;

        bbms = rowData.no_bbm.replace(/\//g, '-');
        this.jobOrderService.getBBMNO({
            no_bbm: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBBMT = res.json;
                this.modalLihatBBMT = true;
                this.idDialogPP = rowData.no_bbm;
            })
    }

    backapp() {
        this.idDialogPP = null;
        this.modalLihatBBM = false;
        this.total = 0;
    }

    backapp1() {
        this.idDialogPP = null;
        this.modalLihatBBMT = false;
    }

    filter() {
        this.loadData();
    }
}
