import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { ApprovalBBM, ApprovalBPB, ApprovalDisposisi, ApprovalLHP } from './approval.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Subject } from 'rxjs/Subject';
import { JhiDateUtils } from 'ng-jhipster';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ApprovalService {

    protected itemValues: ApprovalBBM[];
    values: Subject<any> = new Subject<any>();

    private resourceUrl = process.env.API_C_URL + '/api/approval';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/approval';

    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, protected dateUtils: JhiDateUtils) { }

    public getDataBBM(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/newBBM', options)
            .map((res: Response) => this.convertResponse(res));
    }

    public getDataBBMTeknik(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/newBBM', options)
            .map((res: Response) => this.convertResponse(res));
    }

    public getDataApprove(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/appBBM', options)
            .map((res: Response) => this.convertResponse(res));
    }

    public getDataCancel(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/rejBBM', options)
            .map((res: Response) => this.convertResponse(res));
    }

    searchBBM(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/searchBBM', options)
            .map((res: any) => this.convertResponse(res));
    }

    getBBMNO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-detail-bbm', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatus'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusTeknik(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/approveTeknik'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusTeknikDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/approveTeknikDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    public getDataDisposisi(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/newDisposisi', options)
            .map((res: Response) => this.convertResponse(res));
    }

    searchDisposisi(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/searchDisposisi', options)
            .map((res: any) => this.convertResponse(res));
    }

    getDisposisiSearch(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-detail-disposisi', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatusDisposisi(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusDisposisi'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusDisposisiDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusDisposisiDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    public getDataBpb(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/newBPB', options)
            .map((res: Response) => this.convertResponse(res));
    }

    searchBPB(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/searchBPB', options)
            .map((res: any) => this.convertResponse(res));
    }

    getDetailBPB(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-detail-bpb', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatusBPB(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusBPB'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusBPBDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusBPBDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusLHPDetail(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusLHPDetail'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    changeStatusLHP(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatusLHP'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    getDetail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-detail-lhp', options)
            .map((res: Response) => this.convertResponse(res));
    }

    searchLHP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/searchLHP', options)
            .map((res: any) => this.convertResponse(res));
    }

    public getDataLHP(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/newLHP', options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }
}
