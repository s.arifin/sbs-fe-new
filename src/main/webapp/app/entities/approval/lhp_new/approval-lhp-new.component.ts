import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { Subscription } from 'rxjs';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ApprovalService } from '../approval.service';
import { ApprovalLHP, ApprovalDetailLHP } from '../approval.model';

@Component({
    selector: 'jhi-approval-lhp-new',
    templateUrl: './approval-lhp-new.component.html',
})
export class ApprovalLHPNewComponent implements OnInit {
    @Input() pilihTahun: any
    public data: any;
    public date: Date;
    public options: any;
    public user: any;
    periode: any;
    newLHP: ApprovalLHP[];
    approveLHP: ApprovalLHP[];
    detailLHP: ApprovalDetailLHP[];
    routeData: any;
    links: any;
    totalItems: AudioNode;
    totalItemsNew: any;
    totalItemsApprove: any;
    queryCount: any;
    queryCountNew: any;
    queryCountApprove: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selected: any = [];
    internal: any;
    loginuser: any;
    idDialogPP: any = [];
    currentAccount: any;
    idsearch: any;
    modalLihatLHP: boolean;
    lhp: ApprovalLHP;
    no_lhp: any;
    idStatus: any;
    iduser: any;
    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected jobOrdersService: ApprovalService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
    ) {
        this.modalLihatLHP = false;
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }

    loadData() {
        this.loadingService.loadingStart();
        if (this.pilihTahun === undefined || this.pilihTahun === '' || this.pilihTahun === null) {
            this.date = new Date();
            if (this.date.getMonth() + 1 > 12) {
                this.pilihTahun = this.date.getFullYear() + 1;
            } else {
                this.pilihTahun = this.date.getFullYear();
            }
        } else {
            this.pilihTahun = this.pilihTahun;
        }
        if (this.currentSearch) {
            if (this.idsearch === 10) {
                this.jobOrdersService.searchLHP({
                    internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    stat: 10,
                    page: this.page - 1,
                    query: 'cari:' + this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessNew(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            } else {
                this.jobOrdersService.searchLHP({
                    internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    stat: 11,
                    page: this.page - 1,
                    query: 'cari:' + this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessApprove(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            }
        } else {
            this.jobOrdersService.getDataLHP({
                page: this.page - 1,
                stat: 10,
                query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessNew(res.json, res.headers);
                },
                (res: ResponseWrapper) => {
                    this.onError(res.json);
                }
            );
            this.jobOrdersService.getDataLHP({
                page: this.page - 1,
                stat: 11,
                query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccessApprove(res.json, res.headers);
                },
                (res: ResponseWrapper) => {
                    this.onError(res.json);
                }
            );
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/approval-lhp'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-lhp', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchNew(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.idsearch = 10;
        this.router.navigate(['/approval-lhp', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchApprove(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.idsearch = 11;
        this.router.navigate(['/approval-lhp', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }

    public trackId(index: number, item: ApprovalLHP): string {
        return item.no_periksa;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccessNew(data, headers) {
        this.totalItemsNew = headers.get('X-Total-Count');
        this.queryCountNew = this.totalItemsNew;
        this.newLHP = data;
        this.loadingService.loadingStop();
    }

    private onSuccessApprove(data, headers) {
        this.totalItemsApprove = headers.get('X-Total-Count');
        this.queryCountApprove = this.totalItemsApprove;
        this.approveLHP = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_periksa') {
            result.push('no_periksa');
        }
        return result;
    }

    backapp() {
        this.idDialogPP = null;
        this.modalLihatLHP = false;
    }

    filter() {
        this.loadData();
    }

    UnregPPAtDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailableDetail();
            }
        });
    }

    createRequestAtDialogDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.ApprovedDetail();
            }
        });
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            data: this.selected
        }
        this.jobOrdersService.changeStatusLHP(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }
    notAvailableDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            no_periksa: this.idDialogPP
        }
        this.jobOrdersService.changeStatusTeknikDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'LHP Not Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatLHP = false;
            this.loadData();
            this.loadingService.loadingStop();
            this.router.navigate(['/approval-lhp'])
        }
        // console.log('ini data internal', data);
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
        // console.log('isi selected', this.selected);
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.selected,
            status: 11
        }
        this.jobOrdersService.changeStatusLHP(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ApprovedDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            no_periksa: this.idDialogPP,
            status: 11
        }
        this.jobOrdersService.changeStatusLHPDetail(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            // console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'LHP Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatLHP = false;
            this.loadData();
            this.router.navigate(['/approval-lhp']);
            this.loadingService.loadingStop();

        }
        // console.log('ini data internal', data);
    }

    lihatLHP(rowData: ApprovalLHP) {
        let bbms: string;

        if (rowData.no_periksa.includes('LPRM') || rowData.no_periksa.includes('LHPQ') || rowData.no_periksa.includes('LPPM') || rowData.no_periksa.includes('LPGM')) {
            bbms = rowData.no_periksa.replace(/\//g, '-');
            this.jobOrdersService.getDetail({
                no_periksa: bbms
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.detailLHP = res.json;
                    this.modalLihatLHP = true;
                    this.idDialogPP = rowData.no_periksa;
                })
        } else {
            bbms = rowData.no_periksa.replace(/\//g, '-');
            this.jobOrdersService.getDetail({
                no_periksa: bbms
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.detailLHP = res.json;
                    this.modalLihatLHP = true;
                    this.idDialogPP = rowData.no_periksa;
                })
        }
    }

}
