import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { Subscription } from 'rxjs';
import { ApprovalBPB, DetailBPB } from '../approval.model';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ApprovalService } from '../approval.service';

@Component({
    selector: 'jhi-approval-bpb-new',
    templateUrl: './approval-bpb-new.component.html',
})
export class ApprovalBPBNewComponent implements OnInit {
    @Input() pilihTahun: any
    public data: any;
    public date: Date;
    public options: any;
    public user: any;
    periode: any;
    newBPB: ApprovalBPB[];
    approveBPB: ApprovalBPB[];
    newBPB1: ApprovalBPB[];
    approveBPB1: ApprovalBPB[];
    detailBPB: DetailBPB[];
    routeData: any;
    links: any;
    totalItemsNew: any;
    totalItemsNew1: any;
    totalItems: any;
    totalItemsApprove: any;
    totalItemsApprove1: any;
    queryCountNew: any;
    queryCountNew1: any;
    queryCountApprove: any;
    queryCountApprove1: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selected: any = [];
    internal: any;
    loginuser: any;
    modalLihatBPB: boolean;
    bpb: ApprovalBPB;
    idDialogPP: any = [];
    currentAccount: any;
    idsearch: any;
    iduser: any;
    arrSup: any = [];
    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected jobOrderService: ApprovalService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
    ) {
        this.modalLihatBPB = false;
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.arrSup = ['MHD', 'ABR', 'AEN', 'ARW', 'DAW', 'EKS', 'HTB', 'VER', 'RFL', 'NTO', 'MDR'];
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loginuser = this.principal.getUserLogin();
            if (this.arrSup.includes(this.loginuser)) {
                this.iduser = 1;
            } else {
                this.iduser = 2;
            }
            this.loadData();
        });
    }

    loadData() {
        this.loadingService.loadingStart();
        if (this.pilihTahun === undefined || this.pilihTahun === '' || this.pilihTahun === null) {
            this.date = new Date();
            if (this.date.getMonth() + 1 > 12) {
                this.pilihTahun = this.date.getFullYear() + 1;
            } else {
                this.pilihTahun = this.date.getFullYear();
            }
        } else {
            this.pilihTahun = this.pilihTahun;
        }
        this.internal = this.principal.getIdInternal();
        if (this.currentSearch) {
            if (this.iduser === 1) {
                if (this.idsearch === 9) {
                    this.jobOrderService.searchBPB({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 9,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew1(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else {
                    this.jobOrderService.searchBPB({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 11,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove1(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            } else {
                if (this.idsearch === 10) {
                    this.jobOrderService.searchBPB({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 10,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else {
                    this.jobOrderService.searchBPB({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat: 12,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            }
        } else {
            if (this.iduser === 1) {
                this.jobOrderService.getDataBpb({
                    page: this.page - 1,
                    stat: 9,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessNew1(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrderService.getDataBpb({
                    page: this.page - 1,
                    stat: 11,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessApprove1(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            } else {
                this.jobOrderService.getDataBpb({
                    page: this.page - 1,
                    stat: 10,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessNew(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );

                this.jobOrderService.getDataBpb({
                    page: this.page - 1,
                    stat: 12,
                    query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccessApprove(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => {
                        this.onError(res.json);
                    }
                );
            }
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/approval-bpb'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-bpb', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchNew(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        if (this.iduser === 1) {
            this.idsearch = 9;
        } else {
            this.idsearch = 10;
        }
        this.router.navigate(['/approval-bpb', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchApprove(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        if (this.iduser === 1) {
            this.idsearch = 11;
        } else {
            this.idsearch = 12;
        }
        this.router.navigate(['/approval-bpb', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }

    public trackId(index: number, item: ApprovalBPB): string {
        return item.no_bpb;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccessNew(data, headers) {
        this.totalItemsNew = headers.get('X-Total-Count');
        this.queryCountNew = this.totalItemsNew;
        this.newBPB = data;
        this.loadingService.loadingStop();
    }

    private onSuccessApprove(data, headers) {
        this.totalItemsApprove = headers.get('X-Total-Count');
        this.queryCountApprove = this.totalItemsApprove;
        this.approveBPB = data;
        this.loadingService.loadingStop();
    }

    private onSuccessNew1(data, headers) {
        this.totalItemsNew1 = headers.get('X-Total-Count');
        this.queryCountNew1 = this.totalItemsNew1;
        this.newBPB1 = data;
        this.loadingService.loadingStop();
    }

    private onSuccessApprove1(data, headers) {
        this.totalItemsApprove1 = headers.get('X-Total-Count');
        this.queryCountApprove1 = this.totalItemsApprove1;
        this.approveBPB1 = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_bpb') {
            result.push('no_bpb');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
    }

    Approved() {
        if (this.iduser === 1) {
            const obj = {
                idinternal: this.principal.getIdInternal(),
                createdBy: this.principal.getUserLogin(),
                data: this.selected,
                status: 11
            }
            this.jobOrderService.changeStatusBPB(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            const obj = {
                idinternal: this.principal.getIdInternal(),
                createdBy: this.principal.getUserLogin(),
                data: this.selected,
                status: 9
            }
            this.jobOrderService.changeStatusBPB(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            // console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'BPB Approved !');
            this.idDialogPP = null;
            this.selected = null;
            this.modalLihatBPB = false;
            this.loadData();
            this.router.navigate(['/approval-bpb']);
            this.loadingService.loadingStop();

        }
        // console.log('ini data internal', data);
    }

    lihatBPB(rowData: ApprovalBPB) {
        let bbms: string;

        bbms = rowData.no_bpb.replace(/\//g, '-');
        this.jobOrderService.getDetailBPB({
            no_bpb: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailBPB = res.json;
                this.modalLihatBPB = true;
                this.idDialogPP = rowData.no_bpb;
            })
    }

    backapp() {
        this.idDialogPP = null;
        this.modalLihatBPB = false;
    }

    filter() {
        this.loadData();
    }
}
