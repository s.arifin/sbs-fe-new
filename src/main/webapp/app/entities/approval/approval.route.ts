import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { ApprovalBBMComponent } from './bbm/approval-bbm.component';
import { TabApprovalBBMComponent } from './bbm/tab-approval-bbm.component';
import { ApprovalDisposisi } from './approval.model';
import { ApprovalDisposisiComponent } from './disposisi/approval-disposisi.component';
import { ApprovalBpbComponent } from './bpb/approval-bpb.component';
import { ApprovalLHPComponent } from './lhp/approval-lhp.component';
import { TabApprovalBBMNewComponent } from './bbm_new/tab-approval-bbm-new.component';
import { TabApprovalDisposisiNewComponent } from './disposisi_new/tab-approval-disposisi-new.component';
import { TabApprovalBPBNewComponent } from './bpb_new/tab-approval-bpb-new.component';
import { TabApprovalLHPNewComponent } from './lhp_new/tab-approval-lhp-new.component';
// import {BudgetingItComponent} from '../purchase-order/budgeting-it/it-budgeting.component'
@Injectable()
export class ApprovalResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const approvalRoute: Routes = [
    {
        path: 'approval-bbm',
        component: TabApprovalBBMNewComponent,
        resolve: {
            'pagingParams': ApprovalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval1.bbm'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-disposisi',
        component: TabApprovalDisposisiNewComponent,
        resolve: {
            'pagingParams': ApprovalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval1.disposisi'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-bpb',
        component: TabApprovalBPBNewComponent,
        resolve: {
            'pagingParams': ApprovalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval1.bpb'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-lhp',
        component: TabApprovalLHPNewComponent,
        resolve: {
            'pagingParams': ApprovalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval1.lhp'
        },
        canActivate: [UserRouteAccessService]
    }
];
