import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    ApprovalService,
    TabApprovalBBMComponent,
    ApprovalBBMComponent,
    TabApprovalDisposisiComponent,
    ApprovalDisposisiComponent,
    TabApprovalBpbComponent,
    ApprovalBpbComponent,
    TabApprovalLHPComponent,
    ApprovalLHPComponent,
    TabApprovalBBMNewComponent,
    ApprovalBBMNewComponent,
    TabApprovalDisposisiNewComponent,
    ApprovalDisposisiNewComponent,
    TabApprovalBPBNewComponent,
    ApprovalBPBNewComponent,
    TabApprovalLHPNewComponent,
    ApprovalLHPNewComponent
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextareaModule,
         PaginatorModule,
         ConfirmDialogModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         TabViewModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { ApprovalResolvePagingParams, approvalRoute } from './approval.route';

const ENTITY_STATES = [
    ...approvalRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        TabViewModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        SliderModule,
        RadioButtonModule,
        ],
    exports: [
        ApprovalBBMComponent,
        TabApprovalBBMComponent,
        ApprovalDisposisiComponent,
        TabApprovalDisposisiComponent,
        ApprovalBpbComponent,
        TabApprovalBpbComponent,
        TabApprovalLHPComponent,
        TabApprovalBBMNewComponent,
        ApprovalLHPComponent,
        ApprovalBBMNewComponent,
        TabApprovalDisposisiNewComponent,
        ApprovalDisposisiNewComponent,
        TabApprovalBPBNewComponent,
        ApprovalBPBNewComponent,
        TabApprovalLHPNewComponent,
        ApprovalLHPNewComponent
    ],
    declarations: [
        ApprovalBBMComponent,
        TabApprovalBBMComponent,
        ApprovalDisposisiComponent,
        TabApprovalDisposisiComponent,
        ApprovalBpbComponent,
        TabApprovalBpbComponent,
        TabApprovalLHPComponent,
        ApprovalLHPComponent,
        TabApprovalBBMNewComponent,
        ApprovalBBMNewComponent,
        TabApprovalDisposisiNewComponent,
        ApprovalDisposisiNewComponent,
        TabApprovalBPBNewComponent,
        ApprovalBPBNewComponent,
        TabApprovalLHPNewComponent,
        ApprovalLHPNewComponent
    ],
    entryComponents: [
        ApprovalBBMComponent,
        TabApprovalBBMComponent,
        ApprovalDisposisiComponent,
        TabApprovalDisposisiComponent,
        ApprovalBpbComponent,
        TabApprovalBpbComponent,
        TabApprovalLHPComponent,
        ApprovalLHPComponent,
        TabApprovalBBMNewComponent,
        ApprovalBBMNewComponent,
        TabApprovalDisposisiNewComponent,
        ApprovalDisposisiNewComponent,
        TabApprovalBPBNewComponent,
        ApprovalBPBNewComponent,
        TabApprovalLHPNewComponent,
        ApprovalLHPNewComponent
    ],
    providers: [
        ApprovalService,
        ApprovalResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmApprovalModule {}
