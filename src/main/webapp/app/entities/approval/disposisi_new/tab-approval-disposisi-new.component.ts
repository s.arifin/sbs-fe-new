import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-tab-approval-disposisi-new',
    templateUrl: './tab-approval-disposisi-new.component.html',

})

export class TabApprovalDisposisiNewComponent implements OnInit {
    public data: any;
    public date: Date;
    public options: any;
    periode: any;
    pilihBulan: number;
    tahun: Array<object> = [
        // { label: '2019', value: 2019 },
        // { label: '2020', value: 2020 },
        // { label: '2021', value: 2021 },
        // { label: '2022', value: 2022 },
        // { label: '2023', value: 2023 }
    ];
    pilihTahun: number;
    constructor(
        private loadingService: LoadingService,
        private router: Router,
    ) {
        this.date = new Date();
        if (this.date.getMonth() + 1 > 12) {
            this.pilihTahun = this.date.getFullYear() + 1;
        } else {
            this.pilihTahun = this.date.getFullYear();
        }
        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahun.push({ label: year.toString(), value: year});
        }
    }

    ngOnInit() {
    }

    loadData() {
    }

    filter() {
        console.log('ini adalah bulan periode yang dipilih == ', this.pilihBulan);
        console.log('ini adalah tahun periode yang dipilih == ', this.pilihTahun);
    }
}
