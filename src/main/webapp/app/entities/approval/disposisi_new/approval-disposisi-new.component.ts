import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { Subscription } from 'rxjs';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ApprovalService } from '../approval.service';
import { ApprovalDisposisi, DetailDisposisi} from '../approval.model';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';

@Component({
  selector: 'jhi-approval-disposisi-new',
  templateUrl: './approval-disposisi-new.component.html',
})
export class ApprovalDisposisiNewComponent implements OnInit {
@Input() pilihTahun: any
  public data: any;
  public date: Date;
  public options: any;
  public user: any;
  periode: any;
  newDisposisi: ApprovalDisposisi[];
  approveDisposisi: ApprovalDisposisi[];
  detailDisposisi: DetailDisposisi[];
  routeData: any;
  links: any;
  totalItems: AudioNode;
  totalItemsNew: any;
  totalItemsApprove: any;
  queryCount: any;
  queryCountNew: any;
  queryCountApprove: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  first: Number;
  previousPage: any;
  isLazyLoadingFirst: Boolean = false;
  reverse: any;
  currentSearch: string;
  selected: any = [];
  internal: any;
  loginuser: any;
  idDialogPP: any = [];
  currentAccount: any;
  idsearch: any;
  modalLihatDisposisi: boolean;
  disposisi: ApprovalDisposisi;
  no_dsp: any;
  idStatus: any;
  iduser:  any;
  constructor(
    protected principal: Principal,
    protected toasterService: ToasterService,
    protected confirmationService: ConfirmationService,
    protected jobOrdersService: ApprovalService,
    protected loadingService: LoadingService,
    protected commonUtilService: CommonUtilService,
    protected principalService: Principal,
    protected activatedRoute: ActivatedRoute,
    protected parseLinks: JhiParseLinks,
    protected alertService: JhiAlertService,
    protected router: Router,
    private eventManager: JhiEventManager,
    protected toaster: ToasterService,
  ) {
    this.modalLihatDisposisi = false;
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
  }

  ngOnInit() {
    this.principal.identity(true).then((account) => {
        this.currentAccount = account;
        this.loadData();
    });
  }

  loadData() {
        this.loadingService.loadingStart();
        this.internal = this.principal.getIdInternal();
        this.loginuser = this.principal.getUserLogin().toUpperCase();
        if (this.loginuser === 'IKM' || this.loginuser === 'HKW' ) {
            this.iduser = 1;
        } else {
            this.iduser = 2;
        }

        if (this.currentSearch) {
            if (this.iduser === 1) {
                if (this.idStatus === 9 ) {
                        this.jobOrdersService.searchDisposisi({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat : 9,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else if (this.idStatus === 11) {
                        this.jobOrdersService.searchDisposisi({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat : 11,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            } else {
                if (this.idStatus === 10 ) {
                        this.jobOrdersService.searchDisposisi({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat : 10,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                } else if (this.idStatus === 12) {
                        this.jobOrdersService.searchDisposisi({
                        internals: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        stat : 12,
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => this.onErrorSearch(res.json)
                    );
                    return;
                }
            }
        } else {
            if (this.iduser === 1) {
                    this.jobOrdersService.getDataDisposisi({
                        page: this.page - 1,
                        stat : 9,
                        query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => {
                            this.onError(res.json);
                        }
                    );
                    this.jobOrdersService.getDataDisposisi({
                        page: this.page - 1,
                        stat : 11,
                        query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => {
                            this.onError(res.json);
                        }
                    );
            } else {
                    this.jobOrdersService.getDataDisposisi({
                        page: this.page - 1,
                        stat : 10,
                        query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessNew(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => {
                            this.onError(res.json);
                        }
                    );

                    this.jobOrdersService.getDataDisposisi({
                        page: this.page - 1,
                        stat : 12,
                        query: 'idInternal:' + this.principal.getIdInternal() + '|tahun:' + this.pilihTahun,
                        size: this.itemsPerPage,
                        sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.onSuccessApprove(res.json, res.headers);
                        },
                        (res: ResponseWrapper) => {
                            this.onError(res.json);
                        }
                    );
            }
        }
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
        this.previousPage = page;
        this.transition();
    }
}

    transition() {
        this.router.navigate(['/approval-disposisi'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-disposisi', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchNew(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        if (this.iduser === 1) {
            this.idStatus = 9;
        } else {
            this.idStatus = 10;
        }
        this.router.navigate(['/approval-disposisi', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    searchApprove(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        if (this.iduser === 1) {
            this.idStatus = 11;
        } else {
            this.idStatus = 12;
        }
        this.router.navigate(['/approval-disposisi', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    protected onErrorSearch(error) {
        // console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }

    public trackId(index: number, item: ApprovalDisposisi): string {
        return item.no_dsp;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccessNew(data, headers) {
        this.totalItemsNew = headers.get('X-Total-Count');
        this.queryCountNew = this.totalItemsNew;
        this.newDisposisi = data;
        this.loadingService.loadingStop();
    }

    private onSuccessApprove(data, headers) {
        this.totalItemsApprove = headers.get('X-Total-Count');
        this.queryCountApprove = this.totalItemsApprove;
        this.approveDisposisi = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'no_dsp') {
            result.push('no_dsp');
        }
        return result;
    }

    UnregPPAtDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailableDetail();
            }
        });
    }

    createRequestAtDialogDetail() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.ApprovedDetail();
            }
        });
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Tidak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            data: this.selected
        }
            this.jobOrdersService.changeStatusDisposisi(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }

    notAvailableDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            status: 12,
            data: this.idDialogPP
        }
            this.jobOrdersService.changeStatusDisposisi(obj).subscribe(
                (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'Disposisi Not Approved !');
            this.modalLihatDisposisi = false;
            this.idDialogPP = null;
            this.loadData();
            this.loadingService.loadingStop();
            this.router.navigate(['/approval-disposisi'])
        }
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
    }

    Approved() {
        if (this.iduser === 1) {
            const obj = {
                idinternal: this.principal.getIdInternal(),
                createdBy: this.principal.getUserLogin(),
                data: this.selected,
                status1: 11
            }
                this.jobOrdersService.changeStatusDisposisi(obj).subscribe(
                    (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        } else {
            const obj = {
                idinternal: this.principal.getIdInternal(),
                createdBy: this.principal.getUserLogin(),
                data: this.selected,
                status1: 9
            }
                this.jobOrdersService.changeStatusDisposisi(obj).subscribe(
                    (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        }

    }

    ApprovedDetail() {
        if (this.iduser === 1) {
            const obj = {
                idinternal: this.principal.getIdInternal(),
                createdBy: this.principal.getUserLogin(),
                no_dsp: this.idDialogPP,
                status1: 11
            }
                this.jobOrdersService.changeStatusDisposisiDetail(obj).subscribe(
                    (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        } else {
            const obj = {
                idinternal: this.principal.getIdInternal(),
                createdBy: this.principal.getUserLogin(),
                no_dsp: this.idDialogPP,
                status1: 9
            }
                this.jobOrdersService.changeStatusDisposisiDetail(obj).subscribe(
                    (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        }
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            // console.log('masuk on sukes approve');
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'Disposisi Approved !');
            this.idDialogPP = null;
            this.selected.length = 0;
            this.modalLihatDisposisi = false;
            this.loadData();
            this.router.navigate(['/approval-disposisi']);
            this.loadingService.loadingStop();

        }
        // console.log('ini data internal', data);
    }

    lihatDisposisi(rowData: ApprovalDisposisi) {
        let bbms: string;

        bbms = rowData.no_dsp.replace(/\//g, '-');
        this.jobOrdersService.getDisposisiSearch({
            no_dsp: bbms
        }).subscribe(
            (res: ResponseWrapper) => {
                this.detailDisposisi = res.json;
                this.modalLihatDisposisi = true;
                this.idDialogPP = rowData.no_dsp;
            })
    }

    backapp() {
        this.idDialogPP = null;
        this.modalLihatDisposisi = false;
    }

    filter() {
        this.loadData();
    }

}
