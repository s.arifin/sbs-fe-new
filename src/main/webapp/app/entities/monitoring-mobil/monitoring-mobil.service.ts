import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, Headers } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { ResponseWrapper, createRequestOption } from '../../shared';
// import { MasterBahan } from './.model';
import { MasterBahan } from '../master_bahan';
import { MonitoringMobilVM } from './monitoring-mobil.model';

@Injectable()
export class MonitoringMobilService {
    protected itemValues: MonitoringMobilVM[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/monitoring-mobil';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/monitoring-mobil';
    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    // create(picRole: MasterBahan): Observable<MasterBahan> {
    //     const copy = this.convert(picRole);
    //     return this.http.post(this.resourceUrl, copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }
    create(unitChecks: any): Observable<ResponseWrapper> {
        const copy = this.convert(unitChecks);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convert(payment: MonitoringMobilVM): MonitoringMobilVM {
        const copy: MonitoringMobilVM = Object.assign({}, payment);
        return copy;
    }

    private convertArray(payment: MonitoringMobilVM[]): MonitoringMobilVM[] {
        const copy: MonitoringMobilVM[] = Object.assign([], payment);
        return copy;
    }

    find(id: any): Observable<MonitoringMobilVM> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }
}
