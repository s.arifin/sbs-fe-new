import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TabViewModule } from 'primeng/primeng';
import { MonitoringMobilComponent } from './monitoring-mobil.component';
import { MonitoringMobilResolvePagingParams, MonitoringMobiltRoute } from './monitoring-mobil.route';
import { MonitoringMobilService } from './monitoring-mobil.service';
import { MonitoringMobilVM } from './monitoring-mobil.model';

const ENTITY_STATES = [
    ...MonitoringMobiltRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule
    ],
    declarations: [
        MonitoringMobilComponent,
    ],
    entryComponents: [
        MonitoringMobilComponent
    ],
    providers: [
        MonitoringMobilService,
        MonitoringMobilResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMonitoringMobileModule { }
