import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService, LazyLoadEvent, AccordionTab, DataTable } from 'primeng/primeng';
import { LoadingBarHttp } from '@ngx-loading-bar/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { ExcelService } from '../report/excel.service';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { MonitoringMobilVM } from './monitoring-mobil.model';
import { MonitoringMobilService } from './monitoring-mobil.service';

@Component({
    selector: 'jhi-monitoring-status-mobil',
    templateUrl: './monitoring-mobil.component.html',
    styleUrls: ['monitoring-mobil.scss'] // Pastikan ini sudah ada
})
export class MonitoringMobilComponent implements OnInit {
    picRoles: MonitoringMobilVM[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter_data: string;
    idpp: any;
    first: number;
    currentInternal: string;
    currentDateTime: string;

    constructor(
        private picRoleServices: MonitoringMobilService,
        // protected confirmationService: ConfirmationService,
        // private parseLinks: JhiParseLinks,
        // private alertService: JhiAlertService,
        // private principal: Principal,
        // private activatedRoute: ActivatedRoute,
        // private router: Router,
        // protected toasterService: ToasterService,
        // private eventManager: JhiEventManager,
        // private paginationUtil: JhiPaginationUtil,
        protected loadingService: LoadingService
        // private paginationConfig: PaginationConfig,
        // protected reportUtilService: ReportUtilService,

    ) {
        // this.currentInternal = principal.getIdInternal();
        this.first = 1;
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data.pagingParams.page;
        //     this.previousPage = data.pagingParams.page;
        //     this.reverse = data.pagingParams.ascending;
        //     this.predicate = data.pagingParams.predicate;
        // });
        // this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
        //     this.activatedRoute.snapshot.params['search'] : '';
        // const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
        //     this.activatedRoute.snapshot.params['page'] : 0;
        // this.page = lastPage > 0 ? lastPage : 1;
        // this.first = (this.page - 1) * this.itemsPerPage;
        this.idpp = '';
        this.currentDateTime = '';
    }

    formatNumber(value: number): string {
        return value < 10 ? '0' + value : value.toString();
    }

    // Update tanggal dan waktu
    updateDateTime() {
        const now = new Date();
        const day = this.formatNumber(now.getDate());
        const month = this.formatNumber(now.getMonth() + 1); // Bulan dimulai dari 0
        const year = now.getFullYear();
        const hours = now.getHours();
        const minutes = this.formatNumber(now.getMinutes());
        const seconds = this.formatNumber(now.getSeconds());

        // Format: dd/mm/yyyy H:ii:ss
        this.currentDateTime = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
    }

    ngOnInit() {
        this.updateDateTime(); // Panggil pertama kali ketika komponen diinisialisasi
        setInterval(() => {
            this.updateDateTime(); // Panggil setiap detik
        }, 1000); // 1000ms = 1 detik
        this.loadAll();
        setInterval(() => {
            console.log('load gan');
            this.loadAll(); // Auto reload every 1 minute
        }, 60000); // 60000ms = 1 minute
        // this.principal.identity().then((account) => {
        //     this.currentAccount = account;
        //     console.log('data akun : ', this.currentAccount);
        // });
        // this.registerChangeInPurchasingItems();
    }

    loadAll() {
        this.picRoleServices.query({
            query: 'idInternal:'
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }

    protected onErrorSearch(error) {
        console.log('eror search' + error)
        // this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        // this.clear();
        this.loadingService.loadingStop();
    }
    // loadPage(page: number) {
    //     if (page !== this.previousPage) {
    //         this.previousPage = page;
    //         this.transition();
    //     }
    // }
    // transition() {
    //     this.router.navigate(['/monitoring-status-mobil'], {
    //         queryParams:
    //         {
    //             page: this.page,
    //             size: this.itemsPerPage,
    //             search: this.currentSearch,
    //             sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //         }
    //     });
    //     this.loadAll();
    // }

    // clear() {
    //     this.page = 1;
    //     this.currentSearch = '';
    //     this.router.navigate(['/monitoring-status-mobil', {
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadAll();
    // }
    // search(query) {
    //     if (!query) {
    //         return this.clear();
    //     }
    //     this.page = 0;
    //     this.currentSearch = query;
    //     this.router.navigate(['/monitoring-status-mobil', {
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadAll();
    // }

    // ngOnDestroy() {
    //     this.eventManager.destroy(this.eventSubscriber);
    // }

    // trackId(index: number, item: MonitoringMobilVM) {
    //     return item.id;
    // }
    // registerChangeInPurchasingItems() {
    //     this.eventSubscriber = this.eventManager.subscribe('MasterProjectListModification', (response) => this.loadAll());
    // }

    // sort() {
    //     const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    //     if (this.predicate !== 'id') {
    //         result.push('id');
    //     }
    //     return result;
    // }

    protected onSuccess(data, headers) {
        // console.log('panjang data: ', data.length);
        // if (data.length === 0) {
        //     this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        // } else if (data !== null) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.picRoles = data;
        // } else {
        //     this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        // }
        this.loadingService.loadingStop();
    }
    private onError(error) {
        // this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    getRowClass(rowData: any, rowIndex: number): string {
        return rowIndex % 2 === 0 ? 'even-row' : 'odd-row';
    }
}
