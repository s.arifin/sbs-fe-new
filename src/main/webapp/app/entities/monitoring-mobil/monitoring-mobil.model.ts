import { BaseEntity } from '../../shared';

export class MonitoringMobilVM implements BaseEntity {
    constructor(
        public id?: Number,
        public cust_name?: any,
        public tujuan?: string,
        public nmEkspedisi?: string,
        public no_mobil?: string,
        public NamaSts?: string,
        public nmLokasi?: string
    ) {
    }
}
