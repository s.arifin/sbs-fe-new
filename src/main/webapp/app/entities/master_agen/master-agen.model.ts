import { BaseEntity } from '../../shared';

export class MasterAgen implements BaseEntity {
    constructor(
        public id?: number,
        public cust_code?: any,
        public cust_name?: any,
        public cust_addr?: string,
        public cust_addr2?: string,
        public cust_addr3?: string,
    ) {
    }
}
