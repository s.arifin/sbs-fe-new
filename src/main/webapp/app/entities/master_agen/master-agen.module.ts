import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { MasterAgen, MasterAgenService, MasterAgenResolvePagingParams, MasterAgenNewComponent, MasterAgenEditComponent } from '.';
import { MasterAgenComponent } from './master-agen.component';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { MasterAgenRoute } from './master-agen.route';

const ENTITY_STATES = [
    ...MasterAgenRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule
    ],
    declarations: [
        MasterAgenComponent,
        MasterAgenNewComponent,
        MasterAgenEditComponent
    ],
    entryComponents: [
        MasterAgenComponent,
        MasterAgenNewComponent,
        MasterAgenEditComponent
    ],
    providers: [
        MasterAgenService,
        MasterAgenResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterAgenModule {}
