import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterAgenService } from './master-agen.service';
import { MasterAgen } from './master-agen.model';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-master-agen-edit',
    templateUrl: './master-agen-edit.component.html'
})
export class MasterAgenEditComponent implements OnInit {

    protected subscription: Subscription;
    picRole: MasterAgen;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;

    routeId: number;
    isSaving: boolean;
    cust_code: string;
    cust_name: string;
    cust_addr: string;
    cust_addr2: string;
    idMasBahan: any;
    readonly: boolean;
    pic: any;
    kdbahan: string;
    kdlama: any;
    satuan: string;
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: MasterAgenService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        protected route: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
    ) {
        this.picRole = new MasterAgen();
        this.routeId = 0;
        this.readonly = true;
    }
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.cust_code = params['id'];
                this.load(this.cust_code);
            }
        });
    }

    load(id) {
        this.masterBahanService.find1(id).subscribe(
            (res: MasterAgen) => {
                this.picRole = res;
                this.picRole.cust_code = res.cust_code;
                this.picRole.cust_name = res.cust_name;
                this.picRole.cust_addr = res.cust_addr;
                this.picRole.cust_addr2 = res.cust_addr2;
                console.log(this.picRole);
            }
        )
    }

    trackId(index: number, item: MasterAgen) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    ver() {
        if (this.picRole.cust_code === null || this.picRole.cust_name === ' ' || this.picRole.cust_addr === undefined) {
            this.toaster.showToaster('info', 'Kolom Kosong', 'Kolom Tidak Boleh Kosong');
        } else {
            const obj = {
                cust_code: this.picRole.cust_code,
                cust_name: this.picRole.cust_name,
                cust_addr: this.picRole.cust_addr,
                cust_addr2: this.picRole.cust_addr2,
            }
            this.loadingService.loadingStart();
            this.masterBahanService.saveEdit(obj)
            .subscribe(
                (res) =>
                this.onSaveSuccess(res),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    simpan() {
        this.cust_code = this.picRole.cust_code;
        this.cust_name = this.picRole.cust_name;
        this.cust_addr = this.picRole.cust_addr;
        this.cust_addr2 = this.picRole.cust_addr2;
        this.ver();
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data) {
        this.eventManager.broadcast({ name: 'MasterBahanListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Agen saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Agen Error', error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }
    previousState() {
            this.router.navigate(['/master-agen', { page: this.paramPage }]);
    }
    back() {
            this.router.navigate(['/master-agen'])
        }
}
