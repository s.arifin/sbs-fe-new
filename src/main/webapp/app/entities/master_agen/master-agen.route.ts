import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { MasterAgenComponent } from './master-agen.component';
import { MasterAgenNewComponent } from './master-agen-new.component';
import { MasterAgenEditComponent } from './master-agen-edit.component';

@Injectable()
export class MasterAgenResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const MasterAgenRoute: Routes = [
    {
        path: 'master-agen',
        component: MasterAgenComponent,
        resolve: {
            'pagingParams': MasterAgenResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masteragen.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-agen-new',
        component: MasterAgenNewComponent,
        resolve: {
            'pagingParams': MasterAgenResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masteragen.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-agen-Edit/:route/:page/:id/edit',
        component: MasterAgenEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masteragen.home.createOrEditLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'master-bahan-Edit',
    //     component: MasterBahanEditComponent,
    //     resolve: {
    //         'pagingParams': MasterBahanResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.picrole.home.createLabel'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
];

export const MasterAgenPopupRoute: Routes = [

    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
