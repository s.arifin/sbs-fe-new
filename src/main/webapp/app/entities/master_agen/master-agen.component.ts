import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingBarHttp } from '@ngx-loading-bar/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { MasterAgen } from './master-agen.model';
import { MasterAgenService } from './master-agen.service';

@Component({
    selector: 'jhi-master-agen',
    templateUrl: './master-agen.component.html'
})
export class MasterAgenComponent implements OnInit, OnDestroy {

    picRoles: MasterAgen[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    first: number;
    currentInternal: string;

    constructor(
        private picRoleServices: MasterAgenService,
        protected confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toasterService: ToasterService,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        protected loadingService: LoadingService,
        private paginationConfig: PaginationConfig
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
       }

    loadAll() {
        this.loadingService.loadingStart();
        this.picRoleServices.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            query: 'cari:' + this.currentSearch}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
        this.loadingService.loadingStop();
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/master-agen'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/master-agen', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/master-agen', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchasingItems();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MasterAgen) {
        return item.id;
    }
    registerChangeInPurchasingItems() {
        this.eventSubscriber = this.eventManager.subscribe('MasterBahanListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        console.log('panjang data: ', data.length);
        if (data.length === 0) {
            console.log('')
            this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        } else if (data !== null) {
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.picRoles = data;
        } else {
            this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        }
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Cancle?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.picRoleServices.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'MasterBahanListModification',
                        content: 'Deleted an Data'
                    });
                });
            }
        });
    }
}
