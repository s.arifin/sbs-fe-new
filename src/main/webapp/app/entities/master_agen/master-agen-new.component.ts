import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterAgenService } from './master-agen.service';
import { MasterAgen } from './master-agen.model';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-master-agen-new',
    templateUrl: './master-agen-new.component.html'
})
export class MasterAgenNewComponent implements OnInit {

    picRole: MasterAgen;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;
    internals: any;

    routeId: number;
    isSaving: boolean;
    cust_code: string;
    cust_name: string;
    cust_addr: string;
    cust_addr2: string;
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: MasterAgenService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.picRole = new MasterAgen();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.internals = this.principal.getIdInternal();
        console.log(this.internals);
    }
    trackId(index: number, item: MasterAgen) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    simpan() {
        if (this.cust_name === undefined || this.cust_addr === undefined) {
            alert('Kolom Tidak Boleh Kosong');
        } else {
            const obj = {
                cust_name: this.cust_name,
                cust_addr: this.cust_addr,
                cust_addr2: this.cust_addr2,
            }
            this.loadingService.loadingStart();
            this.masterBahanService.create(obj)
            .subscribe(
                (res) =>
                this.onSaveSuccess(res),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    private onSaveSuccess(data) {
        this.eventManager.broadcast({ name: 'MasterBahanListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Agen saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('info', 'Gagal', 'Nama Agen Sama');
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }

    previousState() {
            this.router.navigate(['/master-agen', { page: this.paramPage }]);
    }

    back() {
            this.router.navigate(['/master-agen'])
    }
}
