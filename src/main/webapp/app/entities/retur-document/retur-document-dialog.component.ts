import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { ReturDocument } from './retur-document.model';
import { ReturDocumentPopupService } from './retur-document-popup.service';
import { ReturDocumentService } from './retur-document.service';

@Component({
    selector: 'jhi-retur-document-dialog',
    templateUrl: './retur-document-dialog.component.html'
})
export class ReturDocumentDialogComponent implements OnInit {

    returDocument: ReturDocument;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private returDocumentService: ReturDocumentService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    // setFileData(event, entity, field, isImage) {
    //     this.dataUtils.setFileData(event, entity, field, isImage);
    // }
    setFileData(event, entity, field, isImage) {
        if (event && event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            if (isImage && !/^image\//.test(file.type)) {
                return;
            }
            this.dataUtils.toBase64(file, (base64Data) => {
                entity[field] = base64Data;
                entity[`${field}ContentType`] = file.type;
            });
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        console.log('ini data file yg di upload => ', this.returDocument);
        this.isSaving = true;
        if (this.returDocument.id !== undefined) {
            this.subscribeToSaveResponse(
                this.returDocumentService.update(this.returDocument));
        } else {
            this.subscribeToSaveResponse(
                this.returDocumentService.create(this.returDocument));
        }
    }

    private subscribeToSaveResponse(result: Observable<ReturDocument>) {
        result.subscribe((res: ReturDocument) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ReturDocument) {
        this.eventManager.broadcast({ name: 'returDocumentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-retur-document-popup',
    template: ''
})
export class ReturDocumentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returDocumentPopupService: ReturDocumentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.returDocumentPopupService
                    .open(ReturDocumentDialogComponent as Component, params['id']);
            } else {
                this.returDocumentPopupService
                    .open(ReturDocumentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
