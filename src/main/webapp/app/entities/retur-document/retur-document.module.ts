import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    ReturDocumentService,
    ReturDocumentPopupService,
    ReturDocumentComponent,
    ReturDocumentDetailComponent,
    ReturDocumentDialogComponent,
    ReturDocumentPopupComponent,
    ReturDocumentDeletePopupComponent,
    ReturDocumentDeleteDialogComponent,
    returDocumentRoute,
    returDocumentPopupRoute,
    ReturDocumentResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...returDocumentRoute,
    ...returDocumentPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ReturDocumentComponent,
        ReturDocumentDetailComponent,
        ReturDocumentDialogComponent,
        ReturDocumentDeleteDialogComponent,
        ReturDocumentPopupComponent,
        ReturDocumentDeletePopupComponent,
    ],
    entryComponents: [
        ReturDocumentComponent,
        ReturDocumentDialogComponent,
        ReturDocumentPopupComponent,
        ReturDocumentDeleteDialogComponent,
        ReturDocumentDeletePopupComponent,
    ],
    providers: [
        ReturDocumentService,
        ReturDocumentPopupService,
        ReturDocumentResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmReturDocumentModule {}
