import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReturDocumentComponent } from './retur-document.component';
import { ReturDocumentDetailComponent } from './retur-document-detail.component';
import { ReturDocumentPopupComponent } from './retur-document-dialog.component';
import { ReturDocumentDeletePopupComponent } from './retur-document-delete-dialog.component';

@Injectable()
export class ReturDocumentResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const returDocumentRoute: Routes = [
    {
        path: 'retur-document',
        component: ReturDocumentComponent,
        resolve: {
            'pagingParams': ReturDocumentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returDocument.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur-document/:id',
        component: ReturDocumentDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returDocument.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const returDocumentPopupRoute: Routes = [
    {
        path: 'retur-document-new',
        component: ReturDocumentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returDocument.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'retur-document/:id/edit',
        component: ReturDocumentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returDocument.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'retur-document/:id/delete',
        component: ReturDocumentDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returDocument.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
