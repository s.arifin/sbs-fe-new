import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { ReturDocument } from './retur-document.model';
import { ReturDocumentService } from './retur-document.service';

@Component({
    selector: 'jhi-retur-document-detail',
    templateUrl: './retur-document-detail.component.html'
})
export class ReturDocumentDetailComponent implements OnInit, OnDestroy {

    returDocument: ReturDocument;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private returDocumentService: ReturDocumentService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReturDocuments();
    }

    load(id) {
        this.returDocumentService.find(id).subscribe((returDocument) => {
            this.returDocument = returDocument;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReturDocuments() {
        this.eventSubscriber = this.eventManager.subscribe(
            'returDocumentListModification',
            (response) => this.load(this.returDocument.id)
        );
    }
}
