import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ReturDocument } from './retur-document.model';
import { ReturDocumentPopupService } from './retur-document-popup.service';
import { ReturDocumentService } from './retur-document.service';

@Component({
    selector: 'jhi-retur-document-delete-dialog',
    templateUrl: './retur-document-delete-dialog.component.html'
})
export class ReturDocumentDeleteDialogComponent {

    returDocument: ReturDocument;

    constructor(
        private returDocumentService: ReturDocumentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.returDocumentService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'returDocumentListModification',
                content: 'Deleted an returDocument'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-retur-document-delete-popup',
    template: ''
})
export class ReturDocumentDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returDocumentPopupService: ReturDocumentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.returDocumentPopupService
                .open(ReturDocumentDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
