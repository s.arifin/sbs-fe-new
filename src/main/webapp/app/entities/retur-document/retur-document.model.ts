import { BaseEntity } from './../../shared';

export class ReturDocument implements BaseEntity {
    constructor(
        public id?: number,
        public iddocretur?: string,
        public idretur?: string,
        public doctype?: string,
        public docContentType?: string,
        public doc?: any,
        public docname?: string,
    ) {
    }
}
