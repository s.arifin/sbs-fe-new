import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { Province } from './province.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProvinceService {
    protected itemValues: Province[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/provinces';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/provinces';
    private resourceCUrl = process.env.API_C_URL + '/api/provinces';

    constructor(protected http: Http) { }

    create(province: Province): Observable<Province> {
        const copy = this.convert(province);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(province: Province): Observable<Province> {
        const copy = this.convert(province);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<Province> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, province: Province): Observable<Province> {
        const copy = this.convert(province);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, provinces: Province[]): Observable<Province[]> {
        const copy = this.convertList(provinces);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(province: Province): Province {
        if (province === null || province === {}) {
            return {};
        }
        // const copy: Province = Object.assign({}, province);
        const copy: Province = JSON.parse(JSON.stringify(province));
        return copy;
    }

    protected convertList(provinces: Province[]): Province[] {
        const copy: Province[] = provinces;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Province[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
