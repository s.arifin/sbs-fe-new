import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhiLanguageHelper, MpmSharedModule } from '../../shared';
import { Overtime, OvertimeService, OvertimeResolvePagingParams, OverTimeRegComponent, OvertimesItemInOvertimeAsListComponent } from '.';
import { OverTimeComponent } from './overtime.component';
import { DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    DataListModule,
    DataGridModule,
    PaginatorModule,
    ScheduleModule,
    DropdownModule,
    ListboxModule,
    FieldsetModule,
    PanelModule,
    TabViewModule,
    ChartModule,
    DragDropModule,
    SliderModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AccordionModule } from 'primeng/primeng';
import { OvertimeRoute } from './overtime.route';
import { OvertimeEditComponent } from './overtime-edit.component';
import { OverTimeNewComponent } from './overtime-new.component';
import { ApprovalOvertimeComponent } from './approval-ot/approval-overtime.component';
import { TabViewOverTimeComponent } from './approval-ot/tab-view-overtime.component';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { TabsModule } from 'ngx-bootstrap';
import { JhiLanguageService } from 'ng-jhipster/src/language/language.service';
import { OvertimeItemService } from '../overtime-item/overtime-item.service';

const ENTITY_STATES = [
    ...OvertimeRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        AccordionModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ScheduleModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        TabViewModule,
        ChartModule,
        ListboxModule,
        DragDropModule,
        SliderModule,
    ],
    declarations: [
        OverTimeComponent,
        OverTimeRegComponent,
        OverTimeNewComponent,
        OvertimeEditComponent,
        ApprovalOvertimeComponent,
        TabViewOverTimeComponent,
        OvertimesItemInOvertimeAsListComponent,
    ],
    entryComponents: [
        OverTimeComponent,
        OverTimeRegComponent,
        OverTimeNewComponent,
        OvertimeEditComponent,
        ApprovalOvertimeComponent,
        TabViewOverTimeComponent,
        OvertimesItemInOvertimeAsListComponent
    ],
    providers: [
        OvertimeService,
        OvertimeItemService,
        OvertimeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmOverTimesModule {
    constructor(protected languageService: JhiLanguageService, protected languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
    }
}
