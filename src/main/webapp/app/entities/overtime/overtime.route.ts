import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { OvertimeEditComponent } from './overtime-edit.component';
import { OverTimeComponent } from './overtime.component';
import { OverTimeRegComponent } from './overtime-reg.component';
import { OverTimeNewComponent } from './overtime-new.component';
import { ApprovalOvertimeComponent } from './approval-ot/approval-overtime.component';

@Injectable()
export class OvertimeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const OvertimeRoute: Routes = [
    {
        path: 'overtime-req',
        component: OverTimeComponent,
        resolve: {
            'pagingParams': OvertimeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'overtime-reg',
        component: OverTimeRegComponent,
        resolve: {
            'pagingParams': OvertimeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'overtime-new',
        component: OverTimeNewComponent,
        resolve: {
            'pagingParams': OvertimeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'overtime-edit',
        component: OvertimeEditComponent,
        resolve: {
            'pagingParams': OvertimeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'overtime-req/:route/:page/:id/edit',
        component: OvertimeEditComponent,
        resolve: {
            'pagingParams': OvertimeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'overtime/approval',
        component: ApprovalOvertimeComponent,
        resolve: {
            'pagingParams': OvertimeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'annual-leave-reg-edit',
    //     component: AnnualLeaveRegEditComponent,
    //     resolve: {
    //         'pagingParams': AnnualLeaveResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.annual.home.createLabel'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
];

export const  OvertimePopupRoute: Routes = [
    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
