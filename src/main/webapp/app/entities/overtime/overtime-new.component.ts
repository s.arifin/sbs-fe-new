import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { OvertimeService } from './overtime.service';
import { Overtime } from './overtime.model';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PersonalInfo } from '../master_bahan';

@Component({
    selector: 'jhi-overtime-new',
    templateUrl: './overtime-new.component.html'
})
export class OverTimeNewComponent implements OnInit {

    picRole: Overtime;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;

    routeId: number;
    isSaving: boolean;
    inName: any;
    inNIK: any;
    inJabatan: any;
    injblReqCuti: any;
    dtStart: any;
    dtFinish: any;
    inApproved: any;

    inDtStart: any;
    inputSeksie: any;
    inputTlhDilak: any;
    inputMenyetujui: any;
    inputPemTgsOt: any;

    unitChecks: Overtime[];
    unitChecksCopy: Overtime[];

    nik: any;
    name: any;
    jabatan: any;
    planDtstart: any;
    planDtFinish: any;
    planTotal: any;
    realDtstart: any;
    realDtFinish: any;
    realTotal: any;
    overTimeNow: any;
    overTimeSdNow: any;
    overTimePersen: any;
    catatan: any;

    index: number;

    divitions: Array<object> = [
        {label : 'Raw Material', value : 'RM'},
        {label : 'Packaging Material', value : 'PM'},
        {label : 'General Item', value : 'GI'},
        {label : 'Sparepart', value : 'S'},
        {label : 'Maintenace', value : 'M'},
        {label : 'Project', value : 'P'},
        {label : 'Jasa', value : 'J'},
        {label : 'Fixed Asets', value : 'F'},
        {label : 'Other', value : 'Other'},
        {label : 'Alat Non Rutin LPM', value : 'AL'},
        {label : 'Glassware Mikro LPM', value : 'GM'},
        {label : 'Glassware FK LPM', value : 'GF'},
        {label : 'Reagent LPM', value : 'RE'},
        {label : 'media LPM', value : 'ME'},
        {label : 'Tool LPM', value : 'TL'},
    ];

    newPersonal: any;
    newPersonals: any[];
    filteredPersonal: any[];
    public personal: PersonalInfo;
    public Personal: PersonalInfo[];
    public selectedPersonal: any;
    public listPersonal = [{label: 'Please Select or Insert', value: null}];

    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: OvertimeService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.inDtStart = new Date();
        this.personal = new PersonalInfo();
        this.inputSeksie = '';
        this.inputTlhDilak = '';
        this.inputMenyetujui = '';
        this.inputPemTgsOt = '';
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    protected clearForm(): void {
        this.clearDataInput();
        this.index = null;
    }

    filterBarangSingle(event) {
        // console.log('data Tipe barang : ', this.pilihTipe);
        const query = event.query;
        this.masterBahanService.getNewBarang().then((newPersonals) => {
            this.filteredPersonal = this.filterBarang(query, newPersonals);
        });
    }

    filterBarang(query, newPersonals: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newPersonals.length; i++) {
            const newPersonal = newPersonals[i];
            if (newPersonal.nama_lengkap.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal);
            } else if (newPersonal.nik.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.personal.idpersonalinfo = this.newPersonal.idpersonalinfo;
        console.log('idmasbahan : ' + this.newPersonal.idpersonalinfo);
        this.masterBahanService.findBarang(this.newPersonal.idpersonalinfo)
        .subscribe(
            (res: PersonalInfo) => {
                console.log('ini isi personal = ', res.nik);
                this.nik = res.nik;
                this.name = res.nama_lengkap;
                this.jabatan = res.jabatan;
            },
            (res: PersonalInfo) => {
                this.name = null;
                console.log('ini isi bahan eror = ', this.name);
            }
        )
    }

    removeData(index: number): void {
        this.unitChecksCopy = Object.assign([], this.unitChecks);
        this.unitChecksCopy.splice(index, 1);
        this.unitChecks = new Array<Overtime>();
        this.unitChecks = this.unitChecksCopy;
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    clearBack() {
        this.activeModal.dismiss('cancel');
        this.previousState();
    }
    ngOnInit() {
        this.picRole = new Overtime();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.unitChecks = Array<Overtime>();
    }
    trackId(index: number, item: Overtime) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    public selectpic(isSelect?: boolean): void {
        this.picRole.name = this.newpic.login;
    }

    filterpTypeSingle(event) {
        const query = event.query;
        this.masterBahanService.getPtype().then((newPtype) => {
            this.filteredpType = this.filterpType(query, newPtype);
        });
    }

    filterpType(query, newpType: any[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpType.length; i++) {
            const newpTypes = newpType[i];
            if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpTypes);
            }
        }
        return filtered;
    }
    // public selectpType(isSelect?: boolean): void {
    //     this.picRole.type = this.newpType.pType;
    // }
    // simpan() {
    //     const obj = {
    //         name: this.inName,
    //         nik: this.inNIK,
    //         jabatan: this.inJabatan,
    //         jmbReqCuti: this.injblReqCuti,
    //         dtStart: this.dtStart,
    //         dtThru: this.dtFinish,
    //         app: this.inApproved
    //     }
    //     this.loadingService.loadingStart();
    //     this.masterBahanService.createNew(obj)
    //     .subscribe(
    //         (res: ResponseWrapper) =>
    //         this.onSaveSuccess(res, res.headers),
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    // }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Bahan saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Bahan Error', error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }
    previousState() {
            this.router.navigate(['/overtime-req', { page: this.paramPage }]);
    }
    back() {
            this.router.navigate(['/overtime-req'])
    }

    protected resetMe() {
        this.nik = null;
    }

    save() {
        // console.log('tipe : ' + this.inputTipe.label);
        const obj = {
            dtStart: this.inDtStart,
            div: this.principal.getIdInternal(),
            seksie: this.inputSeksie,
            tlhDilakApp: this.inputTlhDilak,
            MengtahuiApp: this.inputMenyetujui,
            PemTgsOTAPP: this.inputPemTgsOt,
            data: this.unitChecks
        }
        if (this.inDtStart == null) {
            this.toaster.showToaster('info', 'Save', 'Please Select Date Request');
        } else {
            this.loadingService.loadingStart();
            this.masterBahanService.createNew(obj).subscribe(
                (res: ResponseWrapper) =>
                this.onSaveSuccess(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
        }
    }
    addDataBtnDisable(): boolean {
        if ((this.nik !== '' &&
            this.nik !== undefined &&
            this.nik != null)) {
            return false;
        }
    }

    validateAdd() {
        console.log('isi unitcek : ', this.unitChecks)
        // const totalStock = this.inputSaldo + this.saldoPP + this.saldoPO + this.inputQty;
        this.addData();
    }

    addData(): void {
        const data = new Overtime();
        data.nik = this.nik;
        data.name = this.name;
        data.jabatan = this.jabatan;
        data.planDtstart = this.planDtstart;
        // data.satuan = this.inputSatuan;
        data.planDtFinish = this.planDtFinish;
        data.planTotal = this.planTotal;
        data.realDtstart = this.realDtstart;
        data.realDtFinish = this.realDtFinish;
        data.realTotal = this.realTotal;
        data.overTimeNow = this.overTimeNow;
        data.overTimeSdNow = this.overTimeSdNow;
        data.overTimePersen = this.overTimePersen;
        data.catatan = this.catatan;

        this.unitChecks = [...this.unitChecks, data];
        this.clearDataInput();

        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    clearDataInput(): void {
        this.nik = null;
        this.name = null;
        this.jabatan = null;
        this.planDtstart = null;
        this.newPersonal = null;
        // data.satuan = this.inputSatuan;
        this.planDtFinish = null;
        this.planTotal = null;
        this.realDtstart = null;
        this.realDtFinish = null;
        this.realTotal = null;
        this.overTimeNow = null;
        this.overTimeSdNow = null;
        this.overTimePersen = null;
        this.catatan = null;
    }

}
