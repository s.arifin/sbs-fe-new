import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, Headers } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { Overtime } from './overtime.model';
import { PersonalInfo } from '../master_bahan';

@Injectable()
export class OvertimeService {
    protected itemValues: Overtime[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/overtime';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/overtime';
    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, protected dateUtils: JhiDateUtils) { }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    getNewBarang() {
        return this.http.get(this.resourceUrl + '/getalldropdown').toPromise()
            .then((res) => <any[]> res.json())
            .then((data) => data);
    }

    findBarang(id: any): Observable<PersonalInfo> {
        return this.http.get(`${this.resourceUrl}/personal/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createNew(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    // create(unitChecks: any): Observable<ResponseWrapper> {
    //     // const copy = this.convert(purchaseOrder);
    //     const copy = JSON.stringify(unitChecks);
    //     const options: BaseRequestOptions = new BaseRequestOptions();
    //     options.headers = new Headers;
    //     options.headers.append('Content-Type', 'application/json');
    //     return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
    //         return res.json();
    //     });
    // }
    public getDataByApprovalStatusAndRequirement(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/byStatus', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
            // return res.json();
        // });
    }
    changeStatus(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeStatus'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    public getDataByStatus(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-status', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
            // return res.json();
        // });
    }
    createHrd(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/psu-entry`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    craete(picRole: Overtime): Observable<Overtime> {
        const copy = this.convert(picRole);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // update(overtime: Overtime): Observable<Overtime> {
    //     const copy = this.convert(purchaseOrder);
    //     return this.http.post(this.resourceUrl + '/update', copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         return this.convertItemFromServer(jsonResponse);
    //     });
    // }
    update(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/saveEdit`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    updateItem(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/saveEditItem`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/findItem', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findItem(id: any): Observable<Overtime> {
        return this.http.get(`${this.resourceUrl}/${id}/findItem`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Overtime> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find1(id: any): Observable<ResponseWrapper> {
        const options = createRequestOption(id);
        return this.http.get(`${this.resourceUrl}/${id}/find`, options).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertResponse(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '-req', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryReg(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '-reg', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}/delete`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }

    private convert(picRole: Overtime): Overtime {
        const copy: Overtime = Object.assign({}, picRole);
        return copy;
    }

    getPtype() {
        return this.http.get(this.resourceUrl + '/tipe-produk').toPromise()
            .then((res) => <any[]> res.json())
            .then((data) => data);
    }

}
