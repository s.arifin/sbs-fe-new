import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Overtime } from './overtime.model';
import { OvertimeService } from './overtime.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import * as ConfigConstants from '../../shared/constants/config.constants';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
@Component({
    selector: 'jhi-overtime-edit',
    templateUrl: './overtime-edit.component.html'
})
export class OvertimeEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    purchaseOrder: Overtime;
    isSaving: boolean;
    idPurchaseOrder: any;
    paramPage: number;
    idovertime: any;
    routeId: number;
    dtOrder: any;
    dtNecessity: any;
    inputTipe: any;
    inputKet: any;
    internal: any;
    idlocal: any;
    isloc: any;
    curAccount: any;
    currentDiusul: any;
    currentMGR: any;
    currentKBG: any;

    tipe: Array<object> = [
        {label : 'Spare Parts', value : 'SP'},
        {label : 'Maintenace', value : 'MT'},
        {label : 'Service', value : 'SR'},
        {label : 'Fixed Asets', value : 'FA'},
        {label : 'Project', value : 'PR'}
    ];

    divitions: Array<object> = [
        {label : 'PPIC', value : 'PPIC'},
        {label : 'PROD', value : 'PROD'},
        {label : 'LPM', value : 'LPM'},
        {label : 'PURCHASING', value : 'PURCHASING'},
        {label : 'RND', value : 'RND'},
        {label : 'TEKNIK', value : 'TEKNIK'},
        {label : 'MARKETING', value : 'MARKETING'},
        {label : 'FNA', value : 'FNA'},
        {label : 'PSU', value : 'PSU'},
        {label : 'IT', value : 'IT'}
    ];

    sifats: Array<object> = [
        {label : 'Biasa', value : 'Biasa'},
        {label : 'Segera', value : 'Segera'},
        {label : 'Penting', value : 'Penting'}

    ];

    needs: Array<object> = [
        {label : 'Rutin', value : 'Rutin'},
        {label : 'Non Rutin', value : 'Non Rutin'}

    ];
    listMGR = [{}];
    listUSL = [{}];
    listAPP = [{}];
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected purchaseOrderService: OvertimeService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected dataUtils: JhiDataUtils,
        protected loadingService: LoadingService,
    ) {
        this.purchaseOrder = new Overtime();
        this.routeId = 0;
        this.readonly = true;
        this.inputTipe = '';
        this.inputKet = '';
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.contents = null;
        this.contentTypes = null;
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        // this.dataUtils.clearInputImage(this.joborders, this.elementRef, field, fieldContentType, idInput);
        // this.purchaseOrder.content = null;
        // this.purchaseOrder.contentContentType = null;
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], {'type': fieldContentType});

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
    ngOnInit() {
        this.internal = this.principal.getIdInternal();
        this.curAccount = this.principal.getUserLogin().toUpperCase();
        console.log('userLogin :', this.curAccount);
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.idlocal = account.loc;
            console.log('internal : ', this.idlocal)
            if (account.loc === 'pabrik') {
                this.inputAppPabrik = 'SWY';
            }
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPurchaseOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe((overtime) => {
            this.purchaseOrder = overtime;
            this.dtOrder = new Date(this.purchaseOrder.dtStart);
            this.idovertime = overtime.idOvertime;
            // this.dtNecessity = new Date(this.purchaseOrder.dtNecessity);
            // this.inputTipe = this.purchaseOrder.tipe;
            // this.inputKet = this.purchaseOrder.ket;
            // this.currentDiusul = this.purchaseOrder.diusulkan;
            // this.currentMGR = this.purchaseOrder.appDept;
            // this.currentKBG = this.purchaseOrder.appPabrik;
            // console.log('diusul = ', this.currentDiusul);
            // console.log('MGR = ', this.currentMGR);
            // console.log('KBG = ', this.currentKBG);
            // console.log('Creatby = ', this.purchaseOrder.createdBy);
        });
    }

    previousState() {
        this.router.navigate(['/overtime-req', { page: this.paramPage }]);
}
    // || this.curAccount !==  || this.curAccount !== this.currentMGR || this.curAccount !==
    masukSave() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        this.purchaseOrder.dtStart = this.dtOrder;
        if (this.purchaseOrder.idOvertime !== undefined) {
            this.purchaseOrderService.update(this.purchaseOrder).subscribe(
                (res: ResponseWrapper) =>
                this.onSaveSuccess(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
        }
        // else {
        //     this.subscribeToSaveResponse(
        //         this.purchaseOrderService.craete(this.purchaseOrder));
        // }
    }
    save() {
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.masukSave()
        } else if ( this.curAccount === this.purchaseOrder.tlhDilakApp.toUpperCase()) {
            this.masukSave()
        } else if ( this.curAccount === this.purchaseOrder.MengtahuiApp.toUpperCase()) {
            this.masukSave()
        } else if ( this.curAccount === this.purchaseOrder.PemTgsOTAPP.toUpperCase()) {
            this.masukSave()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat dan Atasan.Terima Kasih.');
        }
    }
    // protected subscribeToSaveResponse(result: Observable<Overtime>) {
    //     result.subscribe((res: Overtime) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    // }

    // protected subscribeToSaveResponse(result: Observable<Overtime>) {
    //     result.subscribe((res: Overtime) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    // }

    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        // this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Bahan Error', error.message);
        // alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.')
        this.alertService.error(error.message, null, null);
    }

    // protected onSaveSuccess(result: Overtime) {
    //     console.log("masuk berhasil")
    //     // this.print(result.idPurchaseOrder);
    //     this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
    //     this.toaster.showToaster('info', 'Save', ' saved !');
    //     this.isSaving = false;
    //     this.previousState();
    //     this.loadingService.loadingStop();
    // }

    print(id: any) {
        const filter_data = 'idOvertime:' + id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/lembur_rpt/pdf', {filterData: filter_data});
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onError(error);
    }

    // protected onError(error) {
    //     this.toaster.showToaster('warning', 'overtime Changed', error.message);
    //     this.alertService.error(error.message, null, null);
    // }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
