import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { Time } from 'ngx-bootstrap/timepicker/timepicker.models';
import { BaseEntity } from '../../shared';

export class Overtime implements BaseEntity {
    constructor(
        public id?: number,
        public idOvertime?: any,
        public idOvertimeItem?: any,
        public nodoc?: any,
        public nik?: any,
        public name?: string,
        public div?: string,
        public seksie?: string,
        public dtStart?: Date,
        public jabatan?: any,
        public kontrol_satpam?: any,
        public planDtstart?: Date,
        public planDtFinish?: Date,
        public planTotal?: any,
        public realDtstart?: Date,
        public realDtFinish?: Date,
        public realTotal?: any,
        public overTimeNow?: any,
        public overTimeSdNow?: any,
        public overTimePersen?: any,
        public tlhDilakApp?: any,
        public MengtahuiApp?: any,
        public PemTgsOTAPP?: any,
        public dttlhDilakApp?: any,
        public dtMengtahuiApp?: any,
        public dtPemTgsOTAPP?: any,
        public catatan?: any,
        public statustype?: any,
        public statDesc?: any,
        public TotalData?: any,
        public createdby?: any,
        public item?: OvertimeItem,
    ) {
        this.item = new OvertimeItem();
    }
}

export class OvertimeItem implements BaseEntity {
    constructor(
        public id?: number,
    ) {

    }
}
