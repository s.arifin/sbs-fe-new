import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { OvertimeService } from './overtime.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { PurchaseOrderService, PurchaseOrder, CustomSupplyer, MasterBahan, MasterBarang } from '../purchase-order';
import { CommonUtilService } from '../../shared';
import * as _ from 'lodash';
import { InternalService } from '../internal';
import { Overtime, OvertimeItem } from './overtime.model';
import { SpawnSyncOptions } from 'child_process';

@Component({
    selector: 'jhi-overtimes-item-in-overtime-as-list',
    templateUrl: './overtimes-item-in-overtime-as-list.component.html'
})
export class OvertimesItemInOvertimeAsListComponent implements OnInit, OnDestroy, OnChanges {

    dtOrder: Date;
    @Input() idPurchaseOrder: any;
    @Input() peruntukan: any;
    @Input() idPurOrdItem: any;
    @Input() idstatus: any;
    @Input() usrLogin: any;
    purchaseOrderItem: Overtime;
    isSaving: boolean;
    itemType: number;
    puechaseOrders: Overtime[];
    currentAccount: any;
    purchaseOrderItems: Overtime[];
    purchaseOrder: Overtime;
    purchaseOrderItemEdit: Overtime;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    newModal: boolean;
    newModalEdit: boolean;
    routeData: any;
    currentSearch: string;
    isOpen: Boolean = false;
    index: number;
    loc: string;

    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputOfferNumber: any;
    inputSupplyerName: string;
    inputSupplyerNameEdit: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: any;
    inputRemainingStock: any;
    inputRemainingPo: any;
    unitChecks: Overtime[];
    unitChecksCopy: Overtime[];
    hasilSave: Overtime;
    supplier: CustomSupplyer;
    Supplyer: CustomSupplyer[];
    selectedSupplier: any;
    listSupplier = [{label: 'Please Select or Insert', value: null}];

    satHarga: Array<object> = [
        {label : 'RUPIAH - IDR', value : 'IDR'},
        {label : 'DOLAR AS - USD', value : 'USD'},
        {label : 'EURO - EUR', value : 'EUR'},
        {label : 'DOLAR SINGAPURA - SGD', value : 'SGD'},
        {label : 'DOLAR AUSTRALIA - AUD', value : 'AUD'},
        {label : 'YEN - JPY', value : 'JPY'},
        {label : 'POUND - GBP', value : 'GBP'},
    ];

    inputSatHarga: any;
    public barang: MasterBahan;
    public Barang: MasterBahan[];
    public selectedBarang: any;
    public listBarang = [{label: 'Please Select or Insert', value: null}];

    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{label: 'Please Select or Insert', value: null}];

    newSuplier: any;
    newSuplierEdit: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];

    newKdBarang: any;
    newKdBarangs: any[];
    filteredKdBarang: any[];
    pilihKdBarang: any;
    internal: any;
    pilihTipe: any;
    newProj: any;
    newProjs: any[];
    listProj = [{}];
    picBarang: any;
    inputSatuan: any;
    created: any;
    currentDiusul: any;
    currentMGR: string;
    currentKBG: string;
    curAccount: string;
    inputDtRequest: any;
    noUrut: any;

    dtSentEdit: any;
    dtOfferEdit: any;
    nik: any;
    name: any;
    jabatan: any;
    planDtstart: any;
    planDtFinish: any;
    planTotal: any;
    realDtstart: any;
    realDtFinish: any;
    realTotal: any;
    overTimeNow: any;
    overTimeSdNow: any;
    overTimePersen: any;
    catatan: any;

    constructor(
        protected purchaseOrderService: OvertimeService,
        protected purchaseOrderItemService: OvertimeService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected paginationUtil: JhiPaginationUtil,
        protected modalService: NgbModal,
        protected paginationConfig: PaginationConfig,
        protected loadingService: LoadingService,

        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected toaster: ToasterService,
        protected commontUtilService: CommonUtilService,
        protected internalService: InternalService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'noUrut';
        this.reverse = 'asc';
        this.purchaseOrder = new Overtime();
        this.newModal = false;
        this.newModalEdit = false;
        this.purchaseOrderItem = new Overtime();
        this.purchaseOrderItemEdit = new Overtime();
        // this.dtSentEdit = new Date;
        // this.dtOfferEdit = new Date;
        this.purchaseOrder = new Overtime();
        this.dtOrder = new Date();
        this.barang = new MasterBahan();
        this.isSelectSupplier = false;
        this.kdbarang = new MasterBahan();
        this.inputSatuan = null;
        this.purchaseOrder.createdby = null;
        this.inputDtRequest = new Date;
        this.noUrut = 0;
    }
    protected resetMe() {
        this.selectedBarang = null;
        this.curAccount = null;
    }

    onSuccessBarang(data, headers) {
        // this.purchaseOrderItem.codeProduk = data.kdBahan;
    }

    onErrorBarang(error) {
        // this.purchaseOrderItem.codeProduk = null;
    }

    loadAll1() {
        this.internal = this.principal.getIdInternal();
        console.log('oninit internal = ', this.curAccount);
        this.purchaseOrderItem = new Overtime();
        this.loadingService.loadingStart();
        this.purchaseOrderItemService.queryFilterBy({
            query: 'idInternal:' + this.idPurchaseOrder,
            idPurOrdItem: this.idPurOrdItem,
            page: 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<PurchaseOrder>();
    }

    loadAll() {
        this.internal = this.principal.getIdInternal();
        console.log('oninit internal = ', this.curAccount);
        this.purchaseOrderItem = new Overtime();
        this.loadingService.loadingStart();
        this.purchaseOrderItemService.queryFilterBy({
            query: 'idInternal:' + this.idPurchaseOrder,
            idPurOrdItem: this.idPurOrdItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<PurchaseOrder>();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/purchase-order-item'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/purchase-order-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.activeModal.dismiss('cancel');
        this.loadAll();
    }
    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);

        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (overtime) => {
            this.purchaseOrder = overtime;
            this.created = overtime.createdby.toUpperCase();
            this.currentDiusul = overtime.tlhDilakApp.toUpperCase();
            this.currentKBG = overtime.MengtahuiApp.toUpperCase();
            this.currentMGR = overtime.PemTgsOTAPP.toUpperCase();
            // console.log('this.purchaseOrder', purchaseOrder);
            // console.log('curAccount: ', this.usrLogin);
            // console.log('created: ', this.created);
            // console.log('diusul: ', this.currentDiusul);
            // console.log('curMGR: ', this.currentMGR);
            // console.log('curKBG: ', this.currentKBG);
            // this.onSuccessDataPP();
        });

        this.registerChangeInBillingItems();
        this.isSaving = false;
        this.loadAll();
    }
    masukAddNewData(): void {
        const data = new PurchaseOrder();
        this.newModal = true;
        data.codeProduk = this.inputCodeName;
        console.log('codeProduk' + data.codeProduk);
        if (this.inputCodeName == null) {
            data.produkName = this.newBarang;
        } else {
            data.produkName = this.inputProdukName;
        }
        if (this.isSelectSupplier === true) {
            data.supplyerName = this.inputSupplyerName;
            console.log('ini suplier true' + data.supplyerName);
        } else {
            data.supplyerName = this.newSuplier;
            console.log('ini suplier' + data.supplyerName);
        }
        data.offerNumber = this.inputOfferNumber;
        data.qty = this.inputQty;
        data.dtOffer = this.inputDtOffer;
        data.supplyerPrice = this.inputSupplyerPrice;
        data.necessity = this.inputNecessity;
        data.dtSent = this.inputDtRequest;
        data.dtNecessity = this.inputDtNecessity;
        data.remainingStock = this.inputRemainingStock;
        data.remainingPo = this.inputRemainingPo;
        data.mataUang = this.inputSatHarga;
        this.unitChecks = [...this.unitChecks, data];
        this.clearDataInput();
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    valMasukAddNewData() {
        if (this.idstatus <= 10) {
            this.masukAddNewData()
        } else if (this.idstatus === 11 && this.loc === 'pusat') {
            this.masukAddNewData()
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Di Approved. Terima Kasih.');
        }
    }

    onSuccessDataPP() {
        if (this.curAccount.toUpperCase() === this.purchaseOrder.createdby.toUpperCase()) {
            this.valMasukAddNewData()
        } else if ( this.curAccount === this.currentDiusul) {
            this.valMasukAddNewData()
        } else if ( this.curAccount === this.currentMGR ) {
            this.valMasukAddNewData()
        } else if ( this.curAccount === this.currentKBG) {
            this.valMasukAddNewData()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    addNewData(): void {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);
        this.onSuccessDataPP();
    }

    editItem(idItem: any): void {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);
        this.onSuccessDataItemPP(idItem);
    }

    onSuccessDataItemPP(idItem: any) {
        if (this.curAccount.toUpperCase() === this.purchaseOrder.createdby.toUpperCase()) {
            this.valMasukAddNewDatItem(idItem)
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Atasan Yang dituju. Terima Kasih.');
        }
    }

    valMasukAddNewDatItem(idItem: any) {
        if (this.idstatus <= 10) {
            this.masukAddNewDataItem(idItem)
        } else if (this.idstatus === 11 && this.loc === 'pusat') {
            this.masukAddNewDataItem(idItem)
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Di Approved. Terima Kasih.');
        }
    }

    masukAddNewDataItem(idItem: any): void {
        this.purchaseOrderService.findItem(idItem).subscribe(
            (purchaseOrderItem) => {
            this.newModalEdit = true;
            this.purchaseOrderItemEdit = purchaseOrderItem;
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idPurchaseOrder']) {
            this.loadAll();
        }
        if (changes['idPurOrdItem']) {
            this.loadAll();
        }
        // if (changes['idInventoryItem']) {
        //     this.loadAll();
        // }
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Overtime) {
        return item.idOvertime;
    }
    // protected registerChangeInBillingItems(): void {
    //     this.eventSubscriber = this.billingItemService.values.subscribe(
    //         (res) => {
    //             this.billingItems = res;
    //         }
    //     )
    // }
    registerChangeInBillingItems() {
        this.eventSubscriber = this.eventManager.subscribe('OvertimeItemListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'noUrut') {
            result.push('noUrut');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = data.totalItems / this.itemsPerPage;
        // this.totalItems = data[0].Totaldata;
        // this.queryCount = this.totalItems;
        this.purchaseOrderItems = data;
        this.loadingService.loadingStop();
        console.log('ini data internal', data);
    }

    protected onError(error: any) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
        // this.loadingService.loadingStop();
        // this.toaster.showToaster('warning', 'billingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        // this.purchaseOrderItemService.executeProcess(item).subscribe(
        //    (value) => console.log('this: ', value),
        //    (err) => console.log(err),
        //    () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        // );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        // if (event.data.id !== undefined) {
        //     this.purchaseOrderItemService.update(event.data)
        //         .subscribe((res: PurchaseOrderItem) =>
        //             this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        // } else {
        //     this.purchaseOrderItemService.create(event.data)
        //         .subscribe((res: PurchaseOrderItem) =>
        //             this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        // }
    }

    protected onRowDataSaveSuccess(result: Overtime) {
        this.toasterService.showToaster('info', 'PurchaseOrderItem Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    clearDataInput(): void {
        this.nik = null;
        this.name = null;
        this.jabatan = null;
        this.planDtstart = null;
        // data.satuan = this.inputSatuan;
        this.planDtFinish = null;
        this.planTotal = null;
        this.realDtstart = null;
        this.realDtFinish = null;
        this.realTotal = null;
        this.overTimeNow = null;
        this.overTimeSdNow = null;
        this.overTimePersen = null;
        this.catatan = null;
    }

    masukSave() {
        this.purchaseOrderItem.idOvertime = this.idPurchaseOrder;
        this.purchaseOrderItem.nik = this.nik;
        this.purchaseOrderItem.name = this.name;
        this.purchaseOrderItem.jabatan = this.jabatan;
        this.purchaseOrderItem.planDtstart = this.planDtstart;
        this.purchaseOrderItem.planDtFinish = this.planDtFinish;
        this.purchaseOrderItem.planTotal = this.planTotal;
        this.purchaseOrderItem.realDtstart = this.realDtstart;
        this.purchaseOrderItem.realDtFinish = this.realDtFinish;
        this.purchaseOrderItem.realTotal = this.realTotal;
        this.purchaseOrderItem.overTimeNow = this.overTimeNow;
        this.purchaseOrderItem.overTimeSdNow = this.overTimeSdNow;
        this.purchaseOrderItem.overTimePersen = this.overTimePersen;
        this.purchaseOrderItem.catatan = this.catatan;
        this.isSaving = true;
        if (this.purchaseOrderItem.nik !== undefined && this.purchaseOrderItem.name !== undefined) {
            this.newModal = false;
            this.loadingService.loadingStart();
            this.purchaseOrderService.updateItem(this.purchaseOrderItem).subscribe(
                (res: ResponseWrapper) =>
                this.onSaveSuccessItem(res, res.headers),
                (res: ResponseWrapper) => this.onErrorItem(res.json));
        } else {
            this.toaster.showToaster('warning', 'Changed', 'Data Belum Lengkap.');
        }
    }

    save() {
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.masukSave()
        } else if ( this.curAccount === this.currentDiusul) {
            this.masukSave()
        } else if ( this.curAccount === this.currentMGR ) {
            this.masukSave()
        } else if ( this.curAccount === this.currentKBG) {
            this.masukSave()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    private onSaveSuccessItem(data, headers) {
        this.eventManager.broadcast({ name: 'OvertimeItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'saved !');
        this.isSaving = false;
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onErrorItem(error) {
        this.newModal = true;
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Error', error.message);
        this.alertService.error(error.message, null, null);
    }

    protected subscribeToSaveResponse(result: Observable<Overtime>) {
        result.subscribe((res: Overtime) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Overtime) {
        this.loadingService.loadingStop();
        this.eventManager.broadcast({ name: 'OvertimeItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Item saved !');
        this.isSaving = false;
        // this.purchaseOrder = new PurchaseOrderItem();
        this.activeModal.dismiss(result);
        this.isSelectSupplier = false;
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onErrorr(error);
    }

    protected onErrorr(error) {
          alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.');
        this.toaster.showToaster('warning', 'PIC Pembelian Barang Berbeda, Pilih Barang Yang Berdasarkan Satu PIC', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBillingById(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }
    masukDelete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.purchaseOrderItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'OvertimeItemListModification',
                        content: 'Deleted'
                    });
                });
            }
        });
    }
    valMasukDelete(id: any) {
        if (this.idstatus <= 10) {
            this.masukDelete(id)
        } else if (this.idstatus === 11 && this.loc === 'pusat') {
            this.masukDelete(id)
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Di Approved. Terima Kasih.');
        }
    }
    delete(id: any) {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
        });
        console.log('log account: ', this.curAccount);
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.valMasukDelete(id)
        } else if ( this.curAccount === this.purchaseOrder.tlhDilakApp) {
            this.valMasukDelete(id)
        } else if ( this.curAccount === this.purchaseOrder.MengtahuiApp) {
            this.valMasukDelete(id)
        } else if ( this.curAccount === this.purchaseOrder.PemTgsOTAPP) {
            this.valMasukDelete(id)
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    saveEdit() {
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.masukSaveEdit()
        } else if ( this.curAccount === this.currentDiusul) {
            this.masukSaveEdit()
        } else if ( this.curAccount === this.currentMGR ) {
            this.masukSaveEdit()
        } else if ( this.curAccount === this.currentKBG) {
            this.masukSaveEdit()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
    }
    masukSaveEdit() {
        console.log('ini data supplier = ', this.inputSupplyerNameEdit);
        // this.purchaseOrderItemEdit.supplyerName = this.inputSupplyerNameEdit;
        // this.purchaseOrderItemEdit.dtSent = this.dtSentEdit;
        // this.purchaseOrderItemEdit.dtOffer = this.dtOfferEdit;

        // this.isSaving = true;
        // if (this.purchaseOrderItemEdit.qty !== undefined) {
        //     this.newModalEdit = false;
        //     this.loadingService.loadingStart();
        //     if (this.purchaseOrderItemEdit.idPurOrdItem !== undefined) {
        //         this.subscribeToSaveResponse(
        //             this.purchaseOrderItemService.saveEdit(this.purchaseOrderItemEdit));
        //     } else {
        //         this.subscribeToSaveResponse(
        //             this.purchaseOrderItemService.saveEdit(this.purchaseOrderItemEdit));
        //     }
        // } else {
        //     this.toaster.showToaster('warning', 'Purchase Request Changed', 'Data Belum Lengkap.');
        // }
    }
    public selectSupplierEdit(isSelect?: boolean): void {
        this.inputSupplyerNameEdit = this.newSuplierEdit.nmSupplier;
        console.log('select supplier edit ' + this.inputSupplyerNameEdit);
    }
}
