import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TandaTerimaSKG, TandaTerimaDokumenSKG, TandaTerimaSuratSKG, TandaTerimaKlaimSKG, DetailTandaTerimaKlaimSKG } from './tanda-terima-skg.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { DetailTandaTerimaSKG } from '../detail-tanda-terima-skg';

@Injectable()
export class TandaTerimaSKGService {
    protected itemValues: DetailTandaTerimaSKG[] = new Array<DetailTandaTerimaSKG>();
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/tanda-terimas-skg';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/tanda-terimas-skg';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(tandaTerima: TandaTerimaSKG): Observable<TandaTerimaSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(tandaTerima: TandaTerimaSKG): Observable<TandaTerimaSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<TandaTerimaSKG> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findKlaim(id: number): Observable<TandaTerimaKlaimSKG> {
        return this.http.get(`${this.resourceUrl}/klaim/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryKlaim(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/klaim', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterKlaim(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterKlaim', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/delete/${id}`);
    }
    deleteItem(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/deleteItem/${id}`);
    }
    deleteKlaim(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/deleteKlaim/${id}`);
    }
    deleteRow(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/deleteRow/${id}`);
    }
    delete1(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/surat/delete/${id}`);
    }
    delete2(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/document/delete/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        // entity.dtreceipt = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtreceipt);
        // entity.dtdue = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtdue);
        // entity.dtpurc = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtpurc);
        // entity.dtmkt = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtmkt);
        // entity.dtacc = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtacc);
        // entity.dtfin = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtfin);
        // entity.dtgiro = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtgiro);
    }

    private convert(tandaTerima: TandaTerimaSKG): TandaTerimaSKG {
        const copy: TandaTerimaSKG = Object.assign({}, tandaTerima);

        // copy.dtreceipt = this.dateUtils.toDate(tandaTerima.dtreceipt);

        // copy.dtdue = this.dateUtils.toDate(tandaTerima.dtdue);

        // copy.dtpurc = this.dateUtils.toDate(tandaTerima.dtpurc);

        // copy.dtmkt = this.dateUtils.toDate(tandaTerima.dtmkt);

        // copy.dtacc = this.dateUtils.toDate(tandaTerima.dtacc);

        // copy.dtfin = this.dateUtils.toDate(tandaTerima.dtfin);

        // copy.dtgiro = this.dateUtils.toDate(tandaTerima.dtgiro);
        return copy;
    }
    pushItems(data: DetailTandaTerimaSKG[]) {
        this.itemValues = new Array<DetailTandaTerimaSKG>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
    queryLOV(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov', options)
            .map((res: Response) => this.convertResponse(res));
    }
    cekNoTT(nott: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/cek/${nott}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    createSurat(tandaTerima: TandaTerimaSuratSKG): Observable<TandaTerimaSuratSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/surat', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createTTK(tandaTerima: TandaTerimaKlaimSKG): Observable<TandaTerimaKlaimSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/createTTK', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createDocument(tandaTerima: TandaTerimaDokumenSKG): Observable<TandaTerimaDokumenSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/document', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateDocument(tandaTerima: TandaTerimaDokumenSKG): Observable<TandaTerimaDokumenSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/document/edit', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateSurat(tandaTerima: TandaTerimaSuratSKG): Observable<TandaTerimaSuratSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl + '/surat', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateIsprint(tandaTerima: TandaTerimaDokumenSKG): Observable<TandaTerimaDokumenSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl + '/isprint', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryDokumen(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/document', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryFilterDokumen(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/document/filter/', options)
            .map((res: Response) => this.convertResponse(res));
    }
    public querySurat(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/surat', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySuratFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/surat/filter/', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findDocument(id: number): Observable<TandaTerimaDokumenSKG> {
        return this.http.get(this.resourceUrl + '/document/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getAutorityUser(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/document/getUser', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAutority(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/document/getallautority', options)
            .map((res: Response) => this.convertResponse(res));
    }

    GetDataTIKI(id: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/print/' + id).map((res: Response) => this.convertResponse(res));
    }

    findSurat(id: number): Observable<TandaTerimaSuratSKG> {
        return this.http.get(this.resourceUrl + '/surat/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateBayar(tandaTerima: TandaTerimaSKG): Observable<TandaTerimaSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl + '/bayar', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryMKT(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/mkt', options)
            .map((res: Response) => this.convertResponse(res))
    }
    changeStatus(tandaTerima: TandaTerimaSKG, idstatus: number): Observable<TandaTerimaSKG> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/set-status/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    cek(id: any): Observable<TandaTerimaSKG> {
        return this.http.get(this.resourceUrl + '/cek-edit/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    getinvSPL(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-inv-spl', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-fix-rebate', options)
            .map((res: Response) => this.convertResponse(res));
    }
    cekinv(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/cek-inv-spl', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getDistributorALL() {
        return this.http.get(this.resourceUrl + '/getallsupplierSKG/all').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    // cekTTK(kdprogramX: string) {
    //     return this.http.get(this.resourceUrl + '/cekTTK/' + kdprogramX).toPromise()
    //         .then((res) => <any[]>res.json())
    //         .then((data) => data);
    // }

    cekTTK(kdprogramX: string): Observable<DetailTandaTerimaKlaimSKG> {
        return this.http.get(`${this.resourceUrl}/cekTTK/${kdprogramX}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    cekData(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/document/getStatus', options)
            .map((res: Response) => this.convertResponse(res));
    }

    cekTIKI(kdprogramX: string): Observable<TandaTerimaDokumenSKG> {
        return this.http.get(`${this.resourceUrl}/document/cekTIKI/${kdprogramX}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    setStatusKirim(purchasing: TandaTerimaDokumenSKG[]): Observable<TandaTerimaDokumenSKG[]> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/document/set-mail-tiki', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    setStatusSelesai(purchasing: TandaTerimaSuratSKG[]): Observable<TandaTerimaSuratSKG[]> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/surat/set-surat', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getDistributor() {
        return this.http.get(this.resourceUrl + '/getallagen/all').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getDistributorSBS() {
        return this.http.get(this.resourceUrl + '/getallagen/allSBS').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    print(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    isCek(purchasing: TandaTerimaDokumenSKG[]): Observable<TandaTerimaDokumenSKG[]> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/set-cek', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convertPurchasings(purchasing: TandaTerimaDokumenSKG[]): TandaTerimaDokumenSKG[] {
        const copy: TandaTerimaDokumenSKG[] = Object.assign([], purchasing);
        return copy;
    }

    getNewSupplierDIV(div: string) {
        return this.http.get(this.resourceUrl + '/getallsupplier/div/' + div).toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    changeStatusTT(purchasing: TandaTerimaDokumenSKG): Observable<ResponseWrapper> {
        const copy = this.convertUpdate(purchasing);
        return this.http.post(`${this.resourceUrl}/document/set-status-tt`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    changePenerima(purchasing: TandaTerimaDokumenSKG): Observable<ResponseWrapper> {
        const copy = this.convertUpdate(purchasing);
        return this.http.post(`${this.resourceUrl}/document/set-penerima`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertUpdate(purchasing: TandaTerimaDokumenSKG): TandaTerimaDokumenSKG {
        const copy: TandaTerimaDokumenSKG = Object.assign({}, purchasing);

        // copy.dtcreated = this.dateUtils.toDate(purchasing.dtcreated);

        // copy.dtmodified = this.dateUtils.toDate(purchasing.dtmodified);
        return copy;
    }

    deleteDocument(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/document/${id}`);
    }

    // updateTTK(tandaTerima: TandaTerimaKlaimSKG): Observable<TandaTerimaKlaimSKG> {
    //     const copy = this.convert(tandaTerima);
    //     return this.http.put(this.resourceUrl + '/updateTTK', copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }

}
