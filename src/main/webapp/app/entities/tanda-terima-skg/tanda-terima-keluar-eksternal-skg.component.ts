import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { MasterCustomer, TandaTerimaDokumenSKG, TandaTerimaDokumenStatus } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
// import {TandaTerimaSuratNewSKG} from './'
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import {TandaTerimaSuratSKGEdit} from './';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';
import { includes } from 'lodash';

@Component({
    selector: 'jhi-tanda-terima-keluar-eksternal',
    templateUrl: './tanda-terima-keluar-eksternal-skg.component.html'
})
export class TandaTerimaKeluarEksternalComponent implements OnInit, OnDestroy {

    currentAccount: any;
    tandaTerimas: TandaTerimaDokumenSKG[];
    tandaTerima: TandaTerimaDokumenSKG;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;
    stat: any;
    modalLihatPO: boolean;
    historyTT: TandaTerimaDokumenStatus[];
    // field filter
    filDtFrom: Date;
    filDtThru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    kdSPL: string;
    isFilter: boolean;
    isFilter1: boolean;
    selectedPIC: string;
    modalCetakan: boolean;
    // field filter
    loginName: string;
    curAccount: string;
    idpp: any;
    divisi: any;
    selected: any = [];
    idArray: any = [];
    isSaving: boolean;
    inputNamaPT: any;
    inputJenisDokumen: any;
    inputStatus: any;
    dtfrom: Date;
    dtthru: Date;
    noteCancelTT: string;
    newModalNote1: boolean;
    newModalTIKI: boolean;
    newModalScan: boolean;
    reciveby: string;
    tanggal_terima: Date;
    nama_pt: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'SKG', value: 'SKG' },
        { label: 'SBS', value: 'SBS' },
        { label: 'ACA', value: 'ACA' },
        { label: 'Pribadi', value: 'Pribadi' }
    ];
    jenis_dokumen: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Dokumen', value: 'Dokumen' },
        { label: 'Paket', value: 'Paket' }
    ];

    jenis_pengiriman: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Internal', value: 'Internal' },
        { label: 'Eksternal', value: 'Eksternal' }
    ];
    status: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Paket Dibuat', value: 0 },
        { label: 'Paket Diperiksa', value: 1 },
        { label: 'Paket Dipickup Kurir', value: 2 },
        { label: 'Paket Diterima Ekspedisi', value: 4 },
        { label: 'Paket Dibatalkan', value: 3 },
    ];

    kurir: Array<object> = [
        { label: 'Pilih Kurir / Ekspedisi', value: '' },
        { label: 'JNE', value: 'jne' },
        { label: 'TIKI', value: 'tiki' },
        { label: 'POS Indonesia', value: 'pos_indonesia' },
        { label: 'Gojek (GoSend)', value: 'gojek' },
        { label: 'Grab (GrabExpress)', value: 'grab' },
        { label: 'SICEPAT', value: 'sicepat' },
        { label: 'Ninja Xpress', value: 'ninja_xpress' },
        { label: 'J&T Express', value: 'jt_express' },
        { label: 'Lion Parcel', value: 'lion_parcel' },
        { label: 'Wahana', value: 'wahana' },
        { label: 'Anteraja', value: 'anteraja' },
        { label: 'Deliveree', value: 'deliveree' },
        { label: 'Raya Logistik', value: 'raya_logistik' },
        { label: 'Pandu Logistics', value: 'pandu_logistics' },
        { label: 'Others', value: 'Others' },
    ];

    inputKurir: string;
    layananKurir: Array<object> = [
        { label: 'Pilih Layanan', value: '' },

        // JNE
        { label: 'JNE Reguler', value: 'jne_reguler', kurir: 'jne' },
        { label: 'JNE YES (Yakin Esok Sampai)', value: 'jne_yes', kurir: 'jne' },
        { label: 'JNE OKE (Ongkos Kirim Ekonomis)', value: 'jne_oke', kurir: 'jne' },
        { label: 'JNE Logistic', value: 'jne_logistic', kurir: 'jne' },

        // TIKI
        { label: 'TIKI Reguler', value: 'tiki_reguler', kurir: 'tiki' },
        { label: 'TIKI Next Day Service', value: 'tiki_next_day', kurir: 'tiki' },
        { label: 'TIKI ONS (Overnight Service)', value: 'tiki_ons', kurir: 'tiki' },

        // POS Indonesia
        { label: 'POS Kilat Khusus', value: 'pos_kilat_khusus', kurir: 'pos_indonesia' },
        { label: 'POS Reguler', value: 'pos_reguler', kurir: 'pos_indonesia' },
        { label: 'POS Ekspres', value: 'pos_ekspres', kurir: 'pos_indonesia' },

        // Gojek (GoSend)
        { label: 'GoSend Same Day', value: 'gojek_gosend_same_day', kurir: 'gojek' },
        { label: 'GoSend Instant', value: 'gojek_gosend_instant', kurir: 'gojek' },

        // Grab (GrabExpress)
        { label: 'GrabExpress Same Day', value: 'grab_grabexpress_same_day', kurir: 'grab' },
        { label: 'GrabExpress Instant', value: 'grab_grabexpress_instant', kurir: 'grab' },

        // SICEPAT
        { label: 'SICEPAT Regular', value: 'sicepat_regular', kurir: 'sicepat' },
        { label: 'SICEPAT Ekspres', value: 'sicepat_ekspres', kurir: 'sicepat' },

        // Ninja Xpress
        { label: 'Ninja Xpress Regular', value: 'ninja_xpress_regular', kurir: 'ninja_xpress' },
        { label: 'Ninja Xpress Same Day', value: 'ninja_xpress_same_day', kurir: 'ninja_xpress' },

        // J&T Express
        { label: 'J&T Express Reguler', value: 'jt_express_reguler', kurir: 'jt_express' },
        { label: 'J&T Express Same Day', value: 'jt_express_same_day', kurir: 'jt_express' },

        // Lion Parcel
        { label: 'Lion Parcel Regular', value: 'lion_parcel_regular', kurir: 'lion_parcel' },
        { label: 'Lion Parcel Ekspres', value: 'lion_parcel_ekspres', kurir: 'lion_parcel' },

        // Wahana
        { label: 'Wahana Reguler', value: 'wahana_reguler', kurir: 'wahana' },
        { label: 'Wahana Ekspres', value: 'wahana_ekspres', kurir: 'wahana' },

        // Anteraja
        { label: 'Anteraja Regular', value: 'anteraja_regular', kurir: 'anteraja' },
        { label: 'Anteraja Ekspres', value: 'anteraja_ekspres', kurir: 'anteraja' },

        // Deliveree
        { label: 'Deliveree Same Day', value: 'deliveree_same_day', kurir: 'deliveree' },
        { label: 'Deliveree Regular', value: 'deliveree_regular', kurir: 'deliveree' },

        // Raya Logistik
        { label: 'Raya Logistik Reguler', value: 'raya_logistik_reguler', kurir: 'raya_logistik' },
        { label: 'Raya Logistik Ekspres', value: 'raya_logistik_ekspres', kurir: 'raya_logistik' },

        // Pandu Logistics
        { label: 'Pandu Logistics Reguler', value: 'pandu_logistics_reguler', kurir: 'pandu_logistics' },
        { label: 'Pandu Logistics Same Day', value: 'pandu_logistics_same_day', kurir: 'pandu_logistics' }
    ];
    filteredCodeSuppliers: any[];
    currentUser: string;
    nottt: string;
    newModalHistory: boolean;
    inputJenis: string;
    inputEkspedisi: string;
    idInternal: string;
    cust_code: string;
    newSuplier: MasterCustomer;
    constructor(
        protected tandaTerimaService: TandaTerimaSKGService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected modalService: NgbModal,
        protected loadingService: LoadingService,
        protected toaster: ToasterService,
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.inputNamaPT = '';
        this.inputJenisDokumen = '';
        this.inputStatus = '';
        this.newModalNote1 = false;
        this.newModalTIKI = false;
        this.tanggal_terima = new Date();
        this.currentUser = this.principal.getUserLogin();
        this.divisi = this.principal.getIdInternal();
        this.newModalScan = false;
        this.nottt = '';
        this.newModalHistory = false;
        this.inputEkspedisi = '';
        this.cust_code = '';
        if (this.divisi.includes('SKG')) {
            this.nama_pt = this.nama_pt.filter((x) =>
                x['label'] === 'SKG' || x['label'] === 'Semua' || x['label'] === 'Pribadi' || x['label'] === 'SBS'
            );

        } else if (!this.divisi.includes('SKG') && !this.divisi.includes('SMK')) {
            this.nama_pt = this.nama_pt.filter((x) =>
                x['label'] === 'SBS' || x['label'] === 'Semua' || x['label'] === 'Pribadi'
            );
        }
        this.kurir = this.kurir.filter((x) =>
            x['label'] === 'Pilih Kurir / Ekspedisi' || x['label'] === 'TIKI'
        );
        this.modalCetakan = false
    }

    ngOnInit() {
        this.curAccount = this.principal.getUserLogin();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchaseOrders();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {
        this.loadingService.loadingStart();
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        if (this.isFilter === true) {
            this.tandaTerimaService.queryFilterDokumen({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'dtfrom:' + dari + '|dtthru:' + sampai + '|pt:' + this.inputNamaPT + '|jenis:' + '|status:' + this.inputStatus + '|jenis_pengiriman:' + '|exit:Eksternal' + '|divisi_penerima:' + '|cust_code:' + this.cust_code + '|kurir:' + this.inputKurir
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.tandaTerimaService.queryDokumen({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch + '|exit:Eksternal'
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.tandaTerimas = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    registerChangeInPurchaseOrders() {
        this.loadingService.loadingStart();
        this.eventSubscriber = this.eventManager.subscribe('TandaTerimaSuratSKGListModification', (response) => this.loadAll());
    }

    trackId(index: number, item: TandaTerimaDokumenSKG) {
        return item.idreceipt;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'idreceipt') {
            result.push('idreceipt');
        }
        return result;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.tandaTerimaService.delete1(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'TandaTerimaSuratSKGListModification',
                        content: 'Deleted an Tanda Terima Surat'
                    });
                });
            }
        });
    }

    filter() {
        this.isFilter = true;
        this.loadingService.loadingStart();
        this.loadAll();
    }

    reset() {
        this.isFilter = false;
        this.inputNamaPT = '';
        this.inputJenisDokumen = '';
        this.inputStatus = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.inputJenis = '';
        this.loadingService.loadingStart();
        this.loadAll()
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/tanda-terima-tiki-skg'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.isFilter = false;
        this.router.navigate(['/tanda-terima-tiki-skg', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isFilter = false;
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/tanda-terima-tiki-skg', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    printTT(rowData: TandaTerimaDokumenSKG) {
        // this.loadingService.loadingStart();
        this.idArray.push(rowData.receiptno);
        const new_id = this.idArray.toString();
        this.modalCetakan = true;
        // this.loadingService.loadingStart();
        // const filter_data = 'id:' + new_id;
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_rpt_tiki/pdf', { filterData: filter_data });
        // this.idArray = [];
        // this.loadingService.loadingStop();
    }

    createRequestScan() {
        this.newModalScan = true;
        // this.confirmationService.confirm({
        //     message: 'Apakah Anda Yakin Paket Sudah Diterima ?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         this.Approved();
        //         this.loadingService.loadingStart();
        //     }
        // });
    }

    createRequestPrint() {
        const panjangArr = this.selected.length;
        for (let i = 0; i < panjangArr; i++) {
            this.idArray.push(this.selected[i].receiptno);
        }
        this.modalCetakan = true;
        // const new_id = this.idArray.toString();
        // this.loadingService.loadingStart();
        // const filter_data = 'id:' + new_id;
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_rpt_tiki/pdf', { filterData: filter_data });
        // this.selected = 0;
        // this.idArray = [];
        // this.loadingService.loadingStop();
    }

    Approved() {
        this.tandaTerimaService.isCek(this.selected).subscribe(
            (res) => {
                this.loadAll();
                this.loadingService.loadingStop();
                this.selected = new Array<TandaTerimaDokumenSKG>();
                alert('Sukses');
            }
        );
    }

    cancelTT(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Cancel?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.tandaTerimaService.deleteDocument(id).subscribe((response) => {
                    this.loadAll();

                });
            }
        });
        this.loadAll();
    }

    cancelTTRCP(purchasing: TandaTerimaDokumenSKG) {
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.tandaTerima = purchasing;
        this.newModalNote1 = true;
    }

    TambahPenerima(purchasing: TandaTerimaDokumenSKG) {
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.tandaTerima = purchasing;
        this.newModalTIKI = true;
    }

    modalCancelTT() {
        if (this.noteCancelTT == null) {
            alert('Kolom Tidak Boleh Kosong');
        } else {
            this.tandaTerima.alasan_cancel = this.noteCancelTT;
            this.tandaTerimaService.changeStatusTT(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStart();
                        this.newModalNote1 = false;
                        this.noteCancelTT = null;
                        this.loadAll();
                    }
                )
        }
    }

    modalTambahPenerima() {
        if (this.reciveby === null || this.reciveby === undefined || this.reciveby === '') {
            alert('Kolom Tidak Boleh Kosong');
        } else {
            this.tandaTerima.no_resi = this.reciveby;
            this.tandaTerimaService.changePenerima(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStart();
                        this.newModalTIKI = false;
                        this.reciveby = null;
                        this.tanggal_terima = new Date();
                        this.loadAll();
                    }
                )
        }
    }

    download() {
        this.loadingService.loadingStart();
        const date = new Date();

        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();

        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|pt:' + this.inputNamaPT + '|jenis_dokumen:' + '|status_tiki:' + this.inputStatus + '|idInternal:' + this.divisi + '|user:' + this.currentUser + '|tiki_exe:Eksternal'
            + '|divisi_penerima:' + '|cust_code:' + this.cust_code;
        this.reportUtilService.downloadFileWithName(`Report Tanda Terima Keluar Eksternal ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_tiki/xlsx', { filterData: filter_data });
        // console.log(filter_data);
        this.loadingService.loadingStop();
    }

    onKeyDown(event: any) {
        if (event.key === 'Enter') {
            if (this.nottt === null || this.nottt === undefined || this.nottt === '') {
                alert('Kolom Tidak Boleh Kosong')
            } else {
                const noTT = this.nottt.replace(/\//g, '-');
                this.tandaTerimaService.cekTIKI(noTT).subscribe(
                    (res: TandaTerimaDokumenSKG) => {
                        if (res.iscek === 10) {
                            this.toasterService.showToaster('success', 'Success Scan', 'Paket Telah Berhasil di Scan');
                            this.nottt = '';
                        } else if (res.iscek !== 10) {
                            this.toasterService.showToaster('error', 'Error Scan', `${res.alasan_cancel}`);
                            this.nottt = '';
                        }
                    }
                )
            }
        }
    }

    backapp() {
        this.nottt = '';
        this.newModalScan = false;
        this.loadAll();
    }

    backappHistory() {
        this.newModalHistory = false;
        this.loadAll();
    }

    createKirim() {
        const allSame = this.selected.every((item) => item.iscek === 1);
        if (allSame) {
            this.confirmationService.confirm({
                message: 'Apakah anda yakin ingin update status dokumen menjadi sudah di kirim ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.tandaTerimaService.setStatusKirim(this.selected).subscribe(
                        (res) => {
                            this.loadAll();
                            this.selected = new Array<TandaTerimaDokumenSKG>();
                            alert('Sukses');
                        }
                    )
                }
            });
        } else {
            alert('Terdapat Tanda Terima Keluar Eksternal yang statusnya bukan sudah diperiksa operator');
        }
    }

    HistoryTT(rowData: TandaTerimaDokumenSKG) {
        this.tandaTerimaService.cekData({
            idreceipt: rowData.idreceipt
        }).subscribe(
            (res: ResponseWrapper) => {
                this.historyTT = res.json;
                this.newModalHistory = true
            })
    }

    filterSupplierSingle(event) {
        const query = event.query;
        if (this.divisi.includes('SKG')) {
            this.tandaTerimaService.getDistributor().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        } else if (!this.divisi.includes('SKG') && !this.divisi.includes('SMK')) {
            this.tandaTerimaService.getDistributorSBS().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        }
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    public selectSupplier(isSelect?: boolean): void {
        if (isSelect === true) {
            this.cust_code = this.newSuplier.cust_name;
        } else {
            this.cust_code = '';
        }
    }

    getLabelForJenisLayanan(value: string): string {
        const layan = [
            { label: 'Pilih Layanan', value: '' },

            // JNE
            { label: 'JNE Reguler', value: 'jne_reguler', kurir: 'jne' },
            { label: 'JNE YES (Yakin Esok Sampai)', value: 'jne_yes', kurir: 'jne' },
            { label: 'JNE OKE (Ongkos Kirim Ekonomis)', value: 'jne_oke', kurir: 'jne' },
            { label: 'JNE Logistic', value: 'jne_logistic', kurir: 'jne' },

            // TIKI
            { label: 'TIKI Reguler', value: 'tiki_reguler', kurir: 'tiki' },
            { label: 'TIKI Next Day Service', value: 'tiki_next_day', kurir: 'tiki' },
            { label: 'TIKI ONS (Overnight Service)', value: 'tiki_ons', kurir: 'tiki' },

            // POS Indonesia
            { label: 'POS Kilat Khusus', value: 'pos_kilat_khusus', kurir: 'pos_indonesia' },
            { label: 'POS Reguler', value: 'pos_reguler', kurir: 'pos_indonesia' },
            { label: 'POS Ekspres', value: 'pos_ekspres', kurir: 'pos_indonesia' },

            // Gojek (GoSend)
            { label: 'GoSend Same Day', value: 'gojek_gosend_same_day', kurir: 'gojek' },
            { label: 'GoSend Instant', value: 'gojek_gosend_instant', kurir: 'gojek' },

            // Grab (GrabExpress)
            { label: 'GrabExpress Same Day', value: 'grab_grabexpress_same_day', kurir: 'grab' },
            { label: 'GrabExpress Instant', value: 'grab_grabexpress_instant', kurir: 'grab' },

            // SICEPAT
            { label: 'SICEPAT Regular', value: 'sicepat_regular', kurir: 'sicepat' },
            { label: 'SICEPAT Ekspres', value: 'sicepat_ekspres', kurir: 'sicepat' },

            // Ninja Xpress
            { label: 'Ninja Xpress Regular', value: 'ninja_xpress_regular', kurir: 'ninja_xpress' },
            { label: 'Ninja Xpress Same Day', value: 'ninja_xpress_same_day', kurir: 'ninja_xpress' },

            // J&T Express
            { label: 'J&T Express Reguler', value: 'jt_express_reguler', kurir: 'jt_express' },
            { label: 'J&T Express Same Day', value: 'jt_express_same_day', kurir: 'jt_express' },

            // Lion Parcel
            { label: 'Lion Parcel Regular', value: 'lion_parcel_regular', kurir: 'lion_parcel' },
            { label: 'Lion Parcel Ekspres', value: 'lion_parcel_ekspres', kurir: 'lion_parcel' },

            // Wahana
            { label: 'Wahana Reguler', value: 'wahana_reguler', kurir: 'wahana' },
            { label: 'Wahana Ekspres', value: 'wahana_ekspres', kurir: 'wahana' },

            // Anteraja
            { label: 'Anteraja Regular', value: 'anteraja_regular', kurir: 'anteraja' },
            { label: 'Anteraja Ekspres', value: 'anteraja_ekspres', kurir: 'anteraja' },

            // Deliveree
            { label: 'Deliveree Same Day', value: 'deliveree_same_day', kurir: 'deliveree' },
            { label: 'Deliveree Regular', value: 'deliveree_regular', kurir: 'deliveree' },

            // Raya Logistik
            { label: 'Raya Logistik Reguler', value: 'raya_logistik_reguler', kurir: 'raya_logistik' },
            { label: 'Raya Logistik Ekspres', value: 'raya_logistik_ekspres', kurir: 'raya_logistik' },

            // Pandu Logistics
            { label: 'Pandu Logistics Reguler', value: 'pandu_logistics_reguler', kurir: 'pandu_logistics' },
            { label: 'Pandu Logistics Same Day', value: 'pandu_logistics_same_day', kurir: 'pandu_logistics' }
        ];
        const layanan = layan.find((item) => item.value === value);
        return layanan ? layanan.label : 'Layanan Tidak Ditemukan';
    }

    getLabelForEkspedisi(value: string): string {
        const layan = [
            { label: 'Pilih Kurir / Ekspedisi', value: '' },
            { label: 'JNE', value: 'jne' },
            { label: 'TIKI', value: 'tiki' },
            { label: 'POS Indonesia', value: 'pos_indonesia' },
            { label: 'Gojek (GoSend)', value: 'gojek' },
            { label: 'Grab (GrabExpress)', value: 'grab' },
            { label: 'SICEPAT', value: 'sicepat' },
            { label: 'Ninja Xpress', value: 'ninja_xpress' },
            { label: 'J&T Express', value: 'jt_express' },
            { label: 'Lion Parcel', value: 'lion_parcel' },
            { label: 'Wahana', value: 'wahana' },
            { label: 'Anteraja', value: 'anteraja' },
            { label: 'Deliveree', value: 'deliveree' },
            { label: 'Raya Logistik', value: 'raya_logistik' },
            { label: 'Pandu Logistics', value: 'pandu_logistics' },
            { label: 'Others', value: 'Others' },
        ];
        const layanan = layan.find((item) => item.value === value);
        return layanan ? layanan.label : 'Ekspedisi Tidak Ditemukan';
    }

    printSizeKecil() {
        this.loadingService.loadingStart();
        const new_id = this.idArray.toString();
        const filter_data = 'id:' + new_id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_rpt_tiki/pdf', { filterData: filter_data });
        this.idArray = [];
        this.selected = 0;
        this.modalCetakan = false;
        this.loadingService.loadingStop();
    }

    printSizeBesar() {
        this.loadingService.loadingStart();
        const new_id = this.idArray.toString();
        const filter_data = 'id:' + new_id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_rpt_tiki_besar/pdf', { filterData: filter_data });
        this.idArray = [];
        this.selected = 0;
        this.modalCetakan = false;
        this.loadingService.loadingStop();
    }
}
