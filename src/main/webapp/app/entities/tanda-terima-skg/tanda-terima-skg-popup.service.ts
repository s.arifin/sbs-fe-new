import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { TandaTerimaSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';

@Injectable()
export class TandaTerimaSKGPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private tandaTerimaService: TandaTerimaSKGService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.tandaTerimaService.find(id).subscribe((tandaTerima) => {
                    tandaTerima.dtreceipt = this.datePipe
                        .transform(tandaTerima.dtreceipt, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.tandaTerimaModalRef(component, tandaTerima);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.tandaTerimaModalRef(component, new TandaTerimaSKG());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    load(component: Component, data?: Object): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.tandaTerimaModalRef(component, new TandaTerimaSKG());
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    tandaTerimaModalRef(component: Component, tandaTerima: TandaTerimaSKG): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tandaTerima = tandaTerima;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
