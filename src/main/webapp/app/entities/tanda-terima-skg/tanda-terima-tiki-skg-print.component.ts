import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';

import { TandaTerimaDokumenSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService} from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService , LazyLoadEvent } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';
import { array } from '@amcharts/amcharts4/core';
import { Location } from '@angular/common';

@Component({
    selector: 'jhi-tanda-terima-tiki-skg-print',
    templateUrl: './tanda-terima-tiki-skg-print.component.html'
})
export class TandaTerimaTikiSKGPrintComponent implements OnInit, OnDestroy {

currentAccount: any;
    tandaTerimas: TandaTerimaDokumenSKG[];
    tandaTerima: TandaTerimaDokumenSKG;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    divisi: any;
    selected: any;
    isSaving: boolean;
    inputNamaPT: any;
    inputJenisDokumen: any;
    inputStatus: any;
    isFilter: boolean;
    dtfrom: Date;
    dtthru: Date;
    noteCancelTT: string;
    newModalNote1: boolean;
    newModalTIKI: boolean;
    reciveby: string;
    tanggal_terima: Date;
    subscription: any;

    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        protected confirmationService: ConfirmationService,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        protected reportUtilService: ReportUtilService,
        private route: ActivatedRoute,
        private location: Location

    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.page = 0;
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.inputNamaPT = '';
        this.inputJenisDokumen = '';
        this.inputStatus = '';
        this.newModalNote1 = false;
        this.newModalTIKI = false;
        this.tanggal_terima = new Date();
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                    this.load(params['id']);
            } else {
                this.location.back();
            }
        });
    }

    load(id) {
        this.loadingService.loadingStart();
        this.tandaTerimaService.GetDataTIKI(id).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        ) ;
    }

    private onSuccess(data, headers) {
        this.tandaTerimas = data;
        console.log(data);
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }

    print() {
        const printContent = document.getElementById('print');
        const WindowPrt = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
        WindowPrt.document.write(printContent.innerHTML);
        WindowPrt.document.close();
        WindowPrt.focus();
        WindowPrt.print();
        WindowPrt.close();
    }

    back() {
        this.router.navigate(['../tanda-terima-tiki-skg']);
    }
}
