import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { MasterCustomer, TandaTerimaSuratSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
// import {TandaTerimaSuratNewSKG} from './'
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import {TandaTerimaSuratSKGEdit} from './';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-tanda-terima-surat-skg',
    templateUrl: './tanda-terima-surat-skg.component.html'
})
export class TandaTerimaSuratSKGComponent implements OnInit, OnDestroy {

    currentAccount: any;
    tandaTerimas: TandaTerimaSuratSKG[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;
    stat: any;
    modalLihatPO: boolean;

    // field filter
    filDtFrom: Date;
    filDtThru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    kdSPL: string;
    isFilter: boolean;
    isFilter1: boolean;
    selectedPIC: string;
    // field filter
    loginName: string;
    curAccount: string;
    inputDivisi: string;
    [x: string]: any;
    divisi: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Others', value: 'Others' },
    ]
    statuss: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Sudah di Terima', value: '1' },
        { label: 'Belum di Terima', value: '0' },
    ]
    cust_code: string;
    newSuplier: MasterCustomer;
    statusTT: string;
    selected: any = [];
    constructor(
        protected tandaTerimaService: TandaTerimaSKGService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected modalService: NgbModal,
        protected loadingService: LoadingService,
        protected toaster: ToasterService,
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.filDtFrom = null;
        this.filDtThru = null;
        this.inputDivisi = '';
        this.cust_code = '';
        this.statusTT = '';
    }

    ngOnInit() {
        this.curAccount = this.principal.getUserLogin();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchaseOrders();

        this.tandaTerimaService.getAutority({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc'],
            query: 'query:SKG'
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.divisi.push({
                    label: element.createdBy,
                    value: element.createdBy
                });
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    public selectSupplier(isSelect?: boolean): void {
        if (isSelect === true) {
            this.cust_code = this.newSuplier.cust_name;
        } else {
            this.cust_code = '';
        }
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {
        this.loadingService.loadingStart();

        if (this.isFilter === true) {
            let dari = '';
            let sampai = '';
            if (this.filDtFrom === null || this.filDtFrom === undefined) {
                dari = '';
                sampai = '';
            } else {
                const waktu = '23:59:59';
                dari = this.filDtFrom.getFullYear() + '-' + (this.filDtFrom.getMonth() + 1) + '-' + this.filDtFrom.getDate();
                sampai = this.filDtThru.getFullYear() + '-' + (this.filDtThru.getMonth() + 1) + '-' + this.filDtThru.getDate() + ' ' + waktu;
            }

            this.tandaTerimaService.querySuratFilter({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'dtfrom:' + dari + '|dtthru:' + sampai + '|cari:' + this.currentSearch + '|cust_code:' + this.cust_code + '|divisi:' + this.inputDivisi + '|status_tt:' + this.statusTT + '|plant:SKG'
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.tandaTerimaService.querySurat({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.tandaTerimas = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    registerChangeInPurchaseOrders() {
        this.loadingService.loadingStart();
        this.eventSubscriber = this.eventManager.subscribe('TandaTerimaSuratSKGListModification', (response) => this.loadAll());
    }

    trackId(index: number, item: TandaTerimaSuratSKG) {
        return item.idreceipt;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'idreceipt') {
            result.push('idreceipt');
        }
        return result;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.tandaTerimaService.delete1(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'TandaTerimaSuratSKGListModification',
                        content: 'Deleted an Tanda Terima Surat'
                    });
                });
            }
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/tanda-terima-surat-skg'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.isFilter = false;
        this.router.navigate(['/tanda-terima-surat-skg', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/tanda-terima-surat-skg', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    filter() {
        this.isFilter = true;
        this.loadAll();
    }

    reset() {
        this.isFilter = false;
        this.filDtFrom = null;
        this.filDtThru = null;
        this.currentSearch = '';
        this.newSuplier = new MasterCustomer();
        this.inputDivisi = '';
        this.cust_code = '';
        this.statusTT = '';
        this.loadAll();
    }

    download() {
        this.loadingService.loadingStart();
        const date = new Date();

        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();

        const waktu = '23:59:59';
        const dari = this.filDtFrom.getFullYear() + '-' + (this.filDtFrom.getMonth() + 1) + '-' + this.filDtFrom.getDate();
        const sampai = this.filDtThru.getFullYear() + '-' + (this.filDtThru.getMonth() + 1) + '-' + this.filDtThru.getDate() + ' ' + waktu;
        const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|cari:' + this.currentSearch + '|cust_code:' + this.cust_code + '|divisi:' + this.inputDivisi + '|status_tt:' + this.statusTT + '|plant:SKG';
        this.reportUtilService.downloadFileWithName(`Report Tanda Terima Masuk ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/tanda_terima_surat_skg/xlsx', { filterData: filter_data });
        // console.log(filter_data);
        this.loadingService.loadingStop();
        console.log(filter_data)
    }

    createKirim() {
        const allSame = this.selected.every((item) => item.is_sent === '0');
        if (allSame) {
            this.confirmationService.confirm({
                message: 'Apakah anda yakin ingin update menjadi sudah di terima ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.tandaTerimaService.setStatusSelesai(this.selected).subscribe(
                        (res) => {
                            this.loadAll();
                            this.selected = new Array<TandaTerimaSuratSKG>();
                            alert('Sukses');
                        }
                    )
                }
            });
        } else {
            alert('Terdapat Tanda Terima Masuk yang statusnya bukan sudah diperiksa operator');
        }
    }

}
