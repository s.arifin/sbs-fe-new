import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { TandaTerimaSKG, MasterCustomer, TandaTerimaKlaimSKG, DetailTandaTerimaKlaimSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
// import {TandaTerimaSuratNewSKG} from './'
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import {TandaTerimaSuratSKGEdit} from './';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-tanda-terima-klaim-skg',
    templateUrl: './tanda-terima-klaim-skg.component.html'
})
export class TandaTerimaKlaimSKGComponent implements OnInit, OnDestroy {

    currentAccount: any;
    tandaTerimas: TandaTerimaKlaimSKG[];
    item: DetailTandaTerimaKlaimSKG;
    items: DetailTandaTerimaKlaimSKG[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;
    stat: any;
    modalLihatPO: boolean;

     // field filter
     filDtFrom: Date;
     filDtThru: Date;
     dtfromstr: string;
     dtthrustr: string;
     filteredSuppliers: any[];
     kdSPL: string;
     isFilter: boolean;
     isFilter1: boolean;
     selectedPIC: string;
     // field filter
     loginName: string;
     curAccount: string;
     newSuplier: MasterCustomer;
     kdsup: string;
     filteredCodeSuppliers: any[];
     dtfrom: Date;
    dtthru: Date;
     constructor(
        protected tandaTerimaService: TandaTerimaSKGService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected modalService: NgbModal,
        protected loadingService: LoadingService,
        protected toaster: ToasterService,
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterCustomer();

    }

    ngOnInit() {
        this.curAccount = this.principal.getUserLogin();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchaseOrders();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {
        this.loadingService.loadingStart();
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        if (this.isFilter === true) {
            this.tandaTerimaService.queryFilterKlaim({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch + '|kdsup:' + this.kdsup + '|dtfrom:' + dari + '|dtthru:' + sampai}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.tandaTerimaService.queryKlaim({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.tandaTerimas = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    registerChangeInPurchaseOrders() {
        this.eventSubscriber = this.eventManager.subscribe('TandaTerimaSuratSKGListModification', (response) => this.loadAll());
    }

    trackId(index: number, item: TandaTerimaKlaimSKG) {
        return item.idreceipt;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'idreceipt') {
            result.push('idreceipt');
        }
        return result;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Cancel?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.tandaTerimaService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'TandaTerimaSuratSKGListModification',
                        content: 'Deleted an Tanda Terima Surat'
                    });
                });
            }
        });
        // this.loadAll();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/tanda-terima-klaim-skg'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.isFilter = false;
        this.router.navigate(['/tanda-terima-klaim-skg', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/tanda-terima-klaim-skg', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    printTT(rowData: TandaTerimaKlaimSKG) {
        const filter_data = 'idreceipt:' + rowData.idreceipt;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tandaterimaklaim_skg/pdf', { filterData: filter_data });
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.cust_code;
    }
    filter() {
        this.isFilter = true;
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.cust_code === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.cust_code;
        }
        this.loadAll();
    }
    reset() {
        this.isFilter = false;
        this.kdsup = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.currentSearch = '';
        this.loadAll();
    }

    download() {
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        if (this.kdsup === '' && this.kdsup === undefined) {
            this.kdsup = '';
        }
        const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdsup;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/tanda_terima_ekspedisi/xlsx', { filterData: filter_data });
    }

}
