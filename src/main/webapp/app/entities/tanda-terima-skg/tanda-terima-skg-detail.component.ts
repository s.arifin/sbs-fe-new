import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TandaTerimaSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';

@Component({
    selector: 'jhi-tanda-terima-skg-detail',
    templateUrl: './tanda-terima-skg-detail.component.html'
})
export class TandaTerimaSKGDetailComponent implements OnInit, OnDestroy {

    tandaTerima: TandaTerimaSKG;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tandaTerimaService: TandaTerimaSKGService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTandaTerimas();
    }

    load(id) {
        this.tandaTerimaService.find(id).subscribe((tandaTerima) => {
            this.tandaTerima = tandaTerima;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTandaTerimas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tandaTerimaListModification',
            (response) => this.load(this.tandaTerima.id)
        );
    }
}
