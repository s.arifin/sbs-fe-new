import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { TandaTerimaDokumenSKG, TandaTerimaDokumenStatus } from './tanda-terima-skg.model';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
// import {TandaTerimaSuratNewSKG} from './'
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import {TandaTerimaSuratSKGEdit} from './';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-tanda-terima-keluar-internal',
    templateUrl: './tanda-terima-keluar-internal-skg.component.html'
})
export class TandaTerimaKeluarInternalComponent implements OnInit, OnDestroy {
    [x: string]: any;
    currentAccount: any;
    tandaTerimas: TandaTerimaDokumenSKG[];
    tandaTerima: TandaTerimaDokumenSKG;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;
    stat: any;
    modalLihatPO: boolean;
    historyTT: TandaTerimaDokumenStatus[];
    // field filter
    filDtFrom: Date;
    filDtThru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    kdSPL: string;
    isFilter: boolean;
    isFilter1: boolean;
    selectedPIC: string;
    // field filter
    loginName: string;
    curAccount: string;
    idpp: any;
    divisi: any;
    selected: any = [];
    idArray: any = [];
    isSaving: boolean;
    inputNamaPT: any;
    inputJenisDokumen: any;
    inputStatus: any;
    dtfrom: Date;
    dtthru: Date;
    noteCancelTT: string;
    newModalNote1: boolean;
    newModalTIKI: boolean;
    newModalScan: boolean;
    reciveby: string;
    tanggal_terima: Date;
    nama_pt: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'SKG', value: 'SKG' },
        { label: 'SBS', value: 'SBS' },
        { label: 'ACA', value: 'ACA' },
        { label: 'SMK', value: 'SMK' },
        { label: 'Other', value: 'Other' }
    ];
    jenis_dokumen: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Dokumen', value: 'Dokumen' },
        { label: 'Paket', value: 'Paket' }
    ];

    jenis_pengiriman: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Internal', value: 'Internal' },
        { label: 'Eksternal', value: 'Eksternal' }
    ];
    status: Array<object> = [
        { label: 'Semua', value: '' },
        { label: 'Paket Dibuat', value: 0 },
        { label: 'Paket Diperiksa', value: 1 },
        { label: 'Paket Dipickup Supir', value: 2 },
        { label: 'Paket Diterima User', value: 4 },
        { label: 'Paket Dibatalkan', value: 3 },
    ];

    kurir: Array<object> = [
        { label: 'Pilih Kurir / Ekspedisi', value: '' },
        { label: 'TIKI', value: 'TIKI' },
        { label: 'JNE', value: 'JNE' },
        { label: 'Lion Parcel', value: 'Lion Parcel' },
        { label: 'Anteraja', value: 'Anteraja' },
        { label: 'Grab Express', value: 'Grab Express' },
        { label: 'Gosend', value: 'Gosend' },
        { label: 'Sicepat', value: 'Sicepat' },
        { label: 'Ninja Xpress', value: 'Ninja Xpress' },
        { label: 'Paxel', value: 'Paxel' },
        { label: 'Maxim', value: 'Maxim' },
        { label: 'J&T', value: 'J&T' },
        { label: 'Wahana', value: 'Wahana' },
        { label: 'Pos Indonesia', value: 'Pos Indonesia' },
        { label: 'Others', value: 'Others' },
    ];
    currentUser: string;
    nottt: string;
    newModalHistory: boolean;
    inputJenis: string;
    inputEkspedisi: string;
    inputDivisi: any;
    divition: Array<any> = [
        { label: 'SBS', items: [] },
        { label: 'SKG', items: [] }
    ]
    constructor(
        protected tandaTerimaService: TandaTerimaSKGService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected modalService: NgbModal,
        protected loadingService: LoadingService,
        protected toaster: ToasterService,
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.inputNamaPT = '';
        this.inputJenisDokumen = '';
        this.inputStatus = '';
        this.newModalNote1 = false;
        this.newModalTIKI = false;
        this.tanggal_terima = new Date();
        this.currentUser = this.principal.getUserLogin();
        this.divisi = this.principal.getIdInternal();
        this.newModalScan = false;
        this.nottt = '';
        this.newModalHistory = false;
        this.inputEkspedisi = '';
        this.inputDivisi = '';
    }

    ngOnInit() {
        this.curAccount = this.principal.getUserLogin();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchaseOrders();

        this.tandaTerimaService.getAutority({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc'],
            query: 'query:all'
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                const groupLabel = element.approvedBy; // Tentukan grup berdasarkan createdBy (SBS atau SKG)

                // Cari grup berdasarkan label (SBS atau SKG)
                const groupIndex = this.divition.findIndex((group) => group.label === groupLabel);
                // Tambahkan item ke grup yang ditemukan
                if (groupIndex !== -1) {
                    this.divition[groupIndex].items.push({
                        label: element.createdBy,  // Label item (sesuaikan dengan kebutuhan)
                        value: element.createdBy    // Value item
                    });
                }
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        console.log(this.divition);
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {
        this.loadingService.loadingStart();
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        if (this.isFilter === true) {
            this.tandaTerimaService.queryFilterDokumen({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'dtfrom:' + dari + '|dtthru:' + sampai + '|pt:' + this.inputNamaPT + '|jenis:' + '|status:' + this.inputStatus + '|jenis_pengiriman:' + '|exit:Internal' + '|divisi_penerima:' + this.inputDivisi + '|cust_code:' + '|kurir:'
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.tandaTerimaService.queryDokumen({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                query: 'cari:' + this.currentSearch + '|exit:Internal'
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.tandaTerimas = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    registerChangeInPurchaseOrders() {
        this.loadingService.loadingStart();
        this.eventSubscriber = this.eventManager.subscribe('TandaTerimaSuratSKGListModification', (response) => this.loadAll());
    }

    trackId(index: number, item: TandaTerimaDokumenSKG) {
        return item.idreceipt;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'idreceipt') {
            result.push('idreceipt');
        }
        return result;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.tandaTerimaService.delete1(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'TandaTerimaSuratSKGListModification',
                        content: 'Deleted an Tanda Terima Surat'
                    });
                });
            }
        });
    }

    filter() {
        this.isFilter = true;
        this.loadingService.loadingStart();
        this.loadAll();
    }

    reset() {
        this.isFilter = false;
        this.inputNamaPT = '';
        this.inputJenisDokumen = '';
        this.inputStatus = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.inputJenis = '';
        this.inputDivisi = '';
        this.loadingService.loadingStart();
        this.loadAll()
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/tanda-terima-keluar-internal-skg'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.isFilter = false;
        this.router.navigate(['/tanda-terima-keluar-internal-skg', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isFilter = false;
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/tanda-terima-keluar-internal-skg', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    printTT(rowData: TandaTerimaDokumenSKG) {
        this.loadingService.loadingStart();
        //     const filter_data = 'idreceipt:' + id;
        //     this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tanda_kirim_skg/pdf', { filterData: filter_data });
        //     if (print === 0) {
        //         this.tandaTerimaService.print(id).subscribe((response) => {
        //             this.loadAll();
        //         });
        // }
        // this.loadingService.loadingStart();
        // const filter_data = 'id:' + id;
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_/pdf', { filterData: filter_data });
        // this.loadingService.loadingStop();
        this.idArray.push(rowData.receiptno);
        const new_id = this.idArray.toString();
        this.loadingService.loadingStart();
        const filter_data = 'id:' + new_id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_rpt_tiki_internal/pdf', { filterData: filter_data });
        this.idArray = [];
        this.loadingService.loadingStop();
    }

    createRequestScan() {
        this.newModalScan = true;
        // this.confirmationService.confirm({
        //     message: 'Apakah Anda Yakin Paket Sudah Diterima ?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         this.Approved();
        //         this.loadingService.loadingStart();
        //     }
        // });
    }

    createRequestPrint() {
        const panjangArr = this.selected.length;
        for (let i = 0; i < panjangArr; i++) {
            // this.idArray.push('\'' + this.selected[i].receiptno.replace(/,/g, '\',\'') + '\'');
            this.idArray.push(this.selected[i].receiptno);
        }
        const new_id = this.idArray.toString();
        this.loadingService.loadingStart();
        const filter_data = 'id:' + new_id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_rpt_tiki_internal/pdf', { filterData: filter_data });
        this.selected = 0;
        this.idArray = [];
        this.loadingService.loadingStop();
    }

    Approved() {
        this.tandaTerimaService.isCek(this.selected).subscribe(
            (res) => {
                this.loadAll();
                this.loadingService.loadingStop();
                this.selected = new Array<TandaTerimaDokumenSKG>();
                alert('Sukses');
            }
        );
    }

    cancelTT(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to Cancel?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.tandaTerimaService.deleteDocument(id).subscribe((response) => {
                    this.loadAll();

                });
            }
        });
        this.loadAll();
    }

    cancelTTRCP(purchasing: TandaTerimaDokumenSKG) {
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.tandaTerima = purchasing;
        this.newModalNote1 = true;
    }

    TambahPenerima(purchasing: TandaTerimaDokumenSKG) {
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.tandaTerima = purchasing;
        this.newModalTIKI = true;
    }

    modalCancelTT() {
        if (this.noteCancelTT == null) {
            alert('Kolom Tidak Boleh Kosong');
        } else {
            this.tandaTerima.alasan_cancel = this.noteCancelTT;
            this.tandaTerimaService.changeStatusTT(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStart();
                        this.newModalNote1 = false;
                        this.noteCancelTT = null;
                        this.loadAll();
                    }
                )
        }
    }

    modalTambahPenerima() {
        if (this.reciveby === null || this.reciveby === undefined || this.reciveby === '' || this.inputEkspedisi === '') {
            alert('Kolom Tidak Boleh Kosong');
        } else {
            this.tandaTerima.no_resi = this.reciveby;
            this.tandaTerima.kurir = this.inputEkspedisi;
            this.tandaTerimaService.changePenerima(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStart();
                        this.newModalTIKI = false;
                        this.reciveby = null;
                        this.tanggal_terima = new Date();
                        this.loadAll();
                    }
                )
        }
    }

    download() {
        this.loadingService.loadingStart();
        const date = new Date();

        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();

        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|pt:' + this.inputNamaPT + '|jenis_dokumen:' + '|status_tiki:' + this.inputStatus + '|idInternal:' + this.divisi
            + '|user:' + this.currentUser + '|tiki_exe:Internal' + '|divisi_penerima:' + this.inputDivisi + '|cust_code:';
        this.reportUtilService.downloadFileWithName(`Report Tanda Terima Keluar Internal ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/report_tiki_internal/xlsx', { filterData: filter_data });
        // console.log(filter_data);
        this.loadingService.loadingStop();
    }

    onKeyDown(event: any) {
        if (event.key === 'Enter') {
            if (this.nottt === null || this.nottt === undefined || this.nottt === '') {
                alert('Kolom Tidak Boleh Kosong')
            } else {
                const noTT = this.nottt.replace(/\//g, '-');
                this.tandaTerimaService.cekTIKI(noTT).subscribe(
                    (res: TandaTerimaDokumenSKG) => {
                        if (res.iscek === 10) {
                            this.toasterService.showToaster('success', 'Success Scan', 'Paket Telah Berhasil di Scan');
                            this.nottt = '';
                        } else if (res.iscek !== 10) {
                            this.toasterService.showToaster('error', 'Error Scan', `${res.alasan_cancel}`);
                            this.nottt = '';
                        }
                    }
                )
            }
        }
    }

    backapp() {
        this.nottt = '';
        this.newModalScan = false;
        this.loadAll();
    }

    backappHistory() {
        this.newModalHistory = false;
        this.loadAll();
    }

    createKirim() {
        const allSame = this.selected.every((item) => item.iscek === 1);
        if (allSame) {
            this.confirmationService.confirm({
                message: 'Apakah anda yakin ingin update status dokumen menjadi sudah di kirim ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.tandaTerimaService.setStatusKirim(this.selected).subscribe(
                        (res) => {
                            this.loadAll();
                            this.selected = new Array<TandaTerimaDokumenSKG>();
                            alert('Sukses');
                        }
                    )
                }
            });
        } else {
            alert('Terdapat Tanda Terima Keluar Internal yang statusnya bukan sudah diperiksa operator');
        }
    }

    HistoryTT(rowData: TandaTerimaDokumenSKG) {
        this.tandaTerimaService.cekData({
            idreceipt: rowData.idreceipt
        }).subscribe(
            (res: ResponseWrapper) => {
                this.historyTT = res.json;
                this.newModalHistory = true
            })
    }
}
