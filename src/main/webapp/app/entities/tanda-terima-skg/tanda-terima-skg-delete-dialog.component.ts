import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TandaTerimaSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGPopupService } from './tanda-terima-skg-popup.service';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';

@Component({
    selector: 'jhi-tanda-terima-skg-delete-dialog',
    templateUrl: './tanda-terima-skg-delete-dialog.component.html'
})
export class TandaTerimaSKGDeleteDialogComponent {

    tandaTerima: TandaTerimaSKG;

    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tandaTerimaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tandaTerimaListModification',
                content: 'Deleted an tandaTerima'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tanda-terima-skg-delete-popup',
    template: ''
})
export class TandaTerimaSKGDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tandaTerimaPopupService: TandaTerimaSKGPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tandaTerimaPopupService
                .open(TandaTerimaSKGDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
