import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TandaTerimaSKGComponent } from './tanda-terima-skg.component';
import { TandaTerimaSKGDetailComponent } from './tanda-terima-skg-detail.component';
import { TandaTerimaSKGPopupComponent } from './tanda-terima-skg-dialog.component';
import { TandaTerimaSKGDeletePopupComponent } from './tanda-terima-skg-delete-dialog.component';
import { TandaTerimaSKGNewComponent } from './tanda-terima-new/tanda-terima-skg-new.component';
import { TandaTerimaNewSuratSKGComponent } from './tanda-terima-new/tanda-terima-new-surat-skg.component';
import { TandaTerimaSuratSKGComponent } from './tanda-terima-surat-skg.component';
import { TandaTerimaKeluarEksternalComponent } from './tanda-terima-keluar-eksternal-skg.component';
import { TandaTerimaNewKeluarEksternalComponent } from './tanda-terima-new/tanda-terima-new-keluar-eksternal.component';
import { TandaTerimaTikiSKGPrintComponent } from './tanda-terima-tiki-skg-print.component';
import { TandaTerimaKlaimSKGComponent } from './tanda-terima-klaim-skg.component';
import { TandaTerimaKlaimSKGNewComponent } from './tanda-terima-new/tanda-terima-klaim-skg-new.component';
import { TandaTerimaKeluarInternalComponent } from './tanda-terima-keluar-internal-skg.component';
import { TandaTerimaNewKeluarInternalComponent } from './tanda-terima-new/tanda-terima-new-keluar-internal.component';

@Injectable()
export class TandaTerimaSKGResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idreceipt,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const tandaTerimaSKGRoute: Routes = [
    {
        path: 'tanda-terima-skg',
        component: TandaTerimaSKGComponent,
        resolve: {
            'pagingParams': TandaTerimaSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-skg/:id',
        component: TandaTerimaSKGDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-skg-new',
        component: TandaTerimaSKGNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-skg-edit/:route/:page/:id/:isview',
        component: TandaTerimaSKGNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-new-surat-skg',
        component: TandaTerimaNewSuratSKGComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-edit-surat-skg/:route/:page/:id/:isview',
        component: TandaTerimaNewSuratSKGComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-surat-skg',
        component: TandaTerimaSuratSKGComponent,
        resolve: {
            'pagingParams': TandaTerimaSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tanda-terima-keluar-eksternal',
        component: TandaTerimaKeluarEksternalComponent,
        resolve: {
            'pagingParams': TandaTerimaSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tanda-terima-keluar-internal',
        component: TandaTerimaKeluarInternalComponent,
        resolve: {
            'pagingParams': TandaTerimaSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tanda-terima-new-keluar-internal',
        component: TandaTerimaNewKeluarInternalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tanda-terima-new-keluar-eksternal',
        component: TandaTerimaNewKeluarEksternalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-edit-keluar-internal-skg/:route/:page/:id/:isview',
        component: TandaTerimaNewKeluarInternalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-edit-keluar-eksternal-skg/:route/:page/:id/:isview',
        component: TandaTerimaNewKeluarEksternalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-tiki-skg-print',
        component: TandaTerimaTikiSKGPrintComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-klaim-skg',
        component: TandaTerimaKlaimSKGComponent,
        resolve: {
            'pagingParams': TandaTerimaSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-klaim-skg-new',
        component: TandaTerimaKlaimSKGNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tanda-terima-klaim-skg-edit/:route/:page/:id/:isview',
        component: TandaTerimaKlaimSKGNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
export const tandaTerimaSKGPopupRoute: Routes = [
    {
        path: ':route/:page/:id/edit',
        component: TandaTerimaSKGPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tanda-terima-skg/:id/delete',
        component: TandaTerimaSKGDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
