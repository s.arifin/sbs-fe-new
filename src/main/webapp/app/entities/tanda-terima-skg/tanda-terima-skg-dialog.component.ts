import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TandaTerimaSKG } from './tanda-terima-skg.model';
import { TandaTerimaSKGPopupService } from './tanda-terima-skg-popup.service';
import { TandaTerimaSKGService } from './tanda-terima-skg.service';

@Component({
    selector: 'jhi-tanda-terima-skg-dialog',
    templateUrl: './tanda-terima-skg-dialog.component.html'
})
export class TandaTerimaSKGDialogComponent {

    tandaTerima: TandaTerimaSKG;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private tandaTerimaService: TandaTerimaSKGService,
        private eventManager: JhiEventManager
    ) {
    }

    // ngOnInit() {
    //     this.isSaving = false;
    // }

    // clear() {
    //     this.activeModal.dismiss('cancel');
    // }

    // save() {
    //     this.isSaving = true;
    //     if (this.tandaTerima.id !== undefined) {
    //         this.subscribeToSaveResponse(
    //             this.tandaTerimaService.update(this.tandaTerima));
    //     } else {
    //         this.subscribeToSaveResponse(
    //             this.tandaTerimaService.create(this.tandaTerima));
    //     }
    // }

    // private subscribeToSaveResponse(result: Observable<TandaTerimaSKG>) {
    //     result.subscribe((res: TandaTerimaSKG) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    // }

    // private onSaveSuccess(result: TandaTerimaSKG) {
    //     this.eventManager.broadcast({ name: 'tandaTerimaListModification', content: 'OK'});
    //     this.isSaving = false;
    //     this.activeModal.dismiss(result);
    // }

    // private onSaveError() {
    //     this.isSaving = false;
    // }

    // private onError(error: any) {
    //     this.alertService.error(error.message, null, null);
    // }
}

@Component({
    selector: 'jhi-tanda-terima-popup',
    template: ''
})
export class TandaTerimaSKGPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tandaTerimaPopupService: TandaTerimaSKGPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tandaTerimaPopupService
                    .open(TandaTerimaSKGDialogComponent as Component, params['id']);
            } else {
                this.tandaTerimaPopupService
                    .open(TandaTerimaSKGDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
