import { DetailTandaTerimaSKG } from '../detail-tanda-terima-skg';
import { BaseEntity } from './../../shared';

export class TandaTerimaSKG implements BaseEntity {
    constructor(
        public id?: number,
        public idreceipt?: any,
        public receiptno?: string,
        public dtreceipt?: any,
        public kdsup?: string,
        public total?: number,
        public grandtotal?: number,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public items?: DetailTandaTerimaSKG[],
        public nmsupplier?: string,
        public mastercustomer?: MasterCustomer,
        public masteragen?: MasterCustomer,
        public divisi?: string,
        public revisi?: any,
        public catatan_revisi?: any,
        public cust_name2?: any,
        public cust_name?: any,
    ) {
        this.items = new Array<DetailTandaTerimaSKG>();
        // tslint:disable-next-line: no-use-before-declare
        this.mastercustomer = new MasterCustomer();
        // tslint:disable-next-line: no-use-before-declare
        this.masteragen = new MasterCustomer();
    }
}
export class MasterCustomer {
    constructor(
        public id?: number,
        public cust_code?: any,
        public cust_name?: string,
        public cust_addr?: string,
        public cust_addr2?: string,
        public cust_addr3?: string,
        public cust_nick?: string
    ) {
    }
}

export class TandaTerimaDokumenSKG implements BaseEntity {
    constructor(
        public id?: number,
        public idreceipt?: any,
        public receiptno?: string,
        public dtreceipt?: any,
        public nama_pengirim?: string,
        public nama_pt?: string,
        public alamat?: any,
        public jenis_layanan?: any,
        public jumlah_dokumen?: any,
        public jenis_dokumen?: any,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public iscek?: number,
        public isprint?: number,
        public div?: string,
        public dtrecive?: any,
        public dtcancel?: any,
        public alasan_cancel?: string,
        public cust_code?: string,
        public alamat_tujuan?: string,
        public is_agen?: string,
        public nmsupplier?: string,
        public mastercustomer?: MasterCustomer,
        public reciveby?: string,
        public tanggal_terima?: Date,
        public nama_penerima?: string,
        public nama_penerima2?: string,
        public no_telp?: string,
        public statustype?: number,
        public nm_status?: string,
        public divisi_internal?: string,
        public tiki_inex?: string,
        public no_resi?: string,
        public kurir?: string,
        public asuransi?: string,
        public nominal_asuransi?: number
    ) {
        this.mastercustomer = new MasterCustomer();
    }
}

export class TandaTerimaDokumenStatus implements BaseEntity {
    constructor(
        public id?: number,
        public idstatus?: any,
        public idreceipt?: any,
        public statustype?: number,
        public dtfrom?: any,
        public dtthru?: any,
    ) {
    }
}

export class TandaTerimaSuratSKG implements BaseEntity {
    constructor(
        public id?: number,
        public idreceipt?: any,
        public receiptno?: string,
        public dtreceipt?: any,
        public receiver?: string,
        public sender?: string,
        public doctype?: any,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public notes?: string,
        public divisi?: string,
        public is_sent?: any
    ) {
    }
}

export class TandaTerimaKlaimSKG implements BaseEntity {
    constructor(
        public id?: number,
        public idreceipt?: any,
        public receiptno?: string,
        public dtreceipt?: any,
        public kdsup?: string,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public cust_code?: string,
        public cust_name?: string,
        public jumlah_surat?: number,
        public items?: DetailTandaTerimaKlaimSKG[],
        public nosrtskg?: string
    ) {
        this.items = new Array<DetailTandaTerimaKlaimSKG>();
        // tslint:disable-next-line: no-use-before-declare
        // this.mastercustomer = new MasterCustomer();
        // // tslint:disable-next-line: no-use-before-declare
        // this.masteragen = new MasterCustomer();
    }
}

export class DetailTandaTerimaKlaimSKG implements BaseEntity {
    constructor(
        public id?: number,
        public iddetreceipt?: any,
        public idreceipt?: any,
        public nosrtskg?: string,
        public kdprogramX?: any,
        public noklaimDist?: string,
        public tglKlaimDist?: Date,
        public kurir?: string,
        public notes?: string,
        public nourut?: number,
        public is_use?: string,
        public cust_code?: string,
        public cust_name?: string,
        public UserPICklaim?: string,
        public StsUpdate?: string,
        public nomorUrut?: string,
        public noUrSurat?: string,
    ) {
        // tslint:disable-next-line: no-use-before-declare
        // this.mastercustomer = new MasterCustomer();
        // // tslint:disable-next-line: no-use-before-declare
        // this.masteragen = new MasterCustomer();
    }
}
