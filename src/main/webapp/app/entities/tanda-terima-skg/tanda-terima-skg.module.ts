import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    TandaTerimaSKGService,
    TandaTerimaSKGPopupService,
    TandaTerimaSKGComponent,
    TandaTerimaSKGDetailComponent,
    TandaTerimaSKGDialogComponent,
    TandaTerimaSKGPopupComponent,
    TandaTerimaSKGDeletePopupComponent,
    TandaTerimaSKGDeleteDialogComponent,
    tandaTerimaSKGRoute,
    tandaTerimaSKGPopupRoute,
    TandaTerimaSKGResolvePagingParams,
    TandaTerimaKlaimSKGComponent,
} from '.';
import { TandaTerimaSKGNewComponent } from './tanda-terima-new/tanda-terima-skg-new.component';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    DataListModule,
    MultiSelectModule,
} from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TandaTerimaNewSuratSKGComponent } from './tanda-terima-new/tanda-terima-new-surat-skg.component';
import { TandaTerimaSuratSKGComponent } from './tanda-terima-surat-skg.component';
import { TandaTerimaKeluarEksternalComponent } from './tanda-terima-keluar-eksternal-skg.component';
import { TandaTerimaNewKeluarEksternalComponent } from './tanda-terima-new/tanda-terima-new-keluar-eksternal.component';
import { TandaTerimaTikiSKGPrintComponent } from './tanda-terima-tiki-skg-print.component';
import { TandaTerimaKlaimSKGNewComponent } from './tanda-terima-new/tanda-terima-klaim-skg-new.component';
import { HttpClientModule } from '@angular/common/http';  // Pastikan ini diimpor
import { TandaTerimaKeluarInternalComponent } from './tanda-terima-keluar-internal-skg.component';
import { TandaTerimaNewKeluarInternalComponent } from './tanda-terima-new/tanda-terima-new-keluar-internal.component';

const ENTITY_STATES = [
    ...tandaTerimaSKGRoute,
    ...tandaTerimaSKGPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        DataListModule,
        HttpClientModule,
        MultiSelectModule
    ],
    declarations: [
        TandaTerimaSKGComponent,
        TandaTerimaSKGDetailComponent,
        TandaTerimaSKGDialogComponent,
        TandaTerimaSKGDeleteDialogComponent,
        TandaTerimaSKGPopupComponent,
        TandaTerimaSKGDeletePopupComponent,
        TandaTerimaSKGNewComponent,
        TandaTerimaNewSuratSKGComponent,
        TandaTerimaSuratSKGComponent,
        TandaTerimaKeluarEksternalComponent,
        TandaTerimaNewKeluarEksternalComponent,
        TandaTerimaTikiSKGPrintComponent,
        TandaTerimaKlaimSKGComponent,
        TandaTerimaKlaimSKGNewComponent,
        TandaTerimaKeluarInternalComponent,
        TandaTerimaNewKeluarInternalComponent
    ],
    entryComponents: [
        TandaTerimaSKGComponent,
        TandaTerimaSKGDialogComponent,
        TandaTerimaSKGPopupComponent,
        TandaTerimaSKGDeleteDialogComponent,
        TandaTerimaSKGDeletePopupComponent,
        TandaTerimaSKGNewComponent,
        TandaTerimaSuratSKGComponent,
        TandaTerimaKeluarEksternalComponent,
        TandaTerimaTikiSKGPrintComponent,
        TandaTerimaKlaimSKGComponent,
        TandaTerimaKlaimSKGNewComponent,
        TandaTerimaKeluarInternalComponent,
        TandaTerimaNewKeluarInternalComponent,
        TandaTerimaNewKeluarEksternalComponent
    ],
    providers: [
        TandaTerimaSKGService,
        TandaTerimaSKGPopupService,
        TandaTerimaSKGResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmTandaTerimaSKGModule { }
