import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { DetailTandaTerimaKlaimSKG, MasterCustomer, TandaTerimaKlaimSKG, TandaTerimaSKG } from '../tanda-terima-skg.model';
import { TandaTerimaSKGService } from '../tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { DetailTandaTerimaSKG, DetailTandaTerimaSKGService } from '../../detail-tanda-terima-skg';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ConfirmationService, ConfirmDialog } from 'primeng/primeng';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { TandaTerimaDocument } from '../../tanda-terima-document';
import { LoadingService } from '../../../layouts';
import { HttpClient } from '@angular/common/http';  // Pastikan mengimpor HttpClient

@Component({
    selector: 'jhi-tanda-terima-klaim-skg-new',
    templateUrl: './tanda-terima-klaim-skg-new.component.html'
})
export class TandaTerimaKlaimSKGNewComponent implements OnInit, OnDestroy {
    [x: string]: any;
    tandaTerima: TandaTerimaKlaimSKG;
    item: DetailTandaTerimaKlaimSKG;
    items: DetailTandaTerimaKlaimSKG[];
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterCustomer;
    subscription: Subscription;
    isview: boolean;
    ttdtreceipt: Date;
    modalLampiran: boolean;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    document: TandaTerimaDocument;
    nott: string;
    paramPage: number;
    routeId: number;
    stsKlaim: string
    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        private router: Router,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        protected dataUtils: JhiDataUtils,
        protected detailTandaTerimaService: DetailTandaTerimaSKGService,
        private loadingService: LoadingService,
        private toasterService: ToasterService,
        private httpClient: HttpClient,  // Pastikan HttpClient di-inject
    ) {
        this.tandaTerima = new TandaTerimaKlaimSKG();
        this.item = new DetailTandaTerimaKlaimSKG();
        this.items = new Array<DetailTandaTerimaKlaimSKG>();
        this.newSuplier = new MasterCustomer();
        this.isview = false
        this.ttdtreceipt = new Date();
        this.modalLampiran = false;
        this.isInsertFile = null;
        this.isValidImage = false;
        this.routeId = 0;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.document = new TandaTerimaDocument();
        this.nott = 'XXXXXX/XXX/XX/XXXX';
        this.stsKlaim = '18';
    }
    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                }
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
    }
    load(id) {
        this.tandaTerimaService.findKlaim(id).subscribe(
            (res: TandaTerimaKlaimSKG) => {
                this.tandaTerima = res;
                this.nott = res.receiptno;
                this.items = res.items;
                this.item.cust_name = res.cust_name;
                this.items.forEach((x) => x.cust_name = res.cust_name);
            }
        )
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    tambahBaris() {
        if (this.item.kdprogramX === null || this.item.kdprogramX === undefined || this.item.kdprogramX === '') {
            alert('Kode Program Surat Tidak Boleh Kosong');
        } else if (this.item.nosrtskg === null || this.item.nosrtskg === undefined || this.item.nosrtskg === '') {
            alert('Nomor Surat Tidak Boleh Kosong');
        } else if (this.item.UserPICklaim === null || this.item.UserPICklaim === undefined || this.item.UserPICklaim === '') {
            alert('PIC Admin Klaim Tidak Boleh Kosong');
        } else if (this.item.cust_code === null || this.item.cust_code === undefined || this.item.cust_code === '') {
            alert('Distributor Tidak Boleh Kosong');
        } else if (this.item.noklaimDist === null || this.item.noklaimDist === undefined || this.item.noklaimDist === '') {
            alert('No Klaim Distributor Tidak Boleh Kosong');
        } else if (this.item.tglKlaimDist === null || this.item.tglKlaimDist === undefined) {
            alert('Tanggal Klaim Distributor Tidak Boleh Kosong');
        } else if (this.item.kurir === null || this.item.kurir === undefined || this.item.kurir === '') {
            alert('Kurir Tidak Boleh Kosong');
        } else {
            if (this.items.length < 1) {
                this.item.nourut = this.items.length + 1;
                this.items = [... this.items, this.item];
                this.item = new DetailTandaTerimaKlaimSKG();
            } else {
                if (this.items[0].cust_code !== this.item.cust_code) {
                    alert('Tidak Boleh Menambahkan Surat Jika Berbeda Distributor');
                    this.item = new DetailTandaTerimaKlaimSKG();
                } else {
                    this.item.nourut = this.items.length + 1;
                    this.items = [... this.items, this.item];
                    this.item = new DetailTandaTerimaKlaimSKG();
                }
            }
        }
        // this.item = new DetailTandaTerimaSKG();
    }
    savett() {
        if (this.items.length < 1) {
            alert('Item Tidak Boleh Kosong Ketika Ingin Di Simpan Datanya');
        } else {
            this.tandaTerima.cust_code = this.items[0].cust_code;
            this.tandaTerima.items = this.items;
            this.loadingService.loadingStart();
            this.tandaTerimaService.createTTK(this.tandaTerima)
                .subscribe(
                    (res) => {
                        // Memanggil link eksternal (API atau URL lain)
                        const externalLink = `https://order.skg.co.id/welcome_new/getUpdateKlaim/${this.tandaTerima.cust_code}`;

                        // Gunakan HttpClient untuk memanggil URL eksternal
                        this.httpClient.get(externalLink).subscribe(
                            (externalRes) => {
                                // Setelah berhasil memanggil URL eksternal, pindah ke halaman internal
                                this.loadingService.loadingStop();
                                this.router.navigate(['../tanda-terima-klaim-skg']);
                            },
                            (externalErr) => {
                                // Menangani error dari request eksternal
                                this.loadingService.loadingStop();
                                console.error('Error accessing external link:', externalErr);

                                // Pindah ke halaman internal meskipun ada error pada eksternal
                                this.router.navigate(['../tanda-terima-klaim-skg']);
                            }
                        );
                    },
                    (err: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                    }
                )
        }
        // if (this.tandaTerima.nomor === undefined || this.tandaTerima.nomor === null) {
        //     alert('Data Tidak Lengkap');
        // } else {
        //     this.loadingService.loadingStart();
        //     this.tandaTerimaService.createTTK(this.tandaTerima)
        //         .subscribe(
        //             (res) => {
        //                 this.loadingService.loadingStop();
        //                 this.router.navigate(['../tanda-terima-klaim-skg']);
        //             },
        //             (err: ResponseWrapper) => {
        //                 this.loadingService.loadingStop();
        //             }
        //         )
        // }
    }
    backMainPage() {
        if (this.routeId === 0) {
            this.router.navigate(['tanda-terima-klaim-skg', { page: this.paramPage }]);
        }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributorALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }

        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributorALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        // if (isSelect === true) {
        //     this.tandaTerima.kdsup = this.newSuplier.cust_code;
        // } else {
        //     this.tandaTerima.kdsup = null;
        // }
    }
    ceknott() {
        // this.tandaTerimaService.cekNoTT(this.tandaTerima.receiptno)
        //     .subscribe(
        //         (res: ResponseWrapper) => {
        //             if (res.status === 200) {
        //                 this.confirmationService.confirm({
        //                     header: 'Information',
        //                     message: 'nomor tanda terima sudah ada',
        //                     rejectVisible: false,
        //                     accept: () => {
        //                         this.tandaTerima.receiptno = ''
        //                     }
        //                 });
        //             }
        //             if (res.status === 404) {

        //             }
        //         }
        //     )
    }
    addAtt(rowData: DetailTandaTerimaSKG) {
        this.modalLampiran = true;
        this.item = rowData;
        this.document = new TandaTerimaDocument();
    }
    public deleteListArray(rowData: DetailTandaTerimaKlaimSKG) {
        if (this.items.length < 1) {
            alert('Item Tidak Boleh Kosong');
        } else {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Apakah Anda yakin akan menghapus data ini ??',
                accept: () => {
                    if (rowData.iddetreceipt === undefined || rowData.iddetreceipt === null || rowData.iddetreceipt === '') {
                        this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                        console.log('items length == ' + this.items.length);
                        if (this.items.length <= 0) {
                        };
                    } else {
                        this.tandaTerimaService.delete(rowData.iddetreceipt).subscribe(
                            (res) => {
                                this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                                console.log('items length == ' + this.items.length);
                                if (this.items.length <= 0) {
                                };
                            }
                        )
                    }
                    this.items = this.setNoUrut(this.items);
                }
            });
        }
    }
    setNoUrut(items: DetailTandaTerimaKlaimSKG[]): DetailTandaTerimaKlaimSKG[] {
        let urut = 1;
        this.items.forEach(
            (e) => {
                e.nourut = urut;
                urut++;
            }
        );
        return this.items;
    }
    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
        } else {
            this.isValidImage = true;
            this.clearInputImage('doc', 'doctype', 'fileImage');
            this.isInsertFile = null;
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.item, this.elementRef, field, fieldContentType, idInput);
    }
    // setImange() {
    //     this.modalLampiran = false;
    //     this.item.document = this.document;
    // }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    onKeyUp(x) { // appending the updated value to the variable
        // this.tandaTerima.cust_name2 = x.target.value;
    }

    onKeyDown(event: any) {
        if (event.key === 'Enter') {
            const kdprogramX = this.item.kdprogramX;
            this.item.cust_code = null;
            this.item.cust_name = null;
            this.item.nosrtskg = null;
            this.item.UserPICklaim = null;
            this.item.noklaimDist = null;
            this.item.tglKlaimDist = null;
            this.item.kurir = null;
            this.item.notes = null;
            this.tandaTerimaService.cekTTK(kdprogramX).subscribe(
                (res: DetailTandaTerimaKlaimSKG) => {
                    if (res === null || res === undefined) {
                        alert('Kode Program Surat Tidak Ditemukan')
                        this.item = new DetailTandaTerimaKlaimSKG;
                        this.item.nosrtskg = null;
                        this.item.cust_code = null;
                        this.item.cust_name = null;
                        this.item.UserPICklaim = null;
                        this.item.kurir = null;
                        this.item.noklaimDist = null;
                        this.item.tglKlaimDist = null;
                        this.stsKlaim = '0'
                    } else {
                        if (res.StsUpdate === '1.0') {
                            alert('Surat Klaim Belum Di Kirim Oleh Distributor');
                            this.item = new DetailTandaTerimaKlaimSKG;
                            this.item.nosrtskg = null;
                            this.item.cust_code = null;
                            this.item.cust_name = null;
                            this.item.UserPICklaim = null;
                            this.item.kurir = null;
                            this.item.noklaimDist = null;
                            this.item.tglKlaimDist = null;
                            this.stsKlaim = '1.0'
                        } else if (res.iddetreceipt !== null) {
                            alert('Surat Klaim Sudah Di Register');
                            this.item = new DetailTandaTerimaKlaimSKG;
                            this.item.nosrtskg = null;
                            this.item.cust_code = null;
                            this.item.cust_name = null;
                            this.item.UserPICklaim = null;
                            this.item.kurir = null;
                            this.item.noklaimDist = null;
                            this.item.tglKlaimDist = null;
                            this.stsKlaim = '3.0'
                        } else if (res.StsUpdate === '2.0') {
                            this.item = res;
                            this.item.tglKlaimDist = new Date(res.tglKlaimDist);
                            this.stsKlaim = '3.0';
                        } else if (res.StsUpdate === '4.1') {
                            alert('Surat Klaim Pending, Tidak Bisa Input Sebelum Di Revisi');
                            this.item = new DetailTandaTerimaKlaimSKG;
                            this.item.nosrtskg = null;
                            this.item.cust_code = null;
                            this.item.cust_name = null;
                            this.item.UserPICklaim = null;
                            this.item.kurir = null;
                            this.item.noklaimDist = null;
                            this.item.tglKlaimDist = null;
                            this.stsKlaim = '4.1'
                        }
                    }
                }
            )
        }
    }

    formatDate(dateString: string): string {
        return dateString ? dateString.split('T')[0] : ''; // Hanya mengambil bagian tanggal
    }
}
