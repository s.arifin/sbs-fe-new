import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { MasterCustomer, TandaTerimaSKG } from '../tanda-terima-skg.model';
import { TandaTerimaSKGService } from '../tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { DetailTandaTerimaSKG, DetailTandaTerimaSKGService } from '../../detail-tanda-terima-skg';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ConfirmationService, ConfirmDialog } from 'primeng/primeng';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { TandaTerimaDocument } from '../../tanda-terima-document';
import { LoadingService } from '../../../layouts';

@Component({
    selector: 'jhi-tanda-terima-skg-new',
    templateUrl: './tanda-terima-skg-new.component.html'
})
export class TandaTerimaSKGNewComponent implements OnInit, OnDestroy {
    [x: string]: any;
    tandaTerima: TandaTerimaSKG;
    item: DetailTandaTerimaSKG;
    items: DetailTandaTerimaSKG[];
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterCustomer;
    subscription: Subscription;
    isview: boolean;
    ttdtreceipt: Date;
    modalLampiran: boolean;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    document: TandaTerimaDocument;
    nott: string;
    paramPage: number;
    routeId: number;
    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        private router: Router,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        protected dataUtils: JhiDataUtils,
        protected detailTandaTerimaService: DetailTandaTerimaSKGService,
        private loadingService: LoadingService,
        private toasterService: ToasterService,
    ) {
        this.tandaTerima = new TandaTerimaSKG();
        this.item = new DetailTandaTerimaSKG();
        this.items = new Array<DetailTandaTerimaSKG>();
        this.newSuplier = new MasterCustomer();
        this.isview = false
        this.ttdtreceipt = new Date();
        this.modalLampiran = false;
        this.isInsertFile = null;
        this.isValidImage = false;
        this.routeId = 0;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.document = new TandaTerimaDocument();
        this.nott = 'XXXXXX/XXX/XX/XXXX';
    }
    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                }
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
    }
    load(id) {
        this.tandaTerimaService.find(id).subscribe(
            (res: TandaTerimaSKG) => {
                this.tandaTerima = res;
                this.ttdtreceipt = new Date(res.dtreceipt);
                this.items = res.items;
                this.items.forEach(
                    (e) => {
                        e.dtinv = new Date(e.dtinv);
                        if (e.dtinvtax === null) {
                        } else {
                            e.dtinvtax = new Date(e.dtinvtax);
                        }
                    }
                )
                this.newSuplier = res.mastercustomer;
                this.nott = res.receiptno
                this.newSuplier.cust_name = res.nmsupplier;
            }
        )
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    tambahBaris() {
        if (this.items.length >= 5) {
            alert('Maksimal hanya bisa menambahkan 5 detail dalam 1 tanda terima');
        } else {
            this.item = new DetailTandaTerimaSKG();
            this.item.nourut = this.items.length + 1;
            this.items = [... this.items, this.item];
        }
    }
    savett() {
        this.tandaTerima.items = this.items;
        this.tandaTerima.dtreceipt = this.ttdtreceipt;
        if (this.tandaTerima.idreceipt === undefined) {
            this.loadingService.loadingStart();
            this.tandaTerimaService.create(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        this.router.navigate(['../tanda-terima-skg']);
                    },
                    (err: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                    }
                )
        } else {
            this.loadingService.loadingStart();
            this.tandaTerimaService.update(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        console.log('sukses');
                        this.router.navigate(['../tanda-terima-skg']);
                    },
                    (err: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                    }
                )
        }

    }
    backMainPage() {
        if (this.routeId === 0) {
            this.router.navigate(['tanda-terima-skg', { page: this.paramPage }]);
        }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributorALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }

        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributorALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        if (isSelect === true) {
            this.tandaTerima.kdsup = this.newSuplier.cust_code;
        } else {
            this.tandaTerima.kdsup = null;
        }
    }
    ceknott() {
        this.tandaTerimaService.cekNoTT(this.tandaTerima.receiptno)
            .subscribe(
                (res: ResponseWrapper) => {
                    if (res.status === 200) {
                        this.confirmationService.confirm({
                            header: 'Information',
                            message: 'nomor tanda terima sudah ada',
                            rejectVisible: false,
                            accept: () => {
                                this.tandaTerima.receiptno = ''
                            }
                        });
                    }
                    if (res.status === 404) {

                    }
                }
            )
    }
    addAtt(rowData: DetailTandaTerimaSKG) {
        this.modalLampiran = true;
        this.item = rowData;
        this.document = new TandaTerimaDocument();
    }
    public deleteListArray(rowData: DetailTandaTerimaSKG) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                if (rowData.iddetreceipt === undefined && rowData.iddetreceipt !== null) {
                    this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                    console.log('items length == ' + this.items.length);
                    if (this.items.length <= 0) {
                    };
                } else {
                    this.tandaTerimaService.deleteItem(rowData.iddetreceipt).subscribe(
                        (res) => {
                            this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                            console.log('items length == ' + this.items.length);
                            if (this.items.length <= 0) {
                            };
                        }
                    )
                }
                this.items = this.setNoUrut(this.items);
            }
        });
    }
    setNoUrut(items: DetailTandaTerimaSKG[]): DetailTandaTerimaSKG[] {
        let urut = 1;
        this.items.forEach(
            (e) => {
                e.nourut = urut;
                urut++;
            }
        );
        return this.items;
    }
    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
        } else {
            this.isValidImage = true;
            this.clearInputImage('doc', 'doctype', 'fileImage');
            this.isInsertFile = null;
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.item, this.elementRef, field, fieldContentType, idInput);
    }
    setImange() {
        this.modalLampiran = false;
        this.item.document = this.document;
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    onKeyUp(x) { // appending the updated value to the variable
        this.tandaTerima.cust_name2 = x.target.value;
    }
}
