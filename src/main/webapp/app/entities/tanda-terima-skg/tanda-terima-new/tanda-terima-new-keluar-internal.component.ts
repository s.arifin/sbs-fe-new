import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { TandaTerimaDokumenSKG, MasterCustomer } from '../tanda-terima-skg.model';
import { TandaTerimaSKGService } from '../tanda-terima-skg.service';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { DetailTandaTerimaSKG, DetailTandaTerimaSKGService } from '../../detail-tanda-terima-skg';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ConfirmationService } from 'primeng/primeng';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { TandaTerimaDocument } from '../../tanda-terima-document';
import { LoadingService } from '../../../layouts';
import { ToasterService } from '../../../shared';

@Component({
    selector: 'jhi-tanda-terima-new-keluar-internal',
    templateUrl: './tanda-terima-new-keluar-internal.component.html'
})
export class TandaTerimaNewKeluarInternalComponent implements OnInit, OnDestroy {
    [x: string]: any;
    tandaTerima: TandaTerimaDokumenSKG;
    item: DetailTandaTerimaSKG;
    items: DetailTandaTerimaSKG[];
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterCustomer;
    subscription: Subscription;
    isview: boolean;
    ttdtreceipt: Date;
    modalLampiran: boolean;
    inputNamaJenis: string;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    document: TandaTerimaDocument;
    nott: string;
    nama_pengirim: string;
    alamat: string;
    inputNamaPT: any;
    inputJenisdokumen: any;
    inputJenislayanan: any;
    jumlah_dokumen: any;
    is_agen: any;
    cust_name: any;
    alamat_tujuan: any;
    cust_code: any;
    suplier: any;
    nama_penerima: any;
    nama_penerima2: any;
    no_telp: any;
    inputDivisi: any;
    brandSelect: any[] = [];
    nama_pt: Array<object> = [
        { label: 'Pilih Group', value: '' },
        { label: 'SKG', value: 'SKG' },
        { label: 'SBS', value: 'SBS' },
        { label: 'ACA', value: 'ACA' },
        { label: 'SMK', value: 'SMK' },
        { label: 'Others', value: 'Others' },
    ];
    divisi: Array<object> = [
        { label: 'Pilih Divisi', value: '' },
    ]

    jenis_exit: Array<object> = [
        { label: 'Pilih Jenis Pengiriman', value: '' },
        { label: 'Internal', value: 'Internal' },
        { label: 'Eksternal', value: 'Eksternal' },
    ];

    jenis: Array<object> = [
        { label: 'Pilih Jenis Layanan', value: '' },
        { label: 'REG', value: 'REG' },
        { label: 'ONS', value: 'ONS' }
    ];
    jenis_dokumen: Array<object> = [
        { label: 'Pilih Jenis Dokumen', value: '' },
        { label: 'Dokumen', value: 'Dokumen' },
        { label: 'Paket', value: 'Paket' }
    ];
    agen: Array<object> = [
        { label: 'Iya', value: 'Iya' },
        { label: 'Tidak', value: 'Tidak' }
    ];

    user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
    paramPage: number;
    routeId: number;
    currentAccount: any;
    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        private router: Router,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        protected dataUtils: JhiDataUtils,
        protected detailTandaTerimaService: DetailTandaTerimaSKGService,
        private loadingService: LoadingService,
        protected toaster: ToasterService,
        private principal: Principal
    ) {
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.isview = false
        this.routeId = 0;
        this.ttdtreceipt = new Date();
        this.modalLampiran = false;
        this.isInsertFile = null;
        this.isValidImage = false;
        this.is_agen = ''
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.document = new TandaTerimaDocument();
        this.nott = 'XXXXXX/XXX/XX/XXXX';
        this.inputNamaJenis = undefined;
        this.inputDivisi = undefined;
        this.suplier = undefined;
        this.alamat = undefined;
        this.cust_code = undefined;
        // this.nama_pengirim = this.principal.getUserLogin();
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.tandaTerima.nama_pt = '';
        this.tandaTerima.divisi_internal = '';
        this.tandaTerima.jenis_dokumen = '';
        this.tandaTerima.jenis_layanan = '';
        this.tandaTerima.tiki_inex = '';
        this.tandaTerima.nama_pengirim = this.principal.getUserLogin();
        this.tandaTerima.nama_penerima = '';
        this.tandaTerima.nama_penerima2 = '';
        this.tandaTerima.jumlah_dokumen = 0;
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                }
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
    }
    load(id) {
        this.tandaTerimaService.findDocument(id).subscribe(
            (res: TandaTerimaDokumenSKG) => {
                this.tandaTerima = res;
                this.tandaTerima.dtreceipt = new Date(res.dtreceipt);
                this.nott = res.receiptno;
                if (this.tandaTerima.nama_pt !== 'ACA' && this.tandaTerima.nama_pt !== 'Others' && this.tandaTerima.nama_pt !== 'SMK') {
                    this.tandaTerimaService.getAutority({ // OTORISASI KE 1
                        page: 0,
                        size: 10000,
                        sort: ['kdBahan', 'asc'],
                        query: 'query:' + this.tandaTerima.nama_pt
                    }).subscribe((res5: ResponseWrapper) => {
                        this.newKdBarang = res5.json;
                        this.newKdBarang.forEach((element) => {
                            this.divisi.push({
                                label: element.createdBy,
                                value: element.createdBy
                            });
                        });

                    },
                        (res5: ResponseWrapper) => {
                            this.commontUtilService.showError(res5.json);
                        }
                    );
                } else {

                }
                this.user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
                if (res.divisi_internal !== '' || res.divisi_internal != null) {
                    this.tandaTerimaService.getAutorityUser({
                        page: 0,
                        size: 10000,
                        sort: ['kdBahan', 'asc'],
                        query1: 'idInternal:' + this.tandaTerima.divisi_internal
                    }).subscribe(
                        (res2: ResponseWrapper) => {
                            // Ambil data user_divisi dari API
                            this.newKdBarang1 = res2.json;
                            this.newKdBarang1.forEach((element) => {
                                this.user_divisi.push({
                                    label: element.createdBy,
                                    value: element.createdBy
                                });
                            });
                        },
                        (res2: ResponseWrapper) => {
                            this.commontUtilService.showError(res2.json);
                        }
                    );

                }
            }
        )
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    tambahBaris() {
        this.item = new DetailTandaTerimaSKG();
        this.item.nourut = this.items.length + 1;
        this.items = [... this.items, this.item];
    }
    savett() {
        this.loadingService.loadingStart();
        if (this.tandaTerima.jumlah_dokumen === null || this.tandaTerima.jumlah_dokumen === undefined || this.tandaTerima.jumlah_dokumen === '') {
            this.tandaTerima.jumlah_dokumen = 0;
        }
        if (this.tandaTerima.alamat === '' || this.tandaTerima.alamat === undefined || this.tandaTerima.alamat === null) {
            this.toaster.showToaster('info', 'Save', 'Alamat Penerima Harus di Isi');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.nama_pt === '') {
            this.toaster.showToaster('info', 'Save', 'Perusahaan Harus di Pilih');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.nama_pengirim === '' || this.tandaTerima.nama_pengirim === undefined || this.tandaTerima.nama_pengirim === null) {
            this.toaster.showToaster('info', 'Save', 'Nama Pengirim Harus di Isi');
            this.loadingService.loadingStop();
        } else if ((this.tandaTerima.nama_pt !== 'ACA' && this.tandaTerima.nama_pt !== 'SMK' && this.tandaTerima.nama_pt !== 'Others') && this.tandaTerima.divisi_internal === '') {
            this.toaster.showToaster('info', 'Save', 'Divisi Penerima Harus di Pilih');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.nama_penerima2 === '' || this.tandaTerima.nama_penerima2 === undefined || this.tandaTerima.nama_penerima2 === null) {
            this.toaster.showToaster('info', 'Save', 'Nama Penerima Harus di Pilih');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.jenis_dokumen === '' || this.tandaTerima.jenis_dokumen === undefined || this.tandaTerima.jenis_dokumen === null) {
            this.toaster.showToaster('info', 'Save', 'Jenis Dokumen Harus di isi');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.jumlah_dokumen < 1) {
            this.toaster.showToaster('info', 'Save', 'Jumlah Dokumen Tidak Boleh 0');
            this.loadingService.loadingStop();
        } else {
            if (this.tandaTerima.idreceipt === undefined) {
                this.tandaTerima.tiki_inex = 'Internal';
                this.tandaTerimaService.createDocument(this.tandaTerima)
                    .subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            console.log('sukses');
                            this.router.navigate(['../tanda-terima-keluar-internal']);
                        }
                    )
            } else {
                this.tandaTerimaService.updateDocument(this.tandaTerima)
                    .subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            console.log('sukses');
                            this.router.navigate(['../tanda-terima-keluar-internal']);
                        }
                    )
            }
        }
    }
    backMainPage() {
        if (this.routeId === 0) {
            this.router.navigate(['tanda-terima-keluar-internal', { page: this.paramPage }]);
        }
    }

    ceknott() {
        this.tandaTerimaService.cekNoTT(this.tandaTerima.receiptno)
            .subscribe(
                (res: ResponseWrapper) => {
                    // console.log(' hasil dari JSON = ', res.json);
                    // console.log(' hasil dari Status = ', res.status);
                    // console.log(' hasil dari Headers = ', res.headers);
                    if (res.status === 200) {
                        this.confirmationService.confirm({
                            header: 'Information',
                            message: 'nomor tanda terima sudah ada',
                            rejectVisible: false,
                            accept: () => {
                                this.tandaTerima.receiptno = ''
                            }
                        });
                    }
                    if (res.status === 404) {

                    }
                }
            )
    }
    addAtt(rowData: DetailTandaTerimaSKG) {
        this.modalLampiran = true;
        this.item = rowData;
        this.document = new TandaTerimaDocument();
    }
    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
            // console.log('entity == ', entity);
            // console.log('field === ', field);
            // console.log('content', this.document.doc);
            // console.log('contenttype', this.document.doctype);
        } else {
            this.isValidImage = true;
            this.clearInputImage('doc', 'doctype', 'fileImage');
            this.isInsertFile = null;
            console.log('masuk kbesaran');
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.item, this.elementRef, field, fieldContentType, idInput);
    }
    setImange() {
        this.modalLampiran = false;
        this.item.document = this.document;
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    public deleteListArray(rowData: DetailTandaTerimaSKG) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                if (rowData.iddetreceipt === undefined && rowData.iddetreceipt !== null) {
                    this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                    console.log('items length == ' + this.items.length);
                    if (this.items.length <= 0) {
                    };
                } else {
                    this.detailTandaTerimaService.delete(rowData.iddetreceipt).subscribe(
                        (res) => {
                            this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                            console.log('items length == ' + this.items.length);
                            if (this.items.length <= 0) {
                            };
                        }
                    )
                }
                this.items = this.setNoUrut(this.items);
            }
        });
    }
    setNoUrut(items: DetailTandaTerimaSKG[]): DetailTandaTerimaSKG[] {
        let urut = 1;
        this.items.forEach(
            (e) => {
                e.nourut = urut;
                urut++;
            }
        );
        return this.items;
    }

    filterSupplierSingle(event) {
        const query = event.query;
        if (this.tandaTerima.nama_pt === 'SKG') {
            this.tandaTerimaService.getDistributor().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        } else if (this.tandaTerima.nama_pt === 'SBS') {
            this.tandaTerimaService.getDistributorSBS().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        } else {
            this.tandaTerimaService.getDistributor().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        }
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    public selectSupplier(isSelect?: boolean): void {
        if (isSelect === true) {
            this.tandaTerima.nama_penerima = this.newSuplier.cust_name;
            this.tandaTerima.alamat = this.newSuplier.cust_addr;
            this.tandaTerima.cust_code = this.newSuplier.cust_code;
        } else {
            this.tandaTerima.nama_penerima = '';
            this.tandaTerima.alamat = '';
            this.tandaTerima.cust_code = '';
        }
    }

    onKeyUp(x) { // appending the updated value to the variable
        this.tandaTerima.nama_penerima = x.target.value;
    }

    setNull() {
        this.tandaTerima.divisi_internal = '';
        this.user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
        this.brandSelect = [];
        this.tandaTerima.nama_penerima = '';
        this.tandaTerima.nama_penerima2 = '';
        this.tandaTerima.cust_code = '';
        this.tandaTerima.alamat = '';
        this.tandaTerima.divisi_internal = '';
    }

    nullPenerima() {
        this.tandaTerima.alamat = '';
        this.tandaTerima.cust_code = '';
        this.tandaTerima.nama_penerima = '';
        this.tandaTerima.nama_penerima2 = '';
        this.tandaTerima.alamat = '';
        this.newSuplier = new MasterSupplier();
        this.divisi = [
            { label: 'Pilih Divisi', value: '' }
        ];
        if (this.tandaTerima.nama_pt === '') {
            alert('Group Penerima Harus di Pilih Untuk Memunculkan Divisi Penerima dan Nama Penerima Paket');
        } else {
            if (this.tandaTerima.nama_pt !== 'ACA' && this.tandaTerima.nama_pt !== 'Others' && this.tandaTerima.nama_pt !== 'SMK') {
                this.tandaTerimaService.getAutority({ // OTORISASI KE 1
                    page: 0,
                    size: 10000,
                    sort: ['kdBahan', 'asc'],
                    query: 'query:' + this.tandaTerima.nama_pt
                }).subscribe((res: ResponseWrapper) => {
                    this.newKdBarang = res.json;
                    this.newKdBarang.forEach((element) => {
                        this.divisi.push({
                            label: element.createdBy,
                            value: element.createdBy
                        });
                    });

                },
                    (res: ResponseWrapper) => {
                        this.commontUtilService.showError(res.json);
                    }
                );
            } else {

            }
        }
        console.log(this.tandaTerima.nama_pt);
    }

    getUserDivisi() {
        this.user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
        this.nama_penerima2 = '';
        if (this.tandaTerima.divisi_internal === '' || this.tandaTerima.divisi_internal === null || this.tandaTerima.divisi_internal === undefined) {
        } else {
            this.tandaTerimaService.getAutorityUser({ // OTORISASI KE 1
                page: 0,
                size: 10000,
                sort: ['kdBahan', 'asc'],
                query1: 'idInternal:' + this.tandaTerima.divisi_internal
            }).subscribe((res5: ResponseWrapper) => {
                this.newKdBarang1 = res5.json;
                this.newKdBarang1.forEach((element) => {
                    this.user_divisi.push({
                        label: element.createdBy,
                        value: element.createdBy
                    });
                });
            },
                (res5: ResponseWrapper) => {
                    this.commontUtilService.showError(res5.json);
                }
            );
        }
    }
}
