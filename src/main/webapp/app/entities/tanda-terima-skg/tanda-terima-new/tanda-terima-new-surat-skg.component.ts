import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { TandaTerimaSuratSKG, MasterCustomer } from '../tanda-terima-skg.model';
import { TandaTerimaSKGService } from '../tanda-terima-skg.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { DetailTandaTerimaSKG, DetailTandaTerimaSKGService } from '../../detail-tanda-terima-skg';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ConfirmationService } from 'primeng/primeng';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { TandaTerimaDocument } from '../../tanda-terima-document';
import { LoadingService } from '../../../layouts';
import { ThrowStmt } from '@angular/compiler';
import { ToasterService } from '../../../shared';

@Component({
    selector: 'jhi-tanda-terima-new-surat-skg',
    templateUrl: './tanda-terima-new-surat-skg.component.html'
})
export class TandaTerimaNewSuratSKGComponent implements OnInit, OnDestroy {
    [x: string]: any;
    tandaTerima: TandaTerimaSuratSKG;
    item: DetailTandaTerimaSKG;
    items: DetailTandaTerimaSKG[];
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterCustomer;
    subscription: Subscription;
    isview: boolean;
    ttdtreceipt: Date;
    modalLampiran: boolean;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    document: TandaTerimaDocument;
    nott: string;
    suplier: string;
    sender: any;
    paramPage: number;
    routeId: number;
    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        private router: Router,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        protected dataUtils: JhiDataUtils,
        protected detailTandaTerimaService: DetailTandaTerimaSKGService,
        private loadingService: LoadingService,
        protected toaster: ToasterService,
    ) {
        this.tandaTerima = new TandaTerimaSuratSKG();
        this.routeId = 0;
        this.isview = false
        this.ttdtreceipt = new Date();
        this.modalLampiran = false;
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.document = new TandaTerimaDocument();
        this.nott = 'XXXXXX/XXX/XX/XXXX';
    }
    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                }
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
    }
    load(id) {
        this.tandaTerimaService.findSurat(id).subscribe(
            (res: TandaTerimaSuratSKG) => {
                this.tandaTerima = res;
                this.ttdtreceipt = new Date(res.dtreceipt);
                this.nott = res.receiptno;
            }
        )
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    tambahBaris() {
        this.item = new DetailTandaTerimaSKG();
        this.item.nourut = this.items.length + 1;
        this.items = [... this.items, this.item];
    }
    savett() {
        this.loadingService.loadingStart();
        if (this.suplier !== undefined) {
            this.tandaTerima.sender = this.suplier
        }

        if (this.tandaTerima.receiver == null || this.tandaTerima.doctype == null || this.tandaTerima.sender == null) {
            this.toaster.showToaster('info', 'Save', 'Kolom Tidak Boleh Kosong');
            this.loadingService.loadingStop();
        } else {
            this.tandaTerima.dtreceipt = this.ttdtreceipt;
        if (this.tandaTerima.idreceipt === undefined) {
            this.tandaTerimaService.createSurat(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        console.log('sukses');
                        this.router.navigate(['../tanda-terima-surat-skg']);
                    }
                )
        } else {
            this.tandaTerimaService.updateSurat(this.tandaTerima)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        console.log('sukses');
                        this.router.navigate(['../tanda-terima-surat-skg']);
                    }
                )
        }
        }
    }
    backMainPage() {
        // this.router.navigate(['../tanda-terima-surat-skg']);
        // window.history.back();
        if (this.routeId === 0) {
            this.router.navigate(['tanda-terima-surat-skg', { page: this.paramPage }]);
        }
    }

    onKeyUp(x) { // appending the updated value to the variable
            this.tandaTerima.sender = x.target.value;
      }

    filterSupplierSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
                if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                    filtered.push(newSuplier);
                }
        }
        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    public selectSupplier(isSelect?: boolean): void {
        if (isSelect === true) {
            this.suplier = this.newSuplier.cust_name;
        } else {
            this.suplier = undefined;
        }
    }
}
