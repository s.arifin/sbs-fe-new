import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { TandaTerimaDokumenSKG, MasterCustomer } from '../tanda-terima-skg.model';
import { TandaTerimaSKGService } from '../tanda-terima-skg.service';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { DetailTandaTerimaSKG, DetailTandaTerimaSKGService } from '../../detail-tanda-terima-skg';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ConfirmationService } from 'primeng/primeng';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { TandaTerimaDocument } from '../../tanda-terima-document';
import { LoadingService } from '../../../layouts';
import { ToasterService } from '../../../shared';

@Component({
    selector: 'jhi-tanda-terima-new-keluar-eksternal',
    templateUrl: './tanda-terima-new-keluar-eksternal.component.html'
})
export class TandaTerimaNewKeluarEksternalComponent implements OnInit, OnDestroy {
    [x: string]: any;
    tandaTerima: TandaTerimaDokumenSKG;
    item: DetailTandaTerimaSKG;
    items: DetailTandaTerimaSKG[];
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterCustomer;
    subscription: Subscription;
    isview: boolean;
    ttdtreceipt: Date;
    modalLampiran: boolean;
    inputNamaJenis: string;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    document: TandaTerimaDocument;
    nott: string;
    nama_pengirim: string;
    alamat: string;
    inputNamaPT: any;
    inputJenisdokumen: any;
    inputJenislayanan: any;
    jumlah_dokumen: any;
    is_agen: any;
    cust_name: any;
    alamat_tujuan: any;
    cust_code: any;
    suplier: any;
    nama_penerima: any;
    nama_penerima2: any;
    no_telp: any;
    inputDivisi: any;
    brandSelect: any[] = [];
    nama_pt: Array<object> = [
        { label: 'Pilih Perusahaan', value: '' },
        { label: 'SKG', value: 'SKG' },
        { label: 'SBS', value: 'SBS' },
        { label: 'ACA', value: 'ACA' },
        { label: 'Pribadi', value: 'Pribadi' }
    ];
    divisi: Array<object> = [
        { label: 'Pilih Divisi', value: '' },
    ]

    jenis_exit: Array<object> = [
        { label: 'Pilih Jenis Pengiriman', value: '' },
        { label: 'Internal', value: 'Internal' },
        { label: 'Eksternal', value: 'Eksternal' },
    ];

    jenis: Array<object> = [
        { label: 'Pilih Jenis Layanan', value: '' },
        { label: 'REG', value: 'REG' },
        { label: 'ONS', value: 'ONS' }
    ];
    jenis_dokumen: Array<object> = [
        { label: 'Pilih Jenis Dokumen', value: '' },
        { label: 'Dokumen', value: 'Dokumen' },
        { label: 'Paket', value: 'Paket' }
    ];
    agen: Array<object> = [
        { label: 'Iya', value: 'Iya' },
        { label: 'Tidak', value: 'Tidak' }
    ];
    user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
    kurir: Array<object> = [
        { label: 'Pilih Kurir / Ekspedisi', value: '' },
        { label: 'JNE', value: 'jne' },
        { label: 'TIKI', value: 'tiki' },
        { label: 'POS Indonesia', value: 'pos_indonesia' },
        { label: 'Gojek (GoSend)', value: 'gojek' },
        { label: 'Grab (GrabExpress)', value: 'grab' },
        { label: 'SICEPAT', value: 'sicepat' },
        { label: 'Ninja Xpress', value: 'ninja_xpress' },
        { label: 'J&T Express', value: 'jt_express' },
        { label: 'Lion Parcel', value: 'lion_parcel' },
        { label: 'Wahana', value: 'wahana' },
        { label: 'Anteraja', value: 'anteraja' },
        { label: 'Deliveree', value: 'deliveree' },
        { label: 'Raya Logistik', value: 'raya_logistik' },
        { label: 'Pandu Logistics', value: 'pandu_logistics' },
        { label: 'Others', value: 'Others' },
    ];

    inputKurir: string;
    layananKurir: Array<any> = [
        { label: 'Pilih Layanan', value: '' },

        // JNE
        { label: 'JNE Reguler', value: 'jne_reguler', kurir: 'jne' },
        { label: 'JNE YES (Yakin Esok Sampai)', value: 'jne_yes', kurir: 'jne' },
        { label: 'JNE OKE (Ongkos Kirim Ekonomis)', value: 'jne_oke', kurir: 'jne' },
        { label: 'JNE Logistic', value: 'jne_logistic', kurir: 'jne' },

        // TIKI
        { label: 'TIKI Reguler', value: 'tiki_reguler', kurir: 'tiki' },
        { label: 'TIKI Next Day Service', value: 'tiki_next_day', kurir: 'tiki' },
        { label: 'TIKI ONS (Overnight Service)', value: 'tiki_ons', kurir: 'tiki' },

        // POS Indonesia
        { label: 'POS Kilat Khusus', value: 'pos_kilat_khusus', kurir: 'pos_indonesia' },
        { label: 'POS Reguler', value: 'pos_reguler', kurir: 'pos_indonesia' },
        { label: 'POS Ekspres', value: 'pos_ekspres', kurir: 'pos_indonesia' },

        // Gojek (GoSend)
        { label: 'GoSend Same Day', value: 'gojek_gosend_same_day', kurir: 'gojek' },
        { label: 'GoSend Instant', value: 'gojek_gosend_instant', kurir: 'gojek' },

        // Grab (GrabExpress)
        { label: 'GrabExpress Same Day', value: 'grab_grabexpress_same_day', kurir: 'grab' },
        { label: 'GrabExpress Instant', value: 'grab_grabexpress_instant', kurir: 'grab' },

        // SICEPAT
        { label: 'SICEPAT Regular', value: 'sicepat_regular', kurir: 'sicepat' },
        { label: 'SICEPAT Ekspres', value: 'sicepat_ekspres', kurir: 'sicepat' },

        // Ninja Xpress
        { label: 'Ninja Xpress Regular', value: 'ninja_xpress_regular', kurir: 'ninja_xpress' },
        { label: 'Ninja Xpress Same Day', value: 'ninja_xpress_same_day', kurir: 'ninja_xpress' },

        // J&T Express
        { label: 'J&T Express Reguler', value: 'jt_express_reguler', kurir: 'jt_express' },
        { label: 'J&T Express Same Day', value: 'jt_express_same_day', kurir: 'jt_express' },

        // Lion Parcel
        { label: 'Lion Parcel Regular', value: 'lion_parcel_regular', kurir: 'lion_parcel' },
        { label: 'Lion Parcel Ekspres', value: 'lion_parcel_ekspres', kurir: 'lion_parcel' },

        // Wahana
        { label: 'Wahana Reguler', value: 'wahana_reguler', kurir: 'wahana' },
        { label: 'Wahana Ekspres', value: 'wahana_ekspres', kurir: 'wahana' },

        // Anteraja
        { label: 'Anteraja Regular', value: 'anteraja_regular', kurir: 'anteraja' },
        { label: 'Anteraja Ekspres', value: 'anteraja_ekspres', kurir: 'anteraja' },

        // Deliveree
        { label: 'Deliveree Same Day', value: 'deliveree_same_day', kurir: 'deliveree' },
        { label: 'Deliveree Regular', value: 'deliveree_regular', kurir: 'deliveree' },

        // Raya Logistik
        { label: 'Raya Logistik Reguler', value: 'raya_logistik_reguler', kurir: 'raya_logistik' },
        { label: 'Raya Logistik Ekspres', value: 'raya_logistik_ekspres', kurir: 'raya_logistik' },

        // Pandu Logistics
        { label: 'Pandu Logistics Reguler', value: 'pandu_logistics_reguler', kurir: 'pandu_logistics' },
        { label: 'Pandu Logistics Same Day', value: 'pandu_logistics_same_day', kurir: 'pandu_logistics' }
    ];
    paramPage: number;
    routeId: number;
    currentAccount: any;
    idInternal: any;
    constructor(
        private tandaTerimaService: TandaTerimaSKGService,
        private router: Router,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        protected dataUtils: JhiDataUtils,
        protected detailTandaTerimaService: DetailTandaTerimaSKGService,
        private loadingService: LoadingService,
        protected toaster: ToasterService,
        private principal: Principal
    ) {
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.isview = false;
        this.routeId = 0;
        this.ttdtreceipt = new Date();
        this.modalLampiran = false;
        this.isInsertFile = null;
        this.isValidImage = false;
        this.is_agen = ''
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.document = new TandaTerimaDocument();
        this.nott = 'XXXXXX/XXX/XX/XXXX';
        this.inputNamaJenis = undefined;
        this.inputDivisi = undefined;
        this.suplier = undefined;
        this.alamat = undefined;
        this.cust_code = undefined;
        // this.nama_pengirim = this.principal.getUserLogin();
        this.tandaTerima = new TandaTerimaDokumenSKG();
        this.tandaTerima.nama_pt = '';
        this.tandaTerima.divisi_internal = '';
        this.tandaTerima.jenis_dokumen = '';
        this.tandaTerima.jenis_layanan = '';
        this.tandaTerima.tiki_inex = '';
        this.tandaTerima.nama_pengirim = ''
        this.tandaTerima.nama_penerima = '';
        this.tandaTerima.nama_penerima2 = '';
        this.tandaTerima.jumlah_dokumen = 0;
        this.tandaTerima.kurir = '';
        this.idInternal = this.principal.getIdInternal();

        if (this.idInternal.includes('SKG')) {
            this.nama_pt = this.nama_pt.filter((x) =>
                x['label'] === 'SKG' || x['label'] === 'Pilih Perusahaan' || x['label'] === 'Pribadi'
            );
            this.kurir = this.kurir.filter((x) =>
                x['label'] === 'TIKI' || x['label'] === 'Pilih Kurir / Ekspedisi'
            );
        } else if (!this.idInternal.includes('SKG') && !this.idInternal.includes('SMK')) {
            this.nama_pt = this.nama_pt.filter((x) =>
                x['label'] === 'SBS' || x['label'] === 'Pilih Perusahaan' || x['label'] === 'Pribadi'
            );
            this.kurir = this.kurir.filter((x) =>
                x['label'] === 'TIKI' || x['label'] === 'Pilih Kurir / Ekspedisi'
            );
        }

        this.layananKurir = this.layananKurir.filter((x) => x['label'] === 'Pilih Layanan');
        this.tandaTerima.asuransi = '';
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                }
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
    }
    load(id) {
        this.tandaTerimaService.findDocument(id).subscribe(
            (res: TandaTerimaDokumenSKG) => {
                this.tandaTerima = res;
                this.tandaTerima.dtreceipt = new Date(res.dtreceipt);
                this.nott = res.receiptno;
                this.layananKurir = [
                    { label: 'Pilih Layanan', value: '' },

                    // JNE
                    { label: 'JNE Reguler', value: 'jne_reguler', kurir: 'jne' },
                    { label: 'JNE YES (Yakin Esok Sampai)', value: 'jne_yes', kurir: 'jne' },
                    { label: 'JNE OKE (Ongkos Kirim Ekonomis)', value: 'jne_oke', kurir: 'jne' },
                    { label: 'JNE Logistic', value: 'jne_logistic', kurir: 'jne' },

                    // TIKI
                    { label: 'TIKI Reguler', value: 'tiki_reguler', kurir: 'tiki' },
                    { label: 'TIKI Next Day Service', value: 'tiki_next_day', kurir: 'tiki' },
                    { label: 'TIKI ONS (Overnight Service)', value: 'tiki_ons', kurir: 'tiki' },

                    // POS Indonesia
                    { label: 'POS Kilat Khusus', value: 'pos_kilat_khusus', kurir: 'pos_indonesia' },
                    { label: 'POS Reguler', value: 'pos_reguler', kurir: 'pos_indonesia' },
                    { label: 'POS Ekspres', value: 'pos_ekspres', kurir: 'pos_indonesia' },

                    // Gojek (GoSend)
                    { label: 'GoSend Same Day', value: 'gojek_gosend_same_day', kurir: 'gojek' },
                    { label: 'GoSend Instant', value: 'gojek_gosend_instant', kurir: 'gojek' },

                    // Grab (GrabExpress)
                    { label: 'GrabExpress Same Day', value: 'grab_grabexpress_same_day', kurir: 'grab' },
                    { label: 'GrabExpress Instant', value: 'grab_grabexpress_instant', kurir: 'grab' },

                    // SICEPAT
                    { label: 'SICEPAT Regular', value: 'sicepat_regular', kurir: 'sicepat' },
                    { label: 'SICEPAT Ekspres', value: 'sicepat_ekspres', kurir: 'sicepat' },

                    // Ninja Xpress
                    { label: 'Ninja Xpress Regular', value: 'ninja_xpress_regular', kurir: 'ninja_xpress' },
                    { label: 'Ninja Xpress Same Day', value: 'ninja_xpress_same_day', kurir: 'ninja_xpress' },

                    // J&T Express
                    { label: 'J&T Express Reguler', value: 'jt_express_reguler', kurir: 'jt_express' },
                    { label: 'J&T Express Same Day', value: 'jt_express_same_day', kurir: 'jt_express' },

                    // Lion Parcel
                    { label: 'Lion Parcel Regular', value: 'lion_parcel_regular', kurir: 'lion_parcel' },
                    { label: 'Lion Parcel Ekspres', value: 'lion_parcel_ekspres', kurir: 'lion_parcel' },

                    // Wahana
                    { label: 'Wahana Reguler', value: 'wahana_reguler', kurir: 'wahana' },
                    { label: 'Wahana Ekspres', value: 'wahana_ekspres', kurir: 'wahana' },

                    // Anteraja
                    { label: 'Anteraja Regular', value: 'anteraja_regular', kurir: 'anteraja' },
                    { label: 'Anteraja Ekspres', value: 'anteraja_ekspres', kurir: 'anteraja' },

                    // Deliveree
                    { label: 'Deliveree Same Day', value: 'deliveree_same_day', kurir: 'deliveree' },
                    { label: 'Deliveree Regular', value: 'deliveree_regular', kurir: 'deliveree' },

                    // Raya Logistik
                    { label: 'Raya Logistik Reguler', value: 'raya_logistik_reguler', kurir: 'raya_logistik' },
                    { label: 'Raya Logistik Ekspres', value: 'raya_logistik_ekspres', kurir: 'raya_logistik' },

                    // Pandu Logistics
                    { label: 'Pandu Logistics Reguler', value: 'pandu_logistics_reguler', kurir: 'pandu_logistics' },
                    { label: 'Pandu Logistics Same Day', value: 'pandu_logistics_same_day', kurir: 'pandu_logistics' }
                ];
                this.layananKurir = this.layananKurir.filter((x) => x['label'] === 'Pilih Layanan' || x['kurir'] === this.tandaTerima.kurir);
                if (res.asuransi === null || res.asuransi === undefined || res.asuransi === '') {
                    this.tandaTerima.asuransi = '';
                }
                // if (this.tandaTerima.nama_pt !== 'Pribadi') {
                //     this.filterSupplierSingle(this.tandaTerima.cust_code);
                //     this.selectSupplier(true);
                // }
            }
        )
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    tambahBaris() {
        this.item = new DetailTandaTerimaSKG();
        this.item.nourut = this.items.length + 1;
        this.items = [... this.items, this.item];
    }
    savett() {
        this.loadingService.loadingStart();
        if (this.tandaTerima.jumlah_dokumen === null || this.tandaTerima.jumlah_dokumen === undefined || this.tandaTerima.jumlah_dokumen === '') {
            this.tandaTerima.jumlah_dokumen = 0;
        }
        if (this.tandaTerima.alamat === '' || this.tandaTerima.alamat === undefined || this.tandaTerima.alamat === null) {
            this.toaster.showToaster('info', 'Save', 'Alamat Penerima Harus di Isi');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.nama_pt === '') {
            this.toaster.showToaster('info', 'Save', 'Perusahaan Harus di Pilih');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.nama_pengirim === '' || this.tandaTerima.nama_pengirim === undefined || this.tandaTerima.nama_pengirim === null) {
            this.toaster.showToaster('info', 'Save', 'Nama Pengirim Harus di Isi');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.kurir === '') {
            this.toaster.showToaster('info', 'Save', 'Ekspedisi Harus di Pilih');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.kurir !== '' && this.tandaTerima.jenis_layanan === '') {
            this.toaster.showToaster('info', 'Save', 'Jenis Layanan Harus di Pilih');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.jenis_dokumen === '') {
            this.toaster.showToaster('info', 'Save', 'Jenis Dokumen Harus di Isi');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.jumlah_dokumen < 1) {
            this.toaster.showToaster('info', 'Save', 'Jumlah Dokumen Tidak Boleh 0');
            this.loadingService.loadingStop();
        } else if (this.tandaTerima.asuransi === 'Y' && (this.tandaTerima.nominal_asuransi === undefined || this.tandaTerima.nominal_asuransi === null || this.tandaTerima.nominal_asuransi < 1)) {
            this.toaster.showToaster('info', 'Save', 'Nominal Asusansi Tidak Boleh Kosong / 0');
            this.loadingService.loadingStop();
        } else {
            if (this.tandaTerima.idreceipt === undefined) {
                this.tandaTerima.tiki_inex = 'Eksternal';
                this.tandaTerimaService.createDocument(this.tandaTerima)
                    .subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            console.log('sukses');
                            this.router.navigate(['../tanda-terima-keluar-eksternal']);
                        }
                    )
            } else {
                this.tandaTerimaService.updateDocument(this.tandaTerima)
                    .subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            console.log('sukses');
                            this.router.navigate(['../tanda-terima-keluar-eksternal']);
                        }
                    )
            }
        }
    }
    backMainPage() {
        if (this.routeId === 0) {
            this.router.navigate(['tanda-terima-keluar-eksternal', { page: this.paramPage }]);
        }
    }

    ceknott() {
        this.tandaTerimaService.cekNoTT(this.tandaTerima.receiptno)
            .subscribe(
                (res: ResponseWrapper) => {
                    // console.log(' hasil dari JSON = ', res.json);
                    // console.log(' hasil dari Status = ', res.status);
                    // console.log(' hasil dari Headers = ', res.headers);
                    if (res.status === 200) {
                        this.confirmationService.confirm({
                            header: 'Information',
                            message: 'nomor tanda terima sudah ada',
                            rejectVisible: false,
                            accept: () => {
                                this.tandaTerima.receiptno = ''
                            }
                        });
                    }
                    if (res.status === 404) {

                    }
                }
            )
    }
    addAtt(rowData: DetailTandaTerimaSKG) {
        this.modalLampiran = true;
        this.item = rowData;
        this.document = new TandaTerimaDocument();
    }
    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
            // console.log('entity == ', entity);
            // console.log('field === ', field);
            // console.log('content', this.document.doc);
            // console.log('contenttype', this.document.doctype);
        } else {
            this.isValidImage = true;
            this.clearInputImage('doc', 'doctype', 'fileImage');
            this.isInsertFile = null;
            console.log('masuk kbesaran');
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.item, this.elementRef, field, fieldContentType, idInput);
    }
    setImange() {
        this.modalLampiran = false;
        this.item.document = this.document;
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    public deleteListArray(rowData: DetailTandaTerimaSKG) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                if (rowData.iddetreceipt === undefined && rowData.iddetreceipt !== null) {
                    this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                    console.log('items length == ' + this.items.length);
                    if (this.items.length <= 0) {
                    };
                } else {
                    this.detailTandaTerimaService.delete(rowData.iddetreceipt).subscribe(
                        (res) => {
                            this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                            console.log('items length == ' + this.items.length);
                            if (this.items.length <= 0) {
                            };
                        }
                    )
                }
                this.items = this.setNoUrut(this.items);
            }
        });
    }
    setNoUrut(items: DetailTandaTerimaSKG[]): DetailTandaTerimaSKG[] {
        let urut = 1;
        this.items.forEach(
            (e) => {
                e.nourut = urut;
                urut++;
            }
        );
        return this.items;
    }

    filterSupplierSingle(event) {
        const query = event.query;
        if (this.tandaTerima.nama_pt === 'SKG') {
            this.tandaTerimaService.getDistributor().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        } else if (this.tandaTerima.nama_pt === 'SBS') {
            this.tandaTerimaService.getDistributorSBS().then((newSupliers) => {
                console.log(newSupliers);
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        } else {
            this.tandaTerimaService.getDistributor().then((newSupliers) => {
                this.filteredSuppliers = this.filterSupplier(query, newSupliers);
            });
        }
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.cust_code.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_name.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.tandaTerimaService.getDistributor().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterCustomer[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.cust_code.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        if (filtered.length > 0) {
            return filtered;
        } else {
            this.selectSupplier(false);
            return filtered;
        }
    }

    public selectSupplier(isSelect?: boolean): void {
        if (isSelect === true) {
            this.tandaTerima.nama_penerima = this.newSuplier.cust_name;
            this.tandaTerima.alamat = this.newSuplier.cust_addr;
            this.tandaTerima.cust_code = this.newSuplier.cust_code;
        } else {
            this.tandaTerima.nama_penerima = '';
            this.tandaTerima.alamat = '';
            this.tandaTerima.cust_code = '';
        }
    }

    onKeyUp(x) { // appending the updated value to the variable
        this.tandaTerima.nama_penerima = x.target.value;
    }

    setNull() {
        this.tandaTerima.divisi_internal = '';
        this.user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
        this.brandSelect = [];
        this.tandaTerima.nama_penerima = '';
        this.tandaTerima.nama_penerima2 = '';
        this.tandaTerima.cust_code = '';
        this.tandaTerima.alamat = '';
        this.tandaTerima.divisi_internal = '';
    }

    nullPenerima() {
        this.tandaTerima.alamat = '';
        this.tandaTerima.cust_code = '';
        this.tandaTerima.nama_penerima = '';
        this.tandaTerima.nama_penerima2 = '';
        this.newSuplier = new MasterSupplier();
    }

    getUserDivisi() {
        this.user_divisi = [{ label: 'Pilih User Penerima', value: '' }];
        if (this.tandaTerima.tiki_inex === '' || this.tandaTerima.tiki_inex === 'Eksternal') {

        } else {
            if (this.tandaTerima.divisi_internal === '' || this.tandaTerima.divisi_internal === null || this.tandaTerima.divisi_internal === undefined) {

            } else {
                this.tandaTerimaService.getAutorityUser({ // OTORISASI KE 1
                    page: 0,
                    size: 10000,
                    sort: ['kdBahan', 'asc'],
                    query1: 'idInternal:' + this.tandaTerima.divisi_internal
                }).subscribe((res: ResponseWrapper) => {
                    this.newKdBarang1 = res.json;
                    this.newKdBarang1.forEach((element) => {
                        this.user_divisi.push({
                            label: element.createdBy,
                            value: element.createdBy
                        });
                    });
                },
                    (res: ResponseWrapper) => {
                        this.commontUtilService.showError(res.json);
                    }
                );
            }
        }
    }

    getLayanan() {
        if (this.tandaTerima.kurir === '' || this.tandaTerima.kurir === undefined || this.tandaTerima.kurir === null) {
            this.layananKurir = this.layananKurir.filter((x) => x['label'] === 'Pilih Layanan')
        } else {
            this.layananKurir = [
                { label: 'Pilih Layanan', value: '' },

                // JNE
                { label: 'JNE Reguler', value: 'jne_reguler', kurir: 'jne' },
                { label: 'JNE YES (Yakin Esok Sampai)', value: 'jne_yes', kurir: 'jne' },
                { label: 'JNE OKE (Ongkos Kirim Ekonomis)', value: 'jne_oke', kurir: 'jne' },
                { label: 'JNE Logistic', value: 'jne_logistic', kurir: 'jne' },

                // TIKI
                { label: 'TIKI Reguler', value: 'tiki_reguler', kurir: 'tiki' },
                { label: 'TIKI Next Day Service', value: 'tiki_next_day', kurir: 'tiki' },
                { label: 'TIKI ONS (Overnight Service)', value: 'tiki_ons', kurir: 'tiki' },

                // POS Indonesia
                { label: 'POS Kilat Khusus', value: 'pos_kilat_khusus', kurir: 'pos_indonesia' },
                { label: 'POS Reguler', value: 'pos_reguler', kurir: 'pos_indonesia' },
                { label: 'POS Ekspres', value: 'pos_ekspres', kurir: 'pos_indonesia' },

                // Gojek (GoSend)
                { label: 'GoSend Same Day', value: 'gojek_gosend_same_day', kurir: 'gojek' },
                { label: 'GoSend Instant', value: 'gojek_gosend_instant', kurir: 'gojek' },

                // Grab (GrabExpress)
                { label: 'GrabExpress Same Day', value: 'grab_grabexpress_same_day', kurir: 'grab' },
                { label: 'GrabExpress Instant', value: 'grab_grabexpress_instant', kurir: 'grab' },

                // SICEPAT
                { label: 'SICEPAT Regular', value: 'sicepat_regular', kurir: 'sicepat' },
                { label: 'SICEPAT Ekspres', value: 'sicepat_ekspres', kurir: 'sicepat' },

                // Ninja Xpress
                { label: 'Ninja Xpress Regular', value: 'ninja_xpress_regular', kurir: 'ninja_xpress' },
                { label: 'Ninja Xpress Same Day', value: 'ninja_xpress_same_day', kurir: 'ninja_xpress' },

                // J&T Express
                { label: 'J&T Express Reguler', value: 'jt_express_reguler', kurir: 'jt_express' },
                { label: 'J&T Express Same Day', value: 'jt_express_same_day', kurir: 'jt_express' },

                // Lion Parcel
                { label: 'Lion Parcel Regular', value: 'lion_parcel_regular', kurir: 'lion_parcel' },
                { label: 'Lion Parcel Ekspres', value: 'lion_parcel_ekspres', kurir: 'lion_parcel' },

                // Wahana
                { label: 'Wahana Reguler', value: 'wahana_reguler', kurir: 'wahana' },
                { label: 'Wahana Ekspres', value: 'wahana_ekspres', kurir: 'wahana' },

                // Anteraja
                { label: 'Anteraja Regular', value: 'anteraja_regular', kurir: 'anteraja' },
                { label: 'Anteraja Ekspres', value: 'anteraja_ekspres', kurir: 'anteraja' },

                // Deliveree
                { label: 'Deliveree Same Day', value: 'deliveree_same_day', kurir: 'deliveree' },
                { label: 'Deliveree Regular', value: 'deliveree_regular', kurir: 'deliveree' },

                // Raya Logistik
                { label: 'Raya Logistik Reguler', value: 'raya_logistik_reguler', kurir: 'raya_logistik' },
                { label: 'Raya Logistik Ekspres', value: 'raya_logistik_ekspres', kurir: 'raya_logistik' },

                // Pandu Logistics
                { label: 'Pandu Logistics Reguler', value: 'pandu_logistics_reguler', kurir: 'pandu_logistics' },
                { label: 'Pandu Logistics Same Day', value: 'pandu_logistics_same_day', kurir: 'pandu_logistics' }
            ];
            this.layananKurir = this.layananKurir.filter((x) => x['label'] === 'Pilih Layanan' || x['kurir'] === this.tandaTerima.kurir)
        }
    }
}
