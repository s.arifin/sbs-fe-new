export * from './detail-tanda-terima.model';
export * from './detail-tanda-terima-popup.service';
export * from './detail-tanda-terima.service';
export * from './detail-tanda-terima-dialog.component';
export * from './detail-tanda-terima-delete-dialog.component';
export * from './detail-tanda-terima-detail.component';
export * from './detail-tanda-terima.component';
export * from './detail-tanda-terima.route';
