import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DetailTandaTerima } from './detail-tanda-terima.model';
import { DetailTandaTerimaService } from './detail-tanda-terima.service';

@Component({
    selector: 'jhi-detail-tanda-terima-detail',
    templateUrl: './detail-tanda-terima-detail.component.html'
})
export class DetailTandaTerimaDetailComponent implements OnInit, OnDestroy {

    detailTandaTerima: DetailTandaTerima;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private detailTandaTerimaService: DetailTandaTerimaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDetailTandaTerimas();
    }

    load(id) {
        this.detailTandaTerimaService.find(id).subscribe((detailTandaTerima) => {
            this.detailTandaTerima = detailTandaTerima;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDetailTandaTerimas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'detailTandaTerimaListModification',
            (response) => this.load(this.detailTandaTerima.id)
        );
    }
}
