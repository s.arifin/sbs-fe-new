import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DetailTandaTerima } from './detail-tanda-terima.model';
import { DetailTandaTerimaService } from './detail-tanda-terima.service';

@Injectable()
export class DetailTandaTerimaPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private detailTandaTerimaService: DetailTandaTerimaService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.detailTandaTerimaService.find(id).subscribe((detailTandaTerima) => {
                    detailTandaTerima.dtinv = this.datePipe
                        .transform(detailTandaTerima.dtinv, 'yyyy-MM-ddTHH:mm:ss');
                    detailTandaTerima.dtinvtax = this.datePipe
                        .transform(detailTandaTerima.dtinvtax, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.detailTandaTerimaModalRef(component, detailTandaTerima);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.detailTandaTerimaModalRef(component, new DetailTandaTerima());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    detailTandaTerimaModalRef(component: Component, detailTandaTerima: DetailTandaTerima): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.detailTandaTerima = detailTandaTerima;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
