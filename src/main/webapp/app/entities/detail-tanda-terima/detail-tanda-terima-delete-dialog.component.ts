import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DetailTandaTerima } from './detail-tanda-terima.model';
import { DetailTandaTerimaPopupService } from './detail-tanda-terima-popup.service';
import { DetailTandaTerimaService } from './detail-tanda-terima.service';

@Component({
    selector: 'jhi-detail-tanda-terima-delete-dialog',
    templateUrl: './detail-tanda-terima-delete-dialog.component.html'
})
export class DetailTandaTerimaDeleteDialogComponent {

    detailTandaTerima: DetailTandaTerima;

    constructor(
        private detailTandaTerimaService: DetailTandaTerimaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.detailTandaTerimaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'detailTandaTerimaListModification',
                content: 'Deleted an detailTandaTerima'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-detail-tanda-terima-delete-popup',
    template: ''
})
export class DetailTandaTerimaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailTandaTerimaPopupService: DetailTandaTerimaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.detailTandaTerimaPopupService
                .open(DetailTandaTerimaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
