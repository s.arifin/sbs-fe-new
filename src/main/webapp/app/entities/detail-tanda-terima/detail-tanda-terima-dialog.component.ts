import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DetailTandaTerima } from './detail-tanda-terima.model';
import { DetailTandaTerimaPopupService } from './detail-tanda-terima-popup.service';
import { DetailTandaTerimaService } from './detail-tanda-terima.service';

@Component({
    selector: 'jhi-detail-tanda-terima-dialog',
    templateUrl: './detail-tanda-terima-dialog.component.html'
})
export class DetailTandaTerimaDialogComponent implements OnInit {

    detailTandaTerima: DetailTandaTerima;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private detailTandaTerimaService: DetailTandaTerimaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.detailTandaTerima.id !== undefined) {
            this.subscribeToSaveResponse(
                this.detailTandaTerimaService.update(this.detailTandaTerima));
        } else {
            this.subscribeToSaveResponse(
                this.detailTandaTerimaService.create(this.detailTandaTerima));
        }
    }

    private subscribeToSaveResponse(result: Observable<DetailTandaTerima>) {
        result.subscribe((res: DetailTandaTerima) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DetailTandaTerima) {
        this.eventManager.broadcast({ name: 'detailTandaTerimaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-detail-tanda-terima-popup',
    template: ''
})
export class DetailTandaTerimaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailTandaTerimaPopupService: DetailTandaTerimaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.detailTandaTerimaPopupService
                    .open(DetailTandaTerimaDialogComponent as Component, params['id']);
            } else {
                this.detailTandaTerimaPopupService
                    .open(DetailTandaTerimaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
