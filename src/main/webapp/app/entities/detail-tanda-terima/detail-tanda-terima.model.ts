import { TandaTerimaDocument } from '../tanda-terima-document';
import { BaseEntity } from './../../shared';

export class DetailTandaTerima implements BaseEntity {
    constructor(
        public id?: number,
        public iddetreceipt?: any,
        public idreceipt?: any,
        public invno?: string,
        public dtinv?: any,
        public invtaxno?: string,
        public dtinvtax?: any,
        public total?: number,
        public notes?: string,
        public ntotal?: number,
        public nourut?: number,
        public nmsupplier?: string,
        public dtreceipt?: Date,
        public receiptno?: string,
        public kdsupplier?: string,
        public document?: TandaTerimaDocument,
        // public is_use?: number,
        public is_disable?: boolean
    ) {
        this.document = new TandaTerimaDocument();
    }
}

export class DetailTandaTerimaRevisi implements BaseEntity {
    constructor(
        public id?: number,
        public iddetreceipt?: any,
        public idreceipt?: any,
        public invno?: string,
        public dtinv?: any,
        public invtaxno?: string,
        public dtinvtax?: any,
        public total?: number,
        public notes_detail?: string,
        public nourut?: number,
        public idhistory?: any,
        public idhistory_detail?: any,
        public receiptno?: string
    ) {
    }
}
