import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    DetailTandaTerimaService,
    DetailTandaTerimaPopupService,
    DetailTandaTerimaComponent,
    DetailTandaTerimaDetailComponent,
    DetailTandaTerimaDialogComponent,
    DetailTandaTerimaPopupComponent,
    DetailTandaTerimaDeletePopupComponent,
    DetailTandaTerimaDeleteDialogComponent,
    detailTandaTerimaRoute,
    detailTandaTerimaPopupRoute,
    DetailTandaTerimaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...detailTandaTerimaRoute,
    ...detailTandaTerimaPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DetailTandaTerimaComponent,
        DetailTandaTerimaDetailComponent,
        DetailTandaTerimaDialogComponent,
        DetailTandaTerimaDeleteDialogComponent,
        DetailTandaTerimaPopupComponent,
        DetailTandaTerimaDeletePopupComponent,
    ],
    entryComponents: [
        DetailTandaTerimaComponent,
        DetailTandaTerimaDialogComponent,
        DetailTandaTerimaPopupComponent,
        DetailTandaTerimaDeleteDialogComponent,
        DetailTandaTerimaDeletePopupComponent,
    ],
    providers: [
        DetailTandaTerimaService,
        DetailTandaTerimaPopupService,
        DetailTandaTerimaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDetailTandaTerimaModule {}
