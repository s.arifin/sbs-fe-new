import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DetailTandaTerimaComponent } from './detail-tanda-terima.component';
import { DetailTandaTerimaDetailComponent } from './detail-tanda-terima-detail.component';
import { DetailTandaTerimaPopupComponent } from './detail-tanda-terima-dialog.component';
import { DetailTandaTerimaDeletePopupComponent } from './detail-tanda-terima-delete-dialog.component';

@Injectable()
export class DetailTandaTerimaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const detailTandaTerimaRoute: Routes = [
    {
        path: 'detail-tanda-terima',
        component: DetailTandaTerimaComponent,
        resolve: {
            'pagingParams': DetailTandaTerimaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'detail-tanda-terima/:id',
        component: DetailTandaTerimaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const detailTandaTerimaPopupRoute: Routes = [
    {
        path: 'detail-tanda-terima-new',
        component: DetailTandaTerimaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-tanda-terima/:id/edit',
        component: DetailTandaTerimaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-tanda-terima/:id/delete',
        component: DetailTandaTerimaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
