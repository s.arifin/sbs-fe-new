import { Routes } from '@angular/router';
import { LoginSisComponent } from './login-sis.component'; // Your login component

export const LoginSisRoutes: Routes = [
  {
    path: 'login-sis/:param1', // Two parameters in the URL
    component: LoginSisComponent,
    data: { name: 'loginSisRoute' } // Named route for reference
  }
];
