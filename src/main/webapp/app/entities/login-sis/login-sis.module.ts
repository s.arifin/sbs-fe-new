import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginSisComponent } from './login-sis.component'; // Your component
import { LoginSisRoutes } from './login-sis.route'; // Import routes
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    LoginSisComponent, // Declare the component,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(LoginSisRoutes), // Import the routes,
    NgbModule
  ],
  providers: [
  ]
})
export class MpmLoginSisModule { }
