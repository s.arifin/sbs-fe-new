import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../../layouts/loading';
import { Account, LoginService, Principal } from '../../shared';
import { JhiEventManager } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-login-sis',
    templateUrl: './login-sis.component.html',
    styleUrls: [
        'loading.scss'
    ]})
export class LoginSisComponent implements OnInit {
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;
    account: Account;
    constructor(
        private route: ActivatedRoute,
        private loginService: LoginService,
        private eventManager: JhiEventManager,
        private router: Router,
        private principal: Principal,
        public activeModal: NgbActiveModal,
        protected loadingService: LoadingService,
    ) { }

    ngOnInit() {
        this.loadingService.loadingStart();
        // Capture the two parameters from the URL
        this.route.params.subscribe((params) => {
            const param1 = params['param1'];
            const param1_decrypt = atob(param1)
            this.loginService.login({
                username: param1_decrypt,
                password: 'S152012#$%',
                rememberMe: this.rememberMe
            }).then(() => {
                this.authenticationError = false;
                this.activeModal.dismiss('login success');
                document.body.classList.remove('not-login');
                setTimeout(function() {
                    document.body.classList.add('hide-loading');
                }, 2000);

                this.principal.identity().then((account) => {
                    this.account = account;
                    if (this.account === undefined) {
                        console.log('belum login');
                    } else {
                        this.router.navigate(['/purchase-order']);
                    }
                });
            }).catch(() => {
                this.router.navigate(['']);
            });
        });

        // Optionally capture route data (e.g., route name)
        this.route.data.subscribe((data) => {
            const routeName = data['name'];
            console.log('Route name:', routeName);
        });
        this.loadingService.loadingStop();
    }
}
