import { BaseEntity } from './../../shared';

export class GeoBoundaryType implements BaseEntity {
    constructor(
        public id?: number,
        public idGeobouType?: number,
        public description?: string,
    ) {
    }
}
