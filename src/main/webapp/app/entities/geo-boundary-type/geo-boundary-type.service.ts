import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { GeoBoundaryType } from './geo-boundary-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class GeoBoundaryTypeService {
    protected itemValues: GeoBoundaryType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/geo-boundary-types';
    protected resourceSearchUrl = 'api/_search/geo-boundary-types';

    constructor(protected http: Http) { }

    create(geoBoundaryType: GeoBoundaryType): Observable<GeoBoundaryType> {
        const copy = this.convert(geoBoundaryType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(geoBoundaryType: GeoBoundaryType): Observable<GeoBoundaryType> {
        const copy = this.convert(geoBoundaryType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<GeoBoundaryType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, geoBoundaryType: GeoBoundaryType): Observable<GeoBoundaryType> {
        const copy = this.convert(geoBoundaryType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, geoBoundaryTypes: GeoBoundaryType[]): Observable<GeoBoundaryType[]> {
        const copy = this.convertList(geoBoundaryTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(geoBoundaryType: GeoBoundaryType): GeoBoundaryType {
        if (geoBoundaryType === null || geoBoundaryType === {}) {
            return {};
        }
        // const copy: GeoBoundaryType = Object.assign({}, geoBoundaryType);
        const copy: GeoBoundaryType = JSON.parse(JSON.stringify(geoBoundaryType));
        return copy;
    }

    protected convertList(geoBoundaryTypes: GeoBoundaryType[]): GeoBoundaryType[] {
        const copy: GeoBoundaryType[] = geoBoundaryTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: GeoBoundaryType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
