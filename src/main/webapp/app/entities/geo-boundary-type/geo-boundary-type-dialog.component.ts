import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {GeoBoundaryType} from './geo-boundary-type.model';
import {GeoBoundaryTypePopupService} from './geo-boundary-type-popup.service';
import {GeoBoundaryTypeService} from './geo-boundary-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-geo-boundary-type-dialog',
    templateUrl: './geo-boundary-type-dialog.component.html'
})
export class GeoBoundaryTypeDialogComponent implements OnInit {

    geoBoundaryType: GeoBoundaryType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected geoBoundaryTypeService: GeoBoundaryTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.geoBoundaryType.idGeobouType !== undefined) {
            this.subscribeToSaveResponse(
                this.geoBoundaryTypeService.update(this.geoBoundaryType));
        } else {
            this.subscribeToSaveResponse(
                this.geoBoundaryTypeService.create(this.geoBoundaryType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<GeoBoundaryType>) {
        result.subscribe((res: GeoBoundaryType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: GeoBoundaryType) {
        this.eventManager.broadcast({ name: 'geoBoundaryTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'geoBoundaryType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'geoBoundaryType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-geo-boundary-type-popup',
    template: ''
})
export class GeoBoundaryTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected geoBoundaryTypePopupService: GeoBoundaryTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.geoBoundaryTypePopupService
                    .open(GeoBoundaryTypeDialogComponent as Component, params['id']);
            } else {
                this.geoBoundaryTypePopupService
                    .open(GeoBoundaryTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
