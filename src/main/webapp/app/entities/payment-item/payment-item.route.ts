import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PaymentItemComponent } from './payment-item.component';
import { PaymentItemDetailComponent } from './payment-item-detail.component';
import { PaymentItemPopupComponent } from './payment-item-dialog.component';
import { PaymentItemDeletePopupComponent } from './payment-item-delete-dialog.component';

@Injectable()
export class PaymentItemResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const paymentItemRoute: Routes = [
    {
        path: 'payment-item',
        component: PaymentItemComponent,
        resolve: {
            'pagingParams': PaymentItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-item/:id',
        component: PaymentItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentItemPopupRoute: Routes = [
    {
        path: 'payment-item-new',
        component: PaymentItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-item/:id/edit',
        component: PaymentItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-item/:id/delete',
        component: PaymentItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
