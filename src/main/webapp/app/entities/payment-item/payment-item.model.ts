import { BaseEntity } from './../../shared';

export class PaymentItem implements BaseEntity {
    constructor(
        public id?: number,
        public idpaydet?: number,
        public idpay?: number,
        public idinvoice?: number,
        public invno?: string,
        public totalpay?: number,
        public invfrom?: string,
        public invtaxno?: string,
        public receiptno?: string,
        public ppn?: number,
        public paynoppn?: number,
        public proformainv?: string,
        public kurs?: number,
        public totalppn?: number,
        public umlunas?: number,
        public nopo?: string,
    ) {
    }
}
