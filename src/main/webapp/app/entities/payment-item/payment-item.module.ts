import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PaymentItemService,
    PaymentItemPopupService,
    PaymentItemComponent,
    PaymentItemDetailComponent,
    PaymentItemDialogComponent,
    PaymentItemPopupComponent,
    PaymentItemDeletePopupComponent,
    PaymentItemDeleteDialogComponent,
    paymentItemRoute,
    paymentItemPopupRoute,
    PaymentItemResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...paymentItemRoute,
    ...paymentItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PaymentItemComponent,
        PaymentItemDetailComponent,
        PaymentItemDialogComponent,
        PaymentItemDeleteDialogComponent,
        PaymentItemPopupComponent,
        PaymentItemDeletePopupComponent,
    ],
    entryComponents: [
        PaymentItemComponent,
        PaymentItemDialogComponent,
        PaymentItemPopupComponent,
        PaymentItemDeleteDialogComponent,
        PaymentItemDeletePopupComponent,
    ],
    providers: [
        PaymentItemService,
        PaymentItemPopupService,
        PaymentItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPaymentItemModule {}
