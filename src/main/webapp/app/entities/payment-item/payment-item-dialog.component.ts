import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentItem } from './payment-item.model';
import { PaymentItemPopupService } from './payment-item-popup.service';
import { PaymentItemService } from './payment-item.service';

@Component({
    selector: 'jhi-payment-item-dialog',
    templateUrl: './payment-item-dialog.component.html'
})
export class PaymentItemDialogComponent implements OnInit {

    paymentItem: PaymentItem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private paymentItemService: PaymentItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentItemService.update(this.paymentItem));
        } else {
            this.subscribeToSaveResponse(
                this.paymentItemService.create(this.paymentItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<PaymentItem>) {
        result.subscribe((res: PaymentItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PaymentItem) {
        this.eventManager.broadcast({ name: 'paymentItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-payment-item-popup',
    template: ''
})
export class PaymentItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentItemPopupService: PaymentItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentItemPopupService
                    .open(PaymentItemDialogComponent as Component, params['id']);
            } else {
                this.paymentItemPopupService
                    .open(PaymentItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
