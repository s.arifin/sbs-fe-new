import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentItem } from './payment-item.model';
import { PaymentItemService } from './payment-item.service';

@Component({
    selector: 'jhi-payment-item-detail',
    templateUrl: './payment-item-detail.component.html'
})
export class PaymentItemDetailComponent implements OnInit, OnDestroy {

    paymentItem: PaymentItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private paymentItemService: PaymentItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPaymentItems();
    }

    load(id) {
        this.paymentItemService.find(id).subscribe((paymentItem) => {
            this.paymentItem = paymentItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPaymentItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'paymentItemListModification',
            (response) => this.load(this.paymentItem.id)
        );
    }
}
