import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentItem } from './payment-item.model';
import { PaymentItemPopupService } from './payment-item-popup.service';
import { PaymentItemService } from './payment-item.service';

@Component({
    selector: 'jhi-payment-item-delete-dialog',
    templateUrl: './payment-item-delete-dialog.component.html'
})
export class PaymentItemDeleteDialogComponent {

    paymentItem: PaymentItem;

    constructor(
        private paymentItemService: PaymentItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.paymentItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'paymentItemListModification',
                content: 'Deleted an paymentItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-payment-item-delete-popup',
    template: ''
})
export class PaymentItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentItemPopupService: PaymentItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.paymentItemPopupService
                .open(PaymentItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
