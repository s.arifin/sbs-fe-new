import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UomTypeComponent } from './uom-type.component';
import { UomTypeEditComponent } from './uom-type-edit.component';
import { UomTypePopupComponent } from './uom-type-dialog.component';

@Injectable()
export class UomTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idUomType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const uomTypeRoute: Routes = [
    {
        path: 'uom-type',
        component: UomTypeComponent,
        resolve: {
            'pagingParams': UomTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const uomTypePopupRoute: Routes = [
    {
        path: 'uom-type-popup-new',
        component: UomTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'uom-type-new',
        component: UomTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'uom-type/:id/edit',
        component: UomTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'uom-type/:route/:page/:id/edit',
        component: UomTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'uom-type/:id/popup-edit',
        component: UomTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.uomType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
