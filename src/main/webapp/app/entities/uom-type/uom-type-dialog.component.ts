import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UomType } from './uom-type.model';
import { UomTypePopupService } from './uom-type-popup.service';
import { UomTypeService } from './uom-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-uom-type-dialog',
    templateUrl: './uom-type-dialog.component.html'
})
export class UomTypeDialogComponent implements OnInit {

    uomType: UomType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected uomTypeService: UomTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.uomType.idUomType !== undefined) {
            this.subscribeToSaveResponse(
                this.uomTypeService.update(this.uomType));
        } else {
            this.subscribeToSaveResponse(
                this.uomTypeService.create(this.uomType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UomType>) {
        result.subscribe((res: UomType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UomType) {
        this.eventManager.broadcast({ name: 'uomTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'uomType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-uom-type-popup',
    template: ''
})
export class UomTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected uomTypePopupService: UomTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.uomTypePopupService
                    .open(UomTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.uomTypePopupService.parent = params['parent'];
                this.uomTypePopupService
                    .open(UomTypeDialogComponent as Component);
            } else {
                this.uomTypePopupService
                    .open(UomTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
