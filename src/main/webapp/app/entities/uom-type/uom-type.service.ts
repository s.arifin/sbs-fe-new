import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { UomType } from './uom-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UomTypeService {
   protected itemValues: UomType[];
   values: Subject<any> = new Subject<any>();

//    protected resourceUrl =  process.env.API_C_URL + 'api/uom-types';
   protected resourceUrl = process.env.API_C_URL + '/api/uom-types';
   protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/uom-types';

   constructor(protected http: Http) { }

   create(uomType: UomType): Observable<UomType> {
       const copy = this.convert(uomType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(uomType: UomType): Observable<UomType> {
       const copy = this.convert(uomType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<UomType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   getAll(): Observable<ResponseWrapper> {
       return this.http.get(this.resourceUrl)
           .map((res: Response) => this.convertResponse(res));
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: UomType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: UomType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to UomType.
    */
   protected convertItemFromServer(json: any): UomType {
       const entity: UomType = Object.assign(new UomType(), json);
       return entity;
   }

   /**
    * Convert a UomType to a JSON which can be sent to the server.
    */
   protected convert(uomType: UomType): UomType {
       if (uomType === null || uomType === {}) {
           return {};
       }
       // const copy: UomType = Object.assign({}, uomType);
       const copy: UomType = JSON.parse(JSON.stringify(uomType));
       return copy;
   }

   protected convertList(uomTypes: UomType[]): UomType[] {
       const copy: UomType[] = uomTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: UomType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
