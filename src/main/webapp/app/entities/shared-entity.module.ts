import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MpmSharedModule } from '../shared';
import { TabsModule } from 'ngx-bootstrap/tabs';

/* jhipster-needle-add-list-import - JHipster will add entity modules imports here */

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
    CheckboxModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    ButtonModule,
    DataTableModule,
    DataListModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
    RadioButtonModule
} from 'primeng/primeng';
import { UomAsListComponent } from './uom';
import { UomConversionAsListComponent } from './uom-conversion';
import { UnitDocumentMessageAsListComponent, UnitDocumentMessageService, UnitDocumentMessageByRequirementComponent } from './unit-document-message';
import { GeneralUploadAsListComponent } from './general-upload';
import { UnitDeliverableResolvePagingParams } from './shared-component/services/share-paging-params';
import { StatusTypeService } from './status-type';
import { InternalService } from './internal';
import {
    NumberOnlyDirective,
} from './shared-component';
import { BaseCalendarService } from './base-calendar';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule,
        TabsModule.forRoot(),
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule
        /* jhipster-needle-add-list-component - JHipster will add entity modules here */
    ],
    exports: [
        NumberOnlyDirective,
        UomAsListComponent,
        UomConversionAsListComponent,
        UnitDocumentMessageAsListComponent,
        UnitDocumentMessageByRequirementComponent,
        GeneralUploadAsListComponent,
    ],
    declarations: [
        // shared component
        NumberOnlyDirective,
        UomAsListComponent,
        UomConversionAsListComponent,
        UnitDocumentMessageAsListComponent,
        UnitDocumentMessageByRequirementComponent,
        GeneralUploadAsListComponent,
    ],
    entryComponents: [
        // shared component
        UomAsListComponent,
        UomConversionAsListComponent,
        UnitDocumentMessageAsListComponent,
        UnitDocumentMessageByRequirementComponent,
        GeneralUploadAsListComponent,
    ],
    providers: [
        StatusTypeService,
        InternalService,
        BaseCalendarService,
        UnitDeliverableResolvePagingParams,
        UnitDocumentMessageService,
        NgbActiveModal
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSharedEntityModule { }
