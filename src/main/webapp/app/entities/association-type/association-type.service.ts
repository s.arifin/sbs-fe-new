import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AssociationType } from './association-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class AssociationTypeService {
    protected itemValues: AssociationType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/association-types';
    protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/association-types';

    constructor(protected http: Http) { }

    create(associationType: AssociationType): Observable<AssociationType> {
        const copy = this.convert(associationType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(associationType: AssociationType): Observable<AssociationType> {
        const copy = this.convert(associationType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<AssociationType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, associationType: AssociationType): Observable<AssociationType> {
        const copy = this.convert(associationType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, associationTypes: AssociationType[]): Observable<AssociationType[]> {
        const copy = this.convertList(associationTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(associationType: AssociationType): AssociationType {
        if (associationType === null || associationType === {}) {
            return {};
        }
        // const copy: AssociationType = Object.assign({}, associationType);
        const copy: AssociationType = JSON.parse(JSON.stringify(associationType));
        return copy;
    }

    protected convertList(associationTypes: AssociationType[]): AssociationType[] {
        const copy: AssociationType[] = associationTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: AssociationType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
