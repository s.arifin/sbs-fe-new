import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {AssociationType} from './association-type.model';
import {AssociationTypePopupService} from './association-type-popup.service';
import {AssociationTypeService} from './association-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-association-type-dialog',
    templateUrl: './association-type-dialog.component.html'
})
export class AssociationTypeDialogComponent implements OnInit {

    associationType: AssociationType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected associationTypeService: AssociationTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.associationType.idAssociattionType !== undefined) {
            this.subscribeToSaveResponse(
                this.associationTypeService.update(this.associationType));
        } else {
            this.subscribeToSaveResponse(
                this.associationTypeService.create(this.associationType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<AssociationType>) {
        result.subscribe((res: AssociationType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: AssociationType) {
        this.eventManager.broadcast({ name: 'associationTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'associationType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'associationType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-association-type-popup',
    template: ''
})
export class AssociationTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected associationTypePopupService: AssociationTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.associationTypePopupService
                    .open(AssociationTypeDialogComponent as Component, params['id']);
            } else {
                this.associationTypePopupService
                    .open(AssociationTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
