import { BaseEntity } from './../../shared';

export class AssociationType implements BaseEntity {
    constructor(
        public id?: number,
        public idAssociattionType?: number,
        public description?: string,
    ) {
    }
}
