import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {InternalOrder} from './internal-order.model';
import {InternalOrderPopupService} from './internal-order-popup.service';
import {InternalOrderService} from './internal-order.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-internal-order-dialog',
    templateUrl: './internal-order-dialog.component.html'
})
export class InternalOrderDialogComponent implements OnInit {

    internalOrder: InternalOrder;
    isSaving: boolean;

    internals: Internal[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected internalOrderService: InternalOrderService,
        protected internalService: InternalService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.internalOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.internalOrderService.update(this.internalOrder));
        } else {
            this.subscribeToSaveResponse(
                this.internalOrderService.create(this.internalOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<InternalOrder>) {
        result.subscribe((res: InternalOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: InternalOrder) {
        this.eventManager.broadcast({ name: 'internalOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'internalOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'internalOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}

@Component({
    selector: 'jhi-internal-order-popup',
    template: ''
})
export class InternalOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected internalOrderPopupService: InternalOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.internalOrderPopupService
                    .open(InternalOrderDialogComponent as Component, params['id']);
            } else {
                this.internalOrderPopupService
                    .open(InternalOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
