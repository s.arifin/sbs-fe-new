import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { InternalOrder } from './internal-order.model';
import { InternalOrderService } from './internal-order.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-internal-order-edit',
    templateUrl: './internal-order-edit.component.html'
})
export class InternalOrderEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    internalOrder: InternalOrder;
    isSaving: boolean;

    internals: Internal[];

    constructor(
        protected alertService: JhiAlertService,
        protected internalOrderService: InternalOrderService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.internalOrder = new InternalOrder();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.internalOrderService.find(id).subscribe((internalOrder) => {
            this.internalOrder = internalOrder;
        });
    }

    previousState() {
        this.router.navigate(['internal-order']);
    }

    save() {
        this.isSaving = true;
        if (this.internalOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.internalOrderService.update(this.internalOrder));
        } else {
            this.subscribeToSaveResponse(
                this.internalOrderService.create(this.internalOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<InternalOrder>) {
        result.subscribe((res: InternalOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: InternalOrder) {
        this.eventManager.broadcast({ name: 'internalOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'internalOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'internalOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
