import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { InternalOrder } from './internal-order.model';
import { InternalOrderPopupService } from './internal-order-popup.service';
import { InternalOrderService } from './internal-order.service';

@Component({
    selector: 'jhi-internal-order-delete-dialog',
    templateUrl: './internal-order-delete-dialog.component.html'
})
export class InternalOrderDeleteDialogComponent {

    internalOrder: InternalOrder;

    constructor(
        protected internalOrderService: InternalOrderService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.internalOrderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'internalOrderListModification',
                content: 'Deleted an internalOrder'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-internal-order-delete-popup',
    template: ''
})
export class InternalOrderDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected internalOrderPopupService: InternalOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.internalOrderPopupService
                .open(InternalOrderDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
