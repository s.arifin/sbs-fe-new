import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { InternalOrderComponent } from './internal-order.component';
import { InternalOrderEditComponent } from './internal-order-edit.component';
import { InternalOrderPopupComponent } from './internal-order-dialog.component';

@Injectable()
export class InternalOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const internalOrderRoute: Routes = [
    {
        path: 'internal-order',
        component: InternalOrderComponent,
        resolve: {
            'pagingParams': InternalOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.internalOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const internalOrderPopupRoute: Routes = [
    {
        path: 'internal-order-new',
        component: InternalOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.internalOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'internal-order/:id/edit',
        component: InternalOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.internalOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'internal-order-popup-new',
        component: InternalOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.internalOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'internal-order/:id/popup-edit',
        component: InternalOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.internalOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
