import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { InternalOrder } from './internal-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class InternalOrderService {
    protected itemValues: InternalOrder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/internal-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/internal-orders';

    constructor(protected http: Http) { }

    create(internalOrder: InternalOrder): Observable<InternalOrder> {
        const copy = this.convert(internalOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(internalOrder: InternalOrder): Observable<InternalOrder> {
        const copy = this.convert(internalOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<InternalOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, internalOrder: InternalOrder): Observable<InternalOrder> {
        const copy = this.convert(internalOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, internalOrders: InternalOrder[]): Observable<InternalOrder[]> {
        const copy = this.convertList(internalOrders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(internalOrder: InternalOrder): InternalOrder {
        if (internalOrder === null || internalOrder === {}) {
            return {};
        }
        // const copy: InternalOrder = Object.assign({}, internalOrder);
        const copy: InternalOrder = JSON.parse(JSON.stringify(internalOrder));
        return copy;
    }

    protected convertList(internalOrders: InternalOrder[]): InternalOrder[] {
        const copy: InternalOrder[] = internalOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: InternalOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
