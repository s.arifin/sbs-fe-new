import { BaseEntity } from './../../shared';

export class DocumentType implements BaseEntity {
    constructor(
        public id?: number,
        public idDocumentType?: number,
        public description?: string,
        public idParent?: number,
    ) {
    }
}
