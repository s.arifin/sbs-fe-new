import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { DocumentType } from './document-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class DocumentTypeService {
    protected itemValues: DocumentType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = process.env.API_C_URL + '/api/document-types';
    protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/document-types';

    constructor(protected http: Http) { }

    create(documentType: DocumentType): Observable<DocumentType> {
        const copy = this.convert(documentType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(documentType: DocumentType): Observable<DocumentType> {
        const copy = this.convert(documentType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<DocumentType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    public findByIdParent(id: number): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/by-id-parent/' + id).map(
            (res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, documentType: DocumentType): Observable<DocumentType> {
        const copy = this.convert(documentType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, documentTypes: DocumentType[]): Observable<DocumentType[]> {
        const copy = this.convertList(documentTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(documentType: DocumentType): DocumentType {
        if (documentType === null || documentType === {}) {
            return {};
        }
        // const copy: DocumentType = Object.assign({}, documentType);
        const copy: DocumentType = JSON.parse(JSON.stringify(documentType));
        return copy;
    }

    protected convertList(documentTypes: DocumentType[]): DocumentType[] {
        const copy: DocumentType[] = documentTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: DocumentType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
