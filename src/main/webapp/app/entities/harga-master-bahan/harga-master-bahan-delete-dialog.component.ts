import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HargaMasterBahan } from './harga-master-bahan.model';
import { HargaMasterBahanPopupService } from './harga-master-bahan-popup.service';
import { HargaMasterBahanService } from './harga-master-bahan.service';

@Component({
    selector: 'jhi-harga-master-bahan-delete-dialog',
    templateUrl: './harga-master-bahan-delete-dialog.component.html'
})
export class HargaMasterBahanDeleteDialogComponent {

    hargaMasterBahan: HargaMasterBahan;

    constructor(
        private hargaMasterBahanService: HargaMasterBahanService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.hargaMasterBahanService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'hargaMasterBahanListModification',
                content: 'Deleted an hargaMasterBahan'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-harga-master-bahan-delete-popup',
    template: ''
})
export class HargaMasterBahanDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hargaMasterBahanPopupService: HargaMasterBahanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.hargaMasterBahanPopupService
                .open(HargaMasterBahanDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
