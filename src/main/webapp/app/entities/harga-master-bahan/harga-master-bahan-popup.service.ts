import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HargaMasterBahan } from './harga-master-bahan.model';
import { HargaMasterBahanService } from './harga-master-bahan.service';

@Injectable()
export class HargaMasterBahanPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private hargaMasterBahanService: HargaMasterBahanService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.hargaMasterBahanService.find(id).subscribe((hargaMasterBahan) => {
                    this.ngbModalRef = this.hargaMasterBahanModalRef(component, hargaMasterBahan);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.hargaMasterBahanModalRef(component, new HargaMasterBahan());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    hargaMasterBahanModalRef(component: Component, hargaMasterBahan: HargaMasterBahan): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.hargaMasterBahan = hargaMasterBahan;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
