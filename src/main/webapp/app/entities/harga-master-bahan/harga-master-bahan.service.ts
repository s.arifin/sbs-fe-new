import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { HargaMasterBahan } from './harga-master-bahan.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class HargaMasterBahanService {

    private resourceUrl = process.env.API_C_URL + '/api/harga-master-bahans';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/harga-master-bahans';

    constructor(private http: Http) { }

    create(hargaMasterBahan: HargaMasterBahan): Observable<HargaMasterBahan> {
        const copy = this.convert(hargaMasterBahan);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(hargaMasterBahan: HargaMasterBahan): Observable<HargaMasterBahan> {
        const copy = this.convert(hargaMasterBahan);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<HargaMasterBahan> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(hargaMasterBahan: HargaMasterBahan): HargaMasterBahan {
        const copy: HargaMasterBahan = Object.assign({}, hargaMasterBahan);
        return copy;
    }
}
