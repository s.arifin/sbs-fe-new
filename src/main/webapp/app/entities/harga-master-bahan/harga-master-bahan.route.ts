import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HargaMasterBahanComponent } from './harga-master-bahan.component';
import { HargaMasterBahanDetailComponent } from './harga-master-bahan-detail.component';
import { HargaMasterBahanPopupComponent } from './harga-master-bahan-dialog.component';
import { HargaMasterBahanDeletePopupComponent } from './harga-master-bahan-delete-dialog.component';

@Injectable()
export class HargaMasterBahanResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const hargaMasterBahanRoute: Routes = [
    {
        path: 'harga-master-bahan',
        component: HargaMasterBahanComponent,
        resolve: {
            'pagingParams': HargaMasterBahanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.hargaMasterBahan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'harga-master-bahan/:id',
        component: HargaMasterBahanDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.hargaMasterBahan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const hargaMasterBahanPopupRoute: Routes = [
    {
        path: 'harga-master-bahan-new',
        component: HargaMasterBahanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.hargaMasterBahan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'harga-master-bahan/:id/edit',
        component: HargaMasterBahanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.hargaMasterBahan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'harga-master-bahan/:id/delete',
        component: HargaMasterBahanDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.hargaMasterBahan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
