import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { HargaMasterBahan } from './harga-master-bahan.model';
import { HargaMasterBahanPopupService } from './harga-master-bahan-popup.service';
import { HargaMasterBahanService } from './harga-master-bahan.service';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-harga-master-bahan-dialog',
    templateUrl: './harga-master-bahan-dialog.component.html'
})
export class HargaMasterBahanDialogComponent implements OnInit {

    hargaMasterBahan: HargaMasterBahan;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private hargaMasterBahanService: HargaMasterBahanService,
        private eventManager: JhiEventManager,
        private loadingService: LoadingService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.hargaMasterBahan.kdbahan !== undefined) {
            this.loadingService.loadingStart();
            this.subscribeToSaveResponse(
                this.hargaMasterBahanService.update(this.hargaMasterBahan));
                this.loadingService.loadingStop();
        } else {
            this.loadingService.loadingStart();
            this.subscribeToSaveResponse(
                this.hargaMasterBahanService.create(this.hargaMasterBahan));
                this.loadingService.loadingStop();
        }
    }

    private subscribeToSaveResponse(result: Observable<HargaMasterBahan>) {
        result.subscribe((res: HargaMasterBahan) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: HargaMasterBahan) {
        this.eventManager.broadcast({ name: 'hargaMasterBahanListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-harga-master-bahan-popup',
    template: ''
})
export class HargaMasterBahanPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hargaMasterBahanPopupService: HargaMasterBahanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.hargaMasterBahanPopupService
                    .open(HargaMasterBahanDialogComponent as Component, params['id']);
            } else {
                this.hargaMasterBahanPopupService
                    .open(HargaMasterBahanDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
