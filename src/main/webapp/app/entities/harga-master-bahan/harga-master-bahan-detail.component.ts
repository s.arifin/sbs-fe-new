import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { HargaMasterBahan } from './harga-master-bahan.model';
import { HargaMasterBahanService } from './harga-master-bahan.service';

@Component({
    selector: 'jhi-harga-master-bahan-detail',
    templateUrl: './harga-master-bahan-detail.component.html'
})
export class HargaMasterBahanDetailComponent implements OnInit, OnDestroy {

    hargaMasterBahan: HargaMasterBahan;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private hargaMasterBahanService: HargaMasterBahanService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHargaMasterBahans();
    }

    load(id) {
        this.hargaMasterBahanService.find(id).subscribe((hargaMasterBahan) => {
            this.hargaMasterBahan = hargaMasterBahan;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHargaMasterBahans() {
        this.eventSubscriber = this.eventManager.subscribe(
            'hargaMasterBahanListModification',
            (response) => this.load(this.hargaMasterBahan.id)
        );
    }
}
