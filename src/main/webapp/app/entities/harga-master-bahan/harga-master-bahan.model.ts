import { BaseEntity } from './../../shared';

export class HargaMasterBahan implements BaseEntity {
    constructor(
        public id?: number,
        public kdbahan?: string,
        public nmbahan?: string,
        public tpbahan?: string,
        public harga_dasar?: number,
    ) {
    }
}
