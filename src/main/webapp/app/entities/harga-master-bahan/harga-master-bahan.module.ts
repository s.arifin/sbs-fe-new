import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    HargaMasterBahanService,
    HargaMasterBahanPopupService,
    HargaMasterBahanComponent,
    HargaMasterBahanDetailComponent,
    HargaMasterBahanDialogComponent,
    HargaMasterBahanPopupComponent,
    HargaMasterBahanDeletePopupComponent,
    HargaMasterBahanDeleteDialogComponent,
    hargaMasterBahanRoute,
    hargaMasterBahanPopupRoute,
    HargaMasterBahanResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...hargaMasterBahanRoute,
    ...hargaMasterBahanPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        HargaMasterBahanComponent,
        HargaMasterBahanDetailComponent,
        HargaMasterBahanDialogComponent,
        HargaMasterBahanDeleteDialogComponent,
        HargaMasterBahanPopupComponent,
        HargaMasterBahanDeletePopupComponent,
    ],
    entryComponents: [
        HargaMasterBahanComponent,
        HargaMasterBahanDialogComponent,
        HargaMasterBahanPopupComponent,
        HargaMasterBahanDeleteDialogComponent,
        HargaMasterBahanDeletePopupComponent,
    ],
    providers: [
        HargaMasterBahanService,
        HargaMasterBahanPopupService,
        HargaMasterBahanResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmHargaMasterBahanModule {}
