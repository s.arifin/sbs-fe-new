import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { CalendarDayOff } from './calendar-day-off.model';
import { CalendarDayOffService } from './calendar-day-off.service';

@Injectable()
export class CalendarDayOffPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private calendarDayOffService: CalendarDayOffService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.calendarDayOffService.find(id).subscribe((calendarDayOff) => {
                    calendarDayOff.dtdayoff = this.datePipe
                        .transform(calendarDayOff.dtdayoff, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.calendarDayOffModalRef(component, calendarDayOff);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.calendarDayOffModalRef(component, new CalendarDayOff());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    calendarDayOffModalRef(component: Component, calendarDayOff: CalendarDayOff): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.calendarDayOff = calendarDayOff;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
