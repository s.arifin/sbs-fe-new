import { BaseEntity } from './../../shared';

export class CalendarDayOff implements BaseEntity {
    constructor(
        public id?: number,
        public iddayoff?: string,
        public dtdayoff?: any,
        public description?: string,
        public status?: number,
    ) {
    }
}
