import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CalendarDayOff } from './calendar-day-off.model';
import { CalendarDayOffPopupService } from './calendar-day-off-popup.service';
import { CalendarDayOffService } from './calendar-day-off.service';

@Component({
    selector: 'jhi-calendar-day-off-delete-dialog',
    templateUrl: './calendar-day-off-delete-dialog.component.html'
})
export class CalendarDayOffDeleteDialogComponent {

    calendarDayOff: CalendarDayOff;

    constructor(
        private calendarDayOffService: CalendarDayOffService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.calendarDayOffService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'calendarDayOffListModification',
                content: 'Deleted an calendarDayOff'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-calendar-day-off-delete-popup',
    template: ''
})
export class CalendarDayOffDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private calendarDayOffPopupService: CalendarDayOffPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.calendarDayOffPopupService
                .open(CalendarDayOffDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
