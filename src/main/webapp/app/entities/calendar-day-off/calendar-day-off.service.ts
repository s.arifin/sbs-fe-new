import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CalendarDayOff } from './calendar-day-off.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CalendarDayOffService {

    private resourceUrl = process.env.API_C_URL + '/api/calendar-day-offs';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/calendar-day-offs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(calendarDayOff: CalendarDayOff): Observable<CalendarDayOff> {
        const copy = this.convert(calendarDayOff);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(calendarDayOff: CalendarDayOff): Observable<CalendarDayOff> {
        const copy = this.convert(calendarDayOff);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<CalendarDayOff> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtdayoff = this.dateUtils
            .convertDateTimeFromServer(entity.dtdayoff);
    }

    private convert(calendarDayOff: CalendarDayOff): CalendarDayOff {
        const copy: CalendarDayOff = Object.assign({}, calendarDayOff);

        // copy.dtdayoff = this.dateUtils.toDate(calendarDayOff.dtdayoff);
        return copy;
    }
}
