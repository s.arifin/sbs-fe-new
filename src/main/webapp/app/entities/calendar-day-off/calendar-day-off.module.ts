import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    CalendarDayOffService,
    CalendarDayOffPopupService,
    CalendarDayOffComponent,
    CalendarDayOffDetailComponent,
    CalendarDayOffDialogComponent,
    CalendarDayOffPopupComponent,
    CalendarDayOffDeletePopupComponent,
    CalendarDayOffDeleteDialogComponent,
    calendarDayOffRoute,
    calendarDayOffPopupRoute,
    CalendarDayOffResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...calendarDayOffRoute,
    ...calendarDayOffPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CalendarDayOffComponent,
        CalendarDayOffDetailComponent,
        CalendarDayOffDialogComponent,
        CalendarDayOffDeleteDialogComponent,
        CalendarDayOffPopupComponent,
        CalendarDayOffDeletePopupComponent,
    ],
    entryComponents: [
        CalendarDayOffComponent,
        CalendarDayOffDialogComponent,
        CalendarDayOffPopupComponent,
        CalendarDayOffDeleteDialogComponent,
        CalendarDayOffDeletePopupComponent,
    ],
    providers: [
        CalendarDayOffService,
        CalendarDayOffPopupService,
        CalendarDayOffResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmCalendarDayOffModule {}
