import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CalendarDayOff } from './calendar-day-off.model';
import { CalendarDayOffService } from './calendar-day-off.service';

@Component({
    selector: 'jhi-calendar-day-off-detail',
    templateUrl: './calendar-day-off-detail.component.html'
})
export class CalendarDayOffDetailComponent implements OnInit, OnDestroy {

    calendarDayOff: CalendarDayOff;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private calendarDayOffService: CalendarDayOffService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCalendarDayOffs();
    }

    load(id) {
        this.calendarDayOffService.find(id).subscribe((calendarDayOff) => {
            this.calendarDayOff = calendarDayOff;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCalendarDayOffs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'calendarDayOffListModification',
            (response) => this.load(this.calendarDayOff.id)
        );
    }
}
