import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CalendarDayOff } from './calendar-day-off.model';
import { CalendarDayOffPopupService } from './calendar-day-off-popup.service';
import { CalendarDayOffService } from './calendar-day-off.service';

@Component({
    selector: 'jhi-calendar-day-off-dialog',
    templateUrl: './calendar-day-off-dialog.component.html'
})
export class CalendarDayOffDialogComponent implements OnInit {

    calendarDayOff: CalendarDayOff;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private calendarDayOffService: CalendarDayOffService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.calendarDayOff.iddayoff !== undefined) {
            this.subscribeToSaveResponse(
                this.calendarDayOffService.update(this.calendarDayOff));
        } else {
            this.subscribeToSaveResponse(
                this.calendarDayOffService.create(this.calendarDayOff));
        }
    }

    private subscribeToSaveResponse(result: Observable<CalendarDayOff>) {
        result.subscribe((res: CalendarDayOff) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CalendarDayOff) {
        this.eventManager.broadcast({ name: 'calendarDayOffListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-calendar-day-off-popup',
    template: ''
})
export class CalendarDayOffPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private calendarDayOffPopupService: CalendarDayOffPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.calendarDayOffPopupService
                    .open(CalendarDayOffDialogComponent as Component, params['id']);
            } else {
                this.calendarDayOffPopupService
                    .open(CalendarDayOffDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
