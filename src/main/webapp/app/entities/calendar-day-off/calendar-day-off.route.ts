import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CalendarDayOffComponent } from './calendar-day-off.component';
import { CalendarDayOffDetailComponent } from './calendar-day-off-detail.component';
import { CalendarDayOffPopupComponent } from './calendar-day-off-dialog.component';
import { CalendarDayOffDeletePopupComponent } from './calendar-day-off-delete-dialog.component';

@Injectable()
export class CalendarDayOffResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const calendarDayOffRoute: Routes = [
    {
        path: 'calendar-day-off',
        component: CalendarDayOffComponent,
        resolve: {
            'pagingParams': CalendarDayOffResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarDayOff.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'calendar-day-off/:id',
        component: CalendarDayOffDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarDayOff.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const calendarDayOffPopupRoute: Routes = [
    {
        path: 'calendar-day-off-new',
        component: CalendarDayOffPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarDayOff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'calendar-day-off/:id/edit',
        component: CalendarDayOffPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarDayOff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'calendar-day-off/:id/delete',
        component: CalendarDayOffDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.calendarDayOff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
