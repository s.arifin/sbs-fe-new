import { BaseEntity } from './../../shared';

export class InvoiceItem implements BaseEntity {
    constructor(
        public id?: any,
        public idinvdet?: any,
        public idinv?: any,
        public nobbm?: string,
        public nopo?: string,
        public donum?: string,
        public nolhp?: string,
        public statuslhp?: string,
        public produkstatus?: string,
        public productcode?: string,
        public productname?: string,
        public qty?: number,
        public price?: number,
        public note?: number,
        public totalprice?: number,
        public totaldisc?: number,
        public discpercent?: number,
        public discprop?: number,
        public qtypaid?: number, // untuk dilempar update ke detail bbm
        public iddetailbbm?: any, // untuk dilempar update ke detail bbm
        public totalprint?: number, // untuk penyimpanan sementara
        public discitempo?: number,
        public totalppn?: number,
    ) {
    }
}
