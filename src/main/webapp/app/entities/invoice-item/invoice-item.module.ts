import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    InvoiceItemService,
    InvoiceItemPopupService,
    InvoiceItemComponent,
    InvoiceItemDetailComponent,
    InvoiceItemDialogComponent,
    InvoiceItemPopupComponent,
    InvoiceItemDeletePopupComponent,
    InvoiceItemDeleteDialogComponent,
    invoiceItemRoute,
    invoiceItemPopupRoute,
    InvoiceItemResolvePagingParams,
} from './';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
} from 'primeng/primeng';

const ENTITY_STATES = [
    ...invoiceItemRoute,
    ...invoiceItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
    ],
    declarations: [
        InvoiceItemComponent,
        InvoiceItemDetailComponent,
        InvoiceItemDialogComponent,
        InvoiceItemDeleteDialogComponent,
        InvoiceItemPopupComponent,
        InvoiceItemDeletePopupComponent,
    ],
    entryComponents: [
        InvoiceItemComponent,
        InvoiceItemDialogComponent,
        InvoiceItemPopupComponent,
        InvoiceItemDeleteDialogComponent,
        InvoiceItemDeletePopupComponent,
    ],
    providers: [
        InvoiceItemService,
        InvoiceItemPopupService,
        InvoiceItemResolvePagingParams,
    ],
    exports: [
        InvoiceItemComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmInvoiceItemModule { }
