import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { InvoiceItem } from './invoice-item.model';
import { InvoiceItemPopupService } from './invoice-item-popup.service';
import { InvoiceItemService } from './invoice-item.service';

@Component({
    selector: 'jhi-invoice-item-dialog',
    templateUrl: './invoice-item-dialog.component.html'
})
export class InvoiceItemDialogComponent implements OnInit {

    invoiceItem: InvoiceItem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private invoiceItemService: InvoiceItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.invoiceItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.invoiceItemService.update(this.invoiceItem));
        } else {
            this.subscribeToSaveResponse(
                this.invoiceItemService.create(this.invoiceItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<InvoiceItem>) {
        result.subscribe((res: InvoiceItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: InvoiceItem) {
        this.eventManager.broadcast({ name: 'invoiceItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-invoice-item-popup',
    template: ''
})
export class InvoiceItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private invoiceItemPopupService: InvoiceItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.invoiceItemPopupService
                    .open(InvoiceItemDialogComponent as Component, params['id']);
            } else {
                this.invoiceItemPopupService
                    .open(InvoiceItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
