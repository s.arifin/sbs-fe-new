import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { InvoiceItem } from './invoice-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class InvoiceItemService {

    private resourceUrl = process.env.API_C_URL + '/api/invoice-items';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/invoice-items';

    constructor(private http: Http) { }

    create(invoiceItem: InvoiceItem): Observable<InvoiceItem> {
        const copy = this.convert(invoiceItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(invoiceItem: InvoiceItem): Observable<InvoiceItem> {
        const copy = this.convert(invoiceItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<InvoiceItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(invoiceItem: InvoiceItem): InvoiceItem {
        const copy: InvoiceItem = Object.assign({}, invoiceItem);
        return copy;
    }

    queryByIdInv(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-idinv', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findDistinct(id: any): Observable<InvoiceItem> {
        return this.http.get(this.resourceUrl + '/get-items-distinct/' + id).map((res: Response) => {
            return res.json();
        });
    }

    deletebyid(invoiceItem: InvoiceItem): Observable<InvoiceItem> {
        const copy = this.convert(invoiceItem);
        return this.http.post(this.resourceUrl + '/delete-items', copy).map((res: Response) => {
            return res.json();
        });
    }
}
