import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitDocumentMessage } from './unit-document-message.model';
import { UnitDocumentMessagePopupService } from './unit-document-message-popup.service';
import { UnitDocumentMessageService } from './unit-document-message.service';
import { ToasterService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-document-message-dialog',
    templateUrl: './unit-document-message-dialog.component.html'
})
export class UnitDocumentMessageDialogComponent implements OnInit {

    unitDocumentMessage: UnitDocumentMessage;
    isSaving: boolean;
    idMessage: any;

    unitdocumentmessages: UnitDocumentMessage[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.unitDocumentMessageService.query()
            .subscribe((res: ResponseWrapper) => { this.unitdocumentmessages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitDocumentMessage.idMessage !== undefined) {
            this.subscribeToSaveResponse(
                this.unitDocumentMessageService.update(this.unitDocumentMessage));
        } else {
            this.subscribeToSaveResponse(
                this.unitDocumentMessageService.create(this.unitDocumentMessage));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitDocumentMessage>) {
        result.subscribe((res: UnitDocumentMessage) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UnitDocumentMessage) {
        this.eventManager.broadcast({ name: 'unitDocumentMessageListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitDocumentMessage saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'unitDocumentMessage Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUnitDocumentMessageById(index: number, item: UnitDocumentMessage) {
        return item.idMessage;
    }
}

@Component({
    selector: 'jhi-unit-document-message-popup',
    template: ''
})
export class UnitDocumentMessagePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitDocumentMessagePopupService: UnitDocumentMessagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitDocumentMessagePopupService
                    .open(UnitDocumentMessageDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.unitDocumentMessagePopupService.parent = params['parent'];
                this.unitDocumentMessagePopupService
                    .open(UnitDocumentMessageDialogComponent as Component);
            } else {
                this.unitDocumentMessagePopupService
                    .open(UnitDocumentMessageDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
