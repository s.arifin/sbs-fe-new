import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UnitDocumentMessage, CustomUnitDocumentMessage, CustomUnitModel } from './unit-document-message.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UnitDocumentMessageService {
    protected itemValues: UnitDocumentMessage[];
    values: Subject<any> = new Subject<any>();
    valuesUnitDocumentMessage: {};

    protected enheritancedata = new BehaviorSubject<any>(this.valuesUnitDocumentMessage);
    passingData = this.enheritancedata.asObservable();

    protected resourceUrl =  process.env.API_C_URL + '/api/unit-document-messages';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/unit-document-messages';
    protected resourceCUrl = process.env.API_C_URL + '/api/SalesNotes';
    protected dummy = 'http://localhost:52374/api/SalesNotes';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    public getDataByApprovalStatusAndRequirement(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-status-join-requirement', options)
            .map((res: Response) => this.convertResponse(res));
        // const httpURL = this.resourceUrl + '/by-status-join-requirement/' + idStatus;
        // return this.http.get(httpURL).map((res: Response) => {
            // return res.json();
        // });
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    printSave(unitDocumentMessages: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessages);
        const options: BaseRequestOptions = new BaseRequestOptions();
        // const params: URLSearchParams = new URLSearchParams();
        // params.set('query', copy);
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl + '/saveprint'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    createRequestnote(unitDocumentMessages: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessages);
        const options: BaseRequestOptions = new BaseRequestOptions();
        // const params: URLSearchParams = new URLSearchParams();
        // params.set('query', copy);
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl + '/createRequestNote'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    pillihCabangC(customUnitModel: CustomUnitModel[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(customUnitModel);

        const options: BaseRequestOptions = new BaseRequestOptions();
        // const params: URLSearchParams = new URLSearchParams();
        // params.set('query', copy);
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl + '/pilihcabang'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    queryFilterByinternalC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    setSelectedC(unitDocumentMessages: UnitDocumentMessage[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessages);

        const options = new RequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}`, copy, options).map((res: Response) => {
            return res;
        });
    }

    public getDataByApprovalStatus(idStatus: number): Observable<ResponseWrapper> {
        const httpURL = this.resourceUrl + '/by-status/' + idStatus;
        return this.http.get(httpURL).map((res: Response) => this.convertResponse(res));
    }

    public getDataByIdRequirementAndApprovalStatus(idReq: string, idStatus: number): Observable<ResponseWrapper> {
        const httpURL = this.resourceUrl + '/by-requirement/' + idReq + '/' + idStatus;
        return this.http.get(httpURL).map((res: Response) => this.convertResponse(res));
    }

    public getDataByIdRequirement(idReq: string): Observable<ResponseWrapper> {
        const httpURL = this.resourceUrl + '/by-requirement/' + idReq;
        return this.http.get(httpURL).map((res: Response) => this.convertResponse(res));
    }

    create(unitDocumentMessage: UnitDocumentMessage): Observable<UnitDocumentMessage> {
        const copy = this.convert(unitDocumentMessage);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(unitDocumentMessage: UnitDocumentMessage): Observable<UnitDocumentMessage> {
        const copy = this.convert(unitDocumentMessage);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<UnitDocumentMessage> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-status-join-requirement', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<UnitDocumentMessage> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcessData(id: Number, param: String, unitDocumentMessage: UnitDocumentMessage): Observable<UnitDocumentMessage> {
        const copy = this.convert(unitDocumentMessage);
        return this.http.post(`${this.resourceUrl}/execute-data/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<UnitDocumentMessage[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    public changeStatus(unitDocumentMessage: UnitDocumentMessage, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(unitDocumentMessage);
        return this.http.post(`${this.resourceUrl}/set-status-approval/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UnitDocumentMessage.
     */
    protected convertItemFromServer(json: any): UnitDocumentMessage {
        const entity: UnitDocumentMessage = Object.assign(new UnitDocumentMessage(), json);
        if (entity.dateCreated) {
            entity.dateCreated = new Date(entity.dateCreated);
        }
        return entity;
    }

    /**
     * Convert a UnitDocumentMessage to a JSON which can be sent to the server.
     */
    protected convert(unitDocumentMessage: UnitDocumentMessage): UnitDocumentMessage {
        if (unitDocumentMessage === null || unitDocumentMessage === {}) {
            return {};
        }
        // const copy: UnitDocumentMessage = Object.assign({}, unitDocumentMessage);
        const copy: UnitDocumentMessage = JSON.parse(JSON.stringify(unitDocumentMessage));

        // copy.dateCreated = this.dateUtils.toDate(unitDocumentMessage.dateCreated);
        return copy;
    }

    protected convertList(unitDocumentMessages: UnitDocumentMessage[]): UnitDocumentMessage[] {
        const copy: UnitDocumentMessage[] = unitDocumentMessages;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: UnitDocumentMessage[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
