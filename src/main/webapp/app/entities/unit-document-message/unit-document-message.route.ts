import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UnitDocumentMessageComponent } from './unit-document-message.component';
import { UnitDocumentMessagePopupComponent } from './unit-document-message-dialog.component';

@Injectable()
export class UnitDocumentMessageResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idMessage,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unitDocumentMessageRoute: Routes = [
    {
        path: 'unit-document-message',
        component: UnitDocumentMessageComponent,
        resolve: {
            'pagingParams': UnitDocumentMessageResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDocumentMessage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitDocumentMessagePopupRoute: Routes = [
    {
        path: 'unit-document-message-popup-new-list/:parent',
        component: UnitDocumentMessagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDocumentMessage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-document-message-new',
        component: UnitDocumentMessagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDocumentMessage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-document-message/:id/edit',
        component: UnitDocumentMessagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDocumentMessage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
