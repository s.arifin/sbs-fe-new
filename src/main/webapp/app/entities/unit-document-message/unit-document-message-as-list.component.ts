import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDocumentMessage } from './unit-document-message.model';
import { UnitDocumentMessageService } from './unit-document-message.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-unit-document-message-as-list',
    templateUrl: './unit-document-message-as-list.component.html'
})
export class UnitDocumentMessageAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idMessage: any;

    currentAccount: any;
    unitDocumentMessages: UnitDocumentMessage[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: Number;

    constructor(
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idMessage';
        this.reverse = 'asc';
    }

    loadAll() {
        this.unitDocumentMessageService.queryFilterBy({
            filterName: 'relation',
            idMessage: this.idMessage,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/unit-document-message', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUnitDocumentMessages();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idMessage']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitDocumentMessage) {
        return item.idMessage;
    }

    registerChangeInUnitDocumentMessages() {
        this.eventSubscriber = this.eventManager.subscribe('unitDocumentMessageListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idMessage') {
            result.push('idMessage');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDocumentMessages = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    // executeProcess(item) {
    //     this.unitDocumentMessageService.executeProcess(item).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.unitDocumentMessageService.update(event.data)
                .subscribe((res: UnitDocumentMessage) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.unitDocumentMessageService.create(event.data)
                .subscribe((res: UnitDocumentMessage) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: UnitDocumentMessage) {
        this.toasterService.showToaster('info', 'UnitDocumentMessage Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitDocumentMessageService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'unitDocumentMessageListModification',
                        content: 'Deleted an unitDocumentMessage'
                    });
                });
            }
        });
    }
}
