import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DetailBBM } from './detail-bbm.model';
import { DetailBBMPopupService } from './detail-bbm-popup.service';
import { DetailBBMService } from './detail-bbm.service';

@Component({
    selector: 'jhi-detail-bbm-delete-dialog',
    templateUrl: './detail-bbm-delete-dialog.component.html'
})
export class DetailBBMDeleteDialogComponent {

    detailBBM: DetailBBM;

    constructor(
        private detailBBMService: DetailBBMService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.detailBBMService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'detailBBMListModification',
                content: 'Deleted an detailBBM'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-detail-bbm-delete-popup',
    template: ''
})
export class DetailBBMDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailBBMPopupService: DetailBBMPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.detailBBMPopupService
                .open(DetailBBMDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
