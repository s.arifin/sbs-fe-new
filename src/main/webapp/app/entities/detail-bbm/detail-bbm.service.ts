import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DetailBBM } from './detail-bbm.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DetailBBMService {

    private resourceUrl = SERVER_API_URL + 'api/detail-bbms';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/detail-bbms';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(detailBBM: DetailBBM): Observable<DetailBBM> {
        const copy = this.convert(detailBBM);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(detailBBM: DetailBBM): Observable<DetailBBM> {
        const copy = this.convert(detailBBM);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<DetailBBM> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.tgl_exp = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_exp);
        entity.tgl_exphal = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_exphal);
        entity.tgl_mfd = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_mfd);
        entity.tgl_input = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_input);
        entity.jam_input = this.dateUtils
            .convertDateTimeFromServer(entity.jam_input);
    }

    private convert(detailBBM: DetailBBM): DetailBBM {
        const copy: DetailBBM = Object.assign({}, detailBBM);

        copy.tgl_exp = this.dateUtils.toDate(detailBBM.tgl_exp);

        copy.tgl_exphal = this.dateUtils.toDate(detailBBM.tgl_exphal);

        copy.tgl_mfd = this.dateUtils.toDate(detailBBM.tgl_mfd);

        copy.tgl_input = this.dateUtils.toDate(detailBBM.tgl_input);

        copy.jam_input = this.dateUtils.toDate(detailBBM.jam_input);
        return copy;
    }
}
