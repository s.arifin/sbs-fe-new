import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DetailBBM } from './detail-bbm.model';
import { DetailBBMPopupService } from './detail-bbm-popup.service';
import { DetailBBMService } from './detail-bbm.service';

@Component({
    selector: 'jhi-detail-bbm-dialog',
    templateUrl: './detail-bbm-dialog.component.html'
})
export class DetailBBMDialogComponent implements OnInit {

    detailBBM: DetailBBM;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private detailBBMService: DetailBBMService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.detailBBM.id !== undefined) {
            this.subscribeToSaveResponse(
                this.detailBBMService.update(this.detailBBM));
        } else {
            this.subscribeToSaveResponse(
                this.detailBBMService.create(this.detailBBM));
        }
    }

    private subscribeToSaveResponse(result: Observable<DetailBBM>) {
        result.subscribe((res: DetailBBM) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DetailBBM) {
        this.eventManager.broadcast({ name: 'detailBBMListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-detail-bbm-popup',
    template: ''
})
export class DetailBBMPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailBBMPopupService: DetailBBMPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.detailBBMPopupService
                    .open(DetailBBMDialogComponent as Component, params['id']);
            } else {
                this.detailBBMPopupService
                    .open(DetailBBMDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
