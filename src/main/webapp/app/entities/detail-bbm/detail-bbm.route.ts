import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DetailBBMComponent } from './detail-bbm.component';
import { DetailBBMDetailComponent } from './detail-bbm-detail.component';
import { DetailBBMPopupComponent } from './detail-bbm-dialog.component';
import { DetailBBMDeletePopupComponent } from './detail-bbm-delete-dialog.component';

@Injectable()
export class DetailBBMResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const detailBBMRoute: Routes = [
    {
        path: 'detail-bbm',
        component: DetailBBMComponent,
        resolve: {
            'pagingParams': DetailBBMResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBBM.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'detail-bbm/:id',
        component: DetailBBMDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBBM.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const detailBBMPopupRoute: Routes = [
    {
        path: 'detail-bbm-new',
        component: DetailBBMPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBBM.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-bbm/:id/edit',
        component: DetailBBMPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBBM.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-bbm/:id/delete',
        component: DetailBBMDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailBBM.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
