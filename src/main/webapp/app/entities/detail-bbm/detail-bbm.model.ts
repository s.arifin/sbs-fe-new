import { BaseEntity } from './../../shared';

export class DetailBBM implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: string,
        public kd_bahan?: string,
        public qty?: number,
        public satuan?: string,
        public no_po?: string,
        public no_batch?: string,
        public keterangan?: string,
        public tgl_exp?: any,
        public tgl_exphal?: any,
        public tgl_mfd?: any,
        public lokasi?: string,
        public no_lpm?: string,
        public id_user?: string,
        public tgl_input?: any,
        public jam_input?: any,
        public nm_bahan?: string,
        public qty_sisa?: number,
        public qty_masuk?: number,
        public idpurchasingitem?: any,
    ) {
    }
}
