import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DetailBBM } from './detail-bbm.model';
import { DetailBBMService } from './detail-bbm.service';

@Component({
    selector: 'jhi-detail-bbm-detail',
    templateUrl: './detail-bbm-detail.component.html'
})
export class DetailBBMDetailComponent implements OnInit, OnDestroy {

    detailBBM: DetailBBM;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private detailBBMService: DetailBBMService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDetailBBMS();
    }

    load(id) {
        this.detailBBMService.find(id).subscribe((detailBBM) => {
            this.detailBBM = detailBBM;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDetailBBMS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'detailBBMListModification',
            (response) => this.load(this.detailBBM.id)
        );
    }
}
