import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    DetailBBMService,
    DetailBBMPopupService,
    DetailBBMComponent,
    DetailBBMDetailComponent,
    DetailBBMDialogComponent,
    DetailBBMPopupComponent,
    DetailBBMDeletePopupComponent,
    DetailBBMDeleteDialogComponent,
    detailBBMRoute,
    detailBBMPopupRoute,
    DetailBBMResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...detailBBMRoute,
    ...detailBBMPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DetailBBMComponent,
        DetailBBMDetailComponent,
        DetailBBMDialogComponent,
        DetailBBMDeleteDialogComponent,
        DetailBBMPopupComponent,
        DetailBBMDeletePopupComponent,
    ],
    entryComponents: [
        DetailBBMComponent,
        DetailBBMDialogComponent,
        DetailBBMPopupComponent,
        DetailBBMDeleteDialogComponent,
        DetailBBMDeletePopupComponent,
    ],
    providers: [
        DetailBBMService,
        DetailBBMPopupService,
        DetailBBMResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDetailBBMModule {}
