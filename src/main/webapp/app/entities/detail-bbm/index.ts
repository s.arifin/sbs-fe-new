export * from './detail-bbm.model';
export * from './detail-bbm-popup.service';
export * from './detail-bbm.service';
export * from './detail-bbm-dialog.component';
export * from './detail-bbm-delete-dialog.component';
export * from './detail-bbm-detail.component';
export * from './detail-bbm.component';
export * from './detail-bbm.route';
