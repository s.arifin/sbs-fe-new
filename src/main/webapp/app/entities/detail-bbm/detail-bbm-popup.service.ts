import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DetailBBM } from './detail-bbm.model';
import { DetailBBMService } from './detail-bbm.service';

@Injectable()
export class DetailBBMPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private detailBBMService: DetailBBMService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.detailBBMService.find(id).subscribe((detailBBM) => {
                    detailBBM.tgl_exp = this.datePipe
                        .transform(detailBBM.tgl_exp, 'yyyy-MM-ddTHH:mm:ss');
                    detailBBM.tgl_exphal = this.datePipe
                        .transform(detailBBM.tgl_exphal, 'yyyy-MM-ddTHH:mm:ss');
                    detailBBM.tgl_mfd = this.datePipe
                        .transform(detailBBM.tgl_mfd, 'yyyy-MM-ddTHH:mm:ss');
                    detailBBM.tgl_input = this.datePipe
                        .transform(detailBBM.tgl_input, 'yyyy-MM-ddTHH:mm:ss');
                    detailBBM.jam_input = this.datePipe
                        .transform(detailBBM.jam_input, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.detailBBMModalRef(component, detailBBM);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.detailBBMModalRef(component, new DetailBBM());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    detailBBMModalRef(component: Component, detailBBM: DetailBBM): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.detailBBM = detailBBM;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
