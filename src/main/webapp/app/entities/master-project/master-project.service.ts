import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, Headers } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
// import { MasterBahan } from './.model';
import { MasterProject } from './master-project.model';
import { MasterBahan } from '../master_bahan';

@Injectable()
export class MasterProjectService {
    protected itemValues: MasterProject[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/master-project';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/master-project';
    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    // create(picRole: MasterBahan): Observable<MasterBahan> {
    //     const copy = this.convert(picRole);
    //     return this.http.post(this.resourceUrl, copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }
    create(unitChecks: any): Observable<ResponseWrapper> {
        const copy = this.convert(unitChecks);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(unitChecks: any): Observable<ResponseWrapper> {
        const copy = this.convert(unitChecks);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getAllSatuan(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getSatuan', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAutority(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getallautority', options)
            .map((res: Response) => this.convertResponse(res));
    }

    saveEdit(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/saveEdit`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    cekNoTT(nott: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/cekProject/${nott}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convert(payment: MasterProject): MasterProject {
        const copy: MasterProject = Object.assign({}, payment);
        return copy;
    }

    private convertArray(payment: MasterProject[]): MasterProject[] {
        const copy: MasterProject[] = Object.assign([], payment);
        return copy;
    }

    find(id: any): Observable<MasterProject> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    changeReqAPPROVE(joborders: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(joborders);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl + '/changeReqAPPROVE'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    public getDataByStatus(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-status', options)
            .map((res: Response) => this.convertResponse(res));
    }

    public getDataByApprovalStatusAndRequirement(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/byStatus', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getArea(req?: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/getArea/${req}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getLokasi(req?: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/getLokasi/${req}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getGedung(req?: any): Observable<MasterBahan> {
        return this.http.get(`${this.resourceUrl}/getGedung/${req}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    getAppPPManagement(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval-register-pp-detail', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatusapp(unitChecks: any): Observable<ResponseWrapper> {
        const copy = this.convertArray(unitChecks);
        return this.http.post(this.resourceUrl + '/updateStatusApprove', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    changeStatusappn(unitChecks: any): Observable<ResponseWrapper> {
        const copy = this.convertArray(unitChecks);
        return this.http.post(this.resourceUrl + '/updateStatusNotApprove', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // update(picRole: MasterBahan): Observable<MasterBahan> {
    //     const copy = this.convert(picRole);
    //     return this.http.put(this.resourceUrl, copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }
    // find1(id: any): Observable<MasterBahan> {
    //     return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }

    // find(id: any): Observable<MasterBahan> {
    //     return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    // delete(id: any): Observable<Response> {
    //     return this.http.delete(`${this.resourceUrl}/${id}`);
    // }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }

    // private convert(picRole: MasterBahan): MasterBahan {
    //     const copy: MasterBahan = Object.assign({}, picRole);
    //     return copy;
    // }

    // getPtype() {
    //     return this.http.get(this.resourceUrl + '/tipe-produk').toPromise()
    //         .then((res) => <any[]>res.json())
    //         .then((data) => data);
    // }

}
