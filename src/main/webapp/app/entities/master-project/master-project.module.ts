import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { MasterProjectResolvePagingParams, MasterProjectRoute } from './master-project.route';
import { MasterProjectComponent } from './master-project.component';
import { MasterProjectService } from './master-project.service';
import { MasterProjectNewComponent } from './master-project-new.component';
import { MasterProjectEditComponent } from './master-project-edit.component';
import { ApprovalProjectComponent } from './approval-project/approval-project.component';
import { TabApprovalProjectComponent } from './approval-project/tab-approval-project.component';
import { TabViewModule } from 'primeng/primeng';

const ENTITY_STATES = [
    ...MasterProjectRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule
    ],
    declarations: [
        MasterProjectComponent,
        MasterProjectNewComponent,
        MasterProjectEditComponent,
        ApprovalProjectComponent,
        TabApprovalProjectComponent
    ],
    entryComponents: [
        MasterProjectComponent,
        MasterProjectNewComponent,
        MasterProjectEditComponent,
        ApprovalProjectComponent,
        TabApprovalProjectComponent
    ],
    providers: [
        MasterProjectService,
        MasterProjectResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterProjectModule { }
