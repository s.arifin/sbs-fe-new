import { Component, OnInit, OnChanges, OnDestroy, Output, SimpleChanges, Input, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import * as FileSaver from 'file-saver';
import { FileCompressionService } from '../../joborders';
import { array } from '@amcharts/amcharts4/core';
import { MasterProject } from '../master-project.model';
import { MasterProjectService } from '../master-project.service';

@Component({
    selector: 'jhi-approval-project',
    templateUrl: './approval-project.component.html'
})
export class ApprovalProjectComponent implements OnInit, OnDestroy {

    @Output()
    fnCallback = new EventEmitter();
    [x: string]: any;
    // this.fnCallback.emit(this.receipts);
    @Input()
    idstatus: number;
    attac_file: any;
    data: MasterProject;
    dataNewRow: MasterProject;
    currentAccount: any;
    isSaving: boolean;
    purchaseOrders: MasterProject[];
    purchaseOrdersReceive: MasterProject[];
    selected: MasterProject[];
    error: any;
    success: any;
    subscription: Subscription;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    po: PurchaseOrder;
    newModalNote: boolean;
    newModalAttachment: boolean;
    idreg: any;
    user: any;
    noteUnregPP: string;
    pp: MasterProject;
    selectedSTS: Number = 0;
    noreg: any;
    contentContentType: string;
    content: any;
    ppStatus: Array<object> = [
        { label: 'Pilih Status', value: '0' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Serah Management', value: '24' }
    ];
    isDisabled: Boolean = false;
    isview: boolean;
    pdfSrc: any;
    pdfBlob: any;
    purchaseOrder: MasterProject;
    ketNotApp: string;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    sizePdf: any;
    role: any;
    noteNotAppPP: string;
    constructor(
        public activeModal: NgbActiveModal,
        private purchaseOrderService: MasterProjectService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private purchasingService: MasterProjectService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private reportUtilService: ReportUtilService,
        private toasterService: ToasterService,
        private activatedRoute: ActivatedRoute,
        protected dataUtils: JhiDataUtils,
        private fileCompressionService: FileCompressionService,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<MasterProject>();
        this.idInternal = this.principal.getIdInternal();
        this.user = this.principal.getUserLogin();
        this.newModalNote = false;
        this.idreg = '';
        this.noteUnregPP = '';
        this.pp = new MasterProject();
        this.dataNewRow = new MasterProject();
        this.noreg = '';
        this.isview = false;
        this.purchaseOrder = new MasterProject();
        this.ketNotApp = '';
        this.newModalAttachment = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.attac_file = null;
        this.newModalNote = false;
        this.noteNotAppPP = '';
    }
    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.purchasingService.getAppPPManagement({
                page: this.page,
                size: this.itemsPerPage,
                query: 'nopo:' + this.currentSearch + '|status:' + this.idstatus
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.purchasingService.getAppPPManagement({
            page: this.page,
            size: this.itemsPerPage,
            query: 'nopo:' + this.currentSearch + '|status:' + this.idstatus
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        // this.loadingService.loadingStop();
        console.log(this.user);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.router.navigate(['/approval-project'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                // sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.role = this.currentAccount.authorities.find((roles) => roles === 'ROLE_MGR_PABRIK');
        });
        console.log(this.currentAccount);
        this.registerFromProcess();
        this.loadAll();
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = headers.get('X-Total-Count');
        this.purchaseOrders = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    back() {
        this.fnCallback.emit();
    }

    registerFromProcess() {
        this.eventManager.subscribe('approvalPPModified', () => this.loadAll());
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.dataNewRow, this.elementRef, field, fieldContentType, idInput);
        // Mengosongkan konten dan tipe konten
        this.dataNewRow[field] = null;
        this.dataNewRow[fieldContentType] = null;

        // Reset input file
        const fileInput = document.getElementById(idInput) as HTMLInputElement;
        if (fileInput) {
            fileInput.value = ''; // Reset file input
        }

        // Set flag untuk validasi required
        this.isInsertFile = ''; // Atur sesuai kebutuhan, misalnya null atau ''
    }

    openPDFAttach(content: any, contentType: any) {
        const byteCharacters = atob(content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': contentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const win = window.open(content);
        window.open(URL.createObjectURL(blobContent), '_blank');
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 5242880 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            } else if (bytes < 5242880) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
            this.sizePdf = event.target.files[0].size;
            alert('Ukuran File Melebihi Batas 10MB')
        }
    }

    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }

    backapp() {
        this.clearInputImage('content', 'contentContentType', 'file_content');
        this.newModalAttachment = false;
        this.dataNewRow = new PurchaseOrder();
        this.isInsertFile = null;
        this.attac_file = null;
        console.log(`ini back ${this.isInsertFile}`);
    }

    unregPP() {
        if (this.selected.length > 1) {
            this.confirmationService.confirm({
                message: 'Are you Sure that you want to Not Approved??',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.notApproved();
                }
            });
        } else {
            this.newModalNote = true;
        }
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Are you Sure that you want to Approved??',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
        // this.unitDocumentMessageService.passingCustomData(this.selected);
    }

    Approved() {
        // const obj = {
        //     idinternal: this.principal.getIdInternal(),
        //     createdBy: this.principal.getUserLogin(),
        //     idStatusType: 11,
        //     data: this.selected
        // }
        this.purchaseOrderService.changeStatusapp(this.selected).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    notApproved() {
        // const obj = {
        //     idinternal: this.principal.getIdInternal(),
        //     createdBy: this.principal.getUserLogin(),
        //     idStatusType: 11,
        //     data: this.selected
        // }
        // this.purchaseOrderService.changeStatusapp(this.selected).subscribe(
        //     (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
        if (this.selected.length <= 1) {
            this.selected[0].ket_not_approve = this.noteNotAppPP;
        } else {

        }

        this.purchaseOrderService.changeStatusappn(this.selected).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessApproved(data, headers) {
        this.eventManager.broadcast({ name: 'approvalProjectListModification', content: 'OK' });
        // this.toaster.showToaster('info', 'Save', 'Project Approved !');
        this.idDialogPP = null;
        this.newModalNote = false;
        this.noteNotAppPP = '';
        this.selected = new Array<MasterProject>();
        this.loadAll();
        this.loadingService.loadingStop();
    }

    backappNotApp() {
        this.noteNotAppPP = null;
        this.newModalNote = false;
    }
}
