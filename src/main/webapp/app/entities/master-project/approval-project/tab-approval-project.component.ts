import { Component, OnInit, OnChanges, OnDestroy, Output, SimpleChanges, Input, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'jhi-tab-approval-project',
    templateUrl: './tab-approval-project.component.html'
})
export class TabApprovalProjectComponent implements OnInit, OnDestroy {

    constructor(
    ) {

    }
    ngOnInit(): void {
    }
    ngOnDestroy(): void {
    }
}
