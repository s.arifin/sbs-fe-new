import { BaseEntity } from '../../shared';

export class MasterProject implements BaseEntity {
    constructor(
        public idproject?: any,
        public id?: Number,
        public kd_project?: any,
        public nm_project?: string,
        public budget?: Number,
        public dtfrom?: Date,
        public dtthru?: Date,
        public kd_div?: string,
        public pic?: string,
        public status?: string,
        public tipe?: string,
        public iduser?: string,
        public waktu_input?: Date,
        public pic2?: string,
        public dtapprove_1?: Date,
        public dtapprove_2?: Date,
        public iduser_edit?: string,
        public waktu_edit?: Date,
        public ket_not_approve?: string,
        public status_project?: Number,
        public keterangan_project?: string,
        public lokasi_project?: string,
        public inisial_project?: string,
        public biaya_project?: Number,
        public kd_perusahaan?: string,
        public area_project?: string,
        public gedung_project?: string,
    ) {
    }
}
