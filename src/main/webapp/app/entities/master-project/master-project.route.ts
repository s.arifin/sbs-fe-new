import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { MasterProjectComponent } from './master-project.component';
import { MasterProjectNewComponent } from './master-project-new.component';
import { MasterProjectEditComponent } from './master-project-edit.component';
import { TabApprovalProjectComponent } from './approval-project/tab-approval-project.component';

@Injectable()
export class MasterProjectResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const MasterProjectRoute: Routes = [
    {
        path: 'master-project',
        component: MasterProjectComponent,
        resolve: {
            'pagingParams': MasterProjectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.master_project.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-project-new',
        component: MasterProjectNewComponent,
        resolve: {
            'pagingParams': MasterProjectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.master_project.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'master-project-Edit/:route/:page/:id/edit',
        component: MasterProjectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.master_project.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'master-project/approval',
        component: TabApprovalProjectComponent,
        resolve: {
            'pagingParams': MasterProjectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.korsal.approval-unit-document-message'
        },
        canActivate: [UserRouteAccessService]
    },
    // // {
    // //     path: 'master-bahan-Edit',
    // //     component: MasterBahanEditComponent,
    // //     resolve: {
    // //         'pagingParams': MasterBahanResolvePagingParams
    // //     },
    // //     data: {
    // //         authorities: ['ROLE_USER'],
    // //         pageTitle: 'mpmApp.picrole.home.createLabel'
    // //     },
    // //     canActivate: [UserRouteAccessService]
    // // },
];

export const MasterProjectPopupRoute: Routes = [

    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
