import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MasterProject } from './master-project.model';
import { MasterProjectService } from './master-project.service';
import { Label } from '@amcharts/amcharts4/core';
import { MasterBahan } from '../master_bahan';

@Component({
    selector: 'jhi-master-project-new',
    templateUrl: './master-project-new.component.html'
})
export class MasterProjectNewComponent implements OnInit {
    [x: string]: any;
    picRole: MasterProject;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType: any;
    internals: any;
    newProj: any;
    newProjs: any[];
    routeId: number;
    isSaving: boolean;
    inKdBahan: string;
    inKdUmBahan: any;
    inNmBahan: any;
    inSatBahan: any;
    inSatBahan2: any;
    inSatBahan3: any;
    inQtybahan2: any;
    inQtybahan3: any;
    inTipeBahan: any;
    inPicBahan: any;
    selectedStatus: any;
    inputKategori: any;
    inputWadah: any;
    listKdBarang = [{}];
    listKdBarang1 = [{}];
    newSatuans2: any;
    newSatuans3: any;
    newSatuans4: any;

    listSatuan: Array<object> = [
        { label: 'Pilih Satuan', value: null },
    ];

    listArea: Array<object> = [{}];
    listLokasi: Array<object> = [{}];
    listGedung: Array<object> = [{}];

    listPerusahaan: Array<object> = [
        { label: 'Pilih Perusahaan', value: '' },
        { label: 'SBS', value: '01' },
        { label: 'SKG', value: '02' },
        { label: 'SMK', value: '03' },
    ];

    listWadah: Array<object> = [
        { label: 'Pilih Wadah', value: null },
        { label: 'Wadah', value: 'Wadah' },
        { label: 'Pallet', value: 'Pallet' },
    ];

    tipe: Array<object> = [
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' }
    ];
    tipe1: Array<object> = [
        { label: 'Pilih Tipe', value: null },
        { label: 'Packaging Material (PM)', value: 'PM' },
        { label: 'Raw Material (RM)', value: 'RM' },
        { label: 'General Item (GI)', value: 'GI' },
    ];
    tipe2: Array<object> = [
        { label: 'Pilih Kategori', value: null },
        { label: 'Barang', value: 'BARANG' },
        { label: 'Jasa', value: 'JASA' },
    ];
    status1: Array<object> = [
        { label: 'Pilih Status', value: null },
        { label: 'Aktif', value: 'Aktif' },
        { label: 'Non Aktif', value: 'Non Aktif' }
    ];
    inkdUmum: string;
    inputQr: string;
    inputHalal: string;
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: MasterProjectService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.inSatBahan = null;
        this.inSatBahan2 = null;
        this.inSatBahan3 = null;
        this.inQtybahan2 = null;
        this.inQtybahan3 = null;
        this.inTipeBahan = null;
        this.inputKategori = null;
        this.inputWadah = null;
        this.inputHalal = null;
        this.inputQr = null;
        this.picRole = new MasterProject();
        const now = new Date();
        let month = (now.getMonth() + 1).toString(); // Bulan mulai dari 0, jadi tambahkan 1
        if (month.length < 2) {
            month = '0' + month; // Jika kurang dari 2 digit, tambahkan '0' di depan
        }
        const year = now.getFullYear().toString().slice(-2); // Ambil dua digit terakhir tahun
        this.picRole.kd_project = `PRJK/${year}${month}/XXX`;
        this.picRole.status = '1';
    }
    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.masterBahanService.getAutority({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.approvedBy,
                    value: element.approvedBy
                });
            });
            this.newKdBarang.forEach((element) => {
                this.listKdBarang1.push({
                    label: element.approvedBy1,
                    value: element.approvedBy1
                });
            });
            this.listKdBarang1.shift();
            this.listKdBarang.shift();

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }
    trackId(index: number, item: MasterProject) {
        return item.id;
    }

    onKeyUp(x) { // appending the updated value to the variable
        if (this.internals === 'PPIC') {
            this.inkdUmum = x.target.value.slice(0, 6);
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    // public selectpic(isSelect?: boolean): void {
    //     this.picRole.name = this.newpic.login;
    // }

    // filterpTypeSingle(event) {
    //     const query = event.query;
    //     this.masterBahanService.getPtype().then((newPtype) => {
    //         this.filteredpType = this.filterpType(query, newPtype);
    //     });
    // }

    // // filterpType(query, newpType: any[]): any[] {

    // //     const filtered: any[] = [];
    // //     for (let i = 0; i < newpType.length; i++) {
    // //         const newpTypes = newpType[i];
    // //         if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
    // //             filtered.push(newpTypes);
    // //         }
    // //     }
    // //     return filtered;
    // // }
    // // public selectpType(isSelect?: boolean): void {
    //     this.picRole.type = this.newpType.pType;
    // }
    simpan() {
        this.loadingService.loadingStart();
        if (this.picRole.nm_project === null || this.picRole.nm_project === undefined || this.picRole.nm_project === '') {
            alert('Nama Project Tidak Boleh Kosong');
            this.loadingService.loadingStop();
        } else if (this.picRole.nm_project.length <= 1) {
            alert('Nama Project Tidak Boleh Kurang Dari 1 Karakter');
            this.loadingService.loadingStop();
        } else if (this.picRole.pic === null || this.picRole.pic === undefined || this.picRole.pic === '') {
            alert('PIC Approval Project 1 harus di pilih');
            this.loadingService.loadingStop();
        } else if (this.picRole.pic2 === null || this.picRole.pic2 === undefined || this.picRole.pic2 === '') {
            alert('PIC Approval Project 2 harus di pilih');
            this.loadingService.loadingStop();
        } else if (this.picRole.status === null || this.picRole.pic2 === undefined || this.picRole.pic2 === '') {
            alert('Status Harus Di Pilih');
            this.loadingService.loadingStop();
        } else {
            this.loadingService.loadingStart();
            this.masterBahanService.create(this.picRole)
                .subscribe(
                    (res: ResponseWrapper) =>
                        this.onSaveSuccess(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res)
                );
        }
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'MasterProjectListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Project saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Project Error', error._body);
        this.alertService.error(error._body, null, null);
    }
    previousState() {
        this.router.navigate(['/master-project', { page: this.paramPage }]);
    }
    back() {
        this.router.navigate(['/master-project', { page: this.paramPage }]);
    }

    cekNamaProject() {
        if (this.picRole.nm_project !== null && this.picRole.nm_project !== undefined && this.picRole.nm_project !== '') {
            this.masterBahanService.cekNoTT(this.picRole.nm_project)
                .subscribe(
                    (res: ResponseWrapper) => {
                        if (res.status === 200) {
                            alert('Nama Project sudah pernah di gunakan');
                            this.picRole.nm_project = ''
                        }
                        if (res.status === 404) {

                        }
                    }
                )
        }

    }

    reverseData() {
        this.newSatuans2 = null;
        this.newSatuans3 = null;
        this.newSatuans4 = null;
        this.listArea = [{ label: 'Pilih Area', value: '' }];
        this.listLokasi = [{ label: 'Pilih Lokasi', value: '' }];
        this.listGedung = [{ label: 'Pilih Gedung', value: '' }];
        if (this.picRole.kd_perusahaan === null || this.picRole.kd_perusahaan === undefined || this.picRole.kd_perusahaan === '') {
            alert('Pilih Perusahaan Terlebih Dahulu');
        } else if (this.picRole.kd_perusahaan === '01' || this.picRole.kd_perusahaan === '02' || this.picRole.kd_perusahaan === '03') {
            if (this.picRole.kd_perusahaan === '01') {
                this.masterBahanService.getArea(this.picRole.kd_perusahaan)
                    .subscribe(
                        (res1: MasterBahan) => {
                            this.newSatuans2 = res1;
                            this.newSatuans2.forEach((e) => {
                                this.listArea.push({
                                    label: e.nmBahan,
                                    value: e.idMasBahan
                                });
                            });
                        },
                        (res1: MasterBahan) => {

                        }
                    )
            } else if (this.picRole.kd_perusahaan === '02') {
                this.masterBahanService.getArea(this.picRole.kd_perusahaan)
                    .subscribe(
                        (res1: MasterBahan) => {
                            this.newSatuans2 = res1;
                            this.newSatuans2.forEach((e) => {
                                this.listArea.push({
                                    label: e.nmBahan,
                                    value: e.idMasBahan
                                });
                            });
                        },
                        (res1: MasterBahan) => {

                        }
                    )
            } else {
                this.masterBahanService.getArea(this.picRole.kd_perusahaan)
                    .subscribe(
                        (res1: MasterBahan) => {
                            this.newSatuans2 = res1;
                            this.newSatuans2.forEach((e) => {
                                this.listArea.push({
                                    label: e.nmBahan,
                                    value: e.idMasBahan
                                });
                            });
                        },
                        (res1: MasterBahan) => {

                        }
                    )
            }
        }
        console.log(this.picRole.kd_perusahaan);
    }

    reverseData1() {
        this.newSatuans3 = null;
        this.newSatuans4 = null;
        this.listLokasi = [{ label: 'Pilih Lokasi', value: '' }];
        this.listGedung = [{ label: 'Pilih Gedung', value: '' }];
        if (this.picRole.area_project === null || this.picRole.area_project === undefined || this.picRole.area_project === '') {
            alert('Pilih Area Terlebih Dahulu');
        } else {
            this.masterBahanService.getLokasi(this.picRole.area_project)
                .subscribe(
                    (res1: MasterBahan) => {
                        this.newSatuans3 = res1;
                        this.newSatuans3.forEach((e) => {
                            this.listLokasi.push({
                                label: e.nmBahan,
                                value: e.idMasBahan
                            });
                        });
                    },
                    (res1: MasterBahan) => {

                    }
                )
        }
        console.log(this.picRole.area_project);
    }

    reverseData2() {
        this.newSatuans4 = null;
        this.listGedung = [{ label: 'Pilih Gedung', value: '' }];
        if (this.picRole.lokasi_project === null || this.picRole.lokasi_project === undefined || this.picRole.lokasi_project === '') {
            alert('Pilih Area Terlebih Dahulu');
        } else {
            this.masterBahanService.getGedung(this.picRole.lokasi_project)
                .subscribe(
                    (res1: MasterBahan) => {
                        this.newSatuans4 = res1;
                        this.newSatuans4.forEach((e) => {
                            this.listGedung.push({
                                label: e.nmBahan,
                                value: e.idMasBahan
                            });
                        });
                    },
                    (res1: MasterBahan) => {

                    }
                )
        }
        console.log(this.picRole.area_project);
    }
}
