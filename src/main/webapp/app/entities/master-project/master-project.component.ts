import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService, LazyLoadEvent, AccordionTab, DataTable } from 'primeng/primeng';
import { LoadingBarHttp } from '@ngx-loading-bar/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { ExcelService } from '../report/excel.service';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { MasterProject } from './master-project.model';
import { MasterProjectService } from './master-project.service';

@Component({
    selector: 'jhi-master-project',
    templateUrl: './master-project.component.html'
})
export class MasterProjectComponent implements OnInit, OnDestroy {
    picRoles: MasterProject[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter_data: string;
    idpp: any;
    first: number;
    currentInternal: string;

    constructor(
        private picRoleServices: MasterProjectService,
        protected confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toasterService: ToasterService,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        protected loadingService: LoadingService,
        private paginationConfig: PaginationConfig,
        protected reportUtilService: ReportUtilService,

    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 1;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.idpp = '';
    }

    loadAll() {
        if (this.principal.getIdInternal() !== undefined || this.principal.getIdInternal() !== null) {
            this.loadingService.loadingStart();
            // if (this.currentSearch) {
            //     this.picRoleServices.search({
            //         internals: 'idInternal:' + this.principal.getIdInternal(),
            //         page: this.page - 1,
            //         query: this.currentSearch,
            //         size: this.itemsPerPage,
            //         sort: this.sort()
            //     }).subscribe(
            //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            //         (res: ResponseWrapper) => this.onErrorSearch(res.json)
            //     );
            //     return;
            // }

            this.picRoleServices.query({
                query: 'idInternal:' + this.principal.getIdInternal() + '|search:' + this.currentSearch,
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {

        }

    }
    protected onErrorSearch(error) {
        console.log('eror search' + error)
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
        this.loadingService.loadingStop();
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/master-project'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.router.navigate(['/master-project', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/master-project', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            console.log('data akun : ', this.currentAccount);
        });
        this.registerChangeInPurchasingItems();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: MasterProject) {
        return item.id;
    }
    registerChangeInPurchasingItems() {
        this.eventSubscriber = this.eventManager.subscribe('MasterProjectListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        console.log('panjang data: ', data.length);
        if (data.length === 0) {
            this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        } else if (data !== null) {
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.picRoles = data;
        } else {
            this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        }
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    reqAppoved(rowData: MasterProject) {
        if (rowData.status === '0') {
            alert('Tidak Bisa Request Approve Project yang sudah tidak aktif');
        } else {
            this.confirmationService.confirm({
                message: 'Yakin untuk meminta approve Project ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.idpp = rowData.idproject;
                    this.loadingService.loadingStart();
                    this.ApprovedAtDialogDetail();

                    // this.loadingService.loadingStart();
                }
            });
        }
    }

    ApprovedAtDialogDetail() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            idPurchaseOrder: this.idpp
        }
        this.picRoleServices.changeReqAPPROVE(obj).subscribe(
            (res: ResponseWrapper) => this.loadAll(),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        // console.log('isi selected', obj);
    }
}
