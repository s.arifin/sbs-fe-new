import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PurchasingService } from '../purchasing';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MasterProject } from './master-project.model';
import { MasterProjectService } from './master-project.service';

@Component({
    selector: 'jhi-master-project-edit',
    templateUrl: './master-project-edit.component.html'
})
export class MasterProjectEditComponent implements OnInit {
    [x: string]: any;
    protected subscription: Subscription;
    picRole: MasterProject;
    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType: any;

    routeId: number;
    isSaving: boolean;
    inKdBahan: any;
    inKdUmBahan: any;
    inNmBahan: any;
    inSatBahan: any;
    inTipeBahan: any;
    inPicBahan: any;

    listSatuan: Array<object> = [
        { label: 'Pilih Satuan', value: null },
    ];

    listWadah: Array<object> = [
        { label: 'Pilih Wadah', value: null },
        { label: 'Wadah', value: 'Wadah' },
        { label: 'Pallet', value: 'Pallet' },
    ];

    tipe: Array<object> = [
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' }
    ];
    tipe1: Array<object> = [
        { label: 'Pilih Tipe', value: null },
        { label: 'Packaging Material (PM)', value: 'PM' },
        { label: 'Raw Material (RM)', value: 'RM' },
        { label: 'General Item (GI)', value: 'GI' },
    ];
    tipe2: Array<object> = [
        { label: 'Pilih Kategori', value: null },
        { label: 'Barang', value: 'BARANG' },
        { label: 'Jasa', value: 'JASA' },
    ];
    status1: Array<object> = [
        { label: 'Pilih Status', value: null },
        { label: 'Aktif', value: 'Aktif' },
        { label: 'Non Aktif', value: 'Non Aktif' }
    ];
    idMasBahan: any;
    readonly: boolean;
    pic: any;
    kdbahan: string;
    kdlama: any;
    satuan: string;
    selectedStatus: any;
    newProj: any;
    newProjs: any[];
    internals: any;
    listKdBarang = [{}];
    listKdBarang1 = [{}];
    constructor(
        public activeModal: NgbActiveModal,
        private masterBahanService: MasterProjectService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        protected route: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.picRole = new MasterProject();
        this.routeId = 0;
        this.readonly = true;
    }
    ngOnInit() {
        this.picRole = new MasterProject();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idMasBahan = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.internals = this.principal.getIdInternal();
        this.masterBahanService.getAutority({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.approvedBy,
                    value: element.approvedBy
                });
            });
            this.newKdBarang.forEach((element) => {
                this.listKdBarang1.push({
                    label: element.approvedBy1,
                    value: element.approvedBy1
                });
            });
            this.listKdBarang1.shift();
            this.listKdBarang.shift();

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }

    load() {
        this.masterBahanService.find(this.idMasBahan).subscribe((masterBahan) => {
            this.picRole = masterBahan;
        });
    }

    trackId(index: number, item: MasterProject) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    ver() {
        this.loadingService.loadingStart();
        if (this.picRole.nm_project === null || this.picRole.nm_project === undefined || this.picRole.nm_project === '') {
            alert('Nama Project Tidak Boleh Kosong');
            this.loadingService.loadingStop();
        } else if (this.picRole.nm_project.length <= 1) {
            alert('Nama Project Tidak Boleh Kurang Dari 1 Karakter');
            this.loadingService.loadingStop();
        } else if (this.picRole.pic === null || this.picRole.pic === undefined || this.picRole.pic === '') {
            alert('PIC Approval Project 1 harus di pilih');
            this.loadingService.loadingStop();
        } else if (this.picRole.pic2 === null || this.picRole.pic2 === undefined || this.picRole.pic2 === '') {
            alert('PIC Approval Project 2 harus di pilih');
            this.loadingService.loadingStop();
        } else if (this.picRole.status === null || this.picRole.pic2 === undefined || this.picRole.pic2 === '') {
            alert('Status Harus Di Pilih');
            this.loadingService.loadingStop();
        } else {
            this.loadingService.loadingStart();
            this.masterBahanService.update(this.picRole)
                .subscribe(
                    (res: ResponseWrapper) =>
                        this.onSaveSuccess(res, res.headers),
                    (res: ResponseWrapper) => this.onError(res)
                );
        }
    }
    simpan() {
        this.ver();
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'MasterProjectListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Project saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
        console.log('masuk on sukses ges!')
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Master Project Error', error._body);
        this.alertService.error(error._body, null, null);
    }
    previousState() {
        this.router.navigate(['/master-project', { page: this.paramPage }]);
    }
    back() {
        this.router.navigate(['/master-project', { page: this.paramPage }]);
    }

    cekNamaProject() {
        if (this.picRole.nm_project !== null && this.picRole.nm_project !== undefined && this.picRole.nm_project !== '') {
            this.masterBahanService.cekNoTT(this.picRole.nm_project)
                .subscribe(
                    (res: ResponseWrapper) => {
                        if (res.status === 200) {
                            alert('Nama Project sudah pernah di gunakan');
                            this.picRole.nm_project = ''
                        }
                        if (res.status === 404) {

                        }
                    }
                )
        }

    }
}
