import { Component, OnInit, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Joborders } from './joborders.model';
import { JobordersService } from './joborders.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ResponseWrapper, Principal } from '../../shared';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
// import { CustomPurchaseOrderHeader, MasterBahan, CustomSupplyer } from './index';
import { Header } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/index';
import { AutorityJO } from './index';
import * as ConfigConstants from '../../shared/constants/config.constants';
import { FileCompressionService } from './file-compression.service';

@Component({
    selector: 'jhi-joborders-new',
    templateUrl: './joborders-new.component.html'
})
export class JobordersNewComponent implements OnInit, OnDestroy {
    [x: string]: any;
    commontUtilService: any;
    @Input() readonly = false;
    protected subscription: Subscription;
    purchaseOrder: Joborders;
    isSaving: boolean;
    idPurchaseOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: any;
    dtFnsBfr: any;
    inputDivReciver: any;
    inputjobDesc: any;
    inputDivition: any;
    inputAppDept: any;
    inputAppDept1: any;
    inputSifat: any;
    inputNeeds: any;
    inputAppPembelian: any;
    inputAppPabrik: any;
    selectedSLNumber: string;
    selectedshipmentPackageId: string;
    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputSupplyerName: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: any;
    inputRemainingStock: any;
    inputRemainingPo: any;
    inputDivKategori: string;
    isOpen: Boolean = false;
    index: number;
    user: string;
    selectedSupplier: any;
    listSupplier = [{ label: 'Please Select or Insert', value: null }];
    public selectedBarang: any;
    public listBarang = [{ label: 'Please Select or Insert', value: null }];
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    listKdBarang = [{}];
    listJO = [{}];
    listKdBarang1 = [{}];
    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK SINDE 1', value: 'TEKNIK SINDE 1' },
        { label: 'TEKNIK SINDE 4', value: 'TEKNIK SINDE 4' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' },
        { label: 'HSE', value: 'HSE' }
    ];

    problem: Array<object> = [
        { label: 'Perbaikan Data / Penarikan Data', value: 'Perbaikan Data / Penarikan Data' },
        { label: 'Pembuatan Modul Baru / Revisi Modul', value: 'Pembuatan Modul Baru / Revisi Modul' },
        { label: 'Pembuatan Program Baru', value: 'Pembuatan Program Baru' },
        { label: 'Perbaikan Hardware / Infrastruktur', value: 'Perbaikan Hardware / Infrastruktur' },
    ];

    problem2: Array<object> = [
        { label: 'Maintenance', value: 'Maintenance' },
        { label: 'Bengkel', value: 'Bengkel' },
        { label: 'Kebersihan', value: 'Kebersihan' },
    ];

    sifats: Array<object> = [
        { label: 'Biasa', value: 'Biasa' },
        { label: 'Segera', value: 'Segera' },
        { label: 'Penting', value: 'Penting' }

    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }

    ];

    internals: AutorityJO[];
    internal: string;
    cek_autority: any;
    autoriti: string;
    newSuplier: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    newJo: any;
    newJOS: any[];
    filteredBarang: any[];
    lastname: any;
    countMan: any;
    isApps: any;
    constructor(
        public activeModal: NgbActiveModal,
        protected dataUtils: JhiDataUtils,
        protected alertService: JhiAlertService,
        protected jobOrderService: JobordersService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected loadingService: LoadingService,
        private fileCompressionService: FileCompressionService
    ) {
        this.purchaseOrder = new Joborders();
        this.routeId = 0;
        this.isSelectSupplier = false;
        this.inputjobDesc = ' ';
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.lastname = '';
        this.countMan = false;
    }
    protected resetMe() {
        this.selectedBarang = null;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 2097152 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            } else if (bytes < 2097152) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
            alert('Ukuran File Melebihi Batas 10MB')
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.purchaseOrder, this.elementRef, field, fieldContentType, idInput);
    }

    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }

    onSuccessBarang(data, headers) {
        this.inputCodeName = data.kdBahan;
    }

    onErrorBarang(error) {
        this.inputCodeName = null;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPurchaseOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }

        });

        // AUTORITY
        this.jobOrderService.getAutority({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.approvedBy,
                    value: element.approvedBy
                });

                if (this.lastname === '') {
                    if (element.approvedBy1 === null) {

                    } else {
                        this.listKdBarang1.push({
                            label: element.approvedBy1,
                            value: element.approvedBy1
                        });
                        this.lastname = element.approvedBy1;
                        this.countMan = true;
                    }
                } else {
                    if (element.approvedBy1 === null) {

                    } else {
                        if (element.approvedBy1 !== this.lastname) {
                            this.listKdBarang1.push({
                                label: element.approvedBy1,
                                value: element.approvedBy1
                            });
                            this.lastname = element.approvedBy1;
                            this.countMan = true;
                        } else {

                        }
                    }
                }
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.user = this.principal.getUserLogin();
        this.internal = this.principal.getIdInternal();
        this.isSaving = false;
        this.internal = this.principal.getIdInternal();
        this.lastname = '';
        if (this.internal === 'GDG') {
            this.problem.push({ label: 'Koreksi Stock', value: 'Koreksi Stock' });
        } else {

        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.clearForm();
    }

    addData(): void {

    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    clearDataInput(): void {
        this.inputCodeName = null;
        this.inputProdukName = null;
        this.inputQty = null;
        this.inputDtOffer = null;
        this.inputSupplyerName = null;
        this.inputSupplyerPrice = null;
        this.inputNecessity = null;
        this.inputDtNecessity = null;
        this.inputRemainingStock = null;
        this.inputRemainingPo = null;
        this.selectedBarang = null;
        this.selectedSupplier = null;
        this.newSuplier = null;
        this.newBarang = null;
        this.isSelectSupplier = false;
        this.inputDivKategori = null;
    }

    protected clearForm(): void {
        this.clearDataInput();
        this.selectedSLNumber = null;
        this.selectedshipmentPackageId = null;
        this.index = null;
    }

    addDataBtnDisable(): boolean {
        if ((this.newBarang !== '' &&
            this.newBarang !== undefined &&
            this.newBarang != null)) {

            return false;
        } else { return true; }
    }

    removeData(index: number): void {
    }

    load() {
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['joborders-request', { page: this.paramPage }]);
        }
    }

    save() {
        const obj = {
            divReq: this.principal.getIdInternal(),
            requester: this.principal.getUserLogin(),
            divReciver: this.inputDivReciver,
            dtOrder: this.dtOrder,
            dtFnsBfr: this.dtFnsBfr,
            appDept: this.inputAppDept,
            appDept1: this.inputAppDept1,
            jobDescription: this.inputjobDesc,
            contentContentType: this.purchaseOrder.contentContentType,
            content: this.purchaseOrder.content,
            kategori: this.inputDivKategori
        }
        this.loadingService.loadingStart();
        if (this.dtOrder === undefined) { // VALIDASI TANGGAL ORDER
            this.toaster.showToaster('info', 'Save', 'Please Select Date order!');
            this.loadingService.loadingStop();
        } else if (this.dtFnsBfr === undefined) { // VALIDASI TANGGAL FINISH JO
            this.toaster.showToaster('info', 'Save', 'Please Select Date finish order!');
            this.loadingService.loadingStop();
        } else if (this.inputDivReciver === undefined) { // DIVISI
            this.toaster.showToaster('info', 'Save', 'Please Select Sent To first!');
            this.loadingService.loadingStop();
        } else if (this.inputjobDesc === undefined) { // JOB DESC
            this.toaster.showToaster('info', 'Save', 'Please input Job Description First!');
            this.loadingService.loadingStop();
        } else if (this.internal === 'PPIC' && this.inputAppDept !== 'PJA' && this.inputAppDept1 === undefined) { // VALIDASI PPIC YG ADA 2 APPROVED DAN YANG KEDUANYA KOSONG
            this.toaster.showToaster('info', 'Save', 'Please select Approve Manager First!');
            this.loadingService.loadingStop();
        } else if (this.internal === 'PPIC' && this.inputAppDept === undefined && this.inputAppDept1 !== undefined) {
            this.toaster.showToaster('info', 'Save', 'Please select Assistant Manager / SPV');
            this.loadingService.loadingStop();
        } else if (this.internal !== 'PPIC' && this.inputAppDept === undefined) { // VALIDASI NON PPIC DAN SPV NULL
            this.toaster.showToaster('info', 'Save', 'Please select Approve Manager First!');
            this.loadingService.loadingStop();
        } else if (this.internal === 'GDG' && this.inputDivReciver === 'IT' && this.inputDivKategori === 'Koreksi Stock' && this.inputAppDept === undefined && this.inputAppDept1 !== undefined) {
            this.toaster.showToaster('info', 'Save', 'Please select Supervisor / Assistant Manager First!');
            this.loadingService.loadingStop();
        } else if (this.internal === 'GDG' && this.inputDivReciver === 'IT' && this.inputDivKategori === 'Koreksi Stock' && this.inputAppDept !== undefined && this.inputAppDept1 === undefined) {
            this.toaster.showToaster('info', 'Save', 'Please select Manager First!');
            this.loadingService.loadingStop();
        } else if (this.internal === 'GDG' && this.inputDivReciver === 'IT' && this.inputDivKategori === 'Koreksi Stock' && this.inputAppDept === undefined && this.inputAppDept1 === undefined) {
            this.toaster.showToaster('info', 'Save', 'Please select Approve Supervisor / Assistant Manager First!');
            this.loadingService.loadingStop();
        } else if (this.inputDivReciver === 'IT' && (this.inputDivKategori === undefined || this.inputDivKategori === '' || this.inputDivKategori === null)) {
            this.toaster.showToaster('info', 'Save', 'Please Select Category Problem First!');
            this.loadingService.loadingStop();
        } else if (this.inputDivReciver === 'PSU' && (this.inputDivKategori === undefined || this.inputDivKategori === '' || this.inputDivKategori === null)) {
            this.toaster.showToaster('info', 'Save', 'Please Select Category Problem First!');
            this.loadingService.loadingStop();
        } else {
            this.jobOrderService.createNew(obj).subscribe(
                (res: ResponseWrapper) =>
                    this.onSaveSuccess(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
        }
    }

    print(id: any) {
        const user = this.principal.getUserLogin();

        const filter_data = 'idOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/jobOrders/pdf', { filterData: filter_data });
    }

    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'jobordersRequestListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'jobOrders saved !');
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.clear();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'jobOrders Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: AutorityJO) {
        return item.idAutorityJo;
    }

}
