import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Joborders } from './joborders.model';
import { JobordersPopupService } from './joborders-popup.service';
import { JobordersService } from './joborders.service';

@Component({
    selector: 'jhi-joborders-delete-dialog',
    templateUrl: './joborders-delete-dialog.component.html'
})
export class JobordersDeleteDialogComponent {

    joborders: Joborders;

    constructor(
        private jobordersService: JobordersService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jobordersService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jobordersListModification',
                content: 'Deleted an joborders'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-joborders-delete-popup',
    template: ''
})
export class JobordersDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jobordersPopupService: JobordersPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jobordersPopupService
                .open(JobordersDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
