import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Joborders } from './joborders.model';
import { JobordersPopupService } from './joborders-popup.service';
import { JobordersService } from './joborders.service';

@Component({
    selector: 'jhi-joborders-dialog',
    templateUrl: './joborders-dialog.component.html'
})
export class JobordersDialogComponent implements OnInit {

    joborders: Joborders;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private jobordersService: JobordersService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.joborders.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jobordersService.update(this.joborders));
        } else {
            this.subscribeToSaveResponse(
                this.jobordersService.create(this.joborders));
        }
    }

    private subscribeToSaveResponse(result: Observable<Joborders>) {
        result.subscribe((res: Joborders) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Joborders) {
        this.eventManager.broadcast({ name: 'jobordersListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-joborders-popup',
    template: ''
})
export class JobordersPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jobordersPopupService: JobordersPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jobordersPopupService
                    .open(JobordersDialogComponent as Component, params['id']);
            } else {
                this.jobordersPopupService
                    .open(JobordersDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
