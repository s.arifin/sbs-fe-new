import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    JobordersService,
    JobordersPopupService,
    JobordersComponent,
    JobordersCancelComponent,
    JobordersSolveComponent,
    JobordersReciveComponent,
    JobordersDetailComponent,
    JobordersDialogComponent,
    JobordersPopupComponent,
    JobordersDeletePopupComponent,
    JobordersDeleteDialogComponent,
    JobordersReciveEditComponent,
    JobordersReciveProgressEditComponent,
    JobordersSolveEditComponent,
    JobordersReciveSolveComponent,
    JobordersReciveProgressComponent,
    jobordersRoute,
    jobordersPopupRoute,
    JobordersResolvePagingParams,
    ApprovalJobOrderComponent,
    ApprovalJobOrderDetailComponent,
    TabViewJobOrderComponent,
    JobordersReciveCompleteComponent,
    JobOrderViewComponent,
    JobOrderCompleteComponent,
    FileCompressionService,
    JobordersNotViewComponent
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextareaModule,
         PaginatorModule,
         ConfirmDialogModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         TabViewModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { JobordersNewComponent } from './joborders-new.component';
import { JobordersEditComponent } from './joborders-edit.component';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmPurchaseOrderItemModule } from '../purchase-order-item/purchase-order-item.module';
import { JobOrderProgressComponent } from './joborders-progress.component';
import { JobordersReciveCancelComponent } from './joborders-recive-cancel.component';
// import { JobordersReciveCompleteComponent } from './joborders-recive-Complete.component';
// import { PurchaseOrderItemService } from '../purchase-order-item/index';
// import { PurchaseOrderNewComponent } from './purchase-order-new.component';

const ENTITY_STATES = [
    ...jobordersRoute,
    ...jobordersPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        TabViewModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        SliderModule,
        RadioButtonModule,
        MpmPurchaseOrderItemModule
    ],
    exports: [
        JobordersComponent,
        JobordersSolveComponent,
        JobordersReciveComponent,
        JobordersCancelComponent,
        JobordersReciveEditComponent,
        JobordersReciveProgressEditComponent,
        JobordersSolveEditComponent,
        JobordersReciveProgressComponent,
        JobordersEditComponent,
        TabViewJobOrderComponent,
        JobOrderViewComponent,
        JobordersReciveSolveComponent,
        JobordersReciveCompleteComponent,
        JobOrderProgressComponent,
        JobOrderCompleteComponent,
        JobordersReciveCancelComponent,
        JobordersNotViewComponent
    ],
    declarations: [
        ApprovalJobOrderComponent,
        ApprovalJobOrderDetailComponent,
        JobordersComponent,
        JobordersSolveComponent,
        JobordersReciveComponent,
        JobordersNewComponent,
        JobordersDetailComponent,
        JobordersDialogComponent,
        JobordersDeleteDialogComponent,
        JobordersPopupComponent,
        JobordersDeletePopupComponent,
        JobordersSolveEditComponent,
        JobordersEditComponent,
        JobordersCancelComponent,
        JobordersReciveEditComponent,
        JobordersReciveProgressEditComponent,
        JobordersReciveProgressComponent,
        JobordersReciveSolveComponent,
        TabViewJobOrderComponent,
        JobOrderViewComponent,
        JobordersReciveCompleteComponent,
        JobOrderProgressComponent,
        JobOrderCompleteComponent,
        JobordersReciveCancelComponent,
        JobordersNotViewComponent
    ],
    entryComponents: [
        ApprovalJobOrderComponent,
        ApprovalJobOrderDetailComponent,
        JobordersSolveComponent,
        JobordersComponent,
        JobordersReciveComponent,
        JobordersNewComponent,
        JobordersDialogComponent,
        JobordersPopupComponent,
        JobordersDeleteDialogComponent,
        JobordersDeletePopupComponent,
        JobordersSolveEditComponent,
        JobordersEditComponent,
        JobordersCancelComponent,
        JobordersReciveEditComponent,
        JobordersReciveProgressEditComponent,
        JobordersReciveProgressComponent,
        TabViewJobOrderComponent,
        JobOrderViewComponent,
        JobordersReciveSolveComponent,
        JobordersReciveCompleteComponent,
        JobOrderProgressComponent,
        JobOrderCompleteComponent,
        JobordersReciveCancelComponent,
        JobordersNotViewComponent
    ],
    providers: [
        JobordersService,
        JobordersPopupService,
        JobordersResolvePagingParams,
        FileCompressionService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmJobordersModule {}
