import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../../shared';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { JobordersService, Joborders, CustomJobOrders } from '../../joborders'
import { Subscription } from 'rxjs';

@Component({
    selector: 'jhi-tab-view-job-order',
    templateUrl: './tab-view-job-order.component.html'
})
export class TabViewJobOrderComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    currentAccount: any;
    @Input() idOrder: any;
    @Input() idStatusType: number;
    @Input() idJobOrder: any;
    joborders: Joborders[];
    public jobOrders: Array<Joborders>;
    public jobOrder: Joborders;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selectedJob: Joborders;
    selected: any = [];
    appJobOrders: Joborders[];
    internal: any;
    statusidtype: number;
    loginuser: any;
    kategori: any;
    divreciver: any;

    constructor(
        protected principal: Principal,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrderService: JobordersService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected router: Router,
        private eventManager: JhiEventManager,
        protected toaster: ToasterService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }

    loadData(): void {
        this.loadingService.loadingStart();
        if (this.principalService.getIdInternal() !== null || this.principalService.getIdInternal() !== undefined) {
            this.jobOrderService.getDataByStatus({
                page: this.page - 1,
                idStatustype: this.idStatusType,
                idInternal: this.principalService.getIdInternal(),
                size: this.itemsPerPage
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccess(res.json, res.headers);
                    this.jobOrders = res.json;
                    this.jobOrder = res.json;
                    this.loadingService.loadingStop();
                },
                (res: ResponseWrapper) => {
                    this.onError(res.json);
                }
            );
        } else {

        }

        this.internal = this.principal.getIdInternal();
        this.loginuser = this.principal.getUserLogin();
        console.log(this.internal);
        console.log(this.loginuser);
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/job-order/approval', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/job-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }
    srch() {
        if (this.principal.getIdInternal() !== null || this.principal.getIdInternal() !== undefined) {
            if (this.currentSearch) {
                this.jobOrderService.search({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: this.idStatusType,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                        this.jobOrders = res.json;
                        this.jobOrder = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            }
        } else {

        }

    }
    protected onErrorSearch(error) {
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    ngOnDestroy() {
    }

    public trackId(index: number, item: CustomUnitDocumentMessage): string {
        return item.idRequirement;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.joborders = data;
        this.kategori = this.joborders['kategori'];
        this.divreciver = this.joborders['divReciver'];
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk TIdak Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.notAvailable();
            }
        });
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 12,
            data: this.selected
        }
        this.jobOrderService.changeStatus(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'jobOrders Approved !');
            this.loadData();
            this.selected = null;
            this.loadingService.loadingStop();
            this.router.navigate(['/job-order/approval'])
        }
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setuju?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.Approved();
            }
        });
    }

    Approved() {
        if ((this.internal === 'PPIC' || this.internal === 'GDG') && this.loginuser !== 'PJA') {
            this.statusidtype = 9;
        } else {
            this.statusidtype = 11;
        }
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: this.statusidtype,
            data: this.selected
        }
        this.jobOrderService.changeStatus(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            this.eventManager.broadcast({ name: 'approvalJobOrderListModification', content: 'OK' });
            this.toaster.showToaster('info', 'Save', 'jobOrders Approved !');
            this.loadData();
            this.selected = null;
            this.loadingService.loadingStop();
        }
    }

}
