import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../../unit-document-message';
import { JobordersService, Joborders, CustomJobOrders } from '../../joborders'
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import * as BaseConstant from '../../../shared/constants/base.constants';
import * as UnitDocumentMessageConstant from '../../../shared/constants/unit-document-message.constants';
import { ResponseWrapper, CommonUtilService, Principal, ITEMS_PER_PAGE, ToasterService } from '../../../shared';
// import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'jhi-approval-job-orders',
    templateUrl: './approval-job-order.component.html'
})
export class ApprovalJobOrderComponent implements OnInit, OnDestroy {
    first: number;
    public jobOrders: Array<Joborders>;
    public jobOrder: Joborders;
    public jobOrdersApp: Array<Joborders>;
    public jobOrderApp: Joborders;
    public jobOrdersSPV: Array<Joborders>;
    public jobOrderSPV: Joborders;
    public jobOrdersProgress: Array<Joborders>;
    public jobOrderProgress: Joborders;
    private eventSubscriber: Subscription;
    currentAccount: any;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    jobOrderApprove: any;
    jobOrderRejeced: any;
    itemsPerPage: any;
    routeData: any;
    totalItems: any;
    queryCount: any;
    idOrder: any;
    paramPage: number;
    selected: any = [];
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    idStatus: any;
    internal: any;
    userlogin: any;
    noteCancelJO: string;
    newModalNote1: boolean;

    constructor(
        protected principal: Principal,
        protected router: Router,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrdersService: JobordersService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        protected alertService: JhiAlertService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.jobOrders = new Array<Joborders>();
        this.jobOrder = new Joborders();
        this.jobOrderApprove = 12;
        this.jobOrderRejeced = 11;
        this.newModalNote1 = false;
        this.noteCancelJO = '';
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
        this.registerChangeInApprovalJobOrder();
    }

    protected loadData(): void {
        this.loadingService.loadingStart();
        this.jobOrdersService.getDataByApprovalStatusAndRequirement({
            page: this.page - 1,
            idStatustype: 12,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrders = res.json;
                this.jobOrder = res.json;
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );

        this.jobOrdersService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 11,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrdersApp = res.json;
                this.jobOrderApp = res.json;
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );

        this.jobOrdersService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 9,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrdersSPV = res.json;
                this.jobOrderSPV = res.json;
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );

        this.jobOrdersService.getDataByStatus({
            page: this.page - 1,
            idStatustype: 14,
            idInternal: this.principalService.getIdInternal(),
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers);
                this.jobOrdersProgress = res.json;
                this.jobOrderProgress = res.json;
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
        this.userlogin = this.principalService.getUserLogin();
        this.internal = this.principalService.getIdInternal();
    }
    srch() {
        if (this.currentSearch) {
            if (this.idStatus === 11) {
                this.jobOrdersService.search({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: this.idStatus,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                        this.jobOrdersApp = res.json;
                        this.jobOrdersApp = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            } else if (this.idStatus === 12) {
                this.jobOrdersService.search({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: this.idStatus,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                        this.jobOrders = res.json;
                        this.jobOrders = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            } else if (this.idStatus === 9) {
                this.jobOrdersService.search({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: this.idStatus,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                        this.jobOrdersSPV = res.json;
                        this.jobOrderSPV = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            } else if (this.idStatus === 14) {
                this.jobOrdersService.search({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: this.idStatus,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                        this.jobOrdersProgress = res.json;
                        this.jobOrdersProgress = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
            }
            return;
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }
    protected onErrorSearch(error) {
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/job-order/approval', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadData();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 11
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/job-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    search9(query) {
        if (!query) {
            return this.clear();
        }
        this.idStatus = 9
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/job-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    search13(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 12;
        this.currentSearch = query;
        this.router.navigate(['/job-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    search14(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.idStatus = 14;
        this.currentSearch = query;
        this.router.navigate(['/job-order/approval', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    protected reset(): void {
        this.jobOrders = new Array<Joborders>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    gotoDetail(rowData) {
        this.jobOrdersService.passingCustomData(rowData);
    }

    public submit(id: number, type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header: 'Confirmation',
                message: 'Do you want to approve this Job Order?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: CustomJobOrders) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderApprove,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                item: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        } else if (type === 'reject') {
            this.confirmationService.confirm({
                header: 'Confirmation',
                message: 'Do you want to reject this Job Order?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: CustomJobOrders) => {
                            const objectHeader: Object = {
                                execute: this.jobOrderRejeced,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                items: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalJobOrder() {
        this.eventSubscriber = this.eventManager.subscribe('approvalJobOrderListModification', (response) => this.loadData());
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.jobOrdersService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                    this.toasterService.showToaster('info', 'Data Job order cancel', 'Cancel jobOrder');
                    this.loadData();
                });
            },
        });
    }

    cancelJO(jo: Joborders) {
        this.jobOrder = new Joborders();
        this.jobOrder = jo;
        this.newModalNote1 = true;
    }

    modalCancelJO() {
        const panjang = this.noteCancelJO.replace(/\s/g, '').trim();
        if (this.noteCancelJO === '' || this.noteCancelJO === null || panjang.length < 1) {
            alert('Alasan Cancel Harus Di Isi');
        } else {
            this.confirmationService.confirm({
                message: 'Are you sure that you want to cancel?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.jobOrder.alasan_cancel = this.noteCancelJO;
                    this.jobOrdersService.changeStatusJO(this.jobOrder)
                        .subscribe(
                            (res) => {
                            }
                        )
                    this.newModalNote1 = false;
                    this.noteCancelJO = '';
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                    this.toasterService.showToaster('info', 'Data Job order cancel', 'Cancel jobOrder');
                    this.loadData();
                },
            });
        }
    }

}
