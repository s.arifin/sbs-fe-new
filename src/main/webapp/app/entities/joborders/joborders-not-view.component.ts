import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../layouts';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper, Account, ToasterService } from '../../shared';
import { JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { JobordersService, Joborders, CustomJobOrders, JobordersNewComponent } from '../joborders'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-job-order-not-view',
    templateUrl: './joborders-not-view.component.html'
})
export class JobordersNotViewComponent implements OnInit {

    currentAccount: any;
    @Input() idOrder: any;
    @Input() idStatusType: string;
    @Input() idJobOrder: any;
    public jobOrders: Array<Joborders>;
    public jobOrder: Joborders;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    first: Number;
    previousPage: any;
    isLazyLoadingFirst: Boolean = false;
    reverse: any;
    currentSearch: string;
    selectedJob: Joborders;
    selected: any = [];
    appJobOrders: Joborders[];
    divitions: Array<object> = [
        { label: 'Pembuat JO', value: 'createdby' },
        { label: 'Approver', value: 'approver' }
    ];
    divReciver: any;

    constructor(
        protected toasterService: ToasterService,
        protected principal: Principal,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected jobOrderService: JobordersService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected modalService: NgbModal,
        protected reportUtilService: ReportUtilService,
        protected router: Router,

    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();

        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.divReciver = '';
    }

    previousState() {
        window.history.back();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadData();
        });
    }
    protected onErrorSearch(error) {
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }

    srch() {
        if (this.currentSearch) {
            if (this.principal.getIdInternal() !== undefined || this.principal.getIdInternal() !== null) {
                this.jobOrderService.searchRequest({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: 12,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                        this.jobOrders = res.json;
                        this.jobOrder = res.json;
                    },
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            }

        }
    }

    protected loadData(): void {
        if (this.principalService.getIdInternal() !== null || this.principalService.getIdInternal() !== undefined) {
            this.loadingService.loadingStart();
            this.jobOrderService.getDataApprove({
                page: this.page - 1,
                idStatustype: 12,
                idInternal: this.principalService.getIdInternal(),
                size: this.itemsPerPage
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccess(res.json, res.headers);
                    this.jobOrders = res.json;
                    this.jobOrder = res.json;
                    this.loadingService.loadingStop();
                },
                (res: ResponseWrapper) => {
                    this.onError(res.json);
                }
            );
        } else {

        }

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/joborders-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.divReciver = '';
        this.loadData();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/joborders-request', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.srch();
    }

    print(id: any) {
        const filter_data = 'idOrder:' + id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/jobOrders/pdf', { filterData: filter_data });
    }

    public trackId(index: number, item: CustomUnitDocumentMessage): string {
        return item.idRequirement;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }

    gotoDetail(rowData) {
        this.jobOrderService.passingCustomData(rowData);
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk TIdak Setujuh?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.notAvailable();
                this.loadingService.loadingStart();
            }
        });
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 12,
            data: this.selected
        }
        this.jobOrderService.changeStatus(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    openAdd() {
        const modalRef = this.modalService.open(JobordersNewComponent, { size: 'lg', backdrop: 'static' });
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.appJobOrders = data;
            this.router.navigate(['/joborders-request'])
            this.loadingService.loadingStop();
        }
    }

    createRequest() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Setujuh?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.Approved();
                this.loadingService.loadingStart();
            }
        });
    }

    Approved() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            idStatusType: 11,
            data: this.selected
        }
        this.jobOrderService.changeStatus(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessApproved(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessApproved(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.appJobOrders = data;
            this.router.navigate(['/joborders-request'])
            this.loadingService.loadingStop();
        }
    }

    filtered(query) {
        if (query == null) {
            return this.clear();
        } else {
            this.page = 0;
            this.currentSearch = query;
            this.router.navigate(['/joborders-request', {
                search: this.currentSearch,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.srch();
        }
    }

}
