import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Joborders } from './joborders.model';
import { JobordersService } from './joborders.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { JobordersNewComponent } from './index';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/index';

@Component({
    selector: 'jhi-joborders',
    templateUrl: './joborders.component.html'
})
export class JobordersComponent implements OnInit, OnDestroy {
    dateFrom: Date;
    dateThru: Date;
    first: number;
    currentInternal: string;
    currentAccount: any;
    joborders: Joborders[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    public jobOrders: Array<Joborders>;
    public jobOrder: Joborders;

    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK', value: 'TEKNIK' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'HRD', value: 'HRD' },
        { label: 'IT', value: 'IT' }
    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }

    ];

    app_pja: Array<object> = [
        { label: 'PJA', value: 'PJA' },
    ];

    divi: Array<object> = [
        { label: 'Pembuat JO', value: 'createdby' },
        { label: 'Approver', value: 'approver' }
    ];
    divReciver: any;

    constructor(
        protected loadingService: LoadingService,
        protected toasterService: ToasterService,
        protected confirmationService: ConfirmationService,
        private jobordersService: JobordersService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        protected modalService: NgbModal,
        protected reportUtilService: ReportUtilService,
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.jobOrders = new Array<Joborders>();
        this.jobOrder = new Joborders();

        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.divReciver = '';
    }

    openAdd() {
        const modalRef = this.modalService.open(JobordersNewComponent, { size: 'lg', backdrop: 'static' });
    }
    protected onErrorSearch(error) {
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
    }
    loadAll() {
        if (this.principal.getIdInternal() !== null || this.principal.getIdInternal() !== undefined) {
            if (this.currentSearch) {
                this.jobordersService.searchRequest({
                    internals: 'idInternal:' + this.principal.getIdInternal(),
                    stat: 10,
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            }
            this.jobordersService.query({
                query: 'idInternal:' + this.principal.getIdInternal(),
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {

        }

    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/joborders-request'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.divReciver = '';
        this.router.navigate(['/joborders-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/joborders-request', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInJoborders();
    }

    protected loadData(): void {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.dateFrom.setHours(0, 0, 0, 0);
        this.dateThru.setHours(23, 59, 59, 99);
        this.loadingService.loadingStart();
        this.jobordersService.getDataByStatus({
            idInternal: this.principal.getIdInternal(),
            startDate: this.dateFrom.toISOString(),
            endDate: this.dateThru.toISOString(),
            page: 0
        }).subscribe(
            (res) => {
                this.jobOrders = res.json;
                this.jobOrder = res.json;
                this.loadingService.loadingStop();
            }
        );
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Joborders) {
        return item.idOrder;
    }

    registerChangeInJoborders() {
        this.eventSubscriber = this.eventManager.subscribe('jobordersRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idorder') {
            result.push('idorder');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.joborders = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.jobordersService.executeProcess(item).subscribe(
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.jobordersService.update(event.data)
                .subscribe((res: Joborders) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.jobordersService.create(event.data)
                .subscribe((res: Joborders) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: Joborders) {
        this.toasterService.showToaster('info', 'Job order Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.jobordersService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                });
            },
        });
    }

    print(id: any) {
        const user = this.principal.getUserLogin();

        const filter_data = 'idOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/jobOrders/pdf', { filterData: filter_data });
    }

    filtered(query) {
        if (query == null) {
            return this.clear();
        } else {
            this.page = 0;
            this.currentSearch = query;
            this.router.navigate(['/joborders-request', {
                search: this.currentSearch,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.search(query);
        }
    }

}
