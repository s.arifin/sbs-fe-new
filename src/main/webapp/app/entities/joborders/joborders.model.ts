import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { BaseEntity } from './../../shared';

export class CustomJobOrders implements BaseEntity {
    constructor(
        public id?: number,
        public idOrder?: any,
        public orderNumber?: any,
        public idOrdTyp?: any,
        public dtOrder?: any,
        public dtFnsBfr?: any,
        public idJobOrder?: any,
        public divReq?: any,
        public requester?: any,
        public divReciver?: string,
        public executor?: any,
        public jobDescription?: any,
        public dtStart?: any,
        public dtFinish?: any,
        public actionDesc?: any,
        public suggestion?: any,
        public createdBy?: any,
        public dtCreated?: any,
        public Totaldata?: any,
        public appDept?: string,
        public appDept1?: string,
        public idStatusType?: any,
        public dtRecive?: any,
        public reciverName?: any,
        public instructionFrom?: any,
        public contentContentType?: string,
        public content?: any,
        public kategori?: string,
        public user_seen?: any,
        public seen_by?: any,
        public dtcomplete?: any,
        public is_print?: any,
    ) {
    }
}
export class MasterUserRole implements BaseEntity {
    constructor(
        public id?: number,
        public idAu?: any,
        public idMasUser?: any,
        public plant?: any,
        public seksie?: any,
        public div?: any,
        public initial?: any,

    ) { }
}

export class AutorityJO implements BaseEntity {
    constructor(
        public id?: number,
        public idAutorityJo?: any,
        public div?: any,
        public createdBy?: any,
        public approvedBy?: any,

    ) { }
}

export class Joborders implements BaseEntity {
    constructor(
        public id?: number,
        public idOrder?: any,
        public orderNumber?: any,
        public idOrdTyp?: any,
        public dtOrder?: any,
        public dtFnsBfr?: any,
        public idJobOrder?: any,
        public divReq?: any,
        public requester?: any,
        public divReciver?: string,
        public executor?: any,
        public jobDescription?: any,
        public dtStart?: any,
        public dtFinish?: any,
        public actionDesc?: any,
        public suggestion?: any,
        public createdBy?: any,
        public dtCreated?: any,
        public Totaldata?: any,
        public appDept?: string,
        public appDept1?: string,
        public idStatusType?: any,
        public dtRecive?: any,
        public reciverName?: any,
        public instructionFrom?: any,
        public dtFromTmln?: any,
        public dtThruTmln?: any,
        public ketExectn?: any,
        public contentContentType?: string,
        public content?: any,
        public kategori?: string,
        public user_seen?: any,
        public seen_by?: any,
        public dtcomplete?: any,
        public is_print?: any,
        public alasan_cancel?: any,
        public cancelby?: any,
        public dtcancel?: any
    ) {
    }
}
