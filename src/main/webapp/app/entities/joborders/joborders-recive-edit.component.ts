import { Component, OnInit, OnDestroy, Input, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Joborders, MasterUserRole } from './joborders.model';
import { JobordersService } from './joborders.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-joborders-recive-edit',
    templateUrl: './joborders-recive-edit.component.html'
})
export class JobordersReciveEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    joborders: Joborders;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: Date;
    dtStart: Date;
    dtFinish: Date;
    dtRecive: Date;
    listKdBarang = [{}];
    dtStartTmln: Date;
    dtFinishTmln: Date;
    internals: any;
    dtFnsBfr: any;
    user_seen: any;
    user: any;
    userby: any;
    kat: any;
    recive: any;
    noteCancelJO: string;
    newModalNote1: boolean;

    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK SINDE 1', value: 'TEKNIK SINDE 1' },
        { label: 'TEKNIK SINDE 4', value: 'TEKNIK SINDE 4' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' },
        { label: 'HSE', value: 'HSE' }

    ];

    sifats: Array<object> = [
        { label: 'Biasa', value: 'Biasa' },
        { label: 'Segera', value: 'Segera' },
        { label: 'Penting', value: 'Penting' }
    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }
    ];

    problem: Array<object> = [
        { label: 'Perbaikan Data / Penarikan Data', value: 'Perbaikan Data / Penarikan Data' },
        { label: 'Pembuatan Modul Baru / Revisi Modul', value: 'Pembuatan Modul Baru / Revisi Modul' },
        { label: 'Pembuatan Program Baru', value: 'Pembuatan Program Baru' },
        { label: 'Perbaikan Hardware / Infrastruktur', value: 'Perbaikan Hardware / Infrastruktur' },
        { label: 'Koreksi Stock', value: 'Koreksi Stock' },
    ];

    constructor(
        protected alertService: JhiAlertService,
        protected confirmationService: ConfirmationService,
        protected jobOrdersService: JobordersService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected loadingService: LoadingService,
        protected dataUtils: JhiDataUtils,
        private location: Location,
    ) {
        this.joborders = new Joborders();
        this.routeId = 0;
        this.readonly = true;
        this.dtStart = new Date();
        this.dtFinish = new Date();
        this.dtRecive = new Date();
        this.dtFnsBfr = new Date();
        this.newModalNote1 = false;
        this.noteCancelJO = '';
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], { 'type': fieldContentType });

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    ngOnInit() {
        this.jobOrdersService.getAllRolUser({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.seksie + ' - ' + element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        if (this.internals === 'IT') {
            this.location.subscribe((val: any) => {
                if (val !== this.routeId) {
                    this.previousState1();
                }
            });
        }
    }

    ngOnDestroy() {
    }

    @HostListener('window:unload', ['$event'])
    unloadHandler(event) {
        this.saveNullSeen();
    }

    @HostListener('window:beforeunload', ['$event'])
    beforeUnloadHander(event) {
        this.saveNullSeen();
    }

    load() {
        this.internals = this.principal.getIdInternal();
        this.user = this.principal.getUserLogin();
        this.jobOrdersService.find(this.idOrder).subscribe((joborders) => {
            this.dtRecive = new Date(this.joborders.dtRecive);
            this.joborders = joborders;
            this.dtOrder = new Date(this.joborders.dtOrder);
            this.dtFnsBfr = new Date(this.joborders.dtFnsBfr);
            if (this.joborders.dtStart === undefined) {
                this.dtStart = new Date(this.joborders.dtStart);
                this.dtFinish = new Date(this.joborders.dtFinish);
            } else {
                this.dtStart = new Date();
                this.dtFinish = new Date();
                this.dtRecive = new Date();
            }
            this.user_seen = this.joborders.user_seen;
            this.userby = this.joborders.seen_by;
            this.kat = this.joborders.divReciver;
            this.recive = this.joborders.reciverName;

            if (this.paramPage === this.paramPage) {
                if (this.internals === 'IT') {
                    if (this.user_seen === null && this.userby === null || this.userby === undefined) {
                        this.saveSeen();
                    } else if (this.user_seen !== null && this.userby === this.user) {
                    } else if (this.user_seen !== null && this.userby !== this.user) {
                        alert(`Mohon maaf, JO ini sedang dibuka oleh user ${this.userby}`);
                        this.previousState();
                    }
                }
            }
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['joborders-recive', { page: this.paramPage }]);
        }
    }

    previousState1() {
        if (this.routeId === 0) {
            this.router.navigate(['joborders-recive', { page: this.paramPage }]);
        }
        this.saveNullSeen();
    }

    delete(jo: Joborders) {
        this.joborders = new Joborders();
        this.joborders = jo;
        this.newModalNote1 = true;
    }

    modalCancelJO() {
        const panjang = this.noteCancelJO.replace(/\s/g, '').trim();
        if (this.noteCancelJO === null || this.noteCancelJO === '' || panjang.length < 1) {
            alert('Alasan Di Cancel Harus Di Isi');
        } else {
            this.confirmationService.confirm({
                message: 'Are you sure that you want to cancel the job orders ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.isSaving = true;
                    this.joborders.alasan_cancel = this.noteCancelJO;
                    this.jobOrdersService.changeStatusJO(this.joborders)
                        .subscribe(
                            (res) => {
                            }
                        )
                    this.newModalNote1 = false;
                    this.noteCancelJO = '';
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                    this.toaster.showToaster('info', 'Data Job order cancel', 'Cancel');
                    if (this.internals === 'IT') {
                        this.previousState1();
                    } else {
                        this.previousState();
                    }
                },
            });
        }
    }

    save() {
        this.loadingService.loadingStart();
        this.isSaving = true;
        if (this.joborders.reciverName === null || this.joborders.reciverName === '') {
            this.toaster.showToaster('info', 'Save', 'Please Insert The Receiver JO');
            this.loadingService.loadingStop();
        } else {
            this.joborders.dtOrder = this.dtOrder;
            this.joborders.dtStart = this.dtStart;
            this.joborders.dtFinish = this.dtFinish;
            this.joborders.dtRecive = this.dtRecive;
            this.joborders.dtFromTmln = this.dtStartTmln;
            this.joborders.dtThruTmln = this.dtFinishTmln;
            if (this.joborders.idOrder !== undefined) {
                this.subscribeToSaveResponse(
                    this.jobOrdersService.updateProgress(this.joborders));
            } else {
                this.subscribeToSaveResponse(
                    this.jobOrdersService.create(this.joborders));
            }
        }
    }

    saveSeen() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        this.joborders.user_seen = 1;
        this.joborders.seen_by = this.user;
        if (this.joborders.idOrder !== undefined) {
            this.subscribeToSaveResponse1(
                this.jobOrdersService.updateSeen(this.joborders));
        }
    }

    saveNullSeen() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        this.joborders.user_seen = null;
        this.joborders.seen_by = null;
        if (this.joborders.idOrder !== undefined) {
            this.subscribeToSaveResponse1(
                this.jobOrdersService.updateSeen(this.joborders));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Joborders>) {
        result.subscribe((res: Joborders) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected subscribeToSaveResponse1(result: Observable<Joborders>) {
        result.subscribe((res: Joborders) =>
            this.onSaveSuccess1(res));
    }

    protected onSaveSuccess(result: Joborders) {
        this.print(result.idOrder);
        this.eventManager.broadcast({ name: 'jobordersReciveListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Job Order saved !');
        this.isSaving = false;
        if (this.internals === 'IT') {
            this.previousState1();
        } else {
            this.previousState();
        }
        this.loadingService.loadingStop();
    }

    protected onSaveSuccess1(result: Joborders) {
        this.print(result.idOrder);
        this.eventManager.broadcast({ name: 'jobordersReciveListModification', content: 'OK' });
        this.isSaving = false;
        this.loadingService.loadingStop();
    }

    print(id: any) {
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'Job Orders Changed', error.message);
        this.alertService.error(error.message, null, null);
        if (this.newModalNote1 === true) {
            this.noteCancelJO = undefined;
            this.newModalNote1 = false;
            if (this.internals === 'IT') {
                this.previousState1();
            } else {
                this.previousState();
            }
        }
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

}
