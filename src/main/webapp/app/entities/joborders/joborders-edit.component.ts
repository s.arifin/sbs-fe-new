import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { Joborders, AutorityJO } from './joborders.model';
import { JobordersService } from './joborders.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { ConfirmationService } from 'primeng/primeng';
import { DatePipe } from '@angular/common/src/pipes/date_pipe';
import * as ConfigConstants from '../../shared/constants/config.constants';
import * as FileSaver from 'file-saver';
import { ArrayBuffer } from '@angular/http/src/static_request';
import { FileCompressionService } from './file-compression.service';
@Component({
    selector: 'jhi-joborders-edit',
    templateUrl: './joborders-edit.component.html'
})
export class JobordersEditComponent implements OnInit, OnDestroy {
    [x: string]: any;
    @Input() readonly = false;
    protected subscription: Subscription;
    joborders: Joborders;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: Date;
    dtFnsBfr: Date;
    listKdBarang = [{}];
    listJO = [{}];
    listKdBarang1 = [{}];
    inputAppDept: any;
    inputAppDept1: any;
    appdept: any;
    appdept1: any;
    kategori: any;
    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK SINDE 1', value: 'TEKNIK SINDE 1' },
        { label: 'TEKNIK SINDE 4', value: 'TEKNIK SINDE 4' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' },
        { label: 'HSE', value: 'HSE' }
    ];

    sifats: Array<object> = [
        { label: 'Biasa', value: 'Biasa' },
        { label: 'Segera', value: 'Segera' },
        { label: 'Penting', value: 'Penting' }
    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }
    ];

    problem: Array<object> = [
        { label: 'Perbaikan Data / Penarikan Data', value: 'Perbaikan Data / Penarikan Data' },
        { label: 'Pembuatan Modul Baru / Revisi Modul', value: 'Pembuatan Modul Baru / Revisi Modul' },
        { label: 'Pembuatan Program Baru', value: 'Pembuatan Program Baru' },
        { label: 'Perbaikan Hardware / Infrastruktur', value: 'Perbaikan Hardware / Infrastruktur' },
    ];

    problem1: Array<object> = [
        { label: 'Perbaikan Data / Penarikan Data', value: 'Perbaikan Data / Penarikan Data' },
        { label: 'Pembuatan Modul Baru / Revisi Modul', value: 'Pembuatan Modul Baru / Revisi Modul' },
        { label: 'Pembuatan Program Baru', value: 'Pembuatan Program Baru' },
        { label: 'Perbaikan Hardware / Infrastruktur', value: 'Perbaikan Hardware / Infrastruktur' },
        { label: 'Koreksi Stock', value: 'Koreksi Stock' },
    ];
    lastname: any;
    countMan: any;
    contents: any;
    contentTypes: any;
    internals: AutorityJO[];
    internal: any;
    cek_autority: any;
    reciver: any;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    newModalNote1: boolean;
    noteCancelJO: string;

    constructor(
        protected alertService: JhiAlertService,
        protected confirmationService: ConfirmationService,
        protected jobOrdersService: JobordersService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected dataUtils: JhiDataUtils,
        protected loadingService: LoadingService,
        private fileCompressionService: FileCompressionService
    ) {
        this.joborders = new Joborders();
        this.routeId = 0;
        this.readonly = true;
        this.dtFnsBfr = new Date();
        this.dtOrder = new Date();
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.contents = null;
        this.contentTypes = null;
        this.lastname = '';
        this.countMan = false;
        this.newModalNote1 = false;
        this.noteCancelJO = '';
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 2097152 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            } else if (bytes < 2097152) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.joborders.content = null;
        this.joborders.contentContentType = null;
    }
    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], { 'type': fieldContentType });

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
    ngOnInit() {
        this.jobOrdersService.getAutority({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.approvedBy,
                    value: element.approvedBy
                });

                if (this.lastname === '') {
                    if (element.approvedBy1 === null) {

                    } else {
                        this.listKdBarang1.push({
                            label: element.approvedBy1,
                            value: element.approvedBy1
                        });
                        this.lastname = element.approvedBy1;
                        this.countMan = true;
                    }
                } else {
                    if (element.approvedBy1 === null) {

                    } else {
                        if (element.approvedBy1 !== this.lastname) {
                            this.listKdBarang1.push({
                                label: element.approvedBy1,
                                value: element.approvedBy1
                            });
                            this.lastname = element.approvedBy1;
                            this.countMan = true;
                        } else {

                        }
                    }
                }
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.load();
        this.isSaving = false;
        this.internal = this.principal.getIdInternal();
        this.cek_autority = this.principal.getUserLogin();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    trackInternalById(index: number, item: AutorityJO) {
        return item.approvedBy;
    }
    load() {
        this.jobOrdersService.find(this.idOrder).subscribe((joborders) => {
            this.joborders = joborders;
            this.joborders.dtFnsBfr = new Date(this.joborders.dtFnsBfr);
            this.joborders.dtOrder = new Date(this.joborders.dtOrder);
            this.inputAppDept = this.joborders.appDept;
            this.inputAppDept1 = this.joborders.appDept1;
            this.appdept = this.joborders.appDept;
            this.appdept1 = this.joborders.appDept1;
            this.contents = this.joborders.content;
            this.contentTypes = this.joborders.contentContentType;
            this.kategori = this.joborders.kategori;
            this.reciver = this.joborders.divReciver;
            this.joborders.idStatusType = this.joborders.idStatusType;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['joborders-request', { page: this.paramPage }]);
        }
    }

    delete(jo: Joborders) {
        if (this.cek_autority !== this.joborders.createdBy) {
            alert('Tidak Bisa Cancel JO Yang Berbeda Pembuat')
        } else {
            // this.confirmationService.confirm({
            //     message: 'Are you sure that you want to cancel the job orders ?',
            //     header: 'Confirmation',
            //     icon: 'fa fa-question-circle',
            //     accept: () => {
            //         this.jobOrdersService.delete(this.idOrder).subscribe((response) => {
            //             this.eventManager.broadcast({
            //                 name: 'jobordersRequestListModification',
            //                 content: 'Deleted an jobOrder'
            //             });
            //             this.toaster.showToaster('info', 'Data Job order cancel', 'Cancel');
            //             this.previousState();
            //         });
            //     },
            // });
            this.joborders = new Joborders();
            this.joborders = jo;
            this.newModalNote1 = true;
        }

    }

    modalCancelJO() {
        const panjang = this.noteCancelJO.replace(/\s/g, '').trim();
        if (this.noteCancelJO === null || this.noteCancelJO === '' || panjang.length < 1) {
            alert('Alasan Di Cancel Harus Di Isi');
        } else {
            this.confirmationService.confirm({
                message: 'Are you sure that you want to cancel the job orders ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.isSaving = true;
                    this.joborders.alasan_cancel = this.noteCancelJO;
                    this.jobOrdersService.changeStatusJO(this.joborders)
                        .subscribe(
                            (res) => {
                            }
                        )
                    this.newModalNote1 = false;
                    this.noteCancelJO = '';
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                    this.toaster.showToaster('info', 'Data Job order cancel', 'Cancel');
                    this.previousState();
                },
            });

        }
    }

    save() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        if (this.cek_autority !== this.joborders.createdBy) {
            this.isSaving = false;
            this.loadingService.loadingStop();
            alert('Tidak Bisa Edit JO Yang Berbeda Pembuat')
        } else {
            this.joborders.appDept = this.inputAppDept;
            this.joborders.appDept1 = this.inputAppDept1;
            this.joborders.divReq = this.internal;
            if (this.joborders.idOrder !== undefined) {
                this.subscribeToSaveResponse(
                    this.jobOrdersService.update(this.joborders));
            } else {
                this.subscribeToSaveResponse(
                    this.jobOrdersService.create(this.joborders));
            }
        }

    }

    protected subscribeToSaveResponse(result: Observable<Joborders>) {
        result.subscribe((res: Joborders) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Joborders) {
        this.print(result.idOrder);
        this.eventManager.broadcast({ name: 'jobOrdersListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Job Order saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    print(id: any) {
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'Job Orders Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
