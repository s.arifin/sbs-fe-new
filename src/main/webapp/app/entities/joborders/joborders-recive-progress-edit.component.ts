import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Joborders } from './joborders.model';
import { JobordersService } from './joborders.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
import { ConfirmationService } from 'primeng/primeng';
@Component({
    selector: 'jhi-joborders-recive-progress-edit',
    templateUrl: './joborders-recive-progress-edit.component.html'
})
export class JobordersReciveProgressEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    joborders: Joborders;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: Date;
    dtStart: Date;
    dtFinish: Date;
    dtRecive: Date;
    dtComplete: Date;

    listKdBarang = [{}];
    dtStartTmln: Date;
    dtFinishTmln: Date;
    internals: any;
    dtFnsBfr: any;
    noteCancelJO: string;
    newModalNote1: boolean;

    divitions: Array<object> = [
        { label: 'PPIC', value: 'PPIC' },
        { label: 'PROD', value: 'PROD' },
        { label: 'LPM', value: 'LPM' },
        { label: 'PURCHASING', value: 'PURCHASING' },
        { label: 'RND', value: 'RND' },
        { label: 'TEKNIK SINDE 1', value: 'TEKNIK SINDE 1' },
        { label: 'TEKNIK SINDE 4', value: 'TEKNIK SINDE 4' },
        { label: 'MARKETING', value: 'MARKETING' },
        { label: 'FNA', value: 'FNA' },
        { label: 'PSU', value: 'PSU' },
        { label: 'IT', value: 'IT' },
        { label: 'HSE', value: 'HSE' }
    ];

    sifats: Array<object> = [
        { label: 'Biasa', value: 'Biasa' },
        { label: 'Segera', value: 'Segera' },
        { label: 'Penting', value: 'Penting' }

    ];

    needs: Array<object> = [
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }

    ];

    problem: Array<object> = [
        { label: 'Perbaikan Data / Penarikan Data', value: 'Perbaikan Data / Penarikan Data' },
        { label: 'Pembuatan Modul Baru / Revisi Modul', value: 'Pembuatan Modul Baru / Revisi Modul' },
        { label: 'Pembuatan Program Baru', value: 'Pembuatan Program Baru' },
        { label: 'Perbaikan Hardware / Infrastruktur', value: 'Perbaikan Hardware / Infrastruktur' },
        { label: 'Koreksi Stock', value: 'Koreksi Stock' },
    ];

    tgl: any;

    constructor(
        protected alertService: JhiAlertService,
        protected confirmationService: ConfirmationService,
        protected jobOrdersService: JobordersService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected loadingService: LoadingService,
        protected dataUtils: JhiDataUtils,

    ) {
        this.joborders = new Joborders();
        this.routeId = 0;
        this.readonly = true;
        this.dtRecive = new Date();
        this.dtStartTmln = new Date();
        this.dtFinishTmln = new Date();
        this.dtFnsBfr = new Date();
        this.dtComplete = new Date();
        this.newModalNote1 = false;
        this.noteCancelJO = '';
    }

    ngOnInit() {
        this.jobOrdersService.getAllRolUser({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.seksie + ' - ' + element.initial,
                    value: element.initial
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], { 'type': fieldContentType });
        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    load() {
        this.internals = this.principal.getIdInternal();
        this.jobOrdersService.find(this.idOrder).subscribe((joborders) => {
            this.joborders = joborders;
            this.dtFinishTmln = new Date(this.joborders.dtThruTmln);
            this.dtStartTmln = new Date(this.joborders.dtFromTmln);
            this.dtOrder = new Date(this.joborders.dtOrder);
            this.dtFnsBfr = new Date(this.joborders.dtFnsBfr);
            this.dtRecive = new Date(this.joborders.dtRecive);
            if (this.joborders.dtStart !== undefined) {
                this.dtFinish = new Date(this.joborders.dtFinish);
                this.dtStart = new Date(this.joborders.dtStart);

            } else {
                this.dtStart = new Date();
                this.dtFinish = new Date();
                this.dtRecive = new Date();
            }

        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['joborders-recive', { page: this.paramPage }]);
        }
    }

    BackStatus() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to back Status?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.jobOrdersService.backStatus(this.idOrder).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Back Status an jobOrder'
                    });
                    this.toaster.showToaster('info', 'Data Job order cancel', 'Cancel');
                    this.previousState();
                });
            },
        });
    }

    delete() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.jobOrdersService.delete(this.idOrder).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                    this.toaster.showToaster('info', 'Data Job order cancel', 'Cancel');
                    this.previousState();
                });
            },
        });
    }

    save() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        this.joborders.dtOrder = this.dtOrder;
        this.joborders.dtStart = this.dtStart;
        this.joborders.dtFinish = this.dtFinish;
        this.joborders.dtRecive = this.dtRecive;
        if (this.joborders.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.jobOrdersService.updateSolve(this.joborders));

        } else {
            this.subscribeToSaveResponse(
                this.jobOrdersService.create(this.joborders));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Joborders>) {
        result.subscribe((res: Joborders) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Joborders) {
        this.print(result.idOrder);
        this.eventManager.broadcast({ name: 'jobordersReciveListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Job Order saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    print(id: any) {
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'Job Orders Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    cancelJO(jo: Joborders) {
        this.jobOrder = new Joborders();
        this.jobOrder = jo;
        this.newModalNote1 = true;
    }

    modalCancelJO() {
        const panjang = this.noteCancelJO.replace(/\s/g, '').trim();
        if (this.noteCancelJO === null || this.noteCancelJO === '' || panjang.length < 1) {
            alert('Alasan Di Cancel Harus Di Isi');
        } else {
            this.confirmationService.confirm({
                message: 'Are you sure that you want to cancel the job orders ?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.isSaving = true;
                    this.joborders.alasan_cancel = this.noteCancelJO;
                    this.jobOrdersService.changeStatusJO(this.joborders)
                        .subscribe(
                            (res) => {
                            }
                        )
                    this.newModalNote1 = false;
                    this.noteCancelJO = '';
                    this.eventManager.broadcast({
                        name: 'jobordersRequestListModification',
                        content: 'Deleted an jobOrder'
                    });
                    this.toaster.showToaster('info', 'Data Job order cancel', 'Cancel');
                    this.previousState();
                },
            });
        }

    }
}
