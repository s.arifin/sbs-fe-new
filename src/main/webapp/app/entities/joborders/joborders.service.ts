import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Joborders, MasterUserRole } from './joborders.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Subject } from 'rxjs/Subject';
import { JhiDateUtils } from 'ng-jhipster';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class JobordersService {

    protected itemValues: Joborders[];
    values: Subject<any> = new Subject<any>();

    private resourceUrl = process.env.API_C_URL + '/api/joborders';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/joborders';

    valueJobOrders = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueJobOrders);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http, protected dateUtils: JhiDateUtils) { }

    // GET DATA JO REQUEST NEW
        query(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET DATA JO REQUEST NEW

    // SEARCH JO REQUEST
        searchRequest(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/searchRequest', options)
                .map((res: any) => this.convertResponse(res));
        }
    // SEARCH JO REQUEST

    // CREATE NEW JO
        createNew(joborders: any): Observable<ResponseWrapper> {
            const copy = JSON.stringify(joborders);
            const options: BaseRequestOptions = new BaseRequestOptions();
            options.headers = new Headers;
            options.headers.append('Content-Type', 'application/json');
            return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
                return res.json();
            });
        }
    // CREATE NEW JO

    // GET AUTORITY USER PEMBUAT JO
        getAllRolUser(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/getallrolluser', options)
                .map((res: Response) => this.convertResponse(res));
        }

        getAllSeksie(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/getallseksie', options)
                .map((res: Response) => this.convertResponse(res));
        }

        getAutority(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/getallautority', options)
                .map((res: Response) => this.convertResponse(res));
        }

        getAutorityK(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/getallautorityK', options)
                .map((res: Response) => this.convertResponse(res));
        }

        getAutorityMGR(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/getallautorityMGR', options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET AUTORITY USER PEMBUAT JO

    // GET DATA JO FOR UPDATE
        find(id: any): Observable<Joborders> {
            return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // GET DATA JO FOR UPDATE

    // CANCEL JO FROM REQUEST
        delete(id: any): Observable<Response> {
            return this.http.delete(`${this.resourceUrl}/${id}`);
        }
    // CANCEL JO FROM REQUEST

    // UPDATE JO FROM REQUEST
        update(joborders: Joborders): Observable<Joborders> {
            const copy = this.convert(joborders);
            return this.http.post(this.resourceUrl + '/update', copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // UPDATE JO FROM REQUEST

    // JO REQUEST YANG SUDAH DI APPROVE / NOT APPROVE
        public getDataApprove(req: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/RequestApprove', options)
                .map((res: Response) => this.convertResponse(res));
        }
    // JO REQUEST YANG SUDAH DI APPROVE / NOT APPROVE

    // JO REQUEST YANG SUDAH DI PROGRESS
        public getDataProgress(req: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/RequestProgress', options)
                .map((res: Response) => this.convertResponse(res));
        }
    // JO REQUEST YANG SUDAH DI PROGRESS

    // JO REQUEST YANG SUDAH DI SOLVE
        querySolve(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/solve`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // JO REQUEST YANG SUDAH DI SOLVE

    // RUBAH STATUS BANYAK JO SOLVE
        changeStatusComplete(joborders: object): Observable<ResponseWrapper> {
            const copy = JSON.stringify(joborders);
            const options: BaseRequestOptions = new BaseRequestOptions();
            options.headers.append('Content-Type', 'application/json');
            return this.http.post(`${this.resourceUrl + '/changeStatusComplete'}`, copy, options).map((res: Response) => {
                return res.json();
            });
        }
    // RUBAH STATUS BANYAK JO SOLVE

    // JO REQUEST YANG DI CANCEL
        queryCancel(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/cancel`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // JO REQUEST YANG DI CANCEL

    // GET DATA JO FOR APPROVE
        public getDataByStatus(req: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/by-status', options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET DATA JO FOR APPROVE

    // CHANGE STATUS JO APPROVE OR NOT
        changeStatus(joborders: object): Observable<ResponseWrapper> {
            const copy = JSON.stringify(joborders);
            const options: BaseRequestOptions = new BaseRequestOptions();
            options.headers.append('Content-Type', 'application/json');
            return this.http.post(`${this.resourceUrl + '/changeStatus'}`, copy, options).map((res: Response) => {
                return res.json();
            });
        }
    // CHANGE STATUS JO APPROVE OR NOT

    // GET DETAIL JO FROM APPROVAL
        queryOne(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/FindOne`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET DETAIL JO FROM APPROVAL

    // SEARCH JO RECIEVE
        searchRecive(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/searchRecive', options)
                .map((res: any) => this.convertResponse(res));
        }
    // SEARCH JO RECIEVE

    // GET NEW JO RECIEVE
        queryRecive(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/recive`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET NEW JO RECIEVE

    // UPDATE USER SEEN JO
        updateSeen(joborders: Joborders): Observable<Joborders> {
            const copy = this.convert(joborders);
            return this.http.post(this.resourceUrl + '/createSeen', copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // UPDATE USER SEEN JO

    // CREATE NEW JO RECIVE STATUS
        create(joborders: Joborders): Observable<Joborders> {
            const copy = this.convert(joborders);
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // CREATE NEW JO RECIVE STATUS

    // UPDATE JO RECIVE STATUS NEW TO PROGRESS
        updateProgress(joborders: Joborders): Observable<Joborders> {
            const copy = this.convert(joborders);
            return this.http.post(this.resourceUrl + '/createProgress', copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // UPDATE JO RECIVE STATUS NEW TO PROGRESS

    // UPDATE STATUS JO RECIVE KE CANCEL
        changeStatusJO(purchasing: Joborders): Observable<ResponseWrapper> {
            const copy = this.convertUpdate(purchasing);
            return this.http.post(`${this.resourceUrl}/set-status-jo`, copy)
                .map((res: Response) => this.convertResponse(res));
        }
    // UPDATE STATUS JO RECIVE KE CANCEL

    // GET JO RECIVE PROGRESS
        queryReciveProgress(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/reciveProgress`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET JO RECIVE PROGRESS

    // UPDATE IS PRINT ON PROGRESS JO TEKNIK ONLY
        updatePrint(joborders: Joborders): Observable<Joborders> {
            const copy = this.convert(joborders);
            return this.http.post(this.resourceUrl + '/createPrint', copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // UPDATE IS PRINT ON PROGRESS JO TEKNIK ONLY

    // UPDATE STATUS JO RECIVE DARI SOLVE TO PROGRESS
        backStatus(id: any): Observable<Response> {
            return this.http.delete(`${this.resourceUrl}/backStat/${id}`);
        }
    // UPDATE STATUS JO RECIVE DARI SOLVE TO PROGRESS

    // UPDATE STATUS JO RECIVE DARI PROGRESS TO SOLVE
        updateSolve(joborders: Joborders): Observable<Joborders> {
            const copy = this.convert(joborders);
            return this.http.post(this.resourceUrl + '/createSolve', copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    // UPDATE STATUS JO RECIVE DARI PROGRESS TO SOLVE

    // GET JO RECIVE SOLVE
        queryReciveSolve(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/reciveSolve`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET JO RECIVE SOLVE

    // CHANGE STATUS SOLVE TO COMPLETE IN JO RECIVE
        changeStatuses(entity: Joborders, id: any, st: any): Observable<ResponseWrapper> {
            const copy = this.convert(entity);
            return this.http.post(`${this.resourceUrl}/set-status/${id}/${st}`, copy)
                .map((res: Response) => this.convertResponse(res));
        }
    // CHANGE STATUS SOLVE TO COMPLETE IN JO RECIVE

    // GET JO RECIVE COMPLETE STATUS
        queryReciveComplete(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/reciveComplete`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET JO RECIVE COMPLETE STATUS

    // GET JO RECIVE CANCEL STATUS
        queryReciveCancel(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(`${this.resourceUrl}/reciveCancel`, options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET JO RECIVE CANCEL STATUS

    // GET JO NOT APPROVE SUPERVISOR
        public getDataByApprovalStatusAndRequirement(req: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/byStatus', options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET JO NOT APPROVE SUPERVISOR

    // GET JO REQUEST STATUS COMPLETE
        public getDataComplete(req: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/RequestComplete', options)
                .map((res: Response) => this.convertResponse(res));
        }
    // GET JO REQUEST STATUS COMPLETE

    // GET IP ADDRESS KETIKA LOGIN
        createNew1(joborders: any): Observable<ResponseWrapper> {
            // const copy = this.convert(purchaseOrder);
            const copy = JSON.stringify(joborders);
            const options: BaseRequestOptions = new BaseRequestOptions();
            options.headers = new Headers;
            options.headers.append('Content-Type', 'application/json');
            return this.http.post(`${this.resourceUrl}/create1`, copy, options).map((res: Response) => {
                return res.json();
            });
        }
    // GET IP ADDRESS KETIKA LOGIN

    // SEARCH JO RECIVE
        search(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/search', options)
                .map((res: any) => this.convertResponse(res));
        }
    // SEARCH JO RECIVE

    // SEARCH JO IN APPROVAL SPV
        search1(req?: any): Observable<ResponseWrapper> {
            const options = createRequestOption(req);
            return this.http.get(this.resourceUrl + '/search1', options)
                .map((res: any) => this.convertResponse(res));
        }
    // SEARCH JO IN APPROVAL SPV

    // UTILITY JO
        passingCustomData(custom) {
            this.enheritancedata.next(custom);
        }

        executeProcess(params?: any, req?: any): Observable<Joborders> {
            const options = createRequestOption(req);
            return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
                return res.json();
            });
        }

        protected convertItemFromServer(json: any): Joborders {
            const entity: Joborders = Object.assign(new Joborders(), json);
            return entity;
        }

        find1(id: any): Observable<ResponseWrapper> {
            const options = createRequestOption(id);
            return this.http.get(`${this.resourceUrl}/${id}/find`, options).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertResponse(jsonResponse);
            });
        }

        private convertResponse(res: Response): ResponseWrapper {
            const jsonResponse = res.json();
            return new ResponseWrapper(res.headers, jsonResponse, res.status);
        }

        private convert(joborders: Joborders): Joborders {
            const copy: Joborders = Object.assign({}, joborders);
            return copy;
        }

        pushItems(data: Joborders[]) {
            this.itemValues = data;
            this.values.next(this.itemValues);
        }

        private convertUpdate(purchasing: Joborders): Joborders {
            const copy: Joborders = Object.assign({}, purchasing);
            return copy;
        }
    // UTILITY JO

}
