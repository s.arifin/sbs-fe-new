import { Injectable } from '@angular/core';
import * as pako from 'pako';

@Injectable()
export class FileCompressionService {

  constructor() { }

  compressFile(blob: Blob): Promise<Blob> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = (event: any) => {
        const compressedData = pako.deflate(event.target.result, { to: 'string' });
        const compressedBlob = new Blob([compressedData], { type: blob.type });
        resolve(compressedBlob);
      };

      reader.readAsArrayBuffer(blob);
    });
  }
}
