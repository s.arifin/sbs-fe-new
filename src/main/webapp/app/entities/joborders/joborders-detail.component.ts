import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Joborders } from './joborders.model';
import { JobordersService } from './joborders.service';

@Component({
    selector: 'jhi-joborders-detail',
    templateUrl: './joborders-detail.component.html'
})
export class JobordersDetailComponent implements OnInit, OnDestroy {

    joborders: Joborders;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jobordersService: JobordersService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJoborders();
    }

    load(id) {
        this.jobordersService.find(id).subscribe((joborders) => {
            this.joborders = joborders;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJoborders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jobordersListModification',
            (response) => this.load(this.joborders.id)
        );
    }
}
