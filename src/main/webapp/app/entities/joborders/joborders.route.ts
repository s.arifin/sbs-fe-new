import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { JobordersNewComponent } from './joborders-new.component';
import { JobordersComponent } from './joborders.component';
import { JobordersDetailComponent } from './joborders-detail.component';
import { JobordersPopupComponent } from './joborders-dialog.component';
import { JobordersDeletePopupComponent } from './joborders-delete-dialog.component';
import { JobordersEditComponent } from './joborders-edit.component';
import { JobordersReciveComponent } from './joborders-recive.component';
import { JobordersSolveEditComponent } from './joborders-solve-edit.component';
import { JobordersCancelComponent } from './joborders-cancel.component';
import { JobordersSolveComponent } from './joborders-solve.component';
import { JobordersReciveEditComponent } from './joborders-recive-edit.component';
import { JobordersReciveProgressEditComponent } from './joborders-recive-progress-edit.component';
// import { ApprovalJobOrderComponent } from './index';
import { ApprovalJobOrderComponent } from './approval-jo/approval-job-order.component'
import { ApprovalJobOrderDetailComponent } from './approval-jo/approval-job-order-detail.component';
// import {BudgetingItComponent} from '../purchase-order/budgeting-it/it-budgeting.component'
@Injectable()
export class JobordersResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const jobordersRoute: Routes = [
    {
        path: 'joborders-request',
        component: JobordersComponent,
        resolve: {
            'pagingParams': JobordersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'joborders-solve',
        component: JobordersSolveComponent,
        resolve: {
            'pagingParams': JobordersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joborders-cancel',
        component: JobordersCancelComponent,
        resolve: {
            'pagingParams': JobordersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joborders-recive',
        component: JobordersReciveComponent,
        resolve: {
            'pagingParams': JobordersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'new-job-order',
        component: JobordersNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'job-order/approval',
        component: ApprovalJobOrderComponent,
        resolve: {
            'pagingParams': JobordersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.korsal.approval-unit-document-message'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-job-order-edit/:idOrder',
        component: ApprovalJobOrderDetailComponent,
        resolve: {
            'pagingParams': JobordersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jobordersPopupRoute: Routes = [
    {
        path: 'joborders-new',
        component: JobordersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'joborders/:route/:page/:id/edit',
        component: JobordersEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joborders/:route/:page/:id/editSolve',
        component: JobordersSolveEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joborders/:route/:page/:id/editRecive',
        component: JobordersReciveEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joborders/:route/:page/:id/editprogress',
        component: JobordersReciveProgressEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'joborders/:id/edit',
        component: JobordersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'joborders/:id/delete',
        component: JobordersDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.joborders.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
