import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrderItemService } from './purchase-order-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { PurchaseOrderItemPopupService } from './purchase-order-item-popup.service';
import { PurchaseOrderService, PurchaseOrder, CustomSupplyer, MasterBahan, MasterBarang } from '../purchase-order';
import { CommonUtilService } from '../../shared';
import * as _ from 'lodash';
import { InternalService } from '../internal';
import { PurchaseOrderItem } from './purchase-order-item.model';
import { SpawnSyncOptions } from 'child_process';

@Component({
    selector: 'jhi-purchase-order-item-in-purchase-order-as-list',
    templateUrl: './purchase-order-item-in-purchase-order-as-list.component.html'
})
export class PurchaseOrderItemInPurchaseOrderAsListComponent implements OnInit, OnDestroy, OnChanges {

    dtOrder: Date;
    @Input() idPurchaseOrder: any;
    @Input() peruntukan: any;
    @Input() idPurOrdItem: any;
    @Input() idstatus: any;
    @Input() usrLogin: any;
    purchaseOrderItem: PurchaseOrderItem;
    isSaving: boolean;
    itemType: number;
    puechaseOrders: PurchaseOrder[];
    currentAccount: any;
    purchaseOrderItems: PurchaseOrderItem[];
    purchaseOrder: PurchaseOrder;
    purchaseOrderItemEdit: PurchaseOrderItem;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    newModal: boolean;
    newModalEdit: boolean;
    routeData: any;
    currentSearch: string;
    isOpen: Boolean = false;
    index: number;
    loc: string;
    newSatuans: any;
    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputOfferNumber: any;
    inputSupplyerName: string;
    inputSupplyerNameEdit: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: any;
    inputRemainingStock: any;
    inputRemainingPo: any;
    unitChecks: PurchaseOrder[];
    unitChecksCopy: PurchaseOrder[];
    hasilSave: PurchaseOrder;
    supplier: CustomSupplyer;
    Supplyer: CustomSupplyer[];
    selectedSupplier: any;
    listSupplier = [{ label: 'Please Select or Insert', value: null }];

    satHarga: Array<object> = [
        { label: 'RUPIAH - IDR', value: 'IDR' },
        { label: 'DOLAR AS - USD', value: 'USD' },
        { label: 'EURO - EUR', value: 'EUR' },
        { label: 'DOLAR SINGAPURA - SGD', value: 'SGD' },
        { label: 'DOLAR AUSTRALIA - AUD', value: 'AUD' },
        { label: 'YEN - JPY', value: 'JPY' },
        { label: 'POUND - GBP', value: 'GBP' },
    ];

    inputSatHarga: any;
    public barang: MasterBahan;
    public Barang: MasterBahan[];
    public selectedBarang: any;
    public listBarang = [{ label: 'Please Select or Insert', value: null }];

    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{ label: 'Please Select or Insert', value: null }];

    newSuplier: any;
    newSuplierEdit: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];

    newKdBarang: any;
    newKdBarangs: any[];
    filteredKdBarang: any[];
    pilihKdBarang: any;
    internal: any;
    pilihTipe: any;
    newProj: any;
    newProjs: any[];
    listProj = [{}];
    picBarang: any;
    inputSatuan: any;
    created: any;
    currentDiusul: any;
    currentMGR: string;
    currentKBG: string;
    curAccount: string;
    inputDtRequest: any;
    noUrut: any;
    listSatuan = [{}];

    dtSentEdit: any;
    dtOfferEdit: any;
    tmpEdtDtOffer: any;

    constructor(
        protected purchaseOrderService: PurchaseOrderService,
        protected purchaseOrderItemService: PurchaseOrderItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected paginationUtil: JhiPaginationUtil,
        protected modalService: NgbModal,
        protected paginationConfig: PaginationConfig,
        protected loadingService: LoadingService,

        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected toaster: ToasterService,
        protected commontUtilService: CommonUtilService,
        protected internalService: InternalService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'noUrut';
        this.reverse = 'asc';
        this.purchaseOrder = new PurchaseOrder();
        this.newModal = false;
        this.newModalEdit = false;
        this.purchaseOrderItem = new PurchaseOrderItem();
        this.purchaseOrderItemEdit = new PurchaseOrder();
        // this.dtSentEdit = new Date;
        // this.dtOfferEdit = new Date;
        this.purchaseOrder = new PurchaseOrder();
        this.dtOrder = new Date();
        this.barang = new MasterBahan();
        this.isSelectSupplier = false;
        this.kdbarang = new MasterBahan();
        this.inputSatuan = null;
        this.purchaseOrder.createdBy = null;
        this.inputDtRequest = new Date;
        this.noUrut = 0;

        // // this.routeData = this.activatedRoute.data.subscribe((data) => {
        // //     this.page = data.pagingParams.page;
        // //     this.previousPage = data.pagingParams.page;
        // //     this.reverse = data.pagingParams.ascending;
        // //     this.predicate = data.pagingParams.predicate;
        // // });
        // this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
        //     this.activatedRoute.snapshot.params['search'] : '';
        // const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
        //             this.activatedRoute.snapshot.params['page'] : 0;
        // this.page = lastPage > 0 ? lastPage : 1;
        // this.first = (this.page - 1) * this.itemsPerPage;
    }
    protected resetMe() {
        this.selectedBarang = null;
        this.curAccount = null;
    }

    filterBarangSingle(event) {
        const query = event.query;
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.getNewBarangTeknik().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        } else {
            this.purchaseOrderService.getNewBarang().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        }
    }

    filterBarang(query, newBarangs: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.barang.idMasBahan = this.newBarang.idMasBahan;
        console.log('idmasbahan : ' + this.newBarang.idMasBahan);
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.newBarang.idMasBahan)
                .subscribe(
                    (res: MasterBarang) => {
                        console.log('ini isi bahan = ', res.kd_barang);
                        this.inputCodeName = res.kd_barang;
                        this.inputProdukName = res.nama_barang;
                        this.newKdBarang = res.kd_barang;
                        this.picBarang = res.pic;
                        this.inputSatuan = res.satuan;
                    },
                    (res: MasterBahan) => {
                        this.inputCodeName = null;
                        console.log('ini isi bahan eror = ', this.inputCodeName);
                    }
                )
        } else {
            this.purchaseOrderService.findBarang(this.newBarang.idMasBahan)
                .subscribe(
                    (res: MasterBahan) => {
                        console.log('ini isi bahan = ', res.kdBahan);
                        if (res.kategori === null || res.kategori === '' || res.kategori === undefined) {
                            alert('Kategori Barang Kosong, Silahkan edit terlebih dahulu di Master Bahan sebelum membuat PP dengan item ini');
                        } else if (res.pic === null || res.pic === '' || res.pic === undefined) {
                            alert('PIC Item Kosong, Silahkan edit terlebih dahulu di Master Bahan sebelum membuat PP dengan item ini');
                        } else {
                            this.inputCodeName = res.kdBahan;
                            this.inputProdukName = res.nmBahan;
                            this.newKdBarang = res.kdBahan;
                            this.picBarang = res.pic;
                            this.inputSatuan = res.satuan;
                            if (this.internal === 'PPIC') {
                                this.purchaseOrderService.getAllSatuan(res.kdBahan)
                                    .subscribe(
                                        (res1: MasterBahan) => {
                                            this.listSatuan = [{}];
                                            this.newSatuans = res1;
                                            this.newSatuans.forEach((e) => {
                                                this.listSatuan.push({
                                                    label: e.kdBahan,
                                                    value: e.kdBahan
                                                });
                                            });
                                            this.listSatuan = this.listSatuan.filter((item) => Object.keys(item).length > 0);
                                        },
                                        (res1: MasterBahan) => {

                                        }
                                    )

                            } else {

                            }
                        }

                    },
                    (res: MasterBahan) => {
                        this.inputCodeName = null;
                        console.log('ini isi bahan eror = ', this.inputCodeName);
                    }
                )
        }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.purchaseOrderService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inputSupplyerName = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
        console.log('select supplier ' + this.inputSupplyerName);
    }

    filterKdBarangSingle(event) {
        // console.log('data Kode barang : ', this.pilihTipe);
        const query = event.query;
        this.purchaseOrderService.getNewKdBarang().then((newKdBarangs) => {
            this.filteredKdBarang = this.filterKdBarang(query, newKdBarangs);
        });
    }

    filterKdBarang(query, newKdBarangs: any[]): any[] {
        const filtBahan: any[] = [];
        for (let i = 0; i < newKdBarangs.length; i++) {
            const newKdBarang = newKdBarangs[i];
            if (newKdBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtBahan.push({
                    label: newKdBarang.kdBahan + ' - ' + newKdBarang.nmBahan,
                    value: newKdBarang.nmBahan
                });
            }
            // console.log('idkd : ', this.newKdBarang.idMasBahan);
        }
        return filtBahan;
    }

    public selectKdBarang(isSelect?: boolean): void {
        console.log('idmasbahankd : ' + this.selectedKdBarang);
        this.kdbarang.idMasBahan = this.selectedKdBarang;
        if (this.principal.getIdInternal() === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.selectedKdBarang)
                .subscribe(
                    (res: MasterBarang) => {
                        console.log('ini isi bahan = ', res.kd_barang);
                        this.inputCodeName = res.kd_barang;
                        this.inputProdukName = res.nama_barang;
                    },
                    (res: MasterBahan) => {
                        this.inputCodeName = null;
                        console.log('ini isi bahan eror = ', this.inputCodeName);
                    }
                )
        } else {
            this.purchaseOrderService.findBarang(this.selectedKdBarang)
                .subscribe(
                    (res: MasterBahan) => {
                        console.log('ini isi bahan = ', res.kdBahan);
                        this.inputCodeName = res.kdBahan;
                        this.inputProdukName = res.nmBahan;
                    },
                    (res: MasterBahan) => {
                        this.inputCodeName = null;
                        console.log('ini isi bahan eror = ', this.inputCodeName);
                    }
                )
        }
    }

    onSuccessBarang(data, headers) {
        this.purchaseOrderItem.codeProduk = data.kdBahan;
    }

    onErrorBarang(error) {
        this.purchaseOrderItem.codeProduk = null;
    }

    loadAll1() {
        this.internal = this.principal.getIdInternal();
        console.log('oninit internal = ', this.curAccount);
        this.purchaseOrderItem = new PurchaseOrderItem();
        this.loadingService.loadingStart();
        this.purchaseOrderItemService.queryFilterBy({
            query: 'idInternal:' + this.idPurchaseOrder,
            idPurOrdItem: this.idPurOrdItem,
            page: 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<PurchaseOrder>();
    }

    loadAll() {
        this.internal = this.principal.getIdInternal();
        console.log('oninit internal = ', this.curAccount);
        this.purchaseOrderItem = new PurchaseOrderItem();
        this.loadingService.loadingStart();
        this.purchaseOrderItemService.queryFilterBy({
            query: 'idInternal:' + this.idPurchaseOrder,
            idPurOrdItem: this.idPurOrdItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<PurchaseOrder>();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/purchase-order-item'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/purchase-order-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.activeModal.dismiss('cancel');
        this.loadAll();
    }
    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);

        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
                this.purchaseOrder = purchaseOrder;
                this.created = purchaseOrder.createdBy.toUpperCase();
                this.currentDiusul = purchaseOrder.diusulkan;
                this.currentMGR = purchaseOrder.appDept;
                this.currentKBG = purchaseOrder.appPabrik;
                this.loc = purchaseOrder.loc;
                // console.log('this.purchaseOrder', purchaseOrder);
                // console.log('curAccount: ', this.usrLogin);
                // console.log('created: ', this.created);
                // console.log('diusul: ', this.currentDiusul);
                // console.log('curMGR: ', this.currentMGR);
                // console.log('curKBG: ', this.currentKBG);
                // // this.onSuccessDataPP();
            });
        this.purchaseOrderService.getAllProj({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newProj = res.json;
            this.newProj.forEach((element) => {
                this.listProj.push({
                    label: element.nm_project,
                    value: element.nm_project
                });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.registerChangeInBillingItems();
        this.isSaving = false;
        this.loadAll();
    }
    masukAddNewData(): void {
        const data = new PurchaseOrder();
        this.newModal = true;
        data.codeProduk = this.inputCodeName;
        console.log('codeProduk' + data.codeProduk);
        if (this.inputCodeName == null) {
            data.produkName = this.newBarang;
        } else {
            data.produkName = this.inputProdukName;
        }
        if (this.isSelectSupplier === true) {
            data.supplyerName = this.inputSupplyerName;
            console.log('ini suplier true' + data.supplyerName);
        } else {
            data.supplyerName = this.newSuplier;
            console.log('ini suplier' + data.supplyerName);
        }
        data.offerNumber = this.inputOfferNumber;
        data.qty = this.inputQty;
        data.dtOffer = this.inputDtOffer;
        data.supplyerPrice = this.inputSupplyerPrice;
        data.necessity = this.inputNecessity;
        data.dtSent = this.inputDtRequest;
        data.dtNecessity = this.inputDtNecessity;
        data.remainingStock = this.inputRemainingStock;
        data.remainingPo = this.inputRemainingPo;
        data.mataUang = this.inputSatHarga;
        this.unitChecks = [...this.unitChecks, data];
        this.clearDataInput();
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    valMasukAddNewData() {
        if (this.idstatus === 5) {
            this.masukAddNewData()
        } else if (this.idstatus === 12) {
            this.masukAddNewData()
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Masuk Proses Approved. Terima Kasih.');
        }
    }

    onSuccessDataPP() {
        if (this.curAccount.toUpperCase() === this.purchaseOrder.createdBy.toUpperCase()) {
            this.valMasukAddNewData()
        } else if (this.curAccount === this.currentDiusul) {
            this.valMasukAddNewData()
        } else if (this.curAccount === this.currentMGR) {
            this.valMasukAddNewData()
        } else if (this.curAccount === this.currentKBG) {
            this.valMasukAddNewData()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    addNewData(): void {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
                this.purchaseOrder = purchaseOrder;
                this.created = purchaseOrder.createdBy.toUpperCase();
                this.currentDiusul = purchaseOrder.diusulkan;
                this.currentMGR = purchaseOrder.appDept;
                this.currentKBG = purchaseOrder.appPabrik;
                this.loc = purchaseOrder.loc;
                // console.log('this.purchaseOrder', purchaseOrder);
                // console.log('curAccount: ', this.usrLogin);
                // console.log('created: ', this.created);
                // console.log('diusul: ', this.currentDiusul);
                // console.log('curMGR: ', this.currentMGR);
                // console.log('curKBG: ', this.currentKBG);
                // // this.onSuccessDataPP();
            });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);
        this.onSuccessDataPP();
    }

    editItem(idItem: any): void {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
                this.purchaseOrder = purchaseOrder;
                this.created = purchaseOrder.createdBy.toUpperCase();
                this.currentDiusul = purchaseOrder.diusulkan;
                this.currentMGR = purchaseOrder.appDept;
                this.currentKBG = purchaseOrder.appPabrik;
                this.loc = purchaseOrder.loc;

            });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);
        this.onSuccessDataItemPP(idItem);
    }

    onSuccessDataItemPP(idItem: any) {
        if (this.curAccount.toUpperCase() === this.purchaseOrder.createdBy.toUpperCase()) {
            this.valMasukAddNewDatItem(idItem)
        } else if (this.curAccount === this.currentDiusul) {
            this.valMasukAddNewDatItem(idItem)
        } else if (this.curAccount === this.currentMGR) {
            this.valMasukAddNewDatItem(idItem)
        } else if (this.curAccount === this.currentKBG) {
            this.valMasukAddNewDatItem(idItem)
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    valMasukAddNewDatItem(idItem: any) {
        if (this.idstatus === 5) {
            this.masukAddNewDataItem(idItem)
        } else if (this.idstatus === 12) {
            this.masukAddNewDataItem(idItem)
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Masuk Proses Approved. Terima Kasih.');
        }
    }

    masukAddNewDataItem(idItem: any): void {
        this.purchaseOrderService.findItem(idItem).subscribe(
            (purchaseOrderItem) => {
                this.newModalEdit = true;
                this.purchaseOrderItemEdit = purchaseOrderItem;
                this.newSuplierEdit = purchaseOrderItem.master_supplier;
                this.dtSentEdit = new Date(purchaseOrderItem.dtSent);
                this.dtOfferEdit = new Date(purchaseOrderItem.dtOffer);
                this.tmpEdtDtOffer = purchaseOrderItem.dtOffer;
                console.log('tmp : ', this.tmpEdtDtOffer);
                if (this.internal === 'PPIC') {
                    this.purchaseOrderService.getAllSatuan(purchaseOrderItem.codeProduk)
                        .subscribe(
                            (res1: MasterBahan) => {
                                this.listSatuan = [{}];
                                this.newSatuans = res1;
                                this.newSatuans.forEach((e) => {
                                    this.listSatuan.push({
                                        label: e.kdBahan,
                                        value: e.kdBahan
                                    });
                                });
                                this.listSatuan = this.listSatuan.filter((item) => Object.keys(item).length > 0);
                            },
                            (res1: MasterBahan) => {

                            }
                        )

                } else {

                }
            });
    }

    clearDataInput(): void {
        this.selectedKdBarang = null;
        this.inputCodeName = null;
        this.inputProdukName = null;
        this.inputOfferNumber = null;
        this.inputQty = null;
        this.inputDtOffer = null;
        this.inputSupplyerName = null;
        this.inputSupplyerPrice = null;
        this.inputNecessity = null;
        this.inputDtNecessity = null;
        this.inputRemainingStock = null;
        this.inputRemainingPo = null;
        this.selectedBarang = null;
        this.selectedSupplier = null;
        this.newSuplier = null;
        this.newBarang = null;
        this.isSelectSupplier = false;
        this.inputSatHarga = null;
        this.picBarang = null;
        this.inputDtRequest = null;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idPurchaseOrder']) {
            this.loadAll();
        }
        if (changes['idPurOrdItem']) {
            this.loadAll();
        }
        // if (changes['idInventoryItem']) {
        //     this.loadAll();
        // }
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PurchaseOrderItem) {
        return item.idPurOrdItem;
    }
    // protected registerChangeInBillingItems(): void {
    //     this.eventSubscriber = this.billingItemService.values.subscribe(
    //         (res) => {
    //             this.billingItems = res;
    //         }
    //     )
    // }
    registerChangeInBillingItems() {
        this.eventSubscriber = this.eventManager.subscribe('PurchaseOrderItemListModification', (response) => this.loadAll1());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'noUrut') {
            result.push('noUrut');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = data.totalItems / this.itemsPerPage;
        // this.totalItems = data[0].Totaldata;
        // this.queryCount = this.totalItems;
        this.purchaseOrderItems = data;
        this.loadingService.loadingStop();
        console.log('ini data internal', data);
    }

    protected onError(error: any) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
        // this.loadingService.loadingStop();
        // this.toaster.showToaster('warning', 'billingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.purchaseOrderItemService.executeProcess(item).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    // loadDataLazy(event: LazyLoadEvent) {
    //     this.itemsPerPage = event.rows;
    //     this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
    //     this.previousPage = this.page;

    //     if (event.sortField !== undefined) {
    //         this.predicate = event.sortField;
    //         this.reverse = event.sortOrder;
    //     }
    //     this.loadAll();
    // }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.purchaseOrderItemService.update(event.data)
                .subscribe((res: PurchaseOrderItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.purchaseOrderItemService.create(event.data)
                .subscribe((res: PurchaseOrderItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: PurchaseOrderItem) {
        this.toasterService.showToaster('info', 'PurchaseOrderItem Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    masukSave() {
        this.purchaseOrderItem.idPurchaseOrder = this.idPurchaseOrder;
        this.purchaseOrderItem.codeProduk = this.inputCodeName;
        this.purchaseOrderItem.unit = this.inputSatuan;
        if (this.purchaseOrderItem.codeProduk == null) {
            this.purchaseOrderItem.produkName = this.newBarang;
        } else {
            this.purchaseOrderItem.produkName = this.inputProdukName;
        }
        if (this.isSelectSupplier === true) {
            this.purchaseOrderItem.supplyerName = this.inputSupplyerName;
            console.log('ini suplier true' + this.purchaseOrderItem.supplyerName);
        } else {
            this.purchaseOrderItem.supplyerName = this.newSuplier;
            console.log('ini suplier' + this.purchaseOrderItem.supplyerName);
        }
        this.purchaseOrderItem.noUrut = this.noUrut;
        this.purchaseOrderItem.mataUang = this.inputSatHarga;
        this.purchaseOrderItem.dtSent = this.inputDtRequest;
        this.purchaseOrderItem.remainingStock = this.inputRemainingStock;
        this.isSaving = true;
        if (this.purchaseOrderItem.codeProduk !== undefined && this.purchaseOrderItem.qty !== undefined) {
            this.newModal = false;
            this.loadingService.loadingStart();
            if (this.purchaseOrderItem.idPurOrdItem !== undefined) {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.update(this.purchaseOrderItem));
            } else {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.create(this.purchaseOrderItem));
            }
        } else {
            this.toaster.showToaster('warning', 'Purchase Request Changed', 'Data Belum Lengkap.');
        }
    }

    save() {
        if (this.curAccount === this.purchaseOrder.createdBy.toUpperCase()) {
            this.masukSave()
        } else if (this.curAccount === this.currentDiusul) {
            this.masukSave()
        } else if (this.curAccount === this.currentMGR) {
            this.masukSave()
        } else if (this.curAccount === this.currentKBG) {
            this.masukSave()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    protected subscribeToSaveResponse(result: Observable<PurchaseOrderItem>) {
        result.subscribe((res: PurchaseOrderItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PurchaseOrderItem) {
        this.loadingService.loadingStop();
        this.eventManager.broadcast({ name: 'PurchaseOrderItemListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Item saved !');
        this.isSaving = false;
        // this.purchaseOrder = new PurchaseOrderItem();
        this.activeModal.dismiss(result);
        this.isSelectSupplier = false;
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onErrorr(error);
    }

    protected onErrorr(error) {
        alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.');
        this.toaster.showToaster('warning', 'PIC Pembelian Barang Berbeda, Pilih Barang Yang Berdasarkan Satu PIC', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBillingById(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }
    masukDelete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.purchaseOrderItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'PurchaseOrderItemListModification',
                        content: 'Deleted an purchaseOrderItem'
                    });
                });
            }
        });
    }
    valMasukDelete(id: any) {
        if (this.idstatus === 5) {
            this.masukDelete(id)
        } else if (this.idstatus === 12) {
            this.masukDelete(id)
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Masuk Proses Approved. Terima Kasih.');
        }
    }
    delete(id: any) {
        this.purchaseOrderService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
                this.purchaseOrder = purchaseOrder;
                this.created = purchaseOrder.createdBy.toUpperCase();
                this.currentDiusul = purchaseOrder.diusulkan;
                this.currentMGR = purchaseOrder.appDept;
                this.currentKBG = purchaseOrder.appPabrik;
                this.loc = purchaseOrder.loc;
            });
        console.log('log account: ', this.curAccount);
        if (this.curAccount === this.purchaseOrder.createdBy.toUpperCase()) {
            this.valMasukDelete(id)
        } else if (this.curAccount === this.currentDiusul) {
            this.valMasukDelete(id)
        } else if (this.curAccount === this.currentMGR) {
            this.valMasukDelete(id)
        } else if (this.curAccount === this.currentKBG) {
            this.valMasukDelete(id)
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    saveEdit() {
        if (this.curAccount === this.purchaseOrder.createdBy.toUpperCase()) {
            this.masukSaveEdit()
        } else if (this.curAccount === this.currentDiusul) {
            this.masukSaveEdit()
        } else if (this.curAccount === this.currentMGR) {
            this.masukSaveEdit()
        } else if (this.curAccount === this.currentKBG) {
            this.masukSaveEdit()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }
    masukSaveEdit() {
        const tmp = new Date;
        const dari = (tmp.getFullYear() - 10) + '-' + (tmp.getMonth() + 1) + '-' + tmp.getDate();
        const smp = new Date(dari);
        console.log('ini tmp1 = ', smp);

        this.purchaseOrderItemEdit.supplyerName = this.inputSupplyerNameEdit;
        if (this.dtSentEdit > smp) {
            this.purchaseOrderItemEdit.dtSent = this.dtSentEdit;
        }
        if (this.dtOfferEdit > smp) {
            this.purchaseOrderItemEdit.dtOffer = this.dtOfferEdit;
        }
        this.isSaving = true;
        if (this.purchaseOrderItemEdit.qty !== undefined) {
            this.newModalEdit = false;
            this.loadingService.loadingStart();
            if (this.purchaseOrderItemEdit.idPurOrdItem !== undefined) {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.saveEdit(this.purchaseOrderItemEdit));
            } else {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.saveEdit(this.purchaseOrderItemEdit));
            }
        } else {
            this.toaster.showToaster('warning', 'Purchase Request Changed', 'Data Belum Lengkap.');
        }
    }
    public selectSupplierEdit(isSelect?: boolean): void {
        this.inputSupplyerNameEdit = this.newSuplierEdit.nmSupplier;
        console.log('select supplier edit ' + this.inputSupplyerNameEdit);
    }
}
