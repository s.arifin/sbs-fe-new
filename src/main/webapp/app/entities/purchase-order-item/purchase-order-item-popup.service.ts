import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PurchaseOrderItem } from './purchase-order-item.model';
import { PurchaseOrderItemService } from './purchase-order-item.service';

@Injectable()
export class PurchaseOrderItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idPurchaseOrder: any;
    idInventoryItem: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected purchaseOrderItemService: PurchaseOrderItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.purchaseOrderItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.purchaseOrderItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new PurchaseOrderItem();
                    data.idPurchaseOrder = this.idPurchaseOrder;
                    this.ngbModalRef = this.purchaseOrderItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    purchaseOrderItemModalRef(component: Component, purchaseOrderItem: PurchaseOrderItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.purchaseOrderItem = purchaseOrderItem;
        modalRef.componentInstance.idPurchaseOrder = this.idPurchaseOrder;
        // modalRef.componentInstance.idInventoryItem = this.idInventoryItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.purchaseOrderItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    purchaseOrderItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
