import { MasterSupplier } from '../master-supplier';
import { BaseEntity } from './../../shared';

export class PurchaseOrderItem implements BaseEntity {
    constructor(
        public id?: any,
        public idPurOrdItem?: any,
        public idPurchaseOrder?: any,
        public codeProduk?: any,
        public produkName?: string,
        public produkName2?: string,
        public produkName3?: string,
        public produkName4?: string,
        public produkName5?: string,
        public qty?: number,
        public qtyPO?: number,
        public unit?: string,
        public dtOffer?: any,
        public supplyerName?: string,
        public supplyerPrice?: number,
        public necessity?: string,
        public dtNecessity?: Date,
        public dtcreate?: Date,
        public remainingStock?: string,
        public remainingPo?: any,
        public supplierPrice?: number,
        public mataUang?: any,
        public OfferNumber?: any,
        public qtybookingpo?: number,
        public noUrut?: any,
        public rejectNote?: string,
        public dtSent?: any,
        public master_supplier?: MasterSupplier,
        public note_pbl?: string,
        public qtytopo?: number,
        public statustype?: number,
        public isSelected?: boolean,
    ) {
        this.isSelected = false;
    }
}

export class BillingItem implements BaseEntity {
    constructor(
        public id?: any,
        public idBillingItem?: any,
        public idItemType?: number,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescriptionription?: number,
        public idProduct?: string,
        public itemDescription?: string,
        public qty?: number,
        public unitPrice?: number,
        public basePrice?: number,
        public discount?: number,
        public taxAmount?: number,
        public totalAmount?: number,
        public billingId?: any,
        public inventoryItemId?: any,
        public idInternal?: any,
        public sequence?: any,
    ) {
    }
}
