import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PurchaseOrderItem, BillingItem } from './purchase-order-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PurchaseOrderItemService {
    protected itemValues: PurchaseOrderItem[];
    values: Subject<any> = new Subject<any>();
    protected resourceUrl =  process.env.API_C_URL + '/api/purchaseorderitems';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/billing-items';

    constructor(protected http: Http) { }

    create(purchaseOrderItem: PurchaseOrderItem): Observable<PurchaseOrderItem> {
        const copy = this.convert(purchaseOrderItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }
    saveEdit(purchaseOrderItem: PurchaseOrderItem): Observable<PurchaseOrderItem> {
        const copy = this.convert(purchaseOrderItem);
        return this.http.post(this.resourceUrl + '/saveedit', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(purchaseOrderItem: PurchaseOrderItem): Observable<PurchaseOrderItem> {
        const copy = this.convert(purchaseOrderItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<PurchaseOrderItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findBillingForTaxList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/req-faktur-pajak', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<PurchaseOrderItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<PurchaseOrderItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PurchaseOrderItem.
     */
    protected convertItemFromServer(json: any): PurchaseOrderItem {
        const entity: PurchaseOrderItem = Object.assign(new PurchaseOrderItem(), json);
        return entity;
    }

    /**
     * Convert a PurchaseOrderItem to a JSON which can be sent to the server.
     */
    protected convert(purchaseOrderItem: PurchaseOrderItem): PurchaseOrderItem {
        if (purchaseOrderItem === null || purchaseOrderItem === {}) {
            return {};
        }
        // const copy: BillingItem = Object.assign({}, billingItem);
        const copy: PurchaseOrderItem = JSON.parse(JSON.stringify(purchaseOrderItem));
        return copy;
    }

    protected convertList(purchaseOrderItem: PurchaseOrderItem[]): PurchaseOrderItem[] {
        const copy: PurchaseOrderItem[] = purchaseOrderItem;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PurchaseOrderItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

}
