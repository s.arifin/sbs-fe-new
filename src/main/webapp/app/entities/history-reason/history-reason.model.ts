import { BaseEntity } from './../../shared';

export class HistoryReason implements BaseEntity {
    constructor(
        public id?: number,
        public idreason?: string,
        public idforeign?: string,
        public dtcreated?: any,
        public createdby?: string,
        public descriptions?: string,
    ) {
    }
}
