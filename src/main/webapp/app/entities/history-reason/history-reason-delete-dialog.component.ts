import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HistoryReason } from './history-reason.model';
import { HistoryReasonPopupService } from './history-reason-popup.service';
import { HistoryReasonService } from './history-reason.service';

@Component({
    selector: 'jhi-history-reason-delete-dialog',
    templateUrl: './history-reason-delete-dialog.component.html'
})
export class HistoryReasonDeleteDialogComponent {

    historyReason: HistoryReason;

    constructor(
        private historyReasonService: HistoryReasonService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.historyReasonService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'historyReasonListModification',
                content: 'Deleted an historyReason'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-history-reason-delete-popup',
    template: ''
})
export class HistoryReasonDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private historyReasonPopupService: HistoryReasonPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.historyReasonPopupService
                .open(HistoryReasonDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
