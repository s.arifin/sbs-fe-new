import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { HistoryReason } from './history-reason.model';
import { HistoryReasonService } from './history-reason.service';

@Component({
    selector: 'jhi-history-reason-detail',
    templateUrl: './history-reason-detail.component.html'
})
export class HistoryReasonDetailComponent implements OnInit, OnDestroy {

    historyReason: HistoryReason;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private historyReasonService: HistoryReasonService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHistoryReasons();
    }

    load(id) {
        this.historyReasonService.find(id).subscribe((historyReason) => {
            this.historyReason = historyReason;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHistoryReasons() {
        this.eventSubscriber = this.eventManager.subscribe(
            'historyReasonListModification',
            (response) => this.load(this.historyReason.id)
        );
    }
}
