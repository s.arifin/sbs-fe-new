import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HistoryReasonComponent } from './history-reason.component';
import { HistoryReasonDetailComponent } from './history-reason-detail.component';
import { HistoryReasonPopupComponent } from './history-reason-dialog.component';
import { HistoryReasonDeletePopupComponent } from './history-reason-delete-dialog.component';

@Injectable()
export class HistoryReasonResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const historyReasonRoute: Routes = [
    {
        path: 'history-reason',
        component: HistoryReasonComponent,
        resolve: {
            'pagingParams': HistoryReasonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.historyReason.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'history-reason/:id',
        component: HistoryReasonDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.historyReason.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const historyReasonPopupRoute: Routes = [
    {
        path: 'history-reason-new',
        component: HistoryReasonPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.historyReason.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'history-reason/:id/edit',
        component: HistoryReasonPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.historyReason.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'history-reason/:id/delete',
        component: HistoryReasonDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.historyReason.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
