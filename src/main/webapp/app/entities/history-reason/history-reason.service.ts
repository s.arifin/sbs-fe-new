import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { HistoryReason } from './history-reason.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class HistoryReasonService {

    private resourceUrl = process.env.API_C_URL + '/api/history-reasons';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/history-reasons';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(historyReason: HistoryReason): Observable<HistoryReason> {
        const copy = this.convert(historyReason);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(historyReason: HistoryReason): Observable<HistoryReason> {
        const copy = this.convert(historyReason);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<HistoryReason> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtcreated = this.dateUtils
            .convertDateTimeFromServer(entity.dtcreated);
    }

    private convert(historyReason: HistoryReason): HistoryReason {
        const copy: HistoryReason = Object.assign({}, historyReason);

        copy.dtcreated = this.dateUtils.toDate(historyReason.dtcreated);
        return copy;
    }

    querypp(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/pp', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querypo(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/po', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getByForeign(idforeign?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/by-foreign/' + idforeign)
            .map((res: Response) => this.convertResponse(res));
    }
}
