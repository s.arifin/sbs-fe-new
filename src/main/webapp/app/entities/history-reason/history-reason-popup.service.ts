import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { HistoryReason } from './history-reason.model';
import { HistoryReasonService } from './history-reason.service';

@Injectable()
export class HistoryReasonPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private historyReasonService: HistoryReasonService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.historyReasonService.find(id).subscribe((historyReason) => {
                    historyReason.dtcreated = this.datePipe
                        .transform(historyReason.dtcreated, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.historyReasonModalRef(component, historyReason);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.historyReasonModalRef(component, new HistoryReason());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    historyReasonModalRef(component: Component, historyReason: HistoryReason): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.historyReason = historyReason;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
