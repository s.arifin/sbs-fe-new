import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { HistoryReason } from './history-reason.model';
import { HistoryReasonPopupService } from './history-reason-popup.service';
import { HistoryReasonService } from './history-reason.service';

@Component({
    selector: 'jhi-history-reason-dialog',
    templateUrl: './history-reason-dialog.component.html'
})
export class HistoryReasonDialogComponent implements OnInit {

    historyReason: HistoryReason;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private historyReasonService: HistoryReasonService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.historyReason.id !== undefined) {
            this.subscribeToSaveResponse(
                this.historyReasonService.update(this.historyReason));
        } else {
            this.subscribeToSaveResponse(
                this.historyReasonService.create(this.historyReason));
        }
    }

    private subscribeToSaveResponse(result: Observable<HistoryReason>) {
        result.subscribe((res: HistoryReason) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: HistoryReason) {
        this.eventManager.broadcast({ name: 'historyReasonListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-history-reason-popup',
    template: ''
})
export class HistoryReasonPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private historyReasonPopupService: HistoryReasonPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.historyReasonPopupService
                    .open(HistoryReasonDialogComponent as Component, params['id']);
            } else {
                this.historyReasonPopupService
                    .open(HistoryReasonDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
