import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    HistoryReasonService,
    HistoryReasonPopupService,
    HistoryReasonComponent,
    HistoryReasonDetailComponent,
    HistoryReasonDialogComponent,
    HistoryReasonPopupComponent,
    HistoryReasonDeletePopupComponent,
    HistoryReasonDeleteDialogComponent,
    historyReasonRoute,
    historyReasonPopupRoute,
    HistoryReasonResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...historyReasonRoute,
    ...historyReasonPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        HistoryReasonComponent,
        HistoryReasonDetailComponent,
        HistoryReasonDialogComponent,
        HistoryReasonDeleteDialogComponent,
        HistoryReasonPopupComponent,
        HistoryReasonDeletePopupComponent,
    ],
    entryComponents: [
        HistoryReasonComponent,
        HistoryReasonDialogComponent,
        HistoryReasonPopupComponent,
        HistoryReasonDeleteDialogComponent,
        HistoryReasonDeletePopupComponent,
    ],
    providers: [
        HistoryReasonService,
        HistoryReasonPopupService,
        HistoryReasonResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmHistoryReasonModule {}
