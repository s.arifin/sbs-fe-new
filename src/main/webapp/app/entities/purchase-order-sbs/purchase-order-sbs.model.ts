import { BaseEntity } from './../../shared';

export class PurchaseOrderSbs implements BaseEntity {
    constructor(
        public id?: number,
        public idPurchaseOrder?: any,
        public noPurchaseOrder?: string,
        public dtOrder?: any,
        public divition?: string,
        public sifat?: string,
        public needs?: string,
        public appPembelian?: string,
        public dtAppPembelian?: any,
        public appPabrik?: string,
        public dtAppPabrik?: any,
        public appDept?: string,
        public dtAppDept?: any,
        public createBy?: string,
        public dtCreateBy?: any,
    ) {
    }
}
