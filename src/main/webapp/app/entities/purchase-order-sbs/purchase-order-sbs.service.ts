import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PurchaseOrderSbs } from './purchase-order-sbs.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PurchaseOrderSbsService {

    private resourceUrl = SERVER_API_URL + 'api/purchase-order-sbs';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/purchase-order-sbs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(purchaseOrderSbs: PurchaseOrderSbs): Observable<PurchaseOrderSbs> {
        const copy = this.convert(purchaseOrderSbs);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(purchaseOrderSbs: PurchaseOrderSbs): Observable<PurchaseOrderSbs> {
        const copy = this.convert(purchaseOrderSbs);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<PurchaseOrderSbs> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtOrder = this.dateUtils
            .convertLocalDateFromServer(entity.dtOrder);
        entity.dtAppPembelian = this.dateUtils
            .convertLocalDateFromServer(entity.dtAppPembelian);
        entity.dtAppPabrik = this.dateUtils
            .convertLocalDateFromServer(entity.dtAppPabrik);
        entity.dtAppDept = this.dateUtils
            .convertLocalDateFromServer(entity.dtAppDept);
        entity.dtCreateBy = this.dateUtils
            .convertLocalDateFromServer(entity.dtCreateBy);
    }

    private convert(purchaseOrderSbs: PurchaseOrderSbs): PurchaseOrderSbs {
        const copy: PurchaseOrderSbs = Object.assign({}, purchaseOrderSbs);
        copy.dtOrder = this.dateUtils
            .convertLocalDateToServer(purchaseOrderSbs.dtOrder);
        copy.dtAppPembelian = this.dateUtils
            .convertLocalDateToServer(purchaseOrderSbs.dtAppPembelian);
        copy.dtAppPabrik = this.dateUtils
            .convertLocalDateToServer(purchaseOrderSbs.dtAppPabrik);
        copy.dtAppDept = this.dateUtils
            .convertLocalDateToServer(purchaseOrderSbs.dtAppDept);
        copy.dtCreateBy = this.dateUtils
            .convertLocalDateToServer(purchaseOrderSbs.dtCreateBy);
        return copy;
    }
}
