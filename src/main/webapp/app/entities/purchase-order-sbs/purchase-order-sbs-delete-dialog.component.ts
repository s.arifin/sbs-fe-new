import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PurchaseOrderSbs } from './purchase-order-sbs.model';
import { PurchaseOrderSbsPopupService } from './purchase-order-sbs-popup.service';
import { PurchaseOrderSbsService } from './purchase-order-sbs.service';

@Component({
    selector: 'jhi-purchase-order-sbs-delete-dialog',
    templateUrl: './purchase-order-sbs-delete-dialog.component.html'
})
export class PurchaseOrderSbsDeleteDialogComponent {

    purchaseOrderSbs: PurchaseOrderSbs;

    constructor(
        private purchaseOrderSbsService: PurchaseOrderSbsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.purchaseOrderSbsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'purchaseOrderSbsListModification',
                content: 'Deleted an purchaseOrderSbs'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-purchase-order-sbs-delete-popup',
    template: ''
})
export class PurchaseOrderSbsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private purchaseOrderSbsPopupService: PurchaseOrderSbsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.purchaseOrderSbsPopupService
                .open(PurchaseOrderSbsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
