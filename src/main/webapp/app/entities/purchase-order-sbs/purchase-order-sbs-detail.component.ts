import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PurchaseOrderSbs } from './purchase-order-sbs.model';
import { PurchaseOrderSbsService } from './purchase-order-sbs.service';

@Component({
    selector: 'jhi-purchase-order-sbs-detail',
    templateUrl: './purchase-order-sbs-detail.component.html'
})
export class PurchaseOrderSbsDetailComponent implements OnInit, OnDestroy {

    purchaseOrderSbs: PurchaseOrderSbs;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private purchaseOrderSbsService: PurchaseOrderSbsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPurchaseOrderSbs();
    }

    load(id) {
        this.purchaseOrderSbsService.find(id).subscribe((purchaseOrderSbs) => {
            this.purchaseOrderSbs = purchaseOrderSbs;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPurchaseOrderSbs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'purchaseOrderSbsListModification',
            (response) => this.load(this.purchaseOrderSbs.id)
        );
    }
}
