import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PurchaseOrderSbsComponent } from './purchase-order-sbs.component';
import { PurchaseOrderSbsDetailComponent } from './purchase-order-sbs-detail.component';
import { PurchaseOrderSbsPopupComponent } from './purchase-order-sbs-dialog.component';
import { PurchaseOrderSbsDeletePopupComponent } from './purchase-order-sbs-delete-dialog.component';

export const purchaseOrderSbsRoute: Routes = [
    {
        path: 'purchase-order-sbs',
        component: PurchaseOrderSbsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrderSbs.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'purchase-order-sbs/:id',
        component: PurchaseOrderSbsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrderSbs.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const purchaseOrderSbsPopupRoute: Routes = [
    {
        path: 'purchase-order-sbs-new',
        component: PurchaseOrderSbsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrderSbs.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'purchase-order-sbs/:id/edit',
        component: PurchaseOrderSbsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrderSbs.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'purchase-order-sbs/:id/delete',
        component: PurchaseOrderSbsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrderSbs.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
