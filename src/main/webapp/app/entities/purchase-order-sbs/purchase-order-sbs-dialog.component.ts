import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrderSbs } from './purchase-order-sbs.model';
import { PurchaseOrderSbsPopupService } from './purchase-order-sbs-popup.service';
import { PurchaseOrderSbsService } from './purchase-order-sbs.service';

@Component({
    selector: 'jhi-purchase-order-sbs-dialog',
    templateUrl: './purchase-order-sbs-dialog.component.html'
})
export class PurchaseOrderSbsDialogComponent implements OnInit {

    purchaseOrderSbs: PurchaseOrderSbs;
    isSaving: boolean;
    dtOrderDp: any;
    dtAppPembelianDp: any;
    dtAppPabrikDp: any;
    dtAppDeptDp: any;
    dtCreateByDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private purchaseOrderSbsService: PurchaseOrderSbsService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.purchaseOrderSbs.id !== undefined) {
            this.subscribeToSaveResponse(
                this.purchaseOrderSbsService.update(this.purchaseOrderSbs));
        } else {
            this.subscribeToSaveResponse(
                this.purchaseOrderSbsService.create(this.purchaseOrderSbs));
        }
    }

    private subscribeToSaveResponse(result: Observable<PurchaseOrderSbs>) {
        result.subscribe((res: PurchaseOrderSbs) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PurchaseOrderSbs) {
        this.eventManager.broadcast({ name: 'purchaseOrderSbsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-purchase-order-sbs-popup',
    template: ''
})
export class PurchaseOrderSbsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private purchaseOrderSbsPopupService: PurchaseOrderSbsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.purchaseOrderSbsPopupService
                    .open(PurchaseOrderSbsDialogComponent as Component, params['id']);
            } else {
                this.purchaseOrderSbsPopupService
                    .open(PurchaseOrderSbsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
