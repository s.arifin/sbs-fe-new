import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PurchaseOrderSbsService,
    PurchaseOrderSbsPopupService,
    PurchaseOrderSbsComponent,
    PurchaseOrderSbsDetailComponent,
    PurchaseOrderSbsDialogComponent,
    PurchaseOrderSbsPopupComponent,
    PurchaseOrderSbsDeletePopupComponent,
    PurchaseOrderSbsDeleteDialogComponent,
    purchaseOrderSbsRoute,
    purchaseOrderSbsPopupRoute,
} from './';

const ENTITY_STATES = [
    ...purchaseOrderSbsRoute,
    ...purchaseOrderSbsPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PurchaseOrderSbsComponent,
        PurchaseOrderSbsDetailComponent,
        PurchaseOrderSbsDialogComponent,
        PurchaseOrderSbsDeleteDialogComponent,
        PurchaseOrderSbsPopupComponent,
        PurchaseOrderSbsDeletePopupComponent,
    ],
    entryComponents: [
        PurchaseOrderSbsComponent,
        PurchaseOrderSbsDialogComponent,
        PurchaseOrderSbsPopupComponent,
        PurchaseOrderSbsDeleteDialogComponent,
        PurchaseOrderSbsDeletePopupComponent,
    ],
    providers: [
        PurchaseOrderSbsService,
        PurchaseOrderSbsPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPurchaseOrderSbsModule {}
