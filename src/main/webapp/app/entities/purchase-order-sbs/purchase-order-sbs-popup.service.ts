import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PurchaseOrderSbs } from './purchase-order-sbs.model';
import { PurchaseOrderSbsService } from './purchase-order-sbs.service';

@Injectable()
export class PurchaseOrderSbsPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private purchaseOrderSbsService: PurchaseOrderSbsService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.purchaseOrderSbsService.find(id).subscribe((purchaseOrderSbs) => {
                    if (purchaseOrderSbs.dtOrder) {
                        purchaseOrderSbs.dtOrder = {
                            year: purchaseOrderSbs.dtOrder.getFullYear(),
                            month: purchaseOrderSbs.dtOrder.getMonth() + 1,
                            day: purchaseOrderSbs.dtOrder.getDate()
                        };
                    }
                    if (purchaseOrderSbs.dtAppPembelian) {
                        purchaseOrderSbs.dtAppPembelian = {
                            year: purchaseOrderSbs.dtAppPembelian.getFullYear(),
                            month: purchaseOrderSbs.dtAppPembelian.getMonth() + 1,
                            day: purchaseOrderSbs.dtAppPembelian.getDate()
                        };
                    }
                    if (purchaseOrderSbs.dtAppPabrik) {
                        purchaseOrderSbs.dtAppPabrik = {
                            year: purchaseOrderSbs.dtAppPabrik.getFullYear(),
                            month: purchaseOrderSbs.dtAppPabrik.getMonth() + 1,
                            day: purchaseOrderSbs.dtAppPabrik.getDate()
                        };
                    }
                    if (purchaseOrderSbs.dtAppDept) {
                        purchaseOrderSbs.dtAppDept = {
                            year: purchaseOrderSbs.dtAppDept.getFullYear(),
                            month: purchaseOrderSbs.dtAppDept.getMonth() + 1,
                            day: purchaseOrderSbs.dtAppDept.getDate()
                        };
                    }
                    if (purchaseOrderSbs.dtCreateBy) {
                        purchaseOrderSbs.dtCreateBy = {
                            year: purchaseOrderSbs.dtCreateBy.getFullYear(),
                            month: purchaseOrderSbs.dtCreateBy.getMonth() + 1,
                            day: purchaseOrderSbs.dtCreateBy.getDate()
                        };
                    }
                    this.ngbModalRef = this.purchaseOrderSbsModalRef(component, purchaseOrderSbs);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.purchaseOrderSbsModalRef(component, new PurchaseOrderSbs());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    purchaseOrderSbsModalRef(component: Component, purchaseOrderSbs: PurchaseOrderSbs): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.purchaseOrderSbs = purchaseOrderSbs;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
