import { BaseEntity } from './../../shared';

export class Internal implements BaseEntity {
    constructor(
        public id?: any,
        public idInternal?: string,
        public partyId?: any,
        public partyName?: any,
        public idMPM ?: any,
        public idAHM ?: any,
        public idRoleType?: number,
    ) {
    }
}
