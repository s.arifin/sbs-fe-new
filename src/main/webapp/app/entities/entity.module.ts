import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MpmPurposeTypeModule } from './purpose-type/purpose-type.module';
import { MpmPeriodTypeModule } from './period-type/period-type.module';
import { MpmUomTypeModule } from './uom-type/uom-type.module';
import { MpmUomModule } from './uom/uom.module';
import { MpmRelationTypeModule } from './relation-type/relation-type.module';
import { MpmCalendarTypeModule } from './calendar-type/calendar-type.module';
import { MpmStandardCalendarModule } from './standard-calendar/standard-calendar.module';
import { MpmBaseCalendarModule } from './base-calendar/base-calendar.module';
import { MpmAssociationTypeModule } from './association-type/association-type.module';
import { MpmGeoBoundaryTypeModule } from './geo-boundary-type/geo-boundary-type.module';
import { MpmInternalOrderModule } from './internal-order/internal-order.module';
import { MpmDocumentTypeModule } from './document-type/document-type.module';
import { MpmDocumentsModule } from './documents/documents.module';
import { MpmGeoBoundaryModule } from './geo-boundary/geo-boundary.module';
import { MpmProvinceModule } from './province/province.module';
import { MpmCityModule } from './city/city.module';
import { MpmDistrictModule } from './district/district.module';
import { MpmVillageModule } from './village/village.module';
import { MpmPostalAddressModule } from './postal-address/postal-address.module';
import { MpmUomConversionModule } from './uom-conversion/uom-conversion.module';
import { MpmUnitDocumentMessageModule } from './unit-document-message/unit-document-message.module';
import { MpmReportModule } from './report/report.module';
import { MpmGeneralUploadModule } from './general-upload/general-upload.module';
import { MpmJenisResikoModule } from './jenis-resiko/jenis-resiko.module';
import { MpmPurchaseOrderSbsModule } from './purchase-order-sbs/purchase-order-sbs.module';
// import { MpmJobModule } from './job/job.module';
// import { MpmJobOrderModule } from './job-order/job-order.module';
import { MpmJobordersModule } from './joborders/joborders.module';
import { MpmBudgetingModule } from './budgeting/budgeting.module';
import { MpmPurchasingModule } from './purchasing/purchasing.module';
import { MpmPurchasingItemModule } from './purchasing-item/purchasing-item.module';
import { MpmPicRoleModule } from './pic-role/pic-role.module';
import { MpmOrderItemModule } from './order-item/order-item.module';
import { MpmMasterSupplierModule } from './master-supplier/master-supplier.module';
import { MpmMasterBahanModule } from './master_bahan/master-bahan.module';
import { MpmOverTimesModule } from './overtime/overtime.module';
import { MpmAnnualLeaveModule } from './annual-leave/annual-leave.module';
import { MpmTravelRequestModule } from './atravel-reuest/travel-request.module';
import { MpmHeaderBBMModule } from './header-bbm/header-bbm.module';
import { MpmDetailBBMModule } from './detail-bbm/detail-bbm.module';
import { MpmDetailBahanModule } from './detail-bahan/detail-bahan.module';
import { MpmReturModule } from './retur/retur.module';
import { MpmReturItemModule } from './retur-item/retur-item.module';
import { MpmUnionBBKModule } from './union-bbk/union-bbk.module';
import { MpmDashboardModule } from './dashboard';
import { MpmPaymentModule } from './payment/payment.module';
import { MpmPaymentItemModule } from './payment-item/payment-item.module';
import { MpmInvoiceModule } from './invoice/invoice.module';
import { MpmInvoiceItemModule } from './invoice-item/invoice-item.module';
import { MpmHeaderKlaimModule } from './header-klaim/header-klaim.module';
import { MpmDetailKlaimModule } from './detail-klaim/detail-klaim.module';
import { MpmOverTimeItemModule } from './overtime-item/overtime-item.module';
import { MpmMasterDistributorModule } from './master-distributor/master-distributor.module';
import { MpmCalendarDayOffModule } from './calendar-day-off/calendar-day-off.module';
import { MpmHistoryReasonModule } from './history-reason/history-reason.module';
import { MpmMasterBankModule } from './master-bank/master-bank.module';
import { MpmTandaTerimaModule } from './tanda-terima/tanda-terima.module';
import { MpmDetailTandaTerimaModule } from './detail-tanda-terima/detail-tanda-terima.module';
import { MpmTandaTerimaDocumentModule } from './tanda-terima-document/tanda-terima-document.module';
import { MpmHargaMasterBahanModule } from './harga-master-bahan/harga-master-bahan.module';
import { MpmPaymentHistoryModule } from './payment-history/payment-history.module';
import { MpmPoSKGModule } from './po-skg/po-skg.module';
import { MpmReturDocumentModule } from './retur-document/retur-document.module';
import { MpmTandaTerimaSKGModule } from './tanda-terima-skg/tanda-terima-skg.module';
import { MpmDetailTandaTerimaSKGModule } from './detail-tanda-terima-skg/detail-tanda-terima-skg.module';
import { MpmMasterAgenModule } from './master_agen/master-agen.module';
import { MpmBukuTelponModule } from './buku-telpon/buku-telpon.module';
import { MpmApprovalModule } from './approval/approval.module';
import { MpmRofoModule } from './rofo/rofo.module';
import { MpmMasterProjectModule } from './master-project/master-project.module';
import { MpmLoginSisModule } from './login-sis';
import { MpmMonitoringMobileModule } from './monitoring-mobil';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        MpmReportModule,
        MpmPurposeTypeModule,
        MpmPeriodTypeModule,
        MpmUomTypeModule,
        MpmUomModule,
        // MpmVendorModule,
        MpmRelationTypeModule,
        // MpmBranchModule,
        // MpmPurchaseOrderModule,
        MpmCalendarTypeModule,
        MpmStandardCalendarModule,
        MpmBaseCalendarModule,
        MpmAssociationTypeModule,
        MpmGeoBoundaryTypeModule,
        MpmInternalOrderModule,
        MpmDocumentTypeModule,
        MpmDocumentsModule,
        MpmGeoBoundaryModule,
        MpmProvinceModule,
        MpmCityModule,
        MpmDistrictModule,
        MpmVillageModule,
        // MpmBillingDisbursementModule,
        MpmPostalAddressModule,
        MpmUomConversionModule,
        MpmUnitDocumentMessageModule,
        MpmGeneralUploadModule,
        MpmJenisResikoModule,
        MpmPurchaseOrderSbsModule,
        MpmJobordersModule,
        MpmBudgetingModule,
        MpmPurchasingModule,
        MpmPurchasingItemModule,
        MpmPicRoleModule,
        MpmMasterBahanModule,
        MpmMasterProjectModule,
        MpmOrderItemModule,
        MpmMasterSupplierModule,
        MpmHeaderBBMModule,
        MpmDetailBBMModule,
        MpmDetailBahanModule,
        MpmReturModule,
        MpmReturItemModule,
        MpmUnionBBKModule,
        MpmDashboardModule,
        MpmPaymentModule,
        MpmPaymentItemModule,
        MpmInvoiceModule,
        MpmInvoiceItemModule,
        MpmOverTimesModule,
        MpmAnnualLeaveModule,
        MpmTravelRequestModule,
        MpmHeaderKlaimModule,
        MpmDetailKlaimModule,
        MpmOverTimeItemModule,
        MpmMasterDistributorModule,
        MpmCalendarDayOffModule,
        MpmHistoryReasonModule,
        MpmMasterBankModule,
        MpmTandaTerimaModule,
        MpmDetailTandaTerimaModule,
        MpmTandaTerimaDocumentModule,
        MpmHargaMasterBahanModule,
        MpmPaymentHistoryModule,
        MpmPoSKGModule,
        MpmReturDocumentModule,
        MpmTandaTerimaSKGModule,
        MpmDetailTandaTerimaSKGModule,
        MpmMasterAgenModule,
        MpmBukuTelponModule,
        MpmApprovalModule,
        MpmRofoModule,
        MpmLoginSisModule,
        MpmMonitoringMobileModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmEntityModule {}
