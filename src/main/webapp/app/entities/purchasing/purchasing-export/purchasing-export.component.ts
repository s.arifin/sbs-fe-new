import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Purchasing, Kabag, PurchasingExport } from '../purchasing.model';
import { PurchasingService } from '../purchasing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { PurchasingItemService } from '../../purchasing-item';
import { LoadingService } from '../../../layouts';
import { MasterSupplierService, MasterSupplier } from '../../master-supplier';
import { PurchasingFilter } from '../purchasing-filter.model';

@Component({
    selector: 'jhi-purchasing-export',
    templateUrl: './purchasing-export.component.html'
})
export class PurchasingExportComponent implements OnInit, OnDestroy {

    currentAccount: any;
    purchasings: PurchasingExport[];
    selectedPO: PurchasingExport[];
    purchasing: Purchasing;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    newModal: boolean;
    kabag: Array<Object> = new Array<Object>();
    selectedKabag: string;
    idPrint: any;
    sortF: any;
    pdfSrc: any;
    isview: boolean;
    isEdit: boolean;
    noteCLosePO: string;
    newModalNote: boolean;
    modalPilihCeteakn: boolean;
    selected: Purchasing[];
    pics: Array<object> = [
        { label: 'all', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    // field filter
    dtfrom: Date;
    dtthru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    newSuplier: MasterSupplier;
    kdSPL: string;
    isFilter: boolean;
    selectedPIC: string;
    dtFilter: PurchasingFilter;
    // field filter
    loginName: string;
    pilihBahasa: string;
    isTnpaHrg: boolean;
    constructor(
        private purchasingService: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private purchasingItemService: PurchasingItemService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Purchasing>();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdSPL = '';
        this.isFilter = false;
        this.selectedPIC = '';
        this.dtFilter = new PurchasingFilter();
        this.loginName = '';
        this.pilihBahasa = '';
        this.isTnpaHrg = false;
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.purchasingService.queryExport().subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        // this.router.navigate(['/purchasing', {
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/purchasing', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    ngOnInit() {
        this.newModalNote = false;
        this.isview = false;
        this.kabag = [
            { label: 'Pilih Kabag', value: null },
            { label: 'Evina HU', value: 'Evina HU' },
            { label: 'Jettir G', value: 'Jettir G' },
            { label: 'Selvyati', value: 'Selvyati' }
        ];
        this.idPrint = '';
        this.newModal = false;
        this.selectedKabag = '';
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            this.loginName = account.login;
            console.log('Inin data account = ', account);
            this.isCanEdit(account);
        });
        this.registerChangeInPurchasings();
        this.purchasing = new Purchasing();
        this.modalPilihCeteakn = false;
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Purchasing) {
        return item.id;
    }
    registerChangeInPurchasings() {
        this.eventSubscriber = this.eventManager.subscribe('purchasingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.purchasings = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    public cancelPO(data: Purchasing) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Cancel PO ' + data.nopurchasing,
            accept: () => {
                console.log('data PO yang akan di cancel == ', data);
                this.purchasingService.changeStatus(data, 13)
                    .subscribe(
                        (res) => this.loadAll()
                    );
            }
        });
    }

    public printPO(purchasing: Purchasing) {
        this.idPrint = purchasing.idpurchasing;
        this.purchasing = purchasing;
        this.modalPilihCeteakn = true;
        // this.pdfSrc = '';
        // const filter_data = 'idpurchaseorder:' + purchasing.idpurchasing;
        // this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
        //     subscribe((response: any) => {
        //         const reader = new FileReader();
        //         reader.readAsDataURL(response.blob());
        //         this.isview = true;
        //         // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
        //         reader.onloadend = (e) => {
        //             this.pdfSrc = reader.result;
        //         }
        //     });
    }
    public pilihPrintPO(bahasa: any) {
        this.pilihBahasa = bahasa
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        const filter_data = 'idpurchaseorder:' + this.purchasing.idpurchasing;
        if (bahasa === 'indonesia') {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                });
            this.modalPilihCeteakn = false;
        }
        if (bahasa === 'inggris') {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggris/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                });
            this.modalPilihCeteakn = false;
        }
    }

    public editPO(purchasing: Purchasing) {
        if (purchasing.printcount === 0 && purchasing.isedit === 0) {
            this.router.navigate(['../purchasing-edit/' + purchasing.idpurchasing]);
        } else if (purchasing.printcount === 1 && purchasing.isedit === 1) {
            alert('Silahkan Meminta Otorisasi Atasan untuk edit');
        } else if (purchasing.printcount === 1 && purchasing.isedit === 0 && purchasing.revision >= 0 && purchasing.revision_internal >= 0) {
            this.router.navigate(['../purchasing-edit/' + purchasing.idpurchasing]);
        } else if (purchasing.revision === 3 || purchasing.revision_internal === 3) {
            alert('Silahkan Meminta Otorisasi Atasan untuk edit');
        } else {
            alert('Silahkan Meminta Otorisasi Atasan untuk edit');
        }
    }
    public printFromModal() {
        this.pdfSrc = '';
        this.newModal = false;
        const filter_data = 'idpurchaseorder:' + this.idPrint + '|kabag:' + this.selectedKabag;
        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
            subscribe((response: any) => {
                const reader = new FileReader();
                reader.readAsDataURL(response.blob());
                this.isview = true;
                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                reader.onloadend = (e) => {
                    this.pdfSrc = reader.result;
                }
            });
    }

    changeSort(event) {
        if (!event.order) {
            this.sortF = 'year';
        } else {
            this.sortF = event.field;
        }
    }

    backMainPage() {
        this.isview = false;
    }

    print() {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Cetak Dokumen ? \r\n  Setelah Print Hanya Bisa Direvisi Sesuai Otoritas.!!',
            accept: () => {
                this.purchasing.printcount = 1;
                this.purchasing.isedit = 1;
                this.purchasingService.update(this.purchasing).subscribe((res) => console.log('update sukses'));
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                const win = window.open(this.pdfSrc);
                win.document.write('<iframe id="printf" src="' + this.pdfSrc + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
            }
        });
    }

    printtanpaharga() {
        this.isTnpaHrg = true;
        const bahasa = this.pilihBahasa;
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        const filter_data = 'idpurchaseorder:' + this.purchasing.idpurchasing;
        if (bahasa === 'indonesia') {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHarga/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                        const win = window.open(this.pdfSrc);
                        win.document.write('<iframe id="printf" src="' + reader.result + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
                    }
                    this.loadingService.loadingStop();
                });
            this.modalPilihCeteakn = false;
        }
        if (bahasa === 'inggris') {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHarga/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                        const win = window.open(this.pdfSrc);
                        win.document.write('<iframe id="printf" src="' + reader.result + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
                    }
                    this.loadingService.loadingStop();
                });
            this.modalPilihCeteakn = false;
        }
    }

    downloadExcel() {
        const filter_data = 'idpurchaseorder:' + this.idPrint;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PrintPOPusat/xlsx', { filterData: filter_data })
    }

    isCanEdit(account: Account) {
        this.isEdit = false;
        if ((account.authorities.indexOf('ROLE_MGR_PRC') > -1) && this.purchasing.printcount === 1) {
            this.isEdit = true;
        }
        if (!(account.authorities.indexOf('ROLE_MGR_PRC') > -1) && this.purchasing.printcount !== 1) {
            this.isEdit = true;
        }
    }

    closePO(purchasing: Purchasing) {
        this.purchasing = new Purchasing();
        this.purchasing = purchasing;
        this.newModalNote = true;
    }
    modalClosePO() {
        this.purchasing.notes2 = this.noteCLosePO;
        this.purchasingService.closePO(this.purchasing)
            .subscribe(
                (res) => {
                    console.log('update sukses');
                    this.newModalNote = false;
                    this.loadAll();
                }
            )
    }
    approveEdit() {
        this.purchasingService.approveEditPO(this.selected).subscribe(
            (res) => {
                this.loadAll();
                this.selected = new Array<Purchasing>();
                alert('Sukses');
            }
        )
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }

    filter() {
        this.isFilter = true;
        this.loadAll()
    }

    reset() {
        this.isFilter = false;
        this.currentSearch = ''
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.loadAll()
    }

    public siapBBM(data: Purchasing) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
            accept: () => {
                console.log('data PO yang akan di cancel == ', data);
                this.purchasingService.changeStatus(data, 26)
                    .subscribe(
                        (res) => this.loadAll()
                    );
            }
        });
    }

    exportToCsv(filename: string, rows: PurchasingExport[]) {
        if (!rows || !rows.length) {
            return;
        }
        const separator = '|';
        const keys = Object.keys(rows[0]);
        const csvContent =
            keys.join(separator) +
            '\n' +
            rows.map((row) => {
                return keys.map((k) => {
                    let cell = row[k] === null || row[k] === undefined ? '' : row[k];
                    cell = cell instanceof Date
                        ? cell.toLocaleString()
                        : cell.toString().replace(/"/g, '""');
                    if (cell.search(/("|,|\n)/g) >= 0) {
                        cell = `${cell}`;
                    }
                    return cell;
                }).join(separator);
            }).join('\n');

        const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');
            if (link.download !== undefined) {
                // Browsers that support HTML5 download attribute
                const url = URL.createObjectURL(blob);
                link.setAttribute('href', url);
                link.setAttribute('download', filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
}
