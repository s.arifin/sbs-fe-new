import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDateUtils } from 'ng-jhipster';

import { Purchasing, Valuta } from './purchasing.model';
import { PurchasingService } from './purchasing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, Account, UserService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PurchaseOrderService, PurchaseOrder, MasterBarang, MasterBahan } from '../purchase-order';
import { PurchaseOrderItemService, PurchaseOrderItem } from '../purchase-order-item';
import { PurchasingItem, MappingUserPO, UOMPO, MasterDiv } from '../purchasing-item';
import { ConfirmationService } from 'primeng/components/common/api';
import { Uom } from '../uom';
import { LoadingService } from '../../layouts';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-purchasing-new-inject',
    templateUrl: './purchasing-new-inject.component.html'
})
export class PurchasingNewInjectComponent implements OnInit, OnDestroy {

    currentAccount: any;
    purchasings: Purchasing[];
    purchasing: Purchasing;
    purchaseOrder: PurchaseOrder;
    purchaseOrderItems: PurchaseOrderItem[];
    purchasingItems: PurchasingItem[];
    purchasingItem: PurchasingItem;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    eventSubscriberPPLOV: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter: PurchasingItem[];
    newSuplier: MasterSupplier;
    newValuta: Valuta;
    newOtorisasi: any;
    newOtorisasis: any[];
    newSupliers: any[];
    filteredSuppliers: any[];
    filteredValuta: any[];
    filteredOtorisasi: any[];
    isSelectSupplier: boolean;
    isSelectValuta: boolean;
    isSelectOtorisasi: boolean;
    inputSupplyerName: string;
    inputValuta: string;
    inputOtorisasi: string;
    splAddress: string;
    mappingUserPo: MappingUserPO;
    jumlah: number;
    checkPPN: boolean;
    tempTotalPrice: number;
    uomPO: UOMPO[];
    isDiscPersen: boolean;
    newOto: string;
    qtypoTemp: number;
    minimumDate: Date = new Date();
    filteredCodeSuppliers: any[];
    checkOngkirPPN: boolean;
    masterDivs: MasterDiv[];
    masterDiv: MasterDiv;
    isretensi: boolean;
    isDiscPersenItem: boolean;
    content: any;
    contenttype: any;
    internals: Array<object> = [
        { label: 'Teknik', value: 'TEKNIK' },
        { label: 'Non Teknik', value: 'NONTEKNIK' },
    ];
    internal: string;
    kdbarang: string;
    nmbarang: string;
    uombarang: string;
    filteredBarang: any[];
    newBarang: any;
    newBarangs: any[];
    qtybarang: number;
    constructor(
        private purchasingService: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private purchaseOrderService: PurchaseOrderService,
        private purchaseOrderItemService: PurchaseOrderItemService,
        protected confirmationService: ConfirmationService,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
        private dateUtils: JhiDateUtils,
        private masterSupplierService: MasterSupplierService,
        private reportUtilService: ReportUtilService,
        private userService: UserService,
    ) {
        this.purchaseOrder = new PurchaseOrder();
        this.purchasingItems = new Array<PurchasingItem>();
        this.uomPO = new Array<UOMPO>();
        this.purchasing = new Purchasing();
        this.newOto = '';
        this.uomPO = new Array<UOMPO>();
        this.purchasing = new Purchasing();
        this.purchasing.isppn = 0;
        this.isDiscPersen = true;
        this.jumlah = 0;
        this.checkPPN = false;
        this.checkOngkirPPN = false;
        this.tempTotalPrice = 0;
        this.purchaseOrderItems = new Array<PurchaseOrderItem>();
        this.purchasingItem = new PurchasingItem();
        this.purchasingItems = new Array<PurchasingItem>();
        this.newSuplier = new MasterSupplier();
        this.newValuta = new Valuta();
        this.isretensi = false;
        this.isDiscPersenItem = false;
        this.internal = 'TEKNIK';
        this.kdbarang = '';
        this.nmbarang = '';
        this.qtybarang = 0;
        this.uombarang = '';
    }

    loadAll() {
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/purchasing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchasing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.purchasing.disc = 0;
        this.purchasing.tolerancenumsend = 0;
        this.loadAll();
        this.checkDisc();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInPurchasings();
        this.registerFromPPLOV();
        this.userService.queryAllDiv()
            .subscribe(
                (res) => this.masterDivs = res.json
            );
    }

    ngOnDestroy() {
        this.eventSubscriberPPLOV.unsubscribe();
    }

    trackId(index: number, item: Purchasing) {
        return item.id;
    }

    registerFromPPLOV() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('purchaseOrderLovModification', () => this.registerChoosPP());
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.purchasings = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    checkIsUsePO(nopo: string): Promise<void> {
        return new Promise<void>(
            (resolvePO, reject) => {
                this.purchasingService.checkIsUsePO({
                    query: 'noPO:' + nopo
                }).subscribe(
                    (res: MappingUserPO) => {
                        this.mappingUserPo = res;
                        resolvePO();
                    }
                )
            }
        )
    }

    registerChoosPP() {
        this.purchaseOrderService.values.subscribe(
            (respARR: PurchaseOrder[]) => {
                console.log('cek data terima == ', respARR);
                respARR.forEach(
                    (e: PurchaseOrder) => {
                        this.purchaseOrder = new PurchaseOrder();
                        this.purchaseOrder = e;
                        if (this.newOto === '') {
                            this.newOto = this.purchaseOrder.pic;
                        }
                        if (this.newOto !== '') {
                            if (this.newOto === e.pic) {
                                // this.checkIsUsePO(this.purchaseOrder.noPurchaseOrder).then(
                                //     () => {
                                // if (this.mappingUserPo.idmapuspo === null) {
                                if (this.purchaseOrder.idPurchaseOrder === null || this.purchaseOrder.idPurchaseOrder === undefined) {
                                    this.toasterService.showToaster('error', 'Pilih Nomo PP', 'Pilih Terlebih Dahulu Nomor PP');
                                } else {
                                    // this.purchasingService.LOVqueryFilterBy({
                                    //     query: 'idPurchaseOrder:' + this.purchaseOrder.idPurchaseOrder +
                                    //             '|noprior:' + this.purchaseOrder.noprior +
                                    //             '|idPurOrdIte:' + this.purchaseOrder.item.idPurOrdItem,
                                    //     page: 0,
                                    //     size: 20
                                    // }).subscribe(
                                    //     (res) => {
                                    this.purchaseOrderItems = new Array<PurchaseOrderItem>();
                                    this.purchaseOrderItems.push(this.purchaseOrder.item); // [...this.purchasingItems, this.purchaseOrder.item];
                                    console.log('cek ini data order itm nya yahh => ', this.purchaseOrderItems);
                                    for (let i = 0; i < this.purchaseOrderItems.length; i++) {
                                        const idItems = this.purchaseOrderItems[i].idPurOrdItem;
                                        const idprior = this.purchaseOrder.mapp.idmapprior;
                                        this.filter = new Array<PurchasingItem>();
                                        this.filter = this.purchasingItems.filter(
                                            function dataSama(items) {
                                                return (items.idPurcOrdIte === idItems && items.idprior === idprior)
                                            }
                                        );
                                        if (this.filter.length <= 0) {
                                            this.purchasingItem = new PurchasingItem();
                                            this.purchasingItem.nourut = this.purchasingItems.length + 1;
                                            this.purchasingItem.id = this.purchasingItems.length + 1;
                                            this.purchasingItem.noPP = this.purchaseOrder.noPurchaseOrder;
                                            this.purchasingItem.idPurcOrdIte = this.purchaseOrderItems[i].idPurOrdItem
                                            this.purchasingItem.productcode = this.purchaseOrderItems[i].codeProduk;
                                            this.purchasingItem.productname = this.purchaseOrderItems[i].produkName;
                                            this.purchasingItem.ppqty = this.purchaseOrderItems[i].qty;
                                            this.purchasingItem.bidprice = this.purchaseOrderItems[i].supplierPrice;
                                            this.purchasingItem.uom = this.purchaseOrderItems[i].unit;
                                            this.purchasingItem.qty = this.purchaseOrderItems[i].qtytopo - this.purchaseOrderItems[i].qtybookingpo;
                                            this.purchasingItem.qtyTemp = this.purchasingItem.qty;
                                            this.purchasingItem.price = 0;
                                            this.purchasingItem.dtsent = this.dateUtils.convertDateTimeFromServer(this.purchaseOrderItems[i].dtSent);
                                            this.purchasingItem.sisaPP = this.purchaseOrderItems[i].qty - this.purchaseOrderItems[i].qtybookingpo;
                                            this.purchasingItem.orderItem = this.purchaseOrderItems[i];
                                            this.purchasingItem.divitions = this.purchaseOrder.divition;
                                            this.purchasingItem.divreceipt = this.purchaseOrder.divition;
                                            this.purchasingItem.dtsentnote = '';
                                            this.purchasingItem.dtsentpp = this.dateUtils.toDate(this.purchaseOrderItems[i].dtSent);
                                            this.purchasingItem.idprior = this.purchaseOrder.mapp.idmapprior;
                                            // this.purchasingItem.content = this.purchaseOrder.content;
                                            // this.purchasingItem.contentContentType = this.purchaseOrder.contentContentType;
                                            // this.purchasingItems.push(this.purchasingItem);
                                            this.purchasingItems = [...this.purchasingItems, this.purchasingItem];
                                            console.log('ini dt set kiriman = ', this.purchasingItem.dtsent);
                                        }
                                    }
                                    // })
                                }
                                // } else {
                                //     this.setMessageUSePO(this.purchaseOrder.noPurchaseOrder, this.mappingUserPo.username);
                                // }
                                // }
                                // )
                            } else {
                                this.toasterService.showToaster('Informasi', 'PIC PP', 'PIC PP tidak Boleh Berbeda');
                            }
                        }
                        // this.createMappingUserPO(this.purchaseOrder);
                        this.purchaseOrderService.values.observers = [];
                    }
                )
            });
    }

    // updateRowData(event) {
    //     if (event.data.id !== undefined) {
    //         this.vehicleSalesOrderService.update(event.data)
    //             .subscribe((res: VehicleSalesOrder) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     } else {
    //         this.vehicleSalesOrderService.create(event.data)
    //             .subscribe((res: VehicleSalesOrder) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     }
    // }

    // private onRowDataSaveSuccess(result: VehicleSalesOrder) {
    //     this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    // }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    public deleteListArray(purchasingItem: PurchasingItem) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                this.purchasingItems = this.purchasingItems.filter(function dataSama(items) { return (items.id !== purchasingItem.id) });
                console.log('purchasing items length == ' + this.purchasingItems.length);
                if (this.purchasingItems.length <= 0) {
                    this.purchasing.otorisasi2 = '';
                    this.purchasing.otorisasi = '';
                    this.newOto = '';
                };
                this.purchasingItems = this.setNoUrut(this.purchasingItems);
                this.calculateTotalPrice(purchasingItem);
            }
        });
    }

    public calculateTotalPrice(purchasingItem: PurchasingItem) {
        this.countQTYListArray(purchasingItem).then(
            () => {
                if (purchasingItem.qty === undefined) {
                    purchasingItem.qty = 0;
                }
                if (purchasingItem.price === undefined) {
                    purchasingItem.price = 0;
                }
                if (purchasingItem.disc === undefined) {
                    purchasingItem.disc = 0;
                }
                if (purchasingItem.discpercent === undefined) {
                    purchasingItem.discpercent = 0;
                }
                // purchasingItem.discpercent = parseFloat(((purchasingItem.disc / purchasingItem.price)).toFixed(2));
                // purchasingItem.disc = parseFloat((purchasingItem.price * purchasingItem.disc / 100).toFixed(2));
                if (this.isDiscPersenItem === true) {
                    purchasingItem.disc = parseFloat((purchasingItem.price * (purchasingItem.discpercent / 100)).toFixed(2));
                    console.log('cek data disc => ', purchasingItem.disc);
                }
                if (this.isDiscPersenItem === false) {
                    purchasingItem.discpercent = parseFloat(((purchasingItem.disc / purchasingItem.price) * 100).toFixed(2));
                    console.log('cek data discpercent => ', purchasingItem.discpercent);
                }
                purchasingItem.totalprice = parseFloat((purchasingItem.qty * (purchasingItem.price - purchasingItem.disc)).toFixed(2));
                this.countGrandTotal(this.purchasingItems);
            }
        )
        this.cekdtsent(purchasingItem);
    }

    createPO() {
        if (this.isretensi === true) {
            this.purchasing.isretensi = 1;
        }
        if (this.isretensi === false) {
            this.purchasing.isretensi = 0;
        }
        if (this.purchasing.retensiday === null || this.purchasing.retensiday === undefined) {
            this.purchasing.retensiday = 0;
        }
        if (this.newSuplier.kd_suppnew === undefined || this.newSuplier.kd_suppnew === '' || this.newSuplier.kd_suppnew === null) {
            this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Supplier Tidak boleh kosong');
        }
        this.purchasing.supliercode = this.newSuplier.kd_suppnew;
        this.purchasing.totalprice = this.tempTotalPrice;
        this.purchasing.valuta = this.newValuta.valutacode;
        this.purchasing.price = this.jumlah;
        this.purchasing.otorisasi2 = this.newOto;
        this.purchasing.otorisasi = this.newOto;
        if (this.purchasing.tolerancenumsend === undefined || this.purchasing.tolerancenumsend === null) {
            this.purchasing.tolerancenumsend = 0;
        }
        if (this.purchasing.notes2 === null || this.purchasing.notes2 === undefined) {
            this.purchasing.notes2 = '';
        }
        if (this.purchasing.notes === null || this.purchasing.notes === undefined) {
            this.purchasing.notes = '';
        }
        if (this.purchasing.paynotes === null || this.purchasing.paynotes === undefined) {
            this.purchasing.paynotes = '';
        }
        if (this.purchasing.ppn === null || this.purchasing.ppn === undefined) {
            this.purchasing.ppn = 0;
        }
        if (this.purchasing.tenor === null || this.purchasing.tenor === undefined) {
            this.purchasing.tenor = 0;
        }
        if (this.purchasing.supliercode === null || this.purchasing.supliercode === undefined || this.purchasing.supliercode === '' || this.newSuplier.kd_suppnew === undefined || this.newSuplier.kd_suppnew === '') {
            this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Supplier Tidak boleh kosong');
        } else
            if (this.purchasing.valuta === null || this.purchasing.valuta === undefined || this.purchasing.valuta === '') {
                this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Mata Uang Tidak boleh kosong');
            } else
                if (this.purchasing.sentto === null || this.purchasing.sentto === undefined || this.purchasing.sentto === '') {
                    this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Dikirim Tidak boleh kosong');
                } else if (this.cekItems() === false) {
                    this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Catatan tanggal kirim Tidak boleh kosong');
                } else {
                    // this.purchasing.otorisasi = this.newOtorisasi.login;
                    this.loadingService.loadingStart();
                    this.purchasing.revision = 0;
                    this.purchasingService.createPOInject(this.purchasing, this.purchasingItems)
                        .subscribe(
                            (res) => {
                                this.router.navigate(['/purchasing']);
                                this.loadingService.loadingStop();
                            })
                }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inputSupplyerName = this.newSuplier.nmSupplier;
        this.splAddress = this.newSuplier.alamat1 + ' ' + this.newSuplier.alamat2;
        this.purchasing.tenor = this.newSuplier.tenor;
        this.isSelectSupplier = true;
    }

    public setMessageUSePO(nopo: string, username: string) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Nomor PO = ' + nopo + '<br> Sedang diProses Oleh = ' + username,
            rejectVisible: false,
            accept: () => {
            }
        });
    }

    createMappingUserPO(purchaseOrder: PurchaseOrder) {
        this.purchasingService.createIsUsePO(purchaseOrder)
            .subscribe(
                (res) => console.log('sukses')
            )
    }

    countGrandTotal(purchasingItems: PurchasingItem[]) {
        this.jumlah = 0;
        this.purchasing.totaldisc = 0;
        let totalDis = 0;
        this.tempTotalPrice = 0;
        purchasingItems.forEach(
            (each) => {
                if (each.price === undefined) {
                    each.totalprice = 0;
                }
                this.jumlah += each.totalprice;
            }
        );
        if (this.purchasing.disc !== undefined && this.purchasing.disc !== 0) {
            totalDis = Math.round(this.jumlah * this.purchasing.disc / 100);
            this.purchasing.totaldisc = totalDis;
        } else if (this.checkPPN === true) {
            if (this.jumlah !== undefined && this.jumlah !== 0) {
                this.checkPPN = true;
                this.purchasing.ppn = parseFloat((this.jumlah * 11 / 100).toFixed(2));
                this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc + parseFloat((this.jumlah * 11 / 100).toFixed(2));
                this.tempTotalPrice = this.purchasing.totalprice;
                this.purchasing.nomppn = 11;
            }
        } else {
            if (this.jumlah !== undefined && this.jumlah !== 0) {
                this.checkPPN = false;
                this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc;
                this.tempTotalPrice = this.purchasing.totalprice;
            }
        }
    }

    checkIsPPn() {
        if (this.purchasing.disc === undefined && this.purchasing.disc === null) {
            this.purchasing.disc = 0;
            this.purchasing.totaldisc = 0;
        }
        if (this.purchasing.totaldisc === undefined && this.purchasing.totaldisc === null) {
            this.purchasing.totaldisc = 0;
            this.purchasing.disc = 0;
        }
        if (this.purchasing.disc >= 100) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Discount Tidak Boleh Melebihi / Sama dengan 100%',
                rejectVisible: false,
                accept: () => {
                }
            });
        }
        if (this.isDiscPersen === true) {
            this.purchasing.totaldisc = parseFloat((this.jumlah * this.purchasing.disc / 100).toFixed(2));
        }
        if (this.isDiscPersen === false) {
            // this.purchasing.disc = (Math.round(this.purchasing.totaldisc / this.jumlah * 100));
            this.purchasing.disc = parseFloat(((this.purchasing.totaldisc / this.jumlah) * 100).toFixed(2));
        }
        if (this.checkPPN === true) {
            this.purchasing.isppn = 1;
            this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * 11 / 100).toFixed(2));
            this.purchasing.nomppn = 11;
        }
        if (this.checkPPN === false) {
            this.purchasing.ppn = 0;
        }
        // if (this.checkOngkirPPN === true) {
        //     this.purchasing.totalongkir = this.purchasing.ongkir + (this.purchasing.ongkir * 10) / 100;
        // }
        // if (this.checkOngkirPPN === false) {
        //     if (this.purchasing.ongkir === undefined) {
        //         this.purchasing.ongkir = 0;
        //         this.purchasing.totalongkir = this.purchasing.ongkir;
        //     } else {
        //         this.purchasing.totalongkir = this.purchasing.ongkir;
        //     }
        // }
        // this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc + this.purchasing.ppn + this.purchasing.totalongkir;
        this.purchasing.totalprice = parseFloat((this.jumlah - this.purchasing.totaldisc + this.purchasing.ppn).toFixed(2));
        this.tempTotalPrice = this.purchasing.totalprice;
    }

    countDisc() {
        if (this.purchasing.disc !== undefined && this.purchasing.disc !== 0) {
            this.tempTotalPrice = 0;
            let totalDis = 0;
            totalDis = Math.round(this.jumlah * this.purchasing.disc / 100);
            this.purchasing.totaldisc = totalDis;
            this.tempTotalPrice = this.purchasing.totalprice;
            this.tempTotalPrice = this.purchasing.totalprice - this.purchasing.totaldisc;
        }
    }

    filterValutaSingle(event) {
        const query = event.query;
        this.purchasingService.getAllValuta().then((valuta) => {
            this.filteredValuta = this.filterValuta(query, valuta);
        });
    }

    filterValuta(query, valuta: Valuta[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < valuta.length; i++) {
            const valutas = valuta[i];
            if (valutas.valutacode.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(valutas);
            }
        }
        return filtered;
    }

    public selectValuta(isSelect?: boolean): void {
        this.inputValuta = this.newValuta.valutacode;
        this.isSelectValuta = true;
    }

    validasiToleransi() {
        if (this.purchasing.tolerancenumsend > 10) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Toleransi Kirim Tidak Boleh Lebih Dari 10%',
                rejectVisible: false,
                accept: () => {
                }
            });
            this.purchasing.tolerancenumsend = 0;
        }
    }

    checkDisc() {
        if (this.isDiscPersen === true) {
            this.purchasing.isdisc = 1;
            console.log('ini cek = ', this.purchasing.isdisc);
        }
        if (this.isDiscPersen === false) {
            this.purchasing.isdisc = 0;
            console.log('ini cek = ', this.purchasing.isdisc);
        }
    }

    filterOtorisasiSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredOtorisasi = this.filterOtorisasi(query, newOtorisasi);
        });
    }

    filterOtorisasi(query, newOtorisasi: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newOtorisasi.length; i++) {
            const newOtorisasis = newOtorisasi[i];
            if (newOtorisasis.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newOtorisasis);
            }
        }
        return filtered;
    }

    public selectOtorisasi(isSelect?: boolean): void {
        this.inputOtorisasi = this.newOtorisasi.login;
        this.isSelectOtorisasi = true;
    }

    public addListArray(items: PurchasingItem) {
        this.countQTYListArray(items).then(
            () => {
                console.log('ini items yang dipilih check == ', items);
                console.log('ini qtypo temp check == ', this.qtypoTemp);
                if ((this.qtypoTemp + items.orderItem.qtybookingpo) < items.orderItem.qty) {
                    const pp = new PurchasingItem();
                    pp.id = this.purchasingItems.length + 1
                    pp.orderItem = items.orderItem;
                    pp.ppqty = items.ppqty;
                    pp.uom = items.uom;
                    pp.productcode = items.productcode;
                    pp.productname = items.productname;
                    pp.ppqty = items.ppqty;
                    pp.dtsent = items.dtsent;
                    // pp.qty = items.qty;// pp.ppqty - this.qtypoTemp;
                    pp.sisaPP = items.sisaPP;
                    pp.qty = items.sisaPP - this.qtypoTemp;
                    pp.qtypoTemp = this.qtypoTemp;
                    pp.price = items.price;
                    pp.totalprice = (pp.ppqty * pp.price)
                    pp.bidprice = items.bidprice;
                    // this.purchasingItems.push(pp);
                    pp.divitions = items.divitions;
                    pp.divreceipt = items.divreceipt;
                    pp.dtsentpp = items.dtsentpp;
                    this.purchasingItems = [...this.purchasingItems, pp];
                    // this.purchasingItems.push(pp);
                    this.calculateTotalPrice(pp);
                    this.purchasingItems = this.setNoUrut(this.purchasingItems);
                } else {
                    this.toasterService.showToaster('INFO', 'INFORMASI', 'Item tidak bisa di pecah lagi karena QTY PO sudah sama dengan QTY PP');
                }
            }
        )
    }

    countQTYListArray(items: PurchasingItem): Promise<void> {
        return new Promise<void>(
            (resolveArr, reject) => {
                this.qtypoTemp = 0;
                this.purchasingItems.forEach(
                    (e) => {
                        // if (e.orderItem.idPurOrdItem === items.orderItem.idPurOrdItem) {
                        //     this.qtypoTemp += e.qty;
                        //     console.log('count List array qty = ', this.qtypoTemp);
                        // }
                    }
                );
                resolveArr();
            }
        )
    }

    backMainPage() {
        this.router.navigate(['/purchasing']);
    }

    setNoUrut(pp: PurchasingItem[]): PurchasingItem[] {
        const aa = Array<PurchasingItem>();
        pp.forEach(
            (e: PurchasingItem) => {
                e.nourut = aa.length + 1;
                aa.push(e);
            }
        )
        return aa;
    }

    setFocus(nourut) {
        document.getElementById('field_dtsent_' + nourut).focus();
    }

    openPdf(rowData: PurchasingItem) {
        this.loadingService.loadingStart();
        const filter_data = 'idPruchaseOrder:' + rowData.orderItem.idPurchaseOrder;
        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf', { filterData: filter_data })
            .subscribe((response: any) => {
                const reader = new FileReader();
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                reader.readAsDataURL(response.blob());
                // tslint:disable-next-line: space-before-function-paren
                reader.onloadend = function () {
                    const win = window.open();
                    win.document.write('<iframe src="' + reader.result + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
                }
                this.loadingService.loadingStop();
            },
                (err) => {
                    this.loadingService.loadingStop();
                });
        this.loadingService.loadingStop();
    }

    cekdtsent(rowData: PurchasingItem) {

        // this.purchasingItems.forEach(
        //     (e) => {
        // if (rowData.dtsent.toUTCString() !== rowData.dtsentpp.toUTCString()) {
        //     if (rowData.dtsentnote === '') {
        //         document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
        //         this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + rowData.nourut);
        //         return;
        //     }
        //     if (rowData.dtsentnote.trim() === '') {
        //         document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
        //         this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + rowData.nourut);
        //         return;
        //     }
        //     if (rowData.dtsentnote === undefined) {
        //         document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
        //         this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + rowData.nourut);
        //         return;
        //     }
        // }
        //     }
        // )
    }

    cekItems(): Boolean {
        const hasil = true;
        this.purchasingItems.forEach(
            (e) => {
                // if (e.dtsent.toUTCString() !== e.dtsentpp.toUTCString()) {
                //     if (e.dtsentnote === '') {
                //         document.getElementById('field_dtsentnote_' + e.nourut).focus();
                //         this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut);
                //         hasil = false;
                //         return hasil;
                //     }
                //     if (e.dtsentnote.trim() === '') {
                //         document.getElementById('field_dtsentnote_' + e.nourut).focus();
                //         this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut);
                //         hasil = false;
                //         return hasil;
                //     }
                //     if (e.dtsentnote === undefined) {
                //         document.getElementById('field_dtsentnote_' + e.nourut).focus();
                //         this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut);
                //         hasil = false;
                //         return hasil;
                //     }
                // }
            }
        )
        return hasil;
    }

    checkRetensi() {
        if (this.isretensi === true) {
            this.purchasing.isretensi = 1;
        }
        if (this.isretensi === false) {
            this.purchasing.isretensi = 0;
            this.purchasing.retensiday = 0;
        }
    }

    openPDF(nopp: any, idpurchaseorder: any) {
        let nopp1 = '';
        nopp1 = nopp.replace(/\//g, '-');
        this.purchasingService.getPP(nopp1, idpurchaseorder).subscribe((purchasing) => {
            this.content = purchasing.content;
            this.contenttype = purchasing.contentContentType;
            if (this.contenttype === '' || this.contenttype === undefined || this.contenttype === null) {
                alert('Lampiran Tidak Ada');
            } else {
                const byteCharacters = atob(this.content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': this.contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const fileURL = URL.createObjectURL(blobContent);
                // window.open(fileURL, '_blank');
                const win = window.open(this.content);
                win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
            }
        });
    }

    filterBarangSingle(event) {
        const query = event.query;
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.getNewBarangTeknik().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        } else {
            this.purchaseOrderService.getNewBarang().then((newBarangs) => {
                this.filteredBarang = this.filterBarang(query, newBarangs);
            });
        }
    }

    filterBarang(query, newBarangs: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        console.log('idmasbahan : ' + this.newBarang.idMasBahan);
        if (this.internal === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.newBarang.idMasBahan)
                .subscribe(
                    (res: MasterBarang) => {
                        console.log('ini isi bahan = ', res.kd_barang);
                        this.kdbarang = res.kd_barang;
                        this.nmbarang = res.nama_barang
                        this.uombarang = res.satuan;
                    },
                    (res: MasterBahan) => {
                        this.kdbarang = res.kdBahan;
                        this.nmbarang = res.nmBahan;
                        this.uombarang = res.satuan;
                    }
                )
        } else {
            this.purchaseOrderService.findBarang(this.newBarang.idMasBahan)
                .subscribe(
                    (res: MasterBahan) => {
                        this.kdbarang = res.kdBahan;
                        this.nmbarang = res.nmBahan;
                        this.uombarang = res.satuan;
                    },
                    (res: MasterBahan) => {
                        this.kdbarang = res.kdBahan;
                        this.nmbarang = res.nmBahan;
                        this.uombarang = res.satuan;
                    }
                )
        }
    }

    public additem() {
        this.purchasingItem = new PurchasingItem();
        this.purchasingItem.nourut = this.purchasingItems.length + 1;
        this.purchasingItem.id = this.purchasingItems.length + 1;
        this.purchasingItem.noPP = '';
        this.purchasingItem.productcode = this.kdbarang
        this.purchasingItem.productname = this.nmbarang;
        this.purchasingItem.ppqty = this.qtybarang;
        this.purchasingItem.bidprice = 0;
        this.purchasingItem.uom = this.uombarang;
        this.purchasingItem.qty = this.qtybarang
        this.purchasingItem.qtyTemp = this.qtybarang;
        this.purchasingItem.price = 0;
        this.purchasingItem.dtsentnote = '';
        this.purchasingItems = [...this.purchasingItems, this.purchasingItem];
    }
}
