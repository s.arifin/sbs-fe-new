import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDateUtils } from 'ng-jhipster';

import { Purchasing, Valuta } from './purchasing.model';
import { PurchasingService } from './purchasing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, Account, UserService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PurchaseOrderService, PurchaseOrder } from '../purchase-order';
import { PurchaseOrderItemService, PurchaseOrderItem } from '../purchase-order-item';
import { PurchasingItem, MappingUserPO, UOMPO, MasterDiv } from '../purchasing-item';
import { ConfirmationService } from 'primeng/components/common/api';
import { Uom } from '../uom';
import { LoadingService } from '../../layouts';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-purchasing-new',
    templateUrl: './purchasing-new.component.html'
})
export class PurchasingNewComponent implements OnInit, OnDestroy {
    [x: string]: any;
    currentAccount: any;
    purchasings: Purchasing[];
    purchasing: Purchasing;
    purchaseOrder: PurchaseOrder;
    purchaseOrderItems: PurchaseOrderItem[];
    purchasingItems: PurchasingItem[];
    purchasingItem: PurchasingItem;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    eventSubscriberPPLOV: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter: PurchasingItem[];
    newSuplier: MasterSupplier;
    newValuta: Valuta;
    newOtorisasi: any;
    newOtorisasis: any[];
    newSupliers: any[];
    filteredSuppliers: any[];
    filteredValuta: any[];
    filteredOtorisasi: any[];
    isSelectSupplier: boolean;
    isSelectValuta: boolean;
    isSelectOtorisasi: boolean;
    inputSupplyerName: string;
    inputValuta: string;
    inputOtorisasi: string;
    splAddress: string;
    mappingUserPo: MappingUserPO;
    jumlah: number;
    checkPPN: boolean;
    checkPPN1: boolean;
    checkPPH: boolean;
    tempTotalPrice: number;
    uomPO: UOMPO[];
    isDiscPersen: boolean;
    newOto: string;
    qtypoTemp: number;
    minimumDate: Date = new Date();
    filteredCodeSuppliers: any[];
    checkOngkirPPN: boolean;
    masterDivs: MasterDiv[];
    masterDiv: MasterDiv;
    isretensi: boolean;
    isDiscPersenItem: boolean;
    content: any;
    contenttype: any;
    maxInputAllowed: any;
    inputCount: any;
    hidden2: any;
    hidden3: any;
    hidden4: any;
    hidden5: any;
    idInternal: any;
    idPlant: any;
    isDiscPersenAtas: boolean;
    isDiscPersenBawah: boolean;
    newPPN: number;
    oldPPN: number;
    arrayPPN: Array<object> = [
        { label: 'Pilih PPN', value: 0 },
    ]
    ppnNew: boolean;
    ppnOld: boolean;
    constructor(
        private purchasingService: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private purchaseOrderService: PurchaseOrderService,
        private purchaseOrderItemService: PurchaseOrderItemService,
        protected confirmationService: ConfirmationService,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
        private dateUtils: JhiDateUtils,
        private masterSupplierService: MasterSupplierService,
        private reportUtilService: ReportUtilService,
        private userService: UserService,
    ) {
        this.purchaseOrder = new PurchaseOrder();
        this.purchasingItems = new Array<PurchasingItem>();
        this.uomPO = new Array<UOMPO>();
        this.purchasing = new Purchasing();
        this.newOto = '';
        this.uomPO = new Array<UOMPO>();
        this.purchasing = new Purchasing();
        this.purchasing.isppn = 0;
        this.purchasing.ispph = 0;
        this.isDiscPersen = true;
        this.jumlah = 0;
        this.checkPPN = false;
        this.checkPPN1 = false;
        this.checkPPH = false;
        this.checkOngkirPPN = false;
        this.tempTotalPrice = 0;
        this.purchaseOrderItems = new Array<PurchaseOrderItem>();
        this.purchasingItem = new PurchasingItem();
        this.purchasingItems = new Array<PurchasingItem>();
        this.newSuplier = new MasterSupplier();
        this.newValuta = new Valuta();
        this.isretensi = false;
        this.isDiscPersenItem = false;
        this.maxInputAllowed = 0;
        this.inputCount = 0;
        this.hidden2 = true;
        this.hidden3 = true;
        this.hidden4 = true;
        this.hidden5 = true;
        this.newPPN = 0;
        this.idInternal = this.principal.getIdInternal();
        if (this.idInternal.includes('SKG')) {
            this.idPlant = 'SKG';
        } else if (this.idInternal.includes('SMK')) {
            this.idPlant = 'SMK';
        } else {
            this.idPlant = 'SBS';
        }
        this.isDiscPersenAtas = false;
        this.isDiscPersenBawah = false;
        this.newPPN = 0;
        this.oldPPN = 0;
        this.ppnNew = false;
        this.ppnOld = false;
    }

    loadAll() {
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/purchasing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/purchasing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.purchasing.disc = 0;
        this.purchasing.tolerancenumsend = 0;
        this.loadAll();
        this.checkDisc();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInPurchasings();
        this.registerFromPPLOV();
        this.userService.queryAllDiv()
            .subscribe(
                (res) => this.masterDivs = res.json
            );
        this.purchasingService.getPPN({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc'],
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                // this.arrayPPN.push({
                //     label: element.createdBy,
                //     value: parseFloat(element.createdBy)
                // });
                this.newPPN = parseFloat(element.createdBy);
                this.oldPPN = parseFloat(element.approvedBy);
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }

    ngOnDestroy() {
        this.eventSubscriberPPLOV.unsubscribe();
    }

    trackId(index: number, item: Purchasing) {
        return item.id;
    }

    registerFromPPLOV() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('purchaseOrderLovModification', () => this.registerChoosPP());
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.purchasings = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    checkIsUsePO(nopo: string): Promise<void> {
        return new Promise<void>(
            (resolvePO, reject) => {
                this.purchasingService.checkIsUsePO({
                    query: 'noPO:' + nopo
                }).subscribe(
                    (res: MappingUserPO) => {
                        this.mappingUserPo = res;
                        resolvePO();
                    }
                )
            }
        )
    }

    registerChoosPP() {
        this.purchaseOrderService.values.subscribe(
            (respARR: PurchaseOrder[]) => {
                respARR.forEach(
                    (e: PurchaseOrder) => {
                        this.purchaseOrder = new PurchaseOrder();
                        this.purchaseOrder = e;
                        if (this.newOto === '') {
                            this.newOto = this.purchaseOrder.pic;
                        }
                        if (this.newOto !== '') {
                            if (this.newOto === e.pic) {
                                if (this.purchaseOrder.idPurchaseOrder === null || this.purchaseOrder.idPurchaseOrder === undefined) {
                                    this.toasterService.showToaster('error', 'Pilih Nomo PP', 'Pilih Terlebih Dahulu Nomor PP');
                                } else {
                                    this.purchaseOrderItems = new Array<PurchaseOrderItem>();
                                    this.purchaseOrderItems.push(this.purchaseOrder.item); // [...this.purchasingItems, this.purchaseOrder.item];
                                    console.log('cek ini data order itm nya yahh => ', this.purchaseOrderItems);
                                    for (let i = 0; i < this.purchaseOrderItems.length; i++) {
                                        const idItems = this.purchaseOrderItems[i].idPurOrdItem;
                                        const idprior = this.purchaseOrder.mapp.idmapprior;
                                        this.filter = new Array<PurchasingItem>();
                                        this.filter = this.purchasingItems.filter(
                                            function dataSama(items) {
                                                return (items.idPurcOrdIte === idItems && items.idprior === idprior)
                                            }
                                        );
                                        if (this.filter.length <= 0) {
                                            if (this.purchaseOrderItems[i].qty === this.purchaseOrderItems[i].qtybookingpo) {
                                                this.toasterService.showToaster('Informasi', 'QTY PO', 'QTY PO SUDAH TERPENUHI');
                                            } else {

                                                if (this.purchaseOrderItems[i].qtytopo === 0) {
                                                    this.toasterService.showToaster('Informasi', 'QTY PO', 'QTY PP TO PO Sudah Tidak Ada');
                                                } else if (this.purchaseOrderItems[i].qtytopo > 0 && (this.purchaseOrderItems[i].qty !== this.purchaseOrderItems[i].qtybookingpo)) {
                                                    this.purchasingItem = new PurchasingItem();
                                                    this.purchasingItem.nourut = this.purchasingItems.length + 1;
                                                    this.purchasingItem.id = this.purchasingItems.length + 1;
                                                    this.purchasingItem.noPP = this.purchaseOrder.noPurchaseOrder;
                                                    this.purchasingItem.idPurcOrdIte = this.purchaseOrderItems[i].idPurOrdItem
                                                    this.purchasingItem.productcode = this.purchaseOrderItems[i].codeProduk;
                                                    this.purchasingItem.productname = this.purchaseOrderItems[i].produkName;
                                                    this.purchasingItem.productname2 = this.purchaseOrderItems[i].produkName2;
                                                    this.purchasingItem.productname3 = this.purchaseOrderItems[i].produkName3;
                                                    this.purchasingItem.productname4 = this.purchaseOrderItems[i].produkName4;
                                                    this.purchasingItem.productname5 = this.purchaseOrderItems[i].produkName5;
                                                    this.purchasingItem.ppqty = this.purchaseOrderItems[i].qty;
                                                    this.purchasingItem.bidprice = this.purchaseOrderItems[i].supplierPrice;
                                                    this.purchasingItem.uom = this.purchaseOrderItems[i].unit;
                                                    if (this.purchaseOrderItems[i].qtybookingpo === 0 && this.purchaseOrderItems[i].qtytopo > 0) {
                                                        this.purchasingItem.qty = this.purchaseOrderItems[i].qtytopo;
                                                    } else if (this.purchaseOrderItems[i].qtybookingpo > 0 && this.purchaseOrderItems[i].qtytopo > 0) {
                                                        if (this.purchaseOrderItems[i].qtybookingpo === 0 && this.purchaseOrderItems[i].qtytopo > 0) {
                                                            this.purchasingItem.qty = this.purchaseOrderItems[i].qtytopo;
                                                        } else if ((this.purchaseOrderItems[i].qtybookingpo > 0 && this.purchaseOrderItems[i].qtybookingpo < this.purchaseOrderItems[i].qty) && this.purchaseOrderItems[i].qtytopo > 0) {
                                                            this.purchasingItem.qty = this.purchaseOrderItems[i].qtytopo - this.purchaseOrderItems[i].qtybookingpo;
                                                        } else if ((this.purchaseOrderItems[i].qtybookingpo === this.purchaseOrderItems[i].qty) && this.purchaseOrderItems[i].qtytopo > 0) {
                                                            this.toasterService.showToaster('Informasi', 'QTY PO', 'QTY PO SUDAH SAMA QTY PP');
                                                        }
                                                    }
                                                    this.purchasingItem.qtyTemp = this.purchasingItem.qty;
                                                    this.purchasingItem.price = 0;
                                                    this.purchasingItem.dtsent = this.dateUtils.convertDateTimeFromServer(this.purchaseOrderItems[i].dtSent);
                                                    this.purchasingItem.sisaPP = this.purchaseOrderItems[i].qty - this.purchaseOrderItems[i].qtybookingpo;
                                                    this.purchasingItem.orderItem = this.purchaseOrderItems[i];
                                                    this.purchasingItem.divitions = this.purchaseOrder.divition;
                                                    this.purchasingItem.divreceipt = this.purchaseOrder.divition;
                                                    this.purchasingItem.dtsentnote = '';
                                                    this.purchasingItem.dtsentpp = this.dateUtils.toDate(this.purchaseOrderItems[i].dtSent);
                                                    this.purchasingItem.idprior = this.purchaseOrder.mapp.idmapprior;
                                                    // this.purchasingItem.content = this.purchaseOrder.content;
                                                    // this.purchasingItem.contentContentType = this.purchaseOrder.contentContentType;
                                                    // this.purchasingItems.push(this.purchasingItem);
                                                    this.purchasingItems = [...this.purchasingItems, this.purchasingItem];
                                                    console.log('ini dt set kiriman = ', this.purchasingItem.dtsent);
                                                } else if (this.purchaseOrderItems[i].qtytopo > 0 && (this.purchaseOrderItems[i].qty === this.purchaseOrderItems[i].qtybookingpo)) {
                                                    this.toasterService.showToaster('Informasi', 'QTY PO', 'QTY PP TO PO Sudah Tidak Ada');
                                                }
                                            }

                                        }
                                    }
                                    // })
                                }
                                // } else {
                                //     this.setMessageUSePO(this.purchaseOrder.noPurchaseOrder, this.mappingUserPo.username);
                                // }
                                // }
                                // )
                            } else {
                                this.toasterService.showToaster('Informasi', 'PIC PP', 'PIC PP tidak Boleh Berbeda');
                            }
                        }
                        // this.createMappingUserPO(this.purchaseOrder);
                        this.purchaseOrderService.values.observers = [];
                    }
                )
            });
    }

    // updateRowData(event) {
    //     if (event.data.id !== undefined) {
    //         this.vehicleSalesOrderService.update(event.data)
    //             .subscribe((res: VehicleSalesOrder) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     } else {
    //         this.vehicleSalesOrderService.create(event.data)
    //             .subscribe((res: VehicleSalesOrder) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     }
    // }

    // private onRowDataSaveSuccess(result: VehicleSalesOrder) {
    //     this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    // }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    public deleteListArray(purchasingItem: PurchasingItem) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                this.purchasingItems = this.purchasingItems.filter(function dataSama(items) { return (items.id !== purchasingItem.id) });
                console.log('purchasing items length == ' + this.purchasingItems.length);
                if (this.purchasingItems.length <= 0) {
                    this.purchasing.otorisasi2 = '';
                    this.purchasing.otorisasi = '';
                    this.newOto = '';
                };
                this.purchasingItems = this.setNoUrut(this.purchasingItems);
                this.calculateTotalPrice(purchasingItem);
            }
        });
    }

    public calculateTotalPrice(purchasingItem: PurchasingItem) {
        this.countQTYListArray(purchasingItem).then(
            () => {
                console.log('ini PO TEMP = ', this.qtypoTemp);
                console.log('ini QTY BOOKING PO = ', purchasingItem.orderItem.qtybookingpo);
                if ((this.qtypoTemp + purchasingItem.orderItem.qtybookingpo) > purchasingItem.orderItem.qty) {
                    this.confirmationService.confirm({
                        header: 'Informasi',
                        message: 'Jumlah QTY PO Tidak Boleh Lebih Besar daripada QTY PP',
                        rejectVisible: false,
                        accept: () => {
                            purchasingItem.qty = purchasingItem.orderItem.qtytopo - this.qtypoTemp;
                        }
                    });
                } else {
                    if (purchasingItem.qty === undefined) {
                        purchasingItem.qty = 0;
                    }
                    if (purchasingItem.price === undefined) {
                        purchasingItem.price = 0;
                    }
                    if (purchasingItem.disc === undefined) {
                        purchasingItem.disc = 0;
                    }
                    if (purchasingItem.discpercent === undefined) {
                        purchasingItem.discpercent = 0;
                    }
                    // purchasingItem.discpercent = parseFloat(((purchasingItem.disc / purchasingItem.price)).toFixed(2));
                    // purchasingItem.disc = parseFloat((purchasingItem.price * purchasingItem.disc / 100).toFixed(2));
                    if (this.isDiscPersenItem === true) {
                        purchasingItem.disc = parseFloat((purchasingItem.price * (purchasingItem.discpercent / 100)).toFixed(2));
                        console.log('cek data disc => ', purchasingItem.disc);
                    }
                    if (this.isDiscPersenItem === false) {
                        purchasingItem.discpercent = parseFloat(((purchasingItem.disc / purchasingItem.price) * 100).toFixed(2));
                        console.log('cek data discpercent => ', purchasingItem.discpercent);
                    }
                    purchasingItem.totalprice = parseFloat((purchasingItem.qty * (purchasingItem.price - purchasingItem.disc)).toFixed(2));
                    this.countGrandTotal(this.purchasingItems)
                }
            }
        )
        this.cekdtsent(purchasingItem);
    }

    createPO() {
        if (this.isretensi === true) {
            this.purchasing.isretensi = 1;
        }
        if (this.isretensi === false) {
            this.purchasing.isretensi = 0;
        }
        if (this.purchasing.retensiday === null || this.purchasing.retensiday === undefined) {
            this.purchasing.retensiday = 0;
        }
        if (this.newSuplier.kd_suppnew === undefined || this.newSuplier.kd_suppnew === '' || this.newSuplier.kd_suppnew === null) {
            this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Supplier Tidak boleh kosong');
        }
        // if (this.isDiscPersen === true) {
        //     this.purchasing.isDiscPersen = 1;
        // } else {
        //     this.purchasing.isDiscPersen = 0;
        // }
        this.purchasing.supliercode = this.newSuplier.kd_suppnew;
        this.purchasing.totalprice = this.tempTotalPrice;
        this.purchasing.valuta = this.newValuta.valutacode;
        this.purchasing.price = this.jumlah;
        this.purchasing.otorisasi2 = this.newOto;
        this.purchasing.otorisasi = this.newOto;
        if (this.purchasing.tolerancenumsend === undefined || this.purchasing.tolerancenumsend === null) {
            this.purchasing.tolerancenumsend = 0;
        }
        if (this.purchasing.notes2 === null || this.purchasing.notes2 === undefined) {
            this.purchasing.notes2 = '';
        }
        if (this.purchasing.notes === null || this.purchasing.notes === undefined) {
            this.purchasing.notes = '';
        }
        if (this.purchasing.paynotes === null || this.purchasing.paynotes === undefined) {
            this.purchasing.paynotes = '';
        }
        if (this.purchasing.ppn === null || this.purchasing.ppn === undefined) {
            this.purchasing.ppn = 0;
        }
        if (this.purchasing.tenor === null || this.purchasing.tenor === undefined) {
            this.purchasing.tenor = 0;
        }
        if (this.purchasing.supliercode === null || this.purchasing.supliercode === undefined || this.purchasing.supliercode === '' || this.newSuplier.kd_suppnew === undefined || this.newSuplier.kd_suppnew === '') {
            this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Supplier Tidak boleh kosong');
        } else
            if (this.purchasing.valuta === null || this.purchasing.valuta === undefined || this.purchasing.valuta === '') {
                this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Mata Uang Tidak boleh kosong');
            } else
                if (this.purchasing.sentto === null || this.purchasing.sentto === undefined || this.purchasing.sentto === '') {
                    this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Dikirim Tidak boleh kosong');
                } else if (this.cekItems() === false) {
                    this.toasterService.showToaster('Information', 'Kolom Kosong', 'Kolom Catatan tanggal kirim Tidak boleh kosong');
                } else {
                    const nilaiDuplikat: PurchasingItem[] = this.findProductPrice(this.purchasingItems);
                    const nilaiQtyPPtoPO: PurchasingItem[] = this.findQTY(this.purchasingItems);
                    if (nilaiDuplikat.length > 0) {
                        this.toasterService.showToaster('Information', 'Beda Harga', 'Harga Item Tidak Boleh Berbeda Jika Terdapat Item Yang Sama');
                        this.loadingService.loadingStop();
                    } else if (nilaiQtyPPtoPO.length > 0) {
                        this.toasterService.showToaster('Information', 'QTY PO > QTY PP', 'Terdapat Item Dengan QTY PO Lebih Besar Dari QTY PO. Tidak Boleh QTY PO Lebih Besar Dari QTY PP');
                        this.loadingService.loadingStop();
                    } else {
                        // this.purchasing.otorisasi = this.newOto;
                        this.loadingService.loadingStart();
                        this.purchasing.revision = 0;
                        this.purchasingService.createPO(this.purchasing, this.purchasingItems)
                            .subscribe(
                                (res) => {
                                    this.router.navigate(['/purchasing']);
                                    this.loadingService.loadingStop();
                                })
                    }
                }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    findProductPrice(products: PurchasingItem[]): PurchasingItem[] {
        const duplikat: { [key: string]: PurchasingItem[] } = {};
        const hasil: PurchasingItem[] = [];

        for (const barang of products) {
            const kunci = barang.productcode;

            if (duplikat[kunci]) {
                // Jika sudah ada barang dengan kode yang sama
                const barangDuplikat = duplikat[kunci].find((b) => b.price !== barang.price);

                if (barangDuplikat) {
                    hasil.push(barang);
                }
            } else {
                duplikat[kunci] = [barang];
            }
        }

        return hasil;
    }

    findQTY(products: PurchasingItem[]): PurchasingItem[] {
        return products.filter((barang) => barang.qty > barang.ppqty);
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inputSupplyerName = this.newSuplier.nmSupplier;
        this.splAddress = this.newSuplier.alamat1 + ' ' + this.newSuplier.alamat2;
        this.purchasing.tenor = this.newSuplier.tenor;
        this.isSelectSupplier = true;
    }

    public setMessageUSePO(nopo: string, username: string) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Nomor PO = ' + nopo + '<br> Sedang diProses Oleh = ' + username,
            rejectVisible: false,
            accept: () => {
            }
        });
    }

    createMappingUserPO(purchaseOrder: PurchaseOrder) {
        this.purchasingService.createIsUsePO(purchaseOrder)
            .subscribe(
                (res) => console.log('sukses')
            )
    }

    countGrandTotal(purchasingItems: PurchasingItem[]) {
        this.jumlah = 0;
        this.purchasing.totaldisc = 0;
        let totalDis = 0;
        this.tempTotalPrice = 0;
        purchasingItems.forEach(
            (each) => {
                if (each.price === undefined) {
                    each.totalprice = 0;
                }
                this.jumlah += each.totalprice;
            }
        );
        if (this.purchasing.disc !== undefined && this.purchasing.disc !== 0) {
            totalDis = Math.round(this.jumlah * this.purchasing.disc / 100);
            this.purchasing.totaldisc = totalDis;
        } else if (this.checkPPN === true) {
            if (this.jumlah !== undefined && this.jumlah !== 0) {
                this.checkPPN = true;
                if (this.ppnOld) {
                    this.purchasing.ppn = parseFloat((this.jumlah * this.oldPPN / 100).toFixed(2));
                    this.purchasing.nomppn = 11;
                } else {
                    this.purchasing.ppn = parseFloat((this.jumlah * this.newPPN / 100).toFixed(2));
                    this.purchasing.nomppn = 11;
                }
                this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc + parseFloat((this.jumlah * 11 / 100).toFixed(2));
                this.tempTotalPrice = this.purchasing.totalprice;
            }
        } else {
            if (this.jumlah !== undefined && this.jumlah !== 0) {
                this.checkPPN = false;
                this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc;
                this.tempTotalPrice = this.purchasing.totalprice;
            }
        }
    }
    checkIsPPn() {
        if (this.purchasing.disc === undefined && this.purchasing.disc === null) {
            this.purchasing.disc = 0;
            this.purchasing.totaldisc = 0;
        }
        if (this.purchasing.totaldisc === undefined && this.purchasing.totaldisc === null) {
            this.purchasing.totaldisc = 0;
            this.purchasing.disc = 0;
        }
        if (this.purchasing.disc >= 100) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Discount Tidak Boleh Melebihi / Sama dengan 100%',
                rejectVisible: false,
                accept: () => {
                }
            });
        }
        if (this.isDiscPersen === true) {
            this.purchasing.totaldisc = parseFloat((this.jumlah * this.purchasing.disc / 100).toFixed(2));
        }
        if (this.isDiscPersen === false) {
            // this.purchasing.disc = (Math.round(this.purchasing.totaldisc / this.jumlah * 100));
            this.purchasing.disc = parseFloat(((this.purchasing.totaldisc / this.jumlah) * 100).toFixed(2));
        }

        if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === false) {
            if (this.checkPPN === true) {
                this.purchasing.isppn = 1;
                if (this.ppnOld) {
                    this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100).toFixed(2));
                    this.purchasing.nomppn = this.oldPPN;
                } else {
                    this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100).toFixed(2));
                    this.purchasing.nomppn = this.newPPN;
                }
            }
            if (this.checkPPN === false) {
                this.purchasing.isppn = 0;
                this.purchasing.ppn = 0;
                this.purchasing.nomppn = 0;
            }
        } else if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === true) {
            alert('Pembulatan PPN tidak boleh aktif keduanya, pilih salah satu saja');
            this.isDiscPersenAtas = false;
            this.isDiscPersenBawah = false;
            if (this.checkPPN === true) {
                this.purchasing.isppn = 1;
                if (this.ppnOld) {
                    this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100).toFixed(2));
                    this.purchasing.nomppn = this.oldPPN;
                } else {
                    this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100).toFixed(2));
                    this.purchasing.nomppn = this.newPPN;
                }
            }
            if (this.checkPPN === false) {
                this.purchasing.isppn = 0;
                this.purchasing.ppn = 0;
                this.purchasing.nomppn = 0;
            }
        } else if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === true) {
            if (this.checkPPN === true) {
                this.purchasing.isppn = 1;
                if (this.ppnOld) {
                    this.purchasing.ppn = parseFloat(Math.floor(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100)).toFixed(2));
                    this.purchasing.nomppn = this.oldPPN;
                } else {
                    this.purchasing.ppn = parseFloat(Math.floor(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100)).toFixed(2));
                    this.purchasing.nomppn = this.newPPN;
                }
            }
            if (this.checkPPN === false) {
                this.purchasing.isppn = 0;
                this.purchasing.ppn = 0;
                this.purchasing.nomppn = 0;
            }
        } else if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === false) {
            if (this.checkPPN === true) {
                this.purchasing.isppn = 1;
                if (this.ppnOld) {
                    this.purchasing.ppn = parseFloat(Math.ceil(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100)).toFixed(2));
                    this.purchasing.nomppn = this.oldPPN;
                } else {
                    this.purchasing.ppn = parseFloat(Math.ceil(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100)).toFixed(2));
                    this.purchasing.nomppn = this.newPPN;
                }
            }
            if (this.checkPPN === false) {
                this.purchasing.isppn = 0;
                this.purchasing.ppn = 0;
                this.purchasing.nomppn = 0;
            }
        }
        // if (this.checkOngkirPPN === true) {
        //     this.purchasing.totalongkir = this.purchasing.ongkir + (this.purchasing.ongkir * 10) / 100;
        // }
        // if (this.checkOngkirPPN === false) {
        //     if (this.purchasing.ongkir === undefined) {
        //         this.purchasing.ongkir = 0;
        //         this.purchasing.totalongkir = this.purchasing.ongkir;
        //     } else {
        //         this.purchasing.totalongkir = this.purchasing.ongkir;
        //     }
        // }
        // this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc + this.purchasing.ppn + this.purchasing.totalongkir;
        this.purchasing.totalprice = parseFloat((this.jumlah - this.purchasing.totaldisc + this.purchasing.ppn).toFixed(2));
        this.tempTotalPrice = this.purchasing.totalprice;
    }

    checkIsPPH() {

        if (this.checkPPH === true) {
            this.purchasing.ispph = 1;
        } else {
            this.purchasing.ispph = 0;
        }

    }

    countDisc() {
        if (this.purchasing.disc !== undefined && this.purchasing.disc !== 0) {
            this.tempTotalPrice = 0;
            let totalDis = 0;
            totalDis = Math.round(this.jumlah * this.purchasing.disc / 100);
            this.purchasing.totaldisc = totalDis;
            this.tempTotalPrice = this.purchasing.totalprice;
            this.tempTotalPrice = this.purchasing.totalprice - this.purchasing.totaldisc;
        }
    }

    filterValutaSingle(event) {
        const query = event.query;
        this.purchasingService.getAllValuta().then((valuta) => {
            this.filteredValuta = this.filterValuta(query, valuta);
        });
    }

    filterValuta(query, valuta: Valuta[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < valuta.length; i++) {
            const valutas = valuta[i];
            if (valutas.valutacode.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(valutas);
            }
        }
        return filtered;
    }

    public selectValuta(isSelect?: boolean): void {
        this.inputValuta = this.newValuta.valutacode;
        this.isSelectValuta = true;
    }

    validasiToleransi() {
        if (this.purchasing.tolerancenumsend > 10) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Toleransi Kirim Tidak Boleh Lebih Dari 10%',
                rejectVisible: false,
                accept: () => {
                }
            });
            this.purchasing.tolerancenumsend = 0;
        }
    }
    checkDisc() {
        if (this.isDiscPersen === true) {
            this.purchasing.isdisc = 1;
            console.log('ini cek = ', this.purchasing.isdisc);
        }
        if (this.isDiscPersen === false) {
            this.purchasing.isdisc = 0;
            console.log('ini cek = ', this.purchasing.isdisc);
        }
    }

    filterOtorisasiSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredOtorisasi = this.filterOtorisasi(query, newOtorisasi);
        });
    }

    filterOtorisasi(query, newOtorisasi: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newOtorisasi.length; i++) {
            const newOtorisasis = newOtorisasi[i];
            if (newOtorisasis.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newOtorisasis);
            }
        }
        return filtered;
    }
    public selectOtorisasi(isSelect?: boolean): void {
        this.inputOtorisasi = this.newOtorisasi.login;
        this.isSelectOtorisasi = true;
    }

    public addListArray(items: PurchasingItem) {

        this.countQTYListArray(items).then(
            () => {
                if ((this.qtypoTemp > items.orderItem.qtytopo) && items.orderItem.qtybookingpo === 0) {
                    this.toasterService.showToaster('INFO', 'INFORMASI', 'QTY PP TO PO Sudah Tidak Bisa Ditambah Lagi');
                } else if ((this.qtypoTemp !== items.orderItem.qtybookingpo) && items.orderItem.qtybookingpo < items.orderItem.qty) {
                    if (items.orderItem.qtytopo - this.qtypoTemp === 0) {
                        this.toasterService.showToaster('INFO', 'INFORMASI', 'Item tidak bisa di pecah lagi karena QTY PO Kurang dari 1');
                    } else {
                        const pp = new PurchasingItem();
                        pp.id = this.purchasingItems.length + 1
                        pp.noPP = items.noPP;
                        pp.orderItem = items.orderItem;
                        pp.ppqty = items.ppqty;
                        pp.uom = items.uom;
                        pp.productcode = items.productcode;
                        pp.productname = items.productname;
                        pp.productname2 = items.productname2;
                        pp.productname3 = items.productname3;
                        pp.productname4 = items.productname4;
                        pp.productname5 = items.productname5;
                        pp.ppqty = items.ppqty;
                        pp.dtsent = items.dtsent;
                        pp.sisaPP = items.sisaPP;
                        pp.qty = items.orderItem.qtytopo - this.qtypoTemp;
                        pp.qtypoTemp = this.qtypoTemp;
                        pp.price = items.price;
                        pp.totalprice = (pp.ppqty * pp.price)
                        pp.bidprice = items.bidprice;
                        pp.divitions = items.divitions;
                        pp.divreceipt = items.divreceipt;
                        pp.dtsentpp = items.dtsentpp;
                        this.purchasingItems = [...this.purchasingItems, pp];
                        this.calculateTotalPrice(pp);
                        this.purchasingItems = this.setNoUrut(this.purchasingItems);
                    }
                } else {
                    this.toasterService.showToaster('INFO', 'INFORMASI', 'Item tidak bisa di pecah lagi karena QTY PO sudah sama dengan QTY PP');
                }
            }
        )
    }

    countQTYListArray(items: PurchasingItem): Promise<void> {
        return new Promise<void>(
            (resolveArr, reject) => {
                this.qtypoTemp = 0;
                this.purchasingItems.forEach(
                    (e) => {
                        if (e.orderItem.idPurOrdItem === items.orderItem.idPurOrdItem) {
                            this.qtypoTemp += e.qty;
                            console.log(this.qtypoTemp);
                        }
                    }
                );
                resolveArr();
            }
        )
    }
    backMainPage() {
        this.router.navigate(['/purchasing']);
    }
    setNoUrut(pp: PurchasingItem[]): PurchasingItem[] {
        const aa = Array<PurchasingItem>();
        pp.forEach(
            (e: PurchasingItem) => {
                e.nourut = aa.length + 1;
                aa.push(e);
            }
        )
        return aa;
    }
    setFocus(nourut) {
        document.getElementById('field_dtsent_' + nourut).focus();
    }

    openPdf(rowData: PurchasingItem) {
        this.loadingService.loadingStart();
        const filter_data = 'idPruchaseOrder:' + rowData.orderItem.idPurchaseOrder;
        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf', { filterData: filter_data })
            .subscribe((response: any) => {
                const reader = new FileReader();
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                reader.readAsDataURL(response.blob());
                // tslint:disable-next-line: space-before-function-paren
                reader.onloadend = function () {
                    const win = window.open();
                    win.document.write('<iframe src="' + reader.result + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
                }
                this.loadingService.loadingStop();
            },
                (err) => {
                    this.loadingService.loadingStop();
                });
        this.loadingService.loadingStop();
    }

    openPDFPP(idpp: any) {
        this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }

    cekdtsent(rowData: PurchasingItem) {

        // this.purchasingItems.forEach(
        //     (e) => {
        if (rowData.dtsent.toUTCString() !== rowData.dtsentpp.toUTCString()) {
            if (rowData.dtsentnote === '') {
                document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
                this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + rowData.nourut);
                return;
            }
            if (rowData.dtsentnote.trim() === '') {
                document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
                this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + rowData.nourut);
                return;
            }
            if (rowData.dtsentnote === undefined) {
                document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
                this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + rowData.nourut);
                return;
            }
        }
        //     }
        // )
    }

    cekItems(): Boolean {
        let hasil = true;
        this.purchasingItems.forEach(
            (e) => {
                if (e.dtsent.toUTCString() !== e.dtsentpp.toUTCString()) {
                    if (e.dtsentnote === '') {
                        document.getElementById('field_dtsentnote_' + e.nourut).focus();
                        this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut);
                        hasil = false;
                        return hasil;
                    }
                    if (e.dtsentnote.trim() === '') {
                        document.getElementById('field_dtsentnote_' + e.nourut).focus();
                        this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut);
                        hasil = false;
                        return hasil;
                    }
                    if (e.dtsentnote === undefined) {
                        document.getElementById('field_dtsentnote_' + e.nourut).focus();
                        this.toasterService.showToaster('Info', 'Tanggal Butuh', 'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut);
                        hasil = false;
                        return hasil;
                    }
                }
            }
        )
        return hasil;
    }
    checkRetensi() {
        if (this.isretensi === true) {
            this.purchasing.isretensi = 1;
        }
        if (this.isretensi === false) {
            this.purchasing.isretensi = 0;
            this.purchasing.retensiday = 0;
        }
    }

    openPDF(nopp: any, idpurchaseorder: any) {
        let nopp1 = '';
        nopp1 = nopp.replace(/\//g, '-');
        this.purchasingService.getPP(nopp1, idpurchaseorder).subscribe((purchasing) => {
            this.content = purchasing.content;
            this.contenttype = purchasing.contentContentType;
            if (this.contenttype === '' || this.contenttype === undefined || this.contenttype === null) {
                alert('Lampiran Tidak Ada');
            } else {
                const byteCharacters = atob(this.content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': this.contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const fileURL = URL.createObjectURL(blobContent);
                // window.open(fileURL, '_blank');
                const win = window.open(this.content);
                win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
            }
        });
    }

    addInput() {
        this.inputCount++;
        if (this.inputCount === 1) {
            this.hidden2 = false;
            this.hidden3 = true;
            this.hidden4 = true;
            this.hidden5 = true;
        } else if (this.inputCount === 2) {
            this.hidden2 = false;
            this.hidden3 = false;
            this.hidden4 = true;
            this.hidden5 = true;
        } else if (this.inputCount === 3) {
            this.hidden2 = false;
            this.hidden3 = false;
            this.hidden4 = false;
            this.hidden5 = true;
        } else if (this.inputCount === 4) {
            this.hidden2 = false;
            this.hidden3 = false;
            this.hidden4 = false;
            this.hidden5 = false;
        } else if (this.inputCount > 4) {
            alert('Kolom Nama Produk Hanya Bisa Menambahkan 4 Kolom Saja');
        }
    }

    minusInput() {
        this.inputCount--;
        if (this.inputCount === 3) {
            this.hidden2 = false;
            this.hidden3 = false;
            this.hidden4 = false;
            this.hidden5 = true;
        } else if (this.inputCount === 2) {
            this.hidden2 = false;
            this.hidden3 = false;
            this.hidden4 = true;
            this.hidden5 = true;
        } else if (this.inputCount === 1) {
            this.hidden2 = false;
            this.hidden3 = true;
            this.hidden4 = true;
            this.hidden5 = true;
        } else if (this.inputCount === 0) {
            this.hidden2 = true;
            this.hidden3 = true;
            this.hidden4 = true;
            this.hidden5 = true;
            alert('Kolom Tambahan Nama Produk Sudah Tidak Ada');
        }
    }
}
