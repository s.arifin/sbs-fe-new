import { BaseEntity } from '../../shared';

export class PurchasingFilter {
    constructor(
        public pic?: string,
        public kdspl?: string,
        public from?: any,
        public thru?: any,
        public need?: any,
    ) {
    }
}
