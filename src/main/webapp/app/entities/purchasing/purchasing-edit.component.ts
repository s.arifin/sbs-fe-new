import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDateUtils } from 'ng-jhipster';

import { Purchasing, Suplier, Valuta, HistoryRevisi } from './purchasing.model';
import { PurchasingService } from './purchasing.service';
import {
  PurchasingItemService,
  PurchasingItem,
  MappingUserPO,
  UOMPO,
  MasterDiv,
} from '../purchasing-item';
import { Principal, ResponseWrapper, ToasterService, UserService } from '../../shared';
import { PurchaseOrderService, PurchaseOrder } from '../purchase-order';
import { PurchaseOrderItem } from '../purchase-order-item';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts';
import { MasterSupplierService, MasterSupplier } from '../master-supplier';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
  selector: 'jhi-purchasing-edit',
  templateUrl: './purchasing-edit.component.html',
})
export class PurchasingEditComponent implements OnInit, OnDestroy {
  @ViewChild('fileInput') inputEl: ElementRef;
  [x: string]: any;
  purchasing: Purchasing;
  private subscription: Subscription;
  private eventSubscriber: Subscription;
  public purchasingItems: PurchasingItem[];
  currentAccount: any;
  purchasings: Purchasing[];
  purchaseOrder: PurchaseOrder;
  purchaseOrderItems: PurchaseOrderItem[];
  purchasingItem: PurchasingItem;
  error: any;
  success: any;
  eventSubscriberPPLOV: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  filter: PurchasingItem[];
  newSuplier: any;
  newValuta: any;
  newSupliers: any[];
  filteredSuppliers: any[];
  filteredValuta: any[];
  isSelectSupplier: boolean;
  isSelectValuta: boolean;
  inputSupplyerName: string;
  inputValuta: string;
  splAddress: string;
  mappingUserPo: MappingUserPO;
  jumlah: number;
  checkPPN: boolean;
  checkPPH: boolean;
  tempTotalPrice: number;
  uomPO: UOMPO[];
  isDiscPersen: boolean;
  dtsent: any;
  uom: string;
  newOtorisasi: any;
  newOtorisasis: any[];
  filteredOtorisasi: any[];
  isSelectOtorisasi: boolean;
  inputOtorisasi: string;
  isEdit: boolean;
  newOto: string;
  qtypoTemp: number;
  isEditPO: boolean;
  historyRev: HistoryRevisi;
  isView: boolean;
  minimumDate: Date = new Date();
  filteredCodeSuppliers: any[];
  masterDivs: MasterDiv[];
  masterDiv: MasterDiv;
  isretensi: boolean;
  isDiscPersenItem: boolean;
  content: any;
  contenttype: any;
  inputCount: any;
  hidden2: any;
  hidden3: any;
  hidden4: any;
  hidden5: any;
  idInternal: any;
  idPlant: any;
  isDiscPersenAtas: boolean;
  isDiscPersenBawah: boolean;
  newPPN: number;
  oldPPN: number;
  ppnNew: boolean;
  ppnOld: boolean;
  constructor(
    private eventManager: JhiEventManager,
    private purchasingService: PurchasingService,
    private route: ActivatedRoute,
    private purchasingItemService: PurchasingItemService,
    private purchaseOrderService: PurchaseOrderService,
    private router: Router,
    protected confirmationService: ConfirmationService,
    private toasterService: ToasterService,
    private dateUtils: JhiDateUtils,
    private loadingService: LoadingService,
    private masterSupplierService: MasterSupplierService,
    private reportUtilService: ReportUtilService,
    private userService: UserService,
    private principal: Principal,

  ) {
    this.isView = false;
    this.isEditPO = false;
    this.isEdit = false;
    this.isretensi = false;
    this.isDiscPersenItem = false;
    this.inputCount = 4;
    this.hidden2 = false;
    this.hidden3 = false;
    this.hidden4 = false;
    this.hidden5 = false;
    this.idInternal = this.principal.getIdInternal();
    if (this.idInternal.includes('SKG')) {
      this.idPlant = 'SKG';
    } else if (this.idInternal.includes('SMK')) {
      this.idPlant = 'SMK';
    } else {
      this.idPlant = 'SBS';
    }
    this.isDiscPersenAtas = false;
    this.isDiscPersenBawah = false;
    this.newPPN = 0;
    this.oldPPN = 0;
    this.ppnNew = false;
    this.ppnOld = false;
  }

  ngOnInit() {
    this.registerFromPPLOV();
    this.subscription = this.route.params.subscribe((params) => {
      if (params['view']) {
        this.isView = true;
        this.isEdit = true;
        this.isEditPO = false;
        this.load(params['id']);
        // console.log('masuk view');
      } else {
        this.load(params['id']);
        // console.log('tidak masuk view');
      }
    });
    this.registerChangeInPurchasings();
    this.historyRev = new HistoryRevisi();
    this.userService
      .queryAllDiv()
      .subscribe((res) => (this.masterDivs = res.json));
    this.purchasingService.getPPN({ // OTORISASI KE 1
      page: 0,
      size: 10000,
      sort: ['kdBahan', 'asc'],
    }).subscribe((res: ResponseWrapper) => {
      this.newKdBarang = res.json;
      this.newKdBarang.forEach((element) => {
        // this.arrayPPN.push({
        //     label: element.createdBy,
        //     value: parseFloat(element.createdBy)
        // });
        this.newPPN = parseFloat(element.createdBy);
        this.oldPPN = parseFloat(element.approvedBy);
      });
    },
      (res: ResponseWrapper) => {
        this.commontUtilService.showError(res.json);
      }
    );
  }

  load(id) {
    // console.log('Cek is edit = ', this.isEdit);
    this.uom = '';
    this.jumlah = 0;
    this.purchasing = new Purchasing();
    this.purchasingService.find(id).subscribe((purchasing) => {
      this.purchasing = purchasing;
      if (this.purchasing.nomppn === null || this.purchasing.nomppn === undefined || this.purchasing.nomppn === 0) {
        this.purchasing.nomppn = 0;
      } else {
        if (this.purchasing.nomppn === this.oldPPN) {
          this.purchasing.nomppn = this.oldPPN;
          this.ppnOld = true;
        } else if (this.purchasing.nomppn === this.newPPN) {
          this.purchasing.nomppn = this.newPPN;
          this.ppnOld = false;
        } else {
          this.purchasing.nomppn = purchasing.nomppn;
          this.ppnOld = false;
        }
      }
      if (this.purchasing.isretensi === 1) {
        this.isretensi = true;
      }
      if (this.purchasing.isretensi === 0) {
        this.isretensi = false;
      }
      if (this.purchasing.isdisc === 1) {
        this.isDiscPersen = true;
      }
      if (this.purchasing.isdisc === 0) {
        this.isDiscPersen = false;
      }
      if (this.purchasing.isppn === 0 && this.purchasing.ppn < 1) {
        this.checkPPN = false;
      }
      if (this.purchasing.isppn === 1 && this.purchasing.ppn >= 1) {
        this.checkPPN = true;
      }

      if (
        this.purchasing.ispph === undefined ||
        this.purchasing.ispph === null ||
        this.purchasing.ispph === 0
      ) {
        this.checkPPH = false;
      } else {
        this.checkPPH = true;
      }

      if (
        purchasing.statuses.statustype === 17 ||
        purchasing.statuses.statustype === 13
      ) {
        this.isEdit = true;
      }
      if (this.isView === false) {
        if (
          purchasing.printcount === 1 ||
          purchasing.revision >= 3 ||
          purchasing.revision_internal >= 3
        ) {
          this.isEditPO = true;
        }
      }
      // if (purchasing.printcount === 1 && purchasing.revision >= 0 && purchasing.revision <= 3) {
      //     this.isEditPO = true;
      // }
      this.newOto = this.purchasing.otorisasi2;
      this.filterSupplierSingleLoad(purchasing.suplier.nm_supplier);
      this.filterSupplierCodeSingleLoad(purchasing.supliercode);
      this.filterValutaSingleLoad(purchasing.valuta);
      // console.log('ini data suplier == ', purchasing.suplier);
      this.purchasingItemService
        .findByIdPurchasing({
          idPurchasing: id,
        })
        .subscribe((res: ResponseWrapper) => {
          this.purchasingItems = res.json;
          this.purchasingItems.forEach((e: PurchasingItem) => {
            e.id += 1;
            this.jumlah += e.totalprice;
            this.dtsent = new Date(e.dtsent);
            e.qtyTemp = e.qty;
            e.sisaPP = e.orderItem.qty - e.orderItem.qtybookingpo;
            e.price = parseFloat(e.price.toFixed(4));
            e.totalprice = parseFloat(e.totalprice.toFixed(2));
            if (
              e.productname2 === null ||
              e.productname2 === '' ||
              e.productname2 === undefined
            ) {
            }
          });
        });
    });
  }
  previousState() {
    window.history.back();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.eventManager.destroy(this.eventSubscriber);
    this.eventSubscriberPPLOV.unsubscribe();
  }

  registerChangeInPurchasings() {
    this.eventSubscriber = this.eventManager.subscribe(
      'purchasingListModification',
      (response) => this.load(this.purchasing.id)
    );
  }

  filterSupplierSingleLoad(event) {
    const query = event;
    this.masterSupplierService.getNewSupplier().then((newSupliers) => {
      this.filteredSuppliers = this.filterSupplier(query, newSupliers, true);
    });
  }

  filterSupplierSingle(event) {
    const query = event.query;
    this.masterSupplierService.getNewSupplier().then((newSupliers) => {
      this.filteredSuppliers = this.filterSupplier(query, newSupliers);
    });
  }

  filterSupplier(query, newSupliers: any[], isEdit: Boolean = false): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < newSupliers.length; i++) {
      const newSuplier = newSupliers[i];
      // console.log('ini data query == ', query.toLowerCase());
      if (
        newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0
      ) {
        filtered.push(newSuplier);
        if (isEdit === true) {
          this.newSuplier = newSuplier;
          this.splAddress = newSuplier.alamat1 + ' ' + newSuplier.alamat2;
        }
      }
    }
    return filtered;
  }
  public selectSupplier(isSelect?: boolean): void {
    this.inputSupplyerName = this.newSuplier.nmSupplier;
    this.splAddress = this.newSuplier.alamat1 + ' ' + this.newSuplier.alamat2;
    this.purchasing.tenor = this.newSuplier.tenor;
    this.isSelectSupplier = true;
  }

  updatePO() {
    this.loadingService.loadingStart();
    if (
      this.newSuplier.kd_suppnew === null ||
      this.newSuplier.kd_suppnew === '' ||
      this.newSuplier.kd_suppnew === undefined
    ) {
      this.toasterService.showToaster(
        'Information',
        'Kolom Kosong',
        'Kolom Supplier Tidak boleh kosong'
      );
      this.loadingService.loadingStop();
    } else {
      if (this.isretensi === true) {
        this.purchasing.isretensi = 1;
      }
      if (this.isretensi === false) {
        this.purchasing.isretensi = 0;
      }
      if (
        this.purchasing.retensiday === null ||
        this.purchasing.retensiday === undefined
      ) {
        this.purchasing.retensiday = 0;
      }
      this.purchasing.price = this.jumlah;
      this.purchasing.supliercode = this.newSuplier.kd_suppnew;
      this.purchasing.valuta = this.newValuta.valutacode;
      if (this.checkPPN === false) {
        this.purchasing.isppn = 0;
      }
      if (this.checkPPN === true) {
        this.purchasing.isppn = 1;
      }

      if (this.checkPPH === false) {
        this.purchasing.ispph = 0;
      } else {
        this.purchasing.ispph = 1;
      }

      if (
        this.purchasing.tolerancenumsend === undefined ||
        this.purchasing.tolerancenumsend === null
      ) {
        this.purchasing.tolerancenumsend = 0;
      }
      if (this.cekItems() === false) {
        this.toasterService.showToaster(
          'Information',
          'Kolom Kosong',
          'Kolom Catatan tanggal kirim Tidak boleh kosong'
        );
      } else {
        const nilaiDuplikat: PurchasingItem[] = this.findProductPrice(
          this.purchasingItems
        );
        const nilaiQtyPPtoPO: PurchasingItem[] = this.findQTY(this.purchasingItems);
        if (nilaiDuplikat.length > 0) {
          this.toasterService.showToaster(
            'Information',
            'Beda Harga',
            'Harga Item Tidak Boleh Berbeda Jika Terdapat Item Yang Sama'
          );
          this.loadingService.loadingStop();
        } else if (nilaiQtyPPtoPO.length > 0) {
          this.toasterService.showToaster('Information', 'QTY PO > QTY PP', 'Terdapat Item Dengan QTY PO Lebih Besar Dari QTY PO. Tidak Boleh QTY PO Lebih Besar Dari QTY PP');
          this.loadingService.loadingStop();
        } else {
          this.purchasingService
            .updatePO(this.purchasing, this.purchasingItems, this.historyRev)
            .subscribe(
              (res) => {
                this.router.navigate(['/purchasing']);
              },
              (err) => {
                this.loadingService.loadingStop();
              },
              () => {
                this.loadingService.loadingStop();
              }
            );
        }
      }
    }
  }

  findProductPrice(products: PurchasingItem[]): PurchasingItem[] {
    const duplikat: { [key: string]: PurchasingItem[] } = {};
    const hasil: PurchasingItem[] = [];

    for (const barang of products) {
      const kunci = barang.productcode;

      if (duplikat[kunci]) {
        // Jika sudah ada barang dengan kode yang sama
        const barangDuplikat = duplikat[kunci].find(
          (b) => b.price !== barang.price
        );

        if (barangDuplikat) {
          hasil.push(barang);
        }
      } else {
        duplikat[kunci] = [barang];
      }
    }

    return hasil;
  }

  findQTY(products: PurchasingItem[]): PurchasingItem[] {
    return products.filter((barang) => barang.qty > barang.ppqty);
  }

  checkIsPPH() {
    if (this.checkPPH === true) {
      this.purchasing.ispph = 1;
    } else {
      this.purchasing.ispph = 0;
    }
  }

  checkIsUsePO(nopo: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.purchasingService
        .checkIsUsePO({
          query: 'noPO:' + nopo,
        })
        .subscribe((res: MappingUserPO) => {
          // console.log('hasil check po = ', res);
          this.mappingUserPo = res;
          resolve();
        });
    });
  }

  // registerChoosPP() {
  //     this.purchaseOrderService.values.subscribe(
  //         (resp: PurchaseOrder) => {
  //             this.purchaseOrder = resp;
  //             if (this.newOto === resp.pic) {
  //                 this.checkIsUsePO(this.purchaseOrder.noPurchaseOrder).then(
  //                     (resolve) => {
  //                         // console.log('hasil promise check po = ', this.mappingUserPo.idmapuspo);
  //                         if (this.mappingUserPo.idmapuspo === null) {
  //                             if (this.purchaseOrder.idPurchaseOrder === null || this.purchaseOrder.idPurchaseOrder === undefined) {
  //                                 this.toasterService.showToaster('error', 'Pilih Nomo PP', 'Pilih Terlebih Dahulu Nomor PP');
  //                             } else {
  //                                 this.purchasingService.LOVqueryFilterBy({
  //                                     query: 'idInternal:' + this.purchaseOrder.idPurchaseOrder,
  //                                     page: 0,
  //                                     size: 20
  //                                 }).subscribe(
  //                                     (res) => {
  //                                         this.purchaseOrderItems = new Array<PurchaseOrderItem>();
  //                                         this.purchaseOrderItems = res.json;
  //                                         for (let i = 0; i < this.purchaseOrderItems.length; i++) {
  //                                             const idItems = this.purchaseOrderItems[i].idPurOrdItem;
  //                                             this.filter = new Array<PurchasingItem>();
  //                                             this.filter = this.purchasingItems.filter(
  //                                                 function dataSama(items) {
  //                                                     return (items.idPurcOrdIte === idItems)
  //                                                 }
  //                                             );
  //                                             if (this.filter.length <= 0) {
  //                                                 // console.log('cek data filter == ', this.filter);
  //                                                 this.purchasingItem = new PurchasingItem();
  //                                                 this.purchasingItem.nourut = this.purchasingItems.length + 1;
  //                                                 this.purchasingItem.id = this.purchasingItems.length + 1;
  //                                                 this.purchasingItem.noPP = this.purchaseOrder.noPurchaseOrder;
  //                                                 this.purchasingItem.idPurcOrdIte = this.purchaseOrderItems[i].idPurOrdItem
  //                                                 this.purchasingItem.productcode = this.purchaseOrderItems[i].codeProduk;
  //                                                 this.purchasingItem.productname = this.purchaseOrderItems[i].produkName;
  //                                                 this.purchasingItem.ppqty = this.purchaseOrderItems[i].qty;
  //                                                 this.purchasingItem.bidprice = this.purchaseOrderItems[i].supplierPrice;
  //                                                 this.purchasingItem.uom = this.purchaseOrderItems[i].unit;
  //                                                 this.purchasingItem.qty = this.purchaseOrderItems[i].qty - this.purchaseOrderItems[i].qtybookingpo;
  //                                                 this.purchasingItem.qtyTemp = this.purchasingItem.qty;
  //                                                 this.purchasingItem.price = 0;
  //                                                 this.purchasingItem.dtsent = this.dateUtils.convertDateTimeFromServer(this.purchaseOrder.dtNecessity);
  //                                                 this.purchasingItem.orderItem = this.purchaseOrderItems[i];
  //                                                 this.purchasingItem.sisaPP = this.purchaseOrderItems[i].qty - this.purchaseOrderItems[i].qtybookingpo;
  //                                                 // this.purchasingItems.push(this.purchasingItem);
  //                                                 this.purchasingItems = [...this.purchasingItems, this.purchasingItem];
  //                                             }
  //                                         }
  //                                         this.createMappingUserPO(this.purchaseOrder);
  //                                     })
  //                             }
  //                         } else {
  //                             this.setMessageUSePO(this.purchaseOrder.noPurchaseOrder, this.mappingUserPo.username);
  //                         }
  //                     }
  //                 )
  //             } else {
  //                 this.toasterService.showToaster('Informasi', 'PIC PP', 'PIC PP tidak Boleh Berbeda');
  //             }
  //             this.purchaseOrderService.values.observers = [];
  //         });
  // }
  registerChoosPP() {
    this.purchaseOrderService.values.subscribe((respARR: PurchaseOrder[]) => {
      respARR.forEach((e: PurchaseOrder) => {
        this.purchaseOrder = new PurchaseOrder();
        this.purchaseOrder = e;
        if (this.newOto === '') {
          this.newOto = this.purchaseOrder.pic;
        }
        if (this.newOto !== '') {
          if (this.newOto === e.pic) {
            if (
              this.purchaseOrder.idPurchaseOrder === null ||
              this.purchaseOrder.idPurchaseOrder === undefined
            ) {
              this.toasterService.showToaster(
                'error',
                'Pilih Nomor PP',
                'Pilih Terlebih Dahulu Nomor PP'
              );
            } else {
              this.purchaseOrderItems = new Array<PurchaseOrderItem>();
              this.purchaseOrderItems.push(this.purchaseOrder.item); // [...this.purchasingItems, this.purchaseOrder.item];
              for (let i = 0; i < this.purchaseOrderItems.length; i++) {
                const idItems = this.purchaseOrderItems[i].idPurOrdItem;
                const idprior = this.purchaseOrder.mapp.idmapprior;
                this.filter = new Array<PurchasingItem>();
                this.filter = this.purchasingItems.filter(function dataSama(
                  items
                ) {
                  return (
                    items.idPurcOrdIte === idItems && items.idprior === idprior
                  );
                });
                if (this.filter.length <= 0) {
                  if (
                    this.purchaseOrderItems[i].qty ===
                    this.purchaseOrderItems[i].qtybookingpo
                  ) {
                    this.toasterService.showToaster(
                      'Informasi',
                      'QTY PO',
                      'QTY PO SUDAH TERPENUHI'
                    );
                  } else {
                    if (this.purchaseOrderItems[i].qtytopo === 0) {
                      this.toasterService.showToaster(
                        'Informasi',
                        'QTY PO',
                        'QTY PP TO PO Sudah Tidak Ada'
                      );
                    } else if (
                      this.purchaseOrderItems[i].qtytopo > 0 &&
                      this.purchaseOrderItems[i].qty !==
                      this.purchaseOrderItems[i].qtybookingpo
                    ) {
                      this.purchasingItem = new PurchasingItem();
                      this.purchasingItem.nourut =
                        this.purchasingItems.length + 1;
                      this.purchasingItem.id = this.purchasingItems.length + 1;
                      this.purchasingItem.noPP =
                        this.purchaseOrder.noPurchaseOrder;
                      this.purchasingItem.idPurcOrdIte =
                        this.purchaseOrderItems[i].idPurOrdItem;
                      this.purchasingItem.productcode =
                        this.purchaseOrderItems[i].codeProduk;
                      this.purchasingItem.productname =
                        this.purchaseOrderItems[i].produkName;
                      this.purchasingItem.productname2 =
                        this.purchaseOrderItems[i].produkName2;
                      this.purchasingItem.productname3 =
                        this.purchaseOrderItems[i].produkName3;
                      this.purchasingItem.productname4 =
                        this.purchaseOrderItems[i].produkName4;
                      this.purchasingItem.productname5 =
                        this.purchaseOrderItems[i].produkName5;
                      this.purchasingItem.ppqty =
                        this.purchaseOrderItems[i].qty;
                      this.purchasingItem.bidprice =
                        this.purchaseOrderItems[i].supplierPrice;
                      this.purchasingItem.uom = this.purchaseOrderItems[i].unit;
                      if (
                        this.purchaseOrderItems[i].qtybookingpo === 0 &&
                        this.purchaseOrderItems[i].qtytopo > 0
                      ) {
                        this.purchasingItem.qty =
                          this.purchaseOrderItems[i].qtytopo;
                      } else if (
                        this.purchaseOrderItems[i].qtybookingpo > 0 &&
                        this.purchaseOrderItems[i].qtytopo > 0
                      ) {
                        if (
                          this.purchaseOrderItems[i].qtybookingpo === 0 &&
                          this.purchaseOrderItems[i].qtytopo > 0
                        ) {
                          this.purchasingItem.qty =
                            this.purchaseOrderItems[i].qtytopo;
                        } else if (
                          this.purchaseOrderItems[i].qtybookingpo > 0 &&
                          this.purchaseOrderItems[i].qtybookingpo <
                          this.purchaseOrderItems[i].qty &&
                          this.purchaseOrderItems[i].qtytopo > 0
                        ) {
                          this.purchasingItem.qty =
                            this.purchaseOrderItems[i].qtytopo -
                            this.purchaseOrderItems[i].qtybookingpo;
                        } else if (
                          this.purchaseOrderItems[i].qtybookingpo ===
                          this.purchaseOrderItems[i].qty &&
                          this.purchaseOrderItems[i].qtytopo > 0
                        ) {
                          this.toasterService.showToaster(
                            'Informasi',
                            'QTY PO',
                            'QTY PO SUDAH SAMA QTY PP'
                          );
                        }
                      }
                      this.purchasingItem.qtyTemp = this.purchasingItem.qty;
                      this.purchasingItem.price = 0;
                      this.purchasingItem.dtsent =
                        this.dateUtils.convertDateTimeFromServer(
                          this.purchaseOrderItems[i].dtSent
                        );
                      this.purchasingItem.sisaPP =
                        this.purchaseOrderItems[i].qty -
                        this.purchaseOrderItems[i].qtybookingpo;
                      this.purchasingItem.orderItem =
                        this.purchaseOrderItems[i];
                      this.purchasingItem.divitions =
                        this.purchaseOrder.divition;
                      this.purchasingItem.divreceipt =
                        this.purchaseOrder.divition;
                      this.purchasingItem.dtsentnote = '';
                      this.purchasingItem.dtsentpp = this.dateUtils.toDate(
                        this.purchaseOrderItems[i].dtSent
                      );
                      this.purchasingItem.idprior =
                        this.purchaseOrder.mapp.idmapprior;
                      // this.purchasingItem.content = this.purchaseOrder.content;
                      // this.purchasingItem.contentContentType = this.purchaseOrder.contentContentType;
                      // this.purchasingItems.push(this.purchasingItem);
                      this.purchasingItems = [
                        ...this.purchasingItems,
                        this.purchasingItem,
                      ];
                    } else if (
                      this.purchaseOrderItems[i].qtytopo > 0 &&
                      this.purchaseOrderItems[i].qty ===
                      this.purchaseOrderItems[i].qtybookingpo
                    ) {
                      this.toasterService.showToaster(
                        'Informasi',
                        'QTY PO',
                        'QTY PP TO PO Sudah Tidak Ada'
                      );
                    }
                  }

                  // console.log('ini dt set kiriman = ', this.purchasingItem);
                }
              }
              // })
            }
            // } else {
            //     this.setMessageUSePO(this.purchaseOrder.noPurchaseOrder, this.mappingUserPo.username);
            // }
            // }
            // )
          } else {
            this.toasterService.showToaster(
              'Informasi',
              'PIC PP',
              'PIC PP tidak Boleh Berbeda'
            );
          }
        }
        // this.createMappingUserPO(this.purchaseOrder);
        this.purchaseOrderService.values.observers = [];
      });
    });
  }

  public deleteListArray(purchasingItem: PurchasingItem) {
    // if (this.purchasingItems.length === 1) {
    //   alert('Data Detail PO tidak Boleh Kosong');
    // } else {
    // console.log('ini data purchasing item = ', purchasingItem);
    if (
      purchasingItem.idPurcOrdIte !== null &&
      purchasingItem.idpurchasingitem === undefined
    ) {
      this.confirmationService.confirm({
        header: 'Konfirmasi',
        message: 'Apakah Anda yakin akan menghapus data ini ??',
        accept: () => {
          this.purchasingItems = this.purchasingItems.filter(function dataSama(
            items
          ) {
            return items.idPurcOrdIte !== purchasingItem.idPurcOrdIte;
          });
          this.purchasingItems = this.setNoUrut(this.purchasingItems);
          this.calculateTotalPrice(purchasingItem);
        },
      });
    }

    if (
      purchasingItem.idPurcOrdIte !== null &&
      purchasingItem.idPurcOrdIte !== '' &&
      purchasingItem.idpurchasingitem !== undefined
    ) {
      this.confirmationService.confirm({
        header: 'Konfirmasi',
        message: 'Apakah Anda yakin akan menghapus data ini ??',
        accept: () => {
          this.loadingService.loadingStart();
          this.purchasingItemService
            .delete(purchasingItem.idpurchasingitem)
            .subscribe((res) => {
              // this.purchasingItems = this.purchasingItems.filter(function dataSama(items) { return (items.idpurchasingitem !== purchasingItem.idpurchasingitem) });
              // this.purchasingItemService.findByIdPurchasing({
              //     idPurchasing: purchasingItem.idpurchasingfk
              // }).subscribe(
              //     (response: ResponseWrapper) => {
              // this.purchasingItems = response.json;
              this.purchasingItems = this.purchasingItems.filter(
                function dataSama(items) {
                  return (
                    items.idpurchasingitem !== purchasingItem.idpurchasingitem
                  );
                }
              );
              // this.purchasingItems.forEach((e: PurchasingItem) => {
              //     // e.id += 1;
              //     this.jumlah = 0;
              //     this.jumlah += e.totalprice;
              //     this.dtsent = new Date(e.dtsent);
              //     e.qtyTemp = e.qty;
              //     e.sisaPP = e.orderItem.qty - e.orderItem.qtybookingpo;
              // });
              this.purchasingItems = this.setNoUrut(this.purchasingItems);
              this.loadingService.loadingStop();
              // },
              // (err) => this.loadingService.loadingStop()
              // );
            });
          // this.purchasingItems = this.purchasingItems.filter(function dataSama(items) { return (items.idpurchasingitem !== purchasingItem.idpurchasingitem) });
        },
      });
    }
    // }
  }

  public calculateTotalPrice(purchasingItem: PurchasingItem) {
    this.countQTYListArray(purchasingItem).then(() => {
      // console.log('ini qtyPOtemp = ', this.qtypoTemp);
      // console.log('ini qtybookingPO = ', purchasingItem.orderItem.qtybookingpo);
      if (this.qtypoTemp > purchasingItem.orderItem.qty) {
        this.confirmationService.confirm({
          header: 'Informasi',
          message: 'Jumlah QTY PO Tidak Boleh Lebih Besar daripada QTY PP',
          rejectVisible: false,
          accept: () => {
            purchasingItem.qty = purchasingItem.qtyTemp;
          },
        });
      } else {
        if (purchasingItem.qty === undefined) {
          purchasingItem.qty = 0;
        }
        if (purchasingItem.price === undefined) {
          purchasingItem.price = 0;
        }
        if (purchasingItem.disc === undefined) {
          purchasingItem.disc = 0;
        }
        if (purchasingItem.discpercent === undefined) {
          purchasingItem.discpercent = 0;
        }
        if (this.isDiscPersenItem === true) {
          purchasingItem.disc = parseFloat(
            (purchasingItem.price * (purchasingItem.discpercent / 100)).toFixed(
              2
            )
          );
          // console.log('cek data disc => ', purchasingItem.disc);
        }
        if (this.isDiscPersenItem === false) {
          purchasingItem.discpercent = parseFloat(
            ((purchasingItem.disc / purchasingItem.price) * 100).toFixed(2)
          );
          // console.log('cek data discpercent => ', purchasingItem.discpercent);
        }
        purchasingItem.totalprice = parseFloat(
          (
            purchasingItem.qty *
            (purchasingItem.price - purchasingItem.disc)
          ).toFixed(2)
        );
        this.countGrandTotal(this.purchasingItems);
      }
    });
    this.cekdtsent(purchasingItem);
  }

  public setMessageUSePO(nopo: string, username: string) {
    this.confirmationService.confirm({
      header: 'Information',
      message: 'Nomor PO = ' + nopo + '<br> Sedang diProses Oleh = ' + username,
      rejectVisible: false,
      accept: () => { },
    });
  }

  createMappingUserPO(purchaseOrder: PurchaseOrder) {
    this.purchasingService
      .createIsUsePO(purchaseOrder)
      .subscribe((res) => console.log('sukses'));
  }

  countGrandTotal(purchasingItems: PurchasingItem[]) {
    this.jumlah = 0;
    this.purchasing.totaldisc = 0;
    let totalDis = 0;
    this.tempTotalPrice = 0;
    purchasingItems.forEach((each) => {
      if (each.price === undefined) {
        each.totalprice = 0;
      }
      this.jumlah += each.totalprice;
    });
    if (this.purchasing.disc !== undefined && this.purchasing.disc !== 0) {
      totalDis = Math.round((this.jumlah * this.purchasing.disc) / 100);
      this.purchasing.totaldisc = totalDis;
    } else if (this.checkPPN === true) {
      if (this.jumlah !== undefined && this.jumlah !== 0) {
        this.checkPPN = true;
        this.purchasing.ppn = parseFloat(
          ((this.jumlah * this.purchasing.nomppn) / 100).toFixed(2)
        );
        this.purchasing.totalprice =
          this.jumlah -
          this.purchasing.totaldisc +
          parseFloat(((this.jumlah * this.purchasing.nomppn) / 100).toFixed(2));
        this.tempTotalPrice = this.purchasing.totalprice;
      }
    } else {
      if (this.jumlah !== undefined && this.jumlah !== 0) {
        this.checkPPN = false;
        this.purchasing.totalprice = this.jumlah - this.purchasing.totaldisc;
        this.tempTotalPrice = this.purchasing.totalprice;
      }
    }
  }

  checkIsPPn() {
    if (this.purchasing.disc === undefined && this.purchasing.disc === null) {
      this.purchasing.disc = 0;
      this.purchasing.totaldisc = 0;
    }
    if (
      this.purchasing.totaldisc === undefined &&
      this.purchasing.totaldisc === null
    ) {
      this.purchasing.totaldisc = 0;
      this.purchasing.disc = 0;
    }
    if (this.purchasing.disc >= 100) {
      this.confirmationService.confirm({
        header: 'Information',
        message: 'Discount Tidak Boleh Melebihi / Sama dengan 100%',
        rejectVisible: false,
        accept: () => { },
      });
    }
    if (this.isDiscPersen === true) {
      this.purchasing.isdisc = 1;
      this.purchasing.totaldisc = parseFloat(
        ((this.jumlah * this.purchasing.disc) / 100).toFixed(2)
      );
    }
    if (this.isDiscPersen === false) {
      this.purchasing.isdisc = 0;
      // this.purchasing.disc = (Math.round(this.purchasing.totaldisc / this.jumlah * 100));
      this.purchasing.disc = parseFloat(
        ((this.purchasing.totaldisc / this.jumlah) * 100).toFixed(2)
      );
    }
    // if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === false) {
    //   if (this.checkPPN === true) {
    //     this.purchasing.isppn = 1;
    //     this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * 11 / 100).toFixed(2));
    //     this.purchasing.nomppn = 11;
    //   }
    //   if (this.checkPPN === false) {
    //     this.purchasing.isppn = 0;
    //     this.purchasing.ppn = 0;
    //   }
    // } else if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === true) {
    //   alert('Pembulatan PPN tidak boleh aktif keduanya, pilih salah satu saja');
    //   this.isDiscPersenAtas = false;
    //   this.isDiscPersenBawah = false;
    //   if (this.checkPPN === true) {
    //     this.purchasing.isppn = 1;
    //     this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * 11 / 100).toFixed(2));
    //     this.purchasing.nomppn = 11;
    //   }
    //   if (this.checkPPN === false) {
    //     this.purchasing.isppn = 0;
    //     this.purchasing.ppn = 0;
    //   }
    // } else if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === true) {
    //   if (this.checkPPN === true) {
    //     this.purchasing.isppn = 1;
    //     this.purchasing.ppn = parseFloat(Math.floor(((this.jumlah - this.purchasing.totaldisc) * 11 / 100)).toFixed(2));
    //     this.purchasing.nomppn = 11;
    //   }
    //   if (this.checkPPN === false) {
    //     this.purchasing.isppn = 0;
    //     this.purchasing.ppn = 0;
    //   }
    // } else if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === false) {
    //   if (this.checkPPN === true) {
    //     this.purchasing.isppn = 1;
    //     this.purchasing.ppn = parseFloat(Math.ceil(((this.jumlah - this.purchasing.totaldisc) * 11 / 100)).toFixed(2));
    //     this.purchasing.nomppn = 11;
    //   }
    //   if (this.checkPPN === false) {
    //     this.purchasing.isppn = 0;
    //     this.purchasing.ppn = 0;
    //   }
    // }
    if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === false) {
      if (this.checkPPN === true) {
        this.purchasing.isppn = 1;
        if (this.ppnOld) {
          this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100).toFixed(2));
          this.purchasing.nomppn = this.oldPPN;
        } else {
          this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100).toFixed(2));
          this.purchasing.nomppn = this.newPPN;
        }
      }
      if (this.checkPPN === false) {
        this.purchasing.isppn = 0;
        this.purchasing.ppn = 0;
        this.purchasing.nomppn = 0;
      }
    } else if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === true) {
      alert('Pembulatan PPN tidak boleh aktif keduanya, pilih salah satu saja');
      this.isDiscPersenAtas = false;
      this.isDiscPersenBawah = false;
      if (this.checkPPN === true) {
        this.purchasing.isppn = 1;
        if (this.ppnOld) {
          this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100).toFixed(2));
          this.purchasing.nomppn = this.oldPPN;
        } else {
          this.purchasing.ppn = parseFloat(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100).toFixed(2));
          this.purchasing.nomppn = this.newPPN;
        }
      }
      if (this.checkPPN === false) {
        this.purchasing.isppn = 0;
        this.purchasing.ppn = 0;
        this.purchasing.nomppn = 0;
      }
    } else if (this.isDiscPersenAtas === false && this.isDiscPersenBawah === true) {
      if (this.checkPPN === true) {
        this.purchasing.isppn = 1;
        if (this.ppnOld) {
          this.purchasing.ppn = parseFloat(Math.floor(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100)).toFixed(2));
          this.purchasing.nomppn = this.oldPPN;
        } else {
          this.purchasing.ppn = parseFloat(Math.floor(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100)).toFixed(2));
          this.purchasing.nomppn = this.newPPN;
        }
      }
      if (this.checkPPN === false) {
        this.purchasing.isppn = 0;
        this.purchasing.ppn = 0;
        this.purchasing.nomppn = 0;
      }
    } else if (this.isDiscPersenAtas === true && this.isDiscPersenBawah === false) {
      if (this.checkPPN === true) {
        this.purchasing.isppn = 1;
        if (this.ppnOld) {
          this.purchasing.ppn = parseFloat(Math.ceil(((this.jumlah - this.purchasing.totaldisc) * this.oldPPN / 100)).toFixed(2));
          this.purchasing.nomppn = this.oldPPN;
        } else {
          this.purchasing.ppn = parseFloat(Math.ceil(((this.jumlah - this.purchasing.totaldisc) * this.newPPN / 100)).toFixed(2));
          this.purchasing.nomppn = this.newPPN;
        }
      }
      if (this.checkPPN === false) {
        this.purchasing.isppn = 0;
        this.purchasing.ppn = 0;
        this.purchasing.nomppn = 0;
      }
    }
    this.purchasing.totalprice = parseFloat(
      (this.jumlah - this.purchasing.totaldisc + this.purchasing.ppn).toFixed(2)
    );
    this.tempTotalPrice = this.purchasing.totalprice;
  }

  countDisc() {
    if (this.purchasing.disc !== undefined && this.purchasing.disc !== 0) {
      this.tempTotalPrice = 0;
      let totalDis = 0;
      totalDis = Math.round((this.jumlah * this.purchasing.disc) / 100);
      this.purchasing.totaldisc = totalDis;
      this.tempTotalPrice = this.purchasing.totalprice;
      this.tempTotalPrice =
        this.purchasing.totalprice - this.purchasing.totaldisc;
    }
  }

  filterValutaSingle(event) {
    const query = event.query;
    this.purchasingService.getAllValuta().then((valuta) => {
      this.filteredValuta = this.filterValuta(query, valuta);
    });
  }

  filterValutaSingleLoad(event) {
    const query = event;
    this.purchasingService.getAllValuta().then((valuta) => {
      this.filteredValuta = this.filterValuta(query, valuta, true);
    });
  }

  filterValuta(query, valuta: Valuta[], isEdit: Boolean = false): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < valuta.length; i++) {
      const valutas = valuta[i];
      if (valutas.valutacode.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(valutas);
        if (isEdit === true) {
          this.newValuta = valutas;
        }
      }
    }
    return filtered;
  }

  public selectValuta(isSelect?: boolean): void {
    this.inputValuta = this.newValuta.valutacode;
    this.isSelectValuta = true;
  }

  validasiToleransi() {
    if (this.purchasing.tolerancenumsend > 10) {
      this.confirmationService.confirm({
        header: 'Information',
        message: 'Toleransi Kirim Tidak Boleh Lebih Dari 10%',
        rejectVisible: false,
        accept: () => { },
      });
      this.purchasing.tolerancenumsend = 0;
    }
  }
  checkDisc() {
    if (this.isDiscPersen === true) {
      this.purchasing.isdisc = 1;
      // console.log('ini cek = ', this.purchasing.isdisc);
    }
    if (this.isDiscPersen === false) {
      this.purchasing.isdisc = 0;
      // console.log('ini cek = ', this.purchasing.isdisc);
    }
  }
  // checkDisc() {
  //     // if (this.purchasing.totaldisc === undefined ) {
  //     //     this.purchasing.totaldisc = 0;
  //     // }
  //     // if (this.purchasing.ppn === undefined ) {
  //     //     this.purchasing.ppn = 0;
  //     // }
  //     // if (this.jumlah === undefined ) {
  //     //     this.jumlah = 0;
  //     // }
  //     // this.purchasing.disc = 0;
  //     // this.purchasing.totaldisc = 0;
  //     // this.purchasing.totalprice = (this.jumlah - (this.purchasing.totaldisc + this.purchasing.ppn));
  // }

  registerFromPPLOV() {
    this.eventSubscriberPPLOV = this.eventManager.subscribe(
      'purchaseOrderLovModification',
      (response) => this.registerChoosPP()
    );
  }
  filterOtorisasiSingle(event) {
    const query = event.query;
    this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
      this.filteredOtorisasi = this.filterSupplier(query, newOtorisasi);
    });
  }

  filterOtorisasi(query, newOtorisasis: any[]): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < newOtorisasis.length; i++) {
      const newOtorisasi = newOtorisasis[i];
      if (newOtorisasi.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(newOtorisasi);
      }
    }
    return filtered;
  }
  public selectOtorisasi(isSelect?: boolean): void {
    this.inputOtorisasi = this.newOtorisasi.login;
    this.isSelectSupplier = true;
  }
  back() {
    if (this.purchasingItems.length < 1) {
      this.confirmationService.confirm({
        header: 'Konfirmasi',
        message: 'Apakah anda yakin akan membiarkan item PO kosong ?',
        accept: () => {
          this.router.navigate(['/purchasing']);
        },
      });
    } else {
      this.router.navigate(['/purchasing']);
    }
  }

  upload() {
    const inputEl: HTMLInputElement = this.inputEl.nativeElement;
    const fileCount: number = inputEl.files.length;
    const formData = new FormData();
    if (fileCount > 0) {
      // a file was selected
      for (let i = 0; i < fileCount; i++) {
        formData.append('file[]', inputEl.files.item(i));
        // console.log('Coba cek input data', inputEl.files.item(i));
      }
      // console.log('Coba cek Form data', formData.get('file[]'));
      // console.log('Coba count Form data', fileCount);
      this.purchasing.document = formData.get('file[]');
      // this.http
      //     .post('http://your.upload.url', formData)
      // do whatever you do...
      // subscribe to observable to listen for response
    }
  }

  public addListArray(items: PurchasingItem) {
    this.countQTYListArray(items).then(() => {
      if (
        this.qtypoTemp > items.orderItem.qtytopo &&
        items.orderItem.qtybookingpo === 0
      ) {
        this.toasterService.showToaster(
          'INFO',
          'INFORMASI',
          'QTY PP TO PO Sudah Tidak Bisa Ditambah Lagi'
        );
      } else if (
        this.qtypoTemp !== items.orderItem.qtybookingpo &&
        items.orderItem.qtybookingpo < items.orderItem.qty
      ) {
        if (items.orderItem.qtytopo - this.qtypoTemp === 0) {
          this.toasterService.showToaster(
            'INFO',
            'INFORMASI',
            'Item tidak bisa di pecah lagi karena QTY PO Kurang dari 1'
          );
        } else {
          const pp = new PurchasingItem();
          pp.noPP = items.noPP;
          pp.id = this.purchasingItems.length + 1;
          pp.orderItem = items.orderItem;
          pp.ppqty = items.ppqty;
          pp.uom = items.uom;
          pp.productcode = items.productcode;
          pp.productname = items.productname;
          pp.productname2 = items.productname2;
          pp.productname3 = items.productname3;
          pp.productname4 = items.productname4;
          pp.productname5 = items.productname5;
          pp.ppqty = items.ppqty;
          pp.dtsent = items.dtsent;
          // pp.qty = items.qty;// pp.ppqty - this.qtypoTemp;
          pp.sisaPP = items.sisaPP;
          pp.qty = items.orderItem.qtytopo - this.qtypoTemp;
          pp.qtypoTemp = this.qtypoTemp;
          pp.price = items.price;
          pp.totalprice = pp.ppqty * pp.price;
          pp.bidprice = items.bidprice;
          pp.qtyTemp = 0;
          // this.purchasingItems.push(pp);
          pp.divitions = items.divitions;
          pp.divreceipt = items.divreceipt;
          pp.dtsentpp = this.dateUtils.toDate(items.dtsentpp.toString());
          this.purchasingItems = [...this.purchasingItems, pp];
          // this.purchasingItems.push(pp);
          // this.calculateTotalPrice(pp);
          this.purchasingItems = this.setNoUrut(this.purchasingItems);
        }
      } else {
        this.toasterService.showToaster(
          'INFO',
          'INFORMASI',
          'Item tidak bisa di pecah lagi karena QTY PO sudah sama dengan QTY PP'
        );
      }
    });
  }

  countQTYListArray(items: PurchasingItem): Promise<void> {
    return new Promise<void>((resolveArr, reject) => {
      // console.log('PO CEK PROMISE = ', items);
      this.qtypoTemp = 0;
      if (
        items.idpurchasingitem !== '' &&
        items.idpurchasingitem !== undefined &&
        items.idpurchasingitem !== null
      ) {
        this.purchasingItems.forEach((e: PurchasingItem) => {
          if (e.orderItem.idPurOrdItem === items.orderItem.idPurOrdItem) {
            // console.log('cek qtytem = ', e.qtyTemp);
            // console.log('cek qty = ', e.qty);
            this.qtypoTemp += e.orderItem.qtytopo - e.orderItem.qtybookingpo;
            // console.log('cek qtyPOTEMP = ', this.qtypoTemp);
          }
        });
        resolveArr();
      }
      if (items.idpurchasingitem === undefined) {
        // console.log('PO CEK PROMISE MASUK IF= ', items);
        this.purchasingItems.forEach((e: PurchasingItem) => {
          if (e.orderItem.idPurOrdItem === items.orderItem.idPurOrdItem) {
            this.qtypoTemp += e.qty;
          }
        });
        resolveArr();
      }
    });
  }
  setRev() {
    this.isEditPO = false;
    // console.log(this.historyRev);
  }

  setNoUrut(pp: PurchasingItem[]): PurchasingItem[] {
    const aa = Array<PurchasingItem>();
    pp.forEach((e: PurchasingItem) => {
      e.nourut = aa.length + 1;
      aa.push(e);
    });
    return aa;
  }

  setFocus(nourut) {
    document.getElementById('field_dtsent_' + nourut).focus();
  }

  filterSupplierCodeSingle(event) {
    const query = event.query;
    this.masterSupplierService.getNewSupplier().then((newSupliers) => {
      this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
    });
  }

  // filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
  //     const filtered: any[] = [];
  //     for (let i = 0; i < newSupliers.length; i++) {
  //         const newSuplier = newSupliers[i];
  //         if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
  //             filtered.push(newSuplier);
  //         }
  //     }
  //     return filtered;
  // }

  filterCodeSupplier(
    query,
    newSupliers: MasterSupplier[],
    isEdit: Boolean = false
  ): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < newSupliers.length; i++) {
      const newSuplier = newSupliers[i];
      // console.log('ini data query == ', query.toLowerCase());
      if (
        newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0
      ) {
        filtered.push(newSuplier);
        if (isEdit === true) {
          this.newSuplier = newSuplier;
          this.splAddress = newSuplier.alamat1 + ' ' + newSuplier.alamat2;
        }
      }
    }
    return filtered;
  }

  filterSupplierCodeSingleLoad(event) {
    const query = event;
    this.masterSupplierService.getNewSupplier().then((newSupliers) => {
      this.filteredCodeSuppliers = this.filterCodeSupplier(
        query,
        newSupliers,
        true
      );
    });
  }
  openPdf(rowData: PurchasingItem) {
    this.loadingService.loadingStart();
    const filter_data = 'idPruchaseOrder:' + rowData.orderItem.idPurchaseOrder;
    this.reportUtilService
      .viewFileReader(
        process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf',
        { filterData: filter_data }
      )
      .subscribe(
        (response: any) => {
          const reader = new FileReader();
          const style =
            'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
          reader.readAsDataURL(response.blob());
          // tslint:disable-next-line: space-before-function-paren
          reader.onloadend = function () {
            const win = window.open();
            win.document.write(
              '<iframe src="' +
              reader.result +
              '" frameborder="0" style="' +
              style +
              '" allowfullscreen></iframe>'
            );
          };
          this.loadingService.loadingStop();
        },
        (err) => {
          this.loadingService.loadingStop();
        }
      );
    this.loadingService.loadingStop();
  }

  cekdtsent(rowData: PurchasingItem) {
    // console.log('cek dtsent pp = ', rowData.dtsentpp);
    // console.log('cek dtsent PO = ', rowData.dtsent);
    // this.purchasingItems.forEach(
    //     (e) => {
    if (
      rowData.idpurchasingitem === undefined ||
      rowData.idpurchasingitem === '' ||
      rowData.idpurchasingitem === null
    ) {
      if (rowData.dtsent.toUTCString() !== rowData.dtsentpp.toUTCString()) {
        if (rowData.dtsentnote === '') {
          document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
          this.toasterService.showToaster(
            'Info',
            'Tanggal Butuh',
            'Tanggal Butuh PP dan PO berbeda pada nomor item = ' +
            rowData.nourut
          );
          return;
        }
        if (rowData.dtsentnote !== '') {
          if (rowData.dtsentnote.trim() === '') {
            document
              .getElementById('field_dtsentnote_' + rowData.nourut)
              .focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' +
              rowData.nourut
            );
            return;
          }
        }
        if (rowData.dtsentnote === undefined) {
          document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
          this.toasterService.showToaster(
            'Info',
            'Tanggal Butuh',
            'Tanggal Butuh PP dan PO berbeda pada nomor item = ' +
            rowData.nourut
          );
          return;
        }
      }
    } else {
      const dtsentpp = this.dateUtils.toDate(rowData.dtsentpp);
      const dtsen =
        rowData.dtsent.getDay() +
        '' +
        rowData.dtsent.getMonth() +
        '' +
        rowData.dtsent.getFullYear();
      const dtsenpp =
        dtsentpp.getDay() +
        '' +
        dtsentpp.getMonth() +
        '' +
        dtsentpp.getFullYear();
      if (dtsen !== dtsenpp) {
        if (rowData.dtsentnote === '') {
          document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
          this.toasterService.showToaster(
            'Info',
            'Tanggal Butuh',
            'Tanggal Butuh PP dan PO berbeda pada nomor item = ' +
            rowData.nourut
          );
          return;
        }
        if (rowData.dtsentnote.trim() === '') {
          document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
          this.toasterService.showToaster(
            'Info',
            'Tanggal Butuh',
            'Tanggal Butuh PP dan PO berbeda pada nomor item = ' +
            rowData.nourut
          );
          return;
        }
        if (rowData.dtsentnote === undefined) {
          document.getElementById('field_dtsentnote_' + rowData.nourut).focus();
          this.toasterService.showToaster(
            'Info',
            'Tanggal Butuh',
            'Tanggal Butuh PP dan PO berbeda pada nomor item = ' +
            rowData.nourut
          );
          return;
        }
      }
    }
    //     }
    // )
  }

  cekItems(): Boolean {
    let hasil = true;
    this.purchasingItems.forEach((e) => {
      if (
        e.idpurchasingitem === undefined ||
        e.idpurchasingitem === '' ||
        e.idpurchasingitem === null
      ) {
        if (e.dtsent.toUTCString() !== e.dtsentpp.toUTCString()) {
          if (e.dtsentnote === '') {
            document.getElementById('field_dtsentnote_' + e.nourut).focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut
            );
            hasil = false;
            return hasil;
          }
          if (e.dtsentnote.trim() === '') {
            document.getElementById('field_dtsentnote_' + e.nourut).focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut
            );
            hasil = false;
            return hasil;
          }
          if (e.dtsentnote === undefined) {
            document.getElementById('field_dtsentnote_' + e.nourut).focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut
            );
            hasil = false;
            return hasil;
          }
        }
      } else {
        const dtsentpp = this.dateUtils.toDate(e.dtsentpp);
        const dtsen =
          e.dtsent.getDay() +
          '' +
          e.dtsent.getMonth() +
          '' +
          e.dtsent.getFullYear();
        const dtsenpp =
          dtsentpp.getDay() +
          '' +
          dtsentpp.getMonth() +
          '' +
          dtsentpp.getFullYear();
        if (dtsen !== dtsenpp) {
          if (e.dtsentnote === '') {
            document.getElementById('field_dtsentnote_' + e.nourut).focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut
            );
            hasil = false;
            return hasil;
          }
          if (e.dtsentnote.trim() === '') {
            document.getElementById('field_dtsentnote_' + e.nourut).focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut
            );
            hasil = false;
            return hasil;
          }
          if (e.dtsentnote === undefined) {
            document.getElementById('field_dtsentnote_' + e.nourut).focus();
            this.toasterService.showToaster(
              'Info',
              'Tanggal Butuh',
              'Tanggal Butuh PP dan PO berbeda pada nomor item = ' + e.nourut
            );
            hasil = false;
            return hasil;
          }
        }
      }
    });
    return hasil;
  }
  checkRetensi() {
    if (this.isretensi === true) {
      this.purchasing.isretensi = 1;
    }
    if (this.isretensi === false) {
      this.purchasing.isretensi = 0;
      this.purchasing.retensiday = 0;
    }
  }

  openPDF(nopp: any, idpurchaseorder: any) {
    let nopp1 = '';
    nopp1 = nopp.replace(/\//g, '-');
    this.purchasingService
      .getPP(nopp1, idpurchaseorder)
      .subscribe((purchasing) => {
        this.content = purchasing.content;
        this.contenttype = purchasing.contentContentType;
      });
    if (
      this.contenttype === null ||
      this.contenttype === '' ||
      this.contenttype === undefined
    ) {
      alert('Lampiran Tidak Ada');
    } else {
      const byteCharacters = atob(this.content);
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      const blobContent = new Blob([byteArray], { type: this.contenttype });
      const style =
        'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
      // const fileURL = URL.createObjectURL(blobContent);
      // window.open(fileURL, '_blank');
      const win = window.open(this.content);
      win.document.write(
        '<iframe id="printf" src="' +
        URL.createObjectURL(blobContent) +
        '" frameborder="0" style="' +
        style +
        '" allowfullscreen></iframe>'
      );
    }
  }

  addInput() {
    this.inputCount++;
    if (this.inputCount === 1) {
      this.hidden2 = false;
      this.hidden3 = true;
      this.hidden4 = true;
      this.hidden5 = true;
    } else if (this.inputCount === 2) {
      this.hidden2 = false;
      this.hidden3 = false;
      this.hidden4 = true;
      this.hidden5 = true;
    } else if (this.inputCount === 3) {
      this.hidden2 = false;
      this.hidden3 = false;
      this.hidden4 = false;
      this.hidden5 = true;
    } else if (this.inputCount === 4) {
      this.hidden2 = false;
      this.hidden3 = false;
      this.hidden4 = false;
      this.hidden5 = false;
    } else if (this.inputCount > 4) {
      alert('Kolom Nama Produk Hanya Bisa Menambahkan 4 Kolom Saja');
    }
  }

  minusInput() {
    this.inputCount--;
    if (this.inputCount === 3) {
      this.hidden2 = false;
      this.hidden3 = false;
      this.hidden4 = false;
      this.hidden5 = true;
    } else if (this.inputCount === 2) {
      this.hidden2 = false;
      this.hidden3 = false;
      this.hidden4 = true;
      this.hidden5 = true;
    } else if (this.inputCount === 1) {
      this.hidden2 = false;
      this.hidden3 = true;
      this.hidden4 = true;
      this.hidden5 = true;
    } else if (this.inputCount === 0) {
      this.hidden2 = true;
      this.hidden3 = true;
      this.hidden4 = true;
      this.hidden5 = true;
      alert('Kolom Tambahan Nama Produk Sudah Tidak Ada');
    }
  }

  openPDFPP(idpp: any) {
    this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
      if (
        pp.contentContentType === null ||
        pp.contentContentType === undefined ||
        pp.contentContentType === ''
      ) {
        alert('File Attachment Tidak Ada');
      } else {
        const content = pp.content;
        const contenttype = pp.contentContentType;

        const byteCharacters = atob(content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { type: contenttype });
        const style =
          'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const win = window.open(content);
        window.open(URL.createObjectURL(blobContent), 'blank');
      }
    });
  }
}
