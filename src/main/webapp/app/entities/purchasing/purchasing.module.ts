import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    DataListModule
} from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import { MpmPurchaseOrderModule } from '../purchase-order/purchase-order.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
    PurchasingService,
    PurchasingPopupService,
    PurchasingComponent,
    PurchasingDetailComponent,
    PurchasingDialogComponent,
    PurchasingPopupComponent,
    PurchasingDeletePopupComponent,
    PurchasingDeleteDialogComponent,
    purchasingRoute,
    purchasingPopupRoute,
    PurchasingResolvePagingParams,
    PurchasingNewComponent,
    PurchaseOrderAsLovComponent,
    PurchasOrderAsLovPopupComponent,
    PurchasingEditComponent,
    RegisterPPComponent,
    RegisterPPReceiveComponent,
    RegisterPPDetailComponent,
    FileUploadComponent,
    UnRegisterPPComponent,
    ApprovalPurchasingComponent,
    ApprovalRegisterPPDetailComponent,
    TabApprovalPurchasingComponent,
    TabApprovalRegisterPPDetailComponent,
    PriorityPPComponent,
    PoAsLovComponent,
    PoAsLovPopupComponent,
} from './';
import { PurchaseOrderPopupService } from '../purchase-order';
import { ApprovalPPTOPOComponent } from './approval-pp-to-po/approval-pp-to-po.component';
import { PurchasingExportComponent } from './purchasing-export/purchasing-export.component';
import { PurchasingItemExportComponent } from './purchasing-export/purchasing-item-export.component';
import { PurchasingPayComponent } from './purchasing-payment/purchasing-pay.component';
import { PurchasingPayDetailComponent } from './purchasing-payment/purchasing-pay-detail.component';
import { MpmPaymentModule } from '../payment/payment.module';
import { MpmInvoiceModule } from '../invoice/invoice.module';
import { PurchasingNewInjectComponent } from './purchasing-new-inject.component';

const ENTITY_STATES = [
    ...purchasingRoute,
    ...purchasingPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        MpmPurchaseOrderModule,
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        MpmPaymentModule,
        MpmInvoiceModule,
        DataListModule
    ],
    declarations: [
        PurchasingComponent,
        PurchasingDetailComponent,
        PurchasingDialogComponent,
        PurchasingDeleteDialogComponent,
        PurchasingPopupComponent,
        PurchasingDeletePopupComponent,
        PurchasingNewComponent,
        PurchaseOrderAsLovComponent,
        PurchasOrderAsLovPopupComponent,
        PurchasingEditComponent,
        RegisterPPComponent,
        RegisterPPReceiveComponent,
        RegisterPPDetailComponent,
        FileUploadComponent,
        UnRegisterPPComponent,
        ApprovalPPTOPOComponent,
        ApprovalPurchasingComponent,
        PurchasingExportComponent,
        PurchasingItemExportComponent,
        ApprovalRegisterPPDetailComponent,
        TabApprovalPurchasingComponent,
        TabApprovalRegisterPPDetailComponent,
        PriorityPPComponent,
        PoAsLovComponent,
        PoAsLovPopupComponent,
        PurchasingPayComponent,
        PurchasingPayDetailComponent,
        PurchasingNewInjectComponent
    ],
    entryComponents: [
        PurchasingComponent,
        PurchasingDialogComponent,
        PurchasingPopupComponent,
        PurchasingDeleteDialogComponent,
        PurchasingDeletePopupComponent,
        PurchasingNewComponent,
        PurchaseOrderAsLovComponent,
        PurchasOrderAsLovPopupComponent,
        PurchasingEditComponent,
        RegisterPPComponent,
        RegisterPPReceiveComponent,
        RegisterPPDetailComponent,
        FileUploadComponent,
        UnRegisterPPComponent,
        ApprovalPPTOPOComponent,
        ApprovalPurchasingComponent,
        PurchasingExportComponent,
        PurchasingItemExportComponent,
        ApprovalRegisterPPDetailComponent,
        TabApprovalPurchasingComponent,
        TabApprovalRegisterPPDetailComponent,
        PriorityPPComponent,
        PoAsLovComponent,
        PoAsLovPopupComponent,
        PurchasingPayComponent,
        PurchasingPayDetailComponent,
        PurchasingNewInjectComponent
    ],
    providers: [
        PurchasingService,
        PurchasingPopupService,
        PurchasingResolvePagingParams,
        PurchasingNewComponent,
        PurchasOrderAsLovPopupComponent,
        PurchaseOrderPopupService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPurchasingModule { }
