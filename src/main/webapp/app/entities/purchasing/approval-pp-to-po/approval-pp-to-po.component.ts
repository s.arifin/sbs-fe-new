import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper, UserService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { PurchaseOrder } from '../../purchase-order';
import { PurchasingService } from '..';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { RegisterPPDetail } from '../purchasing.model';
import { PurchaseOrderItem } from '../../purchase-order-item';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoryReason, HistoryReasonService } from '../../history-reason';

@Component({
    selector: 'jhi-approval-pp-to-po',
    templateUrl: './approval-pp-to-po.component.html'
})
export class ApprovalPPTOPOComponent implements OnInit, OnDestroy {

    data: PurchaseOrder;
    currentAccount: any;
    isSaving: boolean;
    registerDetail: RegisterPPDetail[];
    purchaseOrdersReceive: PurchaseOrder[];
    selected: PurchaseOrderItem[];
    error: any;
    success: any;
    subscription: Subscription;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    po: PurchaseOrder;
    newModalNote: boolean;
    idreg: any;
    noteUnregPP: string;
    pp: RegisterPPDetail;
    selectedSTS: Number = 0;
    noreg: any;
    ppStatus: Array<object> = [
        { label: 'Pilih', value: '0' },
        { label: 'Siap PO', value: '25' }
    ];
    isDisabled: Boolean = false;
    statuses: Array<object> = [
        { label: 'All', value: '0' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Siap PO', value: '25' },
        { label: 'Sudah Di Buat PO', value: '22' },
        { label: 'Siap BBM', value: '26' },
        { label: 'Siap PO Sebagian', value: '27' },
        { label: 'Sudah PO Sebagian', value: '28' }
    ];
    selectedStatus: string;
    modalPilihAdm: boolean;
    // selectedItems: PurchaseOrderItem[];
    purchaseOrderItems: PurchaseOrderItem[];
    totalDataItems: any;
    queryDataCount: any;
    modalPilihItemPO: boolean;
    isHeader: boolean;
    isItem: boolean;
    purchaseOrderItem: PurchaseOrderItem;
    modalRejectItem: boolean;
    admPOs: Account[];
    admPO: Account;
    pItems: PurchaseOrderItem;
    itemsDataPerPage: any;
    regDetail: RegisterPPDetail;
    modalCloseItem: boolean;
    modalReason: boolean;
    reasonPP: string;
    hr: HistoryReason;
    listHR: HistoryReason[];
    modalListHR: boolean;
    clicked: boolean;
    constructor(
        private purchasingService: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private reportUtilService: ReportUtilService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private toasterService: ToasterService,
        private userService: UserService,
        private historyReasonService: HistoryReasonService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<PurchaseOrderItem>();
        this.modalPilihAdm = false;
        // this.selectedItems = new Array<PurchaseOrderItem>();
        this.selectedStatus = '0';
        this.modalPilihItemPO = false;
        this.isHeader = false;
        this.isItem = false;
        this.purchaseOrderItem = new PurchaseOrderItem();
        this.modalRejectItem = false;
        this.admPOs = new Array<Account>();
        this.admPO = new Account();
        this.pItems = new PurchaseOrderItem();
        this.itemsDataPerPage = 0;
        this.regDetail = new RegisterPPDetail();
        this.modalCloseItem = false;
        this.modalReason = false;
        this.reasonPP = '';
        this.hr = new HistoryReason();
        this.listHR = new Array<HistoryReason>();
        this.modalListHR = false;
        this.clicked = false;
        this.idInternal = this.principal.getIdInternal();
    }

    loadAll() {
        this.selected = new Array<PurchaseOrder>();
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.purchasingService.getPPForReadyToPO({
                page: this.page,
                size: this.itemsPerPage,
                query: 'stype:0|nopp:' + this.currentSearch
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.purchasingService.getPPForReadyToPO({
            page: this.page,
            size: this.itemsPerPage,
            query: 'stype:0|nopp:'
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/approval-pp-to-po'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.getAdminPO()
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.registerDetail = data;
        this.clicked = false;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.clicked = false;
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    validateStatus() {
    }

    filter() {
        this.loadingService.loadingStart();
        this.purchasingService.getPPForReadyToPO({
            page: this.page,
            size: this.itemsPerPage,
            query: 'stype:' + this.selectedStatus + '|nopp:' + this.currentSearch
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    reset() {
        this.selectedStatus = '0';
        this.loadAll();
    }

    updatestatus(jenis: string) {
        if (this.selectedSTS === 0) {
            this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
        } else {
            if (jenis === 'header') {
                this.isHeader = true;
                this.isItem = false;
            }
            if (jenis === 'item') {
                this.isHeader = false;
                this.isItem = true;
            }
            console.log('jenis nya == ', jenis);
            this.modalPilihAdm = true;
        }
    }
    openPDF(idpp: any) {
        this.purchasingService.getPP1(idpp).subscribe((purchasing) => {
            const content = purchasing.content;
            const contenttype = purchasing.contentContentType;

            const byteCharacters = atob(content);
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);

            const blobContent = new Blob([byteArray], { 'type': contenttype });
            const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
            // const win = window.open(content);
            window.open(URL.createObjectURL(blobContent), '_blank');
        });

        // win.document.write('<iframe id='printf' src='' + URL.createObjectURL(blobContent) + '' frameborder='0' style='' + style + '' allowfullscreen></iframe>');
    }
    print(id: any) {
        const filter_data = 'idPruchaseOrder:' + id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf', { filterData: filter_data });
    }

    setAdminPP(admpp: string) {
        this.loadingService.loadingStart();
        this.clicked = true;
        if (this.isHeader === true) {
            this.purchasingService.mappingAdmPP(this.selected, admpp)
                .subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<PurchaseOrder>();
                        this.modalPilihAdm = false;
                        this.selected = new Array<PurchaseOrderItem>();
                        this.selectedStatus = '0';
                        this.modalPilihItemPO = false;
                        this.isHeader = false;
                        this.isItem = false;
                        this.selectedSTS = 0;
                        this.clicked = false;
                    }
                );
        }
        if (this.isItem === true) {
            // this.purchaseOrderItems.forEach(
            //     (e) => {
            //         if (e.isSelected === true) {
            //             this.selectedItems = [...this.selectedItems, e];
            //         }
            //     }
            // )
            this.purchasingService.mappingAdmPPItems(this.selected, admpp)
                .subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<PurchaseOrder>();
                        this.modalPilihAdm = false;
                        this.selected = new Array<PurchaseOrderItem>();
                        this.selectedStatus = '0';
                        this.modalPilihItemPO = false;
                        this.isHeader = false;
                        this.isItem = false;
                        this.selectedSTS = 0;
                        this.clicked = false;
                    }
                );
        }
    }
    openDetail(data: RegisterPPDetail) {
        this.regDetail = data;
        this.loadingService.loadingStart();
        this.purchasingService.getItemPPForReadyToPO(data.idpurchaseorder).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccessItems(res.json, res.headers);
                this.modalPilihItemPO = true;
            }
        )
    }
    private onSuccessItems(data, headers) {
        this.totalDataItems = headers.get('X-Total-Count');
        this.itemsDataPerPage = this.totalDataItems;
        this.queryDataCount = this.totalDataItems;
        this.purchaseOrderItems = data;
        this.loadingService.loadingStop();
    }
    rejectItem(data: PurchaseOrderItem) {
        this.purchaseOrderItem = data;
        this.modalRejectItem = true;
    }
    reject() {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Reject (Buat PP Kembali) item ini ??!!',
            accept: () => {
                this.loadingService.loadingStart();
                this.purchasingService.changeStatusPPItem(this.purchaseOrderItem, 31)
                    .subscribe(
                        (res) => {
                            this.modalRejectItem = false;
                            this.modalPilihItemPO = false;
                            this.purchaseOrderItem = new PurchaseOrderItem();
                            this.router.navigate(['/approval-pp-to-po']);
                            this.loadAll();
                        }
                    )
            }
        });
    }

    getAdminPO() {
        this.userService.getByRole('ROLE_ADMPO')
            .subscribe(
                (res: ResponseWrapper) => {
                    this.admPOs = res.json;
                    if (!this.idInternal.includes('SKG') && !this.idInternal.includes('SMK')) {
                        this.admPOs = this.admPOs.filter((pic) => (!pic.login.includes('SKG') && !pic.login.includes('SKG')));
                    } else if (this.idInternal.includes('SKG')) {
                        this.admPOs = this.admPOs.filter((pic) => (pic.login.includes('SKG')));
                    } else {
                        this.admPOs = this.admPOs.filter((pic) => (pic.login.includes('SMK')));
                    }
                });
    }

    loadLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.openDetail(this.regDetail);
    }

    closeItem(data: PurchaseOrderItem) {
        this.purchaseOrderItem = data;
        this.modalCloseItem = true;
    }
    close() {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Close item ini ??!!',
            accept: () => {
                this.loadingService.loadingStart();
                this.purchasingService.changeStatusPPItem(this.purchaseOrderItem, 32)
                    .subscribe(
                        (res) => {
                            this.modalCloseItem = false;
                            this.modalPilihItemPO = false;
                            this.purchaseOrderItem = new PurchaseOrderItem();
                            this.router.navigate(['/approval-pp-to-po']);
                            this.loadAll();
                        }
                    )
            }
        });
    }
    reasonpp(data: RegisterPPDetail) {
        this.modalReason = true;
        this.hr = new HistoryReason();
        this.hr.idforeign = data.idpurchaseorder;
        if (this.hr.descriptions === undefined) {
            this.hr.descriptions = '';
        }
        // this.hr.descriptions = this.reasonPP;
    }
    saveReasonpp() {
        this.historyReasonService.create(this.hr)
            .subscribe(
                (res) => {
                    this.modalReason = false;
                    this.reasonPP = '';
                }
            );
    }

    cekReasonpp(data: RegisterPPDetail) {
        this.loadingService.loadingStart();
        this.historyReasonService.getByForeign(data.idpurchaseorder)
            .subscribe(
                (res: ResponseWrapper) => {
                    this.listHR = res.json;
                    this.loadingService.loadingStop();
                    this.modalListHR = true;
                },
                (err) => {
                    this.loadingService.loadingStop();
                }
            )
    }
    serahprc(data: PurchaseOrderItem) {
        this.purchaseOrderItem = data;
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Serah Purchasing item ini ??!!',
            accept: () => {
                this.loadingService.loadingStart();
                this.purchasingService.changeStatusPPItem(this.purchaseOrderItem, 23)
                    .subscribe(
                        (res) => {
                            this.modalCloseItem = false;
                            this.modalPilihItemPO = false;
                            this.purchaseOrderItem = new PurchaseOrderItem();
                            this.router.navigate(['/approval-pp-to-po']);
                            this.loadAll();
                        }
                    )
            }
        });
    }
    // addItem() {
    //     this.purchaseOrderItems.forEach(
    //         (e) => {
    //             if (e.isSelected === true) {
    //                 this.selectedItems = [...this.selectedItems, e];
    //             }
    //         }
    //     )
    // }
    cekitem(rowData: PurchaseOrderItem) {
        console.log(rowData);
    }
}
