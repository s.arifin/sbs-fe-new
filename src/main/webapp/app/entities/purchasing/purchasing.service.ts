import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { PurchasingItem, MappingUserPO } from '../purchasing-item';
import { PurchaseOrder } from '../purchase-order';
import { createRequestOptionPurchasingPTO } from '../purchasing/purchasing-parameter.util'
import { PurchaseOrderItem } from '../purchase-order-item';
import { HistoryRevisi, Purchasing, RegisterPPDetail } from './purchasing.model';

@Injectable()
export class PurchasingService {
    protected itemValues: Purchasing[] = new Array<Purchasing>();
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/purchasings';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/purchasings';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(purchasing: Purchasing): Observable<Purchasing> {
        const copy = this.convert(purchasing);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createPO(purchasing: Purchasing, items: PurchasingItem[]): Observable<Purchasing> {
        const purchasings = this.convert(purchasing);
        const purchasingItems = this.convertItems(items);
        return this.http.post(this.resourceUrl, { purchasings, purchasingItems }).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Purchasing> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getPP(nopp: any, idpurchaseorder: any): Observable<PurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/${nopp}/${idpurchaseorder}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getPP1(idpurchaseorder: any): Observable<PurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/find/${idpurchaseorder}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    checkIsUsePO(req?: any): Observable<MappingUserPO> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/mapping-user-po/validate', options)
            .map((res: Response) => {
                const jsonResponse = res.json();
                return jsonResponse;
            });
    }

    createIsUsePO(purchaseorder: PurchaseOrder): Observable<Purchasing> {
        const purchasings = this.convertPO(purchaseorder);
        return this.http.post(this.resourceUrl + '/mapping-user-po/create', purchaseorder).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtcreated = this.dateUtils
            .convertDateTimeFromServer(entity.dtcreated);
        entity.dtmodified = this.dateUtils
            .convertDateTimeFromServer(entity.dtmodified);
    }

    private convert(purchasing: Purchasing): Purchasing {
        const copy: Purchasing = Object.assign({}, purchasing);

        // copy.dtcreated = this.dateUtils.toDate(purchasing.dtcreated);

        // copy.dtmodified = this.dateUtils.toDate(purchasing.dtmodified);
        return copy;
    }

    private convertUpdate(purchasing: Purchasing): Purchasing {
        const copy: Purchasing = Object.assign({}, purchasing);

        // copy.dtcreated = this.dateUtils.toDate(purchasing.dtcreated);

        // copy.dtmodified = this.dateUtils.toDate(purchasing.dtmodified);
        return copy;
    }

    private convertItems(items: PurchasingItem[]): PurchasingItem[] {
        const copy: PurchasingItem[] = Object.assign([], items);
        // copy.dtcreated = this.dateUtils.toDate(purchasing.dtcreated);
        // copy.dtmodified = this.dateUtils.toDate(purchasing.dtmodified);
        return copy;
    }

    private convertPO(purchaseorder: PurchaseOrder): Purchasing {
        const copy: PurchaseOrder = Object.assign({}, purchaseorder);
        return copy;
    }

    private convertRegdtl(regDetail: RegisterPPDetail): RegisterPPDetail {
        const copy: PurchaseOrder = Object.assign({}, regDetail);
        return copy;
    }

    private convertPOItem(purchasingItem: PurchasingItem): PurchasingItem {
        const copy: PurchasingItem = Object.assign({}, purchasingItem);
        return copy;
    }

    private convertPPItemARR(Item: PurchaseOrderItem[]): PurchaseOrderItem[] {
        const copy: PurchaseOrderItem[] = Object.assign([], Item);
        return copy;
    }

    private convertPOArr(purchaseorder: PurchaseOrder[]): PurchaseOrder[] {
        const copy: PurchaseOrder[] = Object.assign([], purchaseorder);
        return copy;
    }

    private convertRegDetail(registerDetail: RegisterPPDetail[]): RegisterPPDetail[] {
        const copy: RegisterPPDetail[] = Object.assign([], registerDetail);
        return copy;
    }

    getAllValuta() {
        return this.http.get(this.resourceUrl + '/all-valuta').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    uomByProduk(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/uom-by-produk', options)
            .map((res: Response) => this.convertResponse(res));
    }

    LOVqueryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov-filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryLOV(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov', options)
            .map((res: Response) => this.convertResponse(res));
    }

    updatePO(purchasing: Purchasing, items: PurchasingItem[], historyRevision: HistoryRevisi): Observable<Purchasing> {
        const purchasings = this.convertUpdate(purchasing);
        const purchasingItems = this.convertItems(items);
        const history_revision = this.convertHistoryRevisi(historyRevision);
        const obj: any = {
            'purchasings': purchasings,
            'purchasingItems': purchasingItems,
            'history_revision': history_revision
        }
        const output: JSON = <JSON>obj;
        console.log(output);
        // return this.http.put(this.resourceUrl + '/update-po-detail', { purchasings, purchasingItems, history_revision }).map((res: Response) => {
        return this.http.put(this.resourceUrl + '/update-po-detail', output).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    update(purchasing: Purchasing): Observable<Purchasing> {
        const purchasings = this.convertUpdate(purchasing);
        return this.http.put(this.resourceUrl, purchasings).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getManagerPurchasing() {
        return this.http.get(this.resourceUrl + '/list-manager').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    closePO(purchasing: Purchasing): Observable<Purchasing> {
        const purchasings = this.convertUpdate(purchasing);
        return this.http.put(this.resourceUrl + '/close-purchasing', { purchasings }).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getPPForRegister(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/register-pp', options)
            .map((res: Response) => this.convertResponse(res));
    }

    createRegisterPP(purcaseOrder: PurchaseOrder[]): Observable<PurchaseOrder[]> {
        const copy = this.convertPOArr(purcaseOrder);
        console.log('coba cek ahh data PP nya = ', copy);
        return this.http.post(this.resourceUrl + '/register-pp', copy).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    unRegisterPP(registerDetail: RegisterPPDetail): Observable<RegisterPPDetail> {
        const copy = this.convertRegdtl(registerDetail);
        return this.http.post(this.resourceUrl + '/unregister-pp', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getRegisterDetail(id: any, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/register-pp-detail/' + id, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(purchasing: Purchasing, id: Number): Observable<ResponseWrapper> {
        const copy = this.convertUpdate(purchasing);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }
    // getValidate(req?: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(req);
    //     return this.http.post(this.resourceUrl + '/validate-qty', options)
    //         .map((res: Response) => this.convertResponse(res));
    // }
    // getValidate(item: PurchasingItem): Observable<ResponseWrapper> {
    //     const copy = this.convertPOItem(item);
    //     return this.http.post(`${this.resourceUrl}/validate-qty`, copy)
    //         .map((res: Response) => this.convertResponse(res));
    // }

    getValidate(item: PurchasingItem): Observable<ResponseWrapper> {
        const copy = this.convertPO(item);
        return this.http.post(this.resourceUrl + '/validate-qty', copy).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    changeStatusPPReg(registerDetail: RegisterPPDetail[], id: Number): Observable<ResponseWrapper> {
        const copy = this.convertRegDetail(registerDetail);
        return this.http.post(`${this.resourceUrl}/update-statuspp-register/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    approveEditPO(purchasing: Purchasing[]): Observable<ResponseWrapper> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/approve-edit', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convertPurchasings(purchasing: Purchasing[]): Purchasing[] {
        const copy: PurchaseOrder[] = Object.assign([], purchasing);
        return copy;
    }

    private convertHistoryRevisi(rev: HistoryRevisi): HistoryRevisi {
        const copy: HistoryRevisi = Object.assign({}, rev);
        // copy.dtcreated = this.dateUtils.toDate(purchasing.dtcreated);
        // copy.dtmodified = this.dateUtils.toDate(purchasing.dtmodified);
        return copy;
    }

    getPObyIdPP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getpp-po', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPObyIdPPItem(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getpp-po-item', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryFilterPayment(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOptionPurchasingPTO(req);
        return this.http.get(this.resourceUrl + '/filterPayment', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPPForReadyToPO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval-pp-tp-po', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPoProductCode() {
        return this.http.get(this.resourceUrl + '/po-product-code').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getPoProduct(divisi: string) {
        return this.http.get(this.resourceUrl + `/po-product/${divisi}`).toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    queryPOBBM(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-po-bbm', options)
            .map((res: Response) => this.convertResponse(res));
    }

    mappingAdmPP(purchaseOrders: PurchaseOrder[], admpp: string): Observable<ResponseWrapper> {
        const copy = this.convertPOArr(purchaseOrders);
        return this.http.post(this.resourceUrl + '/set-priority-purchasing/' + admpp, copy).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    mappingAdmPPItems(purchaseOrderItems: PurchaseOrderItem[], admpp: string): Observable<ResponseWrapper> {
        const copy = this.convertPPItemARR(purchaseOrderItems);
        return this.http.post(this.resourceUrl + '/set-item-priority-purchasing/' + admpp, copy).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    getItemPPForReadyToPO(idPurchaseOrder: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/get-item-ready-po/' + idPurchaseOrder)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatusPPItem(purchaseOrderItem: PurchaseOrderItem, idstatus: Number): Observable<ResponseWrapper> {
        const convert: PurchaseOrderItem = Object.assign({}, purchaseOrderItem);
        return this.http.post(`${this.resourceUrl}/set-status-item-pp/${idstatus}`, convert)
            .map((res: Response) => this.convertResponse(res));
    }

    queryApproval(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryExport(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/export', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryExportItem(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/export-item', options)
            .map((res: Response) => this.convertResponse(res));
    }

    cekStatusPO(idpo: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/check-status/${idpo}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getApprovalPO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval-purchasing', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAppPPManagement(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval-register-pp-detail', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryPriorPP(page: any, size: any, query: any, pic: any, admin: any): Observable<ResponseWrapper> {
        // const options = createRequestOption(req);
        return this.http.get(process.env.API_C_URL + '/api/priority-pps?page=' + page + '&size=' + size + '&query=' + query + '&pic=' + pic + '&admin=' + admin)
            .map((res: Response) => this.convertResponse(res));
    }

    pushItems(data: PurchaseOrder[]) {
        this.itemValues = new Array<PurchaseOrder>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
    queryPay(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/payment', options)
            .map((res: Response) => this.convertResponse(res));
    }
    changeStatusPP(purchaseOrder: PurchaseOrder[], idstatustype: Number): Observable<ResponseWrapper> {
        const copy = this.convertPOArr(purchaseOrder);
        return this.http.post(`${this.resourceUrl}/update-status-pp/${idstatustype}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }
    queryLOVPaymnet(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov-po', options)
            .map((res: Response) => this.convertResponse(res));
    }
    findByNo(req: any): Observable<Purchasing> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-no/', options).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    setKurs(purchasing: Purchasing): Observable<Purchasing> {
        const copy = this.convert(purchasing);
        return this.http.put(this.resourceUrl + '/set-kurs', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    setMailPO(purchasing: Purchasing[]): Observable<Purchasing[]> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/set-mail-po', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryGetNotes(req?: any): Observable<PurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-notes', options).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    createPOInject(purchasing: Purchasing, items: PurchasingItem[]): Observable<Purchasing> {
        const purchasings = this.convert(purchasing);
        const purchasingItems = this.convertItems(items);
        return this.http.post(this.resourceUrl + '/inject', { purchasings, purchasingItems }).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getPPN(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getPPN', options)
            .map((res: Response) => this.convertResponse(res));
    }

}
