import { PurchasingItem } from '../purchasing-item';
import { BaseEntity } from './../../shared';

export class Purchasing implements BaseEntity {
    constructor(
        public id?: number,
        public idpurchasing?: any,
        public nopurchasing?: string,
        public supliercode?: string,
        public sentto?: string,
        public paymentterms?: string,
        public revision?: number,
        public tenor?: number,
        public dtcreated?: any,
        public createdby?: string,
        public modifiedby?: string,
        public dtmodified?: any,
        public statuses?: Statuses,
        public suplier?: Suplier,
        public valuta?: string,
        public tolerancenumsend?: number,
        public totalprice?: number,
        public isdisc?: number,
        public disc?: number,
        public isppn?: number,
        public ppn?: number,
        public totaldisc?: number,
        public notes?: string,
        public notes2?: string,
        public price?: number,
        public otorisasi?: string,
        public otorisasi2?: string,
        public printcount?: number,
        public document?: any,
        public idStatusType?: number,
        public DescStatus?: string,
        public paynotes?: string,
        public revision_internal?: number,
        public isedit?: number,
        public historyrevisi?: HistoryRevisi,
        public ongkir?: number,
        public ongkirpp?: number,
        public totalongkir?: number,
        public appwithpass?: number,
        public dtaproved?: Date,
        public items?: PurchasingItem,
        public ketnotapp?: string,
        public uangmuka?: number,
        public downpayment?: number,
        public payment?: number,
        public receiptno?: string,
        public topay?: number,
        public kurs?: number,
        public ppjk?: number,
        public kasnegara?: number,
        public isretensi?: number,
        public retensiday?: number,
        public isemail?: string,
        public nomppn?: number,
        public cancelnote?: string,
        public nm_supplier?: string,
        public ispph?: number,
        public idpay?: any,
    ) {
        this.items = new PurchasingItem();
    }
}

export class HistoryRevisi {
    constructor(
        public id?: number,
        public revision?: number,
        public dtrevision?: Date,
        public idpurchasing?: any,
        public tipe?: string,
        public notes?: string,
    ) { }
}

export class Suplier {
    constructor(
        public almt_supplier1?: string,
        public almt_supplier2?: string,
        public fax?: string,
        public idSupplier?: string,
        public jsup?: string,
        public kd_supplier?: string,
        public nm_supplier?: string,
        public person1?: string,
        public person2?: string,
        public telp?: string
    ) {
    }
}

export class Statuses {
    constructor(
        public author?: any,
        public dtfrom?: Date,
        public dtthru?: Date,
        public idpurchasing?: any,
        public idstatus?: any,
        public statustype?: number
    ) {
    }
}

export class Valuta {
    constructor(
        public idvaluta?: any,
        public valutacode?: string,
        public description?: string
    ) {
    }
}

export class Kabag {
    constructor(
        public label?: any,
        public value?: any
    ) {
    }
}

export class RegisterPP {
    constructor(
        public idregister?: any,
        public noregis?: string,
        public idpurchaseorder?: any,
        public dtcreate?: Date,
        public createdby?: Date,
        public note?: string,
        public isreceived?: number,
        public tipepp?: string,
        public pic?: string,
    ) {
    }
}

export class RegisterPPDetail {
    constructor(
        public idregisdetail?: any,
        public idregis?: any,
        public idpurchaseorder?: any,
        public pic?: string,
        public isregister?: number,
        public notes?: string,
        public nopurchaseorder?: string,
        public dtorder?: Date,
        public createdby?: string,
        public divition?: string,
        public statusdesc?: string,
        public idstatustype?: number,
        public contenttype?: any,
        public content?: any,
        public noregister?: string,
        public dtunreg?: Date,
        public nopurchasing?: string,
        public isApprove?: number,
        public ketnotapp?: string,
        public komen?: string,
    ) {
    }
}

export class PurchasingExport {
    constructor(
        // public id?: any,
        public Tahun?: any,
        public noorder?: string,
        public kdsupp?: string,
        public tglorder?: any,
        public alamat1?: string,
        public alamat2?: string,
        public alamat3?: string,
        public lmkredit?: number,
        public jumlah?: number,
        public kenappn?: string,
        public ppn?: number,
        public pembayaran?: string,
        public disc_ps?: number,
        public disc_rp?: number,
        public ket?: string,
        public nopp?: string,
        public valuta?: string,
        public batal?: string,
        public initial?: string,
        public kdkirim?: string,
        public toleran?: number,
        public revisi?: string,
    ) {
    }
}

export class PurchasingItemExport {
    constructor(
        public tahun?: any,
        public noorder?: string,
        public noitem?: number,
        public nmstock1?: string,
        public nmstock2?: string,
        public nmstock3?: string,
        public nmstock4?: string,
        public satuan?: string,
        public qtyminta?: number,
        public qtykirim?: number,
        public qtyadjust?: number,
        public harga?: number,
        public jumlah?: number,
        public tglkirim?: string,
        public tglorder?: string,
        public nopp?: string,
        public kdbahan?: string,
        public nmbahan1?: string,
        public nmbahan2?: string,
        public nmbahan3?: string,
        public nmbahan4?: string,
    ) {

    }
}
export class MappingPriorityPPVM {
    constructor(
        public idmapprior?: any,
        public idpurchaseorder?: any,
        public idpurchaseorderitem?: any,
        public admpp?: string,
        public dtcreate?: Date,
        public noprior?: number,
        public nopurchaseorder?: string,
        public codeproduk?: string,
        public produkname?: string,
        public pic?: string,
    ) {

    }
}
export class ItemProduct {
    constructor(
        public kdBahan?: string,
        public nmBahan?: string,
    ) {

    }
}

export class PurchasingHistory {
    constructor(
        public idpurchasing?: any,
        public nopurchasing?: string,
        public dtcreated?: Date,
        public kd_suppnew?: string,
        public nm_supplier?: string,
        public productcode?: string,
        public productname?: string,
        public dtsent?: Date,
        public qty?: number,
        public uom?: string,
        public price?: number,
        public disc?: number,
        public totaldisc?: number,
        public ppn?: number,
    ) {

    }
}
