import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Purchasing, Kabag } from './purchasing.model';
import { PurchasingService } from './purchasing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { PurchasingItemService } from '../purchasing-item';
import { LoadingService } from '../../layouts';
import { MasterSupplierService, MasterSupplier } from '../master-supplier';
import { PurchasingFilter } from './purchasing-filter.model';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'jhi-purchasing',
    templateUrl: './purchasing.component.html'
})
export class PurchasingComponent implements OnInit, OnDestroy {

    currentAccount: any;
    purchasings: Purchasing[];
    purchasing: Purchasing;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    newModal: boolean;
    kabag: Array<Object> = new Array<Object>();
    selectedKabag: string;
    idPrint: any;
    sortF: any;
    pdfSrc: any;
    isview: boolean;
    isEdit: boolean;
    noteCLosePO: string;
    noteCancelPO: string;
    newModalNote: boolean;
    newModalNote1: boolean;
    modalPilihCeteakn: boolean;
    selected: Purchasing[];
    user: any;
    pics: Array<object> = [
        { label: 'all', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    isEmail: Array<object> = [
        { label: 'all', value: '' },
        { label: 'Sudah', value: 'Sudah' },
        { label: 'Belum', value: 'Belum' }
    ];
    isStatus: Array<object> = [
        { label: 'Pilih Status PO', value: '' },
        { label: 'New', value: '10' },
        { label: 'Request Approve', value: '29' },
        { label: 'Siap BBM', value: '26' },
        { label: 'Approved', value: '11' }
    ];
    // field filter
    dtfrom: Date;
    dtthru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    newSuplier: MasterSupplier;
    kdSPL: string;
    isFilter: boolean;
    selectedPIC: string;
    dtFilter: PurchasingFilter;
    // field filter
    loginName: string;
    pilihBahasa: string;
    isTnpaHrg: boolean;
    pdfBlob: any;
    isReqApp: boolean;
    newModalNoteNotAPP: boolean;
    ketNotApp: string;
    selectedMail: string;
    selectedStatus: string;
    role: any;
    idInternal: any;
    idPlant: any;
    constructor(
        private purchasingService: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private reportUtilService: ReportUtilService,
        private purchasingItemService: PurchasingItemService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Purchasing>();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdSPL = '';
        this.isFilter = false;
        this.selectedPIC = '';
        this.dtFilter = new PurchasingFilter();
        this.loginName = '';
        this.pilihBahasa = '';
        this.isTnpaHrg = false;
        this.isReqApp = false;
        this.newModalNoteNotAPP = false;
        this.ketNotApp = '';
        this.selectedMail = '';
        this.selectedStatus = '';
        this.idInternal = this.principal.getIdInternal();
        if (this.idInternal.includes('SMK')) {
            this.idPlant = 'SMK';
        } else if (this.idInternal.includes('SKG')) {
            this.idPlant = 'SKG'
        } else {
            this.idPlant = 'SBS';
        }
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter === false && this.currentSearch !== '') {
            this.purchasingService.query({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort(),
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        if (this.isFilter === true && this.currentSearch === '') {
            this.dtFilter.pic = this.selectedPIC;
            this.dtFilter.from = this.dtfrom;
            this.dtFilter.thru = this.dtthru;
            this.dtFilter.kdspl = this.newSuplier.kd_suppnew;
            this.purchasingService.queryFilter({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                pic: this.selectedPIC,
                from: this.dtFilter.from.toJSON(),
                thru: this.dtFilter.thru.toJSON(),
                kdspl: this.newSuplier.kd_suppnew,
                isemail: this.selectedMail,
                isstatus: this.selectedStatus,
                query: this.currentSearch,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        if (this.isFilter === true && this.currentSearch !== '') {
            this.dtFilter.pic = this.selectedPIC;
            this.dtFilter.from = this.dtfrom;
            this.dtFilter.thru = this.dtthru;
            this.dtFilter.kdspl = this.newSuplier.kd_suppnew;
            this.purchasingService.queryFilter({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                pic: this.selectedPIC,
                from: this.dtFilter.from.toJSON(),
                thru: this.dtFilter.thru.toJSON(),
                kdspl: this.newSuplier.kd_suppnew,
                isemail: this.selectedMail,
                isstatus: this.selectedStatus,
                query: this.currentSearch,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.purchasingService.query({
            idInternal: this.principal.getIdInternal(),
            page: this.page,
            size: this.itemsPerPage,
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.user = this.principal.getUserLogin();
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/purchasing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        // this.router.navigate(['/purchasing', {
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/purchasing', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    ngOnInit() {
        this.newModalNote = false;
        this.isview = false;
        this.kabag = [
            { label: 'Pilih Kabag', value: null },
            { label: 'Evina HU', value: 'Evina HU' },
            { label: 'Jettir G', value: 'Jettir G' },
            { label: 'Selvyati', value: 'Selvyati' }
        ];
        this.idPrint = '';
        this.newModal = false;
        this.selectedKabag = '';
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            this.loginName = account.login;
            console.log('Inin data account = ', account);
            this.isCanEdit(account);
        });
        this.registerChangeInPurchasings();
        this.purchasing = new Purchasing();
        this.modalPilihCeteakn = false;
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Purchasing) {
        return item.id;
    }
    registerChangeInPurchasings() {
        this.eventSubscriber = this.eventManager.subscribe('purchasingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.purchasings = data;
        console.log(data);
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    // public cancelPO(data: Purchasing) {
    //     this.confirmationService.confirm({
    //         header: 'Information',
    //         message: 'Apakah Yakin Untuk Cancel PO ' + data.nopurchasing,
    //         accept: () => {
    //             console.log('data PO yang akan di cancel == ', data);
    //             this.purchasingService.changeStatus(data, 13)
    //                 .subscribe(
    //                     (res) => this.loadAll()
    //                 );
    //         }
    //     });
    // }

    public printPO(purchasing: Purchasing) {
        this.idPrint = purchasing.idpurchasing;
        this.purchasing = purchasing;
        this.modalPilihCeteakn = true;
        // this.pdfSrc = '';
        // const filter_data = 'idpurchaseorder:' + purchasing.idpurchasing;
        // this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
        //     subscribe((response: any) => {
        //         const reader = new FileReader();
        //         reader.readAsDataURL(response.blob());
        //         this.isview = true;
        //         // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
        //         reader.onloadend = (e) => {
        //             this.pdfSrc = reader.result;
        //         }
        //     });
    }
    public pilihPrintPO(bahasa: any) {
        this.pilihBahasa = bahasa
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        this.pdfBlob = '';
        const filter_data = 'idpurchaseorder:' + this.purchasing.idpurchasing;
        if (bahasa === 'indonesia') {
            if (this.purchasing.sentto === 'SKG Pluit' || this.purchasing.sentto === 'SKG Tambun') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.sentto === 'SMK') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
        if (bahasa === 'inggris') {
            if (this.purchasing.sentto === 'SKG Pluit' || this.purchasing.sentto === 'SKG Tambun') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.sentto === 'SMK') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggris/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
    }

    public editPO(purchasing: Purchasing) {
        this.purchasingService.cekStatusPO(purchasing.idpurchasing).subscribe(
            (res) => {
                if (res.statustype === 11) {
                    alert('PO Sudah Diapprove Silahkan Meminta Otorisasi Atasan untuk edit ');
                } else {
                    if (purchasing.createdby !== this.user && purchasing.otorisasi !== this.user && purchasing.otorisasi2 !== this.user) {
                        alert('Pembuat PO atau Otoritas PO berbeda');
                    } else if (purchasing.printcount === 0 && purchasing.isedit === 0) {
                        this.router.navigate(['../purchasing-edit/' + purchasing.idpurchasing]);
                    } else if (purchasing.printcount === 1 && purchasing.isedit === 1) {
                        alert('Silahkan Meminta Otorisasi Atasan untuk edit');
                    } else if (purchasing.printcount === 1 && purchasing.isedit === 0 && purchasing.revision >= 0 && purchasing.revision_internal >= 0) {
                        this.router.navigate(['../purchasing-edit/' + purchasing.idpurchasing]);
                    } else if (purchasing.revision === 3 || purchasing.revision_internal === 3) {
                        alert('Silahkan Meminta Otorisasi Atasan untuk edit');
                    } else {
                        alert('Silahkan Meminta Otorisasi Atasan untuk edit');
                    }
                }
            }
        )
    }
    public printFromModal() {
        this.pdfSrc = '';
        this.pdfBlob = '';
        this.newModal = false;
        const filter_data = 'idpurchaseorder:' + this.idPrint + '|kabag:' + this.selectedKabag;
        this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusat/pdf', { filterData: filter_data }).
            subscribe((response: any) => {
                const reader = new FileReader();
                this.pdfBlob = response.blob();
                reader.readAsDataURL(response.blob());
                this.isview = true;
                // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                reader.onloadend = (e) => {
                    this.pdfSrc = reader.result;
                }
            });
    }

    changeSort(event) {
        if (!event.order) {
            this.sortF = 'year';
        } else {
            this.sortF = event.field;
        }
    }

    backMainPage() {
        this.isview = false;
    }

    print() {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Cetak Dokumen ? \r\n  Setelah Print Hanya Bisa Direvisi Sesuai Otoritas.!!',
            accept: () => {
                this.purchasing.printcount = 1;
                this.purchasing.isedit = 1;
                this.purchasingService.update(this.purchasing).subscribe((res) => console.log('update sukses'));
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                const win = window.open(this.pdfSrc);
                win.document.write('<iframe id="printf" src="' + this.pdfSrc + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
            }
        });
    }

    printtanpaharga() {
        this.isTnpaHrg = true;
        const bahasa = this.pilihBahasa;
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        const filter_data = 'idpurchaseorder:' + this.purchasing.idpurchasing;
        if (bahasa === 'indonesia') {
            if (this.purchasing.sentto === 'SKG Pluit' || this.purchasing.sentto === 'SKG Tambun') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHargaSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.sentto === 'SMK') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHargaSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOPusatTanpaHarga/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
        if (bahasa === 'inggris') {
            if (this.purchasing.sentto === 'SKG Pluit' || this.purchasing.sentto === 'SKG Tambun') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHargaSKG/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else if (this.purchasing.sentto === 'SMK') {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHargaSMK/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            } else {
                this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PrintPOInggrisTanpaHarga/pdf', { filterData: filter_data }).
                    subscribe((response: any) => {
                        const reader = new FileReader();
                        this.pdfBlob = response.blob();
                        reader.readAsDataURL(response.blob());
                        this.isview = true;
                        // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                        reader.onloadend = (e) => {
                            this.pdfSrc = reader.result;
                        }
                        this.loadingService.loadingStop();
                    });
            }
            this.modalPilihCeteakn = false;
        }
    }

    downloadExcel() {
        const filter_data = 'idpurchaseorder:' + this.idPrint;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PrintPOPusat/xlsx', { filterData: filter_data })
    }

    isCanEdit(account: Account) {
        this.isEdit = false;
        if ((account.authorities.indexOf('ROLE_MGR_PRC') > -1) && this.purchasing.printcount === 1) {
            this.isEdit = true;
        }
        if (!(account.authorities.indexOf('ROLE_MGR_PRC') > -1) && this.purchasing.printcount !== 1) {
            this.isEdit = true;
        }
    }

    closePO(purchasing: Purchasing) {
        if (purchasing.otorisasi !== this.loginName) {
            alert('Hanya Otorisasi PO Saja Yang Bisa Untuk Close PO');
        } else {
            this.purchasing = new Purchasing();
            this.purchasing = purchasing;
            this.newModalNote = true;
        }
    }

    cancelPO(purchasing: Purchasing) {
        if (purchasing.otorisasi !== this.loginName) {
            alert('Hanya Otorisasi PO Saja Yang Bisa Untuk Cancel PO');
        } else {
            this.purchasing = new Purchasing();
            this.purchasing = purchasing;
            this.newModalNote1 = true;
        }

    }

    modalClosePO() {
        this.purchasing.notes2 = this.noteCLosePO;
        this.purchasingService.closePO(this.purchasing)
            .subscribe(
                (res) => {
                    console.log('update sukses');
                    this.newModalNote = false;
                    this.loadAll();
                }
            )
    }

    modalCancelPO() {
        this.purchasing.cancelnote = this.noteCancelPO;
        this.purchasingService.changeStatus(this.purchasing, 13)
            .subscribe(
                (res) => {
                    console.log('update sukses');
                    this.newModalNote1 = false;
                    this.loadAll();
                }
            )
    }
    approveEdit() {
        const allSame = this.selected.every((item) => item.otorisasi === this.loginName);
        if (allSame) {
            this.purchasingService.approveEditPO(this.selected).subscribe(
                (res) => {
                    alert(res[0].status);
                    this.loadAll();
                    this.selected = new Array<Purchasing>();
                }
            )
        } else {
            alert('Terdapat PO Yang Memiliki Otorisasi Berbeda');
        }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }

    filter() {
        this.isFilter = true;
        this.loadAll()
    }

    reset() {
        this.isFilter = false;
        this.currentSearch = ''
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.loadAll()
    }

    public siapBBM(data: Purchasing) {
        if (data.otorisasi === 'JTR') {
            if (this.loginName === 'JTR' || this.loginName === 'LSD' || this.loginName === 'MLA') {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
                    accept: () => {
                        console.log('data PO yang akan di cancel == ', data);
                        this.purchasingService.changeStatus(data, 26)
                            .subscribe(
                                (res) => this.loadAll()
                            );
                    }
                });
            } else {
                alert('Siap BBM Hanya Bisa Dilakukan Oleh Pembuat PO atau Otorisasinya');
            }
        } else if (data.otorisasi === 'SVY') {
            if (this.loginName === 'SVY' || this.loginName === 'KRI' || this.loginName === 'SNT_PBL' || this.loginName === 'EYP') {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
                    accept: () => {
                        console.log('data PO yang akan di cancel == ', data);
                        this.purchasingService.changeStatus(data, 26)
                            .subscribe(
                                (res) => this.loadAll()
                            );
                    }
                });
            } else {
                alert('Siap BBM Hanya Bisa Dilakukan Oleh Pembuat PO atau Otorisasinya');
            }
        } else if (data.otorisasi === 'EVN') {
            if (this.loginName === 'EVN' || this.loginName === 'EYP') {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
                    accept: () => {
                        console.log('data PO yang akan di cancel == ', data);
                        this.purchasingService.changeStatus(data, 26)
                            .subscribe(
                                (res) => this.loadAll()
                            );
                    }
                });
            } else {
                alert('Siap BBM Hanya Bisa Dilakukan Oleh Pembuat PO atau Otorisasinya');
            }
        } else if (data.otorisasi === 'SVY_SKG') {
            if (this.loginName === 'SVY_SKG' || this.loginName === 'SNT_SKG') {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
                    accept: () => {
                        console.log('data PO yang akan di cancel == ', data);
                        this.purchasingService.changeStatus(data, 26)
                            .subscribe(
                                (res) => this.loadAll()
                            );
                    }
                });
            } else {
                alert('Siap BBM Hanya Bisa Dilakukan Oleh Pembuat PO atau Otorisasinya');
            }
        } else if (data.otorisasi === 'SVY_SMK') {
            if (this.loginName === 'SVY_SMK' || this.loginName === 'SNT_SMK') {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
                    accept: () => {
                        console.log('data PO yang akan di cancel == ', data);
                        this.purchasingService.changeStatus(data, 26)
                            .subscribe(
                                (res) => this.loadAll()
                            );
                    }
                });
            } else {
                alert('Siap BBM Hanya Bisa Dilakukan Oleh Pembuat PO atau Otorisasinya');
            }
        } else if (data.otorisasi === 'JTR_SMK') {
            if (this.loginName === 'JTR_SMK' || this.loginName === 'MLA_SMK') {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin PO ' + data.nopurchasing + ' Siap BBM',
                    accept: () => {
                        console.log('data PO yang akan di cancel == ', data);
                        this.purchasingService.changeStatus(data, 26)
                            .subscribe(
                                (res) => this.loadAll()
                            );
                    }
                });
            } else {
                alert('Siap BBM Hanya Bisa Dilakukan Oleh Pembuat PO atau Otorisasinya');
            }
        }
    }

    downloadBlob() {
        FileSaver.saveAs(this.pdfBlob, this.purchasing.nopurchasing);
    }
    lihatHarga() {
        this.pilihPrintPO(this.pilihBahasa);
    }
    public approvePO(data: Purchasing) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Approve PO ' + data.nopurchasing,
            accept: () => {
                this.purchasingService.changeStatus(data, 11)
                    .subscribe(
                        (res) => this.loadAll()
                    );
            }
        });
    }
    reqApp(data: Purchasing) {
        console.log('nama login = ', this.loginName);
        console.log('nama pembuat = ', data.createdby)
        if (this.loginName === data.createdby) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Request Approve PO ' + data.nopurchasing,
                accept: () => {
                    this.purchasingService.changeStatus(data, 29)
                        .subscribe(
                            (res) => this.loadAll()
                        );
                }
            });
        } else {
            alert('Request Approve di Ijinkan Untuk Pembuat PO');
        }
    }
    notes(data: Purchasing) {
        this.newModalNoteNotAPP = true;
        this.ketNotApp = data.ketnotapp;
    }
    setmail() {
        if (this.loginName === 'JTR' || this.loginName === 'MLA' || this.loginName === 'LSD') {
            const allSame = this.selected.every((item) => item.otorisasi === 'JTR');
            if (allSame) {
                this.purchasingService.setMailPO(this.selected).subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<Purchasing>();
                        alert('Sukses');
                    }
                )
            } else {
                alert('Terdapat PO Yang Otorisasi POnya Berbeda');
            }
        } else if (this.loginName === 'SVY' || this.loginName === 'SNT_PBL' || this.loginName === 'KRI') {
            const allSame = this.selected.every((item) => item.otorisasi === 'SVY');
            if (allSame) {
                this.purchasingService.setMailPO(this.selected).subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<Purchasing>();
                        alert('Sukses');
                    }
                )
            } else {
                alert('Terdapat PO Yang Otorisasi POnya Berbeda');
            }
        } else if (this.loginName === 'EYP') {
            const allSame = this.selected.every((item) => item.otorisasi === 'SVY' || item.otorisasi === 'EVN');
            if (allSame) {
                this.purchasingService.setMailPO(this.selected).subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<Purchasing>();
                        alert('Sukses');
                    }
                )
            } else {
                alert('Terdapat PO Yang Otorisasi POnya Berbeda');
            }
        } else if (this.loginName === 'SVY_SKG' || this.loginName === 'SNT_SKG') {
            const allSame = this.selected.every((item) => item.otorisasi === 'SVY_SKG');
            if (allSame) {
                this.purchasingService.setMailPO(this.selected).subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<Purchasing>();
                        alert('Sukses');
                    }
                )
            } else {
                alert('Terdapat PO Yang Otorisasi POnya Berbeda');
            }
        } else if (this.loginName === 'SVY_SMK' || this.loginName === 'SNT_SMK') {
            const allSame = this.selected.every((item) => item.otorisasi === 'SVY_SMK');
            if (allSame) {
                this.purchasingService.setMailPO(this.selected).subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<Purchasing>();
                        alert('Sukses');
                    }
                )
            } else {
                alert('Terdapat PO Yang Otorisasi POnya Berbeda');
            }
        } else if (this.loginName === 'MLA_SMK' || this.loginName === 'JTR_SMK') {
            const allSame = this.selected.every((item) => item.otorisasi === 'JTR_SMK');
            if (allSame) {
                this.purchasingService.setMailPO(this.selected).subscribe(
                    (res) => {
                        this.loadAll();
                        this.selected = new Array<Purchasing>();
                        alert('Sukses');
                    }
                )
            } else {
                alert('Terdapat PO Yang Otorisasi POnya Berbeda');
            }
        }
    }
}
