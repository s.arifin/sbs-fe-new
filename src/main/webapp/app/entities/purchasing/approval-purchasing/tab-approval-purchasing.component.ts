import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Purchasing, Kabag } from '../purchasing.model';
import { PurchasingService } from '../purchasing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { PurchasingItemService } from '../../purchasing-item';
import { LoadingService } from '../../../layouts';
import { MasterSupplierService, MasterSupplier } from '../../master-supplier';
import { PurchasingFilter } from '../purchasing-filter.model';
import * as FileSaver from 'file-saver';
import { PasswordService } from '../../../account/password/password.service';

@Component({
    selector: 'jhi-tab-approval-purchasing',
    templateUrl: './tab-approval-purchasing.component.html'
})
export class TabApprovalPurchasingComponent implements OnInit, OnDestroy {

    constructor(
    ) {
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    ngOnInit(): void {
        // throw new Error('Method not implemented.');
    }
}
