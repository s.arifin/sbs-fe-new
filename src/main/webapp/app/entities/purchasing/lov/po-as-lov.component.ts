import { Component, OnInit, OnChanges, OnDestroy, Output } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { EventEmitter } from 'events';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { PurchasingService } from '..';
import * as _ from 'lodash';
import { PurchasingPopupService } from '../purchasing-popup.service';
import { Purchasing } from '../purchasing.model';

@Component({
    selector: 'jhi-po-as-lov',
    templateUrl: './po-as-lov.component.html'
})
export class PoAsLovComponent implements OnInit, OnDestroy {

    data: Purchasing;
    currentAccount: any;
    isSaving: boolean;
    purchasings: Purchasing[];
    selected: Purchasing[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    first: number;

    constructor(
        public activeModal: NgbActiveModal,
        private purchasingServices: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private route: ActivatedRoute,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<PurchaseOrder>();
        this.idInternal = this.principal.getIdInternal();
        this.first = 0;
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.purchasingServices.queryLOVPaymnet({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                query: this.currentSearch
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.selected = new Array<PurchaseOrder>();
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new Array<PurchaseOrder>();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.purchasings = data;

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.purchasingServices.pushItems(this.selected);
        this.eventManager.broadcast({ name: 'POLovModification', content: 'OK' });
        this.activeModal.dismiss('close');
        this.selected = new Array<PurchaseOrder>();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }
    sortingNoPP(arr: PurchaseOrder[]): Promise<void> {
        return new Promise<void>(
            (resolvePO, reject) => {
                this.selected = arr.sort((a, b) => (a.noPurchaseOrder < b.noPurchaseOrder ? -1 : 1));
                resolvePO();
            }
        )
    }

    sortingUrutItem(arr: PurchaseOrder[]): Promise<void> {
        return new Promise<void>(
            (resolvePO, reject) => {
                this.selected = arr.sort((a, b) => (a.item.noUrut < b.item.noUrut ? -1 : 1));
                resolvePO();
            }
        )
    }

}

@Component({
    selector: 'jhi-po-lov-popup',
    template: ''
})
export class PoAsLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        protected purchasingPopupService: PurchasingPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.purchasingPopupService.load(PoAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

}
