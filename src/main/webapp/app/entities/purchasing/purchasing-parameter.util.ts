import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createRequestOptionPurchasingPTO = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        // Read All Parameters
        for (const key in req) {
            if (key !== 'sort' && req[key]) {
                const value = req[key];
                params.set(key, value);
            }
        }
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);
        params.set('filter', req.filter);

        params.set('from', req.from);
        params.set('thru', req.thru);
        params.set('pic', req.pic);
        params.set('kdspl', req.kdspl);
        params.set('isemail', req.isemail);
        params.set('isstatus', req.isstatus);

        options.params = params;
    }
    return options;
};
