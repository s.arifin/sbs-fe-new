import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Purchasing } from './purchasing.model';
import { PurchasingPopupService } from './purchasing-popup.service';
import { PurchasingService } from './purchasing.service';

@Component({
    selector: 'jhi-purchasing-dialog',
    templateUrl: './purchasing-dialog.component.html'
})
export class PurchasingDialogComponent implements OnInit {

    purchasing: Purchasing;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private purchasingService: PurchasingService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.purchasing.id !== undefined) {
            this.subscribeToSaveResponse(
                this.purchasingService.update(this.purchasing));
        } else {
            this.subscribeToSaveResponse(
                this.purchasingService.create(this.purchasing));
        }
    }

    private subscribeToSaveResponse(result: Observable<Purchasing>) {
        result.subscribe((res: Purchasing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Purchasing) {
        this.eventManager.broadcast({ name: 'purchasingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-purchasing-popup',
    template: ''
})
export class PurchasingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private purchasingPopupService: PurchasingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.purchasingPopupService
                    .open(PurchasingDialogComponent as Component, params['id']);
            } else {
                this.purchasingPopupService
                    .open(PurchasingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
