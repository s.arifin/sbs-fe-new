import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDateUtils } from 'ng-jhipster';
import { ConfirmationService } from 'primeng/primeng';
import { Purchasing, HistoryRevisi, PurchasingService, Valuta } from '..';
import { LoadingService } from '../../../layouts';
import { ToasterService, UserService, ResponseWrapper, ITEMS_PER_PAGE, } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplierService, MasterSupplier } from '../../master-supplier';
import { PurchaseOrder, PurchaseOrderService } from '../../purchase-order';
import { PurchaseOrderItem } from '../../purchase-order-item';
import { PurchasingItem, MappingUserPO, UOMPO, MasterDiv, PurchasingItemService } from '../../purchasing-item';
import { Payment, PaymentService } from '../../payment';

@Component({
    selector: 'jhi-purchasing-pay-detail',
    templateUrl: './purchasing-pay-detail.component.html'
})
export class PurchasingPayDetailComponent implements OnInit, OnDestroy {

    @ViewChild('fileInput') inputEl: ElementRef;
    purchasing: Purchasing;
    paramPage: number;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public purchasingItems: PurchasingItem[];
    currentAccount: any;
    purchasings: Purchasing[];
    purchaseOrder: PurchaseOrder
    purchaseOrderItems: PurchaseOrderItem[];
    purchasingItem: PurchasingItem;
    error: any;
    success: any;
    eventSubscriberPPLOV: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter: PurchasingItem[];
    newSuplier: any;
    newValuta: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    filteredValuta: any[];
    isSelectSupplier: boolean;
    isSelectValuta: boolean;
    inputSupplyerName: string;
    inputValuta: string;
    splAddress: string;
    mappingUserPo: MappingUserPO;
    jumlah: number;
    checkPPN: boolean;
    tempTotalPrice: number;
    uomPO: UOMPO[];
    isDiscPersen: boolean;
    dtsent: any;
    uom: string;
    newOtorisasi: any;
    newOtorisasis: any[];
    filteredOtorisasi: any[];
    isSelectOtorisasi: boolean;
    inputOtorisasi: string;
    isEdit: boolean;
    newOto: string;
    qtypoTemp: number;
    isEditPO: boolean;
    historyRev: HistoryRevisi;
    isView: boolean;
    minimumDate: Date = new Date();
    filteredCodeSuppliers: any[];
    masterDivs: MasterDiv[];
    masterDiv: MasterDiv;
    nopo: string;
    payment: Payment;
    paytipe: number;
    idpay: any;
    first: number;
    routeId: number;
    constructor(
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
        private route: ActivatedRoute,
        private purchasingItemService: PurchasingItemService,
        private purchaseOrderService: PurchaseOrderService,
        private router: Router,
        protected confirmationService: ConfirmationService,
        private toasterService: ToasterService,
        private dateUtils: JhiDateUtils,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        private reportUtilService: ReportUtilService,
        private userService: UserService,
        private paymentService: PaymentService,
    ) {
        this.isView = false;
        this.isEditPO = false;
        this.isEdit = false;
        this.purchasing = new Purchasing();
        this.nopo = '';
        this.payment = new Payment();
        this.paytipe = 0;
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['view']) {
                this.isView = true;
                this.isEdit = true;
                this.isEditPO = false;
                this.load(params['id'], params['idpay']);
                this.idpay = params['idpay'];
                console.log('masuk view');
            } else {
                this.load(params['id']);
                this.idpay = params['idpay'];
                console.log('tidak masuk view');
            }
        });
        this.setUMtoDPFromINV();
    }

    load(id, idpay = null) {
        this.purchasingService.find(id).subscribe(
            (res: Purchasing) => {
                {
                    this.purchasing = res;
                    this.nopo = res.nopurchasing;
                    if (this.purchasing.uangmuka < this.purchasing.totalprice) {
                        this.paytipe = 2;
                    }
                    if (this.purchasing.uangmuka >= this.purchasing.totalprice) {
                        this.paytipe = 3;
                    }
                    // this.getOnePay(res.idpurchasing);

                    if (idpay !== null && idpay !== undefined) {
                        this.paymentService.find(idpay).subscribe((payment) => {
                            this.payment = payment;
                            if (payment.paytype === 1) {
                                // uang muka
                                this.purchasing.uangmuka = payment.totalpay;
                            }
                            if (payment.paytype === 2) {
                                // Downpayment
                                this.purchasing.downpayment = payment.totalpay;
                            }
                        });
                    }
                }
            }
        );
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        // this.eventManager.destroy(this.eventSubscriber);
        this.eventSubscriberPPLOV.unsubscribe();
    }
    // toDP() {
    //     if (this.paytipe === 2) {
    //         const _um = this.purchasing.uangmuka;
    //         this.purchasing.uangmuka -= _um
    //         // this.purchasing.downpayment += _um;
    //         this.purchasingService.update(this.purchasing).subscribe(
    //             (res) => {
    //                 this.previousState();
    //             })
    //     }
    //     if (this.paytipe === 3) {
    //         const _um = this.purchasing.uangmuka;
    //         this.purchasing.uangmuka -= _um
    //         // this.purchasing.payment += _um;
    //         this.purchasingService.update(this.purchasing).subscribe(
    //             (res) => {
    //                 this.previousState();
    //             })
    //     }
    // }
    getOnePay(idpo: any) {
        this.paymentService.queryOneByPurchasing(idpo).subscribe(
            (res: Payment) => {
                this.payment = res;
            }
        );
    }
    setUMtoDPFromINV() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('invoiceDPPaymentModification', () => this.previousState());
    }
}
