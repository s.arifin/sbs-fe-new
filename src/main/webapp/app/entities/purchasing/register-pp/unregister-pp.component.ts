import { Component, OnInit, OnChanges, OnDestroy, Output } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { EventEmitter } from 'events';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { PurchasingService, RegisterPPDetail } from '..';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { List } from 'lodash';

@Component({
    selector: 'jhi-unregister-pp',
    templateUrl: './unregister-pp.component.html'
})
export class UnRegisterPPComponent implements OnInit, OnDestroy {

    // @Output()
    // fnCallback = new EventEmitter();

    // this.fnCallback.emit(this.receipts);

    data: PurchaseOrder;
    currentAccount: any;
    isSaving: boolean;
    registerDetail: RegisterPPDetail[];
    purchaseOrdersReceive: PurchaseOrder[];
    selected: PurchaseOrder[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    po: PurchaseOrder;
    pics: Array<object> = [
        { label: 'all', value: '' },
        { label: 'EVN', value: 'EVN' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' }
    ];
    selectedPIC: String = '';
    selectedPPtype: String = '';
    ppTypes: Array<object> = [
        { label: 'all', value: '' },
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }
    ];
    isDisable: Boolean = false;
    tempPic: String = '';
    tempTiep: String = '';

    constructor(
        public activeModal: NgbActiveModal,
        private purchaseOrderService: PurchaseOrderService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private reportUtilService: ReportUtilService,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = 20;
        this.page = 0;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<PurchaseOrder>();
        this.idInternal = this.principal.getIdInternal();
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.purchasingService.getPPForRegister({
            isReceive: 'unregister',
            page: this.page,
            size: this.itemsPerPage,
            filter: 'fillpic:|pptipe:'
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.router.navigate(['/PurcahseOrders/lov'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                // search: this.currentSearch,
                // sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new Array<PurchaseOrder>();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = headers.get('X-Total-Count');
        this.registerDetail = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    registerpp() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.pic) || !map.has(item.needs)) {
                map.set(item.pic, true);    // set any value to Map
                map.set(item.needs, true);
                result.push({
                    name: item.pic,
                    tipe: item.needs
                });
            }
        }
        if (result.length === 1) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Anda Yakin Untuk Register Nomor PP ini ???',
                accept: () => {
                    this.selected.forEach(
                        (e: PurchaseOrder) => {
                            this.printPP(e.idPurchaseOrder);
                        }
                    );
                    this.loadingService.loadingStart();
                    this.purchasingService.createRegisterPP(this.selected)
                        .subscribe(
                            (res: any) => {
                                this.selected = new Array<PurchaseOrder>();
                                this.loadAll();
                                this.eventManager.broadcast({ name: 'registerPPDone', content: 'OK' });
                            }
                        )
                }
            });
        }
        if (result.length > 1) {
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki PIC sama');
        }
    }

    printPP(id: any) {
        const user = this.principal.getUserLogin();

        const filter_data = 'idPruchaseOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf', { filterData: filter_data });
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    addDropdownValue(pp: PurchaseOrder[]) {
        // pp.forEach(
        //     (e) => {
        //         let obj = new Object();
        //         obj = {label: e.pic, value: e.pic};
        //         this.pics.push(obj);
        //     }
        // );
        // console.log('this.pics == ', this.pics);
    }

    filter() {
        this.loadingService.loadingStart();
        this.purchasingService.getPPForRegister({
            isReceive: 'false',
            page: this.page,
            size: this.itemsPerPage,
            filter: 'fillpic:' + this.selectedPIC + '|pptipe:' + this.selectedPPtype
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    reset() {
        this.selectedPIC = '';
        this.selectedPPtype = '';
        this.loadAll();
    }
    onRowSelect(event) {
        console.log('leng selected data == ', this.selected);
        if (this.selected.length === 0) {
            this.tempPic = '';
            this.tempTiep = '';
        }
        if (this.selected.length === 1) {
            this.tempPic = event.data.pic;
            this.tempTiep = event.data.needs;
            this.isDisable = false;
        }
        if (this.selected.length > 1) {
            if (this.tempPic === event.data.pic) {
                this.isDisable = false;
            }
            if (this.tempTiep === event.data.needs) {
                this.isDisable = false;
            }

            if (this.tempPic !== event.data.pic) {
                this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki PIC sama');
                this.isDisable = true;
            }
            if (this.tempTiep !== event.data.needs) {
                this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Tipe sama');
                this.isDisable = true;
            }
        }
    }
    onRowUnselect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.pic) || !map.has(item.needs)) {
                map.set(item.pic, true);    // set any value to Map
                map.set(item.needs, true);
                result.push({
                    name: item.pic,
                    tipe: item.needs
                });
            }
        }
        if (result.length === 1) {
            this.tempPic = result[0].name;
            this.tempTiep = result[0].tipe;
            this.isDisable = false;
        }
        if (result.length > 1) {
            this.isDisable = true;
        }
        console.log('cek result ==== ', result);
    }

    openPDF(idpp) {
        // const byteCharacters = atob(content);
        // const byteNumbers = new Array(byteCharacters.length);
        // for (let i = 0; i < byteCharacters.length; i++) {
        //     byteNumbers[i] = byteCharacters.charCodeAt(i);
        // }

        // const byteArray = new Uint8Array(byteNumbers);

        // const blobContent = new Blob([byteArray], {'type': fieldContentType});
        // const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // // const fileURL = URL.createObjectURL(blobContent);
        // // window.open(fileURL, '_blank');
        // const win = window.open(content);
        // win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
        this.purchasingService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }

    print(id: any, nopp: any) {
        const filter_data = 'idPruchaseOrder:' + id;
        if (nopp.includes('SMK')) {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderUnRegPPSMK/pdf', { filterData: filter_data });
        } else if (nopp.includes('SKG')) {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderUnRegPPSKG/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderUnRegPP/pdf', { filterData: filter_data });
        }
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }
}
