import { Component, OnInit, OnChanges, OnDestroy, Output, SimpleChanges, Input, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { PurchasingService } from '..';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { RegisterPPDetail } from '../purchasing.model';
import * as FileSaver from 'file-saver';
import { FileCompressionService } from '../../joborders';
import { array } from '@amcharts/amcharts4/core';

@Component({
    selector: 'jhi-approval-register-pp-detail',
    templateUrl: './approval-register-pp-detail.component.html'
})
export class ApprovalRegisterPPDetailComponent implements OnInit, OnDestroy {

    @Output()
    fnCallback = new EventEmitter();
    [x: string]: any;
    // this.fnCallback.emit(this.receipts);
    @Input()
    idstatus: number;
    attac_file: any;
    data: PurchaseOrder;
    dataNewRow: PurchaseOrder;
    currentAccount: any;
    isSaving: boolean;
    purchaseOrders: PurchaseOrder[];
    purchaseOrdersReceive: PurchaseOrder[];
    selected: RegisterPPDetail[];
    error: any;
    success: any;
    subscription: Subscription;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    po: PurchaseOrder;
    newModalNote: boolean;
    newModalAttachment: boolean;
    idreg: any;
    user: any;
    noteUnregPP: string;
    pp: RegisterPPDetail;
    selectedSTS: Number = 0;
    noreg: any;
    contentContentType: string;
    content: any;
    ppStatus: Array<object> = [
        { label: 'Pilih Status', value: '0' },
        { label: 'Serah Purchasing', value: '23' },
        { label: 'Serah Management', value: '24' }
    ];
    isDisabled: Boolean = false;
    isview: boolean;
    pdfSrc: any;
    pdfBlob: any;
    purchaseOrder: PurchaseOrder;
    ketNotApp: string;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    sizePdf: any;
    constructor(
        public activeModal: NgbActiveModal,
        private purchaseOrderService: PurchaseOrderService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private reportUtilService: ReportUtilService,
        private toasterService: ToasterService,
        private activatedRoute: ActivatedRoute,
        protected dataUtils: JhiDataUtils,
        private fileCompressionService: FileCompressionService,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<RegisterPPDetail>();
        this.idInternal = this.principal.getIdInternal();
        this.user = this.principal.getUserLogin();
        this.newModalNote = false;
        this.idreg = '';
        this.noteUnregPP = '';
        this.pp = new PurchaseOrder();
        this.dataNewRow = new PurchaseOrder();
        this.noreg = '';
        this.isview = false;
        this.purchaseOrder = new PurchaseOrder();
        this.ketNotApp = '';
        this.newModalAttachment = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.attac_file = null;
    }
    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.purchasingService.getAppPPManagement({
                page: this.page,
                size: this.itemsPerPage,
                query: 'nopo:' + this.currentSearch + '|status:' + this.idstatus
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.purchasingService.getAppPPManagement({
            page: this.page,
            size: this.itemsPerPage,
            query: 'nopo:' + this.currentSearch + '|status:' + this.idstatus
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        // this.loadingService.loadingStop();
        console.log(this.user);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.router.navigate(['/approval-pp'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                // sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.registerFromProcess();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = headers.get('X-Total-Count');
        this.purchaseOrders = data;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    // pushData() {
    //     this.purchaseOrderService.pushItems(this.selected);
    //     this.eventManager.broadcast({ name: 'purchaseOrderLovModification', content: 'OK' });
    //     this.activeModal.dismiss('close');
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    print() {
        const filter_data = 'idPruchaseOrder:' + this.pp.idpurchaseorder;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data });
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    back() {
        this.fnCallback.emit();
    }
    modalUnregPP() {
        this.pp.notes = this.noteUnregPP;
        this.purchasingService.unRegisterPP(this.pp).subscribe(
            (res) => this.back()
        );
    }
    unregPP(pp: RegisterPPDetail) {
        this.pp = pp;
        this.newModalNote = true;
    }

    updatestatus() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        if (result.length === 1) {
            if (this.selectedSTS === 0) {
                this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
            } else {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Anda Yakin Untuk Register Nomor PP ini ???',
                    accept: () => {
                        // this.loadingService.loadingStart();
                        if (this.selectedSTS !== 0) {
                            this.purchasingService.changeStatusPPReg(this.selected, this.selectedSTS)
                                .subscribe(
                                    (res) => {
                                        // this.loadingService.loadingStop();
                                        this.fnCallback.emit();
                                    },
                                    (err) => this.loadingService.loadingStop()
                                );
                        }
                        if (this.selectedSTS === 0) {
                            this.toasterService.showToaster('Informasi', 'Status', 'Silahkan Pilih status !');
                        }
                    }
                });
            }
        }
        if (result.length > 1) {
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
    }
    onRowSelect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        console.log('ini data sleect all == ', result);
        if (result.length === 1) {
            this.isDisabled = false;
            if (result[0].status === 'Serah Purchasing') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Serah Management') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Unregister') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'Sudah Di Buat PO') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'BIKIN PP BARU') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'SUDAH PO SEBAGIAN') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
        }
        if (result.length > 1) {
            this.isDisabled = true;
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
        console.log('result.length = ', result.length);
    }
    onRowUnselect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        if (result.length === 0) {
            this.isDisabled = true;
        }
        if (result.length === 1) {
            this.isDisabled = false;
            if (result[0].status === 'Serah Purchasing') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Serah Management') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Unregister') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'Sudah Di Buat PO') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'BIKIN PP BARU') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'SUDAH PO SEBAGIAN') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
        }
        if (result.length > 1) {
            this.isDisabled = true;
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
        console.log('result.length = ', result.length);
    }

    validateStatus() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        if (result.length === 0) {
            this.isDisabled = true;
        }
        if (result.length === 1) {
            this.isDisabled = false;
            if (result[0].status === 'Serah Purchasing') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Serah Management') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Unregister') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'Sudah Di Buat PO') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'BIKIN PP BARU') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'SUDAH PO SEBAGIAN') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
        }
        if (result.length > 1) {
            this.isDisabled = true;
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
        console.log('result.length = ', result.length);
    }

    openPDF(content: any, fieldContentType) {
        const byteCharacters = atob(content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': fieldContentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const fileURL = URL.createObjectURL(blobContent);
        // window.open(fileURL, '_blank');
        const win = window.open(content);
        win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
    }

    public PrintPP(pp: PurchaseOrder) {
        this.pp = pp;
        // this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        this.pdfBlob = '';
        const filter_data = 'idPruchaseOrder:' + pp.idPurchaseOrder;
        if (pp.divition.includes('SKG')) {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderSKG/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    console.log('cek view nya apaan dah = ', this.isview);
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    // this.loadingService.loadingStop();
                });
        } else if (pp.divition.includes('SMK')) {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderSMK/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    console.log('cek view nya apaan dah = ', this.isview);
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    // this.loadingService.loadingStop();
                });
        } else {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    console.log('cek view nya apaan dah = ', this.isview);
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    // this.loadingService.loadingStop();
                });
        }
    }
    backPage() {
        this.isview = false;
    }
    downloadBlob() {
        FileSaver.saveAs(this.pdfBlob, this.pp.nopurchaseorder);
    }
    download(field: any, fieldContentType: string, fileName: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], { 'type': fieldContentType });

        if (navigator.msSaveBlob) {
            const filename = 'Lampiran - ' + fileName;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = 'Lampiran - ' + fileName;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    approvePP() {
        console.log('test masuk approve tidak ??');
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Approve PP ',
            accept: () => {
                // this.loadingService.loadingStart();
                // this.purchasingService.changeStatusPPReg(this.selected, 23)
                this.purchasingService.changeStatusPP(this.selected, 21)
                    .subscribe(
                        (res) => {
                            this.loadAll()
                            this.loadingService.loadingStop()
                        }
                    );
            }
        });

    }

    notApprovePP() {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Not Approve PP ',
            accept: () => {
                // this.loadingService.loadingStart();
                // this.purchasingService.changeStatusPPReg(this.selected, 30)
                this.purchasingService.changeStatusPP(this.selected, 12)
                    .subscribe(
                        (res) => {
                            this.loadAll()
                            this.loadingService.loadingStop()
                        }
                    );
            }
        });
    }

    approve(data: PurchaseOrder) {
        if (data.appManajemen === this.user) {
            const datas: PurchaseOrder[] = new Array<PurchaseOrder>();
            datas.push(data);
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Approve PP ',
                accept: () => {
                    // this.loadingService.loadingStart();
                    // this.purchasingService.changeStatusPPReg(datas, 23)
                    this.purchasingService.changeStatusPP(datas, 21)
                        .subscribe(
                            (res) => {
                                this.eventManager.broadcast({ name: 'approvalPPModified', content: 'OK' });
                                this.loadAll()
                                this.loadingService.loadingStop()
                            }
                        );
                }
            });
        } else {
            this.dataNewRow = data;
            this.newModalAttachment = true;
        }
    }

    notapprove(data: PurchaseOrder) {
        this.newModalNote = true;
        this.dataNewRow = data;
        // const datas: RegisterPPDetail[] = new Array<RegisterPPDetail>();
        // datas.push(data);
        // this.confirmationService.confirm({
        //     header: 'Information',
        //     message: 'Apakah Yakin Untuk Not Approve PP ',
        //     accept: () => {
        //         // this.loadingService.loadingStart();
        //         this.purchasingService.changeStatusPPReg(datas, 30)
        //             .subscribe(
        //                 (res) => {
        //                     this.loadAll()
        //                     this.loadingService.loadingStop()
        //                 }
        //             );
        //     }
        // });
    }
    setNotApprovePO() {
        const datas: PurchaseOrder[] = new Array<PurchaseOrder>();
        this.dataNewRow.ket_not_app = this.ketNotApp;
        datas.push(this.dataNewRow);
        if (this.ketNotApp === '') {
            this.toasterService.showToaster('Info', 'Field Kosong', 'Keterangan Not Approve tidak Boleh kosong');
        } if (this.ketNotApp.length > 255) {
            this.toasterService.showToaster('Info', 'Melebihi Kapasitas', 'Keterangan Not Approve tidak Boleh Lebih dari 255 Karakter');
        } else if (this.ketNotApp !== '' && this.ketNotApp.length <= 255) {
            if (this.dataNewRow.appManajemen !== this.user) {
                if ((this.dataNewRow.content === null || this.dataNewRow.content === '' || this.dataNewRow.content === undefined) && (this.dataNewRow.contentContentType === null || this.dataNewRow.contentContentType === '' || this.dataNewRow.contentContentType === undefined)) {
                    alert('File Lampiran Permintaan Harus Di Pilih');
                } else {
                    this.confirmationService.confirm({
                        header: 'Information',
                        message: 'Apakah Yakin Untuk Not Approve PP ',
                        accept: () => {
                            // this.loadingService.loadingStart();
                            // this.purchasingService.changeStatusPPReg(datas, 30)
                            this.purchasingService.changeStatusPP(datas, 12)
                                .subscribe(
                                    (res) => {
                                        this.eventManager.broadcast({ name: 'approvalPPModified', content: 'OK' });
                                        this.loadAll();
                                        // this.loadingService.loadingStop();
                                        this.newModalNote = false;
                                        this.dataNewRow = new PurchaseOrder();
                                    }
                                );
                        }
                    });
                }
            } else {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Yakin Untuk Not Approve PP ',
                    accept: () => {
                        // this.loadingService.loadingStart();
                        // this.purchasingService.changeStatusPPReg(datas, 30)
                        this.purchasingService.changeStatusPP(datas, 12)
                            .subscribe(
                                (res) => {
                                    this.eventManager.broadcast({ name: 'approvalPPModified', content: 'OK' });
                                    this.loadAll();
                                    // this.loadingService.loadingStop();
                                    this.newModalNote = false;
                                    this.dataNewRow = new PurchaseOrder();
                                }
                            );
                    }
                });
            }
        }
    }
    registerFromProcess() {
        this.eventManager.subscribe('approvalPPModified', () => this.loadAll());
    }

    openPDF1(idpp: any) {
        this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.dataNewRow, this.elementRef, field, fieldContentType, idInput);
        // Mengosongkan konten dan tipe konten
        this.dataNewRow[field] = null;
        this.dataNewRow[fieldContentType] = null;

        // Reset input file
        const fileInput = document.getElementById(idInput) as HTMLInputElement;
        if (fileInput) {
            fileInput.value = ''; // Reset file input
        }

        // Set flag untuk validasi required
        this.isInsertFile = ''; // Atur sesuai kebutuhan, misalnya null atau ''
    }

    openPDFAttach(content: any, contentType: any) {
        const byteCharacters = atob(content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': contentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const win = window.open(content);
        window.open(URL.createObjectURL(blobContent), '_blank');
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 5242880 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            } else if (bytes < 5242880) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
            this.sizePdf = event.target.files[0].size;
            alert('Ukuran File Melebihi Batas 10MB')
        }
    }

    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }

    setAttachment() {
        if ((this.dataNewRow.content === null || this.dataNewRow.content === '' || this.dataNewRow.content === undefined) && (this.dataNewRow.contentContentType === null || this.dataNewRow.contentContentType === '' || this.dataNewRow.contentContentType === undefined)) {
            alert('Lampiran Permintaan Wajib di Isi');
        } else {
            const datas: PurchaseOrder[] = new Array<PurchaseOrder>();
            datas.push(this.dataNewRow);
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Approve PP ',
                accept: () => {
                    // this.loadingService.loadingStart();
                    // this.purchasingService.changeStatusPPReg(datas, 23)
                    this.purchasingService.changeStatusPP(datas, 21)
                        .subscribe(
                            (res) => {
                                this.eventManager.broadcast({ name: 'approvalPPModified', content: 'OK' });
                                this.loadAll()
                                this.loadingService.loadingStop();
                                this.clearInputImage('content', 'contentContentType', 'file_content');
                                this.dataNewRow = new PurchaseOrder();
                                this.newModalAttachment = false;
                                this.isInsertFile = null;
                                this.attac_file = null;
                            }
                        );
                }
            });
        }

    }

    backapp() {
        this.clearInputImage('content', 'contentContentType', 'file_content');
        this.newModalAttachment = false;
        this.dataNewRow = new PurchaseOrder();
        this.isInsertFile = null;
        this.attac_file = null;
        console.log(`ini back ${this.isInsertFile}`);
    }

    cancelApp(id: any, rowData: PurchaseOrder) {
        this.confirmationService.confirm({
            header: 'Confirmation',
            message: 'Apakah anda yakin ingin membatalkan PP yang sudah diapprove kembali ke approval anda ?',
            accept: () => {
                this.loadingService.loadingStart();
                this.purchaseOrderService.getCancelPP(rowData, id).subscribe(
                    (res: ResponseWrapper) => {
                        this.toasterService.showToaster('info', 'Data Proces', 'Approved PP Batal');
                        this.loadData();
                    },
                    (err) => {
                        this.toasterService.showToaster('info', 'Data Proces', 'Cancel Approved PP Gagal Karena PP sudah di proses oleh divisi Pembelian');
                        this.loadData();
                    }
                );
                this.loadingService.loadingStop();

            }
        })
    }

}
