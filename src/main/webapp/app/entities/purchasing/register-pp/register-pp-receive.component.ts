import { Component, OnInit, OnChanges, OnDestroy, Output } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { EventEmitter } from 'events';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { PurchasingService } from '..';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { RegisterPP } from '../purchasing.model';
import { LoadingService } from '../../../layouts';

@Component({
    selector: 'jhi-register-pp-receive',
    templateUrl: './register-pp-receive.component.html'
})
export class RegisterPPReceiveComponent implements OnInit, OnDestroy {

    // @Output()
    // fnCallback = new EventEmitter();

    // this.fnCallback.emit(this.receipts);

    data: PurchaseOrder;
    registerPP: RegisterPP[];
    currentAccount: any;
    isSaving: boolean;
    purchaseOrders: PurchaseOrder[];
    selected: PurchaseOrder[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    po: PurchaseOrder;
    noteUnregPP: string;
    pics: Array<object> = [
        { label: 'all', value: '' },
        { label: 'JTR', value: 'JTR' },
        { label: 'SVY', value: 'SVY' },
        { label: 'SVY_SKG', value: 'SVY_SKG' },
        { label: 'SVY_SMK', value: 'SVY_SMK' },
        { label: 'JTR_SMK', value: 'JTR_SMK' },
    ];
    ppTypes: Array<object> = [
        { label: 'all', value: '' },
        { label: 'Rutin', value: 'Rutin' },
        { label: 'Non Rutin', value: 'Non Rutin' }
    ];
    selectedPIC: String = '';
    selectedPPtype: String = '';
    tempPic: String = '';
    tempTiep: String = '';
    isDetail: boolean;
    idDetail: any;
    isFilter: boolean;
    tipe: any;

    constructor(
        public activeModal: NgbActiveModal,
        private purchaseOrderService: PurchaseOrderService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        protected confirmationService: ConfirmationService,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<PurchaseOrder>();
        this.idInternal = this.principal.getIdInternal();
        this.noteUnregPP = '';
        this.pics = [
            { label: 'all', value: '' },
            { label: 'JTR', value: 'JTR' },
            { label: 'SVY', value: 'SVY' },
            { label: 'SVY_SKG', value: 'SVY_SKG' },
            { label: 'SVY_SMK', value: 'SVY_SMK' },
            { label: 'JTR_SMK', value: 'JTR_SMK' },
        ];
        this.ppTypes = [
            { label: 'all', value: '' },
            { label: 'Rutin', value: 'Rutin' },
            { label: 'Non Rutin', value: 'Non Rutin' }
        ];
        this.isDetail = false;
        this.isFilter = false;
        this.tipe = '';
        if (!this.idInternal.includes('SMK') && !this.idInternal.includes('SKG')) {
            this.pics = this.pics.filter((pic) => (!pic['value'].includes('SMK') && !pic['value'].includes('SKG')) || pic['label'] === 'all');
        } else if (this.idInternal.includes('SMK')) {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SMK') || pic['label'] === 'all');
        } else {
            this.pics = this.pics.filter((pic) => pic['value'].includes('SKG') || pic['label'] === 'all');
        }
    }

    loadAll() {
        if (this.isFilter === true) {
            this.loadingService.loadingStart();
            this.purchasingService.getPPForRegister({
                isReceive: 'true',
                idInternal: this.idInternal,
                page: this.page,
                size: this.itemsPerPage,
                filter: 'fillpic:' + this.selectedPIC + '|pptipe:' + this.selectedPPtype,
                query: this.currentSearch
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() },
            );
        } else {
            this.purchasingService.getPPForRegister({
                isReceive: 'true',
                idInternal: this.idInternal,
                page: this.page,
                size: this.itemsPerPage,
                filter: 'fillpic:|pptipe:',
                query: this.currentSearch
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.router.navigate(['/PurcahseOrders/lov'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                // search: this.currentSearch,
                // sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new Array<PurchaseOrder>();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.loadAll();
        this.registerChangeInPurchasings();
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = headers.get('X-Total-Count');
        this.registerPP = data;

    };
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    // pushData() {
    //     this.purchaseOrderService.pushItems(this.selected);
    //     this.eventManager.broadcast({ name: 'purchaseOrderLovModification', content: 'OK' });
    //     this.activeModal.dismiss('close');
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    registerChangeInPurchasings() {
        this.eventSubscriber = this.eventManager.subscribe('registerPPDone', (response) => this.loadAll());
    }

    print(data: RegisterPP) {
        const filter_data = 'idregister:' + data.idregister;
        if (data.tipepp === 'Rutin') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/RegisterPP/pdf', { filterData: filter_data });
        }
        if (data.tipepp === 'Non Rutin') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/RegisterPPNonRutin/pdf', { filterData: filter_data });
        }
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }
    filter() {
        this.isFilter = true;
        this.loadAll();
    }
    reset() {
        this.selectedPIC = '';
        this.selectedPPtype = '';
        this.loadAll();
    }
    toDetail(id: any, tipe: any) {
        this.idDetail = id;
        this.tipe = tipe;
        this.isDetail = true;
    }
    back() {
        this.isDetail = false;
        this.loadAll();
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], { 'type': fieldContentType });

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

}
