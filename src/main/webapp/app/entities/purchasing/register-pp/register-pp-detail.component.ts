import { Component, OnInit, OnChanges, OnDestroy, Output, SimpleChanges, Input, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { PurchaseOrderService, PurchaseOrder, PurchaseOrderPopupService } from '../../purchase-order';
import { PurchasingService } from '..';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { RegisterPPDetail } from '../purchasing.model';
import { fn } from '@angular/compiler/src/output/output_ast';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'jhi-register-pp-detail',
    templateUrl: './register-pp-detail.component.html'
})
export class RegisterPPDetailComponent implements OnInit, OnDestroy, OnChanges {

    @Output()
    fnCallback = new EventEmitter();

    // this.fnCallback.emit(this.receipts);

    @Input()
    idDetail: any;
    @Input()
    tipe: string;

    data: PurchaseOrder;
    currentAccount: any;
    isSaving: boolean;
    registerDetail: RegisterPPDetail[];
    purchaseOrdersReceive: PurchaseOrder[];
    selected: RegisterPPDetail[];
    error: any;
    success: any;
    subscription: Subscription;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    po: PurchaseOrder;
    newModalNote: boolean;
    idreg: any;
    noteUnregPP: string;
    pp: RegisterPPDetail;
    selectedSTS: Number = 0;
    noreg: any;
    ppStatus: Array<object> = [];
    isDisabled: Boolean = false;
    isview: boolean;
    pdfSrc: any;
    pdfBlob: any;
    newModalNoteNotAPP: boolean;
    ketNotApp: string;
    constructor(
        public activeModal: NgbActiveModal,
        private purchaseOrderService: PurchaseOrderService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private router: Router,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private reportUtilService: ReportUtilService,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<PurchaseOrder>();
        this.idInternal = this.principal.getIdInternal();
        this.newModalNote = false;
        this.idreg = '';
        this.noteUnregPP = '';
        this.pp = new PurchaseOrder();
        this.noreg = '';
        this.isview = false;
        this.newModalNoteNotAPP = false;
        this.ketNotApp = '';
    }
    ngOnChanges(changes: SimpleChanges): void {
        this.loadAll(this.idDetail);
    }

    loadAll(id) {
        this.loadingService.loadingStart();
        this.purchasingService.getRegisterDetail(id, {
            query: 'idRegis:' + id,
            page: this.page,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.router.navigate(['/PurcahseOrders/lov'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                // search: this.currentSearch,
                // sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        // this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        // this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.loadAll();
    }

    ngOnInit() {
        this.isDisabled = true;
        this.selected = new Array<PurchaseOrder>();
        this.subscription = this.route.params.subscribe((params) => {
            this.loadAll(params['id']);
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    trackId(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = headers.get('X-Total-Count');
        this.registerDetail = data;
        this.noreg = this.registerDetail[0].noregister;
        this.loadingService.loadingStop();

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    // pushData() {
    //     this.purchaseOrderService.pushItems(this.selected);
    //     this.eventManager.broadcast({ name: 'purchaseOrderLovModification', content: 'OK' });
    //     this.activeModal.dismiss('close');
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    print() {
        const filter_data = 'idPruchaseOrder:' + this.pp.idpurchaseorder;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf', { filterData: filter_data });
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    back() {
        this.fnCallback.emit();
        this.loadingService.loadingStop();
    }
    modalUnregPP() {
        this.pp.notes = this.noteUnregPP;
        this.loadingService.loadingStart();
        this.purchasingService.unRegisterPP(this.pp).subscribe(
            (res) => this.back()
        );
    }
    unregPP(pp: RegisterPPDetail) {
        this.pp = pp;
        this.newModalNote = true;
    }

    updatestatus() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        if (result.length === 1) {
            if (this.selectedSTS === 0) {
                this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
            } else {
                this.confirmationService.confirm({
                    header: 'Information',
                    message: 'Apakah Anda Yakin Untuk Register Nomor PP ini ???',
                    accept: () => {
                        this.loadingService.loadingStart();
                        if (this.selectedSTS !== 0) {
                            this.purchasingService.changeStatusPPReg(this.selected, this.selectedSTS)
                                .subscribe(
                                    (res) => {
                                        this.loadingService.loadingStop();
                                        this.fnCallback.emit();
                                    },
                                    (err) => this.loadingService.loadingStop()
                                );
                        }
                        if (this.selectedSTS === 0) {
                            this.toasterService.showToaster('Informasi', 'Status', 'Silahkan Pilih status !');
                        }
                    }
                });
            }
        }
        if (result.length > 1) {
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
    }
    onRowSelect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        console.log('ini data sleect all == ', result);
        if (result.length === 1) {
            this.isDisabled = false;
            if (result[0].status === 'Serah Purchasing') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Serah Management') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Unregister') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'Sudah Di Buat PO') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'BIKIN PP BARU') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'SUDAH PO SEBAGIAN') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
        }
        if (result.length > 1) {
            this.isDisabled = true;
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
        console.log('result.length = ', result.length);
    }
    onRowUnselect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        if (result.length === 0) {
            this.isDisabled = true;
        }
        if (result.length === 1) {
            this.isDisabled = false;
            if (result[0].status === 'Serah Purchasing') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Serah Management') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    { label: 'Serah Management', value: '24' },
                    // { label: 'Siap PO', value: '25' }
                ];
            }
            if (result[0].status === 'Unregister') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'Sudah Di Buat PO') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'BIKIN PP BARU') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
            if (result[0].status === 'SUDAH PO SEBAGIAN') {
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                ];
            }
        }
        if (result.length > 1) {
            this.isDisabled = true;
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
        console.log('result.length = ', result.length);
    }

    validateStatus() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.statusdesc)) {
                map.set(item.statusdesc, true);    // set any value to Map
                result.push({
                    status: item.statusdesc,
                });
            }
        }
        if (result.length === 0) {
            this.isDisabled = true;
        }
        if (result.length === 1) {
            if (this.tipe.toLowerCase() === 'rutin') {
                this.isDisabled = false;
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    // { label: 'Serah Management', value: '24' }
                ];
                if (result[0].status === 'Serah Purchasing') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                        // { label: 'Siap PO', value: '25' }
                    ];
                }
                if (result[0].status === 'Serah Management') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                        // { label: 'Serah Purchasing', value: '23' },
                        // { label: 'Siap PO', value: '25' }
                    ];
                }
                if (result[0].status === 'Unregister') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
                if (result[0].status === 'Sudah Di Buat PO') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
                if (result[0].status === 'BIKIN PP BARU') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
                if (result[0].status === 'SUDAH PO SEBAGIAN') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
            }

            if (this.tipe.toLowerCase() === 'non rutin') {
                this.isDisabled = false;
                this.ppStatus = [
                    { label: 'Pilih Status', value: '0' },
                    { label: 'Serah Purchasing', value: '23' },
                    // { label: 'Serah Management', value: '24' }
                ];
                if (result[0].status === 'Serah Purchasing') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                        // { label: 'Siap PO', value: '25' }
                    ];
                }
                if (result[0].status === 'Serah Management') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                        // { label: 'Serah Purchasing', value: '23' },
                        // { label: 'Siap PO', value: '25' }
                    ];
                }
                if (result[0].status === 'Unregister') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
                if (result[0].status === 'Sudah Di Buat PO') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
                if (result[0].status === 'BIKIN PP BARU') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
                if (result[0].status === 'SUDAH PO SEBAGIAN') {
                    this.ppStatus = [
                        { label: 'Pilih Status', value: '0' },
                    ];
                }
            }
        }
        if (result.length > 1) {
            this.isDisabled = true;
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki Status yang sama');
        }
        console.log('result.length = ', result.length);
    }

    openPDF(idpp: any) {
        this.purchasingService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
        // const byteCharacters = atob(content);
        // const byteNumbers = new Array(byteCharacters.length);
        // for (let i = 0; i < byteCharacters.length; i++) {
        //     byteNumbers[i] = byteCharacters.charCodeAt(i);
        // }

        // const byteArray = new Uint8Array(byteNumbers);

        // const blobContent = new Blob([byteArray], { 'type': fieldContentType });
        // const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // // const fileURL = URL.createObjectURL(blobContent);
        // // window.open(fileURL, '_blank');
        // const win = window.open(content);
        // win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');

    }

    public PrintPP(pp) {
        this.pp = pp;
        this.loadingService.loadingStart();
        console.log('cek masuk pilihan ngga ?');
        this.pdfSrc = '';
        this.pdfBlob = '';
        const filter_data = 'idPruchaseOrder:' + this.pp.idpurchaseorder;
        if (this.pp.nopurchaseorder.includes('SMK')) {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderRegPPSMK/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    console.log('cek view nya apaan dah = ', this.isview);
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                });
        } else if (this.pp.nopurchaseorder.includes('SKG')) {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderRegPPSKG/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    console.log('cek view nya apaan dah = ', this.isview);
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                });
        } else {
            this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/PurchaseOrderRegPP/pdf', { filterData: filter_data }).
                subscribe((response: any) => {
                    const reader = new FileReader();
                    this.pdfBlob = response.blob();
                    reader.readAsDataURL(response.blob());
                    this.isview = true;
                    console.log('cek view nya apaan dah = ', this.isview);
                    // https://stackoverflow.com/questions/41737620/angular2-typescript-filereader-onload-property-does-not-exist
                    reader.onloadend = (e) => {
                        this.pdfSrc = reader.result;
                    }
                    this.loadingService.loadingStop();
                });
        }
    }
    backPage() {
        this.isview = false;
    }
    downloadBlob() {
        FileSaver.saveAs(this.pdfBlob, this.pp.nopurchaseorder);
    }
    download(idpp: any, nopp: any) {
        this.purchasingService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blob = new Blob([byteArray], { 'type': contenttype });

                if (navigator.msSaveBlob) {
                    const filename = 'Lampiran - ' + nopp;
                    navigator.msSaveBlob(blob, filename);
                } else {
                    const link = document.createElement('a');

                    link.href = URL.createObjectURL(blob);

                    link.setAttribute('visibility', 'hidden');
                    link.download = 'Lampiran - ' + nopp;

                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }

        });
        // const byteCharacters = atob(field);
        // const byteNumbers = new Array(byteCharacters.length);
        // for (let i = 0; i < byteCharacters.length; i++) {
        //     byteNumbers[i] = byteCharacters.charCodeAt(i);
        // }

        // const byteArray = new Uint8Array(byteNumbers);

        // const blob = new Blob([byteArray], {'type': fieldContentType});

        // if (navigator.msSaveBlob) {
        //     const filename = 'Lampiran - ' + fileName;
        //     navigator.msSaveBlob(blob, filename);
        // } else {
        //     const link = document.createElement('a');

        //     link.href = URL.createObjectURL(blob);

        //     link.setAttribute('visibility', 'hidden');
        //     link.download = 'Lampiran - ' + fileName;

        //     document.body.appendChild(link);
        //     link.click();
        //     document.body.removeChild(link);
        // }
    }

    notes(data: RegisterPPDetail) {
        this.newModalNoteNotAPP = true;
        this.ketNotApp = data.ketnotapp;
    }

}
