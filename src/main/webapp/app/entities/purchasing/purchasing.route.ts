import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PurchasingComponent } from './purchasing.component';
import { PurchasingDetailComponent } from './purchasing-detail.component';
import { PurchasingPopupComponent } from './purchasing-dialog.component';
import { PurchasingDeletePopupComponent } from './purchasing-delete-dialog.component';
import { PurchasingNewComponent } from './purchasing-new.component';
import { PurchasOrderAsLovPopupComponent } from './lov/purchase-order-as-lov.component';
import { PurchasingEditComponent } from './purchasing-edit.component';
import { RegisterPPComponent } from './register-pp/register-pp.component';
import { RegisterPPDetailComponent } from './register-pp/register-pp-detail.component';
import { ApprovalPPTOPOComponent } from './approval-pp-to-po/approval-pp-to-po.component';
import { ApprovalPurchasingComponent } from './approval-purchasing/approval-purchasing.component';
import { PurchasingExportComponent } from './purchasing-export/purchasing-export.component';
import { PurchasingItemComponent } from '../purchasing-item';
import { PurchasingItemExport } from './purchasing.model';
import { PurchasingItemExportComponent } from './purchasing-export/purchasing-item-export.component';
import { ApprovalRegisterPPDetailComponent } from './register-pp/approval-register-pp-detail.component';
import { TabApprovalPurchasingComponent } from './approval-purchasing/tab-approval-purchasing.component';
import { TabApprovalRegisterPPDetailComponent } from './register-pp/tab-approval-register-pp-detail.component';
import { PriorityPPComponent } from './priority-pp/priority-pp.component';
import { PoAsLovPopupComponent } from './lov/po-as-lov.component';
import { PurchasingPayComponent } from './purchasing-payment/purchasing-pay.component';
import { PurchasingPayDetailComponent } from './purchasing-payment/purchasing-pay-detail.component';
import { PurchasingNewInjectComponent } from './purchasing-new-inject.component';

@Injectable()
export class PurchasingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const purchasingRoute: Routes = [
    {
        path: 'purchasing',
        component: PurchasingComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'purchasing/:id',
        component: PurchasingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'purchasing-new',
        component: PurchasingNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'purchasing-edit/:id',
        component: PurchasingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'register-pp',
        component: RegisterPPComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'register-pp-detail/:id',
        component: RegisterPPDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'purchasing-edit/:id/:view',
        component: PurchasingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-pp-to-po',
        component: ApprovalPPTOPOComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-po',
        // component: ApprovalPurchasingComponent,
        component: TabApprovalPurchasingComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'purchasing-export',
        component: PurchasingExportComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'purchasing-item-export',
        component: PurchasingItemExportComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-pp',
        component: TabApprovalRegisterPPDetailComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'priority-pp',
        component: PriorityPPComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'purchasing-pay',
        component: PurchasingPayComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home1.title1'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'purchasing-pay-detail/:id/:view/:idpay',
        component: PurchasingPayDetailComponent,
        resolve: {
            'pagingParams': PurchasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'purchasing-new-inject',
        component: PurchasingNewInjectComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const purchasingPopupRoute: Routes = [
    // {
    //     path: 'purchasing-new',
    //     component: PurchasingPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasing.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    {
        path: 'purchasing/:id/edit',
        component: PurchasingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'purchasing/:id/delete',
        component: PurchasingDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'purchase-order-as-lov',
        component: PurchasOrderAsLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'po-as-lov',
        component: PoAsLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
