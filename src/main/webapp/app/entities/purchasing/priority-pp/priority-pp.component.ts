
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as FileSaver from 'file-saver';
import { JhiParseLinks, JhiAlertService, JhiEventManager, JhiPaginationUtil } from 'ng-jhipster';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { MappingPriorityPPVM, Purchasing, PurchasingService } from '..';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, User, UserService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { PurchasingItemService } from '../../purchasing-item';
import { PurchasingFilter } from '../purchasing-filter.model';

@Component({
    selector: 'jhi-priority-pp',
    templateUrl: './priority-pp.component.html'
})
export class PriorityPPComponent implements OnInit, OnDestroy {

    currentAccount: any;
    mappingPriorityPPVMs: MappingPriorityPPVM[];
    mappingPriorityPPVM: MappingPriorityPPVM;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    newModal: boolean;
    kabag: Array<Object> = new Array<Object>();
    selectedKabag: string;
    idPrint: any;
    sortF: any;
    pdfSrc: any;
    isview: boolean;
    isEdit: boolean;
    noteCLosePO: string;
    newModalNote: boolean;
    modalPilihCeteakn: boolean;
    selected: Purchasing[];
    pics: Array<object>;
    adms: Array<object>;
    //  = [
    //     { label: 'all', value: '' },
    //     { label: 'EVN', value: 'EVN' },
    //     { label: 'JTR', value: 'JTR' },
    //     { label: 'SVY', value: 'SVY' }
    // ];
    // field filter
    dtfrom: Date;
    dtthru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    newSuplier: MasterSupplier;
    kdSPL: string;
    isFilter: boolean;
    selectedPIC: string;
    selectedADM: string;
    dtFilter: PurchasingFilter;
    // field filter
    loginName: string;
    pilihBahasa: string;
    isTnpaHrg: boolean;
    pdfBlob: any;
    isReqApp: boolean;
    newModalNoteNotAPP: boolean;
    ketNotApp: string;
    managers: User[];
    admins: User[];
    constructor(
        private purchasingService: PurchasingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        protected confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private userService: UserService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Purchasing>();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.newSuplier = new MasterSupplier();
        this.kdSPL = '';
        this.isFilter = false;
        this.selectedPIC = '';
        this.dtFilter = new PurchasingFilter();
        this.loginName = '';
        this.pilihBahasa = '';
        this.isTnpaHrg = false;
        this.isReqApp = false;
        this.newModalNoteNotAPP = false;
        this.ketNotApp = '';
        this.managers = new Array<User>();
        this.admins = new Array<User>();
        this.pics = new Array<object>();
        this.adms = new Array<object>();
        this.selectedADM = '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.purchasingService.queryPriorPP(
                this.page,
                this.itemsPerPage,
                'nopp:' + this.currentSearch,
                this.selectedPIC,
                this.selectedADM
            ).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.purchasingService.queryPriorPP(
            this.page,
            this.itemsPerPage,
            'nopp:' + this.currentSearch,
            this.selectedPIC,
            this.selectedADM
        ).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/priority-pp'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/priority-pp', {
            page: this.page,
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/priority-pp', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.newModalNote = false;
        this.isview = false;
        this.idPrint = '';
        this.newModal = false;
        this.selectedKabag = '';
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            console.log('Inin data account = ', account);
            // this.isCanEdit(account);
        });
        this.registerChangeInPurchasings();
        this.modalPilihCeteakn = false;
        this.getManagerPO();
        this.getAdminPO();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Purchasing) {
        return item.id;
    }
    registerChangeInPurchasings() {
        this.eventSubscriber = this.eventManager.subscribe('purchasingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.mappingPriorityPPVMs = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    public cancelPO(data: Purchasing) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Cancel PO ' + data.nopurchasing,
            accept: () => {
                console.log('data PO yang akan di cancel == ', data);
                this.purchasingService.changeStatus(data, 13)
                    .subscribe(
                        (res) => this.loadAll()
                    );
            }
        });
    }
    changeSort(event) {
        if (!event.order) {
            this.sortF = 'year';
        } else {
            this.sortF = event.field;
        }
    }

    backMainPage() {
        this.isview = false;
    }
    getAdminPO() {
        const obj1 = { label: 'ALL', value: '' }
        this.adms.push(obj1);
        this.userService.getByRole('ROLE_ADMPO')
            .subscribe(
                (res: ResponseWrapper) => {
                    console.log('ini data admin PO = ', res.json);
                    this.admins = res.json;
                    this.admins.forEach(
                        (e) => {
                            const obj = { label: e.login, value: e.login }
                            this.adms.push(obj);
                        }
                    )
                });
    }
    getManagerPO() {
        const obj1 = { label: 'ALL', value: '' }
        this.pics.push(obj1);
        this.userService.getByRole('ROLE_MGR_PRC')
            .subscribe(
                (res: ResponseWrapper) => {
                    console.log('ini data manager PO = ', res.json);
                    this.managers = res.json;
                    this.managers.forEach(
                        (e) => {
                            const obj = { label: e.login, value: e.login }
                            this.pics.push(obj);
                        }
                    )
                });
    }
    filter() {
        this.loadAll()
    }
    reset() {
        this.selectedPIC = '';
        this.selectedADM = '';
        this.page = 0;
        this.loadAll();
    }
}
