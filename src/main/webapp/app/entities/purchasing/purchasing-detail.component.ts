import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Purchasing } from './purchasing.model';
import { PurchasingService } from './purchasing.service';

@Component({
    selector: 'jhi-purchasing-detail',
    templateUrl: './purchasing-detail.component.html'
})
export class PurchasingDetailComponent implements OnInit, OnDestroy {

    purchasing: Purchasing;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPurchasings();
    }

    load(id) {
        this.purchasingService.find(id).subscribe((purchasing) => {
            this.purchasing = purchasing;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPurchasings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'purchasingListModification',
            (response) => this.load(this.purchasing.id)
        );
    }
}
