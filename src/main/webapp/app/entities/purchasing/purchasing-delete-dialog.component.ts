import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Purchasing } from './purchasing.model';
import { PurchasingPopupService } from './purchasing-popup.service';
import { PurchasingService } from './purchasing.service';

@Component({
    selector: 'jhi-purchasing-delete-dialog',
    templateUrl: './purchasing-delete-dialog.component.html'
})
export class PurchasingDeleteDialogComponent {

    purchasing: Purchasing;

    constructor(
        private purchasingService: PurchasingService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.purchasingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'purchasingListModification',
                content: 'Deleted an purchasing'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-purchasing-delete-popup',
    template: ''
})
export class PurchasingDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private purchasingPopupService: PurchasingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.purchasingPopupService
                .open(PurchasingDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
