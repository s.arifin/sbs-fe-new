import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Purchasing } from './purchasing.model';
import { PurchasingService } from './purchasing.service';

@Injectable()
export class PurchasingPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private purchasingService: PurchasingService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.purchasingService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.purchaseOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Purchasing();
                    // data.vendorId = this.idVendor;
                    // data.internalId = this.idInternal;
                    // data.billToId = this.idBillTo;
                    this.ngbModalRef = this.purchaseOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    purchaseOrderModalRef(component: Component, purchaseOrder: Purchasing): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    // load(component: Component, data?: Object): Promise<NgbModalRef> {
    //     return new Promise<NgbModalRef>((resolve, reject) => {
    //         const isOpen = this.ngbModalRef !== null;
    //         if (isOpen) {
    //             resolve(this.ngbModalRef);
    //         }

    //         // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
    //         setTimeout(() => {
    //             this.ngbModalRef = this.purchasingLoadModalRef(component);
    //             resolve(this.ngbModalRef);
    //         }, 0);
    //     });
    // }

    // purchasingLoadModalRef(component: Component): NgbModalRef {
    //     const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
    //     modalRef.result.then((result) => {
    //         this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
    //         this.ngbModalRef = null;
    //     }, (reason) => {
    //         this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
    //         this.ngbModalRef = null;
    //     });
    //     return modalRef;
    // }

        // :idProduct/:idPartyRole/:idFeature/:regNoKa/:regNoSin
        load(component: Component, data?: Object): Promise<NgbModalRef> {
            return new Promise<NgbModalRef>((resolve, reject) => {
                const isOpen = this.ngbModalRef !== null;
                if (isOpen) {
                    resolve(this.ngbModalRef);
                }
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.purchasingLoadModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            });
        }
        purchasingLoadModalRef(component: Component, data: Object): NgbModalRef {
            const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
            modalRef.componentInstance.data = data;
            modalRef.result.then((result) => {
                this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
                this.ngbModalRef = null;
            }, (reason) => {
                this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
                this.ngbModalRef = null;
            });
            return modalRef;
        }
}
