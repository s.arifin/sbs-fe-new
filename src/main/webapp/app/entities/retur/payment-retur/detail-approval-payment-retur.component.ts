import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDateUtils, JhiDataUtils } from 'ng-jhipster';
import { Retur, UnionLHP } from '../retur.model';
import { ReturService } from '../retur.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { UnionBBK } from '../unionbbk.model';
import { DetailBBK, DetailBBKTeknik, UnionBBKService } from '../../union-bbk';
import { ReturItem } from '../../retur-item';
import { UnionLHPService } from '../../union-bbk/union-lhp.service';
import { LoadingService } from '../../../layouts';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { FileCompressionService } from '../../joborders/file-compression.service';

@Component({
    selector: 'jhi-detail-approval-payment-retur',
    templateUrl: './detail-approval-payment-retur.component.html'
})
export class DetailApprovalPaymentReturComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: Retur[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selectedSuppCode: string;
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    filteredSuppliers: any[];
    tgl: Date;
    tgl_coretax: Date;
    modalBBK: boolean;
    BBKs: UnionBBK[];
    itemsDataPerPage: any;
    selectedItems: UnionBBK;
    eventSubscriberBBKLOV: Subscription;
    returItems: ReturItem[];
    detailBBK: DetailBBK[];
    detailBBKTeknik: DetailBBKTeknik[];
    returItem: ReturItem;
    jumlah: number;
    isDiscPersen: boolean;
    retur: Retur;
    checkPPN: boolean;
    private subscription: Subscription;
    isEdit: boolean;
    isUpdate: boolean;
    spl: MasterSupplier;
    dttax: Date;
    pembulatan: boolean;
    jnspembulatan: boolean;
    lblpembualatan: string;
    lbljnspembulatan: string;
    pembulatan1: boolean;
    jnspembulatan1: boolean;
    lblpembualatan1: string;
    lbljnspembulatan1: string;
    noretur: string;
    jenis_retur: string;
    ppStatus: Array<object> = [
        { label: 'Non Payment', value: '1' },
        { label: 'Payment', value: null },
    ];
    jenis_pv: any;
    taxno: string;
    sizePDF: any;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;

    constructor(
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private masterSupplierService: MasterSupplierService,
        private returService: ReturService,
        private uionBBkService: UnionBBKService,
        private toasterService: ToasterService,
        private route: ActivatedRoute,
        private uionLHPService: UnionLHPService,
        private loadingService: LoadingService,
        private dateUtils: JhiDateUtils,
        private confirmationService: ConfirmationService,
        private reportUtilService: ReportUtilService,
        protected dataUtils: JhiDataUtils,
        private fileCompressionService: FileCompressionService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.tgl = new Date();
        this.tgl_coretax = new Date();
        this.modalBBK = false;
        this.itemsDataPerPage = 0;
        this.selectedItems = new UnionBBK();
        this.returItems = new Array<ReturItem>();
        this.detailBBK = new Array<DetailBBK>();
        this.detailBBKTeknik = new Array<DetailBBKTeknik>();
        this.returItem = new ReturItem();
        this.jumlah = 0;
        this.isDiscPersen = false;
        this.retur = new Retur();
        this.retur.disc = 0;
        this.retur.total = 0;
        this.retur.grandtot = 0;
        this.checkPPN = false;
        this.retur.tgl_coretax = new Date();
        if (this.retur.disc === undefined) {
            this.retur.disc = 0;

        }
        if (this.retur.ppn === undefined) {
            this.retur.ppn = 0;
        }
        if (this.retur.grandtot === undefined) {
            this.retur.grandtot = 0;
        }
        if (this.retur.ppnbm === undefined) {
            this.retur.ppnbm = 0;
        }
        this.isEdit = true;
        this.isUpdate = false;
        this.spl = new MasterSupplier();
        this.dttax = new Date();
        this.pembulatan = false;
        this.lblpembualatan = 'Tidak';
        this.jnspembulatan = false;
        this.lbljnspembulatan = 'Bawah';
        this.pembulatan1 = false;
        this.lblpembualatan1 = 'Tidak';
        this.jnspembulatan1 = false;
        this.lbljnspembulatan1 = 'Bawah';
        this.noretur = '';
        this.jenis_retur = '';
        this.jenis_pv = null;
        this.taxno = '';
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        // this.contents = null;
        // this.contentTypes = null;
        this.sizePDF = 0;
    }

    loadEdit(id) {
        this.returService.find(id)
            .subscribe(
                (res: Retur) => {
                    this.retur = res;
                    this.noretur = res.noretur;
                    this.returItems = res.retur_item;
                    this.jumlah = res.total;
                    this.newSuplier = res.master_supplier;
                    this.spl = res.master_supplier;
                    this.selectedSuppCode = res.master_supplier.kd_suppnew;
                    if (this.retur.ppn > 0) {
                        this.checkPPN = true;
                    }
                    if (res.dttax != null) {
                        this.dttax = this.dateUtils
                            .convertDateTimeFromServer(res.dttax);
                    }
                    if (res.dtdoc != null) {
                        this.tgl = this.dateUtils
                            .convertDateTimeFromServer(res.dtdoc);
                    }
                    this.jenis_pv = res.is_pv;
                    this.taxno = res.taxno;

                }
            );
    }
    ngOnInit() {
        this.retur.disc = 0;
        this.retur.total = 0;
        this.retur.grandtot = 0;
        this.registerFromBBKLOV();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['edit'] === 'true') {
                    this.isEdit = true;
                    this.loadEdit(params['id']);
                    this.isUpdate = true;
                }
                if (params['edit'] === 'false') {
                    this.loadEdit(params['id']);
                    this.isEdit = false;
                }
            } else {
                // this.retur.dttax = new Date();
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.retur.noretur = 'XXXX/XXX-XXX-XX/XXX/XXXX';
        this.noretur = 'XXXX/XXX-XXX-XX/XXX/XXXX';

    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.selectedSuppCode = this.newSuplier.kd_suppnew;
    }

    cekFP(note: string) {
        if (note === '' || note === undefined || note === null) {
        } else {
            this.returService.cekFP(note).subscribe(
                (res: Retur) => {
                    if (res.noretur === null || res.noretur === undefined || res.noretur === '') {

                    } else {
                        alert(`No. Faktur Pajak Telah Digunakan Pada No Retur ${res.noretur}`);
                    }
                }
            )
        }
    }
    // getBBKFromLOV() {
    //     this.uionBBkService.values.subscribe(
    //         (res: UnionBBK) => {
    //             console.log('Cek data BBK = ', res);
    //             if (res.div === 'TEKNIK') {
    //                 this.uionBBkService.findDetailBBKTeknik({
    //                     query: 'nobbk:' + res.no_bbk
    //                 })
    //                     .subscribe(
    //                         (resTek: ResponseWrapper) => {
    //                             this.detailBBKTeknik = resTek.json;
    //                             this.detailBBKTeknik.forEach(
    //                                 (e) => {
    //                                     this.returItem = new ReturItem();
    //                                     this.returItem.productcode = e.kd_barang;
    //                                     this.returItem.qty = e.qty;
    //                                     this.returItem.uom = e.satuan;
    //                                     this.returItem.nobbk = e.no_bbk;
    //                                     this.returItem.productname = e.nama_barang;
    //                                     this.returItem.div = 'TEK';
    //                                     this.returItems = [...this.returItems, this.returItem];
    //                                 }
    //                             )
    //                         }
    //                     )
    //                 this.uionBBkService.values.observers = [];
    //             }
    //             if (res.div !== 'TEKNIK') {
    //                 this.uionBBkService.findDetailBBK({
    //                     query: 'nobbk:' + res.no_bbk
    //                 })
    //                     .subscribe(
    //                         (resNonTek: ResponseWrapper) => {
    //                             this.detailBBK = resNonTek.json;
    //                             this.detailBBK.forEach(
    //                                 (e) => {
    //                                     this.returItem = new ReturItem();
    //                                     this.returItem.productname = e.nama_bahan;
    //                                     this.returItem.productcode = e.kd_bahan;
    //                                     this.returItem.qty = e.qty;
    //                                     this.returItem.uom = e.satuan;
    //                                     this.returItem.nobbk = e.no_bbk;
    //                                     this.returItems = [...this.returItems, this.returItem];
    //                                 }
    //                             )
    //                         }
    //                     )
    //                 this.uionBBkService.values.observers = [];
    //             }
    //         }
    //     )
    // }
    getBBKFromLOV() {
        this.uionLHPService.values.subscribe(
            (resx: UnionLHP[]) => {
                resx.forEach((res) => {
                    this.loadingService.loadingStart();
                    this.jenis_retur = res.jenis_retur;
                    this.returService.queryLHP({
                        query: 'nolhp:' + res.nolhp + '|kdbahan:' + res.kdbahan + '|jenisklaim:' + res.jenisklaim + '|tipe:NR'
                    })
                        .subscribe(
                            (resTek: ResponseWrapper) => {
                                this.returItem = new ReturItem();
                                this.returItem = resTek.json;
                                this.returItems = [...this.returItems, this.returItem];
                                this.masterSupplierService.findByCode(this.returItem.suppcode)
                                    .subscribe(
                                        (resSPL: MasterSupplier) => {
                                            this.spl = resSPL;
                                            this.retur.suppcode = resSPL.kd_suppnew;
                                            this.retur.suppname = resSPL.nmSupplier;
                                        })
                                this.loadingService.loadingStop();
                            },
                            (err) => {
                                this.toasterService.showToaster('Info', 'Error', 'Terjadi Error Silahkan Refresh Halaman');
                                this.loadingService.loadingStop();
                            }
                        )
                    this.uionLHPService.values.observers = [];
                })
            }
        )
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/header-bbks/lov'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
            }
        });
        // this.openLOV();
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.BBKs = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    batal() {
        this.modalBBK = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadLOV();
    }
    registerFromBBKLOV() {
        this.eventSubscriberBBKLOV = this.eventManager.subscribe('unionLHPLovModification', () => this.getBBKFromLOV());
    }

    calculateTotalPrice(row: ReturItem) {
        this.jumlah = 0;
        this.retur.grandtot = 0;
        row.totalprice = row.qty * row.price;
        this.returItems.forEach(
            (e) => {
                this.jumlah += e.totalprice;
            }
        );
        if (this.pembulatan1 === false) {
            this.jnspembulatan1 = false
            this.lblpembualatan1 = 'Tidak';
        }
        if (this.pembulatan1 === true) {
            this.lblpembualatan1 = 'Ya';
        }
        if (this.jnspembulatan1 === false) {
            this.lbljnspembulatan1 = 'Bawah';
        }
        if (this.jnspembulatan1 === true) {
            this.lbljnspembulatan1 = 'Atas';
        }

        if (this.pembulatan1 === false) {
            this.jumlah = this.jumlah;
        }
        if (this.pembulatan1 === true) {
            if (this.jnspembulatan1 === false) {
                this.jumlah = Math.floor(this.jumlah);
            }
            if (this.jnspembulatan1 === true) {
                this.jumlah = Math.ceil(this.jumlah);
            }
        }
        if (this.checkPPN === true) {
            this.retur.ppn = (this.jumlah * 11 / 100);
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
        }
        this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;

    }

    calculateTotalPrice1() {
        this.jumlah = 0;
        this.retur.grandtot = 0;
        this.returItems.forEach(
            (e) => {
                this.jumlah += e.totalprice;
            }
        );
        if (this.pembulatan1 === false) {
            this.jnspembulatan1 = false
            this.lblpembualatan1 = 'Tidak';
        }
        if (this.pembulatan1 === true) {
            this.lblpembualatan1 = 'Ya';
        }
        if (this.jnspembulatan1 === false) {
            this.lbljnspembulatan1 = 'Bawah';
        }
        if (this.jnspembulatan1 === true) {
            this.lbljnspembulatan1 = 'Atas';
        }

        if (this.pembulatan1 === false) {
            this.jumlah = this.jumlah;
        }
        if (this.pembulatan1 === true) {
            if (this.jnspembulatan1 === false) {
                this.jumlah = Math.floor(this.jumlah);
            }
            if (this.jnspembulatan1 === true) {
                this.jumlah = Math.ceil(this.jumlah);
            }
        }
        if (this.checkPPN === true) {
            this.retur.ppn = (this.jumlah * 11 / 100);
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
        }
        this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
    }

    countGrandTotal() {
        this.retur.grandtot = this.jumlah + this.retur.ppnbm + this.retur.ppn;
    }

    checkIsPPn() {
        if (this.pembulatan === false) {
            this.jnspembulatan = false
            this.lblpembualatan = 'Tidak';
        }
        if (this.pembulatan === true) {
            this.lblpembualatan = 'Ya';
        }
        if (this.jnspembulatan === false) {
            this.lbljnspembulatan = 'Bawah';
        }
        if (this.jnspembulatan === true) {
            this.lbljnspembulatan = 'Atas';
        }

        if (this.checkPPN === true) {
            if (this.pembulatan === false) {
                this.retur.ppn = (this.jumlah * 11 / 100);
            }
            if (this.pembulatan === true) {
                if (this.jnspembulatan === false) {
                    this.retur.ppn = Math.floor((this.jumlah * 11 / 100));
                }
                if (this.jnspembulatan === true) {
                    this.retur.ppn = Math.ceil((this.jumlah * 11 / 100));
                }
            }
            this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
            this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
        }
    }

    createRetur() {
        this.loadingService.loadingStart();
        // this.retur.dttax = this.tgl
        this.retur.dttax = this.dttax;
        this.retur.dtdoc = this.tgl;
        this.retur.jenisklaim = 'NR';
        this.retur.jenis_retur = this.jenis_retur;
        this.retur.taxno = this.taxno;
        this.retur.is_pv = this.jenis_pv;
        if (this.retur.ppnbm == null || this.retur.ppnbm === undefined) {
            this.retur.ppnbm = 0;
        }
        if (this.retur.ppn == null || this.retur.ppn === undefined) {
            this.retur.ppn = 0;
        }
        if (this.spl === null || this.spl === undefined) {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'DATA SUPPLIER MASIH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.returItems.length <= 0) {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'DATA ITEM MASIH KOSONG');
            this.loadingService.loadingStop();
        } else {
            this.retur.total = this.jumlah;
            this.retur.retur_item = this.returItems;
            this.returService.create(this.retur).subscribe((res) => {
                this.backMainPage();
                this.loadingService.loadingStop();
            });
        }
    }

    updateRetur() {
        this.loadingService.loadingStart();
        if (this.retur.contentContentType === null || this.retur.contentContentType === '' || this.retur.contentContentType === undefined) {
            this.toasterService.showToaster('PERHATIAN !!', 'LAMPIRAN KOSONG', 'LAMPIRAN CORETAX TIDAK BOLEH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.retur.no_coretax === null || this.retur.no_coretax === '' || this.retur.no_coretax === undefined) {
            this.toasterService.showToaster('PERHATIAN !!', 'NO CORETAX KOSONG', 'NO CORETAX TIDAK BOLEH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.sizePDF > this.maxFileSize) {
            this.toasterService.showToaster('PERHATIAN !!', 'FILE LAMPIRAN OVERSIZE', 'FILE LAMPIRAN LEBIH BESAR DARI 10 MB');
            this.loadingService.loadingStop();
        } else {
            this.returService.update(this.retur).subscribe((res) => {
                this.toasterService.showToaster('PERHATIAN !!', 'UPDATE CORETAX', 'DATA CORETAX BERHASIL DI UPDATE KE NOTA RETUR');
                this.backMainPage();
            });
        }
    }

    backMainPage() {
        this.router.navigate(['/approval-payment-retur']);
    }

    public deleteRow(data: ReturItem) {
        // Cek apakah data yang dihapus adalah data terakhir
        if (this.returItems.length === 1 && this.returItems[0].idreturitem === data.idreturitem) {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Apakah Anda yakin akan menghapus data Terakhir Ini, ini juga akan menghapus data Suppliernya juga ??',
                accept: () => {
                    this.returService.deleteRow(data).subscribe((e) => alert('Sukses dihapus'));
                    this.returItems = this.returItems.filter(function dataSama(items) {
                        return items.idreturitem !== data.idreturitem;
                    });
                    this.calculateTotalPrice(data);
                    this.spl = new MasterSupplier();
                    this.retur.suppname = '';
                    this.retur.suppcode = '';
                    this.retur.picspl = 'Bapak / Ibu';
                    this.spl.nmSupplier = '';
                    this.spl.almtSupplier1 = '';
                    this.spl.almtSupplier2 = '';
                    this.spl.telp = '';
                    this.spl.fax = '';
                }
            });
        } else {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Apakah Anda yakin akan menghapus data ini ??',
                accept: () => {
                    this.returService.deleteRow(data).subscribe((e) => alert('Sukses dihapus'));
                    this.returItems = this.returItems.filter(function dataSama(items) {
                        return items.idreturitem !== data.idreturitem;
                    });
                    this.calculateTotalPrice(data);
                }
            });
        }
    }

    openPdf(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;

        bbms = rowData.nolhp.replace(/\//g, '-');
        const filter_data = 'nolhp:' + bbms;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_lhp/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

    openPdf1(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;

        bbms = rowData.nobbk.replace(/\//g, '-');
        const pecah = bbms.split('-');

        const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbk_supplier/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        // this.dataUtils.clearInputImage(this.joborders, this.elementRef, field, fieldContentType, idInput);
        this.retur.content = null;
        this.retur.contentContentType = null;
        this.sizePDF = 0;
    }

    openPDF(content: any, contentType: any) {
        const byteCharacters = atob(this.retur.content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': this.retur.contentContentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        // const win = window.open(content);
        window.open(URL.createObjectURL(blobContent), '_blank');
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            if (bytes >= 5242880 && bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
                const file = this.compressFile(event);
                this.dataUtils.setFileData(file, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
                this.sizePDF = event.target.files[0].size;
            } else if (bytes < 5242880) {
                this.dataUtils.setFileData(event, entity, field, isImage);
                this.isValidImage = false;
                this.isInsertFile = 'valid';
                this.sizePDF = event.target.files[0].size;
            }
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
            alert('Ukuran File Melebihi Batas 10MB, Silahkan Input Attachment dibawah 10 MB')
            this.sizePDF = 0;
        }
    }

    async compressFile(fileInput: any): Promise<void> {
        const file: File = fileInput.target.files[0];

        const compressedBlob: Blob = await this.fileCompressionService.compressFile(file);
    }
}
