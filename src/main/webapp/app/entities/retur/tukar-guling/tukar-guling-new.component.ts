import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiAlertService, JhiDataUtils, JhiDateUtils, JhiEventManager } from 'ng-jhipster';
import { ConfirmationService, LazyLoadEvent, TRISTATECHECKBOX_VALUE_ACCESSOR } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Retur, UnionBBK, ReturService, UnionLHP } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ReturDocument, ReturDocumentService } from '../../retur-document';
import { ReturItem } from '../../retur-item';
import { DetailBBK, DetailBBKTeknik, UnionBBKService } from '../../union-bbk';
import { UnionLHPService } from '../../union-bbk/union-lhp.service';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-tukar-guling-new',
    templateUrl: './tukar-guling-new.component.html'
})
export class TukarGulingNewComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: Retur[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selectedSuppCode: string;
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    filteredSuppliers: any[];
    tgl: Date;
    modalBBK: boolean;
    BBKs: UnionBBK[];
    itemsDataPerPage: any;
    selectedItems: UnionBBK;
    eventSubscriberBBKLOV: Subscription;
    returItems: ReturItem[];
    detailBBK: DetailBBK[];
    detailBBKTeknik: DetailBBKTeknik[];
    returItem: ReturItem;
    jumlah: number;
    isDiscPersen: boolean;
    retur: Retur;
    checkPPN: boolean;
    private subscription: Subscription;
    isEdit: boolean;
    isUpdate: boolean;
    spl: MasterSupplier;
    returDocument: ReturDocument;
    returDocuments: ReturDocument[];
    nmbahan: string;
    nobatch: string;
    nopalet: string;
    jmlpalet: number;
    jmltdksesuai: number;
    jenis_retur: string;
    noretur: string;
    constructor(
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private masterSupplierService: MasterSupplierService,
        private returService: ReturService,
        private uionLHPService: UnionLHPService,
        private toasterService: ToasterService,
        private route: ActivatedRoute,
        private loadingService: LoadingService,
        private dataUtils: JhiDataUtils,
        private confirmationService: ConfirmationService,
        private returDocumentService: ReturDocumentService,
        private dateUtils: JhiDateUtils,
        private reportUtilService: ReportUtilService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.tgl = new Date();
        this.modalBBK = false;
        this.itemsDataPerPage = 0;
        this.selectedItems = new UnionBBK();
        this.returItems = new Array<ReturItem>();
        this.detailBBK = new Array<DetailBBK>();
        this.detailBBKTeknik = new Array<DetailBBKTeknik>();
        this.returItem = new ReturItem();
        this.jumlah = 0;
        this.isDiscPersen = false;
        this.retur = new Retur();
        this.retur.disc = 0;
        this.retur.total = 0;
        this.retur.grandtot = 0;
        this.jenis_retur = '';
        this.checkPPN = false;
        if (this.retur.disc === undefined) {
            this.retur.disc = 0;

        }
        if (this.retur.ppn === undefined) {
            this.retur.ppn = 0;
        }
        if (this.retur.grandtot === undefined) {
            this.retur.grandtot = 0;
        }
        if (this.retur.ppnbm === undefined) {
            this.retur.ppnbm = 0;
        }
        this.isEdit = true;
        this.isUpdate = false;
        this.spl = new MasterSupplier();
        this.returDocument = new ReturDocument();
        this.returDocuments = new Array<ReturDocument>();
        this.nmbahan = '';
        this.nobatch = '';
        this.nopalet = '';
        this.jmlpalet = 0;
        this.jmltdksesuai = 0;
        this.noretur = '';
    }

    loadEdit(id) {
        this.loadingService.loadingStart();
        this.returService.find(id)
            .subscribe(
                (res: Retur) => {
                    this.retur = res;
                    this.noretur = res.noretur;
                    this.returItems = res.retur_item;
                    this.jumlah = res.total;
                    this.spl = res.master_supplier;
                    this.selectedSuppCode = res.master_supplier.kd_suppnew;
                    if (this.retur.ppn > 0) {
                        this.checkPPN = true;
                    };
                    this.returDocumentService.queryByIdRetur(res.idretur)
                        .subscribe(
                            (resDoc: ResponseWrapper) => {
                                this.returDocuments = resDoc.json;
                            }
                        )
                    this.loadingService.loadingStop();

                },
                (err) => {
                    this.loadingService.loadingStop();
                }
            );
    }
    ngOnInit() {
        this.retur.noretur = 'XXXX/XXX-XXX-XX/XXX/XXXX';
        this.noretur = 'XXXX/XXX-XXX-XX/XXX/XXXX';
        this.retur.srtheader = 'Dengan Hormat, ' +
            '\r\n' +
            'Bersama ini kami informasikan bahwa ditemukan barang/material yang Tidak Memenuhi Syarat, dengan rincian sebagai berikut :'
        this.retur.srtfooter =
            'Kami berharap barang/material yang Tidak Memenuhi Syarat tersebut dapat segera ditarik, Serta melakukan penggantian barang yang sesuai dengan permintaan kami. ' +
            'Demikian informasi dari kami, atas kerjasamanya kami ucapkan terima kasih. ' +
            '\r\n\r\nKet : ' +
            '\r\n1. Penarikan barang maksimal 15 HK dari terima email Surat Klaim ini. ' +
            '\r\n2. Maksimal batas penarikan 5 HK sebelum akhir bulan' +
            '\r\n\r\nNOTE : Mohon Surat ini WAJIB dibawa pada saat penarikan barang TLU dan harap konfirmasi minimal H-1 sebelum penarikan.';
        // this.retur.srtcc = '1.\r\n' +
        //     '2.\r\n' +
        //     '3.\r\n' +
        //     '4.\r\n' +
        //     '5.';
        this.retur.srtfooter2 = '1.\r\n' +
            '2.\r\n' +
            '3.\r\n' +
            '4.\r\n' +
            '5.';
        this.retur.picspl = 'Bapak / Ibu';
        this.retur.disc = 0;
        this.retur.total = 0;
        this.retur.grandtot = 0;
        this.retur.srtcc = 'Bp. HKW, Ibu PJA, Bp. MJS'
        this.registerFromBBKLOV();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['edit'] === 'true') {
                    this.isEdit = true;
                    this.loadEdit(params['id']);
                    this.isUpdate = true;
                }
                if (params['edit'] === 'false') {
                    this.loadEdit(params['id']);
                    this.isEdit = false;
                }
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.selectedSuppCode = this.newSuplier.kd_suppnew;
    }
    getBBKFromLOV() {
        this.uionLHPService.values.subscribe(
            (res: UnionLHP[]) => {
                this.loadingService.loadingStart();
                console.log('Cek data BBK = ', res);
                this.jenis_retur = res[0].jenis_retur;
                res.forEach((e) => {
                    if (e.nolhp !== undefined && e.nolhp !== '') {
                        this.returService.queryLHP({
                            query: 'nolhp:' + e.nolhp + '|kdbahan:' + e.kdbahan + '|jenisklaim:' + e.jenisklaim + '|tipe:TG'
                        })
                            .subscribe(
                                (resTek: ResponseWrapper) => {
                                    const data = resTek.json;
                                    data.forEach((item) => {
                                        this.returItem = new ReturItem();
                                        this.returItem = item;
                                        this.returItem.dtsj = this.dateUtils.convertDateTimeFromServer(item.dtsj);
                                        this.returItem.dtpo = this.dateUtils.convertDateTimeFromServer(item.dtpo);
                                        this.returItem.dtproduksi = this.dateUtils.convertDateTimeFromServer(item.dtproduksi);
                                        this.returItems = [...this.returItems, this.returItem];
                                        this.nmbahan += this.returItem.productname + ', ';
                                        this.nobatch += this.returItem.nobatch + ', ';
                                        this.nopalet += this.returItem.nopalet + ', ';
                                        this.jmlpalet += this.returItem.jmlpalet;
                                        this.jmltdksesuai += this.returItem.qty;

                                        this.masterSupplierService.findByCode(this.returItem.suppcode)
                                            .subscribe(
                                                (resSPL: MasterSupplier) => {
                                                    this.spl = resSPL;
                                                    this.retur.picspl = 'Bapak / Ibu ' + this.spl.person1;
                                                })

                                    });
                                    this.loadingService.loadingStop();
                                },
                                (err) => {
                                    this.toasterService.showToaster('Info', 'Error', 'Terjadi Error Silahkan Refresh Halaman');
                                    this.loadingService.loadingStop();
                                }
                            )
                    }
                });
                this.uionLHPService.values.observers = [];
            }
        )
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/header-bbks/lov'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
            }
        });
        // this.openLOV();
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.BBKs = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    batal() {
        this.modalBBK = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadLOV();
    }
    registerFromBBKLOV() {
        this.eventSubscriberBBKLOV = this.eventManager.subscribe('unionLHPLovModification', () => this.getBBKFromLOV());
    }

    calculateTotalPrice(row: ReturItem) {
        this.jumlah = 0;
        this.retur.grandtot = 0;
        row.totalprice = row.qty * row.price;
        console.log('INI QTY = ', row.qty);
        console.log('INI PRICE = ', row.price);
        console.log('INI TOTAL PRICE = ', row.totalprice);
        this.returItems.forEach(
            (e) => {
                this.jumlah += e.totalprice;
            }
        );
        if (this.checkPPN === true) {
            this.retur.ppn = (this.jumlah * 10 / 100);
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
        }
        this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;

    }

    countGrandTotal() {
        this.retur.grandtot = this.jumlah + this.retur.ppnbm + this.retur.ppn;
    }

    checkIsPPn() {
        if (this.checkPPN === true) {
            this.retur.ppn = (this.jumlah * 10 / 100);
            this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
            this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
        }
    }

    createRetur() {
        this.loadingService.loadingStart();
        if (this.spl === null || this.spl === undefined) {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'DATA SUPPLIER MASIH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.returItems.length <= 0) {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'DATA ITEM MASIH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.retur.hal === null || this.retur.hal === undefined || this.retur.hal === '') {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'HAL TIDAK BOLEH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.retur.srtreff === null || this.retur.srtreff === undefined || this.retur.srtreff === '') {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'REFERENSI TIDAK BOLEH KOSONG');
            this.loadingService.loadingStop();
        } else {
            this.retur.retur_documents = this.returDocuments;
            this.retur.retur_item = new Array<ReturItem>();
            this.retur.jenisklaim = 'TG';
            this.retur.jenis_retur = this.jenis_retur;
            if (this.retur.ppnbm == null || this.retur.ppnbm === undefined) {
                this.retur.ppnbm = 0;
            }
            if (this.retur.ppn == null || this.retur.ppn === undefined) {
                this.retur.ppn = 0;
            } else {
                this.retur.suppname = this.spl.nmSupplier;
                this.retur.suppcode = this.spl.kd_suppnew;
                this.retur.total = this.jumlah;
                this.retur.retur_item = this.returItems;
                // this.retur.retur_item = [...this.returItems, this.returItem]
                this.returService.create(this.retur).subscribe((res) => {
                    this.backMainPage();
                    this.loadingService.loadingStop();
                });
            }
        }
    }

    updateRetur() {
        this.loadingService.loadingStart();
        if (this.spl === null || this.spl === undefined) {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'DATA SUPPLIER MASIH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.returItems.length <= 0) {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'DATA ITEM MASIH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.retur.hal === null || this.retur.hal === undefined || this.retur.hal === '') {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'HAL TIDAK BOLEH KOSONG');
            this.loadingService.loadingStop();
        } else if (this.retur.srtreff === null || this.retur.srtreff === undefined || this.retur.srtreff === '') {
            this.toasterService.showToaster('PERHATIAN !!', 'DATA KOSONG', 'REFERENSI TIDAK BOLEH KOSONG');
            this.loadingService.loadingStop();
        } else {
            this.retur.retur_documents = this.returDocuments;
            this.retur.retur_item = new Array<ReturItem>();
            if (this.retur.ppnbm == null || this.retur.ppnbm === undefined) {
                this.retur.ppnbm = 0;
            }
            if (this.retur.ppn == null || this.retur.ppn === undefined) {
                this.retur.ppn = 0;
            } else {
                this.retur.suppname = this.spl.nmSupplier;
                this.retur.suppcode = this.spl.kd_suppnew;
                this.retur.total = this.jumlah;
                this.retur.retur_item = this.returItems;
                // this.retur.retur_item = [...this.returItems, this.returItem]
                this.returService.update(this.retur).subscribe((res) => {
                    this.backMainPage();
                    this.loadingService.loadingStop();
                });
            }
        }
    }

    backMainPage() {
        this.router.navigate(['/tukar-guling']);
    }
    setFileData(event, entity, field, isImage) {
        if (event && event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            if (isImage && !/^image\//.test(file.type)) {
                return;
            }
            this.dataUtils.toBase64(file, (base64Data) => {
                entity[field] = base64Data;
                entity[`${field}ContentType`] = file.type;
                // nama file ambil dari file.name
                // doc ambil dari entity.doc
                // contenttype ambil dari entity.docContentType
                this.returDocument = new ReturDocument();
                this.returDocument.id = this.returDocuments.length + 1;
                this.returDocument.idretur = null;
                this.returDocument.doc = entity.doc;
                this.returDocument.doctype = entity.docContentType;
                this.returDocument.docname = file.name;
                this.returDocuments = [...this.returDocuments, this.returDocument];
                // console.log('ini adalah file nya coba cek => ', file);
                // console.log('ini adalah entity nya coba cek => ', entity);
            });
        }
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    public deleteRow(data: ReturItem) {
        // Cek apakah data yang dihapus adalah data terakhir
        if (this.returItems.length === 1 && this.returItems[0].idreturitem === data.idreturitem) {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Apakah Anda yakin akan menghapus data Terakhir Ini, ini juga akan menghapus data Suppliernya juga ??',
                accept: () => {
                    this.returService.deleteRow(data).subscribe((e) => alert('Sukses dihapus'));
                    this.returItems = this.returItems.filter(function dataSama(items) {
                        return items.idreturitem !== data.idreturitem;
                    });
                    this.calculateTotalPrice(data);
                    this.spl = new MasterSupplier();
                    this.retur.suppname = '';
                    this.retur.suppcode = '';
                    this.retur.picspl = 'Bapak / Ibu';
                    this.spl.nmSupplier = '';
                    this.spl.almtSupplier1 = '';
                    this.spl.almtSupplier2 = '';
                    this.spl.telp = '';
                    this.spl.fax = '';
                }
            });
        } else {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Apakah Anda yakin akan menghapus data ini ??',
                accept: () => {
                    this.returService.deleteRow(data).subscribe((e) => alert('Sukses dihapus'));
                    this.returItems = this.returItems.filter(function dataSama(items) {
                        return items.idreturitem !== data.idreturitem;
                    });
                    this.calculateTotalPrice(data);
                }
            });
        }
    }

    openPdf(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;
        let jenisLHP: number;
        bbms = rowData.nolhp.replace(/\//g, '-');
        if (bbms.includes('LPPM') || bbms.includes('LPRM') || bbms.includes('LPGM')) {
            jenisLHP = 1;
        } else if (bbms.includes('LPMR') || bbms.includes('LPMP')) {
            jenisLHP = 2;
        }

        const filter_data = 'nolhp:' + bbms + '|jenisLHP:' + jenisLHP;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_lhp/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }
}
