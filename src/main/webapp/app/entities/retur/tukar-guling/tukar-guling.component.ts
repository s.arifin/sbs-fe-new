import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Retur, ReturService, ReturVM } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';

@Component({
    selector: 'jhi-tukar-guling',
    templateUrl: './tukar-guling.component.html'
})
export class TukarGulingComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: ReturVM[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtfrom: Date;
    dtthru: Date;
    kdSPL: string;
    newSuplier: any;
    filteredSuppliers: any[];
    isFilter: boolean;
    jenis: string;
    pic: string;
    selected: ReturVM[];
    loginName: string;
    jenisStatus: Array<object> = [
        { label: 'All', value: '' },
        { label: 'New', value: 'New' },
        { label: 'Request Approve', value: 'Request Approve' },
        { label: 'Approved', value: 'Approve' },
        { label: 'Sudah BBK', value: 'Sudah BBK' },
        { label: 'Not Approved', value: 'Not Approve' },
    ];
    picJenis: Array<object> = [
        { label: 'All', value: '' },
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' },
        { label: 'EVN', value: 'EVN' },
    ];
    isEmail: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Sudah', value: 'Sudah' },
        { label: 'Belum', value: 'Belum' }
    ];
    selectedMail: string;
    constructor(
        private returService: ReturService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        protected confirmationService: ConfirmationService,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.isFilter = false;
        this.pic = '';
        this.jenis = '';
        this.selectedMail = ''
        this.selected = new Array<ReturVM>();
        this.loginName = this.principal.getUserLogin();
        this.page = 0;

    }

    loadAll() {
        this.loadingService.loadingStart();
        this.loadingService.loadingStart();
        if (this.isFilter) {
            const waktu = '23:59:59.000';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            if (this.currentSearch) {
                this.returService.queryKlaim({
                    page: this.page,
                    query: 'jenisklaim:TG' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic + '|email:' + this.selectedMail + '|is_pv:' + undefined,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
                this.returService.queryKlaim({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:TG' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic + '|email:' + this.selectedMail + '|is_pv:' + undefined,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        } else {
            if (this.currentSearch) {
                this.returService.queryKlaim({
                    page: this.page,
                    query: 'jenisklaim:TG' + '|nolhp:' + this.currentSearch + '|dtfrom:|dtthru:|kdsup:|jenis_status:|pic:|email:|is_pv:',
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryKlaim({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:TG' + '|nolhp:' + this.currentSearch + '|dtfrom:|dtthru:|kdsup:|jenis_status:|pic:|email:|is_pv:',
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/tukar-guling'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/tukar-guling', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/tukar-guling', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInReturs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Retur) {
        return item.id;
    }
    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe('returListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.returs = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    cetakRetur(retur: Retur) {
        // if (retur.suppcode === 'PUNITE') {
        const filter_data = 'idretur:' + retur.idretur;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/report_sr/pdf', { filterData: filter_data });
        // }
        // if (retur.suppcode !== 'PUNITE') {
        //     const filter_data = 'idretur:' + retur.idretur;
        //     this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tukar_guling_general/pdf', { filterData: filter_data });
        // }
    }

    downloadRetur(retur: Retur) {
        // if (retur.suppcode === 'PUNITE') {
        const filter_data = 'idretur:' + retur.idretur;
        const noretur = retur.noretur;
        this.reportUtilService.downloadFileWithName(`Surat Tukar Guling ${noretur}`, process.env.API_C_URL + '/api/report/report_sr/pdf', { filterData: filter_data });
        // }
        // if (retur.suppcode !== 'PUNITE') {
        //     const filter_data = 'idretur:' + retur.idretur;
        //     this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/tukar_guling_general/pdf', { filterData: filter_data });
        // }
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    filter() {
            this.isFilter = true;
            this.loadAll();
    }
    reset() {
        this.currentSearch = '';
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.jenis = '';
        this.pic = '';
                this.newSuplier = new MasterSupplier();
        this.loadAll();
    }
    setstatus(idstatus: number, rowData: Retur) {
        if (idstatus === 13) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Cancel Retur ini ??',
                accept: () => {
                    this.returService.changeStatus(rowData, idstatus)
                        .subscribe(
                            (res) => {
                                if (res['messageResponse'] === '  : ') {
                                    alert('Berhasil Cancel');
                                } else {
                                    alert(`Beberapa BBK sudah ada Pengganti BBM Retur : ${res['messageResponse']}`);
                                }
                                this.loadAll()
                            },
                        )
                }
            });
        } else {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Request Approve ??',
                accept: () => {
                    this.returService.changeStatus(rowData, idstatus)
                        .subscribe(
                            (res) => {
                                this.loadingService.loadingStart();
                                this.loadAll();
                            }
                        );
                }
            });
        }

    }

    setstatus1(idstatus: number, rowData: Retur) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Cancel Tukar Guling ??',
            accept: () => {
                this.returService.changeStatus(rowData, idstatus)
                    .subscribe(
                        (res) => {
                            if (res['messageResponse'] === '  : ') {
                                alert('Berhasil Cancel');
                            } else {
                                alert(`Beberapa BBK sudah ada Pengganti BBM Retur : ${res['messageResponse']}`);
                            }
                            this.loadAll()
                        },
                    )
            }
        });
    }

    setmail() {
        this.loadingService.loadingStart();
        this.returService.setMailPO(this.selected).subscribe(
            (res) => {
                this.loadAll();
                this.selected = new Array<ReturVM>();
                alert('Sukses Update Sudah Email');
            }
        )
        this.loadingService.loadingStop();
    }

    approveEdit() {
        this.loadingService.loadingStart();
        const allSame = this.selected.every((item) => item.pic === this.loginName);
        if (allSame) {
            const hasStatus13 = this.selected.some((item) =>
                item.status_desc === 'Cancel' || item.status_desc === 'Close' || item.status_desc === 'BBK Sebagian'
            );
            if (hasStatus13) {
                alert('Terdapat item dengan status Cancel, Close atau BBK Sebagian. Tidak dapat melanjutkan proses.');
                this.loadingService.loadingStop();
            } else {
                this.returService.approveEditRetur(this.selected).subscribe(
                    (res) => {
                        alert('Berhasil Approve Edit');
                        this.loadAll();
                        this.selected = new Array<ReturVM>();
                        this.loadingService.loadingStop();
                    }
                );
            }
        } else {
            alert('Terdapat Retur Yang Memiliki Otorisasi Berbeda');
            this.loadingService.loadingStop();
        }
    }
}
