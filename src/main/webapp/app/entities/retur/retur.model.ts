import { MasterSupplier } from '../master-supplier';
import { ReturDocument } from '../retur-document';
import { ReturItem } from '../retur-item';
import { BaseEntity } from './../../shared';

export class Retur implements BaseEntity {
    constructor(
        public id?: number,
        public idretur?: number,
        public noretur?: string,
        public dtcreated?: any,
        public createdby?: string,
        public dtmodified?: any,
        public modifiedby?: string,
        public nobbk?: string,
        public tglbbk?: any,
        public suppcode?: string,
        public total?: number,
        public disc?: number,
        public ppn?: number,
        public grandtot?: number,
        public taxno?: string,
        public ppnbm?: number,
        public retur_item?: ReturItem[],
        public master_supplier?: MasterSupplier,
        public dttax?: Date,
        public jenisklaim?: string,
        public hal?: string,
        public retur_status?: ReturStatus,
        public status_desc?: string,
        public srtheader?: string,
        public srtfooter?: string,
        public picspl?: string,
        public suppname?: string,
        public srtcc?: string,
        public srtreff?: string,
        public retur_documents?: ReturDocument[],
        public dtdoc?: Date,
        public messageResponse?: string,
        public srtfooter2?: string,
        public jenis_retur?: string,
        public is_pv?: number,
        public pic?: string,
        public no_coretax?: string,
        public tgl_coretax?: Date,
        public contentContentType?: string,
        public content?: any,
        public keterangan_coretax?: string,
        public note_revert?: string
    ) {
        this.retur_item = new Array<ReturItem>();
        this.master_supplier = new MasterSupplier();
        // tslint:disable-next-line: no-use-before-declare
        this.retur_status = new ReturStatus();
    }
}

export class ReturVM implements BaseEntity {
    constructor(
        public id?: number,
        public idretur?: number,
        public noretur?: string,
        public dtcreated?: any,
        public createdby?: string,
        public suppcode?: string,
        public status_desc?: string,
        public nm_supplier?: string,
        public is_email?: string,
        public pic?: string,
        public statustype?: number,
        public receiptno?: string,
        public grandtot?: number,
        public keterangan_coretax?: string,
        public note_revert?: string,
        public no_coretax?: string
    ) {
    }
}

export class UnionLHP {
    constructor(
        public idlhp?: any,
        public nolhp?: string,
        public jenisklaim?: string,
        public nobbm?: string,
        public nmbahan?: string,
        public kdbahan?: string,
        public qty?: number,
        public pic?: string,
        public tanggallhp?: string,
        public status_surat?: string,
        public kd_suppnew?: string,
        public nm_supplier?: string,
        public jenis_retur?: string,
        public nobbk?: string,
        public no_surat?: string
    ) {
    }
}

export class ReturStatus {
    constructor(
        public idstatus?: any,
        public idretur?: any,
        public statustype?: number,
        public dtfrom?: Date,
        public dtthru?: Date,
        public author?: string,
    ) {
    }
}

export class DetailLHP {
    constructor(
        public nolhp?: string,
        public kdbahan?: string,
        public namabahan?: string,
        public qtytolak?: number,
        public satuan?: string,
        public keterangan?: string,
        public nobbm?: string,
        public jenisklaim?: string,
        public tglperiksa?: Date,
        public adm_pbl?: string
    ) { }
}

export class MonitoringBBK {
    constructor(
        public id?: number,
        public idretur?: any,
        public noretur?: string,
        public dtcreated?: Date,
        public createdby?: string,
        public suppcode?: string,
        public nm_supplier?: string,
        public jenisklaimdesc?: string,
        public jenisklaim?: string,
        public statustype?: number,
        public returStatus?: string,
        public pic?: string
    ) {

    }
}

export class ItemBBK {
    constructor(
        public id?: number,
        public idretur?: any,
        public idreturitem?: any,
        public productcode?: string,
        public productname?: string,
        public uom?: string,
        public nolhp?: string,
        public no_bbk?: string,
        public no_sj?: string,
        public qty_tlu?: Number,
        public no_bbm?: string,
        public tgl_bbm?: Date,
        public tgl_sj?: Date,
        public qtyBBM?: Number,
    ) {

    }
}
