import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Retur } from './retur.model';
import { ReturPopupService } from './retur-popup.service';
import { ReturService } from './retur.service';

@Component({
    selector: 'jhi-retur-dialog',
    templateUrl: './retur-dialog.component.html'
})
export class ReturDialogComponent implements OnInit {

    retur: Retur;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private returService: ReturService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.retur.id !== undefined) {
            this.subscribeToSaveResponse(
                this.returService.update(this.retur));
        } else {
            this.subscribeToSaveResponse(
                this.returService.create(this.retur));
        }
    }

    private subscribeToSaveResponse(result: Observable<Retur>) {
        result.subscribe((res: Retur) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Retur) {
        this.eventManager.broadcast({ name: 'returListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-retur-popup',
    template: ''
})
export class ReturPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returPopupService: ReturPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.returPopupService
                    .open(ReturDialogComponent as Component, params['id']);
            } else {
                this.returPopupService
                    .open(ReturDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
