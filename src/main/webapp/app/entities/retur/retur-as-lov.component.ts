import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Retur, ReturPopupService, ReturVM } from '.';
import { Principal, ResponseWrapper, ToasterService } from '../../shared';
import { ReturService } from './retur.service';

@Component({
    selector: 'jhi-retur-as-lov',
    templateUrl: './retur-as-lov.component.html'
})
export class ReturAsLovComponent implements OnInit, OnDestroy {

    returs: ReturVM[]
    selected: ReturVM[];
    currentAccount: any;
    isSaving: boolean;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    first: number;
    isDisable: boolean;
    tempTT: string;

    constructor(
        public returService: ReturService,
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private principal: Principal,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = 100;
        this.page = 1;
        this.predicate = 'idretur';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<Retur>();
        this.idInternal = this.principal.getIdInternal();
        this.first = 0;
        this.isDisable = false;
        this.tempTT = '';
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            console.log(params['jenis']);
            this.returService.query({
                query: 'noretur:' + this.currentSearch + '|jenisklaim:nr',
                page: this.page - 1,
                size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.selected = new Array<Retur>();
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new Array<Retur>();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: Retur) {
        return item.idretur;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.returs = data;

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.idretur)) {
                map.set(item.idretur, true);    // set any value to Map
                result.push({
                    // this.tempTT = result[0].nott;
                    nott: item.idretur,
                });
            }
        }
        // if (result.length === 1) {
            this.returService.pushItems(this.selected);
            this.eventManager.broadcast({ name: 'ReturModified', content: 'OK' });
            this.activeModal.dismiss('close');
            this.selected = new Array<Retur>();
        // }
        // if (result.length > 1) {
        //     this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki PIC dan Tipe PP yang sama');
        // }
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    onRowSelect(event) {
        console.log('leng selected data == ', this.selected);
        if (this.selected.length === 0) {
            this.tempTT = ''
        }
        if (this.selected.length === 1) {
            this.tempTT = event.data.receiptno
            this.isDisable = false;
        }
        if (this.selected.length > 1) {
            if (this.tempTT !== event.data.receiptno) {
                this.toasterService.showToaster('Info', 'Informasi', 'INVOICE yang dipilih harus memiliki TANDA TERIMA sama');
                this.isDisable = true;
            }
        }
    }
    onRowUnselect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.idretur)) {
                map.set(item.idretur, true);    // set any value to Map
                result.push({
                    nott: item.idretur,
                });
            }
        }
        if (result.length === 1) {
            this.tempTT = result[0].nott;
            this.isDisable = false;
        }
        if (result.length > 1) {
            this.isDisable = true;
        }
        console.log('cek result ==== ', result);
    }

}

@Component({
    selector: 'jhi-retur-lov-popup',
    template: ''
})
export class ReturLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        protected returPopupService: ReturPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.returPopupService.load(ReturAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
