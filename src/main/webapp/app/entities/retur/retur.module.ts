import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
    AccordionModule,
    AutoCompleteModule,
    ButtonModule,
    CalendarModule,
    CheckboxModule,
    ConfirmDialogModule,
    DataTableModule,
    DialogModule,
    DropdownModule,
    FieldsetModule,
    InputMaskModule,
    InputTextareaModule,
    RadioButtonModule,
    TabViewModule,
    TooltipModule,
    FileUploadModule
} from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import {
    ReturService,
    ReturPopupService,
    ReturComponent,
    ReturDetailComponent,
    ReturDialogComponent,
    ReturPopupComponent,
    ReturDeletePopupComponent,
    ReturDeleteDialogComponent,
    returRoute,
    returPopupRoute,
    ReturResolvePagingParams,
    ReturNewComponent,
    LhpComponent,
    TabLhpComponent,
    TukarGulingComponent,
    NotaPembatalanComponent,
    NotaPembatalanNewComponent,
    TabApprovalNotaPembatalanComponent,
    ApprovalNotaPembatalanComponent,
    ApprovalReturComponent,
    LhpSRComponent,
    LhpNPComponent,
    LhpNRComponent,
    LhpTGComponent,
    LhpL2Component,
    TabApprovalPaymentReturComponent,
    ApprovalPaymentReturComponent,
    DetailApprovalPaymentReturComponent
} from './';
import { ReturAsLovComponent, ReturLovPopupComponent } from './retur-as-lov.component';
import { TukarGulingNewComponent } from './tukar-guling/tukar-guling-new.component';
import { ApprovalTukarGulingComponent } from './tukar-guling/approval-tukar-guling.component';
import { SuratTukarGulingComponent } from './tukar-guling/surat-tukar-guling.component';
import { TabApprovalTukarGulingComponent } from './tukar-guling/tab-approval-tukar-guling.component';
import { TabApprovalReturComponent } from './tab-approval-retur.component';
import { ApprovalSuratReturComponent } from './surat_retur/approval-surat-retur.component';
import { SuratReturNewComponent } from './surat_retur/surat-retur-new.component';
import { SuratReturComponent } from './surat_retur/surat-retur.component';
import { SuratSuratReturComponent } from './surat_retur/surat-surat-retur.component';
import { TabApprovalSuratReturComponent } from './surat_retur/tab-approval-surat-retur.component';
import { MonitoringBBKComponent } from './monitoring-bbk/monitoring-bbk-component';

// import { MpmReturDocumentModule } from '../retur-document/retur-document.module';

const ENTITY_STATES = [
    ...returRoute,
    ...returPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        FileUploadModule,
        // MpmReturDocumentModule
    ],
    declarations: [
        ReturComponent,
        ReturDetailComponent,
        ReturDialogComponent,
        ReturDeleteDialogComponent,
        ReturPopupComponent,
        ReturDeletePopupComponent,
        ReturNewComponent,
        ReturAsLovComponent,
        ReturLovPopupComponent,
        LhpComponent,
        TabLhpComponent,
        TukarGulingComponent,
        TukarGulingNewComponent,
        ApprovalTukarGulingComponent,
        SuratTukarGulingComponent,
        TabApprovalTukarGulingComponent,
        TabApprovalNotaPembatalanComponent,
        NotaPembatalanComponent,
        NotaPembatalanNewComponent,
        ApprovalNotaPembatalanComponent,
        TabApprovalReturComponent,
        ApprovalReturComponent,
        SuratReturComponent,
        ApprovalSuratReturComponent,
        SuratReturNewComponent,
        SuratSuratReturComponent,
        TabApprovalSuratReturComponent,
        MonitoringBBKComponent,
        LhpSRComponent,
        LhpNPComponent,
        LhpNRComponent,
        LhpTGComponent,
        LhpL2Component,
        TabApprovalPaymentReturComponent,
        ApprovalPaymentReturComponent,
        DetailApprovalPaymentReturComponent
    ],
    entryComponents: [
        ReturComponent,
        ReturDialogComponent,
        ReturPopupComponent,
        ReturDeleteDialogComponent,
        ReturDeletePopupComponent,
        ReturNewComponent,
        ReturAsLovComponent,
        ReturLovPopupComponent,
        LhpComponent,
        TabLhpComponent,
        TukarGulingComponent,
        TukarGulingNewComponent,
        ApprovalTukarGulingComponent,
        SuratTukarGulingComponent,
        TabApprovalTukarGulingComponent,
        TabApprovalNotaPembatalanComponent,
        NotaPembatalanComponent,
        NotaPembatalanNewComponent,
        ApprovalNotaPembatalanComponent,
        TabApprovalReturComponent,
        ApprovalReturComponent,
        SuratReturComponent,
        ApprovalSuratReturComponent,
        SuratReturNewComponent,
        SuratSuratReturComponent,
        TabApprovalSuratReturComponent,
        MonitoringBBKComponent,
        LhpSRComponent,
        LhpNPComponent,
        LhpNRComponent,
        LhpTGComponent,
        LhpL2Component,
        TabApprovalPaymentReturComponent,
        ApprovalPaymentReturComponent,
        DetailApprovalPaymentReturComponent
    ],
    providers: [
        ReturService,
        ReturPopupService,
        ReturResolvePagingParams,

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmReturModule { }
