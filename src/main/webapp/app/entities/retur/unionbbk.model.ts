import { BaseEntity } from '../../shared';

export class UnionBBK {
    constructor(
       public no_bbk?: string,
       public tgl_bbk?: Date,
       public iduser?: string,
       public ket?: string,
       public div?: string,
    ) {
    }
}
