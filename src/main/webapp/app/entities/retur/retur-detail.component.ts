import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Retur } from './retur.model';
import { ReturService } from './retur.service';

@Component({
    selector: 'jhi-retur-detail',
    templateUrl: './retur-detail.component.html'
})
export class ReturDetailComponent implements OnInit, OnDestroy {

    retur: Retur;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private returService: ReturService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReturs();
    }

    load(id) {
        this.returService.find(id).subscribe((retur) => {
            this.retur = retur;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'returListModification',
            (response) => this.load(this.retur.id)
        );
    }
}
