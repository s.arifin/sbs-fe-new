import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { DetailLHP, Retur, ReturService, UnionLHP } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplierService } from '../../master-supplier';

@Component({
    selector: 'jhi-lhp-nr',
    templateUrl: './list-lhp-nr.component.html'
})
export class LhpNRComponent implements OnInit, OnDestroy {

    @Input() jenisklaim: string;
    currentAccount: any;
    lhps: UnionLHP[];
    selected: UnionLHP[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtfrom: Date;
    dtthru: Date;
    kdSPL: string;
    newSuplier: any;
    filteredSuppliers: any[];
    lhp: DetailLHP;
    newModalLHP: boolean;
    selectedSTS: string;
    isFilter: boolean;
    ppStatus: Array<object> = [
        { label: 'Pilih', value: '' },
        { label: 'Tukar Guling', value: 'TG' },
        { label: 'Nota Retur', value: 'NR' },
        { label: 'Surat Retur', value: 'SR' },
        { label: 'Nota Pembatalan', value: 'NP' },
        { label: 'Close', value: 'CL' }
    ];
    isDisabled: Boolean = false;
    jenis: string;
    jenisStatus: Array<object> = [
        { label: 'Pilih Status', value: '' },
        { label: 'Pending', value: 'Pending' },
        { label: 'Done', value: 'Done' },
    ];

    constructor(
        private returService: ReturService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selected = new Array<UnionLHP>();
        // this.jenisklaim = '';
        this.lhp = new DetailLHP();
        this.newModalLHP = false;
        this.selectedSTS = '';
        this.isFilter = false;
        this.jenis = '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter) {
            const waktu = '23:59:59.000';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            if (this.currentSearch) {
                this.returService.queryListLhpKlaim({
                    page: this.page,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryListLhpKlaim({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        } else {
            if (this.currentSearch) {
                this.returService.queryListLhpKlaim({
                    page: this.page,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + undefined + '|dtthru:' + undefined + '|kdsup:' + undefined + '|jenis_status:' + undefined,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryListLhpKlaim({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + undefined + '|dtthru:' + undefined + '|kdsup:' + undefined + '|jenis_status:' + undefined,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/list-lhp'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/list-lhp', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/list-lhp', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInReturs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Retur) {
        return item.id;
    }
    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe('returListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.lhps = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    cetakRetur(retur: Retur) {
        const pisah = retur.noretur.split('/');
        const filter_data = 'noretur:' + pisah[0].toString() +
            '|pt:' + pisah[1].toString() +
            '|bulan:' + pisah[2].toString() +
            '|tahun:' + pisah[3].toString();
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }

    downloadRetur(retur: Retur) {
        const pisah = retur.noretur.split('/');
        const filter_data = 'noretur:' + pisah[0].toString() +
            '|pt:' + pisah[1].toString() +
            '|bulan:' + pisah[2].toString() +
            '|tahun:' + pisah[3].toString();
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    filter() {
        if (this.jenis === '') {
            this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
        } else {
            this.isFilter = true;
            this.loadAll();
        }

    }
    reset() {
        this.currentSearch = '';
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.jenis = '';
        this.loadAll();
    }
    updatestatuslhp(type: string) {
        this.selected.map((e) => e.jenisklaim = type);
        console.log('ini data yang di select => ', this.selected);
        this.returService.updateLHP(this.selected)
            .subscribe(
                (res: UnionLHP[]) => {
                    console.log('ini hasil dari backend => ', res);
                    this.loadAll();
                }
            )
    }

    updatestatuslhpDetail(type: string) {
        this.lhp.jenisklaim = type;
        console.log('ini data yang di select => ', this.lhp);
        this.returService.updateLHPDetail(this.lhp)
            .subscribe(
                (res: UnionLHP) => {
                    this.newModalLHP = false;
                    console.log('ini hasil dari backend => ', res);
                    this.loadAll();
                }
            )
    }
    getFindOne(nolhp, kdbahan) {
        this.returService.getDetailLHP({
            query: 'nolhp:' + nolhp + '|kdbahan:' + kdbahan + '|jenisklaim:NR'
        }).subscribe(
            (res: DetailLHP) => {
                this.newModalLHP = true;
                this.lhp = res;
            }
        )
    }
    updatestatus() {
        if (this.selectedSTS === '') {
            this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
        } else {
            this.updatestatuslhpDetail(this.selectedSTS)
        }
    }
    openPdf(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;
        let jenisLHP: number;
        bbms = rowData.nolhp.replace(/\//g, '-');
        if (bbms.includes('LPPM') || bbms.includes('LPRM') || bbms.includes('LPGM')) {
            jenisLHP = 1;
        } else if (bbms.includes('LPMR') || bbms.includes('LPMP')) {
            jenisLHP = 2;
        }

        const filter_data = 'nolhp:' + bbms + '|jenisLHP:' + jenisLHP;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_lhp/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

    cancelLhp(nolhp, kdbahan) {
        this.loadingService.loadingStart();
        this.returService.getCancel({
            query: 'nolhp:' + nolhp + '|kdbahan:' + kdbahan + '|jenisklaim:NR'
        }).subscribe(
            (res: ResponseWrapper) => {
                this.loadingService.loadingStop();
                this.loadAll();
            }
        )
    }
}
