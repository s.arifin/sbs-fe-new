import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'jhi-tab-lhp',
    templateUrl: './tab-lhp.component.html'
})
export class TabLhpComponent implements OnInit, OnDestroy {
    new: string;
    TG: string;
    NR: string;
    NP: String;
    CL: String;
    SR: String;
    L2: String;
    constructor() {
        this.new = 'new';
        this.TG = 'TG';
        this.NR = 'NR';
        this.CL = 'CL';
        this.SR = 'SR';
        this.NP = 'NP';
        this.L2 = 'L2';

    }
    ngOnInit(): void {
        // throw new Error('Method not implemented.');
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
}
