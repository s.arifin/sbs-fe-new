import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { DetailLHP, Retur, ReturService, UnionLHP } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, ToasterService, Account, UserService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplierService } from '../../master-supplier';

@Component({
    selector: 'jhi-lhp',
    templateUrl: './lhp.component.html'
})
export class LhpComponent implements OnInit, OnDestroy {

    @Input() jenisklaim: string;
    currentAccount: any;
    lhps: UnionLHP[];
    selected: UnionLHP[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtfrom: Date;
    dtthru: Date;
    kdSPL: string;
    newSuplier: any;
    filteredSuppliers: any[];
    lhp: DetailLHP;
    newModalLHP: boolean;
    selectedSTS: string;
    isFilter: boolean;
    ppStatus: Array<object> = [
        { label: 'Pilih', value: '' },
        { label: 'Tukar Guling', value: 'TG' },
        { label: 'Nota Retur', value: 'NR' },
        { label: 'Nota Pembatalan', value: 'NP' },
        { label: 'Lain - Lain', value: 'L2' },
    ];
    isDisabled: Boolean = false;
    modalPilihAdm: boolean;
    admPOs: Account[];
    admPO: Account;
    modalPilihCeteakn: boolean;
    constructor(
        private returService: ReturService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        private toasterService: ToasterService,
        private userService: UserService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selected = new Array<UnionLHP>();
        // this.jenisklaim = '';
        this.lhp = new DetailLHP();
        this.newModalLHP = false;
        this.selectedSTS = '';
        this.isFilter = false;
        this.modalPilihAdm = false;
        this.admPOs = new Array<Account>();
        this.admPO = new Account();
        this.modalPilihCeteakn = false;
    }

    getAdminPO() {
        this.userService.getByRole('ROLE_ADM_KLAIM_PBL')
            .subscribe(
                (res: ResponseWrapper) => {
                    console.log('ini data admin PO = ', res.json);
                    this.admPOs = res.json;
                });
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter) {
            const waktu = '23:59:59.000';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            if (this.currentSearch) {
                this.returService.queryListLhpNew({
                    page: this.page,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryListLhpNew({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        } else {
            if (this.currentSearch) {
                this.returService.queryListLhpNew({
                    page: this.page,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + undefined + '|dtthru:' + undefined + '|kdsup:',
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryListLhpNew({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:' + this.jenisklaim + '|nolhp:' + this.currentSearch + '|dtfrom:' + undefined + '|dtthru:' + undefined + '|kdsup:',
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }

    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/list-lhp'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/list-lhp', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/list-lhp', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInReturs();
        this.getAdminPO();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Retur) {
        return item.id;
    }
    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe('returListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.lhps = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    cetakRetur(retur: Retur) {
        const pisah = retur.noretur.split('/');
        const filter_data = 'noretur:' + pisah[0].toString() +
            '|pt:' + pisah[1].toString() +
            '|bulan:' + pisah[2].toString() +
            '|tahun:' + pisah[3].toString();
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }

    downloadRetur(retur: Retur) {
        const pisah = retur.noretur.split('/');
        const filter_data = 'noretur:' + pisah[0].toString() +
            '|pt:' + pisah[1].toString() +
            '|bulan:' + pisah[2].toString() +
            '|tahun:' + pisah[3].toString();
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }

    download() {
        this.modalPilihCeteakn = true;
    }

    printExcel() {
        const waktu = '23:59:59.000';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        let kdspl = '';
        let search = '';
        const date = new Date();
        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();
        if (this.kdSPL === null || this.kdSPL === undefined || this.kdSPL === '') {
            kdspl = 'all'
        } else {
            kdspl = this.kdSPL;
        }

        if (this.currentSearch === null || this.currentSearch === '' || this.currentSearch === undefined) {
            search = 'all';
        } else {
            search = this.currentSearch;
        }
        const filter_data = 'dtFrom:' + dari +
            '|dtThru:' + sampai +
            '|kdsup:' + kdspl +
            '|search:' + search
        this.reportUtilService.downloadFileWithName(`Report List TLU LHP ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/list_tlu/xlsx', { filterData: filter_data });
        this.modalPilihCeteakn = false;
    }

    printPDF() {
        const waktu = '23:59:59.000';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        let kdspl = '';
        let search = '';
        const date = new Date();
        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();
        if (this.kdSPL === null || this.kdSPL === undefined || this.kdSPL === '') {
            kdspl = 'all'
        } else {
            kdspl = this.kdSPL;
        }

        if (this.currentSearch === null || this.currentSearch === '' || this.currentSearch === undefined) {
            search = 'all';
        } else {
            search = this.currentSearch;
        }
        const filter_data = 'dtFrom:' + dari +
            '|dtThru:' + sampai +
            '|kdsup:' + kdspl +
            '|search:' + search
        this.reportUtilService.downloadFileWithName(`Report List TLU LHP ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/list_tlu/pdf', { filterData: filter_data });
        this.modalPilihCeteakn = false;
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    filter() {
        this.isFilter = true;
        this.loadAll();
    }
    reset() {
        this.currentSearch = '';
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.newSuplier.kd_suppnew = '';
        this.loadAll();
    }
    updatestatuslhp(type: string) {
        this.selected.map((e) => e.jenisklaim = type);
        console.log('ini data yang di select => ', this.selected);
        this.returService.updateLHP(this.selected)
            .subscribe(
                (res: UnionLHP[]) => {
                    console.log('ini hasil dari backend => ', res);
                    this.loadAll();
                }
            )
    }

    updatestatuslhpDetail(type: string, adm: string) {
        this.lhp.jenisklaim = type;
        this.lhp.adm_pbl = adm;
        this.returService.updateLHPDetail(this.lhp)
            .subscribe(
                (res: UnionLHP) => {
                    this.newModalLHP = false;
                    this.modalPilihAdm = false;
                    this.selectedSTS = '';
                    this.toasterService.showToaster('Informasi', 'Berhasil Update', 'KLaim Berhasil Dibuat');
                    this.loadAll();
                }
            )
    }
    getFindOne(nolhp, kdbahan) {
        this.returService.getDetailLHP({
            query: 'nolhp:' + nolhp + '|kdbahan:' + kdbahan + '|jenisklaim:' + undefined
        }).subscribe(
            (res: DetailLHP) => {
                this.newModalLHP = true;
                this.lhp = res;
            }
        )
    }
    updatestatus() {
        if (this.selectedSTS === '') {
            this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
        } else {
            this.modalPilihAdm = true;
        }
    }

    setAdminPP(data: string) {
        this.updatestatuslhpDetail(this.selectedSTS, data)
    }

    openPdf(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;
        let jenisLHP: number;
        bbms = rowData.nolhp.replace(/\//g, '-');
        if (bbms.includes('LPPM') || bbms.includes('LPRM') || bbms.includes('LPGM')) {
            jenisLHP = 1;
        } else if (bbms.includes('LPMR') || bbms.includes('LPMP')) {
            jenisLHP = 2;
        }

        const filter_data = 'nolhp:' + bbms + '|jenisLHP:' + jenisLHP;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_lhp/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }
}
