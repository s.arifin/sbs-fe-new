import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReturComponent } from './retur.component';
import { ReturDetailComponent } from './retur-detail.component';
import { ReturPopupComponent } from './retur-dialog.component';
import { ReturDeletePopupComponent } from './retur-delete-dialog.component';
import { ReturNewComponent } from './retur-new.component';
import { ReturAsLovComponent, ReturLovPopupComponent } from './retur-as-lov.component';
import { LhpComponent } from './list-lhp/lhp.component';
import { TabLhpComponent } from './list-lhp/tab-lhp.component';
import { TukarGulingComponent } from './tukar-guling/tukar-guling.component';
import { TukarGulingNewComponent } from './tukar-guling/tukar-guling-new.component';
import { ApprovalTukarGulingComponent } from './tukar-guling/approval-tukar-guling.component';
import { SuratTukarGulingComponent } from './tukar-guling/surat-tukar-guling.component';
import { TabApprovalTukarGulingComponent } from './tukar-guling/tab-approval-tukar-guling.component';
import { NotaPembatalanComponent } from './nota-pembatalan/nota-pembatalan.component';
import { NotaPembatalanNewComponent } from './nota-pembatalan/nota-pembatalan-new.component';
import { TabApprovalNotaPembatalanComponent } from './nota-pembatalan/tab-approval-nota-pembatalan.component';
import { TabApprovalReturComponent } from './tab-approval-retur.component';
import { SuratReturComponent } from './surat_retur/surat-retur.component';
import { SuratReturNewComponent } from './surat_retur/surat-retur-new.component';
import { MonitoringBBKComponent } from './monitoring-bbk/monitoring-bbk-component';
import { TabApprovalPaymentReturComponent } from './payment-retur/tab-approval-payment-retur.component';
import { DetailApprovalPaymentReturComponent } from './payment-retur/detail-approval-payment-retur.component';
@Injectable()
export class ReturResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const returRoute: Routes = [
    {
        path: 'retur',
        component: ReturComponent,
        resolve: {
            'pagingParams': ReturResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tukar-guling',
        component: TukarGulingComponent,
        resolve: {
            'pagingParams': ReturResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur/:id',
        component: ReturDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur-new',
        component: ReturNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tukar-guling-new',
        component: TukarGulingNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur/:id/:edit',
        component: ReturNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur/payment-retur/:id/:edit',
        component: DetailApprovalPaymentReturComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur-as-lov',
        component: ReturLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'approval-list-lhp',
        component: TabLhpComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService],
    }, {
        path: 'list-lhp',
        component: TabLhpComponent,
        // resolve: {
        //     'pagingParams': ReturResolvePagingParams
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService],
    }, {
        path: 'tukar-guling/:id/:edit',
        component: TukarGulingNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-tukar-guling',
        // component: ApprovalTukarGulingComponent,
        component: TabApprovalTukarGulingComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'surat-tukar-guling',
        component: SuratTukarGulingComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'nota-pembatalan',
        component: NotaPembatalanComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'nota-pembatalan/:id/:edit',
        component: NotaPembatalanNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'nota-pembatalan-new',
        component: NotaPembatalanNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-nota-pembatalan',
        component: TabApprovalNotaPembatalanComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-payment-retur',
        component: TabApprovalPaymentReturComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-retur',
        component: TabApprovalReturComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'new-retur-as-lov/:jenis',
        component: ReturLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'surat-retur',
        component: SuratReturComponent,
        resolve: {
            'pagingParams': ReturResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'surat-retur-new',
        component: SuratReturNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'monitoring-bbk',
        component: MonitoringBBKComponent,
        resolve: {
            'pagingParams': ReturResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title1'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'surat-retur/:id/:edit',
        component: SuratReturNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const returPopupRoute: Routes = [
    // {
    //     path: 'retur-new',
    //     component: ReturPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.retur.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    {
        path: 'retur/:id/edit',
        component: ReturPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'retur/:id/delete',
        component: ReturDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.retur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
