import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Retur, ReturService, ReturVM } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper, ToasterService } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplierService } from '../../master-supplier';

@Component({
    selector: 'jhi-nota-pembatalan',
    templateUrl: './nota-pembatalan.component.html'
})
export class NotaPembatalanComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: ReturVM[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtfrom: Date;
    dtthru: Date;
    kdSPL: string;
    newSuplier: any;
    filteredSuppliers: any[];
    isFilter: boolean;
    jenis: string;
    pic: string;
    selected: ReturVM[];
    jenisStatus: Array<object> = [
        { label: 'Pilih Status', value: '' },
        { label: 'New', value: 'New' },
        { label: 'Request Approve', value: 'Request Approve' },
        { label: 'Approved', value: 'Approved' },
        { label: 'Sudah BBK', value: 'Sudah BBK' },
    ];
    picJenis: Array<object> = [
        { label: 'Pilih PIC', value: '' },
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' },
        { label: 'EVN', value: 'EVN' },
    ];
    isEmail: Array<object> = [
        { label: 'all', value: '' },
        { label: 'Sudah', value: 'Sudah' },
        { label: 'Belum', value: 'Belum' }
    ];
    selectedMail: string;
    constructor(
        private returService: ReturService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        protected confirmationService: ConfirmationService,
        private toasterService: ToasterService,

    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.isFilter = false;
        this.pic = '';
        this.jenis = '';
        this.selectedMail = ''
        this.selected = new Array<ReturVM>();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter) {
            const waktu = '23:59:59.000';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            if (this.currentSearch) {
                this.returService.queryKlaim({
                    page: this.page - 1,
                    query: 'jenisklaim:NP' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic + '|email:' + this.selectedMail,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryKlaim({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:NP' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic + '|email:' + this.selectedMail,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        } else {
            if (this.currentSearch) {
                this.returService.queryKlaim({
                    page: this.page - 1,
                    query: 'jenisklaim:NP' + '|nolhp:' + this.currentSearch + '|dtfrom:' + undefined + '|dtthru:' + undefined + '|kdsup:' + undefined + '|jenis_status:' + undefined + '|pic:' + undefined + '|email:' + undefined,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryKlaim({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:NP' + '|nolhp:' + this.currentSearch + '|dtfrom:' + undefined + '|dtthru:' + undefined + '|kdsup:' + undefined + '|jenis_status:' + undefined + '|pic:' + undefined + '|email:' + undefined,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/retur'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/retur', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/retur', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInReturs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Retur) {
        return item.id;
    }
    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe('returListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.returs = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    cetakRetur(retur: Retur) {
        const filter_data = 'idretur:' + retur.idretur;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/ReturNP/pdf', { filterData: filter_data });
    }

    downloadRetur(retur: Retur) {
        const noretur = retur.noretur;
        const filter_data = 'idretur:' + retur.idretur;
        this.reportUtilService.downloadFileWithName(`Nota Pembatalan ${noretur}`, process.env.API_C_URL + '/api/report/ReturNP/pdf', { filterData: filter_data });
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    filter() {
        if (this.jenis === '') {
            this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');
        } else if (this.pic === '') {
            this.toasterService.showToaster('Informasi', 'Pilih Status', 'Silahkan Pilih status terlebih dahulu');

        } else {
            this.isFilter = true;
            this.loadAll();
        }

    }
    reset() {
        this.currentSearch = '';
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.jenis = '';
        this.pic = '';
        this.loadAll();
    }
    setstatus(idstatus: number, rowData: Retur) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Request Approve ??',
            accept: () => {
                this.returService.changeStatus(rowData, idstatus)
                    .subscribe(
                        (res) => this.loadAll()
                    )
            }
        });
    }

    setmail() {
        this.returService.setMailPO(this.selected).subscribe(
            (res) => {
                this.loadAll();
                this.selected = new Array<ReturVM>();
                alert('Sukses Update Sudah Email');
            }
        )
    }
}
