import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiAlertService, JhiDateUtils, JhiEventManager } from 'ng-jhipster';
import { LazyLoadEvent, TRISTATECHECKBOX_VALUE_ACCESSOR } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Retur, UnionBBK, ReturService, UnionLHP } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ReturItem } from '../../retur-item';
import { DetailBBK, DetailBBKTeknik, UnionBBKService } from '../../union-bbk';
import { UnionLHPService } from '../../union-bbk/union-lhp.service';

@Component({
    selector: 'jhi-nota-pembatalan-new',
    templateUrl: './nota-pembatalan-new.component.html'
})
export class NotaPembatalanNewComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: Retur[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selectedSuppCode: string;
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    filteredSuppliers: any[];
    tgl: Date;
    modalBBK: boolean;
    BBKs: UnionBBK[];
    itemsDataPerPage: any;
    selectedItems: UnionBBK;
    eventSubscriberBBKLOV: Subscription;
    returItems: ReturItem[];
    detailBBK: DetailBBK[];
    detailBBKTeknik: DetailBBKTeknik[];
    returItem: ReturItem;
    jumlah: number;
    isDiscPersen: boolean;
    retur: Retur;
    checkPPN: boolean;
    private subscription: Subscription;
    isEdit: boolean;
    isUpdate: boolean;
    spl: MasterSupplier;
    dttax: Date;
    pembulatan: boolean;
    jnspembulatan: boolean;
    lblpembualatan: string;
    lbljnspembulatan: string;
    noretur: string;
    jenis_retur: string;
    constructor(
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private masterSupplierService: MasterSupplierService,
        private returService: ReturService,
        private uionLHPService: UnionLHPService,
        private toasterService: ToasterService,
        private route: ActivatedRoute,
        private loadingService: LoadingService,
        private dateUtils: JhiDateUtils,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.tgl = new Date();
        this.modalBBK = false;
        this.itemsDataPerPage = 0;
        this.selectedItems = new UnionBBK();
        this.returItems = new Array<ReturItem>();
        this.detailBBK = new Array<DetailBBK>();
        this.detailBBKTeknik = new Array<DetailBBKTeknik>();
        this.returItem = new ReturItem();
        this.jumlah = 0;
        this.isDiscPersen = false;
        this.retur = new Retur();
        this.retur.disc = 0;
        this.retur.total = 0;
        this.retur.grandtot = 0;
        this.checkPPN = false;
        if (this.retur.disc === undefined) {
            this.retur.disc = 0;

        }
        if (this.retur.ppn === undefined) {
            this.retur.ppn = 0;
        }
        if (this.retur.grandtot === undefined) {
            this.retur.grandtot = 0;
        }
        if (this.retur.ppnbm === undefined) {
            this.retur.ppnbm = 0;
        }
        this.isEdit = true;
        this.isUpdate = false;
        this.spl = new MasterSupplier();
        this.dttax = new Date();
        this.pembulatan = false;
        this.lblpembualatan = 'Tidak';
        this.jnspembulatan = false;
        this.lbljnspembulatan = 'Bawah';
        this.noretur = '';
        this.jenis_retur = '';
    }

    loadEdit(id) {
        this.loadingService.loadingStart();
        this.returService.find(id)
            .subscribe(
                (res: Retur) => {
                    this.retur = res;
                    this.noretur = res.noretur;
                    this.returItems = res.retur_item;
                    this.jumlah = res.total;
                    this.spl = res.master_supplier;
                    this.selectedSuppCode = res.master_supplier.kd_suppnew;
                    if (this.retur.ppn > 0) {
                        this.checkPPN = true;
                    };
                    if (res.dttax != null) {
                        this.dttax = this.dateUtils
                            .convertDateTimeFromServer(res.dttax);
                    }
                    if (res.dtdoc != null) {
                        this.tgl = this.dateUtils
                            .convertDateTimeFromServer(res.dtdoc);
                    }
                    this.loadingService.loadingStop();

                },
                (err) => {
                    this.loadingService.loadingStop();
                }
            );
    }
    ngOnInit() {
        this.retur.disc = 0;
        this.retur.total = 0;
        this.retur.grandtot = 0;
        this.retur.noretur = 'XXXX/XXX-XXX-XX/XXX/XXXX';
        this.noretur = 'XXXX/XXX-XXX-XX/XXX/XXXX';
        this.registerFromBBKLOV();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['edit'] === 'true') {
                    this.isEdit = true;
                    this.loadEdit(params['id']);
                    this.isUpdate = true;
                }
                if (params['edit'] === 'false') {
                    this.loadEdit(params['id']);
                    this.isEdit = false;
                }
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.selectedSuppCode = this.newSuplier.kd_suppnew;
    }
    getBBKFromLOV() {
        this.uionLHPService.values.subscribe(
            (resarr: UnionLHP[]) => {
                resarr.forEach((res) => {
                    console.log(res);
                    this.jenis_retur = res.jenis_retur;
                    this.loadingService.loadingStart();
                    this.returService.queryLHP({
                        query: 'nolhp:' + res.nolhp + '|kdbahan:' + res.kdbahan + '|jenisklaim:' + res.jenisklaim
                    })
                        .subscribe(
                            (resTek: ResponseWrapper) => {
                                this.returItem = new ReturItem();
                                this.returItem = resTek.json;
                                this.returItems = [...this.returItems, this.returItem];
                                this.masterSupplierService.findByCode(this.returItem.suppcode)
                                    .subscribe(
                                        (resSPL: MasterSupplier) => {
                                            this.spl = resSPL;
                                            this.retur.suppcode = resSPL.kd_suppnew;
                                            this.retur.suppname = resSPL.nmSupplier;
                                        })
                                this.loadingService.loadingStop();
                            },
                            (err) => {
                                this.toasterService.showToaster('Info', 'Error', 'Terjadi Error Silahkan Refresh Halaman');
                                this.loadingService.loadingStop();
                            }
                        )
                    this.uionLHPService.values.observers = [];
                });
            }
        )
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/header-bbks/lov'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
            }
        });
        // this.openLOV();
    }
    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.BBKs = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    batal() {
        this.modalBBK = false;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadLOV();
    }
    registerFromBBKLOV() {
        this.eventSubscriberBBKLOV = this.eventManager.subscribe('unionLHPLovModification', () => this.getBBKFromLOV());
    }

    calculateTotalPrice(row: ReturItem) {
        this.jumlah = 0;
        this.retur.grandtot = 0;
        row.totalprice = row.qty * row.price;
        console.log('INI QTY = ', row.qty);
        console.log('INI PRICE = ', row.price);
        console.log('INI TOTAL PRICE = ', row.totalprice);
        this.returItems.forEach(
            (e) => {
                this.jumlah += e.totalprice;
            }
        );
        if (this.checkPPN === true) {
            this.retur.ppn = (this.jumlah * 10 / 100);
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
        }
        this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;

    }

    countGrandTotal() {
        this.retur.grandtot = this.jumlah + this.retur.ppnbm + this.retur.ppn;
    }

    checkIsPPn() {
        if (this.pembulatan === false) {
            this.jnspembulatan = false
            this.lblpembualatan = 'Tidak';
        }
        if (this.pembulatan === true) {
            this.lblpembualatan = 'Ya';
        }
        if (this.jnspembulatan === false) {
            this.lbljnspembulatan = 'Bawah';
        }
        if (this.jnspembulatan === true) {
            this.lbljnspembulatan = 'Atas';
        }

        if (this.checkPPN === true) {
            if (this.pembulatan === false) {
                this.retur.ppn = (this.jumlah * 11 / 100);
            }
            if (this.pembulatan === true) {
                if (this.jnspembulatan === false) {
                    this.retur.ppn = Math.floor((this.jumlah * 11 / 100));
                }
                if (this.jnspembulatan === true) {
                    this.retur.ppn = Math.ceil((this.jumlah * 11 / 100));
                }
            }
            this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
        }
        if (this.checkPPN === false) {
            this.retur.ppn = 0;
            this.retur.grandtot = this.jumlah - this.retur.disc + this.retur.ppn;
        }
    }

    createRetur() {
        this.retur.dttax = this.dttax;
        this.retur.dtdoc = this.tgl;
        this.retur.retur_item = new Array<ReturItem>();
        this.retur.jenisklaim = 'NP';
        if (this.retur.ppnbm == null || this.retur.ppnbm === undefined) {
            this.retur.ppnbm = 0;
        }
        if (this.retur.ppn == null || this.retur.ppn === undefined) {
            this.retur.ppn = 0;
        } else {
            this.retur.suppname = this.spl.nmSupplier;
            this.retur.suppcode = this.spl.kd_suppnew;
            this.retur.total = this.jumlah;
            // this.retur.retur_item = this.returItems;
            this.retur.retur_item = [...this.returItems, this.returItem]
            this.returService.create(this.retur).subscribe((res) => {
                this.backMainPage();
            });
        }
    }

    updateRetur() {
        this.retur.dttax = this.dttax;
        this.retur.dtdoc = this.tgl;
        this.retur.retur_item = new Array<ReturItem>();
        if (this.retur.ppnbm == null || this.retur.ppnbm === undefined) {
            this.retur.ppnbm = 0;
        }
        if (this.retur.ppn == null || this.retur.ppn === undefined) {
            this.retur.ppn = 0;
        } else {
            this.retur.suppname = this.spl.nmSupplier;
            this.retur.suppcode = this.spl.kd_suppnew;
            this.retur.total = this.jumlah;
            this.retur.retur_item = [...this.returItems, this.returItem]
            this.returService.update(this.retur).subscribe((res) => {
                this.backMainPage();
            });
        }
    }

    backMainPage() {
        this.router.navigate(['/nota-pembatalan']);
    }
}
