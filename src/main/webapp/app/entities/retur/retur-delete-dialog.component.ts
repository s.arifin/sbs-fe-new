import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Retur } from './retur.model';
import { ReturPopupService } from './retur-popup.service';
import { ReturService } from './retur.service';

@Component({
    selector: 'jhi-retur-delete-dialog',
    templateUrl: './retur-delete-dialog.component.html'
})
export class ReturDeleteDialogComponent {

    retur: Retur;

    constructor(
        private returService: ReturService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.returService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'returListModification',
                content: 'Deleted an retur'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-retur-delete-popup',
    template: ''
})
export class ReturDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returPopupService: ReturPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.returPopupService
                .open(ReturDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
