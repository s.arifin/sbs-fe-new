import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DetailLHP, Retur, ReturVM, UnionLHP } from './retur.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { ReturItem } from '../retur-item';

@Injectable()
export class ReturService {
    protected itemValues: Retur[] = new Array<Retur>();
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/returs';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/returs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(retur: Retur): Observable<Retur> {
        const copy = this.convert(retur);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(retur: Retur): Observable<Retur> {
        const copy = this.convert(retur);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Retur> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryMonitoringBBK(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/listBBK`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    deleteRow(retur: ReturItem): Observable<ReturItem> {
        const copy = this.convert(retur);
        return this.http.post(`${this.resourceUrl}/deleteItem`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtcreated = this.dateUtils
            .convertDateTimeFromServer(entity.dtcreated);
        entity.dtmodified = this.dateUtils
            .convertDateTimeFromServer(entity.dtmodified);
        entity.tglbbk = this.dateUtils
            .convertDateTimeFromServer(entity.tglbbk);
    }

    private convert(retur: Retur): Retur {
        const copy: Retur = Object.assign({}, retur);

        // copy.dtcreated = this.dateUtils.toDate(retur.dtcreated);

        // copy.dtmodified = this.dateUtils.toDate(retur.dtmodified);

        // copy.tglbbk = this.dateUtils.toDate(retur.tglbbk);
        return copy;
    }
    pushItems(data: Retur[]) {
        this.itemValues = new Array<Retur>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    getReturByIdPay(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-idpay', options)
            .map((res: Response) => this.convertResponse(res));
    }
    deleteMapReturByID(retur: Retur): Observable<Retur> {
        const copy = this.convert(retur);
        return this.http.post(this.resourceUrl + '/mapping-delete', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryListLhp(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/listlhp', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryListLhpNew(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/newLHP', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryListLhpKlaim(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/LhpKlaim', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryKlaim(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/JenisKlaim', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querygetNR(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getKlaimNR', options)
            .map((res: Response) => this.convertResponse(res));
    }

    updateLHP(unionlhps: UnionLHP[]): Observable<UnionLHP[]> {
        const copy = this.convertUnionLHP(unionlhps);
        return this.http.post(this.resourceUrl + '/update-lhp', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    updateLHPDetail(unionlhp: DetailLHP): Observable<DetailLHP> {
        const copy = this.convertUnionLHPDETAIL(unionlhp);
        return this.http.post(this.resourceUrl + '/update-lhp-detail', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    cekFP(kdprogramX: string): Observable<Retur> {
        return this.http.get(`${this.resourceUrl}/cekFP/${kdprogramX}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convertUnionLHP(unionlhps: UnionLHP[]): UnionLHP[] {
        const copy: UnionLHP[] = Object.assign([], unionlhps);
        return copy;
    }
    private convertUnionLHPDETAIL(detailLHP: DetailLHP): DetailLHP {
        const copy: DetailLHP = Object.assign({}, detailLHP);
        return copy;
    }
    queryLHP(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/detail-lhp', options)
            .map((res: Response) => this.convertResponse(res));
    }
    changeStatus(retur: Retur, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(retur);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }

    setMailPO(purchasing: ReturVM[]): Observable<ReturVM[]> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/set-mail-po', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    approveEditRetur(purchasing: ReturVM[]): Observable<ResponseWrapper> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/approve-edit', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getCancelPP(pay: Retur, id: number): Observable<Retur> {
            const copy = this.convert(pay);
            return this.http.post(this.resourceUrl + '/set-status-cancel/' + id, copy).map((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
        }

    getPObyIdPPItem(id: any): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/listBBK/${id}`).map((res: Response) => this.convertResponse(res));
    }
    queryApproval(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/approval', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getDetailLHP(req?: any): Observable<DetailLHP> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/find-one-lhp', options).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getCancel(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/cancel-one-lhp', options).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    query_test(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(process.env.API_C_URL + '/api/test-object', options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertPurchasings(purchasing: ReturVM[]): ReturVM[] {
        const copy: ReturVM[] = Object.assign([], purchasing);
        return copy;
    }
}
