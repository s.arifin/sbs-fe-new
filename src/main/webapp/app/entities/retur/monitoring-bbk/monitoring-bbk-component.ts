import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { MonitoringBBK, Retur, ReturService, ItemBBK } from '..';
import { LoadingService } from '../../../layouts';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';

@Component({
    selector: 'jhi-monitoring-bbk',
    templateUrl: './monitoring-bbk.component.html'
})
export class MonitoringBBKComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: MonitoringBBK[];
    purchaseOrders1: ItemBBK[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtfrom: Date;
    dtthru: Date;
    kdSPL: string;
    newSuplier: any;
    filteredSuppliers: any[];
    modalLihatPO: boolean;
    isUpdate: string;
    isFilter: boolean;
    jenis: string;
    pic: string;
    selected: MonitoringBBK[];
    jenisStatus: Array<object> = [
        { label: 'All', value: '' },
        { label: 'New', value: 'New' },
        { label: 'Request Approve', value: 'Request Approve' },
        { label: 'Approved', value: 'Approve' },
        { label: 'Sudah BBK', value: 'Sudah BBK' },
        { label: 'BBM Sebagian', value: 'BBM Sebagian' },
        { label: 'Close', value: 'Close' },
        { label: 'Not Approved', value: 'Not Approve' },
    ];
    picJenis: Array<object> = [
        { label: 'All', value: '' },
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' },
        { label: 'EVN', value: 'EVN' },
    ];
    constructor(
        private returService: ReturService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        protected confirmationService: ConfirmationService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.modalLihatPO = false;
        this.isUpdate = '';
        this.isFilter = false;
        this.pic = '';
        this.jenis = '';
        this.page = 0;
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter) {
                const waktu = '23:59:59.000';
                const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
                const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
                if (this.currentSearch) {
                    this.returService.queryMonitoringBBK({
                        page: this.page,
                        query: 'noretur:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic,
                        size: this.itemsPerPage
                    }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );
                    return;
                } else {
                    this.returService.queryMonitoringBBK({
                        page: this.page,
                        size: this.itemsPerPage,
                        query: 'noretur:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic,
                    }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );
                }
        } else {
            if (this.currentSearch) {
                this.returService.queryMonitoringBBK({
                    page: this.page,
                    query: 'noretur:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:|dtthru:|kdsup:|jenis_status:|pic:',
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryMonitoringBBK({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'noretur:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:|dtthru:|kdsup:|jenis_status:|pic:',
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/monitoring-bbk'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.isFilter = false;
        this.page = 1;
        this.currentSearch = '';
        this.router.navigate(['/monitoring-bbk', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.router.navigate(['/monitoring-bbk', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInReturs();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Retur) {
        return item.id;
    }
    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe('returListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.returs = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    cetakRetur(retur: Retur) {
        // if (retur.suppcode === 'PUNITE') {
        //     const filter_data = 'idretur:' + retur.idretur;
        //     this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tukar_guling_ucc/pdf', { filterData: filter_data });
        // }
        // if (retur.suppcode !== 'PUNITE') {
        //     const filter_data = 'idretur:' + retur.idretur;
        //     this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tukar_guling_general/pdf', { filterData: filter_data });
        // }
        const filter_data = 'idretur:' + retur.idretur;
        const noretur = retur.noretur;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/report_sr/pdf', { filterData: filter_data });
    }

    downloadRetur(retur: Retur) {
        // if (retur.suppcode === 'PUNITE') {
        //     const filter_data = 'idretur:' + retur.idretur;
        //     this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/tukar_guling_ucc/pdf', { filterData: filter_data });
        // }
        // if (retur.suppcode !== 'PUNITE') {
        //     const filter_data = 'idretur:' + retur.idretur;
        //     this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/tukar_guling_general/pdf', { filterData: filter_data });
        // }
        const filter_data = 'idretur:' + retur.idretur;
        const noretur = retur.noretur;
        this.reportUtilService.downloadFileWithName(`Surat Tukar Guling ${noretur}`, process.env.API_C_URL + '/api/report/report_sr/pdf', { filterData: filter_data });
    }

    download() {
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();

        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;

        this.loadingService.loadingStart();
        const filter_data = 'dtFrom:' + dari + '|dtThru:' + sampai + '|pic:' + this.pic + '|status1:' + this.jenis + '|kdsup:' + this.kdSPL;
        this.reportUtilService.downloadFileWithName(`Report Monitoring BBK ${currentDay}/${currentMonth}/${currentYear}`, process.env.API_C_URL + '/api/report/report_monitoring_bbk/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    setstatus(idstatus: number, rowData: Retur) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Request Approve ??',
            accept: () => {
                this.returService.changeStatus(rowData, idstatus)
                    .subscribe(
                        (res) => this.loadAll()
                    )
            }
        });
    }

    filter() {
        this.isFilter = true;
        this.loadAll();
    }
    reset() {
        this.currentSearch = '';
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.jenis = '';
        this.pic = '';
        this.newSuplier = new MasterSupplier();
        this.loadAll();
    }

    lihatPO(rowData: MonitoringBBK) {
        this.returService.getPObyIdPPItem(rowData.idretur).subscribe(
            (res: ResponseWrapper) => {
                this.purchaseOrders1 = res.json;
                console.log(res.json)
                // console.log('ini data PP to PO = ', this.purchaseOrders);
                this.modalLihatPO = true;
            })
    }
}
