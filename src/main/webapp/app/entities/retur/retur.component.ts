import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Retur, ReturVM } from './retur.model';
import { ReturService } from './retur.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { PurchaseOrderService } from '../purchase-order';

@Component({
    selector: 'jhi-retur',
    templateUrl: './retur.component.html'
})
export class ReturComponent implements OnInit, OnDestroy {

    currentAccount: any;
    returs: ReturVM[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtfrom: Date;
    dtthru: Date;
    kdSPL: string;
    newSuplier: any;
    filteredSuppliers: any[];
    isFilter: boolean;
    jenis: string;
    pic: string;
    selected: ReturVM[];
    jenisStatus: Array<object> = [
        { label: 'All', value: '' },
        { label: 'New', value: 'New' },
        { label: 'Request Approve', value: 'Request Approve' },
        { label: 'Not Approved', value: 'Not Approve' },
        { label: 'Cancel', value: 'Cancel' },
        { label: 'Approved', value: 'Approve Manager' },
        { label: 'Close', value: 'Close' },
    ];
    picJenis: Array<object> = [
        { label: 'All', value: '' },
        { label: 'SVY', value: 'SVY' },
        { label: 'JTR', value: 'JTR' },
        { label: 'EVN', value: 'EVN' },
    ];
    isEmail: Array<object> = [
        { label: 'All', value: '' },
        { label: 'Sudah', value: 'Sudah' },
        { label: 'Belum', value: 'Belum' }
    ];
    is_pv: string;
    selectedMail: string;
    loginName: string;
    noteModal: boolean;
    note_revert: string;
    constructor(
        private returService: ReturService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private masterSupplierService: MasterSupplierService,
        private confirmationService: ConfirmationService,
        private toasterService: ToasterService,
        protected purchaseOrderService: PurchaseOrderService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.isFilter = false;
        this.pic = '';
        this.jenis = '';
        this.selectedMail = ''
        this.selected = new Array<ReturVM>();
        this.loginName = this.principal.getUserLogin();
        this.page = 0;
        this.is_pv = '';
        this.noteModal = false;
        this.note_revert = '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter) {
            const waktu = '23:59:59.000';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            if (this.currentSearch) {
                this.returService.queryKlaim({
                    page: this.page,
                    query: 'jenisklaim:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic + '|email:' + this.selectedMail + '|is_pv:' + this.is_pv,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
            this.returService.queryKlaim({
                page: this.page,
                size: this.itemsPerPage,
                query: 'jenisklaim:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:' + dari + '|dtthru:' + sampai + '|kdsup:' + this.kdSPL + '|jenis_status:' + this.jenis + '|pic:' + this.pic + '|email:' + this.selectedMail + '|is_pv:' + this.is_pv,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            if (this.currentSearch) {
                this.returService.queryKlaim({
                    page: this.page,
                    query: 'jenisklaim:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:|dtthru:|kdsup:|jenis_status:|pic:|email:|is_pv:',

                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.returService.queryKlaim({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'jenisklaim:NR' + '|nolhp:' + this.currentSearch + '|dtfrom:|dtthru:|kdsup:|jenis_status:|pic:|email:|is_pv:',
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/retur'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/retur', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/retur', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInReturs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Retur) {
        return item.id;
    }
    registerChangeInReturs() {
        this.eventSubscriber = this.eventManager.subscribe('returListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.returs = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    cetakRetur(retur: Retur) {
        const filter_data = 'idretur:' + retur.idretur;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }

    downloadRetur(retur: Retur) {
        const filter_data = 'idretur:' + retur.idretur;
        const noretur = retur.noretur;
        this.reportUtilService.downloadFileWithName(`Nota Retur ${noretur}`, process.env.API_C_URL + '/api/report/retur/pdf', { filterData: filter_data });
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }
    public selectSupplier(isSelect?: boolean): void {
        this.kdSPL = this.newSuplier.kd_suppnew;
    }
    filter() {
        this.isFilter = true;
        this.loadAll();
    }
    reset() {
        this.currentSearch = '';
        this.isFilter = false;
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.kdSPL = '';
        this.jenis = '';
        this.pic = '';
        this.newSuplier = new MasterSupplier();
        this.loadAll();
    }
    setstatus(idstatus: number, rowData: Retur) {
        if (idstatus === 13) {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Cancel Retur ini ??',
                accept: () => {
                    this.returService.changeStatus(rowData, idstatus)
                        .subscribe(
                            (res) => {
                                this.loadingService.loadingStart();
                                this.loadAll();
                            }
                        );
                }
            });
        } else {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Untuk Request Approve ??',
                accept: () => {
                    this.returService.changeStatus(rowData, idstatus)
                        .subscribe(
                            (res) => {
                                this.loadingService.loadingStart();
                                this.loadAll();
                            }
                        );
                }
            });
        }

    }

    setstatus1(idstatus: number, rowData: Retur) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Cancel Retur ??',
            accept: () => {
                this.returService.changeStatus(rowData, idstatus)
                    .subscribe(
                        (res) => { alert('Berhasil Cancel'); this.loadAll() },
                    )
            }
        });
    }

    setmail() {
        this.loadingService.loadingStart();
        this.returService.setMailPO(this.selected).subscribe(
            (res) => {
                this.loadAll();
                this.selected = new Array<ReturVM>();
                alert('Sukses Update Sudah Email');
            }
        )
        this.loadingService.loadingStop();
    }

    approveEdit() {
        this.loadingService.loadingStart();
        const allSame = this.selected.every((item) => item.pic === this.loginName);
        if (allSame) {
            const hasStatus13 = this.selected.some((item) =>
                item.status_desc === 'Cancel' || item.status_desc === 'Close' || item.status_desc === 'BBK Sebagian' || item.status_desc === 'Request Approve Accounting'
            );
            if (hasStatus13) {
                alert('Terdapat item dengan status Cancel, Close atau BBK Sebagian, Atau Request Approve Accounting. Tidak dapat melanjutkan proses.');
                this.loadingService.loadingStop();
            } else {
                this.returService.approveEditRetur(this.selected).subscribe(
                    (res) => {
                        alert('Berhasil Approve Edit');
                        this.loadAll();
                        this.selected = new Array<ReturVM>();
                        this.loadingService.loadingStop();
                    }
                );
            }
        } else {
            alert('Terdapat Retur Yang Memiliki Otorisasi Berbeda');
            this.loadingService.loadingStop();
        }
    }

    printExcel() {
        const waktu = '23:59:59.000';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        let kdspl = '';
        let jenis_status = '';
        let pic = '';
        let pv = '';
        const date = new Date();
        const currentDay = date.getDate();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();
        if (this.kdSPL === null || this.kdSPL === undefined || this.kdSPL === '') {
            kdspl = 'all'
        } else {
            kdspl = this.kdSPL;
        }

        if (this.jenis === null || this.jenis === '' || this.jenis === undefined) {
            jenis_status = 'all';
        } else {
            jenis_status = this.jenis;
        }

        if (this.pic === null || this.pic === undefined || this.pic === '') {
            pic = 'all';
        } else {
            pic = this.pic;
        }

        if (this.is_pv === null || this.is_pv === undefined || this.is_pv === '') {
            pv = 'all';
        } else {
            pv = this.is_pv;
        }

        const filter_data = 'dtFrom:' + dari +
            '|dtThru:' + sampai +
            '|kdsup:' + kdspl +
            '|jenis_status:' + jenis_status +
            '|pic:' + pic +
            '|is_pv:' + pv;
        this.reportUtilService.downloadFileWithName(`Report Nota Retur ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/cetakan_nr/xlsx', { filterData: filter_data });
    }

    openPDF(idpp: any) {
        this.purchaseOrderService.getPP1(idpp).subscribe((pp) => {
            if (pp.contentContentType === null || pp.contentContentType === undefined || pp.contentContentType === '') {
                alert('File Attachment Coretax Tidak Ada');
            } else {
                const content = pp.content;
                const contenttype = pp.contentContentType;

                const byteCharacters = atob(content);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }

                const byteArray = new Uint8Array(byteNumbers);

                const blobContent = new Blob([byteArray], { 'type': contenttype });
                const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                // const win = window.open(content);
                window.open(URL.createObjectURL(blobContent), '_blank');
            }

        });
    }

    revertNR(rowData: Retur) {
        this.note_revert = rowData.note_revert;
        this.noteModal = true;
    }

    backapp() {
        this.noteModal = false;
        this.note_revert = '';
    }
}
