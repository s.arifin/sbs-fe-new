import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ToasterService} from '../../shared';
import { ResponseWrapper } from '../../shared';
import { JenisResiko } from './jenis-resiko.model';
import { JenisResikoService } from './jenis-resiko.service';

@Component({
    selector: 'jhi-jenis-resiko-tambah',
    templateUrl: './jenis-resiko-tambah.component.html'
})
export class JenisResikoTambahComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    jenisResiko: JenisResiko;
    isSaving: boolean;
    // risk: JenisResiko;

    constructor(
        protected alertService: JhiAlertService,
        protected jenisResikoService: JenisResikoService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.jenisResiko = new JenisResiko();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.jenisResikoService.findById(id).subscribe((jenisResiko) => {
            this.jenisResiko = jenisResiko;
            console.log('this.jenisResiko', this.jenisResiko);
        });
    }

    previousState() {
        this.router.navigate(['jenis-resiko']);
    }

    save() {
        this.isSaving = true;
        {this.subscribeToSaveResponse(this.jenisResikoService.createR(this.jenisResiko));
        }
    }

    protected subscribeToSaveResponse(result: Observable<JenisResiko>) {
        result.subscribe((res: JenisResiko) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: JenisResiko) {
        this.eventManager.broadcast({ name: 'jenisResikoListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'jenisResiko saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'jenisResiko Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
