import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { JenisResikoService } from './jenis-resiko.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ToasterService } from '../../shared/alert/toaster.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { JenisResiko} from './jenis-resiko.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-jenis-resiko',
    templateUrl: './jenis-resiko.component.html'
})
export class JenisResikoComponent implements OnInit, OnDestroy {

    currentAccount: any;
    jenisResikos: JenisResiko[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    // printInsurance: boolean;
    // editInsurance: boolean;
    // insuranceTypeUpdate: InsuranceType;
    isSaving: boolean;
    searching: boolean;
    // insuranceType: InsuranceType;

    constructor(
        protected jenisResikoService: JenisResikoService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected toaster: ToasterService,
        protected activeModal: NgbActiveModal,
        protected loadingService: LoadingService,
        protected confirmationService: ConfirmationService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.searching = false;
        this.currentSearch = activatedRoute.snapshot &&
        this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        // this.loadingService.loadingStart();
        if (this.searching) {
            this.jenisResikoService.search({
                page: this.page,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.jenisResikoService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        // this.loadingService.loadingStop();        this.loadingService.loadingStop();
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/jenis-resiko'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.searching = false;
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.searching = true;
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInJenisResikos();
    }

    ngOnDestroy() {
       // this.eventManager.destroy(this.eventSubscriber);
    }

    // save() {
    //     this.isSaving = true;
    //     if (this.insuranceType.idInsuranceType !== undefined) {
    //         this.subscribeToSaveResponse(
    //             this.tipeAsuransiService.update(this.insuranceType));
    //     } else {
    //         this.subscribeToSaveResponse(
    //             this.tipeAsuransiService.create(this.insuranceType));
    //     }
    // }

    protected subscribeToSaveResponse(result: Observable<JenisResiko>) {
        result.subscribe((res: JenisResiko) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: JenisResiko) {
        this.eventManager.broadcast({ name: 'jenisResikoListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'JenisResiko saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    trackId(index: number, item: JenisResiko) {
        return item.idristyp;
    }
    registerChangeInJenisResikos() {
        this.eventSubscriber = this.eventManager.subscribe('jenisResikoListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idristyp') {
            result.push('idristyp');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        if (data[0] !== undefined) {
            this.totalItems = data[0].TotalData;
        } else {
            this.totalItems = 0;
        }
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.jenisResikos = null;
        this.jenisResikos = data;
        console.log('this.jenisResikos', this.jenisResikos);
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    // executeProcess(item) {
    //     this.jenisResikoService.executeProcess(item).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    // buildReindex() {
    //     this.jenisResikoService.process({command: 'buildIndex'}).subscribe((r) => {
    //         this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
    //     });
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected onRowDataSaveSuccess(result: JenisResiko) {
        this.toasterService.showToaster('info', 'JenisResiko Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.jenisResikoService.update(event.data)
                .subscribe((res: JenisResiko) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.jenisResikoService.create(event.data)
                .subscribe((res: JenisResiko) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    gotoDetail(data: JenisResiko) {
        this.jenisResikoService.changeShareDataTmp(data);
        this.router.navigate(['../jenis-resiko-edit']);
    }

    previousState() {
        this.activeModal.dismiss('cancel');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.jenisResikoService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'jenisResikoListModification',
                    content: 'Deleted an jenisResiko'
                    });
                    this.loadAll();
                });
            }
        });
    }
}
