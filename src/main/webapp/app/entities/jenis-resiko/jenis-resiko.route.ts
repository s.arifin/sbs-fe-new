import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { JenisResikoComponent } from './jenis-resiko.component';
import { JenisResikoEditComponent } from './jenis-resiko-edit.component';
import { JenisResikoTambahComponent } from './jenis-resiko-tambah.component';

@Injectable()
export class JenisResikoResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idristyp,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const jenisResikoRoute: Routes = [
    {
        path: 'jenis-resiko',
        component: JenisResikoComponent,
        resolve: {
            'pagingParams': JenisResikoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.jenisResiko.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'jenis-resiko-edit',
        component: JenisResikoEditComponent,
        resolve: {
            'pagingParams': JenisResikoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.jenisResiko.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'jenis-resiko-tambah',
        component: JenisResikoTambahComponent,
        resolve: {
            'pagingParams': JenisResikoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.jenisResiko.home.title'
        },
        canActivate: [UserRouteAccessService]
    }

];

export const jenisResikoPopupRoute: Routes = [
    {
        path: 'party-category-type-new',
        component: JenisResikoEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
    // {
    //     path: 'leasing-tenor-provide-lov',
    //     component: LeasingTenorProvideLovPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.leasingTenorProvide.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'leasing-tenor-provide-new',
    //     component: LeasingTenorProvidePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.leasingTenorProvide.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'leasing-tenor-provide/:id/edit',
    //     component: LeasingTenorProvidePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.leasingTenorProvide.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'leasing-tenor-provide/:id/view/:view',
    //     component: LeasingTenorProvidePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.leasingTenorProvide.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
