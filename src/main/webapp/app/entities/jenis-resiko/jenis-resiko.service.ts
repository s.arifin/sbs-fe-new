import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JenisResiko } from './jenis-resiko.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class JenisResikoService {

    valueTmp = {};
    protected dataSource = new BehaviorSubject<any>(this.valueTmp);
    currentData = this.dataSource.asObservable();
    private resourceUrl = SERVER_API_URL + 'api/jenis-resikos';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/jenis-resikos';
    protected resourceCUr = process.env.API_C_URL + '/api/InsuranceRisk';
    protected dummy = 'http://localhost:52374/api/InsuranceRisk';

    constructor(private http: Http) { }

    create(jenisResiko: JenisResiko): Observable<JenisResiko> {
        const copy = this.convert(jenisResiko);
        return this.http.post(this.dummy, copy).map((res: Response) => {
            return res.json();
        });
    }

    createR(jenisResiko: JenisResiko): Observable<JenisResiko> {
        const copy = this.convert(jenisResiko);
        return this.http.post(this.resourceCUr + '/Create', copy).map((res: Response) => {
            return res.json();
        });
    }

    update(jenisResiko: JenisResiko): Observable<JenisResiko> {
        const copy = this.convert(jenisResiko);
        return this.http.put(this.dummy, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateR(jenisResiko: JenisResiko): Observable<JenisResiko> {
        const copy = this.convert(jenisResiko);
        return this.http.post(this.resourceCUr + '/Update', copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<JenisResiko> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findById(id: any): Observable<JenisResiko> {
        return this.http.get(`${this.resourceCUr}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(id: Number, param: String, jenisResiko: JenisResiko): Observable<JenisResiko> {
        const copy = this.convert(jenisResiko);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('Masukkkkk');
        return this.http.get(this.resourceCUr + '/GetData', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(this.resourceCUr + '/DelData?id=' + id);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUr + '/FindData', options)
            .map((res: any) => this.convertResponse(res));
    }

    getByFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUr + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.dummy, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(jenisResiko: JenisResiko): JenisResiko {
        const copy: JenisResiko = Object.assign({}, jenisResiko);
        return copy;
    }

    changeShareDataTmp(datatmp) {
        this.dataSource.next(datatmp);
    }
}
