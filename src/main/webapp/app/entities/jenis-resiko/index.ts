export * from './jenis-resiko.model';
export * from './jenis-resiko.service';
export * from './jenis-resiko.route';
export * from './jenis-resiko-popup.service';
export * from './jenis-resiko.module';
export * from './jenis-resiko.component';
export * from './jenis-resiko-edit.component';
export * from './jenis-resiko-tambah.component';
