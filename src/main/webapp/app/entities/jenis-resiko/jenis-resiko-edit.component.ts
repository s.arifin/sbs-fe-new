import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ToasterService} from '../../shared';
import { ResponseWrapper } from '../../shared';
import { JenisResiko } from './jenis-resiko.model';
import { JenisResikoService } from './jenis-resiko.service';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-jenis-resiko-edit',
    templateUrl: './jenis-resiko-edit.component.html'
})
export class JenisResikoEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    jenisResiko: JenisResiko;
    isSaving: boolean;
    idristyp: any;
    paramPage: number;
    routeId: number;

    constructor(
        protected alertService: JhiAlertService,
        protected jenisResikoService: JenisResikoService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        private loadingService: LoadingService
    ) {
        this.jenisResiko = new JenisResiko();
    }

    ngOnInit() {
        this.jenisResikoService.currentData.subscribe(
            (res) => this.jenisResiko = res
        )
        this.isSaving = false;
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    load(id) {
        this.jenisResikoService.findById(id).subscribe((jenisResiko) => {
            this.jenisResiko = jenisResiko;
            console.log('this.jenisResiko', this.jenisResiko);
        });
    }

    previousState() {
        this.router.navigate(['../jenis-resiko']);
    }

    save() {
        this.isSaving = true;
        this.jenisResikoService.updateR(this.jenisResiko).subscribe(
            (res) => this.onSaveSuccess(res),
            (res) => this.onSaveError(res)
        )
    }

    protected subscribeToSaveResponse(result: Observable<JenisResiko>) {
        result.subscribe((res: JenisResiko) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: JenisResiko) {
        this.eventManager.broadcast({ name: 'jenisResikoListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'jenisResiko saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'jenisResiko Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
