import { BaseEntity } from './../../shared';

export class JenisResiko implements BaseEntity {
    constructor(
        public id?: any,
        public idristyp?: String,
        public description?: String,
    ) {
    }
}
