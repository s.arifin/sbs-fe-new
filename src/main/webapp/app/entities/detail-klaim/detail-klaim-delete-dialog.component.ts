import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DetailKlaim } from './detail-klaim.model';
import { DetailKlaimPopupService } from './detail-klaim-popup.service';
import { DetailKlaimService } from './detail-klaim.service';

@Component({
    selector: 'jhi-detail-klaim-delete-dialog',
    templateUrl: './detail-klaim-delete-dialog.component.html'
})
export class DetailKlaimDeleteDialogComponent {

    detailKlaim: DetailKlaim;

    constructor(
        private detailKlaimService: DetailKlaimService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.detailKlaimService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'detailKlaimListModification',
                content: 'Deleted an detailKlaim'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-detail-klaim-delete-popup',
    template: ''
})
export class DetailKlaimDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailKlaimPopupService: DetailKlaimPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.detailKlaimPopupService
                .open(DetailKlaimDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
