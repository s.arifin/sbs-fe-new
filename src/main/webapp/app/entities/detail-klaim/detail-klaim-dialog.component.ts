import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DetailKlaim } from './detail-klaim.model';
import { DetailKlaimPopupService } from './detail-klaim-popup.service';
import { DetailKlaimService } from './detail-klaim.service';

@Component({
    selector: 'jhi-detail-klaim-dialog',
    templateUrl: './detail-klaim-dialog.component.html'
})
export class DetailKlaimDialogComponent implements OnInit {

    detailKlaim: DetailKlaim;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private detailKlaimService: DetailKlaimService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.detailKlaim.id !== undefined) {
            this.subscribeToSaveResponse(
                this.detailKlaimService.update(this.detailKlaim));
        } else {
            this.subscribeToSaveResponse(
                this.detailKlaimService.create(this.detailKlaim));
        }
    }

    private subscribeToSaveResponse(result: Observable<DetailKlaim>) {
        result.subscribe((res: DetailKlaim) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DetailKlaim) {
        this.eventManager.broadcast({ name: 'detailKlaimListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-detail-klaim-popup',
    template: ''
})
export class DetailKlaimPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailKlaimPopupService: DetailKlaimPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.detailKlaimPopupService
                    .open(DetailKlaimDialogComponent as Component, params['id']);
            } else {
                this.detailKlaimPopupService
                    .open(DetailKlaimDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
