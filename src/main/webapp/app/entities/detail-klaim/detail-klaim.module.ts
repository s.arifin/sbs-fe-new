import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    DetailKlaimService,
    DetailKlaimPopupService,
    DetailKlaimComponent,
    DetailKlaimDetailComponent,
    DetailKlaimDialogComponent,
    DetailKlaimPopupComponent,
    DetailKlaimDeletePopupComponent,
    DetailKlaimDeleteDialogComponent,
    detailKlaimRoute,
    detailKlaimPopupRoute,
    DetailKlaimResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...detailKlaimRoute,
    ...detailKlaimPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DetailKlaimComponent,
        DetailKlaimDetailComponent,
        DetailKlaimDialogComponent,
        DetailKlaimDeleteDialogComponent,
        DetailKlaimPopupComponent,
        DetailKlaimDeletePopupComponent,
    ],
    entryComponents: [
        DetailKlaimComponent,
        DetailKlaimDialogComponent,
        DetailKlaimPopupComponent,
        DetailKlaimDeleteDialogComponent,
        DetailKlaimDeletePopupComponent,
    ],
    providers: [
        DetailKlaimService,
        DetailKlaimPopupService,
        DetailKlaimResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDetailKlaimModule {}
