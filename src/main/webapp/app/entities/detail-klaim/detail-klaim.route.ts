import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DetailKlaimComponent } from './detail-klaim.component';
import { DetailKlaimDetailComponent } from './detail-klaim-detail.component';
import { DetailKlaimPopupComponent } from './detail-klaim-dialog.component';
import { DetailKlaimDeletePopupComponent } from './detail-klaim-delete-dialog.component';

@Injectable()
export class DetailKlaimResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const detailKlaimRoute: Routes = [
    {
        path: 'detail-klaim',
        component: DetailKlaimComponent,
        resolve: {
            'pagingParams': DetailKlaimResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailKlaim.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'detail-klaim/:id',
        component: DetailKlaimDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailKlaim.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const detailKlaimPopupRoute: Routes = [
    {
        path: 'detail-klaim-new',
        component: DetailKlaimPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailKlaim.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-klaim/:id/edit',
        component: DetailKlaimPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailKlaim.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-klaim/:id/delete',
        component: DetailKlaimDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailKlaim.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
