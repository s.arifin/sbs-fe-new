export * from './detail-klaim.model';
export * from './detail-klaim-popup.service';
export * from './detail-klaim.service';
export * from './detail-klaim-dialog.component';
export * from './detail-klaim-delete-dialog.component';
export * from './detail-klaim-detail.component';
export * from './detail-klaim.component';
export * from './detail-klaim.route';
