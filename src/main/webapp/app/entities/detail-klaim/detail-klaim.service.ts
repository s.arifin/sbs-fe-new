import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { DetailKlaim } from './detail-klaim.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DetailKlaimService {

    private resourceUrl = process.env.API_C_URL + '/api/detail-klaims';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/detail-klaims';

    constructor(private http: Http) { }

    create(detailKlaim: DetailKlaim): Observable<DetailKlaim> {
        const copy = this.convert(detailKlaim);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(detailKlaim: DetailKlaim): Observable<DetailKlaim> {
        const copy = this.convert(detailKlaim);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<DetailKlaim> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(detailKlaim: DetailKlaim): DetailKlaim {
        const copy: DetailKlaim = Object.assign({}, detailKlaim);
        return copy;
    }
    private convertArray(detailKlaims: DetailKlaim[]): Array<DetailKlaim> {
        const copy: DetailKlaim[] = Object.assign([], detailKlaims);
        return copy;
    }

    approveManagement(detailKlaims: DetailKlaim[]): Observable<DetailKlaim[]> {
        const copy = this.convertArray(detailKlaims);
        return this.http.put(this.resourceUrl + '/approve-management', copy).map((res: Response) => {
            return res.json();
        });
    }
}
