import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DetailKlaim } from './detail-klaim.model';
import { DetailKlaimService } from './detail-klaim.service';

@Component({
    selector: 'jhi-detail-klaim-detail',
    templateUrl: './detail-klaim-detail.component.html'
})
export class DetailKlaimDetailComponent implements OnInit, OnDestroy {

    detailKlaim: DetailKlaim;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private detailKlaimService: DetailKlaimService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDetailKlaims();
    }

    load(id) {
        this.detailKlaimService.find(id).subscribe((detailKlaim) => {
            this.detailKlaim = detailKlaim;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDetailKlaims() {
        this.eventSubscriber = this.eventManager.subscribe(
            'detailKlaimListModification',
            (response) => this.load(this.detailKlaim.id)
        );
    }
}
