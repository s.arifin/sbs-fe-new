import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PurchasingItem } from './purchasing-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PurchasingItemService {

    private resourceUrl = process.env.API_C_URL + '/api/purchasing-items';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/purchasing-items';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(purchasingItem: PurchasingItem): Observable<PurchasingItem> {
        const copy = this.convert(purchasingItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(purchasingItem: PurchasingItem): Observable<PurchasingItem> {
        const copy = this.convert(purchasingItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PurchasingItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtsent = this.dateUtils
            .convertDateTimeFromServer(entity.dtsent);
    }

    private convert(purchasingItem: PurchasingItem): PurchasingItem {
        const copy: PurchasingItem = Object.assign({}, purchasingItem);

        copy.dtsent = this.dateUtils.toDate(purchasingItem.dtsent);
        return copy;
    }

    findByIdPurchasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-idpurchasing' , options)
            .map((res: Response) => this.convertResponse(res));
    }
}
