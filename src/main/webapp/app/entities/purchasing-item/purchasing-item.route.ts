import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PurchasingItemComponent } from './purchasing-item.component';
import { PurchasingItemDetailComponent } from './purchasing-item-detail.component';
import { PurchasingItemPopupComponent } from './purchasing-item-dialog.component';
import { PurchasingItemDeletePopupComponent } from './purchasing-item-delete-dialog.component';

@Injectable()
export class PurchasingItemResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const purchasingItemRoute: Routes = [
    {
        path: 'purchasing-item',
        component: PurchasingItemComponent,
        resolve: {
            'pagingParams': PurchasingItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasingItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'purchasing-item/:id',
        component: PurchasingItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasingItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const purchasingItemPopupRoute: Routes = [
    {
        path: 'purchasing-item-new',
        component: PurchasingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'purchasing-item/:id/edit',
        component: PurchasingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'purchasing-item/:id/delete',
        component: PurchasingItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchasingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
