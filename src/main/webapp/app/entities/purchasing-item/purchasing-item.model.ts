import { BaseEntity } from './../../shared';
import { OrderItem } from '../order-item'
import { PurchaseOrderItem } from '../purchase-order-item';

export class PurchasingItem implements BaseEntity {
    constructor(
        public id?: number,
        public idpurchasingitem?: any,
        public idpurchasingfk?: any,
        public productcode?: string,
        public productname?: string,
        public productname2?: string,
        public productname3?: string,
        public productname4?: string,
        public productname5?: string,
        public uom?: string,
        public qty?: number,
        public ppqty?: number,
        public price?: number,
        public bidprice?: number,
        public totalprice?: number,
        public dtsent?: Date,
        public divitions?: string,
        public noPP?: any,
        public idPurcOrdIte?: any,
        public orderItem?: PurchaseOrderItem,
        public qtypoTemp?: number,
        public sisaPP?: number,
        public qtyTemp?: number,
        public qty_kirim?: number,
        public nourut?: number,
        public divreceipt?: string,
        public dtsentnote?: string,
        public dtsentpp?: Date,
        public idprior?: any,
        public disc?: number,
        public discpercent?: number,
        public contentContentType?: string,
        public content?: any,
        public is_bbm?: any
    ) {
    }
}

export class MappingUserPO implements BaseEntity {
    constructor(
        public id?: number,
        public idmapuspo?: any,
        public idpurchaseorder?: any,
        public username?: string
    ) { }
}

export class UOMPO {
    constructor(
        public kdbahan?: string,
        public uom?: string
    ) { }
}

export class MasterDiv {
    constructor(
        public value?: string,
        public label?: string
    ) { }
}
