import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PurchasingItem } from './purchasing-item.model';
import { PurchasingItemPopupService } from './purchasing-item-popup.service';
import { PurchasingItemService } from './purchasing-item.service';

@Component({
    selector: 'jhi-purchasing-item-dialog',
    templateUrl: './purchasing-item-dialog.component.html'
})
export class PurchasingItemDialogComponent implements OnInit {

    purchasingItem: PurchasingItem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private purchasingItemService: PurchasingItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.purchasingItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.purchasingItemService.update(this.purchasingItem));
        } else {
            this.subscribeToSaveResponse(
                this.purchasingItemService.create(this.purchasingItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<PurchasingItem>) {
        result.subscribe((res: PurchasingItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PurchasingItem) {
        this.eventManager.broadcast({ name: 'purchasingItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-purchasing-item-popup',
    template: ''
})
export class PurchasingItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private purchasingItemPopupService: PurchasingItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.purchasingItemPopupService
                    .open(PurchasingItemDialogComponent as Component, params['id']);
            } else {
                this.purchasingItemPopupService
                    .open(PurchasingItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
