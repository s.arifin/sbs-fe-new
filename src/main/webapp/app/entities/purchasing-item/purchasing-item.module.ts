import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PurchasingItemService,
    PurchasingItemPopupService,
    PurchasingItemComponent,
    PurchasingItemDetailComponent,
    PurchasingItemDialogComponent,
    PurchasingItemPopupComponent,
    PurchasingItemDeletePopupComponent,
    PurchasingItemDeleteDialogComponent,
    purchasingItemRoute,
    purchasingItemPopupRoute,
    PurchasingItemResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...purchasingItemRoute,
    ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PurchasingItemComponent,
        PurchasingItemDetailComponent,
        PurchasingItemDialogComponent,
        PurchasingItemDeleteDialogComponent,
        PurchasingItemPopupComponent,
        PurchasingItemDeletePopupComponent,
    ],
    entryComponents: [
        PurchasingItemComponent,
        PurchasingItemDialogComponent,
        PurchasingItemPopupComponent,
        PurchasingItemDeleteDialogComponent,
        PurchasingItemDeletePopupComponent,
    ],
    providers: [
        PurchasingItemService,
        PurchasingItemPopupService,
        PurchasingItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPurchasingItemModule {}
