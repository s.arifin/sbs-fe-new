import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PurchasingItem } from './purchasing-item.model';
import { PurchasingItemPopupService } from './purchasing-item-popup.service';
import { PurchasingItemService } from './purchasing-item.service';

@Component({
    selector: 'jhi-purchasing-item-delete-dialog',
    templateUrl: './purchasing-item-delete-dialog.component.html'
})
export class PurchasingItemDeleteDialogComponent {

    purchasingItem: PurchasingItem;

    constructor(
        private purchasingItemService: PurchasingItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.purchasingItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'purchasingItemListModification',
                content: 'Deleted an purchasingItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-purchasing-item-delete-popup',
    template: ''
})
export class PurchasingItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private purchasingItemPopupService: PurchasingItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.purchasingItemPopupService
                .open(PurchasingItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
