import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PurchasingItem } from './purchasing-item.model';
import { PurchasingItemService } from './purchasing-item.service';

@Injectable()
export class PurchasingItemPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private purchasingItemService: PurchasingItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.purchasingItemService.find(id).subscribe((purchasingItem) => {
                    // purchasingItem.dtsent = this.datePipe.transform(purchasingItem.dtsent, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.purchasingItemModalRef(component, purchasingItem);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.purchasingItemModalRef(component, new PurchasingItem());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    purchasingItemModalRef(component: Component, purchasingItem: PurchasingItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.purchasingItem = purchasingItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
