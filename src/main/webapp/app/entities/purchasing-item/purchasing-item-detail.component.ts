import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PurchasingItem } from './purchasing-item.model';
import { PurchasingItemService } from './purchasing-item.service';

@Component({
    selector: 'jhi-purchasing-item-detail',
    templateUrl: './purchasing-item-detail.component.html'
})
export class PurchasingItemDetailComponent implements OnInit, OnDestroy {

    purchasingItem: PurchasingItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private purchasingItemService: PurchasingItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPurchasingItems();
    }

    load(id) {
        this.purchasingItemService.find(id).subscribe((purchasingItem) => {
            this.purchasingItem = purchasingItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPurchasingItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'purchasingItemListModification',
            (response) => this.load(this.purchasingItem.id)
        );
    }
}
