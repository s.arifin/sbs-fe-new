import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { AnnualLeave, AnnualLeaveService, AnnualLeaveResolvePagingParams, AnnualLeaveNewComponent, AnnualLeaveRegEditComponent } from '.';
import { AnnualLeaveComponent } from './annual-leave.component';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule, TabViewModule, } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AnnualLeaveRoute } from './annual-leave.route';
import { AnnualLeaveRegComponent } from './annual-leave-reg.component';
import { ApprovalJobOrderDetailComponent } from './approval-ct/approval-annual-detail.component';
import { ApprovalAnnualComponent } from './approval-ct/approval-annual.component';
import { TabViewAnnualComponent } from './approval-ct/tab-view-annual.component';
import { AnnualLeaveIduNewComponent } from './annual-leave-idu-new.component';

const ENTITY_STATES = [
    ...AnnualLeaveRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        TabViewModule,
        InputTextareaModule
    ],
    declarations: [
        AnnualLeaveComponent,
        AnnualLeaveNewComponent,
        AnnualLeaveIduNewComponent,
        AnnualLeaveRegComponent,
        AnnualLeaveRegEditComponent,
        ApprovalAnnualComponent,
        ApprovalJobOrderDetailComponent,
        TabViewAnnualComponent
    ],
    entryComponents: [
        AnnualLeaveComponent,
        AnnualLeaveNewComponent,
        AnnualLeaveIduNewComponent,
        AnnualLeaveRegComponent,
        AnnualLeaveRegEditComponent,
        ApprovalAnnualComponent,
        ApprovalJobOrderDetailComponent,
        TabViewAnnualComponent
    ],
    providers: [
        AnnualLeaveService,
        AnnualLeaveResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmAnnualLeaveModule {}
