import { BaseEntity } from '../../shared';

export class AnnualLeave implements BaseEntity {
    constructor(
        public id?: number,
        public idcuti?: any,
        public noDoc?: any,
        public name?: string,
        public div?: string,
        public loc?: string,
        public dtStart?: Date,
        public dtThru?: Date,
        public nik?: any,
        public jabatan?: any,
        public jmbReqCuti?: any,
        public app?: any,
        public sisaCuti?: any,
        public hakCuti?: any,
        public jmlHakCuti?: any,
        public cutiYgDiambil?: any,
        public sisCtiTgl?: any,
        public catatan?: any,
        public statustype?: any,
        public statDesc?: any,
        public seksie?: any,
        public jmbReqIdu?: any,
        public jmbReqItu?: any,
        public type?: any,
        public ket?: any
    ) {
    }
}
