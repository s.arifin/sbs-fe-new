import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { AnnualLeaveComponent } from './annual-leave.component';
import { AnnualLeaveNewComponent } from './annual-leave-new.component';
import { AnnualLeaveIduNewComponent } from './annual-leave-idu-new.component';
import { AnnualLeaveRegEditComponent } from './annual-leave-reg-edit.component';
import { AnnualLeaveRegComponent } from '.';
import { ApprovalAnnualComponent } from './approval-ct/approval-annual.component';

@Injectable()
export class AnnualLeaveResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const AnnualLeaveRoute: Routes = [
    {
        path: 'annual-leave-req',
        component: AnnualLeaveComponent,
        resolve: {
            'pagingParams': AnnualLeaveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.annual.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'annual-leave-reg',
        component: AnnualLeaveRegComponent,
        resolve: {
            'pagingParams': AnnualLeaveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.annual.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'annual-leave-new',
        component: AnnualLeaveNewComponent,
        resolve: {
            'pagingParams': AnnualLeaveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.annual.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'annual-leave-idu-new',
        component: AnnualLeaveIduNewComponent,
        resolve: {
            'pagingParams': AnnualLeaveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.annual.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'annualleavereg/:route/:page/:id/edit',
        component: AnnualLeaveRegEditComponent,
        resolve: {
            'pagingParams': AnnualLeaveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.annual.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'annualleave/approval',
        component: ApprovalAnnualComponent,
        resolve: {
            'pagingParams': AnnualLeaveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.overtime.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'annual-leave-reg-edit',
    //     component: AnnualLeaveRegEditComponent,
    //     resolve: {
    //         'pagingParams': AnnualLeaveResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.annual.home.createLabel'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
];

export const  AnnualLeavePopupRoute: Routes = [
    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
