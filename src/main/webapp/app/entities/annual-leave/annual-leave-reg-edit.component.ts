import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { AnnualLeave } from './annual-leave.model';
import { AnnualLeaveService } from './annual-leave.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
@Component({
    selector: 'jhi-annual-leave-reg-edit',
    templateUrl: './annual-leave-reg-edit.component.html'
})
export class AnnualLeaveRegEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    joborders: AnnualLeave;
    picRole: AnnualLeave;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;
    dtOrder: any;
    dtStart: any;
    dtFinish: any;
    listKdBarang = [{}];
    dtStartTmln: Date;
    dtFinishTmln: Date;
    internals: any;
    dtFnsBfr: any;
    inSisCuti: any;
    inHakCuti: any;
    inJmbHakCuti: any;
    InCutYgDiabl: any;
    inSisCtiTgl: any;
    inCatatan: any;

    divitions: Array<object> = [
        {label : 'PPIC', value : 'PPIC'},
        {label : 'PROD', value : 'PROD'},
        {label : 'LPM', value : 'LPM'},
        {label : 'PURCHASING', value : 'PURCHASING'},
        {label : 'RND', value : 'RND'},
        {label : 'TEKNIK SINDE 1', value : 'TEKNIK SINDE 1'},
        {label : 'TEKNIK SINDE 4', value : 'TEKNIK SINDE 4'},
        {label : 'MARKETING', value : 'MARKETING'},
        {label : 'FNA', value : 'FNA'},
        {label : 'PSU', value : 'PSU'},
        {label : 'IT', value : 'IT'},
        {label : 'HSE', value : 'HSE'}
    ];

    sifats: Array<object> = [
        {label : 'Biasa', value : 'Biasa'},
        {label : 'Segera', value : 'Segera'},
        {label : 'Penting', value : 'Penting'}

    ];

    needs: Array<object> = [
        {label : 'Rutin', value : 'Rutin'},
        {label : 'Non Rutin', value : 'Non Rutin'}

    ];

    constructor(
        protected alertService: JhiAlertService,
        protected jobOrdersService: AnnualLeaveService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected loadingService: LoadingService,
        protected dataUtils: JhiDataUtils,
    ) {
        this.joborders = new AnnualLeave();
        this.picRole = new AnnualLeave();
        this.routeId = 0;
        this.readonly = true;
        this.dtRecive = new Date();
        this.dtStartTmln = new Date();
        this.dtFinishTmln = new Date();
        this.dtFnsBfr = new Date();
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    download(field: any, fieldContentType: string, idInput: string) {
        const byteCharacters = atob(field);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blob = new Blob([byteArray], {'type': fieldContentType});

        if (navigator.msSaveBlob) {
            const filename = fieldContentType;
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');

            link.href = URL.createObjectURL(blob);

            link.setAttribute('visibility', 'hidden');
            link.download = fieldContentType;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            console.log('params = ', params);
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
                console.log('idIF = ', params['id']);
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.internals =  this.principal.getIdInternal();
        console.log('internal : ', this.internals)

        this.jobOrdersService.find(this.idOrder).subscribe((joborders) => {
            console.log('date = ', this.idOrder);
            this.picRole = joborders;
            this.dtStart = new Date(this.picRole.dtStart);
            this.dtFinish = new Date(this.picRole.dtThru);
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['annual-leave-reg', { page: this.paramPage }]);
        }
    }

    complete() {
        // this.jobOrdersService.changeStatuses(this.joborders, this.idOrder, 16)
        // .subscribe((r) => {
        //    this.previousState();
        //    this.toaster.showToaster('info', 'Data Complete', 'Complete.....');
        //  });
    }

    simpan() {
        const obj = {
            idcuti: this.picRole.idcuti,
            sisaCuti: this.picRole.sisaCuti,
            hakCuti: this.picRole.hakCuti,
            jmlHakCuti: this.picRole.jmlHakCuti,
            cutiYgDiambil: this.picRole.cutiYgDiambil,
            sisCtiTgl: this.picRole.sisCtiTgl,
            catatan: this.picRole.catatan
        }
        this.loadingService.loadingStart();
        console.log('isi simpan', obj);
        this.jobOrdersService.createHrd(obj)
        .subscribe(
            (res: ResponseWrapper) =>
            this.onSaveSuccessSimpan(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    private onSaveSuccessSimpan(data, headers) {
        // this.print(result.idOrder);
        this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Cuti saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    back() {
        this.router.navigate(['/annual-leave-reg'])
    }

    protected subscribeToSaveResponse(result: Observable<AnnualLeave>) {
        result.subscribe((res: AnnualLeave) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: AnnualLeave) {
        // this.print(result.idOrder);
        this.eventManager.broadcast({ name: 'annualLeavesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Cuti saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    print(id: any) {
        // const filter_data = 'idPruchaseOrder:' + id;
        // this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
        // // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'Cuti Changed', error.message);
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
