import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TandaTerimaDocument } from './tanda-terima-document.model';
import { TandaTerimaDocumentPopupService } from './tanda-terima-document-popup.service';
import { TandaTerimaDocumentService } from './tanda-terima-document.service';

@Component({
    selector: 'jhi-tanda-terima-document-delete-dialog',
    templateUrl: './tanda-terima-document-delete-dialog.component.html'
})
export class TandaTerimaDocumentDeleteDialogComponent {

    tandaTerimaDocument: TandaTerimaDocument;

    constructor(
        private tandaTerimaDocumentService: TandaTerimaDocumentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tandaTerimaDocumentService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tandaTerimaDocumentListModification',
                content: 'Deleted an tandaTerimaDocument'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tanda-terima-document-delete-popup',
    template: ''
})
export class TandaTerimaDocumentDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tandaTerimaDocumentPopupService: TandaTerimaDocumentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tandaTerimaDocumentPopupService
                .open(TandaTerimaDocumentDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
