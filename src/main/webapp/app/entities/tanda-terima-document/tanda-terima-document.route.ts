import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TandaTerimaDocumentComponent } from './tanda-terima-document.component';
import { TandaTerimaDocumentDetailComponent } from './tanda-terima-document-detail.component';
import { TandaTerimaDocumentPopupComponent } from './tanda-terima-document-dialog.component';
import { TandaTerimaDocumentDeletePopupComponent } from './tanda-terima-document-delete-dialog.component';

@Injectable()
export class TandaTerimaDocumentResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const tandaTerimaDocumentRoute: Routes = [
    {
        path: 'tanda-terima-document',
        component: TandaTerimaDocumentComponent,
        resolve: {
            'pagingParams': TandaTerimaDocumentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerimaDocument.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-document/:id',
        component: TandaTerimaDocumentDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerimaDocument.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tandaTerimaDocumentPopupRoute: Routes = [
    {
        path: 'tanda-terima-document-new',
        component: TandaTerimaDocumentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerimaDocument.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tanda-terima-document/:id/edit',
        component: TandaTerimaDocumentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerimaDocument.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tanda-terima-document/:id/delete',
        component: TandaTerimaDocumentDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerimaDocument.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
