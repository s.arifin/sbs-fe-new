import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { TandaTerimaDocument } from './tanda-terima-document.model';
import { TandaTerimaDocumentPopupService } from './tanda-terima-document-popup.service';
import { TandaTerimaDocumentService } from './tanda-terima-document.service';

@Component({
    selector: 'jhi-tanda-terima-document-dialog',
    templateUrl: './tanda-terima-document-dialog.component.html'
})
export class TandaTerimaDocumentDialogComponent implements OnInit {

    tandaTerimaDocument: TandaTerimaDocument;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private tandaTerimaDocumentService: TandaTerimaDocumentService,
        private elementRef: ElementRef,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.tandaTerimaDocument, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tandaTerimaDocument.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tandaTerimaDocumentService.update(this.tandaTerimaDocument));
        } else {
            this.subscribeToSaveResponse(
                this.tandaTerimaDocumentService.create(this.tandaTerimaDocument));
        }
    }

    private subscribeToSaveResponse(result: Observable<TandaTerimaDocument>) {
        result.subscribe((res: TandaTerimaDocument) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TandaTerimaDocument) {
        this.eventManager.broadcast({ name: 'tandaTerimaDocumentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-tanda-terima-document-popup',
    template: ''
})
export class TandaTerimaDocumentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tandaTerimaDocumentPopupService: TandaTerimaDocumentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tandaTerimaDocumentPopupService
                    .open(TandaTerimaDocumentDialogComponent as Component, params['id']);
            } else {
                this.tandaTerimaDocumentPopupService
                    .open(TandaTerimaDocumentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
