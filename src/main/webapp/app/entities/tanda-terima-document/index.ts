export * from './tanda-terima-document.model';
export * from './tanda-terima-document-popup.service';
export * from './tanda-terima-document.service';
export * from './tanda-terima-document-dialog.component';
export * from './tanda-terima-document-delete-dialog.component';
export * from './tanda-terima-document-detail.component';
export * from './tanda-terima-document.component';
export * from './tanda-terima-document.route';
