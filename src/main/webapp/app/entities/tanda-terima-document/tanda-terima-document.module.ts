import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    TandaTerimaDocumentService,
    TandaTerimaDocumentPopupService,
    TandaTerimaDocumentComponent,
    TandaTerimaDocumentDetailComponent,
    TandaTerimaDocumentDialogComponent,
    TandaTerimaDocumentPopupComponent,
    TandaTerimaDocumentDeletePopupComponent,
    TandaTerimaDocumentDeleteDialogComponent,
    tandaTerimaDocumentRoute,
    tandaTerimaDocumentPopupRoute,
    TandaTerimaDocumentResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...tandaTerimaDocumentRoute,
    ...tandaTerimaDocumentPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TandaTerimaDocumentComponent,
        TandaTerimaDocumentDetailComponent,
        TandaTerimaDocumentDialogComponent,
        TandaTerimaDocumentDeleteDialogComponent,
        TandaTerimaDocumentPopupComponent,
        TandaTerimaDocumentDeletePopupComponent,
    ],
    entryComponents: [
        TandaTerimaDocumentComponent,
        TandaTerimaDocumentDialogComponent,
        TandaTerimaDocumentPopupComponent,
        TandaTerimaDocumentDeleteDialogComponent,
        TandaTerimaDocumentDeletePopupComponent,
    ],
    providers: [
        TandaTerimaDocumentService,
        TandaTerimaDocumentPopupService,
        TandaTerimaDocumentResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmTandaTerimaDocumentModule {}
