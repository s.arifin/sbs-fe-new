import { BaseEntity } from './../../shared';

export class TandaTerimaDocument implements BaseEntity {
    constructor(
        public id?: number,
        public iddocument?: any,
        public idreceipt?: any,
        public iddetreceipt?: any,
        public doctype?: string,
        public doc?: any,
        public nourut?: number,
    ) {
    }
}
