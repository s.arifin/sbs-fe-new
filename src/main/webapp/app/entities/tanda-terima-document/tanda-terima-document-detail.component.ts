import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { TandaTerimaDocument } from './tanda-terima-document.model';
import { TandaTerimaDocumentService } from './tanda-terima-document.service';

@Component({
    selector: 'jhi-tanda-terima-document-detail',
    templateUrl: './tanda-terima-document-detail.component.html'
})
export class TandaTerimaDocumentDetailComponent implements OnInit, OnDestroy {

    tandaTerimaDocument: TandaTerimaDocument;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private tandaTerimaDocumentService: TandaTerimaDocumentService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTandaTerimaDocuments();
    }

    load(id) {
        this.tandaTerimaDocumentService.find(id).subscribe((tandaTerimaDocument) => {
            this.tandaTerimaDocument = tandaTerimaDocument;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTandaTerimaDocuments() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tandaTerimaDocumentListModification',
            (response) => this.load(this.tandaTerimaDocument.id)
        );
    }
}
