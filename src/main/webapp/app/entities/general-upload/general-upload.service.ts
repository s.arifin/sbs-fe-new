import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { GeneralUpload } from './general-upload.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class GeneralUploadService {
   protected itemValues: GeneralUpload[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  process.env.API_C_URL + 'api/general-uploads';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/general-uploads';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(generalUpload: GeneralUpload): Observable<GeneralUpload> {
       const copy = this.convert(generalUpload);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   createUploadOrganization(generalUpload: GeneralUpload): Observable<GeneralUpload> {
    const copy = this.convert(generalUpload);
    return this.http.post(process.env.API_C_URL + '/api/organization-customers/uploads', copy).map((res: Response) => {
        const jsonResponse = res.json();
        return this.convertItemFromServer(jsonResponse);
    });
}

   createUploadCustomer(generalUpload: GeneralUpload): Observable<GeneralUpload> {
    const copy = this.convert(generalUpload);
    return this.http.post(process.env.API_C_URL + '/api/customer-uploads', copy).map((res: Response) => {
        const jsonResponse = res.json();
        return this.convertItemFromServer(jsonResponse);
    });
}

   update(generalUpload: GeneralUpload): Observable<GeneralUpload> {
       const copy = this.convert(generalUpload);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<GeneralUpload> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(generalUpload: GeneralUpload, id: Number): Observable<GeneralUpload> {
       const copy = this.convert(generalUpload);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertItemFromServer(res.json));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: GeneralUpload, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: GeneralUpload[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to GeneralUpload.
    */
   protected convertItemFromServer(json: any): GeneralUpload {
       const entity: GeneralUpload = Object.assign(new GeneralUpload(), json);
       if (entity.dateCreate) {
           entity.dateCreate = new Date(entity.dateCreate);
       }
       return entity;
   }

   /**
    * Convert a GeneralUpload to a JSON which can be sent to the server.
    */
   protected convert(generalUpload: GeneralUpload): GeneralUpload {
       if (generalUpload === null || generalUpload === {}) {
           return {};
       }
       // const copy: GeneralUpload = Object.assign({}, generalUpload);
       const copy: GeneralUpload = JSON.parse(JSON.stringify(generalUpload));

       // copy.dateCreate = this.dateUtils.toDate(generalUpload.dateCreate);
       return copy;
   }

   protected convertList(generalUploads: GeneralUpload[]): GeneralUpload[] {
       const copy: GeneralUpload[] = generalUploads;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: GeneralUpload[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
