import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { GeneralUpload } from './general-upload.model';
import { GeneralUploadService } from './general-upload.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { PurposeType, PurposeTypeService } from '../purpose-type';
import { ResponseWrapper } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';

@Component({
   selector: 'jhi-general-upload-edit',
   templateUrl: './general-upload-edit.component.html'
})
export class GeneralUploadEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   generalUpload: GeneralUpload;
   isSaving: boolean;
   idGeneralUpload: any;
   paramPage: number;
   routeId: number;

   internals: Internal[];

   purposetypes: PurposeType[];

   constructor(
       protected dataUtils: JhiDataUtils,
       protected alertService: JhiAlertService,
       protected generalUploadService: GeneralUploadService,
       protected internalService: InternalService,
       protected purposeTypeService: PurposeTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService,
       protected confirmationService: ConfirmationService
   ) {
       this.generalUpload = new GeneralUpload();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idGeneralUpload = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.internalService.query(
           {size: 1000}
       )
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.purposeTypeService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.purposetypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.generalUploadService.find(this.idGeneralUpload).subscribe((generalUpload) => {
           this.generalUpload = generalUpload;
       });
   }

   byteSize(field) {
       return this.dataUtils.byteSize(field);
   }

   openFile(contentType, field) {
       return this.dataUtils.openFile(contentType, field);
   }

   setFileData(event, generalUpload, field, isImage) {
       if (event && event.target.files && event.target.files[0]) {
           const file = event.target.files[0];
           if (isImage && !/^image\//.test(file.type)) {
               return;
           }
           this.dataUtils.toBase64(file, (base64Data) => {
               generalUpload[field] = base64Data;
               generalUpload[`${field}ContentType`] = file.type;
           });
       }
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['general-upload', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.generalUpload.idGeneralUpload !== undefined) {
           this.subscribeToSaveResponse(
               this.generalUploadService.update(this.generalUpload));
       } else {
           this.subscribeToSaveResponse(
               this.generalUploadService.create(this.generalUpload));
       }
   }

   protected subscribeToSaveResponse(result: Observable<GeneralUpload>) {
       result.subscribe((res: GeneralUpload) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: GeneralUpload) {
       this.eventManager.broadcast({ name: 'generalUploadListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'generalUpload saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'generalUpload Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }

    trackPurposeTypeById(index: number, item: PurposeType) {
        return item.idPurposeType;
    }

    activated() {
        const idUp =  this.generalUpload.idGeneralUpload;
        const idPurp =  this.generalUpload.purposeId;
        this.generalUploadService.process({id: idUp, idPurposeType: idPurp}).delay(1000).subscribe((r) => {
            this.load();
            this.toaster.showToaster('info', 'Data Process', 'Processed.....');
            this.eventManager.broadcast({
                name: 'generalUploadListModification',
                content: 'Process data....'
             });
            this.previousState();
        });
    }

    approved() {
        this.generalUploadService.changeStatus(this.generalUpload, 12).delay(1000).subscribe((r) => {
            this.load();
            this.toaster.showToaster('info', 'Data Approved', 'Approved.....');
        });
    }

    completed() {
       this.generalUploadService.changeStatus(this.generalUpload, 17).delay(1000).subscribe((r) => {
           this.eventManager.broadcast({
               name: 'generalUploadListModification',
               content: 'Completed an generalUpload'
            });
            this.toaster.showToaster('info', 'Data Completed', 'Completed.....');
            this.previousState();
        });
    }

    canceled() {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to cancel?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.generalUploadService.changeStatus(this.generalUpload, 13).delay(1000).subscribe((r) => {
                   this.eventManager.broadcast({
                       name: 'generalUploadListModification',
                       content: 'Cancel an generalUpload'
                    });
                    this.toaster.showToaster('info', 'Data generalUpload cancel', 'Cancel generalUpload.....');
                    this.previousState();
                });
            }
        });
    }

}
