import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { GeneralUpload } from './general-upload.model';
import { GeneralUploadService } from './general-upload.service';

@Injectable()
export class GeneralUploadPopupService {
    protected ngbModalRef: NgbModalRef;
    idInternal: any;
    idPurpose: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected generalUploadService: GeneralUploadService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.generalUploadService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.generalUploadModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new GeneralUpload();
                    data.internalId = this.idInternal;
                    data.purposeId = this.idPurpose;
                    this.ngbModalRef = this.generalUploadModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    generalUploadModalRef(component: Component, generalUpload: GeneralUpload): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.generalUpload = generalUpload;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPurpose = this.idPurpose;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.generalUploadLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    generalUploadLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPurpose = this.idPurpose;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
