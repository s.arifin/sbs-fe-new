import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { GeneralUploadComponent } from './general-upload.component';
import { GeneralUploadEditComponent } from './general-upload-edit.component';
import { GeneralUploadPopupComponent } from './general-upload-dialog.component';

@Injectable()
export class GeneralUploadResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idGeneralUpload,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const generalUploadRoute: Routes = [
    {
        path: 'general-upload',
        component: GeneralUploadComponent,
        resolve: {
            'pagingParams': GeneralUploadResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const generalUploadPopupRoute: Routes = [
    {
        path: 'general-upload-popup-new-list/:parent',
        component: GeneralUploadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'general-upload-popup-new',
        component: GeneralUploadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'general-upload-new',
        component: GeneralUploadEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'general-upload/:id/edit',
        component: GeneralUploadEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'general-upload/:route/:page/:id/edit',
        component: GeneralUploadEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'general-upload/:id/popup-edit',
        component: GeneralUploadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.generalUpload.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
