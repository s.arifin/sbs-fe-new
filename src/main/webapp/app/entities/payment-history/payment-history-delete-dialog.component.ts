import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentHistory } from './payment-history.model';
import { PaymentHistoryPopupService } from './payment-history-popup.service';
import { PaymentHistoryService } from './payment-history.service';

@Component({
    selector: 'jhi-payment-history-delete-dialog',
    templateUrl: './payment-history-delete-dialog.component.html'
})
export class PaymentHistoryDeleteDialogComponent {

    paymentHistory: PaymentHistory;

    constructor(
        private paymentHistoryService: PaymentHistoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.paymentHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'paymentHistoryListModification',
                content: 'Deleted an paymentHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-payment-history-delete-popup',
    template: ''
})
export class PaymentHistoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentHistoryPopupService: PaymentHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.paymentHistoryPopupService
                .open(PaymentHistoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
