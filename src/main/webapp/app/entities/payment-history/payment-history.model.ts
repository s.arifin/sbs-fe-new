import { BaseEntity } from './../../shared';

export class PaymentHistory implements BaseEntity {
    constructor(
        public id?: any,
        public idpayhist?: any,
        public idpay?: any,
        public totalpay?: number,
        public createdby?: string,
        public dtcreated?: any,
        public description?: string,
        public nogiro?: string,
        public nopayfin?: string,
        public dtrelease?: any,
        public dtpay?: any,
    ) {
    }
}
