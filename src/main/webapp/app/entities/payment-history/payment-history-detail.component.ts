import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentHistory } from './payment-history.model';
import { PaymentHistoryService } from './payment-history.service';

@Component({
    selector: 'jhi-payment-history-detail',
    templateUrl: './payment-history-detail.component.html'
})
export class PaymentHistoryDetailComponent implements OnInit, OnDestroy {

    paymentHistory: PaymentHistory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private paymentHistoryService: PaymentHistoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPaymentHistories();
    }

    load(id) {
        this.paymentHistoryService.find(id).subscribe((paymentHistory) => {
            this.paymentHistory = paymentHistory;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPaymentHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'paymentHistoryListModification',
            (response) => this.load(this.paymentHistory.id)
        );
    }
}
