import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PaymentHistory } from './payment-history.model';
import { PaymentHistoryService } from './payment-history.service';

@Injectable()
export class PaymentHistoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private paymentHistoryService: PaymentHistoryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.paymentHistoryService.find(id).subscribe((paymentHistory) => {
                    paymentHistory.dtcreated = this.datePipe
                        .transform(paymentHistory.dtcreated, 'yyyy-MM-ddTHH:mm:ss');
                    paymentHistory.dtrelease = this.datePipe
                        .transform(paymentHistory.dtrelease, 'yyyy-MM-ddTHH:mm:ss');
                    paymentHistory.dtpay = this.datePipe
                        .transform(paymentHistory.dtpay, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.paymentHistoryModalRef(component, paymentHistory);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.paymentHistoryModalRef(component, new PaymentHistory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    paymentHistoryModalRef(component: Component, paymentHistory: PaymentHistory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.paymentHistory = paymentHistory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
