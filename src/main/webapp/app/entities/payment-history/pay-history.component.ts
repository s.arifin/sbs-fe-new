import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PaymentHistory } from './payment-history.model';
import { PaymentHistoryService } from './payment-history.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterBank, MasterBankService } from '../master-bank';
import { array } from '@amcharts/amcharts4/core';

@Component({
    selector: 'jhi-pay-history',
    templateUrl: './pay-history.component.html'
})
export class PayHistoryComponent implements OnInit, OnDestroy, OnChanges {

    @Input() idpay: any;
    @Input() reload: any;
    @Output()
    fnCallback = new EventEmitter();
    fnReload = new EventEmitter();
    currentAccount: any;
    paymentHistories: PaymentHistory[];
    paymentHistorie: PaymentHistory;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    banks: MasterBank[];

    constructor(
        private paymentHistoryService: PaymentHistoryService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private masterBankService: MasterBankService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.banks = new Array<MasterBank>();
        this.paymentHistorie = new PaymentHistory();
    }
    ngOnChanges() {
        this.loadAll();
    }

    loadAll() {
        this.paymentHistoryService.findByIdPay(this.idpay).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/payment-history'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/payment-history', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/payment-history', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.masterBankService.queryComboBox()
            .subscribe(
                (res: ResponseWrapper) => {
                    this.banks = res.json;
                }
            )
        this.loadAll();
        if (this.idpay) {
            this.loadAll();
        }
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPaymentHistories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PaymentHistory) {
        return item.id;
    }
    registerChangeInPaymentHistories() {
        this.eventSubscriber = this.eventManager.subscribe('paymentHistoryListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.paymentHistories = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    update(rowData: PaymentHistory) {
        this.paymentHistorie = rowData;
        // console.log('cek payment histoey coba => ', rowData);
        this.fnCallback.emit(rowData);
    }
    reloadChild(event) {
        this.fnReload.emit();
        console.log('masuk sini log nya');
    }
}
