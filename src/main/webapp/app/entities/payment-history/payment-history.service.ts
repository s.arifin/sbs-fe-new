import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PaymentHistory } from './payment-history.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentHistoryService {

    private resourceUrl = process.env.API_C_URL + '/api/payment-histories';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/payment-histories';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(paymentHistory: PaymentHistory): Observable<PaymentHistory> {
        const copy = this.convert(paymentHistory);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(paymentHistory: PaymentHistory): Observable<PaymentHistory> {
        const copy = this.convert(paymentHistory);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<PaymentHistory> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtcreated = this.dateUtils
            .convertDateTimeFromServer(entity.dtcreated);
        entity.dtrelease = this.dateUtils
            .convertDateTimeFromServer(entity.dtrelease);
        entity.dtpay = this.dateUtils
            .convertDateTimeFromServer(entity.dtpay);
    }

    private convert(paymentHistory: PaymentHistory): PaymentHistory {
        const copy: PaymentHistory = Object.assign({}, paymentHistory);

        // copy.dtcreated = this.dateUtils.toDate(paymentHistory.dtcreated);

        // copy.dtrelease = this.dateUtils.toDate(paymentHistory.dtrelease);

        // copy.dtpay = this.dateUtils.toDate(paymentHistory.dtpay);
        return copy;
    }

    findByIdPay(idpay: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/get-pay-hist/' + idpay)
            .map((res: Response) => this.convertResponse(res));
    }
}
