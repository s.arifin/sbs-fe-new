import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PaymentHistoryService,
    PaymentHistoryPopupService,
    PaymentHistoryComponent,
    PaymentHistoryDetailComponent,
    PaymentHistoryDialogComponent,
    PaymentHistoryPopupComponent,
    PaymentHistoryDeletePopupComponent,
    PaymentHistoryDeleteDialogComponent,
    paymentHistoryRoute,
    paymentHistoryPopupRoute,
    PaymentHistoryResolvePagingParams,
    PayHistoryComponent,
} from './';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
} from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';

const ENTITY_STATES = [
    ...paymentHistoryRoute,
    ...paymentHistoryPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        CurrencyMaskModule
    ],
    declarations: [
        PaymentHistoryComponent,
        PaymentHistoryDetailComponent,
        PaymentHistoryDialogComponent,
        PaymentHistoryDeleteDialogComponent,
        PaymentHistoryPopupComponent,
        PaymentHistoryDeletePopupComponent,
        PayHistoryComponent,
    ],
    entryComponents: [
        PaymentHistoryComponent,
        PaymentHistoryDialogComponent,
        PaymentHistoryPopupComponent,
        PaymentHistoryDeleteDialogComponent,
        PaymentHistoryDeletePopupComponent,
        PayHistoryComponent,
    ],
    providers: [
        PaymentHistoryService,
        PaymentHistoryPopupService,
        PaymentHistoryResolvePagingParams,
    ],
    exports: [
        PayHistoryComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPaymentHistoryModule {}
