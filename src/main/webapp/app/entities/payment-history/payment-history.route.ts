import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PaymentHistoryComponent } from './payment-history.component';
import { PaymentHistoryDetailComponent } from './payment-history-detail.component';
import { PaymentHistoryPopupComponent } from './payment-history-dialog.component';
import { PaymentHistoryDeletePopupComponent } from './payment-history-delete-dialog.component';

@Injectable()
export class PaymentHistoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const paymentHistoryRoute: Routes = [
    {
        path: 'payment-history',
        component: PaymentHistoryComponent,
        resolve: {
            'pagingParams': PaymentHistoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-history/:id',
        component: PaymentHistoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentHistoryPopupRoute: Routes = [
    {
        path: 'payment-history-new',
        component: PaymentHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-history/:id/edit',
        component: PaymentHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-history/:id/delete',
        component: PaymentHistoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
