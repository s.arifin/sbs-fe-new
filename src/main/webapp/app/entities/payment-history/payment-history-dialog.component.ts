import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentHistory } from './payment-history.model';
import { PaymentHistoryPopupService } from './payment-history-popup.service';
import { PaymentHistoryService } from './payment-history.service';

@Component({
    selector: 'jhi-payment-history-dialog',
    templateUrl: './payment-history-dialog.component.html'
})
export class PaymentHistoryDialogComponent implements OnInit {

    paymentHistory: PaymentHistory;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private paymentHistoryService: PaymentHistoryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentHistoryService.update(this.paymentHistory));
        } else {
            this.subscribeToSaveResponse(
                this.paymentHistoryService.create(this.paymentHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<PaymentHistory>) {
        result.subscribe((res: PaymentHistory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PaymentHistory) {
        this.eventManager.broadcast({ name: 'paymentHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-payment-history-popup',
    template: ''
})
export class PaymentHistoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentHistoryPopupService: PaymentHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentHistoryPopupService
                    .open(PaymentHistoryDialogComponent as Component, params['id']);
            } else {
                this.paymentHistoryPopupService
                    .open(PaymentHistoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
