import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Invoice, Pobbmlov } from './invoice.model';
import { InvoiceService } from './invoice.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterSupplierService } from '../master-supplier/master-supplier.service';
import { MasterSupplier } from '../master-supplier';
import { InvoiceItem } from '../invoice-item';
import { Purchasing, PurchasingService } from '../purchasing';
import { Payment, PaymentService } from '../payment';
import { PaymentItem } from '../payment-item';

@Component({
    selector: 'jhi-invoice-payment-new-downpayment',
    templateUrl: './invoice-payment-new-downpayment.component.html'
})
export class InvoicePaymentNewDownpaymentComponent implements OnInit, OnDestroy {
    @Input() purchasing: Purchasing = new Purchasing();
    @Input() payment: Payment = new Payment();
    @Input() invno: string;
    @Input() taxinvno: string;
    @Input() paytipe: number;
    inv: Invoice;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    invItems: InvoiceItem[];
    invItem: InvoiceItem;
    eventSubscriber: Subscription;
    eventSubscriberPPLOV: Subscription;
    checkPPN: boolean;
    subscription: Subscription;
    isview: boolean;
    dtReceipt: Date;
    filter: InvoiceItem[];
    payItem: PaymentItem;
    payItems: PaymentItem[];
    pay: Payment;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private paymentService: PaymentService,
    ) {
        this.inv = new Invoice();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.invItems = new Array<InvoiceItem>();
        this.invItem = new InvoiceItem;
        this.checkPPN = false;
        this.isview = false;
        this.dtReceipt = new Date();
        this.filter = new Array<InvoiceItem>();
        this.payment = new Payment();
        this.payItem = new PaymentItem();
        this.payItems = new Array<PaymentItem>();
        this.pay = new Payment();
    }
    ngOnInit() {
        console.log('ini data payment tipe ', this.paytipe);
        this.regGetPOBBm(this.purchasing);
        console.log('ini adalah data kiriman payment = ', this.payment);
        if (this.invno) {
            this.inv.invno = this.invno;
            this.dtReceipt = new Date();
            this.inv.receiptno = 'UM PO ' + this.purchasing.nopurchasing;
        }
        if (this.taxinvno) {
            this.inv.taxinvno = this.taxinvno;
        }
        // this.subscription = this.route.params.subscribe((params) => {
        //     if (params['id']) {
        //         if (params['isview'] === 'true') {
        //             this.isview = true;
        //             this.load(params['id']);
        //         }
        //         if (params['isview'] === 'false') {
        //             this.isview = false;
        //             this.load(params['id']);
        //         }
        //     }
        // });
    }
    load(id) {
        this.invoiceService.find(id).subscribe(
            (res: Invoice) => {
                this.inv = res;
                this.invItems = res.invoice_item;
                this.getSuplierData(res.invfrom);
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.isSelectSupplier = true;
    }

    createINV() {
        this.inv.invtype = this.paytipe;
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.inv.dtreceipt = this.dtReceipt;
        console.log('cek data invoice id nya apa = ', this.inv)
        if (this.inv.invno === '' || this.inv.invno === undefined || this.inv.invno === null) {
            alert('Nomor Faktur Tidak Boleh Kosong');
        } else if (this.inv.taxinvno === '' || this.inv.taxinvno === undefined || this.inv.taxinvno === null) {
            alert('Nomor Faktur Pajak Tidak Boleh Kosong');
        } else {
            this.inv.invoice_item = this.invItems;
            if (this.inv.idinv === undefined) {
                this.invoiceService.create(this.inv).subscribe(
                    (res: Invoice) => {
                        this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
                        this.mappingToPayment(res).then(
                            () => {
                                this.createPayment();
                            }
                        );
                        this.backMainPage();
                    }
                )
            } else if (this.inv.idinv !== undefined) {
                this.invoiceService.update(this.inv).subscribe(
                    (res) => {
                        this.backMainPage();
                        this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
                    }
                )
            }
        }
    }

    backMainPage() {
        this.router.navigate(['/invoice']);
    }
    // registerFromPOBBMLOV() {
    //     this.eventSubscriberPPLOV = this.eventManager.subscribe('POLovModification',
    //         () => {
    //             this.regGetPOBBm();
    //         });
    // }
    regGetPOBBm(po: Purchasing) {
        this.inv.total = this.purchasing.uangmuka;
        this.inv.subtotal = this.purchasing.uangmuka;
        this.inv.grandtotal = this.purchasing.uangmuka;
        if (this.inv.valuta !== undefined) {
            if (this.inv.valuta.toLowerCase() !== po.valuta.toLowerCase()) {
                this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                return;
            }
        }
        if (this.inv.valuta === undefined) {
            this.inv.valuta = po.valuta;
        }
        if (this.newSuplier.kd_suppnew === undefined) {
            console.log('get data values po bbm == ', po);
            const _nopo = po.nopurchasing;
            const _qty = 1;
            this.filter = new Array<InvoiceItem>();
            this.filter = this.invItems.filter(
                function dataSama(items) {
                    return (
                        items.nopo === _nopo &&
                        items.qty === _qty)
                }
            );
            if (this.filter.length <= 0) {
                this.invItem = new InvoiceItem();
                this.invItem.nopo = po.nopurchasing;
                this.invItem.qty = _qty;
                this.invItem.price = po.totalprice;
                this.invItem.totalprice = (_qty * po.totalprice);
                this.invItems = [... this.invItems, this.invItem];
            }
            this.getSuplierData(po.supliercode);
        }
        if (this.newSuplier.kd_suppnew !== undefined) {
            if (this.newSuplier.kd_suppnew.toLocaleLowerCase() !== po.supliercode.toLowerCase()) {
                this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
            }
            if (this.newSuplier.kd_suppnew.toLocaleLowerCase() === po.supliercode.toLowerCase()) {
                console.log('get data values po bbm == ', po);
                const _nopo = po.nopurchasing;
                const _qty = 1;
                this.filter = new Array<InvoiceItem>();
                this.filter = this.invItems.filter(
                    function dataSama(items) {
                        return (
                            items.nopo === _nopo &&
                            items.qty === _qty)
                    }
                );
                if (this.filter.length <= 0) {
                    this.invItem = new InvoiceItem();
                    this.invItem.nopo = po.nopurchasing;
                    this.invItem.qty = _qty;
                    this.invItem.price = po.totalprice;
                    this.invItem.totalprice = (_qty * po.totalprice);
                    this.invItems = [... this.invItems, this.invItem];
                }
                this.getSuplierData(po.supliercode);
            }
        }
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
            }
        )
    }
    mappingToPayment(inv: Invoice): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.pay = this.payment;
                if (this.newSuplier.kd_suppnew === undefined) {
                    const _idinv = inv.idinv;
                    const _invno = inv.invno;
                    const _grandtotal = inv.grandtotal;
                    const _invfrom = inv.invfrom;
                    const _taxinvno = inv.taxinvno;
                    this.filter = this.payItems.filter(
                        function dataSama(items) {
                            return (
                                items.idinvoice === _idinv &&
                                items.invno === _invno &&
                                items.totalpay === _grandtotal &&
                                items.invfrom === _invfrom &&
                                items.invtaxno === _taxinvno
                            )
                        }
                    );
                    if (this.filter.length <= 0) {
                        this.payItem = new PaymentItem();
                        this.payItem.idinvoice = inv.idinv;
                        this.payItem.invno = inv.invno;
                        this.payItem.totalpay = inv.grandtotal;
                        this.payItem.invfrom = inv.invfrom;
                        this.payItem.invtaxno = inv.taxinvno;
                        this.payItems = [... this.payItems, this.payItem];
                    }
                    // this.countPrice().then(() => this.countPPH());
                    // this.getSuplierData(res[0].invfrom);
                }
                if (this.newSuplier.kd_suppnew !== undefined) {
                    if (this.newSuplier.kd_suppnew.toLowerCase() === inv.invfrom.toLowerCase()) {
                        console.log('get data values INVOICE == ', inv);
                        const _idinv = inv.idinv;
                        const _invno = inv.invno;
                        const _grandtotal = inv.grandtotal;
                        const _invfrom = inv.invfrom;
                        const _taxinvno = inv.taxinvno;
                        this.filter = this.payItems.filter(
                            function dataSama(items) {
                                return (
                                    items.idinvoice === _idinv &&
                                    items.invno === _invno &&
                                    items.totalpay === _grandtotal &&
                                    items.invfrom === _invfrom &&
                                    items.invtaxno === _taxinvno
                                )
                            }
                        );
                        if (this.filter.length <= 0) {
                            this.payItem = new PaymentItem();
                            this.payItem.idinvoice = inv.idinv;
                            this.payItem.invno = inv.invno;
                            this.payItem.totalpay = inv.grandtotal;
                            this.payItem.invfrom = inv.invfrom;
                            this.payItem.invtaxno = inv.taxinvno;
                            this.payItems = [... this.payItems, this.payItem];
                        }
                        // this.countPrice().then(() => this.countPPH());
                        // this.getSuplierData(res[0].invfrom);
                    }
                    // if (this.newSuplier.kd_suppnew.toLowerCase() !== res[0].invfrom.toLowerCase()) {
                    //     this.toasterService.showToaster('Info', 'Info', 'Supplier Tidak Boleh Berbeda')
                    // }
                }
                resolvedt();
            }
        )
    }
    createPayment() {
        this.pay.payment_items = this.payItems;
        this.pay.paytype = this.paytipe;
        this.paymentService.create(this.pay).subscribe(
            (res: Payment) => {
                console.log('ini masuk hasil create payment');
                this.paymentService.setStatus(this.payment, 16).subscribe(
                    () => {
                        console.log('ini masuk hasil create payment status');
                    }
                );
            }
        )
    }

}
