import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Invoice, Pobbmlov } from './invoice.model';
import { InvoiceService } from './invoice.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { MasterSupplierService } from '../master-supplier/master-supplier.service';
import { MasterSupplier } from '../master-supplier';
import { InvoiceItem, InvoiceItemService } from '../invoice-item';
import { Purchasing, PurchasingService } from '../purchasing';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts';
import { TandaTerimaService } from '../tanda-terima';
import { DetailTandaTerima } from '../detail-tanda-terima';
import { FileAttachment, Listpaypo, PaymentService } from '../payment';

@Component({
    selector: 'jhi-invoice-new-ppjk',
    templateUrl: './invoice-new-ppjk.component.html'
})
export class InvoiceNewPpjkComponent implements OnInit, OnDestroy {
    [x: string]: any;
    inv: Invoice;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    invItems: InvoiceItem[];
    invItem: InvoiceItem;
    eventSubscriber: Subscription;
    eventSubscriberPPLOV: Subscription;
    checkPPN: boolean;
    subscription: Subscription;
    isview: boolean;
    dtReceipt: Date;
    filter: InvoiceItem[];
    togleCheck: boolean;
    nomppn: boolean;
    pembulatan: boolean;
    jnspembulatan: boolean;
    lblpembualatan: string;
    lbljnspembulatan: string;
    listpaypo: Listpaypo[];
    newPPN: number;
    oldPPN: number;
    poPPN: number;
    saveBool: boolean;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        private tandaTerimaService: TandaTerimaService,
        private invoiceItemService: InvoiceItemService,
        private paymentService: PaymentService,
    ) {
        this.inv = new Invoice();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.invItems = new Array<InvoiceItem>();
        this.invItem = new InvoiceItem;
        this.checkPPN = false;
        this.isview = false;
        this.dtReceipt = new Date();
        this.filter = new Array<InvoiceItem>();
        this.togleCheck = false;
        this.nomppn = false;
        this.pembulatan = false;
        this.lblpembualatan = 'Tidak';
        this.jnspembulatan = false;
        this.lbljnspembulatan = 'Bawah';
        this.listpaypo = new Array<Listpaypo>();
        this.newPPN = 0;
        this.oldPPN = 0;
        this.poPPN = 0;
        this.saveBool = false;
    }
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.saveBool = true;
                    this.load(params['id']);
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.saveBool = false;
                    this.load(params['id']);
                }
            }
        });
        this.registerFromPOBBMLOV();
        this.registerTandaTerima();
        this.purchasingService.getPPN({ // OTORISASI KE 1
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc'],
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                // this.arrayPPN.push({
                //     label: element.createdBy,
                //     value: parseFloat(element.createdBy)
                // });
                this.newPPN = parseFloat(element.createdBy);
                this.oldPPN = parseFloat(element.approvedBy);
            });

        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );
    }
    load(id) {
        this.invoiceService.find(id).subscribe(
            (res: Invoice) => {
                if (res.statuses !== 10 && this.isview === false) {
                    this.backMainPage();
                } else {
                    this.inv = res;
                    this.invItems = res.invoice_item;
                    if (this.inv.invtype === 5) {
                        this.togleCheck = true;
                    }
                    if (this.inv.invtype === 4) {
                        this.togleCheck = false;
                    }
                    this.inv.addcost = res.addcost;
                    this.inv.addcostnote = res.addcostnote;
                    this.invItems.forEach((e) => {
                        this.paymentService.FindPOBYNo1({ query: 'nopo:' + e.nopo + '|type_inv:' + 4 }).subscribe((resfind: ResponseWrapper) => {
                            resfind.json.forEach((e1: Listpaypo) => {
                                this.listpaypo = [... this.listpaypo, e1];
                            });
                        })
                    });

                    this.getSuplierData(res.invfrom);
                }
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.isSelectSupplier = true;
    }

    createINV() {
        // if this.togleCheck true PPJK
        // if this.togleCheck false kas negara
        this.saveBool = true;
        if (this.togleCheck === true) {
            this.inv.invtype = 5;
        }
        if (this.togleCheck === false) {
            this.inv.invtype = 4;
        }
        this.inv.total = this.inv.subtotal;
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.inv.dtreceipt = this.dtReceipt;
        console.log('cek data invoice id nya apa = ', this.inv)
        if (this.inv.invno === '' || this.inv.invno === undefined || this.inv.invno === null) {
            alert('Nomor Faktur Tidak Boleh Kosong');
            this.saveBool = false;
        } else if (this.inv.taxinvno === '' || this.inv.taxinvno === undefined || this.inv.taxinvno === null) {
            alert('Nomor Faktur Pajak Tidak Boleh Kosong');
            this.saveBool = false;
        } else if (this.invItems.length < 1) {
            alert('Nomor PO Harus Dipilih');
            this.saveBool = false;
        } else {
            this.inv.invoice_item = this.invItems;
            if (this.inv.idinv === undefined) {
                this.invoiceService.create(this.inv).subscribe(
                    (res) => {
                        this.saveBool = false;
                        this.backMainPage();
                    }
                )
            } else if (this.inv.idinv !== undefined) {
                this.invoiceService.update(this.inv).subscribe(
                    (res) => {
                        this.saveBool = false;
                        this.backMainPage();
                    }
                )
            }
        }

    }

    backMainPage() {
        this.router.navigate(['/invoice']);
    }
    registerFromPOBBMLOV() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('POLovModification',
            () => {
                this.regGetPOBBm();
            });
    }
    regGetPOBBm(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.purchasingService.values.subscribe(
                    (res: Purchasing[]) => {
                        // if (this.inv.valuta !== undefined) {
                        //     if (this.inv.valuta.toLowerCase() !== res[0].valuta.toLowerCase()) {
                        //         this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                        //         return;
                        //     }
                        // }
                        if (this.inv.valuta === undefined) {
                            this.inv.valuta = 'Rp';
                        }
                        if (this.newSuplier.kd_suppnew === undefined) {
                            console.log('get data values po bbm == ', res);
                            res.forEach((e) => {
                                const _nopo = e.nopurchasing;
                                const _qty = 1;
                                this.filter = new Array<InvoiceItem>();
                                this.filter = this.invItems.filter(
                                    function dataSama(items) {
                                        return (
                                            items.nopo === _nopo &&
                                            items.qty === _qty)
                                    }
                                );
                                if (this.filter.length <= 0) {
                                    this.invItem = new InvoiceItem();
                                    this.invItem.nopo = e.nopurchasing;
                                    this.invItem.qty = _qty;
                                    this.invItem.price = e.price;
                                    this.invItem.totaldisc = e.totaldisc;
                                    this.invItem.totalppn = e.ppn;
                                    this.invItem.totalprice = (_qty * e.totalprice);
                                    this.invItems = [... this.invItems, this.invItem];
                                }
                            });
                            this.getSuplierData(res[0].supliercode);
                            this.poPPN = res[0].nomppn === null ? 0 : res[0].nomppn;
                        }
                        if (this.newSuplier.kd_suppnew !== undefined) {
                            if (this.newSuplier.kd_suppnew.toLocaleLowerCase() !== res[0].supliercode.toLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
                            }
                                console.log('get data values po bbm == ', res);
                                res.forEach((e) => {
                                    const _nopo = e.nopurchasing;
                                    const _qty = 1;
                                    this.filter = new Array<InvoiceItem>();
                                    this.filter = this.invItems.filter(
                                        function dataSama(items) {
                                            return (
                                                items.nopo === _nopo &&
                                                items.qty === _qty)
                                        }
                                    );
                                    if (this.filter.length <= 0) {
                                        this.invItem = new InvoiceItem();
                                        this.invItem.nopo = e.nopurchasing;
                                        this.invItem.qty = _qty;
                                        this.invItem.price = e.price;
                                        this.invItem.totaldisc = e.totaldisc;
                                        this.invItem.totalppn = e.ppn;
                                        this.invItem.totalprice = (_qty * e.totalprice);
                                        this.invItems = [... this.invItems, this.invItem];
                                    }
                                });
                                this.poPPN = res[0].nomppn === null ? 0 : res[0].nomppn;
                                // this.getSuplierData(res[0].supliercode);
                        }
                    }
                );
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }
    // countPrice(data: Pobbmlov) {
    //     console.log('cek data masuk bhitung == ');
    //     this.inv.total = 0;
    //     if (this.inv.disc === undefined || this.inv.disc === null) {
    //         this.inv.disc = 0;
    //     }
    //     this.inv.subtotal = 0;
    //     this.inv.ppn = 0;
    //     this.inv.grandtotal = 0;
    //     this.invItems.forEach(
    //         (e) => {
    //             this.inv.total += (e.qty * e.price)
    //         });
    //     this.inv.subtotal = this.inv.total - this.inv.disc;
    //     if (data !== null ) {
    //         if (data.isppn === 1) {
    //             this.checkPPN = true;
    //             this.inv.ppn = this.inv.subtotal * (10 / 100);
    //             this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.disc;
    //         }
    //         if (data.isppn !== 1) {
    //             this.checkPPN = false;
    //             this.inv.ppn = 0;
    //             this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.disc;
    //         }
    //     }
    //     if (data === null) {
    //         if (this.checkPPN === true) {
    //             this.inv.ppn = this.inv.subtotal * (10 / 100);
    //             this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.disc;
    //         }
    //         if (this.checkPPN === false) {
    //             this.inv.ppn = 0;
    //             this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.disc;
    //         }
    //     }
    // }

    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
            }
        )
    }
    deleteItem(invItem: InvoiceItem) {
        if (this.isview === true) {
            alert('Anda Tidak Bisa Menghapus Pada Mode View');
        } else {
            this.confirmationService.confirm({
                header: 'Information',
                message: 'Apakah Yakin Hapus Data ??!!',
                accept: () => {
                    if (invItem.idinv !== null && invItem.idinv !== '' && invItem.idinv !== undefined) {
                        this.invoiceItemService.deletebyid(invItem).subscribe((e) => { console.log(); });
                        this.invItems = this.invItems.filter(function dataSama(items) { return (items.idinvdet !== invItem.idinvdet) });
                    } else {
                        this.invItems = this.invItems.filter(function dataSama(items) { return (items.nopo !== invItem.nopo) });
                    }
                }
            });
        }
    }
    handleChange(e) {
        const isChecked = e.checked;
        console.log('cek data togle cek = ', e.checked);
        // if this.togleCheck true PPJK
        // if this.togleCheck false kas negara
        if (e.checked === true) {
            this.togleCheck = true;
        }
        if (e.checked === false) {
            this.togleCheck = false;
        }
    }

    registerTandaTerima() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('TandaTerimaModified',
            () => {
                this.regTT();
            });
    }
    regTT(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.tandaTerimaService.values.subscribe(
                    (res: DetailTandaTerima) => {
                        console.log('Cek data detail Tanda Terima == ', res);
                        this.inv.receiptno = res.receiptno;
                        this.inv.invno = res.invno;
                        this.inv.taxinvno = res.invtaxno;
                        this.inv.iddetreceipt = res.iddetreceipt;
                        this.dtReceipt = new Date(res.dtreceipt);
                        this.masterSupplierService.findByCode(res.kdsupplier).subscribe(
                            (resSPL: MasterSupplier) => {
                                this.newSuplier = resSPL;
                                this.inv.invfrom = resSPL.kd_suppnew;
                            }
                        )
                    }
                );
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }

    countppn() {
        if (this.inv !== null && this.inv !== undefined) {
            if (this.nomppn === true) {
                if (this.inv.addcost === undefined || this.inv.addcost === null) {
                    this.inv.addcost = 0;
                }
                if (this.pembulatan === false) {
                    this.jnspembulatan = false
                    this.lblpembualatan = 'Tidak';
                }
                if (this.pembulatan === true) {
                    this.lblpembualatan = 'Ya';
                }
                if (this.jnspembulatan === false) {
                    this.lbljnspembulatan = 'Bawah';
                }
                if (this.jnspembulatan === true) {
                    this.lbljnspembulatan = 'Atas';
                }
                if (this.pembulatan === false) {
                    this.inv.ppn = (this.inv.subtotal * (this.poPPN / 100));
                    this.inv.grandtotal = (this.inv.subtotal * (this.poPPN / 100)) + this.inv.subtotal + this.inv.addcost;
                    this.inv.total = (this.inv.subtotal * (this.poPPN / 100)) + this.inv.subtotal + this.inv.addcost;
                }
                if (this.pembulatan === true) {
                    if (this.jnspembulatan === false) {
                        this.inv.ppn = (this.inv.subtotal * (this.poPPN / 100));
                        this.inv.grandtotal = Math.floor((this.inv.subtotal * (this.poPPN / 100)) + this.inv.subtotal + this.inv.addcost);
                        this.inv.total = Math.floor((this.inv.subtotal * (this.poPPN / 100)) + this.inv.subtotal + this.inv.addcost);
                    }
                    if (this.jnspembulatan === true) {
                        this.inv.ppn = (this.inv.subtotal * (this.poPPN / 100));
                        this.inv.grandtotal = Math.ceil((this.inv.subtotal * (this.poPPN / 100)) + this.inv.subtotal + this.inv.addcost);
                        this.inv.total = Math.ceil((this.inv.subtotal * (this.poPPN / 100)) + this.inv.subtotal + this.inv.addcost);
                    }
                }
            } else if (this.nomppn === false) {
                if (this.inv.addcost === undefined || this.inv.addcost === null) {
                    this.inv.addcost = 0;
                }
                if (this.pembulatan === false) {
                    this.jnspembulatan = false
                    this.lblpembualatan = 'Tidak';
                }
                if (this.pembulatan === true) {
                    this.lblpembualatan = 'Ya';
                }
                if (this.jnspembulatan === false) {
                    this.lbljnspembulatan = 'Bawah';
                }
                if (this.jnspembulatan === true) {
                    this.lbljnspembulatan = 'Atas';
                }

                if (this.pembulatan === false) {
                    this.inv.ppn = 0;
                    this.inv.grandtotal = 0 + this.inv.subtotal + this.inv.addcost;
                    this.inv.total = 0 + this.inv.subtotal + this.inv.addcost;
                }
                if (this.pembulatan === true) {
                    if (this.jnspembulatan === false) {
                        this.inv.ppn = 0;
                        this.inv.grandtotal = Math.floor(0 + this.inv.subtotal + this.inv.addcost);
                        this.inv.total = Math.floor(0 + this.inv.subtotal + this.inv.addcost);
                    }
                    if (this.jnspembulatan === true) {
                        this.inv.ppn = 0;
                        this.inv.grandtotal = Math.ceil(0 + this.inv.subtotal + this.inv.addcost);
                        this.inv.total = Math.ceil(0 + this.inv.subtotal + this.inv.addcost);
                    }
                }

            }
        }
        console.log(this.inv.subtotal);
    }

}
