import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Invoice, Pobbmlov } from './invoice.model';
import { InvoiceService } from './invoice.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { MasterSupplierService } from '../master-supplier/master-supplier.service';
import { MasterSupplier } from '../master-supplier';
import { InvoiceItem } from '../invoice-item';
import { Purchasing, PurchasingService } from '../purchasing';
import { Payment, PaymentService } from '../payment';
import { PaymentItem } from '../payment-item';
import { TandaTerimaService } from '../tanda-terima';
import { ConfirmationService } from 'primeng/primeng';
import { DetailTandaTerima } from '../detail-tanda-terima';

@Component({
    selector: 'jhi-invoice-um-po',
    templateUrl: './invoice-um-po.component.html'
})
export class InvoiceUmPoComponent implements OnInit, OnDestroy {
    @Input() purchasing: Purchasing = new Purchasing();
    @Input() payment: Payment = new Payment();
    @Input() invno: string;
    @Input() taxinvno: string;
    @Input() paytipe: number;
    @Input() nopo: string;
    @Input() idpay: any;
    @Input() payment3: Payment = new Payment();
    inv: Invoice;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    invItems: InvoiceItem[];
    invItem: InvoiceItem;
    eventSubscriber: Subscription;
    eventSubscriberPPLOV: Subscription;
    checkPPN: boolean;
    subscription: Subscription;
    isview: boolean;
    dtReceipt: Date;
    filter: InvoiceItem[];
    payItem: PaymentItem;
    payItems: PaymentItem[];
    pay: Payment;
    pay2: Payment;
    penyelesaian: any
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private paymentService: PaymentService,
        private tandaTerimaService: TandaTerimaService,
        private confirmationService: ConfirmationService,
    ) {
        this.inv = new Invoice();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.invItems = new Array<InvoiceItem>();
        this.invItem = new InvoiceItem;
        this.checkPPN = false;
        this.isview = false;
        this.dtReceipt = new Date();
        this.filter = new Array<InvoiceItem>();
        this.payment = new Payment();
        this.payItem = new PaymentItem();
        this.payItems = new Array<PaymentItem>();
        this.pay = new Payment();
        this.pay2 = new Payment();
        this.penyelesaian = false;
    }
    ngOnInit() {
        this.regGetPOBBm(this.purchasing);
        if (this.invno) {
            this.inv.invno = this.invno;
            this.dtReceipt = new Date();
            this.inv.receiptno = 'UM PO ' + this.purchasing.nopurchasing;
            if (this.nopo) {
                this.paymentService.queryByPurchasing({
                    page: 0,
                    size: 20,
                    query: 'nopo:' + this.nopo
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.pay2 = res.json[0];
                    }
                );
            }
        }
        if (this.taxinvno) {
            this.inv.taxinvno = this.taxinvno;
        }
        this.registerTandaTerima();
        // this.subscription = this.route.params.subscribe((params) => {
        //     if (params['id']) {
        //         if (params['isview'] === 'true') {
        //             this.isview = true;
        //             this.load(params['id']);
        //         }
        //         if (params['isview'] === 'false') {
        //             this.isview = false;
        //             this.load(params['id']);
        //         }
        //     }
        // });
    }
    load(id) {
        this.invoiceService.find(id).subscribe(
            (res: Invoice) => {
                this.inv = res;
                this.invItems = res.invoice_item;
                this.getSuplierData(res.invfrom);
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.isSelectSupplier = true;
    }

    createINV() {
        this.inv.invtype = this.paytipe;
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.inv.dtreceipt = this.dtReceipt;
        if (this.inv.invno === '' || this.inv.invno === undefined || this.inv.invno === null) {
            alert('Nomor Faktur Tidak Boleh Kosong');
        } else if (this.inv.taxinvno === '' || this.inv.taxinvno === undefined || this.inv.taxinvno === null) {
            alert('Nomor Faktur Pajak Tidak Boleh Kosong');
        } else {
            // this.inv.invoice_item = this.invItems;
            // if (this.inv.idinv === undefined) {
            //     this.invoiceService.create(this.inv).subscribe(
            //         (res: Invoice) => {
            //             this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
            //             this.mappingToPayment(res).then(
            //                 () => {
            //                     this.createPayment();
            //                 }
            //             );
            //             this.backMainPage();
            //         }
            //     )
            // } else if (this.inv.idinv !== undefined) {
            //     this.invoiceService.update(this.inv).subscribe(
            //         (res) => {
            //             this.backMainPage();
            //             this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
            //         }
            //     )
            // }
        }
        if (this.nopo && this.pay2 && this.paytipe === 3) {
            this.pay2.receiptno = this.inv.receiptno;
            const nott = this.inv.receiptno;
            const um = this.inv.total;
            const nopo = this.nopo;
            const idpay = this.pay2.idpay;
            const invno = this.inv.invno;
            const taxinvno = this.inv.taxinvno;
            this.invoiceService.updateumtopayment(um, nopo).subscribe(
                (ress) => {
                    this.paymentService.setnott(nott, idpay, invno, taxinvno)
                        .subscribe(
                            (res) => {
                                this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
                            }
                        );
                }
            );
        }

        if (this.nopo && this.pay2 && this.paytipe === 2) {
            this.pay2.receiptno = this.inv.receiptno;
            const nott = this.inv.receiptno;
            const um = this.inv.total;
            const nopo = this.nopo;
            const idpay = this.pay2.idpay;
            const invno = this.inv.invno;
            const taxinvno = this.inv.taxinvno;
            this.invoiceService.updateumtodp(um, nopo).subscribe(
                (ress) => {
                    this.paymentService.setnott(nott, idpay, invno, taxinvno)
                        .subscribe(
                            (res) => {
                                this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
                            }
                        );
                }
            );
        }
    }
    createINV2() {
        this.inv.invtype = 2;
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.inv.dtreceipt = this.dtReceipt;
        this.inv.idpay = this.idpay;
        this.inv.paytype2 = 2;
        if (this.inv.invno === '' || this.inv.invno === undefined || this.inv.invno === null) {
            alert('Nomor Faktur Tidak Boleh Kosong');
        } else if (this.inv.taxinvno === '' || this.inv.taxinvno === undefined || this.inv.taxinvno === null) {
            alert('Nomor Faktur Pajak Tidak Boleh Kosong');
        } else {
            this.inv.invoice_item = this.invItems;
            if (this.inv.idinv === undefined) {
                this.invoiceService.findinv(this.inv).subscribe((resfind) => {
                    if (resfind.code === 200 && resfind.messages === 'Data Ada') {
                        alert('Data Invoice untuk suplier ini sudah ada !!');
                        return;
                    } else {
                        this.invoiceService.create(this.inv).subscribe(
                            (res) => {
                                // this.paymentService.setnott(this.inv.receiptno, this.payment3.idpay, this.inv.invno, this.inv.taxinvno)
                                //     .subscribe(
                                //         (res) => {
                                //             this.eventManager.broadcast({ name: 'invoiceDPPaymentModification', content: 'OK' });
                                //         }
                                //     );
                                this.backMainPage();
                            }
                        )
                    }
                });
            } else if (this.inv.idinv !== undefined) {
                this.invoiceService.update(this.inv).subscribe(
                    (res) => {
                        this.backMainPage();
                    }
                )
            }
            this.backMainPage();
        }
    }

    backMainPage() {
        this.router.navigate(['/purchasing-pay']);
    }
    // registerFromPOBBMLOV() {
    //     this.eventSubscriberPPLOV = this.eventManager.subscribe('POLovModification',
    //         () => {
    //             this.regGetPOBBm();
    //         });
    // }
    regGetPOBBm(po: Purchasing) {
        if (this.idpay) {
            this.paymentService.find(this.idpay).subscribe((payment) => {
                this.payment3 = payment;
                this.inv.total = this.payment3.subtotal;
                this.inv.subtotal = this.payment3.subtotal;
                this.inv.grandtotal = this.payment3.subtotal;
                console.log('ini data paymet2 masuk ngga => ', this.payment3);
            });
        }
        if (this.inv.valuta !== undefined) {
            if (this.inv.valuta.toLowerCase() !== po.valuta.toLowerCase()) {
                this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                return;
            }
        }
        if (this.inv.valuta === undefined) {
            this.inv.valuta = po.valuta;
        }
        if (this.newSuplier.kd_suppnew === undefined) {
            const _nopo = po.nopurchasing;
            const _qty = 1;
            this.filter = new Array<InvoiceItem>();
            this.filter = this.invItems.filter(
                function dataSama(items) {
                    return (
                        items.nopo === _nopo &&
                        items.qty === _qty)
                }
            );
            if (this.filter.length <= 0) {
                this.invItem = new InvoiceItem();
                this.invItem.nopo = po.nopurchasing;
                this.invItem.qty = _qty;
                this.invItem.price = po.totalprice;
                this.invItem.totalprice = (_qty * po.totalprice);
                this.invItems = [... this.invItems, this.invItem];
            }
            this.getSuplierData(po.supliercode);
        }
        if (this.newSuplier.kd_suppnew !== undefined) {
            if (this.newSuplier.kd_suppnew.toLocaleLowerCase() !== po.supliercode.toLowerCase()) {
                this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
            }
            if (this.newSuplier.kd_suppnew.toLocaleLowerCase() === po.supliercode.toLowerCase()) {
                const _nopo = po.nopurchasing;
                const _qty = 1;
                this.filter = new Array<InvoiceItem>();
                this.filter = this.invItems.filter(
                    function dataSama(items) {
                        return (
                            items.nopo === _nopo &&
                            items.qty === _qty)
                    }
                );
                if (this.filter.length <= 0) {
                    this.invItem = new InvoiceItem();
                    this.invItem.nopo = po.nopurchasing;
                    this.invItem.qty = _qty;
                    this.invItem.price = po.totalprice;
                    this.invItem.totalprice = (_qty * po.totalprice);
                    this.invItems = [... this.invItems, this.invItem];
                }
                this.getSuplierData(po.supliercode);
            }
        }
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
            }
        )
    }
    mappingToPayment(inv: Invoice): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.pay = this.payment;
                if (this.newSuplier.kd_suppnew === undefined) {
                    const _idinv = inv.idinv;
                    const _invno = inv.invno;
                    const _grandtotal = inv.grandtotal;
                    const _invfrom = inv.invfrom;
                    const _taxinvno = inv.taxinvno;
                    this.filter = this.payItems.filter(
                        function dataSama(items) {
                            return (
                                items.idinvoice === _idinv &&
                                items.invno === _invno &&
                                items.totalpay === _grandtotal &&
                                items.invfrom === _invfrom &&
                                items.invtaxno === _taxinvno
                            )
                        }
                    );
                    if (this.filter.length <= 0) {
                        this.payItem = new PaymentItem();
                        this.payItem.idinvoice = inv.idinv;
                        this.payItem.invno = inv.invno;
                        this.payItem.totalpay = inv.grandtotal;
                        this.payItem.invfrom = inv.invfrom;
                        this.payItem.invtaxno = inv.taxinvno;
                        this.payItems = [... this.payItems, this.payItem];
                    }
                    // this.countPrice().then(() => this.countPPH());
                    // this.getSuplierData(res[0].invfrom);
                }
                if (this.newSuplier.kd_suppnew !== undefined) {
                    if (this.newSuplier.kd_suppnew.toLowerCase() === inv.invfrom.toLowerCase()) {
                        const _idinv = inv.idinv;
                        const _invno = inv.invno;
                        const _grandtotal = inv.grandtotal;
                        const _invfrom = inv.invfrom;
                        const _taxinvno = inv.taxinvno;
                        this.filter = this.payItems.filter(
                            function dataSama(items) {
                                return (
                                    items.idinvoice === _idinv &&
                                    items.invno === _invno &&
                                    items.totalpay === _grandtotal &&
                                    items.invfrom === _invfrom &&
                                    items.invtaxno === _taxinvno
                                )
                            }
                        );
                        if (this.filter.length <= 0) {
                            this.payItem = new PaymentItem();
                            this.payItem.idinvoice = inv.idinv;
                            this.payItem.invno = inv.invno;
                            this.payItem.totalpay = inv.grandtotal;
                            this.payItem.invfrom = inv.invfrom;
                            this.payItem.invtaxno = inv.taxinvno;
                            this.payItems = [... this.payItems, this.payItem];
                        }
                        // this.countPrice().then(() => this.countPPH());
                        // this.getSuplierData(res[0].invfrom);
                    }
                    // if (this.newSuplier.kd_suppnew.toLowerCase() !== res[0].invfrom.toLowerCase()) {
                    //     this.toasterService.showToaster('Info', 'Info', 'Supplier Tidak Boleh Berbeda')
                    // }
                }
                resolvedt();
            }
        )
    }
    createPayment() {
        this.pay.payment_items = this.payItems;
        this.pay.paytype = this.paytipe;
        this.paymentService.create(this.pay).subscribe(
            (res: Payment) => {
                this.paymentService.setStatus(this.payment, 16).subscribe(
                    () => {
                        console.log('ini masuk hasil create payment status');
                    }
                );
            }
        )
    }
    ispenyelesaian() {
        console.log('ini penyelesaian');
        console.log(this.penyelesaian);
    }

    regTT(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.tandaTerimaService.values.subscribe(
                    (res: DetailTandaTerima) => {
                        if (res.kdsupplier !== this.newSuplier.kd_suppnew) {
                            alert('Tidak Boleh Berbeda Supplier');
                        } else {
                            this.inv.receiptno = res.receiptno;
                            this.inv.invno = res.invno;
                            this.inv.taxinvno = res.invtaxno;
                            this.inv.iddetreceipt = res.iddetreceipt;
                            this.dtReceipt = new Date(res.dtreceipt);
                            //     this.masterSupplierService.findByCode(res.kdsupplier).subscribe(
                            //         (resSPL: MasterSupplier) => {
                            //             this.newSuplier = resSPL;
                            //             this.inv.invfrom = resSPL.kd_suppnew;
                            //         }
                            // )
                        }
                        console.log('Cek data detail Tanda Terima == ', res);
                        console.log('Cek supplier == ', this.newSuplier.kd_suppnew);
                    }
                );
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }

    registerTandaTerima() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('TandaTerimaModified',
            () => {
                this.regTT();
            });
    }

}
