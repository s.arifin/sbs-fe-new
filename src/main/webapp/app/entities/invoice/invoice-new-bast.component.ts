import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Invoice, Pobbmlov } from './invoice.model';
import { InvoiceService } from './invoice.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { MasterSupplierService } from '../master-supplier/master-supplier.service';
import { MasterSupplier } from '../master-supplier';
import { InvoiceItem, InvoiceItemService } from '../invoice-item';
import { ConfirmationService, TRISTATECHECKBOX_VALUE_ACCESSOR } from 'primeng/primeng';
import { LoadingService } from '../../layouts';
import { TandaTerimaService } from '../tanda-terima';
import { DetailTandaTerima } from '../detail-tanda-terima';
import { getuid } from 'process';
import { GuidGenerator } from './GuidGenerator';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { FileAttachment, PaymentService } from '../payment';
import * as FileSaver from 'file-saver';
import { ReportService } from '../report';
import { BbmPrint } from '../report/report.model';
import { PurchasingService } from '../purchasing/purchasing.service';
import { Purchasing } from '../purchasing/purchasing.model';

@Component({
    selector: 'jhi-invoice-new-bast',
    templateUrl: './invoice-new-bast.component.html'
})
export class InvoiceNewBastComponent implements OnInit, OnDestroy {
    @Input() idpay: any;
    inv: Invoice;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    isSelectSupplier: boolean;
    newSuplier: MasterSupplier;
    invItems: InvoiceItem[];
    invItem: InvoiceItem;
    eventSubscriber: Subscription;
    eventSubscriberPPLOV: Subscription;
    checkPPN: boolean;
    subscription: Subscription;
    isview: boolean;
    dtReceipt: Date;
    filter: InvoiceItem[];
    purchasings: Purchasing[];
    isprop: boolean;
    pembulatan: boolean;
    jnspembulatan: boolean;
    lblpembualatan: string;
    lbljnspembulatan: string;
    isviewpdf: boolean;
    pdfBlob: string;
    pdfSrc: any;
    invbbm: InvoiceItem;
    div: string;
    nomppn: boolean;
    isppn: number;
    totalppn: number;
    constructor(
        private masterSupplierService: MasterSupplierService,
        private eventManager: JhiEventManager,
        private invoiceService: InvoiceService,
        private router: Router,
        private route: ActivatedRoute,
        private purchasingService: PurchasingService,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private tandaTerimaService: TandaTerimaService,
        private guidGenerator: GuidGenerator,
        private reportUtilService: ReportUtilService,
        private paymentService: PaymentService,
        private reportService: ReportService,
        private invoiceItemService: InvoiceItemService
    ) {
        this.inv = new Invoice();
        this.isSelectSupplier = false;
        this.newSuplier = new MasterSupplier();
        this.invItems = new Array<InvoiceItem>();
        this.invItem = new InvoiceItem;
        this.checkPPN = false;
        this.isview = false;
        this.dtReceipt = new Date();
        this.filter = new Array<InvoiceItem>();
        this.purchasings = new Array<Purchasing>();
        this.isprop = false;
        this.pembulatan = false;
        this.lblpembualatan = 'Tidak';
        this.jnspembulatan = false;
        this.lbljnspembulatan = 'Bawah';
        this.isviewpdf = false;
        this.invbbm = new InvoiceItem();
        this.div = '';
        this.nomppn = false;
    }
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.isview = true;
                    this.load(params['id']);
                    this.cekPO();
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                    this.cekPO();
                }
            }
        });
        this.registerFromPOBBMLOV();
        this.registerTandaTerima();
    }
    load(id) {
        this.invoiceService.find(id).subscribe(
            (res: Invoice) => {
                if (res.statuses !== 10 && this.isview === false) {
                    this.backMainPage();
                } else {
                    this.totalppn = res.ppn;
                    if (res.ppn > 0 || res.ppn < 0) {
                        this.isppn = 1;
                    }
                    this.inv = res;
                    this.invItems = res.invoice_item;
                    res.invoice_item.forEach(
                        (xx) => {
                            xx.qtypaid = xx.qty;
                        }
                    );
                    console.log('CEK LAGI');
                    const array = [];
                    res.invoice_item.forEach((e) => { array.push({ nopo: e.nopo, total: e.totalprice }) });
                    const result = [];
                    console.log(result);
                    this.getSuplierData(res.invfrom);
                }
            }
        )
    }
    ngOnDestroy() {
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplier().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.isSelectSupplier = true;
    }

    createINV() {
        this.inv.ppn = this.totalppn;
        this.inv.invtype = 6;
        this.inv.invfrom = this.newSuplier.kd_suppnew;
        this.inv.dtreceipt = this.dtReceipt;
        this.inv.idpay = this.idpay;
        this.inv.paytype2 = 1;
        console.log('cek data invoice id nya apa = ', this.inv)
        if (this.inv.invno === '' || this.inv.invno === undefined || this.inv.invno === null) {
            alert('Nomor Faktur Tidak Boleh Kosong');
        } else if (this.inv.taxinvno === '' || this.inv.taxinvno === undefined || this.inv.taxinvno === null) {
            alert('Nomor Faktur Pajak Tidak Boleh Kosong');
        } else {
            this.inv.invoice_item = this.invItems;
            if (this.inv.idinv === undefined) {
                this.invoiceService.create(this.inv).subscribe(
                    (res) => {
                        this.backMainPage();
                    }
                )
            } else if (this.inv.idinv !== undefined) {
                this.invoiceService.update(this.inv).subscribe(
                    (res) => {
                        this.backMainPage();
                    }
                )
            }
            this.backMainPage();
        }
    }

    backMainPage() {
        this.router.navigate(['/invoice']);
    }
    registerFromPOBBMLOV() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('poBBMLOVModified',
            () => {
                this.regGetPOBBm();
            });
    }
    regGetPOBBm(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.invoiceService.values.subscribe(
                    (res: Pobbmlov[]) => {
                        if (this.inv.valuta !== undefined) {
                            if (this.inv.valuta.toLowerCase() !== res[0].valuta.toLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Mata Uang Pada Nomor PO Berbeda');
                                return;
                            }
                        }
                        if (this.inv.valuta === undefined) {
                            this.inv.valuta = res[0].valuta;
                        }
                        console.log('cek data supplier == ', this.newSuplier);
                        if (this.newSuplier.kd_suppnew === undefined) {
                            console.log('get data values po bbm == ', res);
                            res.forEach((e) => {
                                const _nopo = e.nopo;
                                const _nobbm = e.nobbm;
                                const _nosj = e.nosj;
                                const _nolhp = e.lhp;
                                const _qty = e.qty;
                                const _kdbahan = e.productcode;
                                const _nmbahan = e.productname;
                                const _stslhp = e.statuslhp;
                                const _iddetailbbm = e.iddetailbbm;
                                const _disc = e.disc;
                                const _discpercent = e.discpercent;
                                this.filter = new Array<InvoiceItem>();
                                this.filter = this.invItems.filter(
                                    function dataSama(items) {
                                        return (
                                            items.nopo === _nopo &&
                                            items.nobbm === _nobbm &&
                                            items.donum === _nosj &&
                                            items.nolhp === _nolhp &&
                                            items.qty === _qty &&
                                            items.productname === _nmbahan &&
                                            items.productcode === _kdbahan &&
                                            items.iddetailbbm === _iddetailbbm &&
                                            items.discpercent === _discpercent &&
                                            items.totaldisc === _disc)
                                    }
                                );
                                if (this.filter.length <= 0) {
                                    this.invItem = new InvoiceItem();
                                    this.invItem.id = this.invItems.length + 1;
                                    this.invItem.idinvdet = this.guidGenerator.newGuid();
                                    this.invItem.nopo = e.nopo;
                                    this.invItem.nobbm = e.nobbm;
                                    this.invItem.donum = e.nosj;
                                    this.invItem.nolhp = e.lhp;
                                    this.invItem.productcode = e.productcode;
                                    this.invItem.qty = e.qty;
                                    this.invItem.price = e.price;
                                    this.invItem.productname = e.productname
                                    this.invItem.totalprice = (e.qty * e.price);
                                    this.invItem.statuslhp = e.statuslhp;
                                    this.invItem.totaldisc = 0;
                                    this.invItem.discpercent = 0;
                                    this.invItem.qtypaid = e.qty;
                                    this.invItem.iddetailbbm = e.iddetailbbm;
                                    this.invItem.discprop = 0;
                                    this.invItem.totalprint = e.totalprint;
                                    this.invItem.discpercent = e.discpercent;
                                    this.invItem.discprop = e.disc;
                                    this.invItem.totaldisc = (e.qty * e.disc);
                                    this.invItem.discitempo = e.discitempo;
                                    this.invItems = [... this.invItems, this.invItem];
                                }
                            });
                            this.getSuplierData(res[0].kdsupnew);
                            this.inv.invfrom = res[0].kdsupnew;
                            this.countTotal().then(
                                () => {
                                    this.countPrice2(null);
                                }
                            );
                        }
                        if (this.newSuplier.kd_suppnew !== undefined) {
                            if (res[0].kdsupnew.toLocaleLowerCase() !== this.newSuplier.kd_suppnew.toLocaleLowerCase()) {
                                this.toasterService.showToaster('Info', 'Peringatan', 'Supplier Pada Nomor PO Berbeda');
                            } else {
                                console.log('get data values po bbm == ', res);
                                res.forEach((e) => {
                                    const _nopo = e.nopo;
                                    const _nobbm = e.nobbm;
                                    const _nosj = e.nosj;
                                    const _nolhp = e.lhp;
                                    const _qty = e.qty;
                                    const _kdbahan = e.productcode;
                                    const _nmbahan = e.productname;
                                    const _stslhp = e.statuslhp;
                                    const _iddetailbbm = e.iddetailbbm;
                                    const _disc = e.disc;
                                    const _discpercent = e.discpercent;
                                    this.filter = new Array<InvoiceItem>();
                                    this.filter = this.invItems.filter(
                                        function dataSama(items) {
                                            return (
                                                items.nopo === _nopo &&
                                                items.nobbm === _nobbm &&
                                                items.donum === _nosj &&
                                                items.nolhp === _nolhp &&
                                                items.qty === _qty &&
                                                items.productname === _nmbahan &&
                                                items.productcode === _kdbahan &&
                                                items.iddetailbbm === _iddetailbbm &&
                                                items.discpercent === _discpercent &&
                                                items.totaldisc === _disc)
                                        }
                                    );
                                    if (this.filter.length <= 0) {
                                        this.invItem = new InvoiceItem();
                                        this.invItem.id = this.invItems.length + 1;
                                        this.invItem.idinvdet = this.guidGenerator.newGuid();
                                        this.invItem.nopo = e.nopo;
                                        this.invItem.nobbm = e.nobbm;
                                        this.invItem.donum = e.nosj;
                                        this.invItem.nolhp = e.lhp;
                                        this.invItem.productcode = e.productcode;
                                        this.invItem.qty = e.qty;
                                        this.invItem.price = e.price;
                                        this.invItem.productname = e.productname
                                        this.invItem.totalprice = (e.qty * e.price);
                                        this.invItem.statuslhp = e.statuslhp;
                                        this.invItem.totaldisc = 0;
                                        this.invItem.discpercent = 0;
                                        this.invItem.qtypaid = e.qty;
                                        this.invItem.iddetailbbm = e.iddetailbbm;
                                        this.invItem.discprop = 0;
                                        this.invItem.totalprint = e.totalprint;
                                        this.invItem.discpercent = e.discpercent;
                                        this.invItem.discprop = e.disc;
                                        this.invItem.totaldisc = (e.qty * e.disc);
                                        this.invItem.discitempo = e.discitempo;
                                        this.invItems = [... this.invItems, this.invItem];
                                    }
                                });
                                // this.getSuplierData(res[0].kdsupnew);
                            }
                            this.countTotal().then(
                                () => {
                                    this.countPrice2(null);
                                }
                            );
                        }
                    }
                );
                this.invItems.forEach(
                    (e) => {
                        const _totp = e.qtypaid * e.price;
                        // e.discprop = this.inv.disc / (this.invItems.length);
                        e.totalprice = _totp - (e.totaldisc + this.inv.disc / (this.invItems.length));
                    }
                )
                this.invoiceService.values.observers = [];
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }
    getSuplierData(kdsupnew) {
        this.masterSupplierService.findByCode(kdsupnew).subscribe(
            (resS: MasterSupplier) => {
                this.newSuplier = resS;
            }
        )
    }
    countTotal(): Promise<void> {
        return new Promise<void>(
            (resolveTotal) => {
                this.purchasings = new Array<Purchasing>();
                this.inv.total = 0;
                this.inv.subtotal = 0;
                if (this.inv.ppn === undefined || this.inv.ppn === null) {
                    this.inv.ppn = 0;
                }
                if (this.inv.downpayment === undefined || this.inv.downpayment === null) {
                    this.inv.uangmuka = 0;
                }
                if (this.inv.downpayment === undefined || this.inv.downpayment === null) {
                    this.inv.downpayment = 0;
                }
                this.inv.grandtotal = 0;
                if (this.inv.disc === undefined || this.inv.disc === null) {
                    this.inv.disc = 0;
                }
                const array = [];
                this.invItems.forEach((e) => { array.push({ nopo: e.nopo, total: e.totalprice }) });
                const result = [];
                // tslint:disable-next-line: space-before-function-paren
                array.reduce(function (res, value) {
                    if (!res[value.nopo]) {
                        res[value.nopo] = { nopo: value.nopo, total: 0 };
                        result.push(res[value.nopo])
                    }
                    res[value.nopo].total += value.total;
                    return res;
                }, {});

                console.log(result);
                result.forEach(
                    (r) => {
                        this.inv.total = 0;
                        this.inv.disc = 0;
                        this.inv.subtotal = 0;
                        this.inv.ppn = 0;
                        this.inv.grandtotal = 0
                        this.purchasingService.findByNo({ query: r.nopo }).subscribe(
                            (respur: Purchasing) => {
                                respur.totaldisc = (respur.price) * (respur.disc / 100);
                                respur.topay = 0;
                                this.invItems.forEach((xx) => {
                                    respur.topay += xx.totalprice;
                                    this.inv.disc += xx.discitempo;
                                });
                                this.purchasings = [... this.purchasings, respur];
                                this.inv.total += respur.topay;
                                // this.inv.disc += respur.totaldisc;
                                this.inv.subtotal += (respur.topay - respur.totaldisc);
                                this.isppn = respur.isppn;
                                if (respur.isppn === 1) {
                                    if (respur.nomppn == null || respur.nomppn === 10) {
                                        this.inv.ppn += (respur.topay - respur.totaldisc) * (10 / 100);
                                        this.inv.grandtotal += ((respur.topay - respur.totaldisc) + (respur.topay - respur.totaldisc) * (10 / 100));
                                        if (this.inv.ppn < 0) {
                                            this.inv.ppn = this.inv.ppn * -1;
                                        }
                                    }
                                    if (respur.nomppn === 11) {
                                        this.inv.ppn += (respur.topay - respur.totaldisc) * (11 / 100);
                                        this.inv.grandtotal += ((respur.topay - respur.totaldisc) + (respur.topay - respur.totaldisc) * (11 / 100));
                                        if (this.inv.ppn < 0) {
                                            this.inv.ppn = this.inv.ppn * -1;
                                        }
                                    }
                                } else {
                                    this.inv.grandtotal += ((respur.topay - respur.totaldisc));
                                }
                            }
                        )
                    }
                )
                resolveTotal();
            }
        )
    }

    updateKurs(po: Purchasing) {
        this.loadingService.loadingStart();
        this.purchasingService.update(po).subscribe(
            (res) => {
                this.loadingService.loadingStop();
            },
            (err) => { this.loadingService.loadingStop() }
        )
    }
    countDisc(invItem: InvoiceItem) {
        this.countPrice3(null).then(
            () => {
                invItem.totaldisc = parseFloat(((invItem.qtypaid * invItem.price) * invItem.discpercent / 100).toFixed(2));
                invItem.discpercent = parseFloat(((invItem.totaldisc / (invItem.qtypaid * invItem.price)) * 100).toFixed(2));
                invItem.totalprice = (invItem.qtypaid * invItem.price) - invItem.totaldisc - invItem.discprop;
            }
        );
    }

    public cekPO() {
        console.log('CEK LAGI');
        const array = [];
        this.invItems.forEach((e) => { array.push({ nopo: e.nopo, total: e.totalprice }) });
        const result = [];
        // tslint:disable-next-line: space-before-function-paren
        array.reduce(function (resCEK, value) {
            if (!resCEK[value.nopo]) {
                resCEK[value.nopo] = { nopo: value.nopo, total: 0 };
                result.push(resCEK[value.nopo])
            }
            resCEK[value.nopo].total += value.total;
            return resCEK;
        }, {});

        console.log(result);
        result.forEach(
            (r) => {
                this.inv.total = 0;
                this.inv.disc = 0;
                this.inv.subtotal = 0;
                this.inv.ppn = 0;
                this.inv.grandtotal = 0
                this.purchasingService.findByNo({ query: r.nopo }).subscribe(
                    (respur: Purchasing) => {
                        respur.totaldisc = (r.total - respur.downpayment - respur.uangmuka) * (respur.disc / 100);
                        respur.topay = r.total;
                        this.purchasings = [... this.purchasings, respur];
                        this.inv.total += respur.topay;
                        this.inv.disc += respur.totaldisc;
                        this.inv.subtotal += (respur.topay - respur.totaldisc);
                        if (respur.isppn === 1) {
                            if (respur.nomppn == null || respur.nomppn === 10) {
                                this.inv.ppn += (respur.topay - respur.totaldisc) * (10 / 100);
                                this.inv.grandtotal += ((respur.topay - respur.totaldisc) + (respur.topay - respur.totaldisc) * (10 / 100));
                            }
                            if (respur.nomppn === 11) {
                                this.inv.ppn += (respur.topay - respur.totaldisc) * (11 / 100);
                                this.inv.grandtotal += ((respur.topay - respur.totaldisc) + (respur.topay - respur.totaldisc) * (11 / 100));
                            }
                        } else {
                            this.inv.grandtotal += ((respur.topay - respur.totaldisc));
                        }
                    }
                )
            }
        )
    }
    public deleteListArray(rowData: InvoiceItem) {
        console.log('data yang akan di hapus == ', this.invItems);
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                this.invoiceItemService.deletebyid(rowData).subscribe((e) => {console.log(); });
                this.invItems = this.invItems.filter(function dataSama(items) { return (items.idinvdet !== rowData.idinvdet) });
                if (this.invItems.length <= 0) {
                    this.inv.total = 0;
                    this.inv.subtotal = 0;
                    this.inv.discpercent = 0;
                    this.inv.disc = 0;
                    this.inv.ppn = 0;
                    this.inv.uangmuka = 0;
                    this.inv.downpayment = 0;
                    this.inv.grandtotal = 0;
                    this.newSuplier = new MasterSupplier();
                };
                this.countTotal();
            }
        });
    }
    registerTandaTerima() {
        this.eventSubscriberPPLOV = this.eventManager.subscribe('TandaTerimaModified',
            () => {
                this.regTT();
            });
    }
    regTT(): Promise<void> {
        return new Promise<void>(
            (resolvedt) => {
                this.tandaTerimaService.values.subscribe(
                    (res: DetailTandaTerima) => {
                        console.log('Cek data detail Tanda Terima == ', res);
                        this.inv.receiptno = res.receiptno;
                        this.inv.invno = res.invno;
                        this.inv.taxinvno = res.invtaxno;
                        this.inv.iddetreceipt = res.iddetreceipt;
                        this.masterSupplierService.findByCode(res.kdsupplier).subscribe(
                            (resSPL: MasterSupplier) => {
                                this.newSuplier = resSPL;
                                this.inv.invfrom = resSPL.kd_suppnew;
                            }
                        )
                    }
                );
                this.eventSubscriberPPLOV.unsubscribe();
                resolvedt();
            }
        )
    }
    openPdf(rowData: InvoiceItem) {
        console.log('ini adalah hasil nya => ' + rowData);
        this.invbbm = rowData;
        this.loadingService.loadingStart();
        let bbms: string;
        bbms = rowData.nobbm.replace(/\//g, '-');
        this.invoiceService.cekBBM(bbms).subscribe(
            (res) => {
                if (res === 'nontek') {
                    this.div = 'nontek';
                    const pecah = bbms.split('-');
                    const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString() +
                        '|awallhp:' + pecah[0].toString() + '|tengahlhp:' + pecah[1].toString() + '|akhirlhp:' + pecah[2].toString();
                    this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/bbm_non_tek/pdf', { filterData: filter_data })
                        .subscribe((response: any) => {
                            const reader = new FileReader();
                            this.pdfBlob = response.blob();
                            reader.readAsDataURL(response.blob());
                            this.isviewpdf = true;
                            // tslint:disable-next-line: space-before-function-paren
                            reader.onloadend = (e) => {
                                this.pdfSrc = reader.result;
                            }
                            this.loadingService.loadingStop();
                        },
                            (err) => {
                                this.loadingService.loadingStop();
                            });
                    // this.paymentService.bbmTekAtt(
                    //     {
                    //         nobbm: rowData.nobbm
                    //     }
                    // ).subscribe(
                    //     (resAtt: FileAttachment) => {
                    //         this.openPDF(resAtt.filedata, 'application/pdf');
                    //     }
                    // )
                }
                if (res === 'tek') {
                    this.div = 'tek';
                    const pecah = bbms.split('-');
                    let lhp: string;
                    let pecahlhp1 = '';
                    let pecahlhp2 = '';
                    let pecahlhp3 = '';
                    if (rowData.nolhp !== '' && rowData.nolhp !== undefined) {
                        lhp = rowData.nolhp.replace(/\//g, '-');
                        const pecahlhp = lhp.split('-');
                        pecahlhp1 = pecahlhp[0].toString();
                        pecahlhp2 = pecahlhp[1].toString();
                        pecahlhp3 = pecahlhp[2].toString();
                    }
                    const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString()
                        + '|awallhp:' + pecahlhp1 + '|tengahlhp:' + pecahlhp2 + '|akhirlhp:' + pecahlhp3;
                    this.reportUtilService.viewFileReader(process.env.API_C_URL + '/api/report/bbm_tek/pdf', { filterData: filter_data })
                        .subscribe((response: any) => {
                            const reader = new FileReader();
                            const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                            this.pdfBlob = response.blob();
                            reader.readAsDataURL(response.blob());
                            this.isviewpdf = true;
                            // tslint:disable-next-line: space-before-function-paren
                            reader.onloadend = (e) => {
                                this.pdfSrc = reader.result;
                            }
                            this.loadingService.loadingStop();
                        },
                            (err) => {
                                this.loadingService.loadingStop();
                            });
                    this.paymentService.bbmTekAtt(
                        {
                            nobbm: rowData.nobbm
                        }
                    ).subscribe(
                        (resAtt: FileAttachment) => {
                            this.openPDF(resAtt.filedata, 'application/pdf');
                        }
                    )
                }
            }
        )
        this.loadingService.loadingStop();
    }

    print() {
        const bbmprint = new BbmPrint();
        bbmprint.nobbm = this.invbbm.nobbm;
        bbmprint.nolhp = this.invbbm.nolhp;
        bbmprint.div = this.div;
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Cetak Dokumen ? \r\nDokumen hanya bisa di cetak satu kali !!',
            accept: () => {
                this.reportService.setTotalPrint(bbmprint).subscribe(
                    (res) => {
                        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
                        const win = window.open(this.pdfSrc);
                        win.document.write('<iframe id="printf" src="' + this.pdfSrc + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
                    }
                )
            }
        });
    }

    downloadBlob() {
        FileSaver.saveAs(this.pdfBlob, this.invbbm.nobbm);
    }

    openPDF(content: any, fieldContentType) {
        const byteCharacters = atob(content);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        const blobContent = new Blob([byteArray], { 'type': fieldContentType });
        const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
        const win = window.open(content);
        win.document.write('<iframe id="printf" src="' + URL.createObjectURL(blobContent) + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
    }

    backMainPageNew() {
        this.isviewpdf = false;
    }

    countPrice2(data: Pobbmlov): Promise<void> {
        return new Promise<void>(
            (resolvePrice) => {
                let numbppn = 11;
                if (this.nomppn === false) {
                    numbppn = 11;
                }
                if (this.nomppn === true) {
                    numbppn = 10;
                }
                if (this.pembulatan === false) {
                    this.jnspembulatan = false
                    this.lblpembualatan = 'Tidak';
                }
                if (this.pembulatan === true) {
                    this.lblpembualatan = 'Ya';
                }
                if (this.jnspembulatan === false) {
                    this.lbljnspembulatan = 'Bawah';
                }
                if (this.jnspembulatan === true) {
                    this.lbljnspembulatan = 'Atas';
                }
                if (this.inv.disc === undefined || this.inv.disc === null) {
                    this.inv.disc = 0;
                }
                if (this.inv.ppn === undefined || this.inv.ppn === null) {
                    this.inv.ppn = 0;
                }
                if (this.inv.cutcost === undefined || this.inv.cutcost === null) {
                    this.inv.cutcost = 0;
                }
                if (this.inv.invdisc === undefined || this.inv.invdisc === null) {
                    this.inv.invdisc = 0;
                }
                if (this.inv.ppn < 0 ) {
                    this.inv.ppn = this.inv.ppn * -1;
                }
                if (data === null) {
                    console.log('cek data null ngga == ', data);
                    console.log('cek ppn null ngga == ', numbppn);
                    console.log('cek inv.ppn null ngga == ', this.inv.ppn);
                    console.log('cek is ppn nya => ', this.isppn);
                    this.inv.grandtotal = 0;
                    if ((this.inv.ppn > 0 || this.inv.ppn < 0) && this.isppn === 1) {
                        console.log('cek masuk ppn isi == ');
                        if (numbppn === 10) {
                            console.log('cek data masuk radio ppn 10 == ', this.inv.nomppn);
                            this.inv.subtotal = (this.inv.total - this.inv.disc);
                            this.checkPPN = true;
                            this.inv.ppn = (this.inv.total - this.inv.disc) * (10 / 100);
                            this.totalppn = (this.inv.total - this.inv.disc) * (10 / 100);
                            // this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            if (this.pembulatan === false) {
                                this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            }
                            if (this.pembulatan === true) {
                                if (this.jnspembulatan === false) {
                                    this.inv.grandtotal = Math.floor(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                                if (this.jnspembulatan === true) {
                                    this.inv.grandtotal = Math.ceil(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                            }
                        }
                        if (numbppn === 11) {
                            console.log('cek data masuk radio ppn 11 == ', this.inv.nomppn);
                            this.inv.subtotal = (this.inv.total - this.inv.disc);
                            this.checkPPN = true;
                            this.inv.ppn = (this.inv.total - this.inv.disc) * (11 / 100);
                            this.totalppn = (this.inv.total - this.inv.disc) * (11 / 100);
                            console.log('MASUK SINI NGGA ?  == ', this.inv.ppn);
                            // this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            if (this.pembulatan === false) {
                                this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            }
                            if (this.pembulatan === true) {
                                if (this.jnspembulatan === false) {
                                    this.inv.grandtotal = Math.floor(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                                if (this.jnspembulatan === true) {
                                    this.inv.grandtotal = Math.ceil(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                            }
                        }
                    }
                    if (this.inv.ppn === 0 ) {
                        this.checkPPN = false;
                        this.inv.ppn = 0;
                        this.inv.subtotal = (this.inv.total - this.inv.disc);
                        console.log('cek masuk ppn tidak isi == ');
                        // this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                        if (this.pembulatan === false) {
                            this.inv.grandtotal = this.inv.subtotal +  this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                        }
                        if (this.pembulatan === true) {
                            if (this.jnspembulatan === false) {
                                this.inv.grandtotal = Math.floor(this.inv.subtotal + this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                            }
                            if (this.jnspembulatan === true) {
                                this.inv.grandtotal = Math.ceil(this.inv.subtotal + - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                            }
                        }
                    }
                }
                resolvePrice();
            }
        )
    }

    countPrice3(data: Pobbmlov): Promise<void> {
        return new Promise<void>(
            (resolvePrice) => {
                this.inv.total = 0;
                this.inv.disc = 0;
                this.invItems.forEach(
                    (ex) => {
                        this.inv.total += ex.totalprice;
                        this.inv.disc += ex.discitempo;
                    }
                )
                let numbppn = 11;
                if (this.nomppn === false) {
                    numbppn = 11;
                }
                if (this.nomppn === true) {
                    numbppn = 10;
                }
                if (this.pembulatan === false) {
                    this.jnspembulatan = false
                    this.lblpembualatan = 'Tidak';
                }
                if (this.pembulatan === true) {
                    this.lblpembualatan = 'Ya';
                }
                if (this.jnspembulatan === false) {
                    this.lbljnspembulatan = 'Bawah';
                }
                if (this.jnspembulatan === true) {
                    this.lbljnspembulatan = 'Atas';
                }
                if (this.inv.disc === undefined || this.inv.disc === null) {
                    this.inv.disc = 0;
                }
                if (this.inv.ppn === undefined || this.inv.ppn === null) {
                    this.inv.ppn = 0;
                }
                if (this.inv.cutcost === undefined || this.inv.cutcost === null) {
                    this.inv.cutcost = 0;
                }
                if (this.inv.invdisc === undefined || this.inv.invdisc === null) {
                    this.inv.invdisc = 0;
                }
                if (this.inv.ppn < 0 ) {
                    this.inv.ppn = this.inv.ppn * -1;
                }
                if (data === null) {
                    this.inv.grandtotal = 0;
                    if ((this.inv.ppn > 0 || this.inv.ppn < 0) && this.isppn === 1) {
                        if (numbppn === 10) {
                            this.inv.subtotal = (this.inv.total - this.inv.disc);
                            this.checkPPN = true;
                            this.inv.ppn = (this.inv.total - this.inv.disc) * (10 / 100);
                            this.totalppn = (this.inv.total - this.inv.disc) * (10 / 100);
                            // this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            if (this.pembulatan === false) {
                                this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            }
                            if (this.pembulatan === true) {
                                if (this.jnspembulatan === false) {
                                    this.inv.grandtotal = Math.floor(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                                if (this.jnspembulatan === true) {
                                    this.inv.grandtotal = Math.ceil(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                            }
                        }
                        if (numbppn === 11) {
                            console.log('cek data masuk radio ppn 11 == ', this.inv.nomppn);
                            this.inv.subtotal = (this.inv.total - this.inv.disc);
                            this.checkPPN = true;
                            this.inv.ppn = (this.inv.total - this.inv.disc) * (11 / 100);
                            this.totalppn = (this.inv.total - this.inv.disc) * (11 / 100);
                            console.log('MASUK SINI NGGA ?  == ', this.inv.ppn);
                            // this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            if (this.pembulatan === false) {
                                this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                            }
                            if (this.pembulatan === true) {
                                if (this.jnspembulatan === false) {
                                    this.inv.grandtotal = Math.floor(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                                if (this.jnspembulatan === true) {
                                    this.inv.grandtotal = Math.ceil(this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                                }
                            }
                        }
                    }
                    if (this.inv.ppn === 0 ) {
                        this.checkPPN = false;
                        this.inv.ppn = 0;
                        this.inv.subtotal = (this.inv.total - this.inv.disc);
                        console.log('cek masuk ppn tidak isi == ');
                        // this.inv.grandtotal = this.inv.subtotal + this.inv.ppn - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                        if (this.pembulatan === false) {
                            this.inv.grandtotal = this.inv.subtotal +  this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost;
                        }
                        if (this.pembulatan === true) {
                            if (this.jnspembulatan === false) {
                                this.inv.grandtotal = Math.floor(this.inv.subtotal + this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                            }
                            if (this.jnspembulatan === true) {
                                this.inv.grandtotal = Math.ceil(this.inv.subtotal + - this.inv.uangmuka - this.inv.downpayment - this.inv.cutcost);
                            }
                        }
                    }
                }
                resolvePrice();
            }
        )
    }
}
