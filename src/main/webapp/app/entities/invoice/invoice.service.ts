import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';

import { JhiDateUtils } from 'ng-jhipster';

import { Invoice, Pobbmlov, DPUM } from './invoice.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { InvoiceItem } from '../invoice-item';

@Injectable()
export class InvoiceService {
    protected itemValues: Pobbmlov[] = new Array<Pobbmlov>();
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected itemINVValues: Invoice[] = new Array<Invoice>();
    valusInv: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemINVValues);

    private resourceUrl = process.env.API_C_URL + '/api/invoices';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/invoices';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(invoice: Invoice): Observable<Invoice> {
        const copy = this.convert(invoice);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(invoice: Invoice): Observable<Invoice> {
        const copy = this.convert(invoice);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Invoice> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findDP(nopo: any): Observable<Invoice> {
        return this.http.get(`${this.resourceUrl}/${nopo}/findDP`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findDPUM(nopo: any): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/${nopo}/findDPUM`).map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        // entity.dtinv = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtinv);
        // entity.dtcreate = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtcreate);
        // entity.dtmodified = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtmodified);
    }

    private convert(invoice: Invoice): Invoice {
        const copy: Invoice = Object.assign({}, invoice);

        // copy.dtinv = this.dateUtils.toDate(invoice.dtinv);

        // copy.dtcreate = this.dateUtils.toDate(invoice.dtcreate);

        // copy.dtmodified = this.dateUtils.toDate(invoice.dtmodified);
        return copy;
    }

    private convert1(invoice: InvoiceItem): InvoiceItem {
        const copy: InvoiceItem = Object.assign({}, invoice);

        // copy.dtinv = this.dateUtils.toDate(invoice.dtinv);

        // copy.dtcreate = this.dateUtils.toDate(invoice.dtcreate);

        // copy.dtmodified = this.dateUtils.toDate(invoice.dtmodified);
        return copy;
    }

    queryLOV(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov', options)
            .map((res: Response) => this.convertResponse(res));
    }

    pushItemsPobbmLOV(data: Pobbmlov[]) {
        this.itemValues = new Array<Pobbmlov>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    pushItemsInvLov(data: Invoice[]) {
        this.itemINVValues = new Array<Invoice>();
        this.itemINVValues = data;
        this.valusInv.next(this.itemINVValues);
    }
    lov(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-lov', options)
            .map((res: Response) => this.convertResponse(res));
    }
    changeStatus(invoice: Invoice, idstatus: number): Observable<Invoice> {
        const copy = this.convert(invoice);
        return this.http.post(this.resourceUrl + '/set-status/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryLOVPO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lovpo', options)
            .map((res: Response) => this.convertResponse(res));
    }
    findDistinct(id: any): Observable<Invoice> {
        return this.http.get(this.resourceUrl + '/distinct/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    cekBBM(nobbm: any): Observable<string> {
        return this.http.get(this.resourceUrl + '/cek-bbm/' + nobbm).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    updateumtopayment(uangmuka: number, nopurchasing: any): Observable<Invoice> {
        const json: Object = Object.assign({}, { um: uangmuka, nopo: nopurchasing });
        return this.http.post(this.resourceUrl + '/update-um-to-payment', json).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateumtodp(uangmuka: number, nopurchasing: any): Observable<Invoice> {
        const json: Object = Object.assign({}, { um: uangmuka, nopo: nopurchasing });
        return this.http.post(this.resourceUrl + '/update-um-to-dp', json).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    createfromum(invoice: Invoice): Observable<Invoice> {
        const copy = this.convert(invoice);
        return this.http.post(this.resourceUrl + '/from-um', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryLOVBast(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov-bast', options)
            .map((res: Response) => this.convertResponse(res));
    }
    findinv(invoice: Invoice): Observable<any> {
        const copy = this.convert(invoice);
        return this.http.post(this.resourceUrl + '/findinv', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    findpopph(invoice: Invoice): Observable<any> {
        const copy = this.convert(invoice);
        return this.http.post(this.resourceUrl + '/find-po-pph', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    deletebyid(invoiceItem: InvoiceItem): Observable<InvoiceItem> {
        const copy = this.convert1(invoiceItem);
        return this.http.post(this.resourceUrl + '/delete-items', copy).map((res: Response) => {
            return res.json();
        });
    }
}
