import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { InvoiceComponent } from './invoice.component';
import { InvoiceDetailComponent } from './invoice-detail.component';
import { InvoicePopupComponent } from './invoice-dialog.component';
import { InvoiceDeletePopupComponent } from './invoice-delete-dialog.component';
import { InvoiceNewComponent } from './invoice-new.component';
import { PoBbmAsLovComponent, POBBMLovPopupComponent } from './lov/po-bbm-as-lov.component';
import { InvLovPopupComponent } from './lov/invoice-as-lov.component';
import { InvoiceNewDownpaymentComponent } from './invoice-new-downpayment.component';
import { InvoiceNewPpjkComponent } from './invoice-new-ppjk.component';
// import { InvoiceNewBastComponent } from './invoice-new-bast.component';
// import { POBASTLovPopupComponent } from './lov/po-bast-as-lov.component';

@Injectable()
export class InvoiceResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const invoiceRoute: Routes = [
    {
        path: 'invoice',
        component: InvoiceComponent,
        resolve: {
            'pagingParams': InvoiceResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice/:id',
        component: InvoiceDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice-new',
        component: InvoiceNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice-edit/:id/:isview',
        component: InvoiceNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-new-downpayment',
        component: InvoiceNewDownpaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice-edit-dp/:id/:isview',
        component: InvoiceNewDownpaymentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice-new-ppjk',
        component: InvoiceNewPpjkComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-edit-ppjk/:id/:isview',
        component: InvoiceNewPpjkComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // }, {
    //     path: 'invoice-new-bast',
    //     component: InvoiceNewBastComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.invoice.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // }, {
    //     path: 'invoice-edit-bast/:id/:isview',
    //     component: InvoiceNewBastComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.invoice.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // }
];

export const invoicePopupRoute: Routes = [
    // {
    //     path: 'invoice-new',
    //     component: InvoicePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.invoice.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    {
        path: 'invoice/:id/edit',
        component: InvoicePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invoice/:id/delete',
        component: InvoiceDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'po-bbm-as-lov/:kdsup/:isbbm',
        component: POBBMLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'invoice-as-lov',
        component: InvLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    // {
    //     path: 'po-bast-as-lov/:kdsup/:isbbm',
    //     component: POBASTLovPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.invoice.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
