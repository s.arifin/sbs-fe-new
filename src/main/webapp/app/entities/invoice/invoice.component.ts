import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Invoice, InvoiceNew } from './invoice.model';
import { InvoiceService } from './invoice.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';

@Component({
    selector: 'jhi-invoice',
    templateUrl: './invoice.component.html'
})
export class InvoiceComponent implements OnInit, OnDestroy {

    currentAccount: any;
    invoices: InvoiceNew[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selected: Invoice[];
    dtfrom: Date;
    dtthru: Date;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    selectedPIC: any;
    selectedType: any;
    kdsup: string;
    statuses: Array<object> = [
        { label: 'all', value: 0 },
        { label: 'Downpayment', value: 2 },
        { label: 'Pelunasan', value: 3 },
        { label: 'kasnegara', value: 4 },
        { label: 'PPJK', value: 5 }
    ];
    types: Array<object> = [
        { value: 0, label: 'ALL' },
        { value: 'EVN', label: 'EVN' },
        { value: 'JTR', label: 'JTR' },
        { value: 'SVY', label: 'SVY' },
    ];
    isFilter: boolean;
    constructor(
        private invoiceService: InvoiceService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private confirmationService: ConfirmationService,
        private masterSupplierService: MasterSupplierService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.page = 0;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<Invoice>();
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selectedPIC = 0;
        this.selectedType = 0;
        this.kdsup = '';
        this.isFilter = false;
        this.newSuplier = new MasterSupplier();
    }

    loadAll() {
        if (this.isFilter === true) {
            const waktu = '23:59:59';
            const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
            const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
            if (this.currentSearch) {
                this.invoiceService.queryFilter({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'cari:' + this.currentSearch + '|kdsup:' + this.kdsup + '|dtfrom:' + dari + '|dtthru:' + sampai + '|pic:' + this.selectedPIC + '|jenis:' + this.selectedType,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            } else {
                this.invoiceService.queryFilter({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'cari:' + this.currentSearch + '|kdsup:' + this.kdsup + '|dtfrom:' + dari + '|dtthru:' + sampai + '|pic:' + this.selectedPIC + '|jenis:' + this.selectedType,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        } else {
            if (this.currentSearch) {
                this.invoiceService.query({
                    page: this.page,
                    query: 'cari:' + this.currentSearch,
                    size: this.itemsPerPage
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            } else {
                this.invoiceService.query({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'cari:',
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/invoice'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    filter() {
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.kd_suppnew === undefined) {
            this.kdsup = ''
        } else {
            this.kdsup = this.newSuplier.kd_suppnew;
        }
        this.loadAll();
        this.isFilter = true;
    }

    reset() {
        this.loadAll();
        this.kdsup = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.selectedPIC = 0;
        this.selectedType = 0;
        this.isFilter = false;
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/invoice', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/invoice', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInInvoices();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Invoice) {
        return item.id;
    }
    registerChangeInInvoices() {
        this.eventSubscriber = this.eventManager.subscribe('invoiceListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.invoices = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    public cancel(data: Invoice) {
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Cancel Faktu (invoice) ' + data.invno,
            accept: () => {
                this.invoiceService.changeStatus(data, 13)
                    .subscribe(
                        (res) => this.loadAll()
                    );
            }
        });
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.kd_suppnew;
    }
}
