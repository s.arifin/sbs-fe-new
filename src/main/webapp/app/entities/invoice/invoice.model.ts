import { InvoiceItem } from '../invoice-item';
import { BaseEntity } from './../../shared';

export class Invoice implements BaseEntity {
    constructor(
        public id?: any,
        public idinv?: any,
        public invno?: string,
        public taxinvno?: string,
        public invfrom?: string,
        public total?: number,
        public isdic?: number,
        public disc?: number,
        public discpercent?: number,
        public subtotal?: number,
        public ppn?: number,
        public grandtotal?: number,
        public dtinv?: any,
        public dtcreate?: any,
        public createby?: string,
        public dtmodified?: any,
        public modifiedby?: string,
        public invto?: string,
        public note?: string,
        public receiptno?: string,
        public dtreceipt?: Date,
        public invoice_item?: InvoiceItem[],
        public invtype?: number,
        public invtypedesc?: string,
        public valuta?: string,
        public invoice_type?: InvoiceType,
        public uangmuka?: number,
        public downpayment?: number,
        public statuses?: number,
        public stype?: string,
        public totalpaid?: number,
        public totalpaidtrf?: number,
        public totalpaidretur?: number,
        public totalpaidcash?: number,
        public cutcost?: number,
        public notecutcost ?: string,
        public paynoppn?: number,
        public checkppn?: boolean,
        public checkpokok?: boolean,
        public totalpaidppn?: number,
        public totalpaidnoppn?: number,
        public iddetreceipt?: any,
        public kurs?: number,
        public totalppn?: number,
        public invdisc?: number,
        public nomppn?: number,
        public idpay?: any,
        public paytype2?: number,
        public notes2?: string,
        public addcost?: number,
        public addcostnote?: string,
        public catatan_dp?: string,
        public is_dp?: number,
        public dp_persen?: number,
    ) {
        this.invoice_item = new Array<InvoiceItem>();
        // tslint:disable-next-line: no-use-before-declare
        this.invoice_type = new InvoiceType();
    }
}

export class DPUM implements BaseEntity {
    constructor(
        public id?: any,
        public receiptno?: string,
        public nopay?: string,
        public paytype?: number
    ) {

    };
}

export class Pobbmlov {
    constructor(
        public idpo?: any,
        public nopo?: string,
        public nobbm?: string,
        public nosj?: string,
        public lhp?: string,
        public productcode?: string,
        public productname?: string,
        public qty?: number,
        public price?: number,
        public kdsupnew?: string,
        public isppn?: number,
        public valuta?: string,
        public statuslhp?: string,
        public iddetailbbm?: any,
        public nobbk?: string,
        public totalprint?: number,
        public disc?: number,
        public discpercent?: number,
        public nomppn?: number,
        public discitempo?: number,
        public jenis_klaim?: string,
        public is_nr?: number
    ) {

    }
}

export class InvoiceType {
    constructor(
        public id?: number,
        public description?: string,
    ) {

    }
}

export class InvoiceNew implements BaseEntity {
    constructor(
        public id?: any,
        public idinv?: any,
        public invno?: string,
        public taxinvno?: string,
        public invfrom?: string,
        public dtcreate?: any,
        public createby?: string,
        public receiptno?: string,
        public dtreceipt?: Date,
        public invtype?: number,
        public invtypedesc?: string,
        public statuses?: number,
        public stype?: string,
        public nm_supplier?: string
    ) {
    }
}
