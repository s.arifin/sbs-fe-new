import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    ToggleButtonModule
} from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import {
    InvoiceService,
    InvoicePopupService,
    InvoiceComponent,
    InvoiceDetailComponent,
    InvoiceDialogComponent,
    InvoicePopupComponent,
    InvoiceDeletePopupComponent,
    InvoiceDeleteDialogComponent,
    invoiceRoute,
    invoicePopupRoute,
    InvoiceResolvePagingParams,
    InvoiceNewComponent,
    InvoiceNewDownpaymentComponent,
    InvoiceUmPoComponent
} from './';
import { GuidGenerator } from './GuidGenerator';
import { InvoiceNewPpjkComponent } from './invoice-new-ppjk.component';
import { InvoiceNewUmComponent } from './invoice-new-um.component';
import { InvoicePaymentNewDownpaymentComponent } from './invoice-payment-new-downpayment.component';
import { InvLovPopupComponent, InvoiceAsLovComponent } from './lov/invoice-as-lov.component';
import { PoBbmAsLovComponent, POBBMLovPopupComponent } from './lov/po-bbm-as-lov.component';
// import { InvoiceNewBastComponent } from './invoice-new-bast.component';
// import { POBASTLovPopupComponent, PoBastAsLovComponent } from './lov/po-bast-as-lov.component';

const ENTITY_STATES = [
    ...invoiceRoute,
    ...invoicePopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        ToggleButtonModule
    ],
    declarations: [
        InvoiceComponent,
        InvoiceDetailComponent,
        InvoiceDialogComponent,
        InvoiceDeleteDialogComponent,
        InvoicePopupComponent,
        InvoiceDeletePopupComponent,
        InvoiceNewComponent,
        PoBbmAsLovComponent,
        POBBMLovPopupComponent,
        InvLovPopupComponent,
        InvoiceAsLovComponent,
        InvoiceNewDownpaymentComponent,
        InvoicePaymentNewDownpaymentComponent,
        InvoiceNewPpjkComponent,
        InvoiceUmPoComponent,
        InvoiceNewUmComponent,
        // InvoiceNewBastComponent,
        // PoBastAsLovComponent,
        // POBASTLovPopupComponent,
    ],
    entryComponents: [
        InvoiceComponent,
        InvoiceDialogComponent,
        InvoicePopupComponent,
        InvoiceDeleteDialogComponent,
        InvoiceDeletePopupComponent,
        InvoiceNewComponent,
        PoBbmAsLovComponent,
        POBBMLovPopupComponent,
        InvLovPopupComponent,
        InvoiceAsLovComponent,
        InvoiceNewDownpaymentComponent,
        InvoicePaymentNewDownpaymentComponent,
        InvoiceNewPpjkComponent,
        InvoiceUmPoComponent,
        InvoiceNewUmComponent,
        // InvoiceNewBastComponent,
        // PoBastAsLovComponent,
        // POBASTLovPopupComponent,
    ],
    providers: [
        InvoiceService,
        InvoicePopupService,
        InvoiceResolvePagingParams,
        InvoicePopupService,
        GuidGenerator
    ],
    exports: [
        InvoicePaymentNewDownpaymentComponent,
        InvoiceUmPoComponent,
        InvoiceNewUmComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmInvoiceModule { }
