import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { NgbActiveModal, } from '@ng-bootstrap/ng-bootstrap';
import { LazyLoadEvent } from 'primeng/primeng';
import * as _ from 'lodash';
import { Invoice } from '../invoice.model';
import { InvoiceService } from '../invoice.service';
import { InvoicePopupService } from '../invoice-popup.service';

@Component({
    selector: 'jhi-invoice-as-lov',
    templateUrl: './invoice-as-lov.component.html'
})
export class InvoiceAsLovComponent implements OnInit, OnDestroy {

    invoices: Invoice[]
    selected: Invoice[];
    currentAccount: any;
    isSaving: boolean;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    first: number;
    isDisable: boolean;
    tempTT: string;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private principal: Principal,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private invoiceServices: InvoiceService,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = 100;
        this.page = 1;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<Invoice>();
        this.idInternal = this.principal.getIdInternal();
        this.first = 0;
        this.isDisable = false;
        this.tempTT = '';
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.invoiceServices.lov({
                query: 'cari:' + this.currentSearch,
                page: this.page - 1,
                size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.selected = new Array<Invoice>();
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new Array<Invoice>();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: Invoice) {
        return item.idinv;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.invoices = data;

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.receiptno)) {
                map.set(item.receiptno, true);    // set any value to Map
                result.push({
                    // this.tempTT = result[0].nott;
                    nott: item.receiptno,
                });
            }
        }
        if (result.length === 1) {
            this.invoiceServices.pushItemsInvLov(this.selected);
            this.eventManager.broadcast({ name: 'InvoiceModified', content: 'OK' });
            this.activeModal.dismiss('close');
            this.selected = new Array<Invoice>();
        }
        if (result.length > 1) {
            this.toasterService.showToaster('Info', 'Informasi', 'PP yang dipilih harus memiliki PIC dan Tipe PP yang sama');
        }
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    onRowSelect(event) {
        console.log('leng selected data == ', this.selected);
        if (this.selected.length === 0) {
            this.tempTT = ''
        }
        if (this.selected.length === 1) {
            this.tempTT = event.data.receiptno
            this.isDisable = false;
        }
        if (this.selected.length > 1) {
            if (this.tempTT !== event.data.receiptno) {
                this.toasterService.showToaster('Info', 'Informasi', 'INVOICE yang dipilih harus memiliki TANDA TERIMA sama');
                this.isDisable = true;
            }
        }
    }
    onRowUnselect(event) {
        const result = [];
        const map = new Map();
        for (const item of this.selected) {
            if (!map.has(item.receiptno)) {
                map.set(item.receiptno, true);    // set any value to Map
                result.push({
                    nott: item.receiptno,
                });
            }
        }
        if (result.length === 1) {
            this.tempTT = result[0].nott;
            this.isDisable = false;
        }
        if (result.length > 1) {
            this.isDisable = true;
        }
        console.log('cek result ==== ', result);
    }

}

@Component({
    selector: 'jhi-inv-lov-popup',
    template: ''
})
export class InvLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        protected invoicePopupService: InvoicePopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.invoicePopupService.load(InvoiceAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
