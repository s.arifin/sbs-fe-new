import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { NgbActiveModal, } from '@ng-bootstrap/ng-bootstrap';
import { LazyLoadEvent } from 'primeng/primeng';
import { PurchasingService } from '../../purchasing';
import * as _ from 'lodash';
import { PurchasingPopupService } from '../../purchasing/purchasing-popup.service';
import { Pobbmlov } from '../invoice.model';
import { InvoiceService } from '../invoice.service';
import { LoadingService } from '../../../layouts';

@Component({
    selector: 'jhi-po-bbm-as-lov',
    templateUrl: './po-bbm-as-lov.component.html'
})
export class PoBbmAsLovComponent implements OnInit, OnDestroy {

    // @Output()
    // fnCallback = new EventEmitter();

    // this.fnCallback.emit(this.receipts);

    pobbms: Pobbmlov[]
    selected: Pobbmlov[];
    currentAccount: any;
    isSaving: boolean;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearchBBM: string;
    currentSearchBBMRetur: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    idInternal: any;
    first: number;
    data: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private principal: Principal,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private invoiceServices: InvoiceService,
        private loadingService: LoadingService,
    ) {
        this.itemsPerPage = 100;
        this.page = 1;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<Pobbmlov>();
        this.idInternal = this.principal.getIdInternal();
        this.first = 0;
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            console.log('cek data yang masuk = ', this.data);
            if (this.currentSearch || this.currentSearchBBM || this.currentSearchBBMRetur) {
                if ( this.data['isbbm'] === 'true') {
                    console.log('cek data yang masuk true = ', this.data);
                    this.loadingService.loadingStart();
                    this.invoiceServices.queryLOV({
                        query: 'nopo:' + this.currentSearch + '|kdsup:' + this.data['kdsup'] + '|nobbm:' + this.currentSearchBBM + '|nobbmretur:' + this.currentSearchBBMRetur,
                        page: this.page - 1,
                        size: this.itemsPerPage,
                    }).subscribe(
                        (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                        (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
                    );
                    this.selected = new Array<Pobbmlov>();
                }
                if ( this.data['isbbm'] === 'false') {
                    console.log('cek data yang masuk false = ', this.data);
                    this.loadingService.loadingStart();
                    this.invoiceServices.queryLOVPO({
                        query: 'nopo:' + this.currentSearch + '|kdsup:' + this.data['kdsup'] + '|nobbm:' + this.currentSearchBBM + '|nobbmretur:' + this.currentSearchBBMRetur,
                        page: this.page - 1,
                        size: this.itemsPerPage,
                    }).subscribe(
                        (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
                        (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
                    );
                    this.selected = new Array<Pobbmlov>();
                }
            }
        });
    }
    // loadAll() {
    //     this.routeSub = this.route.params.subscribe((params) => {
    //         if (this.currentSearch) {
    //                 this.loadingService.loadingStart();
    //                 this.invoiceServices.queryLOV({
    //                     query: 'nopo:' + this.currentSearch + '|kdsup:' + this.data['kdsup'],
    //                     page: this.page - 1,
    //                     size: this.itemsPerPage,
    //                 }).subscribe(
    //                     (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers); this.loadingService.loadingStop() },
    //                     (res: ResponseWrapper) => { this.onError(res.json); this.loadingService.loadingStop() }
    //                 );
    //                 this.selected = new Array<Pobbmlov>();
    //         }
    //     });
    // }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.currentSearchBBM = '';
        this.currentSearchBBMRetur = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.loadAll();
    }
    searchBBM(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearchBBM = query;
        this.loadAll();
    }
    searchBBMRetur(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearchBBMRetur = query;
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new Array<Pobbmlov>();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: Pobbmlov) {
        return item.nobbm;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.pobbms = data;

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.invoiceServices.pushItemsPobbmLOV(this.selected);
        this.eventManager.broadcast({ name: 'poBBMLOVModified', content: 'OK' });
        this.activeModal.dismiss('close');
        this.selected = new Array<Pobbmlov>();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-po-bbm-lov-popup',
    template: ''
})
export class POBBMLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        protected purchasingPopupService: PurchasingPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['isbbm']) {
                const data: object = {
                    'kdsup': params['kdsup'],
                   'isbbm': params['isbbm']
                }
                this.purchasingPopupService.load(PoBbmAsLovComponent as Component, data);
            } else {
                this.purchasingPopupService.load(PoBbmAsLovComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

}
