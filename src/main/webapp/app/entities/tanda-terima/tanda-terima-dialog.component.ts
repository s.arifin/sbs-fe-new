import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TandaTerima } from './tanda-terima.model';
import { TandaTerimaPopupService } from './tanda-terima-popup.service';
import { TandaTerimaService } from './tanda-terima.service';

@Component({
    selector: 'jhi-tanda-terima-dialog',
    templateUrl: './tanda-terima-dialog.component.html'
})
export class TandaTerimaDialogComponent implements OnInit {

    tandaTerima: TandaTerima;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private tandaTerimaService: TandaTerimaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tandaTerima.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tandaTerimaService.update(this.tandaTerima));
        } else {
            this.subscribeToSaveResponse(
                this.tandaTerimaService.create(this.tandaTerima));
        }
    }

    private subscribeToSaveResponse(result: Observable<TandaTerima>) {
        result.subscribe((res: TandaTerima) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TandaTerima) {
        this.eventManager.broadcast({ name: 'tandaTerimaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-tanda-terima-popup',
    template: ''
})
export class TandaTerimaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tandaTerimaPopupService: TandaTerimaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tandaTerimaPopupService
                    .open(TandaTerimaDialogComponent as Component, params['id']);
            } else {
                this.tandaTerimaPopupService
                    .open(TandaTerimaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
