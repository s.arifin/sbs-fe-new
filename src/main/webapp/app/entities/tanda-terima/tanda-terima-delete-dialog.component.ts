import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TandaTerima } from './tanda-terima.model';
import { TandaTerimaPopupService } from './tanda-terima-popup.service';
import { TandaTerimaService } from './tanda-terima.service';

@Component({
    selector: 'jhi-tanda-terima-delete-dialog',
    templateUrl: './tanda-terima-delete-dialog.component.html'
})
export class TandaTerimaDeleteDialogComponent {

    tandaTerima: TandaTerima;

    constructor(
        private tandaTerimaService: TandaTerimaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tandaTerimaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tandaTerimaListModification',
                content: 'Deleted an tandaTerima'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tanda-terima-delete-popup',
    template: ''
})
export class TandaTerimaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tandaTerimaPopupService: TandaTerimaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tandaTerimaPopupService
                .open(TandaTerimaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
