import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TandaTerimaComponent } from './tanda-terima.component';
import { TandaTerimaDetailComponent } from './tanda-terima-detail.component';
import { TandaTerimaPopupComponent } from './tanda-terima-dialog.component';
import { TandaTerimaDeletePopupComponent } from './tanda-terima-delete-dialog.component';
import { TandaTerimaNewComponent } from './tanda-terima-new/tanda-terima-new.component';
import { TandaTerimaLovPopupComponent } from './lov/tanda-terima-as-lov.component';
import { TandaTerimaNewSuratComponent } from './tanda-terima-new/tanda-terima-new-surat.component';
import { TandaTerimaSuratComponent } from './tanda-terima-surat.component';
import { TandaTerimaBayarComponent } from './tanda-terima-bayar.component';
import { TandaTerimaItemComponent } from './tanda-terima-item.component';

@Injectable()
export class TandaTerimaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const tandaTerimaRoute: Routes = [
    {
        path: 'tanda-terima',
        component: TandaTerimaComponent,
        resolve: {
            'pagingParams': TandaTerimaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima/:id',
        component: TandaTerimaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-new',
        component: TandaTerimaNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-edit/:id/:isview',
        component: TandaTerimaNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-new-surat',
        component: TandaTerimaNewSuratComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-edit-surat/:id/:isview',
        component: TandaTerimaNewSuratComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-surat',
        component: TandaTerimaSuratComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tanda-terima-bayar/:id/:isview',
        component: TandaTerimaBayarComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tanda-terima-items',
        component: TandaTerimaItemComponent,
        resolve: {
            'pagingParams': TandaTerimaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
export const tandaTerimaPopupRoute: Routes = [
    {
        path: 'tanda-terima/:id/edit',
        component: TandaTerimaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tanda-terima/:id/delete',
        component: TandaTerimaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'tanda-terima-as-lov',
        component: TandaTerimaLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.tandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
