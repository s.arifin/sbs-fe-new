import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { TandaTerima } from '../tanda-terima.model';
import { TandaTerimaService } from '../tanda-terima.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { DetailTandaTerima, DetailTandaTerimaService } from '../../detail-tanda-terima';
import { MasterSupplier, MasterSupplierService } from '../../master-supplier';
import { ConfirmationService, ConfirmDialog } from 'primeng/primeng';
import * as ConfigConstants from '../../../shared/constants/config.constants';
import { TandaTerimaDocument } from '../../tanda-terima-document';
import { LoadingService } from '../../../layouts';

@Component({
    selector: 'jhi-tanda-terima-new',
    templateUrl: './tanda-terima-new.component.html'
})
export class TandaTerimaNewComponent implements OnInit, OnDestroy {
    [x: string]: any;
    tandaTerima: TandaTerima;
    item: DetailTandaTerima;
    items: DetailTandaTerima[];
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    subscription: Subscription;
    isview: boolean;
    ttdtreceipt: Date;
    modalLampiran: boolean;
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;
    document: TandaTerimaDocument;
    currentAccount: any;
    nott: string;
    user: string;
    constructor(
        private tandaTerimaService: TandaTerimaService,
        private masterSupplierService: MasterSupplierService,
        private router: Router,
        private route: ActivatedRoute,
        private confirmationService: ConfirmationService,
        protected dataUtils: JhiDataUtils,
        protected detailTandaTerimaService: DetailTandaTerimaService,
        private loadingService: LoadingService,
        private toasterService: ToasterService,
        private principal: Principal,
    ) {
        this.tandaTerima = new TandaTerima();
        this.item = new DetailTandaTerima();
        this.items = new Array<DetailTandaTerima>();
        this.newSuplier = new MasterSupplier();
        this.isview = false
        this.ttdtreceipt = new Date();
        this.modalLampiran = false;
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
        this.document = new TandaTerimaDocument();
        this.nott = 'XXXX-XXXXXXX';
    }
    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                if (params['isview'] === 'true') {
                    this.tandaTerimaService.cek(params['id'])
                        .subscribe(
                            (ress) => {
                                this.isview = true;
                                this.load(params['id']);
                            },
                            (error) => {
                                this.toasterService.showToaster('info', 'Edit', 'Tanda Terima tidak boleh di edit karena sudah jadi invoice / payment');
                                this.confirmationService.confirm({
                                    header: 'WARNING',
                                    message: 'Tanda Terima sudah di buat Invoice / Payment',
                                    accept: () => {
                                        window.history.back();
                                        this.router.navigate(['tanda-terima']);
                                    },
                                    rejectVisible: false,
                                    reject: () => {
                                        window.history.back();
                                        this.router.navigate(['tanda-terima']);
                                    }
                                });
                            }
                        )
                    // this.cekPO();
                }
                if (params['isview'] === 'false') {
                    this.isview = false;
                    this.load(params['id']);
                    // this.cekPO();
                }
            } else {
                this.isview = true;
            }

            this.principal.identity(true).then((account) => {
                this.currentAccount = account;
            });
        });
    }
    load(id) {
        this.tandaTerimaService.find(id).subscribe(
            (res: TandaTerima) => {
                this.tandaTerima = res;
                this.ttdtreceipt = new Date(res.dtreceipt);
                this.items = res.items;
                this.items.forEach(
                    (e) => {
                        e.dtinv = new Date(e.dtinv);
                        if (e.dtinvtax === null) {

                        } else {
                            e.dtinvtax = new Date(e.dtinvtax);
                        }

                        if (res.dtpay !== null) {
                            e.is_disable = true;
                        } else {
                            e.is_disable = false;
                        }
                    }
                )
                this.newSuplier = res.mastersupplier;
                this.nott = res.receiptno
                this.newSuplier.nmSupplier = res.nmsupplier;
            }
        )
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
    tambahBaris() {
        this.item = new DetailTandaTerima();
        this.item.nourut = this.items.length + 1;
        this.items = [... this.items, this.item];
    }
    savett() {
        this.tandaTerima.items = this.items;
        this.tandaTerima.dtreceipt = this.ttdtreceipt;
        if (this.tandaTerima.jenis === undefined || this.tandaTerima.jenis === '' || this.tandaTerima.jenis === null) {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Jenis Tanda Terima Tidak Boleh Kosong',
                accept: () => { }
            });
        } else if (this.tandaTerima.kdsup === undefined || this.tandaTerima.kdsup === '' || this.tandaTerima.kdsup === null) {
            this.confirmationService.confirm({
                header: 'Konfirmasi',
                message: 'Suplier Tidak Boleh Kosong',
                accept: () => { }
            });
        } else {
            if (this.tandaTerima.idreceipt === undefined) {
                this.loadingService.loadingStart();
                this.tandaTerimaService.create(this.tandaTerima)
                    .subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            console.log('sukses');
                            this.router.navigate(['../tanda-terima']);
                        },
                        (err: ResponseWrapper) => {
                            this.loadingService.loadingStop();
                        }
                    )
            } else {
                if (this.principal.getUserLogin() !== this.tandaTerima.createby) {
                    alert('Pembuat Tanda Terima Berbeda, Tidak Boleh Edit Yang Berbeda Pembuat')
                } else {
                    if (this.tandaTerima.statustype === 13) {
                        alert('Tanda Terima yang sudah di cancel tidak bisa di edit lagi !');
                    } else {
                        this.loadingService.loadingStart();
                        this.tandaTerimaService.update(this.tandaTerima)
                            .subscribe(
                                (res) => {
                                    this.loadingService.loadingStop();
                                    console.log('sukses');
                                    this.router.navigate(['../tanda-terima']);
                                },
                                (err: ResponseWrapper) => {
                                    this.loadingService.loadingStop();
                                }
                            )
                    }
                }
            }
        }
    }
    backMainPage() {
        this.router.navigate(['../tanda-terima']);
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.tandaTerima.jenis).then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierDIV(this.tandaTerima.jenis).then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.tandaTerima.kdsup = this.newSuplier.kd_suppnew;
    }
    ceknott() {
        this.tandaTerimaService.cekNoTT(this.tandaTerima.receiptno)
            .subscribe(
                (res: ResponseWrapper) => {
                    // console.log(' hasil dari JSON = ', res.json);
                    // console.log(' hasil dari Status = ', res.status);
                    // console.log(' hasil dari Headers = ', res.headers);
                    if (res.status === 200) {
                        this.confirmationService.confirm({
                            header: 'Information',
                            message: 'nomor tanda terima sudah ada',
                            rejectVisible: false,
                            accept: () => {
                                this.tandaTerima.receiptno = ''
                            }
                        });
                    }
                    if (res.status === 404) {

                    }
                }
            )
    }
    addAtt(rowData: DetailTandaTerima) {
        this.modalLampiran = true;
        this.item = rowData;
        this.document = new TandaTerimaDocument();
    }
    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
            console.log('entity == ', entity);
            console.log('field === ', field);
            console.log('content', this.document.doc);
            console.log('contenttype', this.document.doctype);
        } else {
            this.isValidImage = true;
            this.clearInputImage('doc', 'doctype', 'fileImage');
            this.isInsertFile = null;
            console.log('masuk kbesaran');
        }
    }
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.item, this.elementRef, field, fieldContentType, idInput);
    }
    setImange() {
        this.modalLampiran = false;
        this.item.document = this.document;
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    public deleteListArray(rowData: DetailTandaTerima) {
        this.confirmationService.confirm({
            header: 'Konfirmasi',
            message: 'Apakah Anda yakin akan menghapus data ini ??',
            accept: () => {
                if (rowData.iddetreceipt === undefined && rowData.iddetreceipt !== null) {
                    this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                    console.log('items length == ' + this.items.length);
                    if (this.items.length <= 0) {
                    };
                } else {
                    this.detailTandaTerimaService.delete(rowData.iddetreceipt).subscribe(
                        (res) => {
                            this.items = this.items.filter(function dataSama(items) { return (items.nourut !== rowData.nourut) });
                            console.log('items length == ' + this.items.length);
                            if (this.items.length <= 0) {
                            };
                        }
                    )
                }
                this.items = this.setNoUrut(this.items);
            }
        });
    }
    setNoUrut(items: DetailTandaTerima[]): DetailTandaTerima[] {
        let urut = 1;
        this.items.forEach(
            (e) => {
                e.nourut = urut;
                urut++;
            }
        );
        return this.items;
    }

    cekInvoice(rowData: DetailTandaTerima) {
        if (rowData.invno !== null && rowData.invno !== '' && rowData.invno !== undefined) {
            this.tandaTerimaService.cekINV(rowData)
                .subscribe(
                    (res) => {
                        if (res.notes === undefined ||  res.notes === '' || res.notes === null) {

                        } else {
                            if (res.idreceipt === this.tandaTerima.idreceipt) {

                            } else {
                                alert(res.notes);
                            }
                        }
                    },
                    (err: ResponseWrapper) => {
                    }
                )
        }
    }
}
