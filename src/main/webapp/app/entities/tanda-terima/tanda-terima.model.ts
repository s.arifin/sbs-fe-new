import { DetailTandaTerima } from '../detail-tanda-terima';
import { BaseEntity } from './../../shared';

export class TandaTerima implements BaseEntity {
    constructor(
        public id?: number,
        public idreceipt?: any,
        public receiptno?: string,
        public dtreceipt?: any,
        public kdsup?: string,
        public dtdue?: any,
        public dtpurc?: any,
        public dtmkt?: any,
        public dtacc?: any,
        public dtfin?: any,
        public numbgiro?: string,
        public dtgiro?: any,
        public total?: number,
        public grandtotal?: number,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public items?: DetailTandaTerima[],
        public nmsupplier?: string,
        public mastersupplier?: MasterSupplier,
        public jenis?: string,
        public receiver?: string,
        public sender?: string,
        public doctype?: string,
        public notes?: string,
        public totalpaid?: number,
        public bankcode?: string,
        public dtrelease?: Date,
        public dtpay?: Date,
        public nogiro?: string,
        public noinv?: string,
        public notaxinv?: string,
        public cancelnote?: string,
        public costreduction?: number,
        public costreductionnotes?: string,
        public ppn?: number,
        public totalppn?: number,
        public pph?: number,
        public notepph?: string,
        public dtregacc?: Date,
        public dtaccappby?: Date,
        public dtregfin?: Date,
        public statustype?: number,
        public status_desc?: string,
    ) {
        this.items = new Array<DetailTandaTerima>();
        // tslint:disable-next-line: no-use-before-declare
        this.mastersupplier = new MasterSupplier();
    }
}
export class MasterSupplier {
    constructor(
        public id?: number,
        public idSupplier?: any,
        public kdSupplier?: string,
        public nm_supplier?: string,
        public almtSupplier1?: any,
        public almtSupplier2?: any,
        public telp?: string,
        public fax?: string,
        public person1?: string,
        public jsup?: string,
        public person2?: string,
        public alamat1?: string,
        public alamat2?: string,
        public kd_suppnew?: string,
        public tenor?: number,
        public email?: string,
        public npwp?: string,
    ) {
    }
}
