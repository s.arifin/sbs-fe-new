import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TandaTerima } from './tanda-terima.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { DetailTandaTerima } from '../detail-tanda-terima';

@Injectable()
export class TandaTerimaService {
    protected itemValues: DetailTandaTerima[] = new Array<DetailTandaTerima>();
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/tanda-terimas';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/tanda-terimas';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<TandaTerima> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        // entity.dtreceipt = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtreceipt);
        // entity.dtdue = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtdue);
        // entity.dtpurc = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtpurc);
        // entity.dtmkt = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtmkt);
        // entity.dtacc = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtacc);
        // entity.dtfin = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtfin);
        // entity.dtgiro = this.dateUtils
        //     .convertDateTimeFromServer(entity.dtgiro);
    }

    private convert(tandaTerima: TandaTerima): TandaTerima {
        const copy: TandaTerima = Object.assign({}, tandaTerima);

        // copy.dtreceipt = this.dateUtils.toDate(tandaTerima.dtreceipt);

        // copy.dtdue = this.dateUtils.toDate(tandaTerima.dtdue);

        // copy.dtpurc = this.dateUtils.toDate(tandaTerima.dtpurc);

        // copy.dtmkt = this.dateUtils.toDate(tandaTerima.dtmkt);

        // copy.dtacc = this.dateUtils.toDate(tandaTerima.dtacc);

        // copy.dtfin = this.dateUtils.toDate(tandaTerima.dtfin);

        // copy.dtgiro = this.dateUtils.toDate(tandaTerima.dtgiro);
        return copy;
    }

    private convertDetail(tandaTerima: DetailTandaTerima): DetailTandaTerima {
        const copy: DetailTandaTerima = Object.assign({}, tandaTerima);

        // copy.dtreceipt = this.dateUtils.toDate(tandaTerima.dtreceipt);

        // copy.dtdue = this.dateUtils.toDate(tandaTerima.dtdue);

        // copy.dtpurc = this.dateUtils.toDate(tandaTerima.dtpurc);

        // copy.dtmkt = this.dateUtils.toDate(tandaTerima.dtmkt);

        // copy.dtacc = this.dateUtils.toDate(tandaTerima.dtacc);

        // copy.dtfin = this.dateUtils.toDate(tandaTerima.dtfin);

        // copy.dtgiro = this.dateUtils.toDate(tandaTerima.dtgiro);
        return copy;
    }
    pushItems(data: DetailTandaTerima[]) {
        this.itemValues = new Array<DetailTandaTerima>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
    queryLOV(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov', options)
            .map((res: Response) => this.convertResponse(res));
    }
    cekNoTT(nott: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/cek/${nott}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    cekINV(tandaTerima: DetailTandaTerima): Observable<DetailTandaTerima> {
        const copy = this.convertDetail(tandaTerima);
        return this.http.post(`${this.resourceUrl}/cekINV`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    cekNoTTTT(nott: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/cekTT/${nott}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    createSurat(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/surat', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateSurat(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl + '/surat', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    querySurat(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/surat', options)
            .map((res: Response) => this.convertResponse(res));
    }

    setStatusSelesai(purchasing: TandaTerima[]): Observable<TandaTerima[]> {
        const copy = this.convertPurchasings(purchasing);
        return this.http.post(this.resourceUrl + '/surat/set-surat', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findSurat(id: number): Observable<TandaTerima> {
        return this.http.get(this.resourceUrl + '/surat/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateBayar(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.put(this.resourceUrl + '/bayar', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    queryMKT(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/mkt', options)
            .map((res: Response) => this.convertResponse(res))
    }
    changeStatus(tandaTerima: TandaTerima, idstatus: number): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/set-status/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    cek(id: any): Observable<TandaTerima> {
        return this.http.get(this.resourceUrl + '/cek-edit/' + id).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    getinvSPL(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-inv-spl', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-fix-rebate', options)
            .map((res: Response) => this.convertResponse(res));
    }
    cekinv(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/cek-inv-spl', options)
            .map((res: Response) => this.convertResponse(res));
    }
    setlunas(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/set-lunas', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }
    updateTTMKT(tandaTerima: TandaTerima): Observable<TandaTerima> {
        const copy = this.convert(tandaTerima);
        return this.http.post(this.resourceUrl + '/update-tt-mkt', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    setStatus(pay: TandaTerima, idstatus: number): Observable<TandaTerima> {
        const copy = this.convert(pay);
        return this.http.post(this.resourceUrl + '/set-status/' + idstatus, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convertPurchasings(purchasing: TandaTerima[]): TandaTerima[] {
        const copy: TandaTerima[] = Object.assign([], purchasing);
        return copy;
    }
}
