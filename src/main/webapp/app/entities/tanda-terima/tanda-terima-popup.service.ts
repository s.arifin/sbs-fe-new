import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { TandaTerima } from './tanda-terima.model';
import { TandaTerimaService } from './tanda-terima.service';

@Injectable()
export class TandaTerimaPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private tandaTerimaService: TandaTerimaService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.tandaTerimaService.find(id).subscribe((tandaTerima) => {
                    tandaTerima.dtreceipt = this.datePipe
                        .transform(tandaTerima.dtreceipt, 'yyyy-MM-ddTHH:mm:ss');
                    tandaTerima.dtdue = this.datePipe
                        .transform(tandaTerima.dtdue, 'yyyy-MM-ddTHH:mm:ss');
                    tandaTerima.dtpurc = this.datePipe
                        .transform(tandaTerima.dtpurc, 'yyyy-MM-ddTHH:mm:ss');
                    tandaTerima.dtmkt = this.datePipe
                        .transform(tandaTerima.dtmkt, 'yyyy-MM-ddTHH:mm:ss');
                    tandaTerima.dtacc = this.datePipe
                        .transform(tandaTerima.dtacc, 'yyyy-MM-ddTHH:mm:ss');
                    tandaTerima.dtfin = this.datePipe
                        .transform(tandaTerima.dtfin, 'yyyy-MM-ddTHH:mm:ss');
                    tandaTerima.dtgiro = this.datePipe
                        .transform(tandaTerima.dtgiro, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.tandaTerimaModalRef(component, tandaTerima);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.tandaTerimaModalRef(component, new TandaTerima());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    load(component: Component, data?: Object): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.tandaTerimaModalRef(component, new TandaTerima());
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    tandaTerimaModalRef(component: Component, tandaTerima: TandaTerima): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tandaTerima = tandaTerima;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
