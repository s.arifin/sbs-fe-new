import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiParseLinks, JhiAlertService, JhiEventManager, JhiPaginationUtil, JhiDateUtils } from 'ng-jhipster';
import { ConfirmationService } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { LoadingService } from '../../layouts';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Invoice, InvoiceService } from '../invoice';
import { InvoiceItem, InvoiceItemService } from '../invoice-item';
import { MasterBank, MasterBankService } from '../master-bank';
import { Payment, PaymentService } from '../payment';
import { PaymentItemService } from '../payment-item';
import { MasterSupplier, TandaTerima } from './tanda-terima.model';
import { TandaTerimaService } from './tanda-terima.service';

@Component({
    selector: 'jhi-tanda-terima-bayar',
    templateUrl: './tanda-terima-bayar.component.html'
})
export class TandaTerimaBayarComponent implements OnInit, OnDestroy {

    currentAccount: any;
    tanter: TandaTerima;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    dtpay: Date;
    dtrelease: Date;
    private subscription: Subscription;
    isview: boolean;
    selectedBank: MasterBank;
    banks: MasterBank[];
    totalpaid: number;
    newSuplier: MasterSupplier;
    nott: string;
    hideColumn: boolean;
    constructor(
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private reportUtilService: ReportUtilService,
        private confirmationService: ConfirmationService,
        private loadingService: LoadingService,
        private route: ActivatedRoute,
        private dateUtils: JhiDateUtils,
        private masterBankService: MasterBankService,
        private toasterService: ToasterService,
        private tandaTerimaService: TandaTerimaService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.tanter = new TandaTerima();
        this.dtpay = new Date();
        this.dtrelease = new Date();
        this.isview = false;
        this.selectedBank = new MasterBank();
        this.banks = new Array<MasterBank>();
        this.totalpaid = 0;
        this.newSuplier = new MasterSupplier();
        this.nott = '';
        this.hideColumn = false;
    }

    load(id) {
        this.tandaTerimaService.find(id).subscribe(
            (res: TandaTerima) => {
                this.tanter = res;
                this.newSuplier = res.mastersupplier;
                this.nott = res.receiptno;
                if (res.dtrelease === null) {
                    this.dtrelease = new Date();
                }
                if (res.dtrelease !== null) {
                    this.dtrelease = this.dateUtils
                        .convertDateTimeFromServer(res.dtrelease);
                }
                // if (this.currentAccount.idInterna === 'FIN') {
                //     this.dtrelease = this.dateUtils
                //         .convertDateTimeFromServer(res.dtfin);
                // }
                // if (this.currentAccount.idInterna === 'ACC') {
                //     this.dtrelease = this.dateUtils
                //     .convertDateTimeFromServer(res.dtacc);
                // }
                // if (this.currentAccount.idInterna === 'MKT') {
                //     this.dtrelease = this.dateUtils
                //     .convertDateTimeFromServer(res.dtmkt);
                // }

                if (res.dtpay === null) {
                    this.dtpay = new Date();
                }
                if (res.dtpay !== null) {
                    this.dtpay = this.dateUtils
                        .convertDateTimeFromServer(res.dtpay);
                }
                if (res.costreduction === null || res.costreduction === undefined) {
                    res.costreduction = 0;
                }
                this.tanter.totalpaid = res.total - res.costreduction;
            }
        )
    }
    ngOnInit() {
        this.masterBankService.queryComboBox()
            .subscribe(
                (res: ResponseWrapper) => {
                    this.banks = res.json;
                }
            )
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
            if (params['isview'] === 'true') {
                this.isview = true;
            }
            if (params['isview'] === 'false') {
                this.isview = false;
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            if (this.currentAccount.idInternal !== 'RCP' && this.currentAccount.idInternal !== 'PBL') {
                this.hideColumn = true;
            }
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }
    print(data: Payment) {
        const filter_data = 'idpay:' + data.idpay;
        if (data.valuta !== 'Rp') {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer_usd/pdf', { filterData: filter_data });
        } else {
            this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/payment_voucer/pdf', { filterData: filter_data });
        }
    }
    backMainPage() {
        // this.router.navigate(['/tanda-terima']);
        window.history.back();
    }
    trackNopoItem(index, item: InvoiceItem) {
        return item.nopo;
    }
    updateBayar() {
        this.loadingService.loadingStart();
        this.tanter.dtrelease = this.dtrelease;
        this.tanter.dtpay = this.dtpay;
        this.tandaTerimaService.updateBayar(this.tanter)
            .subscribe(
                (res) => { this.backMainPage(), this.loadingService.loadingStop() },
                (err) => this.loadingService.loadingStop()
            )

    }
    potongan() {
        if (this.tanter.costreduction !== 0 && this.tanter.costreduction !== undefined) {
            this.tanter.totalpaid = this.tanter.total - this.tanter.costreduction;
        }
        if (this.tanter.costreduction === 0 || this.tanter.costreduction === undefined) {
            this.tanter.totalpaid = this.tanter.total - 0;
        }
    }
}
