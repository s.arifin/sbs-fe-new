import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    TandaTerimaService,
    TandaTerimaPopupService,
    TandaTerimaComponent,
    TandaTerimaDetailComponent,
    TandaTerimaDialogComponent,
    TandaTerimaPopupComponent,
    TandaTerimaDeletePopupComponent,
    TandaTerimaDeleteDialogComponent,
    tandaTerimaRoute,
    tandaTerimaPopupRoute,
    TandaTerimaResolvePagingParams,
} from './';
import { TandaTerimaNewComponent } from './tanda-terima-new/tanda-terima-new.component';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    DataListModule,
} from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TandaTerimaAsLovComponent, TandaTerimaLovPopupComponent } from './lov/tanda-terima-as-lov.component';
import { TandaTerimaNewSuratComponent } from './tanda-terima-new/tanda-terima-new-surat.component';
import { TandaTerimaSuratComponent } from './tanda-terima-surat.component';
import { TandaTerimaBayarComponent } from './tanda-terima-bayar.component';
import { TandaTerimaItemComponent } from './tanda-terima-item.component';
import { FinTandaTerimaComponent } from '../payment/payment-fin/fin-tanda-terima.component';
import { MpmPaymentModule } from '../payment/payment.module';
const ENTITY_STATES = [
    ...tandaTerimaRoute,
    ...tandaTerimaPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        DataListModule,
        MpmPaymentModule
    ],
    declarations: [
        TandaTerimaComponent,
        TandaTerimaDetailComponent,
        TandaTerimaDialogComponent,
        TandaTerimaDeleteDialogComponent,
        TandaTerimaPopupComponent,
        TandaTerimaDeletePopupComponent,
        TandaTerimaNewComponent,
        TandaTerimaLovPopupComponent,
        TandaTerimaAsLovComponent,
        TandaTerimaNewSuratComponent,
        TandaTerimaSuratComponent,
        TandaTerimaBayarComponent,
        TandaTerimaItemComponent
    ],
    entryComponents: [
        TandaTerimaComponent,
        TandaTerimaDialogComponent,
        TandaTerimaPopupComponent,
        TandaTerimaDeleteDialogComponent,
        TandaTerimaDeletePopupComponent,
        TandaTerimaNewComponent,
        TandaTerimaLovPopupComponent,
        TandaTerimaAsLovComponent,
        TandaTerimaSuratComponent,
        TandaTerimaBayarComponent,
        TandaTerimaItemComponent
    ],
    providers: [
        TandaTerimaService,
        TandaTerimaPopupService,
        TandaTerimaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmTandaTerimaModule {}
