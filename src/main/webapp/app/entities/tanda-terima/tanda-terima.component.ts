import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { TandaTerima } from './tanda-terima.model';
import { TandaTerimaService } from './tanda-terima.service';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ConfirmationService, DataList, LazyLoadEvent } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { MasterSupplier, MasterSupplierService } from '../master-supplier';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-tanda-terima',
    templateUrl: './tanda-terima.component.html'
})
export class TandaTerimaComponent implements OnInit, OnDestroy {

    @Input() div: String
    currentAccount: Account;
    tandaTerimas: TandaTerima[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredSuppliers: any[];
    filteredCodeSuppliers: any[];
    newSuplier: MasterSupplier;
    kdsup: string;
    dtfrom: Date;
    dtthru: Date;
    hideColumn: boolean;
    idInternal: string;
    modalNoteCancel: boolean;
    terimaTanda: TandaTerima;
    noteCancel: string;
    constructor(
        private tandaTerimaService: TandaTerimaService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private reportUtilService: ReportUtilService,
        private masterSupplierService: MasterSupplierService,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.newSuplier = new MasterSupplier();
        this.kdsup = '';
        this.dtfrom = new Date();
        this.dtthru = new Date();
        this.hideColumn = false;
        this.idInternal = '';
        this.modalNoteCancel = false;
        this.terimaTanda = new TandaTerima();
        this.noteCancel = '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        // if (this.currentSearch) {
        //     this.tandaTerimaService.search({
        //         page: this.page - 1,
        //         query: this.currentSearch,
        //         size: this.itemsPerPage,
        //         sort: this.sort()}).subscribe(
        //             (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //             (res: ResponseWrapper) => this.onError(res.json)
        //         );
        //     return;
        // }
        this.tandaTerimaService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            query: 'cari:' + this.currentSearch + '|kdsup:' + this.kdsup + '|dtfrom:' + this.dtfrom.toISOString().split('T')[0] + '|dtthru:' + this.dtthru.toISOString().split('T')[0]
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/tanda-terima'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/tanda-terima', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/tanda-terima', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account: Account) => {
            this.currentAccount = account;
            this.idInternal = account.idInternal;
            if (this.currentAccount.idInternal !== 'FIN' && this.currentAccount.idInternal !== 'ACC') {
                this.hideColumn = true;
            }
        });
        this.registerChangeInTandaTerimas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TandaTerima) {
        return item.id;
    }
    registerChangeInTandaTerimas() {
        this.eventSubscriber = this.eventManager.subscribe('tandaTerimaListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.tandaTerimas = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    printTT(rowData: TandaTerima) {
        const filter_data = 'idreceipt:' + rowData.idreceipt;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/tandaterima/pdf', { filterData: filter_data });
    }
    filterSupplierSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.nmSupplier.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    filterSupplierCodeSingle(event) {
        const query = event.query;
        this.masterSupplierService.getNewSupplierALL().then((newSupliers) => {
            this.filteredCodeSuppliers = this.filterCodeSupplier(query, newSupliers);
        });
    }

    filterCodeSupplier(query, newSupliers: MasterSupplier[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            // if (newSuplier.kd_suppnew.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
            //     filtered.push(newSuplier);
            // }
            if (newSuplier.kd_suppnew.toLowerCase().includes(query.toLowerCase())) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.kdsup = this.newSuplier.kd_suppnew;
    }
    filter() {
        this.page = 1;
        if (this.kdsup === '' && this.kdsup === undefined && this.newSuplier.kd_suppnew === undefined) {
            this.kdsup = 'all'
        } else {
            this.kdsup = this.newSuplier.kd_suppnew;
        }
        this.loadAll();
    }
    reset() {
        this.loadAll();
        this.kdsup = '';
    }
    setlunas(rowData: TandaTerima) {
        this.tandaTerimaService.setlunas(rowData).subscribe(
            (res) => {
                this.loadAll();
            }
        )
    }

    edittt(idreceipt: any) {
        this.router.navigate(['../tanda-terima-edit/' + idreceipt + '/false']);
        // this.tandaTerimaService.cek(idreceipt)
        //     .subscribe(
        //         (ress) => { this.router.navigate(['../tanda-terima-edit/' + idreceipt + '/false']); },
        //         (error) => {
        //             // this.toasterService.showToaster('info', 'Edit', 'Tanda Terima tidak boleh di edit karena sudah jadi invoice / payment');
        //             this.confirmationService.confirm({
        //                 header: 'WARNING',
        //                 message: 'Tanda Terima sudah di buat Invoice / Payment',
        //                 accept: () => {
        //                     // window.history.back();
        //                 },
        //                 rejectVisible: false,
        //                 // reject: () => {
        //                 //     window.history.back();
        //                 // }
        //             });
        //         }
        //     )
    }
    viewtt(idreceipt: any) {
        // this.tandaTerimaService.cek(idreceipt)
        //     .subscribe(
        //         (ress) => { this.router.navigate(['../tanda-terima-edit/' + idreceipt + '/true']); },
        //         (error) => {
        //             // this.toasterService.showToaster('info', 'Edit', 'Tanda Terima tidak boleh di edit karena sudah jadi invoice / payment');
        //             this.confirmationService.confirm({
        //                 header: 'WARNING',
        //                 message: 'Tanda Terima sudah di buat Invoice / Payment',
        //                 accept: () => {
        //                     // window.history.back();
        //                 },
        //                 rejectVisible: false,
        //                 // reject: () => {
        //                 //     window.history.back();
        //                 // }
        //             });
        //         }
        //     )
        this.router.navigate(['../tanda-terima-edit/' + idreceipt + '/true']);
    }

    cancel(rowData: TandaTerima) {
        this.terimaTanda = rowData;
        this.modalNoteCancel = true;
    }

    modalCancelTT() {
        if (this.noteCancel === '' || this.noteCancel === undefined || this.noteCancel === null) {
            alert('Note Cancel Tidak Boleh Kosong');
        } else if (this.noteCancel.length <= 1) {
            alert('Note Cancel minimal 2 karakter');
        } else {
            this.terimaTanda.cancelnote = this.noteCancel;
            this.confirmationService.confirm({
                header: 'Cancel Tanda Terima',
                message: 'Apakah Anda Yakin Cancel Tanda Terima No  ' + this.terimaTanda.receiptno,
                accept: () => {
                    this.tandaTerimaService.setStatus(this.terimaTanda, 13)
                        .subscribe(
                            (res) => {
                                this.loadingService.loadingStart();
                                this.loadAll();
                                this.modalNoteCancel = false;
                                this.noteCancel = '';
                                this.terimaTanda = new TandaTerima();
                                this.loadingService.loadingStop();
                            }
                        );
                }
            });
        }
    }

    exportExcel() {
        this.loadingService.loadingStart()
        const date = new Date();

        const currentDay = date.getDate();

        const currentMonth = date.getMonth() + 1;

        const currentYear = date.getFullYear();
        const waktu = '23:59:59';
        const dari = this.dtfrom.getFullYear() + '-' + (this.dtfrom.getMonth() + 1) + '-' + this.dtfrom.getDate();
        const sampai = this.dtthru.getFullYear() + '-' + (this.dtthru.getMonth() + 1) + '-' + this.dtthru.getDate() + ' ' + waktu;
        const filter_data = 'dtfrom:' + dari + '|dtthru:' + sampai + '|kdspl:' + this.kdsup + '|cari:' + this.currentSearch;
        this.reportUtilService.downloadFileWithName(`Report Tanda Terima ${currentDay}_${currentMonth}_${currentYear}`, process.env.API_C_URL + '/api/report/rpt_tt/xlsx', { filterData: filter_data });
        this.loadingService.loadingStop();
    }
}
