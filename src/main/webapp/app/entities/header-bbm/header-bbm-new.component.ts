import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { HeaderBBM } from './header-bbm.model';
import { HeaderBBMService } from './header-bbm.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PurchasingService, Purchasing, Suplier } from '../purchasing';
import { LazyLoadEvent } from 'primeng/primeng';
import { PurchasingItemService, PurchasingItem } from '../purchasing-item';
import { DetailBBM } from '../detail-bbm';

@Component({
    selector: 'jhi-header-bbm-new',
    templateUrl: './header-bbm-new.component.html'
})
export class HeaderBBMNewComponent implements OnInit, OnDestroy {

    currentAccount: any;
    headerBBMS: HeaderBBM[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    totalItemspo: any;
    purchasings: Purchasing[];
    newModal: boolean;
    selectedpo: Purchasing;
    po: Purchasing;
    nopo: string;
    nmspl: string;
    kdspl: string;
    headerBBM: HeaderBBM;
    nosj: string;
    dtsj: Date;
    purchasingItems: PurchasingItem[];
    selectedItems: PurchasingItem;
    detailBBMs: DetailBBM[];
    detailBBM: DetailBBM;

    constructor(
        private headerBBMService: HeaderBBMService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private purchasingService: PurchasingService,
        private purchasingItemService: PurchasingItemService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.newModal = false;
        this.nopo = '';
        this.nmspl = '';
        this.kdspl = '';
        this.selectedpo = new Purchasing();
        this.nosj = '';
        this.dtsj = new Date();
        this.purchasingItems = new Array<PurchasingItem>();
        this.selectedItems = new PurchasingItem();
        this.detailBBMs = new Array<DetailBBM>();
        this.detailBBM = new DetailBBM();
        this.headerBBM = new HeaderBBM();
    }

    loadAll() {
        // if (this.currentSearch) {
        //     this.headerBBMService.search({
        //         page: this.page - 1,
        //         query: this.currentSearch,
        //         size: this.itemsPerPage,
        //         sort: this.sort()
        //     }).subscribe(
        //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //         (res: ResponseWrapper) => this.onError(res.json)
        //     );
        //     return;
        // }
        // this.headerBBMService.query({
        //     page: this.page - 1,
        //     size: this.itemsPerPage,
        //     sort: this.sort()
        // }).subscribe(
        //     (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/header-bbm'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.purchasingService.queryPOBBM().subscribe(
            (res: ResponseWrapper) => { this.onSuccesspo(res.json, res.headers); this.newModal = true },
        )
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/header-bbm', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInHeaderBBMS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: HeaderBBM) {
        return item.id;
    }
    registerChangeInHeaderBBMS() {
        this.eventSubscriber = this.eventManager.subscribe('headerBBMListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.headerBBMS = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private onSuccesspo(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemspo = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.purchasings = data;
    }
    private onErrorpo(error) {
        this.alertService.error(error.message, null, null);
    }

    cariPO() {
        this.purchasingService.queryPOBBM().subscribe(
            (res: ResponseWrapper) => { this.onSuccesspo(res.json, res.headers); this.newModal = true },
        )
    }

    cariPOSearch(search) {
        if (this.currentSearch) {
            this.purchasingService.queryPOBBM({
                query: this.currentSearch
            }).subscribe(
                (res: ResponseWrapper) => { this.onSuccesspo(res.json, res.headers); this.newModal = true },
            );
            return;
        }
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    pilihPO() {
        this.newModal = false;
        console.log('ini data selectedPO = ', this.selectedpo);
        this.nopo = this.selectedpo.nopurchasing;
        this.kdspl = this.selectedpo.supliercode;
        this.nmspl = this.selectedpo.suplier.nm_supplier;
        this.purchasingItemService.findByIdPurchasing({
            idPurchasing: this.selectedpo.idpurchasing
        }).subscribe(
            (res: ResponseWrapper) => this.purchasingItems = res.json
        )
    }

    onRowSelect(event) {
        this.detailBBM = new DetailBBM();
        this.detailBBM.kd_bahan = event.data.productcode;
        this.detailBBM.nm_bahan = event.data.productname;
        this.detailBBM.qty = event.data.qty;
        this.detailBBM.qty_sisa = event.data.qty - event.data.qty_kirim;
        this.detailBBM.satuan = event.data.uom;
        this.detailBBM.idpurchasingitem = event.data.idpurchasingitem;
        this.detailBBMs = [... this.detailBBMs, this.detailBBM];
    }

    createBBM() {
        this.headerBBM.no_po = this.nopo;
        this.headerBBM.kd_supplier =  this.kdspl;
        // this.headerBBM. this.nmspl
        this.headerBBM.no_sj_supplier = this.nosj
        if (this.dtsj != null && this.dtsj !== undefined) {
            this.headerBBM.tgl_sj_supplier = this.dtsj
        }
        this.headerBBM.detailBbm = this.detailBBMs;
        this.headerBBMService.create(this.headerBBM).subscribe(
            (res) => console.log('cek data save = ', res)
        )
    }
    backMainPage() { }

    validqtymasuk(detail: DetailBBM) {
        if (detail.qty_sisa < detail.qty_masuk) {
            console.log('qty masuk = ', detail.qty_masuk);
            console.log('qty sisa = ', detail.qty_sisa);
            alert('Qty masuk tidak boleh melebihin QTY sisa');
            detail.qty_masuk = 0;
        }
    }
}
