import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    HeaderBBMService,
    HeaderBBMPopupService,
    HeaderBBMComponent,
    HeaderBBMDetailComponent,
    HeaderBBMDialogComponent,
    HeaderBBMPopupComponent,
    HeaderBBMDeletePopupComponent,
    HeaderBBMDeleteDialogComponent,
    headerBBMRoute,
    headerBBMPopupRoute,
    HeaderBBMResolvePagingParams,
} from './';
import {
    DataTableModule,
    DialogModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
} from 'primeng/primeng';
import { HeaderBBMNewComponent } from './header-bbm-new.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';

const ENTITY_STATES = [
    ...headerBBMRoute,
    ...headerBBMPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule
    ],
    declarations: [
        HeaderBBMComponent,
        HeaderBBMDetailComponent,
        HeaderBBMDialogComponent,
        HeaderBBMDeleteDialogComponent,
        HeaderBBMPopupComponent,
        HeaderBBMDeletePopupComponent,
        HeaderBBMNewComponent
    ],
    entryComponents: [
        HeaderBBMComponent,
        HeaderBBMDialogComponent,
        HeaderBBMPopupComponent,
        HeaderBBMDeleteDialogComponent,
        HeaderBBMDeletePopupComponent,
        HeaderBBMNewComponent
    ],
    providers: [
        HeaderBBMService,
        HeaderBBMPopupService,
        HeaderBBMResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmHeaderBBMModule {}
