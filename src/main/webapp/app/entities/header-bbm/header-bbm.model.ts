import { BaseEntity } from './../../shared';
import { DetailBBM } from '../detail-bbm';

export class HeaderBBM implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbm?: string,
        public tgl_bbm?: any,
        public kd_supplier?: string,
        public no_sj_supplier?: string,
        public tgl_sj_supplier?: Date,
        public iduser?: string,
        public tgl_input?: any,
        public jam_input?: any,
        public no_batch?: string,
        public kd_div?: string,
        public kd_group?: string,
        public kd_produk?: string,
        public no_ret?: string,
        public tgl_ret?: any,
        public ket_pakai?: string,
        public id_app?: string,
        public tg_app?: any,
        public jm_app?: any,
        public no_po?: string,
        public detailBbm?: DetailBBM[]
    ) {
        this.detailBbm = new Array<DetailBBM>();
    }
}
