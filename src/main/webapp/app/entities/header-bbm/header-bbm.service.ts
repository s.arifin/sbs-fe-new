import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { HeaderBBM } from './header-bbm.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class HeaderBBMService {

    private resourceUrl = process.env.API_C_URL + '/api/header-bbms';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/header-bbms';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(headerBBM: HeaderBBM): Observable<HeaderBBM> {
        const copy = this.convert(headerBBM);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(headerBBM: HeaderBBM): Observable<HeaderBBM> {
        const copy = this.convert(headerBBM);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<HeaderBBM> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.tgl_bbm = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_bbm);
        entity.tgl_sj_supplier = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_sj_supplier);
        entity.tgl_input = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_input);
        entity.jam_input = this.dateUtils
            .convertDateTimeFromServer(entity.jam_input);
    }

    private convert(headerBBM: HeaderBBM): HeaderBBM {
        const copy: HeaderBBM = Object.assign({}, headerBBM);

        // copy.tgl_bbm = this.dateUtils.toDate(headerBBM.tgl_bbm);

        // copy.tgl_sj_supplier = this.dateUtils.toDate(headerBBM.tgl_sj_supplier);

        // copy.tgl_input = this.dateUtils.toDate(headerBBM.tgl_input);

        // copy.jam_input = this.dateUtils.toDate(headerBBM.jam_input);
        return copy;
    }
}
