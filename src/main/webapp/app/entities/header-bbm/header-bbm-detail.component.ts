import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { HeaderBBM } from './header-bbm.model';
import { HeaderBBMService } from './header-bbm.service';

@Component({
    selector: 'jhi-header-bbm-detail',
    templateUrl: './header-bbm-detail.component.html'
})
export class HeaderBBMDetailComponent implements OnInit, OnDestroy {

    headerBBM: HeaderBBM;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private headerBBMService: HeaderBBMService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHeaderBBMS();
    }

    load(id) {
        this.headerBBMService.find(id).subscribe((headerBBM) => {
            this.headerBBM = headerBBM;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHeaderBBMS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'headerBBMListModification',
            (response) => this.load(this.headerBBM.id)
        );
    }
}
