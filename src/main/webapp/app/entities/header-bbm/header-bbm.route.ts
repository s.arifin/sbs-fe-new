import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HeaderBBMComponent } from './header-bbm.component';
import { HeaderBBMDetailComponent } from './header-bbm-detail.component';
import { HeaderBBMPopupComponent } from './header-bbm-dialog.component';
import { HeaderBBMDeletePopupComponent } from './header-bbm-delete-dialog.component';
import { HeaderBBMNewComponent } from './header-bbm-new.component';

@Injectable()
export class HeaderBBMResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const headerBBMRoute: Routes = [
    {
        path: 'header-bbm',
        component: HeaderBBMComponent,
        resolve: {
            'pagingParams': HeaderBBMResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerBBM.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'header-bbm/:id',
        component: HeaderBBMDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerBBM.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'header-bbm-new',
        component: HeaderBBMNewComponent,
        resolve: {
            'pagingParams': HeaderBBMResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerBBM.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const headerBBMPopupRoute: Routes = [
    {
        path: 'header-bbm-new',
        component: HeaderBBMPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerBBM.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'header-bbm/:id/edit',
        component: HeaderBBMPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerBBM.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'header-bbm/:id/delete',
        component: HeaderBBMDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerBBM.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
