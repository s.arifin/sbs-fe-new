import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { HeaderBBM } from './header-bbm.model';
import { HeaderBBMPopupService } from './header-bbm-popup.service';
import { HeaderBBMService } from './header-bbm.service';

@Component({
    selector: 'jhi-header-bbm-dialog',
    templateUrl: './header-bbm-dialog.component.html'
})
export class HeaderBBMDialogComponent implements OnInit {

    headerBBM: HeaderBBM;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private headerBBMService: HeaderBBMService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.headerBBM.id !== undefined) {
            this.subscribeToSaveResponse(
                this.headerBBMService.update(this.headerBBM));
        } else {
            this.subscribeToSaveResponse(
                this.headerBBMService.create(this.headerBBM));
        }
    }

    private subscribeToSaveResponse(result: Observable<HeaderBBM>) {
        result.subscribe((res: HeaderBBM) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: HeaderBBM) {
        this.eventManager.broadcast({ name: 'headerBBMListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-header-bbm-popup',
    template: ''
})
export class HeaderBBMPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private headerBBMPopupService: HeaderBBMPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.headerBBMPopupService
                    .open(HeaderBBMDialogComponent as Component, params['id']);
            } else {
                this.headerBBMPopupService
                    .open(HeaderBBMDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
