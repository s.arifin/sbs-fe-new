import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { HeaderBBM } from './header-bbm.model';
import { HeaderBBMService } from './header-bbm.service';

@Injectable()
export class HeaderBBMPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private headerBBMService: HeaderBBMService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.headerBBMService.find(id).subscribe((headerBBM) => {
                    headerBBM.tgl_bbm = this.datePipe
                        .transform(headerBBM.tgl_bbm, 'yyyy-MM-ddTHH:mm:ss');
                    // headerBBM.tgl_sj_supplier = this.datePipe
                    //     .transform(headerBBM.tgl_sj_supplier, 'yyyy-MM-ddTHH:mm:ss');
                    headerBBM.tgl_input = this.datePipe
                        .transform(headerBBM.tgl_input, 'yyyy-MM-ddTHH:mm:ss');
                    headerBBM.jam_input = this.datePipe
                        .transform(headerBBM.jam_input, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.headerBBMModalRef(component, headerBBM);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.headerBBMModalRef(component, new HeaderBBM());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    headerBBMModalRef(component: Component, headerBBM: HeaderBBM): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.headerBBM = headerBBM;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
