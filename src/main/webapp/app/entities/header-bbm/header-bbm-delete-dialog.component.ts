import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HeaderBBM } from './header-bbm.model';
import { HeaderBBMPopupService } from './header-bbm-popup.service';
import { HeaderBBMService } from './header-bbm.service';

@Component({
    selector: 'jhi-header-bbm-delete-dialog',
    templateUrl: './header-bbm-delete-dialog.component.html'
})
export class HeaderBBMDeleteDialogComponent {

    headerBBM: HeaderBBM;

    constructor(
        private headerBBMService: HeaderBBMService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.headerBBMService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'headerBBMListModification',
                content: 'Deleted an headerBBM'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-header-bbm-delete-popup',
    template: ''
})
export class HeaderBBMDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private headerBBMPopupService: HeaderBBMPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.headerBBMPopupService
                .open(HeaderBBMDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
