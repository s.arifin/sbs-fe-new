import { TandaTerimaDocument } from '../tanda-terima-document';
import { BaseEntity } from '../../shared';

export class DetailTandaTerimaSKG implements BaseEntity {
    constructor(
        public id?: number,
        public iddetreceipt?: any,
        public idreceipt?: any,
        public invno?: string,
        public dtinv?: any,
        public invtaxno?: string,
        public dtinvtax?: any,
        public total?: number,
        public notes?: string,
        public ntotal?: number,
        public nourut?: number,
        public cust_name?: string,
        public dtreceipt?: Date,
        public receiptno?: string,
        public kdsupplier?: string,
        public document?: TandaTerimaDocument,
    ) {
        this.document = new TandaTerimaDocument();
    }
}
