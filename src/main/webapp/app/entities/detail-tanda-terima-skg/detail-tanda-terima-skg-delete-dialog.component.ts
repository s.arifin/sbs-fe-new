import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DetailTandaTerimaSKG } from './detail-tanda-terima-skg.model';
import { DetailTandaTerimaSKGPopupService } from './detail-tanda-terima-skg-popup.service';
import { DetailTandaTerimaSKGService } from './detail-tanda-terima-skg.service';

@Component({
    selector: 'jhi-detail-tanda-terima-skg-delete-dialog',
    templateUrl: './detail-tanda-terima-skg-delete-dialog.component.html'
})
export class DetailTandaTerimaSKGDeleteDialogComponent {

    detailTandaTerima: DetailTandaTerimaSKG;

    constructor(
        private detailTandaTerimaService: DetailTandaTerimaSKGService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.detailTandaTerimaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'detailTandaTerimaListModification',
                content: 'Deleted an detailTandaTerima'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-detail-tanda-terima-skg-delete-popup',
    template: ''
})
export class DetailTandaTerimaSKGDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailTandaTerimaPopupService: DetailTandaTerimaSKGPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.detailTandaTerimaPopupService
                .open(DetailTandaTerimaSKGDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
