export * from './detail-tanda-terima-skg.model';
export * from './detail-tanda-terima-skg-popup.service';
export * from './detail-tanda-terima-skg.service';
export * from './detail-tanda-terima-skg-dialog.component';
export * from './detail-tanda-terima-skg-delete-dialog.component';
export * from './detail-tanda-terima-skg-detail.component';
export * from './detail-tanda-terima-skg.component';
export * from './detail-tanda-terima-skg.route';
