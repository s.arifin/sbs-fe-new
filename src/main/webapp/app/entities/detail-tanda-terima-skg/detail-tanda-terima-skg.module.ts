import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    DetailTandaTerimaSKGService,
    DetailTandaTerimaSKGPopupService,
    DetailTandaTerimaSKGComponent,
    DetailTandaTerimaSKGDetailComponent,
    DetailTandaTerimaSKGDialogComponent,
    DetailTandaTerimaSKGPopupComponent,
    DetailTandaTerimaSKGDeletePopupComponent,
    DetailTandaTerimaSKGDeleteDialogComponent,
    detailTandaTerimaSKGRoute,
    detailTandaTerimaSKGPopupRoute,
    DetailTandaTerimaSKGResolvePagingParams,
} from '.';

const ENTITY_STATES = [
    ...detailTandaTerimaSKGRoute,
    ...detailTandaTerimaSKGPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DetailTandaTerimaSKGComponent,
        DetailTandaTerimaSKGDetailComponent,
        DetailTandaTerimaSKGDialogComponent,
        DetailTandaTerimaSKGDeleteDialogComponent,
        DetailTandaTerimaSKGPopupComponent,
        DetailTandaTerimaSKGDeletePopupComponent,
    ],
    entryComponents: [
        DetailTandaTerimaSKGComponent,
        DetailTandaTerimaSKGDialogComponent,
        DetailTandaTerimaSKGPopupComponent,
        DetailTandaTerimaSKGDeleteDialogComponent,
        DetailTandaTerimaSKGDeletePopupComponent,
    ],
    providers: [
        DetailTandaTerimaSKGService,
        DetailTandaTerimaSKGPopupService,
        DetailTandaTerimaSKGResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDetailTandaTerimaSKGModule {}
