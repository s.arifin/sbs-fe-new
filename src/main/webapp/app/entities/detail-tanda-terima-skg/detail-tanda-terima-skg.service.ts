import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DetailTandaTerimaSKG } from './detail-tanda-terima-skg.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DetailTandaTerimaSKGService {

    private resourceUrl =  process.env.API_C_URL + '/api/detail-tanda-terimas-skg';
    private resourceSearchUrl =  process.env.API_C_URL + '/api/_search/detail-tanda-terimas-skg';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(detailTandaTerima: DetailTandaTerimaSKG): Observable<DetailTandaTerimaSKG> {
        const copy = this.convert(detailTandaTerima);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(detailTandaTerima: DetailTandaTerimaSKG): Observable<DetailTandaTerimaSKG> {
        const copy = this.convert(detailTandaTerima);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<DetailTandaTerimaSKG> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtinv = this.dateUtils
            .convertDateTimeFromServer(entity.dtinv);
        entity.dtinvtax = this.dateUtils
            .convertDateTimeFromServer(entity.dtinvtax);
    }

    private convert(detailTandaTerima: DetailTandaTerimaSKG): DetailTandaTerimaSKG {
        const copy: DetailTandaTerimaSKG = Object.assign({}, detailTandaTerima);

        copy.dtinv = this.dateUtils.toDate(detailTandaTerima.dtinv);

        copy.dtinvtax = this.dateUtils.toDate(detailTandaTerima.dtinvtax);
        return copy;
    }
}
