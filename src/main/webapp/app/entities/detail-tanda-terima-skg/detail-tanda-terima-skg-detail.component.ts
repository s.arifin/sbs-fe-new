import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DetailTandaTerimaSKG } from './detail-tanda-terima-skg.model';
import { DetailTandaTerimaSKGService } from './detail-tanda-terima-skg.service';

@Component({
    selector: 'jhi-detail-tanda-terima-skg-detail',
    templateUrl: './detail-tanda-terima-skg-detail.component.html'
})
export class DetailTandaTerimaSKGDetailComponent implements OnInit, OnDestroy {

    detailTandaTerima: DetailTandaTerimaSKG;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private detailTandaTerimaService: DetailTandaTerimaSKGService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDetailTandaTerimas();
    }

    load(id) {
        this.detailTandaTerimaService.find(id).subscribe((detailTandaTerima) => {
            this.detailTandaTerima = detailTandaTerima;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDetailTandaTerimas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'detailTandaTerimaListModification',
            (response) => this.load(this.detailTandaTerima.id)
        );
    }
}
