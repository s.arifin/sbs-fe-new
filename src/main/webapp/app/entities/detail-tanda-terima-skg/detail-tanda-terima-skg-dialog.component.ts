import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DetailTandaTerimaSKG } from './detail-tanda-terima-skg.model';
import { DetailTandaTerimaSKGPopupService } from './detail-tanda-terima-skg-popup.service';
import { DetailTandaTerimaSKGService } from './detail-tanda-terima-skg.service';

@Component({
    selector: 'jhi-detail-tanda-terima-skg-dialog',
    templateUrl: './detail-tanda-terima-skg-dialog.component.html'
})
export class DetailTandaTerimaSKGDialogComponent implements OnInit {

    detailTandaTerima: DetailTandaTerimaSKG;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private detailTandaTerimaService: DetailTandaTerimaSKGService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.detailTandaTerima.id !== undefined) {
            this.subscribeToSaveResponse(
                this.detailTandaTerimaService.update(this.detailTandaTerima));
        } else {
            this.subscribeToSaveResponse(
                this.detailTandaTerimaService.create(this.detailTandaTerima));
        }
    }

    private subscribeToSaveResponse(result: Observable<DetailTandaTerimaSKG>) {
        result.subscribe((res: DetailTandaTerimaSKG) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DetailTandaTerimaSKG) {
        this.eventManager.broadcast({ name: 'detailTandaTerimaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-detail-tanda-terima-skg-popup',
    template: ''
})
export class DetailTandaTerimaSKGPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private detailTandaTerimaPopupService: DetailTandaTerimaSKGPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.detailTandaTerimaPopupService
                    .open(DetailTandaTerimaSKGDialogComponent as Component, params['id']);
            } else {
                this.detailTandaTerimaPopupService
                    .open(DetailTandaTerimaSKGDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
