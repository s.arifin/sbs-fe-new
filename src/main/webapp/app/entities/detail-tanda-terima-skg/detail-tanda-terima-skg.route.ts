import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DetailTandaTerimaSKGComponent } from './detail-tanda-terima-skg.component';
import { DetailTandaTerimaSKGDetailComponent } from './detail-tanda-terima-skg-detail.component';
import { DetailTandaTerimaSKGPopupComponent } from './detail-tanda-terima-skg-dialog.component';
import { DetailTandaTerimaSKGDeletePopupComponent } from './detail-tanda-terima-skg-delete-dialog.component';

@Injectable()
export class DetailTandaTerimaSKGResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const detailTandaTerimaSKGRoute: Routes = [
    {
        path: 'detail-tanda-terima-skg',
        component: DetailTandaTerimaSKGComponent,
        resolve: {
            'pagingParams': DetailTandaTerimaSKGResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'detail-tanda-terima-skg/:id',
        component: DetailTandaTerimaSKGDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const detailTandaTerimaSKGPopupRoute: Routes = [
    {
        path: 'detail-tanda-terima-skg-new',
        component: DetailTandaTerimaSKGPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-tanda-terima-skg/:id/edit',
        component: DetailTandaTerimaSKGPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'detail-tanda-terima-skg/:id/delete',
        component: DetailTandaTerimaSKGDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.detailTandaTerima.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
