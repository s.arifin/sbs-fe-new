import { BaseEntity } from '../../shared';

export class PicRole implements BaseEntity {
    constructor(
        public id?: number,
        public idPic?: any,
        public role?: string,
        public div?: string,
        public type?: string,
        public name?: string,
        public dtFrom?: Date,
        public dtThru?: Date,
        public ket?: any
    ) {
    }
}
