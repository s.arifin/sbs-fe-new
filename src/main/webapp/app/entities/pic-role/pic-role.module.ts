import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { PicRoleRoute, PicRoleService, PicRoleResolvePagingParams, PicRoleNewComponent } from '.';
import { PicRoleComponent } from './pic-role.component';
import { DataTableModule, ButtonModule, CalendarModule, ConfirmDialogModule, AutoCompleteModule, RadioButtonModule, TooltipModule, CheckboxModule, DialogModule, InputTextareaModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';

const ENTITY_STATES = [
    ...PicRoleRoute,
    // ...purchasingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule
    ],
    declarations: [
        PicRoleComponent,
        PicRoleNewComponent
    ],
    entryComponents: [
        PicRoleComponent,
        PicRoleNewComponent
    ],
    providers: [
        PicRoleService,
        PicRoleResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPicRoleModule {}
