import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { PicRoleComponent } from './pic-role.component';
import { PicRoleNewComponent } from './pic-role-new.component';

@Injectable()
export class PicRoleResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const PicRoleRoute: Routes = [
    {
        path: 'pic-role',
        component: PicRoleComponent,
        resolve: {
            'pagingParams': PicRoleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pic-role-new',
        component: PicRoleNewComponent,
        resolve: {
            'pagingParams': PicRoleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.picrole.home.createLabel'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const picRolePopupRoute: Routes = [
    // {
    //     path: 'pic-role-new',
    //     component: PicRoleNewComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'Create PIC'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/edit',
    //     component: PurchasingItemPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // },
    // {
    //     path: 'purchasing-item/:id/delete',
    //     component: PurchasingItemDeletePopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.purchasingItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
];
