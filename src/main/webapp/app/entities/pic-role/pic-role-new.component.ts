import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { PicRoleService } from './pic-role.service';
import { PicRole } from './pic-role.model';
import { PurchasingService } from '../purchasing';

@Component({
    selector: 'jhi-pic-role-new',
    templateUrl: './pic-role-new.component.html'
})
export class PicRoleNewComponent implements OnInit {

    picRole: PicRole;
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType:  any;

    divitions: Array<object> = [
        {label : 'Raw Material', value : 'RM'},
        {label : 'Packaging Material', value : 'PM'},
        {label : 'General Item', value : 'GI'},
        {label : 'Sparepart', value : 'S'},
        {label : 'Maintenace', value : 'M'},
        {label : 'Project', value : 'P'},
        {label : 'Jasa', value : 'J'},
        {label : 'Fixed Asets', value : 'F'},
        {label : 'Other', value : 'Other'},
        {label : 'Alat Non Rutin LPM', value : 'AL'},
        {label : 'Glassware Mikro LPM', value : 'GM'},
        {label : 'Glassware FK LPM', value : 'GF'},
        {label : 'Reagent LPM', value : 'RE'},
        {label : 'media LPM', value : 'ME'},
        {label : 'Tool LPM', value : 'TL'},
    ];
    constructor(
        private picRoleServices: PicRoleService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private purchasingService: PurchasingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.picRole = new PicRole();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }
    trackId(index: number, item: PicRole) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    filterpicSingle(event) {
        const query = event.query;
        this.purchasingService.getManagerPurchasing().then((newOtorisasi) => {
            this.filteredpic = this.filterpic(query, newOtorisasi);
        });
    }

    filterpic(query, newpic: Account[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpic.length; i++) {
            const newpics = newpic[i];
            if (newpics.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpics);
            }
        }
        console.log('ini data list manager filtered = ', filtered);
        return filtered;
    }
    public selectpic(isSelect?: boolean): void {
        this.picRole.name = this.newpic.login;
    }

    filterpTypeSingle(event) {
        const query = event.query;
        this.picRoleServices.getPtype().then((newPtype) => {
            this.filteredpType = this.filterpType(query, newPtype);
        });
    }

    filterpType(query, newpType: any[]): any[] {

        const filtered: any[] = [];
        for (let i = 0; i < newpType.length; i++) {
            const newpTypes = newpType[i];
            if (newpTypes.pType.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newpTypes);
            }
        }
        return filtered;
    }
    public selectpType(isSelect?: boolean): void {
        this.picRole.type = this.newpType.pType;
    }
    simpan() {
        const obj = {
            name: this.picRole.name,
            dtFrom: this.picRole.dtFrom,
            type: this.newpType,
        }
        this.picRoleServices.create(obj)
        .subscribe(
            (res) => {this.router.navigate(['/pic-role'])}
        );
    }
    back() {
            this.router.navigate(['/pic-role'])
        }
}
