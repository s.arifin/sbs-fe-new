import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ReturItem } from './retur-item.model';
import { ReturItemService } from './retur-item.service';

@Component({
    selector: 'jhi-retur-item-detail',
    templateUrl: './retur-item-detail.component.html'
})
export class ReturItemDetailComponent implements OnInit, OnDestroy {

    returItem: ReturItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private returItemService: ReturItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReturItems();
    }

    load(id) {
        this.returItemService.find(id).subscribe((returItem) => {
            this.returItem = returItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReturItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'returItemListModification',
            (response) => this.load(this.returItem.id)
        );
    }
}
