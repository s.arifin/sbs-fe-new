import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    ReturItemService,
    ReturItemPopupService,
    ReturItemComponent,
    ReturItemDetailComponent,
    ReturItemDialogComponent,
    ReturItemPopupComponent,
    ReturItemDeletePopupComponent,
    ReturItemDeleteDialogComponent,
    returItemRoute,
    returItemPopupRoute,
    ReturItemResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...returItemRoute,
    ...returItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ReturItemComponent,
        ReturItemDetailComponent,
        ReturItemDialogComponent,
        ReturItemDeleteDialogComponent,
        ReturItemPopupComponent,
        ReturItemDeletePopupComponent,
    ],
    entryComponents: [
        ReturItemComponent,
        ReturItemDialogComponent,
        ReturItemPopupComponent,
        ReturItemDeleteDialogComponent,
        ReturItemDeletePopupComponent,
    ],
    providers: [
        ReturItemService,
        ReturItemPopupService,
        ReturItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmReturItemModule {}
