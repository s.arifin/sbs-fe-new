import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReturItemComponent } from './retur-item.component';
import { ReturItemDetailComponent } from './retur-item-detail.component';
import { ReturItemPopupComponent } from './retur-item-dialog.component';
import { ReturItemDeletePopupComponent } from './retur-item-delete-dialog.component';

@Injectable()
export class ReturItemResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const returItemRoute: Routes = [
    {
        path: 'retur-item',
        component: ReturItemComponent,
        resolve: {
            'pagingParams': ReturItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'retur-item/:id',
        component: ReturItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const returItemPopupRoute: Routes = [
    {
        path: 'retur-item-new',
        component: ReturItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'retur-item/:id/edit',
        component: ReturItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'retur-item/:id/delete',
        component: ReturItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
