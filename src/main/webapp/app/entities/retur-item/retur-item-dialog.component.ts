import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReturItem } from './retur-item.model';
import { ReturItemPopupService } from './retur-item-popup.service';
import { ReturItemService } from './retur-item.service';

@Component({
    selector: 'jhi-retur-item-dialog',
    templateUrl: './retur-item-dialog.component.html'
})
export class ReturItemDialogComponent implements OnInit {

    returItem: ReturItem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private returItemService: ReturItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.returItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.returItemService.update(this.returItem));
        } else {
            this.subscribeToSaveResponse(
                this.returItemService.create(this.returItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<ReturItem>) {
        result.subscribe((res: ReturItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ReturItem) {
        this.eventManager.broadcast({ name: 'returItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-retur-item-popup',
    template: ''
})
export class ReturItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returItemPopupService: ReturItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.returItemPopupService
                    .open(ReturItemDialogComponent as Component, params['id']);
            } else {
                this.returItemPopupService
                    .open(ReturItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
