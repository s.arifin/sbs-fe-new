import { BaseEntity } from './../../shared';

export class ReturItem implements BaseEntity {
    constructor(
        public id?: number,
        public idreturitem?: number,
        public idretur?: number,
        public productcode?: string,
        public productname?: string,
        public uom?: string,
        public qty?: number,
        public price?: number,
        public totalprice?: number,
        public nobbk?: string,
        public div?: string,
        public nolhp?: string,
        public jenis?: string,
        public nobatch?: string,
        public kodeproduksi?: string,
        public dtproduksi?: Date,
        public nopalet?: string,
        public jmlpalet?: number,
        public nosj?: string,
        public dtsj?: Date,
        public jmlsj?: number,
        public nopo?: string,
        public dtpo?: Date,
        public Ketidaksesuaian?: string,
        public pic?: string,
        public suppcode?: string,
        public catatan?: string,
        public qty_sj?: number,
        public qty_lu?: number,
        public qty_tlu?: number,
        public qty_bbm?: number,
        public is_bbk?: boolean,
    ) {
    }
}
