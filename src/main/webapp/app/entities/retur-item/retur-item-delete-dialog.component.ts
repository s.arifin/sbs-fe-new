import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ReturItem } from './retur-item.model';
import { ReturItemPopupService } from './retur-item-popup.service';
import { ReturItemService } from './retur-item.service';

@Component({
    selector: 'jhi-retur-item-delete-dialog',
    templateUrl: './retur-item-delete-dialog.component.html'
})
export class ReturItemDeleteDialogComponent {

    returItem: ReturItem;

    constructor(
        private returItemService: ReturItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.returItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'returItemListModification',
                content: 'Deleted an returItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-retur-item-delete-popup',
    template: ''
})
export class ReturItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returItemPopupService: ReturItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.returItemPopupService
                .open(ReturItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
