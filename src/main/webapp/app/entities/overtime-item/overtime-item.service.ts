import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OvertimeItem, BillingItem } from './overtime-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Overtime } from '../overtime/overtime.model';

@Injectable()
export class OvertimeItemService {
    protected itemValues: OvertimeItem[];
    values: Subject<any> = new Subject<any>();
    protected resourceUrl =  process.env.API_C_URL + '/api/overtimeitems';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/overtimeitems';

    constructor(protected http: Http) { }

    create(purchaseOrderItem: OvertimeItem): Observable<OvertimeItem> {
        const copy = this.convert(purchaseOrderItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    saveEdit(purchaseOrderItem: OvertimeItem): Observable<OvertimeItem> {
        const copy = this.convert(purchaseOrderItem);
        return this.http.post(this.resourceUrl + '/saveEditItem', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(purchaseOrderItem: OvertimeItem): Observable<OvertimeItem> {
        const copy = this.convert(purchaseOrderItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    findItem(id: any): Observable<OvertimeItem> {
        return this.http.get(`${this.resourceUrl}/${id}/findOneItem`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<OvertimeItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/findItem', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findBillingForTaxList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/req-faktur-pajak', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<OvertimeItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<OvertimeItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PurchaseOrderItem.
     */
    protected convertItemFromServer(json: any): OvertimeItem {
        const entity: OvertimeItem = Object.assign(new OvertimeItem(), json);
        return entity;
    }

    /**
     * Convert a PurchaseOrderItem to a JSON which can be sent to the server.
     */
    protected convert(purchaseOrderItem: OvertimeItem): OvertimeItem {
        if (purchaseOrderItem === null || purchaseOrderItem === {}) {
            return {};
        }
        // const copy: BillingItem = Object.assign({}, billingItem);
        const copy: OvertimeItem = JSON.parse(JSON.stringify(purchaseOrderItem));
        return copy;
    }

    protected convertList(purchaseOrderItem: OvertimeItem[]): OvertimeItem[] {
        const copy: OvertimeItem[] = purchaseOrderItem;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: OvertimeItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

}
