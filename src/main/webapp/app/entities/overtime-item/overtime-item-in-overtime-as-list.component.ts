import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { OvertimeItemService } from './overtime-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { PurchaseOrderItemOtPopupService } from './purchase-order-item-ot-popup.service';
import { PurchaseOrderService, PurchaseOrder, CustomSupplyer, MasterBahan, MasterBarang } from '../purchase-order';
import { CommonUtilService } from '../../shared';
import * as _ from 'lodash';
import { InternalService } from '../internal';
import { OvertimeItem } from './overtime-item.model';
import { SpawnSyncOptions } from 'child_process';
import { OvertimeService } from '../overtime/overtime.service';
import { Overtime } from '../overtime/overtime.model';
import { PersonalInfo } from '../master_bahan/master-bahan.model';

@Component({
    selector: 'jhi-overtime-item-in-overtime-as-list',
    templateUrl: './overtime-item-in-overtime-as-list.component.html'
})
export class OvertimeItemInOvertimeAsListComponent implements OnInit, OnDestroy, OnChanges {

    dtOrder: Date;
    @Input() idPurchaseOrder: any;
    @Input() peruntukan: any;
    @Input() idPurOrdItem: any;
    @Input() idstatus: any;
    @Input() usrLogin: any;
    purchaseOrderItem: OvertimeItem;
    isSaving: boolean;
    itemType: number;
    puechaseOrders: PurchaseOrder[];
    currentAccount: any;
    purchaseOrderItems: OvertimeItem[];
    purchaseOrder: Overtime;
    purchaseOrderItemEdit: OvertimeItem;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    newModal: boolean;
    newModalEdit: boolean;
    routeData: any;
    currentSearch: string;
    isOpen: Boolean = false;
    index: number;
    loc: string;

    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputOfferNumber: any;
    inputSupplyerName: string;
    inputSupplyerNameEdit: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: any;
    inputRemainingStock: any;
    inputRemainingPo: any;
    unitChecks: PurchaseOrder[];
    unitChecksCopy: PurchaseOrder[];
    hasilSave: PurchaseOrder;
    supplier: CustomSupplyer;
    Supplyer: CustomSupplyer[];
    selectedSupplier: any;
    listSupplier = [{label: 'Please Select or Insert', value: null}];

    satHarga: Array<object> = [
        {label : 'RUPIAH - IDR', value : 'IDR'},
        {label : 'DOLAR AS - USD', value : 'USD'},
        {label : 'EURO - EUR', value : 'EUR'},
        {label : 'DOLAR SINGAPURA - SGD', value : 'SGD'},
        {label : 'DOLAR AUSTRALIA - AUD', value : 'AUD'},
        {label : 'YEN - JPY', value : 'JPY'},
        {label : 'POUND - GBP', value : 'GBP'},
    ];

    inputSatHarga: any;
    public barang: MasterBahan;
    public Barang: MasterBahan[];
    public selectedBarang: any;
    public listBarang = [{label: 'Please Select or Insert', value: null}];

    public kdbarang: MasterBahan;
    public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{label: 'Please Select or Insert', value: null}];

    newSuplier: any;
    newSuplierEdit: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];

    newKdBarang: any;
    newKdBarangs: any[];
    filteredKdBarang: any[];
    pilihKdBarang: any;
    internal: any;
    pilihTipe: any;
    newProj: any;
    newProjs: any[];
    listProj = [{}];
    picBarang: any;
    inputSatuan: any;
    created: any;
    currentDiusul: any;
    currentMGR: string;
    currentKBG: string;
    curAccount: string;
    inputDtRequest: any;
    noUrut: any;

    dtSentEdit: any;
    dtOfferEdit: any;
    nik: any;
    name: any;
    jabatan: any;
    planDtstart: any;
    planDtFinish: any;
    planTotal: any;
    realDtstart: any;
    realDtFinish: any;
    realTotal: any;
    overTimeNow: any;
    overTimeSdNow: any;
    overTimePersen: any;
    catatan: any;

    dtSentEditEdt: any;
    dtOfferEditEdt: any;
    nikEdt: any;
    nameEdt: any;
    jabatanEdt: any;
    planDtstartEdt: any;
    planDtFinishEdt: any;
    planTotalEdt: any;
    realDtstartEdt: any;
    realDtFinishEdt: any;
    realTotalEdt: any;
    overTimeNowEdt: any;
    overTimeSdNowEdt: any;
    overTimePersenEdt: any;
    catatanEdt: any;

    newPersonal: any;
    newPersonals: any[];
    filteredPersonal: any[];
    public personal: PersonalInfo;
    public Personal: PersonalInfo[];
    public selectedPersonal: any;
    public listPersonal = [{label: 'Please Select or Insert', value: null}];

    tmp: any;
    dari: any;
    smp: any;

    constructor(
        protected purchaseOrderService: PurchaseOrderService,
        protected overtimeService: OvertimeService,
        protected purchaseOrderItemService: OvertimeItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected paginationUtil: JhiPaginationUtil,
        protected modalService: NgbModal,
        protected paginationConfig: PaginationConfig,
        protected loadingService: LoadingService,

        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected toaster: ToasterService,
        protected commontUtilService: CommonUtilService,
        protected internalService: InternalService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'noUrut';
        this.reverse = 'asc';
        this.purchaseOrder = new PurchaseOrder();
        this.newModal = false;
        this.newModalEdit = false;
        this.purchaseOrderItem = new OvertimeItem();
        this.purchaseOrderItemEdit = new OvertimeItem();
        // this.dtSentEdit = new Date;
        // this.dtOfferEdit = new Date;
        this.purchaseOrder = new Overtime();
        this.dtOrder = new Date();
        this.barang = new MasterBahan();
        this.isSelectSupplier = false;
        this.kdbarang = new MasterBahan();
        this.inputSatuan = null;
        this.purchaseOrder.createdby = null;
        this.inputDtRequest = new Date;
        this.personal = new PersonalInfo();
        this.noUrut = 0;
        this.planDtstartEdt = null;
        this.planDtFinishEdt = null;
        this.realDtstartEdt = null;
        this.realDtFinishEdt = null;
    }
    protected resetMe() {
        this.selectedBarang = null;
        this.curAccount = null;
    }

    filterBarangSingle(event) {
        // console.log('data Tipe barang : ', this.pilihTipe);
        const query = event.query;
        this.overtimeService.getNewBarang().then((newPersonals) => {
            this.filteredPersonal = this.filterBarang(query, newPersonals);
        });
    }

    filterBarang(query, newPersonals: any[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newPersonals.length; i++) {
            const newPersonal = newPersonals[i];
            if (newPersonal.nama_lengkap.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal);
            } else if (newPersonal.nik.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newPersonal)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.personal.idpersonalinfo = this.newPersonal.idpersonalinfo;
        console.log('idmasbahan : ' + this.newPersonal.idpersonalinfo);
        this.overtimeService.findBarang(this.newPersonal.idpersonalinfo)
        .subscribe(
            (res: PersonalInfo) => {
                console.log('ini isi personal = ', res.nik);
                this.nik = res.nik;
                this.name = res.nama_lengkap;
                this.jabatan = res.jabatan;
            },
            (res: PersonalInfo) => {
                this.name = null;
                console.log('ini isi bahan eror = ', this.name);
            }
        )
    }

    filterSupplierSingle(event) {
        const query = event.query;
        this.purchaseOrderService.getNewSupplier().then((newSupliers) => {
            this.filteredSuppliers = this.filterSupplier(query, newSupliers);
        });
    }

    filterSupplier(query, newSupliers: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < newSupliers.length; i++) {
            const newSuplier = newSupliers[i];
            if (newSuplier.nmSupplier.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(newSuplier);
            }
        }
        return filtered;
    }

    public selectSupplier(isSelect?: boolean): void {
        this.inputSupplyerName = this.newSuplier.nmSupplier;
        this.isSelectSupplier = true;
        console.log('select supplier ' + this.inputSupplyerName);
    }

    filterKdBarangSingle(event) {
        // console.log('data Kode barang : ', this.pilihTipe);
        const query = event.query;
        this.purchaseOrderService.getNewKdBarang().then((newKdBarangs) => {
            this.filteredKdBarang = this.filterKdBarang(query, newKdBarangs);
        });
    }

    filterKdBarang(query, newKdBarangs: any[]): any[] {
        const filtBahan: any[] = [];
        for (let i = 0; i < newKdBarangs.length; i++) {
            const newKdBarang = newKdBarangs[i];
            if (newKdBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtBahan.push({
                    label: newKdBarang.kdBahan + ' - ' + newKdBarang.nmBahan,
                    value: newKdBarang.nmBahan
                });
            }
            // console.log('idkd : ', this.newKdBarang.idMasBahan);
        }
        return filtBahan;
    }

    public selectKdBarang(isSelect?: boolean): void {
        console.log('idmasbahankd : ' + this.selectedKdBarang);
        this.kdbarang.idMasBahan = this.selectedKdBarang;
        if (this.principal.getIdInternal() === 'TEKNIK') {
            this.purchaseOrderService.findBarangTeknik(this.selectedKdBarang)
            .subscribe(
                (res: MasterBarang) => {
                    console.log('ini isi bahan = ', res.kd_barang);
                    this.inputCodeName = res.kd_barang;
                    this.inputProdukName = res.nama_barang;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        } else {
            this.purchaseOrderService.findBarang(this.selectedKdBarang)
            .subscribe(
                (res: MasterBahan) => {
                    console.log('ini isi bahan = ', res.kdBahan);
                    this.inputCodeName = res.kdBahan;
                    this.inputProdukName = res.nmBahan;
                },
                (res: MasterBahan) => {
                    this.inputCodeName = null;
                    console.log('ini isi bahan eror = ', this.inputCodeName);
                }
            )
        }
    }

    onSuccessBarang(data, headers) {
        // this.purchaseOrderItem.codeProduk = data.kdBahan;
    }

    onErrorBarang(error) {
        // this.purchaseOrderItem.codeProduk = null;
    }

    loadAll1() {
        this.internal = this.principal.getIdInternal();
        console.log('oninit internal = ', this.curAccount);
        this.purchaseOrderItem = new OvertimeItem();
        this.loadingService.loadingStart();
        this.purchaseOrderItemService.queryFilterBy({
            query: 'idInternal:' + this.idPurchaseOrder,
            idPurOrdItem: this.idPurOrdItem,
            page: 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<PurchaseOrder>();
    }

    loadAll() {
        this.internal = this.principal.getIdInternal();
        console.log('oninit internal = ', this.curAccount);
        this.purchaseOrderItem = new OvertimeItem();
        this.loadingService.loadingStart();
        this.purchaseOrderItemService.queryFilterBy({
            query: 'idInternal:' + this.idPurchaseOrder,
            idPurOrdItem: this.idPurOrdItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<PurchaseOrder>();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/purchase-order-item'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/purchase-order-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.activeModal.dismiss('cancel');
        this.loadAll();
    }
    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);

        this.overtimeService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
            this.created = purchaseOrder.createdby.toUpperCase();
            this.currentDiusul = purchaseOrder.tlhDilakApp.toUpperCase();
            this.currentMGR = purchaseOrder.MengtahuiApp.toUpperCase();
            this.currentKBG = purchaseOrder.PemTgsOTAPP.toUpperCase();
            // this.loc = purchaseOrder.loc;
            // console.log('this.purchaseOrder', purchaseOrder);
            // console.log('curAccount: ', this.usrLogin);
            // console.log('created: ', this.created);
            // console.log('diusul: ', this.currentDiusul);
            // console.log('curMGR: ', this.currentMGR);
            // console.log('curKBG: ', this.currentKBG);
            // // this.onSuccessDataPP();
        });

        this.registerChangeInBillingItems();
        this.isSaving = false;
        this.loadAll();
    }
    masukAddNewData(): void {
        const data = new PurchaseOrder();
        this.newModal = true;
        this.clearDataInput();
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    valMasukAddNewData() {
        if (this.idstatus <= 10) {
            this.masukAddNewData()
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Di Approved. Terima Kasih.');
        }
    }

    onSuccessDataPP() {
        if (this.curAccount.toUpperCase() === this.purchaseOrder.createdby.toUpperCase()) {
            this.valMasukAddNewData()
        } else if ( this.curAccount === this.currentDiusul) {
            this.valMasukAddNewData()
        } else if ( this.curAccount === this.currentMGR ) {
            this.valMasukAddNewData()
        } else if ( this.curAccount === this.currentKBG) {
            this.valMasukAddNewData()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    addNewData(): void {
        this.overtimeService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
            this.created = purchaseOrder.createdby.toUpperCase();
            this.currentDiusul = purchaseOrder.tlhDilakApp.toUpperCase();
            this.currentMGR = purchaseOrder.MengtahuiApp.toUpperCase();
            this.currentKBG = purchaseOrder.PemTgsOTAPP.toUpperCase();
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);
        this.onSuccessDataPP();
    }

    editItem(idItem: any): void {
        this.overtimeService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
            this.created = purchaseOrder.createdby.toUpperCase();
            this.currentDiusul = purchaseOrder.tlhDilakApp.toUpperCase();
            this.currentMGR = purchaseOrder.MengtahuiApp.toUpperCase();
            this.currentKBG = purchaseOrder.PemTgsOTAPP.toUpperCase();
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.curAccount = this.usrLogin;
        console.log('userLoginItem :', this.usrLogin);
        this.onSuccessDataItemPP(idItem);
    }

    onSuccessDataItemPP(idItem: any) {
        if (this.curAccount.toUpperCase() === this.purchaseOrder.createdby.toUpperCase()) {
            this.valMasukAddNewDatItem(idItem)
        } else if ( this.curAccount === this.currentDiusul) {
            this.valMasukAddNewDatItem(idItem)
        } else if ( this.curAccount === this.currentMGR ) {
            this.valMasukAddNewDatItem(idItem)
        } else if ( this.curAccount === this.currentKBG) {
            this.valMasukAddNewDatItem(idItem)
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    valMasukAddNewDatItem(idItem: any) {
        if (this.idstatus <= 10) {
            this.masukAddNewDataItem(idItem)
        } else if (this.idstatus === 11 && this.loc === 'pusat') {
            this.masukAddNewDataItem(idItem)
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Di Approved. Terima Kasih.');
        }
    }

    masukAddNewDataItem(idItem: any): void {
        this.purchaseOrderItemService.findItem(idItem).subscribe(
            (purchaseOrderItem) => {
            this.purchaseOrderItemEdit = purchaseOrderItem;
            this.planDtstart = new Date(purchaseOrderItem.planDtstart);
            this.planDtFinish = new Date(purchaseOrderItem.planDtFinish);
            this.realDtstart = new Date(purchaseOrderItem.realDtstart);
            this.realDtFinish = new Date(purchaseOrderItem.realDtFinish);
            console.log('isi item edit: ', this.purchaseOrderItemEdit);
            this.newModalEdit = true;
        });
    }

    clearDataInput(): void {
        this.nik = null;
        this.name = null;
        this.jabatan = null;
        this.planDtstart = null;
        this.planDtFinish = null;
        this.planTotal = null;
        this.realDtstart = null;
        this.realDtFinish = null;
        this.realTotal = null;
        this.overTimeNow = null;
        this.overTimeSdNow = null;
        this.overTimePersen = null;
        this.catatan = null;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idPurchaseOrder']) {
            this.loadAll();
        }
        if (changes['idPurOrdItem']) {
            this.loadAll();
        }
        // if (changes['idInventoryItem']) {
        //     this.loadAll();
        // }
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: OvertimeItem) {
        return item.idOvertimeItem;
    }
    // protected registerChangeInBillingItems(): void {
    //     this.eventSubscriber = this.billingItemService.values.subscribe(
    //         (res) => {
    //             this.billingItems = res;
    //         }
    //     )
    // }
    registerChangeInBillingItems() {
        this.eventSubscriber = this.eventManager.subscribe('PurchaseOrderItemListModification', (response) => this.loadAll1());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'noUrut') {
            result.push('noUrut');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = data.totalItems / this.itemsPerPage;
        // this.totalItems = data[0].Totaldata;
        // this.queryCount = this.totalItems;
        this.purchaseOrderItems = data;
        this.loadingService.loadingStop();
        console.log('ini data internal', data);
    }

    protected onError(error: any) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
        // this.loadingService.loadingStop();
        // this.toaster.showToaster('warning', 'billingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.purchaseOrderItemService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    // loadDataLazy(event: LazyLoadEvent) {
    //     this.itemsPerPage = event.rows;
    //     this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
    //     this.previousPage = this.page;

    //     if (event.sortField !== undefined) {
    //         this.predicate = event.sortField;
    //         this.reverse = event.sortOrder;
    //     }
    //     this.loadAll();
    // }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.purchaseOrderItemService.update(event.data)
                .subscribe((res: OvertimeItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.purchaseOrderItemService.create(event.data)
                .subscribe((res: OvertimeItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: OvertimeItem) {
        this.toasterService.showToaster('info', 'PurchaseOrderItem Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    masukSave() {
        this.purchaseOrderItem.idOvertime = this.idPurchaseOrder;
        this.purchaseOrderItem.nik = this.nik;
        this.purchaseOrderItem.name = this.name;
        this.purchaseOrderItem.jabatan = this.jabatan;
        this.purchaseOrderItem.planDtstart = this.planDtstart;
        this.purchaseOrderItem.planDtFinish = this.planDtFinish;
        this.purchaseOrderItem.planTotal = this.planTotal;
        this.purchaseOrderItem.realDtstart = this.realDtstart;
        this.purchaseOrderItem.realDtFinish = this.realDtFinish;
        this.purchaseOrderItem.realTotal = this.realTotal;
        this.purchaseOrderItem.overTimeNow = this.overTimeNow;
        this.purchaseOrderItem.overTimeSdNow = this.overTimeSdNow;
        this.purchaseOrderItem.overTimePersen = this.overTimePersen;
        this.purchaseOrderItem.catatan = this.catatan;
        this.isSaving = true;
        if (this.purchaseOrderItem.nik !== undefined && this.purchaseOrderItem.name !== undefined) {
            this.newModal = false;
            this.loadingService.loadingStart();
            if (this.purchaseOrderItem.idOvertimeItem !== undefined) {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.update(this.purchaseOrderItem));
            } else {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.create(this.purchaseOrderItem));
            }
        } else {
            this.toaster.showToaster('warning', 'Purchase Request Changed', 'Data Belum Lengkap.');
        }
    }

    save() {
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.masukSave()
        } else if ( this.curAccount === this.currentDiusul) {
            this.masukSave()
        } else if ( this.curAccount === this.currentMGR ) {
            this.masukSave()
        } else if ( this.curAccount === this.currentKBG) {
            this.masukSave()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat PP Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    protected subscribeToSaveResponse(result: Observable<OvertimeItem>) {
        result.subscribe((res: OvertimeItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: OvertimeItem) {
        this.loadingService.loadingStop();
        this.eventManager.broadcast({ name: 'PurchaseOrderItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Item saved !');
        this.isSaving = false;
        // this.purchaseOrder = new PurchaseOrderItem();
        this.activeModal.dismiss(result);
        this.isSelectSupplier = false;
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onErrorr(error);
    }

    protected onErrorr(error) {
          alert('PIC Pembelian Barang Berbeda, Pilih Barang Berdasarkan Satu PIC.');
        this.toaster.showToaster('warning', 'PIC Pembelian Barang Berbeda, Pilih Barang Yang Berdasarkan Satu PIC', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBillingById(index: number, item: PurchaseOrder) {
        return item.idPurchaseOrder;
    }
    masukDelete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.purchaseOrderItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'PurchaseOrderItemListModification',
                        content: 'Deleted'
                    });
                });
            }
        });
    }
    valMasukDelete(id: any) {
        if (this.idstatus <= 10) {
            this.masukDelete(id)
        } else if (this.idstatus === 11 && this.loc === 'pusat') {
            this.masukDelete(id)
        } else {
            alert('Maaf Data Tidak Bisa Dirubah Karena Sudah Di Approved. Terima Kasih.');
        }
    }
    delete(id: any) {
        this.overtimeService.find(this.idPurchaseOrder).subscribe(
            (purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
            this.created = purchaseOrder.createdby.toUpperCase();
            this.currentDiusul = purchaseOrder.tlhDilakApp.toUpperCase();
            this.currentMGR = purchaseOrder.MengtahuiApp.toUpperCase();
            this.currentKBG = purchaseOrder.PemTgsOTAPP.toUpperCase();
        });
        console.log('log account: ', this.curAccount);
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.valMasukDelete(id)
        } else if ( this.curAccount === this.currentDiusul) {
            this.valMasukDelete(id)
        } else if ( this.curAccount === this.currentMGR ) {
            this.valMasukDelete(id)
        } else if ( this.curAccount === this.currentKBG) {
            this.valMasukDelete(id)
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
    }

    saveEdit() {
        if (this.curAccount === this.purchaseOrder.createdby.toUpperCase()) {
            this.masukSaveEdit()
        } else if ( this.curAccount === this.currentDiusul) {
            this.masukSaveEdit()
        } else if ( this.curAccount === this.currentMGR ) {
            this.masukSaveEdit()
        } else if ( this.curAccount === this.currentKBG) {
            this.masukSaveEdit()
        } else {
            alert('Maaf Data Hanya Bisa Dirubah Oleh Pembuat Dan Atasan Yang dituju. Terima Kasih.');
        }
        this.tmp = new Date;
        this.dari = (this.tmp.getFullYear() - 10) + '-' + (this.tmp.getMonth() + 1) + '-' + this.tmp.getDate();
        this.smp = new Date (this.dari);
    }
    masukSaveEdit() {
        console.log('ini data supplier = ', this.inputSupplyerNameEdit);
        if (this.planDtstart > this.smp) {
            this.purchaseOrderItemEdit.planDtstart = this.planDtstart;
        }
        if (this.planDtFinish > this.smp) {
            this.purchaseOrderItemEdit.planDtFinish = this.planDtFinish;
        }
        if (this.realDtstart > this.smp) {
            this.purchaseOrderItemEdit.realDtstart = this.realDtstart;
        }
        if (this.realDtFinish > this.smp) {
            this.purchaseOrderItemEdit.realDtFinish = this.realDtFinish;
        }

        this.isSaving = true;
        if (this.purchaseOrderItemEdit.nik !== undefined) {
            if (this.purchaseOrderItemEdit.idOvertimeItem !== undefined) {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.saveEdit(this.purchaseOrderItemEdit));
            } else {
                this.subscribeToSaveResponse(
                    this.purchaseOrderItemService.saveEdit(this.purchaseOrderItemEdit));
            }
            this.newModalEdit = false;
            this.loadingService.loadingStart();
        } else {
            this.toaster.showToaster('warning', 'Changed', 'Data Belum Lengkap.');
        }
    }
    public selectSupplierEdit(isSelect?: boolean): void {
        this.inputSupplyerNameEdit = this.newSuplierEdit.nmSupplier;
        console.log('select supplier edit ' + this.inputSupplyerNameEdit);
    }
}
