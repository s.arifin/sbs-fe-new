import { MasterSupplier } from '../master-supplier';
import { BaseEntity } from './../../shared';

export class PurchaseOrderItem implements BaseEntity {
    constructor(
        public id?: any,
        public idPurOrdItem?: any,
        public idPurchaseOrder?: any,
        public codeProduk?: any,
        public produkName?: string,
        public qty?: number,
        public unit?: string,
        public dtOffer?: any,
        public supplyerName?: string,
        public supplyerPrice?: number,
        public necessity?: string,
        public dtNecessity?: Date,
        public dtcreate?: Date,
        public remainingStock?: string,
        public remainingPo?: any,
        public supplierPrice?: number,
        public mataUang?: any,
        public OfferNumber?: any,
        public qtybookingpo?: number,
        public noUrut?: any,
        public rejectNote?: string,
        public dtSent?: any,
        public master_supplier?: MasterSupplier,
        public note_pbl?: string,
    ) {
    }
}

export class OvertimeItem implements BaseEntity {
    constructor(
        public id?: any,
        public idOvertime?: any,
        public idOvertimeItem?: any,
        public nodoc?: any,
        public nik?: any,
        public name?: string,
        public div?: string,
        public seksie?: string,
        public dtStart?: Date,
        public jabatan?: any,
        public kontrol_satpam?: any,
        public planDtstart?: Date,
        public planDtFinish?: Date,
        public planTotal?: any,
        public realDtstart?: Date,
        public realDtFinish?: Date,
        public realTotal?: any,
        public overTimeNow?: any,
        public overTimeSdNow?: any,
        public overTimePersen?: any,
        public tlhDilakApp?: any,
        public MengtahuiApp?: any,
        public PemTgsOTAPP?: any,
        public dttlhDilakApp?: any,
        public dtMengtahuiApp?: any,
        public dtPemTgsOTAPP?: any,
        public catatan?: any,
        public statustype?: any,
        public statDesc?: any,
        public TotalData?: any,
        public createdby?: any,
    ) {
    }
}

export class BillingItem implements BaseEntity {
    constructor(
        public id?: any,
        public idBillingItem?: any,
        public idItemType?: number,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescriptionription?: number,
        public idProduct?: string,
        public itemDescription?: string,
        public qty?: number,
        public unitPrice?: number,
        public basePrice?: number,
        public discount?: number,
        public taxAmount?: number,
        public totalAmount?: number,
        public billingId?: any,
        public inventoryItemId?: any,
        public idInternal?: any,
        public sequence?: any,
    ) {
    }
}
