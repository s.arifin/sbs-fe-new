import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule } from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import {
    UnionBBKService,
    UnionBBKPopupService,
    UnionBBKComponent,
    UnionBBKDetailComponent,
    UnionBBKDialogComponent,
    UnionBBKPopupComponent,
    UnionBBKDeletePopupComponent,
    UnionBBKDeleteDialogComponent,
    unionBBKRoute,
    unionBBKPopupRoute,
    UnionBBKResolvePagingParams,
    UnionBBKAsLovComponent,
    UnionBBkLovPopupComponent
} from './';
import { UnionLHPAsLovComponent, UnionLHPLovPopupComponent } from './union-lhp-as-lov.component';
import { UnionLHPPopupService } from './union-lhp-popup.service';
import { UnionLHPService } from './union-lhp.service';

const ENTITY_STATES = [
    ...unionBBKRoute,
    ...unionBBKPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule
    ],
    declarations: [
        UnionBBKComponent,
        UnionBBKDetailComponent,
        UnionBBKDialogComponent,
        UnionBBKDeleteDialogComponent,
        UnionBBKPopupComponent,
        UnionBBKDeletePopupComponent,
        UnionBBKAsLovComponent,
        UnionBBkLovPopupComponent,
        UnionLHPAsLovComponent,
        UnionLHPLovPopupComponent
    ],
    entryComponents: [
        UnionBBKComponent,
        UnionBBKDialogComponent,
        UnionBBKPopupComponent,
        UnionBBKDeleteDialogComponent,
        UnionBBKDeletePopupComponent,
        UnionBBKAsLovComponent,
        UnionBBkLovPopupComponent,
        UnionLHPAsLovComponent,
        UnionLHPLovPopupComponent
    ],
    providers: [
        UnionBBKService,
        UnionBBKPopupService,
        UnionBBKResolvePagingParams,
        UnionBBkLovPopupComponent,
        UnionLHPLovPopupComponent,
        UnionLHPPopupService,
        UnionLHPService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmUnionBBKModule {}
