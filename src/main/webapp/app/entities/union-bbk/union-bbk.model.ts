import { BaseEntity } from './../../shared';

export class UnionBBK implements BaseEntity {
    constructor(
        public id?: number,
        public no_bbk?: string,
        public tgl_bbk?: any,
        public iduser?: string,
        public ket?: string,
        public div?: string,
    ) {
    }
}

export class DetailBBKTeknik {
    constructor(
        public id?: any,
        public no_bbk?: string,
        public kd_barang?: string,
        public nama_barang?: string,
        public qty?: number,
        public qty_sisa?: number,
        public satuan?: string,
        public lokasi?: string,
        public ket?: string,
        public status?: string,
        public tipe_bbk?: string,
        public no_item?: string,
        public kd_lama?: string,
    ) {

    }
}

export class DetailBBK {
    constructor(
        public id?: any,
        public no_bbk?: string,
        public kd_bahan?: string,
        public qty?: number,
        public satuan?: string,
        public keterangan?: string,
        public iduser?: string,
        public tgl_input?: Date,
        public jam_input?: any,
        public no_lpm?: string,
        public batch_bahan?: string,
        public tgl_exp?: Date,
        public tgl_hal?: Date,
        public no_lpm2?: string,
        public no_palet?: string,
        public nama_bahan?: string,
    ) {

    }
}
