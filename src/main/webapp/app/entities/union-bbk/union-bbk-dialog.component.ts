import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnionBBK } from './union-bbk.model';
import { UnionBBKPopupService } from './union-bbk-popup.service';
import { UnionBBKService } from './union-bbk.service';

@Component({
    selector: 'jhi-union-bbk-dialog',
    templateUrl: './union-bbk-dialog.component.html'
})
export class UnionBBKDialogComponent implements OnInit {

    unionBBK: UnionBBK;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private unionBBKService: UnionBBKService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unionBBK.id !== undefined) {
            this.subscribeToSaveResponse(
                this.unionBBKService.update(this.unionBBK));
        } else {
            this.subscribeToSaveResponse(
                this.unionBBKService.create(this.unionBBK));
        }
    }

    private subscribeToSaveResponse(result: Observable<UnionBBK>) {
        result.subscribe((res: UnionBBK) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: UnionBBK) {
        this.eventManager.broadcast({ name: 'unionBBKListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-union-bbk-popup',
    template: ''
})
export class UnionBBKPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unionBBKPopupService: UnionBBKPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unionBBKPopupService
                    .open(UnionBBKDialogComponent as Component, params['id']);
            } else {
                this.unionBBKPopupService
                    .open(UnionBBKDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
