import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UnionBBK } from './union-bbk.model';
import { UnionBBKPopupService } from './union-bbk-popup.service';
import { UnionBBKService } from './union-bbk.service';

@Component({
    selector: 'jhi-union-bbk-delete-dialog',
    templateUrl: './union-bbk-delete-dialog.component.html'
})
export class UnionBBKDeleteDialogComponent {

    unionBBK: UnionBBK;

    constructor(
        private unionBBKService: UnionBBKService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.unionBBKService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'unionBBKListModification',
                content: 'Deleted an unionBBK'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-union-bbk-delete-popup',
    template: ''
})
export class UnionBBKDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unionBBKPopupService: UnionBBKPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.unionBBKPopupService
                .open(UnionBBKDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
