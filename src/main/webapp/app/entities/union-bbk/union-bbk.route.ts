import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnionBBKComponent } from './union-bbk.component';
import { UnionBBKDetailComponent } from './union-bbk-detail.component';
import { UnionBBKPopupComponent } from './union-bbk-dialog.component';
import { UnionBBKDeletePopupComponent } from './union-bbk-delete-dialog.component';
import { UnionBBKAsLovComponent, UnionBBkLovPopupComponent } from './union-bbk-as-lov.component';
import { UnionLHPLovPopupComponent } from './union-lhp-as-lov.component';

@Injectable()
export class UnionBBKResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unionBBKRoute: Routes = [
    {
        path: 'union-bbk',
        component: UnionBBKComponent,
        resolve: {
            'pagingParams': UnionBBKResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'union-bbk/:id',
        component: UnionBBKDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unionBBKPopupRoute: Routes = [
    {
        path: 'union-bbk-new',
        component: UnionBBKPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'union-bbk/:id/edit',
        component: UnionBBKPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'union-bbk/:id/delete',
        component: UnionBBKDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'union-bbk-lov',
        component: UnionBBkLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'union-lhp-lov/:jenis',
        component: UnionLHPLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unionBBK.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
