import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { UnionBBK } from './union-bbk.model';
import { UnionBBKService } from './union-bbk.service';
import { UnionBBKPopupService } from './union-bbk-popup.service';
import { LoadingService } from '../../layouts';
import { UnionBBKAsLovComponent } from '.';
import { UnionLHPPopupService } from './union-lhp-popup.service';
import { UnionLHP } from '../retur';
import { UnionLHPService } from './union-lhp.service';
import { ReportUtilService } from '../../shared/report/report-util.service';
@Component({
    selector: 'jhi-union-lhp-as-lov',
    templateUrl: './union-lhp-as-lov.component.html'
})
export class UnionLHPAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idUomType: any;
    unionLHP: UnionLHP[];
    selected: UnionLHP[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    routeSub: any;
    jenis: number | any;
    constructor(
        public activeModal: NgbActiveModal,
        protected unionLHPService: UnionLHPService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        private route: ActivatedRoute,
        private loadingService: LoadingService,
        private reportUtilService: ReportUtilService,
    ) {
        this.itemsPerPage = 20;
        this.page = 0;
        this.predicate = 'idUom';
        this.reverse = 'asc';
        this.first = 0;
        this.selected = [];
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.loadingService.loadingStart();
            if (this.currentSearch) {
                this.unionLHPService.getLHPLOV({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'nolhp:' + this.currentSearch + '|jenisklaim:' + this.jenis
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
            this.unionLHPService.getLHPLOV({
                page: this.page,
                size: this.itemsPerPage,
                query: 'nolhp:|jenisklaim:' + this.jenis
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.selected = new Array<UnionLHP>();
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        console.log('jenis nya si klaim => ', this.jenis);
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

    trackId(index: number, item: UnionBBK) {
        return item.no_bbk;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idUom') {
            result.push('idUom');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unionLHP = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        // this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    pushData() {
        this.unionLHPService.pushItems(this.selected);
        this.eventManager.broadcast({ name: 'unionLHPLovModification', content: 'OK' });
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    openPdf(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;

        bbms = rowData.nolhp.replace(/\//g, '-');
        const filter_data = 'nolhp:' + bbms;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/new_lhp/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

    openPdf1(rowData: UnionLHP) {
        this.loadingService.loadingStart();
        let bbms: string;

        bbms = rowData.nobbk.replace(/\//g, '-');
        const pecah = bbms.split('-');

        const filter_data = 'awal:' + pecah[0].toString() + '|tengah:' + pecah[1].toString() + '|akhir:' + pecah[2].toString();
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/bbk_supplier/pdf', { filterData: filter_data });
        this.loadingService.loadingStop();
    }

}

@Component({
    selector: 'jhi-union-lhp-lov-popup',
    template: ''
})
export class UnionLHPLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unionLHPPopupService: UnionLHPPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.unionLHPPopupService
                // .load(UnionLHPAsLovComponent as Component, params['jenis']);
                .open(UnionLHPAsLovComponent as Component, params['jenis']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
