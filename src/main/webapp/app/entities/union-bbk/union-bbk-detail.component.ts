import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { UnionBBK } from './union-bbk.model';
import { UnionBBKService } from './union-bbk.service';

@Component({
    selector: 'jhi-union-bbk-detail',
    templateUrl: './union-bbk-detail.component.html'
})
export class UnionBBKDetailComponent implements OnInit, OnDestroy {

    unionBBK: UnionBBK;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private unionBBKService: UnionBBKService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUnionBBKS();
    }

    load(id) {
        this.unionBBKService.find(id).subscribe((unionBBK) => {
            this.unionBBK = unionBBK;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUnionBBKS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'unionBBKListModification',
            (response) => this.load(this.unionBBK.id)
        );
    }
}
