import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { UnionBBK } from './union-bbk.model';
import { UnionBBKService } from './union-bbk.service';
import { UnionBBKPopupService } from './union-bbk-popup.service';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-union-bbk-as-lov',
    templateUrl: './union-bbk-as-lov.component.html'
})
export class UnionBBKAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idUomType: any;
    unionBBK: UnionBBK[];
    selected: UnionBBK[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    routeSub: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected unionBBKService: UnionBBKService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        private route: ActivatedRoute,
        private loadingService: LoadingService
    ) {
        this.itemsPerPage = 10;
        this.page = 0;
        this.predicate = 'idUom';
        this.reverse = 'asc';
        this.first = 0;
        this.selected = [];
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.loadingService.loadingStart();
            if (this.currentSearch) {
                this.unionBBKService.getBBKLOV({
                    page: this.page,
                    size: this.itemsPerPage,
                    query: 'nobbk:' + this.currentSearch
                    }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
            this.unionBBKService.getBBKLOV({
                page: this.page,
                size: this.itemsPerPage,
                query: 'nobbk:'
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
            );
            this.selected = new Array<UnionBBK>();
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

    trackId(index: number, item: UnionBBK) {
        return item.no_bbk;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idUom') {
            result.push('idUom');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unionBBK = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        // this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    pushData() {
        this.unionBBKService.pushItems(this.selected);
        this.eventManager.broadcast({ name: 'unionBBKLovModification', content: 'OK' });
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-union-bbk-lov-popup',
    template: ''
})
export class UnionBBkLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unionBBKPopupService: UnionBBKPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.unionBBKPopupService
                .load(UnionBBKAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
