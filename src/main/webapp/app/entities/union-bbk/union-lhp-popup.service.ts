import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { UnionLHP } from '../retur';
import { UnionLHPService } from './union-lhp.service';
import { ResponseWrapper } from '../../shared';

@Injectable()
export class UnionLHPPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private unionLHPService: UnionLHPService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unionLHPService.getLHPLOV({
                    page: 0,
                    size: 20,
                    query: 'nolhp:|jenisklaim:' + id
                    }).subscribe((unionLHP: ResponseWrapper) => {
                    // unionLHP.tgl_LHP = this.datePipe
                    //     .transform(unionLHP.tgl_LHP, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.unionLHPModalRef(component, unionLHP.json, id);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.unionLHPModalRef(component, new Array<UnionLHP>(), id);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unionLHPModalRef(component: Component, unionLHP: UnionLHP[], id: number | any): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unionLHP = unionLHP;
        modalRef.componentInstance.jenis = id;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    unionLHPLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.unionLHPLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }
}
