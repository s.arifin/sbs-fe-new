import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DetailBBK, DetailBBKTeknik, UnionBBK } from './union-bbk.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnionBBKService {

    protected itemValues: UnionBBK[] = new Array<UnionBBK>();
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = process.env.API_C_URL + '/api/union-bbks';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/union-bbks';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(unionBBK: UnionBBK): Observable<UnionBBK> {
        const copy = this.convert(unionBBK);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(unionBBK: UnionBBK): Observable<UnionBBK> {
        const copy = this.convert(unionBBK);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<UnionBBK> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.tgl_bbk = this.dateUtils
            .convertDateTimeFromServer(entity.tgl_bbk);
    }

    private convert(unionBBK: UnionBBK): UnionBBK {
        const copy: UnionBBK = Object.assign({}, unionBBK);

        copy.tgl_bbk = this.dateUtils.toDate(unionBBK.tgl_bbk);
        return copy;
    }

    getBBKLOV(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/lov', options)
            .map((res: Response) => this.convertResponse(res));
    }

    pushItems(data: UnionBBK[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    findDetailBBKTeknik(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-teknik-detail-by-id', options).map((res: Response) => this.convertResponse(res));
    }

    findDetailBBK(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-detail-by-id', options).map((res: Response) => this.convertResponse(res));
    }
}
