import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HeaderKlaim } from './header-klaim.model';
import { HeaderKlaimPopupService } from './header-klaim-popup.service';
import { HeaderKlaimService } from './header-klaim.service';

@Component({
    selector: 'jhi-header-klaim-delete-dialog',
    templateUrl: './header-klaim-delete-dialog.component.html'
})
export class HeaderKlaimDeleteDialogComponent {

    headerKlaim: HeaderKlaim;

    constructor(
        private headerKlaimService: HeaderKlaimService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.headerKlaimService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'headerKlaimListModification',
                content: 'Deleted an headerKlaim'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-header-klaim-delete-popup',
    template: ''
})
export class HeaderKlaimDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private headerKlaimPopupService: HeaderKlaimPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.headerKlaimPopupService
                .open(HeaderKlaimDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
