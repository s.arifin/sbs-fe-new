import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule
} from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import { MpmPaymentModule } from '../payment/payment.module';
import {
    HeaderKlaimService,
    HeaderKlaimPopupService,
    HeaderKlaimComponent,
    HeaderKlaimDetailComponent,
    HeaderKlaimDialogComponent,
    HeaderKlaimPopupComponent,
    HeaderKlaimDeletePopupComponent,
    HeaderKlaimDeleteDialogComponent,
    headerKlaimRoute,
    headerKlaimPopupRoute,
    HeaderKlaimResolvePagingParams,
    ApprovalHeaderKlaimComponent,
    TabHeaderKlaimComponent,
} from './';
import { ApprovalDetailKlaimComponent } from './approval/approval-detail-klaim.component';

const ENTITY_STATES = [
    ...headerKlaimRoute,
    ...headerKlaimPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule
    ],
    declarations: [
        HeaderKlaimComponent,
        HeaderKlaimDetailComponent,
        HeaderKlaimDialogComponent,
        HeaderKlaimDeleteDialogComponent,
        HeaderKlaimPopupComponent,
        HeaderKlaimDeletePopupComponent,
        TabHeaderKlaimComponent,
        ApprovalHeaderKlaimComponent,
        ApprovalDetailKlaimComponent
    ],
    entryComponents: [
        HeaderKlaimComponent,
        HeaderKlaimDialogComponent,
        HeaderKlaimPopupComponent,
        HeaderKlaimDeleteDialogComponent,
        HeaderKlaimDeletePopupComponent,
        TabHeaderKlaimComponent,
        ApprovalHeaderKlaimComponent,
        ApprovalDetailKlaimComponent
    ],
    providers: [
        HeaderKlaimService,
        HeaderKlaimPopupService,
        HeaderKlaimResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmHeaderKlaimModule { }
