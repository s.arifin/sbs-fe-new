import { BaseEntity } from './../../shared';

export class HeaderKlaim implements BaseEntity {
    constructor(
        public id?: number,
        public iduser?: string,
        public notrans?: string,
        public notransKlaim?: string,
        public bulan_program?: string,
        public no_klaim?: string,
        public tgl_klaim?: string,
        public jam_klaim?: string,
        public no_srt_skg?: string,
        public tgl_srt_skg?: string,
        public hal_skg?: string,
        public no_srt_sbs?: string,
        public tgl_srt_sbs?: string,
        public hal_sbs?: string,
        public jml_edit?: number,
        public sts_klaim?: string,
        public giro_num?: string,
        public giro_val?: number,
        public nmkurir?: string,
        public check1?: number,
        public check2?: number,
        public check3?: number,
        public check4?: number,
        public check5?: number,
        public check6?: number,
        public check7?: number,
        public ketACC?: string,
        public checklengkap?: string,
        public ketaccX?: string,
        public userAPPV?: string,
        public userAPPVx?: string,
    ) {
    }
}
