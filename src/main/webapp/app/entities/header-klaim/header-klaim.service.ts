import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { HeaderKlaim } from './header-klaim.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { DetailKlaim } from '../detail-klaim';
import { Header } from 'primeng/primeng';

@Injectable()
export class HeaderKlaimService {

    private resourceUrl = process.env.API_C_URL + '/api/header-klaims';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/header-klaims';

    constructor(private http: Http) { }

    create(headerKlaim: HeaderKlaim): Observable<HeaderKlaim> {
        const copy = this.convert(headerKlaim);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(headerKlaim: HeaderKlaim): Observable<HeaderKlaim> {
        const copy = this.convert(headerKlaim);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<HeaderKlaim> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(headerKlaim: HeaderKlaim): HeaderKlaim {
        const copy: HeaderKlaim = Object.assign({}, headerKlaim);
        return copy;
    }

    private convertArray(headerKlaim: HeaderKlaim[]): HeaderKlaim[] {
        const copy: HeaderKlaim[] = Object.assign([], headerKlaim);
        return copy;
    }
    queryDetailByNotrans(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/detail-by-notrans', options)
            .map((res: Response) => this.convertResponse(res));
    }
    approveManagement(headerKlaims: HeaderKlaim[]): Observable<HeaderKlaim[]> {
        const copy = this.convertArray(headerKlaims);
        return this.http.put(this.resourceUrl + '/approve-management', copy).map((res: Response) => {
            return res.json();
        });
    }

    notApproveManagement(headerKlaims: HeaderKlaim[]): Observable<HeaderKlaim[]> {
        const copy = this.convertArray(headerKlaims);
        return this.http.put(this.resourceUrl + '/not-approve-management', copy).map((res: Response) => {
            return res.json();
        });
    }
}
