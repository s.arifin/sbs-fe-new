import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { HeaderKlaim } from '../header-klaim.model';
import { HeaderKlaimService } from '../header-klaim.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-tab-header-klaim',
    templateUrl: './tab-header-klaim.component.html'
})
export class TabHeaderKlaimComponent implements OnInit, OnDestroy {

    currentAccount: any;
    headerKlaims: HeaderKlaim[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    onproses: string;
    forapprove: string;
    nextproses: string;

    constructor(
        private headerKlaimService: HeaderKlaimService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.onproses = 'onproses';
        this.forapprove = 'forapprove';
        this.nextproses = 'nextproses';
    }
    ngOnInit(): void {
        // throw new Error('Method not implemented.');
    }
    ngOnDestroy(): void {
        // throw new Error('Method not implemented.');
    }
}
