import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { HeaderKlaim } from '../header-klaim.model';
import { HeaderKlaimService } from '../header-klaim.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { DetailKlaim } from '../../detail-klaim';

@Component({
    selector: 'jhi-approval-header-klaim',
    templateUrl: './approval-header-klaim.component.html'
})
export class ApprovalHeaderKlaimComponent implements OnInit, OnDestroy {

    @Input() proses: string;
    currentAccount: any;
    headerKlaims: HeaderKlaim[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    noTrans: string;
    modalDetailKlaim: boolean;
    login: string;
    noKlaim: string;
    selected: HeaderKlaim[];
    newModalNote: boolean;
    ketNotApp: string;

    constructor(
        private headerKlaimService: HeaderKlaimService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        this.proses = '';
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.noTrans = '';
        this.modalDetailKlaim = false;
        this.login = '';
        this.noKlaim = '';
        this.selected = new Array<HeaderKlaim>();
        this.newModalNote = false;
        this.ketNotApp = '';
    }

    loadAll() {
        if (this.proses !== undefined) {
            this.loadingService.loadingStart();
            if (this.currentSearch) {
                this.headerKlaimService.query({
                    page: this.page,
                    query: 'proses:' + this.proses + '|noklaim:' + this.currentSearch,
                    size: this.itemsPerPage,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
            this.headerKlaimService.query({
                page: this.page,
                size: this.itemsPerPage,
                query: 'proses:' + this.proses + '|noklaim:' + this.currentSearch,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/approval-klaim'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-klaim', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/approval-klaim', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.noTrans = '';
        this.noKlaim = '';
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.login = account.login;
        });
        this.registerChangeInHeaderKlaims();
        this.registerChangeInDetailKlaims();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: HeaderKlaim) {
        return item.id;
    }
    registerChangeInHeaderKlaims() {
        this.eventSubscriber = this.eventManager.subscribe('headerKlaimListModification', (response) => this.loadAll());
    }
    registerChangeInDetailKlaims() {
        this.eventSubscriber = this.eventManager.subscribe('detailKlaimListModification', (response) => {
            this.loadAll();
            this.modalDetailKlaim = false;
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.headerKlaims = data;
        this.loadingService.loadingStop();
    }
    private onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
    }
    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage);

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    approveDetail(rowData: HeaderKlaim) {
        this.noTrans = rowData.notrans;
        this.noKlaim = rowData.no_klaim;
        this.modalDetailKlaim = true;
    }
    approve() {
        console.log('ini data yang akan di approve == ', this.selected);
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Approve Detail Klaim',
            accept: () => {
                this.headerKlaimService.approveManagement(this.selected)
                    .subscribe(
                        (res) => {
                            this.loadAll();
                            this.eventManager.broadcast({ name: 'detailKlaimListModification', content: 'OK' });
                        }
                    );
            }
        });
    }

    notApprove() {
        this.selected.forEach(
            (e) => {
                e.ketaccX = this.ketNotApp;
            }
        );
        console.log('ini data yang akan di approve == ', this.selected);
        this.confirmationService.confirm({
            header: 'Information',
            message: 'Apakah Yakin Untuk Approve Detail Klaim',
            accept: () => {
                this.headerKlaimService.notApproveManagement(this.selected)
                    .subscribe(
                        (res) => {
                            this.loadAll();
                            this.resetDialog();
                            this.eventManager.broadcast({ name: 'detailKlaimListModification', content: 'OK' });
                        }
                    );
            }
        });
    }
    cekNotApprove() {
        this.newModalNote = true;
    }
    resetDialog() {
        this.ketNotApp = '';
        this.noKlaim = '';
        this.noTrans = '';
        this.modalDetailKlaim = false;
        this.newModalNote = false;
    }
}
