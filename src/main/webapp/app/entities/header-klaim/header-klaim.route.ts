import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HeaderKlaimComponent } from './header-klaim.component';
import { HeaderKlaimDetailComponent } from './header-klaim-detail.component';
import { HeaderKlaimPopupComponent } from './header-klaim-dialog.component';
import { HeaderKlaimDeletePopupComponent } from './header-klaim-delete-dialog.component';
import { TabHeaderKlaimComponent } from './approval/tab-header-klaim.component';

@Injectable()
export class HeaderKlaimResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const headerKlaimRoute: Routes = [
    {
        path: 'header-klaim',
        component: HeaderKlaimComponent,
        resolve: {
            'pagingParams': HeaderKlaimResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerKlaim.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'header-klaim/:id',
        component: HeaderKlaimDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerKlaim.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-klaim',
        component: TabHeaderKlaimComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerKlaim.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const headerKlaimPopupRoute: Routes = [
    {
        path: 'header-klaim-new',
        component: HeaderKlaimPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerKlaim.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'header-klaim/:id/edit',
        component: HeaderKlaimPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerKlaim.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'header-klaim/:id/delete',
        component: HeaderKlaimDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.headerKlaim.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
