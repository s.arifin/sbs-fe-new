import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { HeaderKlaim } from './header-klaim.model';
import { HeaderKlaimService } from './header-klaim.service';

@Component({
    selector: 'jhi-header-klaim-detail',
    templateUrl: './header-klaim-detail.component.html'
})
export class HeaderKlaimDetailComponent implements OnInit, OnDestroy {

    headerKlaim: HeaderKlaim;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private headerKlaimService: HeaderKlaimService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHeaderKlaims();
    }

    load(id) {
        this.headerKlaimService.find(id).subscribe((headerKlaim) => {
            this.headerKlaim = headerKlaim;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHeaderKlaims() {
        this.eventSubscriber = this.eventManager.subscribe(
            'headerKlaimListModification',
            (response) => this.load(this.headerKlaim.id)
        );
    }
}
