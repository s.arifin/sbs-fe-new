import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {StandardCalendar} from './standard-calendar.model';
import {StandardCalendarService} from './standard-calendar.service';

@Component({
    selector: 'jhi-standard-calendar-detail',
    templateUrl: './standard-calendar-detail.component.html'
})
export class StandardCalendarDetailComponent implements OnInit, OnDestroy {

    standardCalendar: StandardCalendar;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected standardCalendarService: StandardCalendarService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStandardCalendars();
    }

    load(id) {
        this.standardCalendarService.find(id).subscribe((standardCalendar) => {
            this.standardCalendar = standardCalendar;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStandardCalendars() {
        this.eventSubscriber = this.eventManager.subscribe(
            'standardCalendarListModification',
            (response) => this.load(this.standardCalendar.idCalendar)
        );
    }
}
