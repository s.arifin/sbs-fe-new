import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { StandardCalendar } from './standard-calendar.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class StandardCalendarService {
   protected itemValues: StandardCalendar[];
   values: Subject<any> = new Subject<any>();

//    protected resourceUrl =  process.env.API_C_URL + 'api/standard-calendars';
   protected resourceUrl = process.env.API_C_URL + '/api/standard-calendars';
   protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/standard-calendars';

   constructor(protected http: Http) { }

   create(standardCalendar: StandardCalendar): Observable<StandardCalendar> {
       const copy = this.convert(standardCalendar);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(standardCalendar: StandardCalendar): Observable<StandardCalendar> {
       const copy = this.convert(standardCalendar);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<StandardCalendar> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

    queryFilterBySearch(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: StandardCalendar, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: StandardCalendar[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to StandardCalendar.
    */
   protected convertItemFromServer(json: any): StandardCalendar {
       const entity: StandardCalendar = Object.assign(new StandardCalendar(), json);
       return entity;
   }

   /**
    * Convert a StandardCalendar to a JSON which can be sent to the server.
    */
   protected convert(standardCalendar: StandardCalendar): StandardCalendar {
       if (standardCalendar === null || standardCalendar === {}) {
           return {};
       }
       // const copy: StandardCalendar = Object.assign({}, standardCalendar);
       const copy: StandardCalendar = JSON.parse(JSON.stringify(standardCalendar));
       return copy;
   }

   protected convertList(standardCalendars: StandardCalendar[]): StandardCalendar[] {
       const copy: StandardCalendar[] = standardCalendars;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: StandardCalendar[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
