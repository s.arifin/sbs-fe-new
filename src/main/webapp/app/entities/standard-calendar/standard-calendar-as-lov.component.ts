import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { StandardCalendar } from './standard-calendar.model';
import { StandardCalendarService } from './standard-calendar.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { StandardCalendarPopupService } from './standard-calendar-popup.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-standard-calendar-as-lov',
    templateUrl: './standard-calendar-as-lov.component.html'
})
export class StandardCalendarAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idInternal: any;
    standardCalendars: StandardCalendar[];
    selected: StandardCalendar[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        public activeModal: NgbActiveModal,
        protected standardCalendarService: StandardCalendarService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idCalendar';
        this.reverse = 'asc';
        this.first = 0;
        this.selected = [];
    }

    loadAll() {
        if (this.currentSearch) {
            this.standardCalendarService.queryFilterBySearch({
                idInternal: this.idInternal,
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.standardCalendarService.queryFilterBy({
            idInternal: this.idInternal,
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: StandardCalendar) {
        return item.idCalendar;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idCalendar') {
            result.push('idCalendar');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.standardCalendars = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.standardCalendarService.pushItems(this.selected);
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-standard-calendar-lov-popup',
    template: ''
})
export class StandardCalendarLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected standardCalendarPopupService: StandardCalendarPopupService
    ) {}

    ngOnInit() {
        this.standardCalendarPopupService.idInternal = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            this.standardCalendarPopupService
                .load(StandardCalendarAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
