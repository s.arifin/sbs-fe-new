import { BaseEntity } from './../../shared';
import { BaseCalendar } from '../base-calendar/base-calendar.model';

export class StandardCalendar extends BaseCalendar {
    constructor(
        public workDay?: boolean,
        public internalId?: any,
    ) {
        super();
    }
}
