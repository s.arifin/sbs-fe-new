import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StandardCalendarComponent } from './standard-calendar.component';
import { StandardCalendarEditComponent } from './standard-calendar-edit.component';
import { StandardCalendarLovPopupComponent } from './standard-calendar-as-lov.component';
import { StandardCalendarPopupComponent } from './standard-calendar-dialog.component';

@Injectable()
export class StandardCalendarResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCalendar,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const standardCalendarRoute: Routes = [
    {
        path: 'standard-calendar',
        component: StandardCalendarComponent,
        resolve: {
            'pagingParams': StandardCalendarResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const standardCalendarPopupRoute: Routes = [
    {
        path: 'standard-calendar-lov',
        component: StandardCalendarLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'standard-calendar-popup-new',
        component: StandardCalendarPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'standard-calendar-new',
        component: StandardCalendarEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'standard-calendar/:id/edit',
        component: StandardCalendarEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'standard-calendar/:route/:page/:id/edit',
        component: StandardCalendarEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'standard-calendar/:id/popup-edit',
        component: StandardCalendarPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.standardCalendar.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
