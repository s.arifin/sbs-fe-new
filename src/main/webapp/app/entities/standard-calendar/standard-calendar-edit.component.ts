import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StandardCalendar } from './standard-calendar.model';
import { StandardCalendarService } from './standard-calendar.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-standard-calendar-edit',
   templateUrl: './standard-calendar-edit.component.html'
})
export class StandardCalendarEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   standardCalendar: StandardCalendar;
   isSaving: boolean;
   idCalendar: any;
   paramPage: number;
   routeId: number;

   internals: Internal[];

   constructor(
       protected alertService: JhiAlertService,
       protected standardCalendarService: StandardCalendarService,
       protected internalService: InternalService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.standardCalendar = new StandardCalendar();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idCalendar = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.internalService.getAllDropdown()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.standardCalendarService.find(this.idCalendar).subscribe((standardCalendar) => {
           this.standardCalendar = standardCalendar;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['standard-calendar', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.standardCalendar.idCalendar !== undefined) {
           this.subscribeToSaveResponse(
               this.standardCalendarService.update(this.standardCalendar));
       } else {
           this.subscribeToSaveResponse(
               this.standardCalendarService.create(this.standardCalendar));
       }
   }

   protected subscribeToSaveResponse(result: Observable<StandardCalendar>) {
       result.subscribe((res: StandardCalendar) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: StandardCalendar) {
       this.eventManager.broadcast({ name: 'standardCalendarListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'standardCalendar saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'standardCalendar Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }
}
