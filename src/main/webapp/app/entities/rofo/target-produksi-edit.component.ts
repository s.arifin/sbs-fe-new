import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import * as ConfigConstants from '../../shared/constants/config.constants';
import { RofoModel, TargetProduksi } from './rofo.model';
import { RofoService } from './rofo.service';
@Component({
    selector: 'jhi-target-produksi-edit',
    templateUrl: './target-produksi-edit.component.html'
})
export class TargetProduksiEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    purchaseOrder: TargetProduksi;
    isSaving: boolean;
    idPurchaseOrder: any;
    paramPage: number;
    routeId: number;

    constructor(
        protected alertService: JhiAlertService,
        protected purchaseOrderService: RofoService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected dataUtils: JhiDataUtils,
        protected loadingService: LoadingService,
    ) {
        this.purchaseOrder = new RofoModel();
        this.routeId = 0;
    }

    ngOnInit() {
        this.internal = this.principal.getIdInternal();
        this.curAccount = this.principal.getUserLogin().toUpperCase();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.idlocal = account.loc;
        });
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPurchaseOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.purchaseOrderService.find1(this.idPurchaseOrder).subscribe((purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['target-produksi', { page: this.paramPage }]);
        }
    }

    saveRofo() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        if (this.purchaseOrder.id_target !== undefined) {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.update1(this.purchaseOrder));
        } else {

        }
    }

    protected subscribeToSaveResponse(result: Observable<RofoModel>) {
        result.subscribe((res: RofoModel) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RofoModel) {
        this.eventManager.broadcast({ name: 'RofoListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'Target Produksi saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'Target Produksi Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
