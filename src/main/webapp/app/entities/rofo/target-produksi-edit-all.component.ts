import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, Account, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../layouts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RofoService } from './rofo.service';
import { RofoModel, TargetProduksi } from './rofo.model';
import { MasterProduk } from '../report/report.model';

@Component({
    selector: 'jhi-target-produksi-edit-all',
    templateUrl: './target-produksi-edit-all.component.html'
})
export class TargetProduksiEditAllComponent implements OnInit {

    currentAccount: any;
    error: any;
    paramPage: number;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filteredpic: any[];
    filteredpType: any[];
    newpic: any;
    newpType: any;
    internals: any;
    first: any;
    routeId: number;
    isSaving: boolean;
    selectBulan: string;
    selectTahun: string;
    ViewRofo: boolean;
    masterProduk: TargetProduksi[];
    bulans: Array<object> = [
        { label: 'Pilih Bulan', value: '' },
        { label: 'Januari', value: '01' },
        { label: 'Februari', value: '02' },
        { label: 'Maret', value: '03' },
        { label: 'April', value: '04' },
        { label: 'Mei', value: '05' },
        { label: 'Juni', value: '06' },
        { label: 'Juli', value: '07' },
        { label: 'Agustus', value: '08' },
        { label: 'September', value: '09' },
        { label: 'Oktober', value: '10' },
        { label: 'November', value: '11' },
        { label: 'Desember', value: '12' },
    ];

    tahuns: Array<object> = [
        { label: 'Pilih', value: '' },
    ];
    inkdUmum: string;
    constructor(
        public activeModal: NgbActiveModal,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private purchasingService: RofoService,
    ) {
        this.internals = principal.getIdInternal();
        this.ViewRofo = false;
        // this.first = 0;
        // this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data.pagingParams.page;
        //     this.previousPage = data.pagingParams.page;
        //     this.reverse = data.pagingParams.ascending;
        //     this.predicate = data.pagingParams.predicate;
        // });
        // this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
        //     this.activatedRoute.snapshot.params['search'] : '';
        // const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
        //     this.activatedRoute.snapshot.params['page'] : 0;
        // this.page = lastPage > 0 ? lastPage : 1;
        // this.first = (this.page - 1) * this.itemsPerPage;
    }
    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        const startYear = 2021;
        const endYear = new Date().getFullYear(); // Mendapatkan tahun saat ini;
        for (let year = startYear; year <= endYear; year++) {
            this.tahuns.push({ label: year.toString(), value: year.toString() });
        }
    }
    trackId(index: number, item: TargetProduksi) {
        return item.id_target;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id_target') {
            result.push('id_target');
        }
        return result;
    }

    onChangeRofo() {
        if ((this.selectBulan !== '' || this.selectTahun !== undefined || this.selectBulan !== null) && (this.selectTahun !== '' || this.selectTahun !== undefined || this.selectTahun !== null)) {
            this.cekRofo();
        } else {

        }
    }

    createROFO() {
        this.loadingService.loadingStart()
        this.purchasingService.updateROFO1(this.masterProduk)
            .subscribe(
                (res) => {
                    this.router.navigate(['/target-produksi']);
                    this.loadingService.loadingStop();
                })
    }

    cekRofo() {
        this.loadingService.loadingStart();
        if (this.selectBulan === '' || this.selectBulan === null || this.selectBulan === undefined) {
            alert('Pilih Bulan Terlebih Dahulu');
            this.loadingService.loadingStop();
        } else if (this.selectTahun === '' || this.selectTahun === null || this.selectTahun === undefined) {
            alert('Pilih Tahun Terlebih Dahulu');
            this.loadingService.loadingStop();
        } else if ((this.selectBulan === '' || this.selectBulan === null || this.selectBulan === undefined) && (this.selectTahun === '' || this.selectTahun === null || this.selectTahun === undefined)) {
            alert('Tahun dan Bulan Dipilih Terlebih Dahulu');
            this.loadingService.loadingStop();
        } else {
            this.purchasingService.getDataTarget(this.selectBulan, this.selectTahun).subscribe((pp) => {
                if (pp === null) {
                    alert('Data Target Produksi Tidak Ditemukan, Silahkan Tambah Data Produksi Terlebih Dahulu');
                    this.loadingService.loadingStop();
                } else {
                    this.purchasingService.getProdukEdit1({
                        bulan: this.selectBulan,
                        tahun: this.selectTahun
                    }).subscribe(
                        (res: ResponseWrapper) => {
                            this.masterProduk = res.json;
                            this.ViewRofo = true;
                            this.loadingService.loadingStop();
                        })
                }

            });
        }
    }

    simpan() {
        // if (this.inPicBahan === null || this.inPicBahan === ' ' || this.inPicBahan === undefined) {
        //     this.toaster.showToaster('info', 'PIC Kosong', 'Pilih PIC Barang Terlebih Dahulu !');
        // } else if (this.inKdBahan === null || this.inKdBahan === ' ' || this.inKdBahan === undefined) {
        //     this.toaster.showToaster('info', 'Kode Bahan Kosong', 'Isi Kode Barang Terlebih Dahulu !');
        // } else if (this.inNmBahan === null || this.inNmBahan === ' ' || this.inNmBahan === undefined) {
        //     this.toaster.showToaster('info', 'Nama Bahan Kosong', 'Isi Nama Bahan Terlebih Dahulu !');
        // } else if (this.inSatBahan === null || this.inSatBahan === ' ' || this.inSatBahan === undefined) {
        //     this.toaster.showToaster('info', 'Satuan Bahan Kosong', 'Isi Satuan Bahan Terlebih Dahulu !');
        // } else {
        //     const obj = {
        //         tpbahan: this.inTipeBahan,
        //         kdBahanLama: this.inKdUmBahan,
        //         kdUmum: this.inkdUmum,
        //         kdbahan: this.inKdBahan.replace(/\s/g, ''),
        //         nmbahan: this.inNmBahan,
        //         satuan: this.inSatBahan,
        //         pic : this.inPicBahan,
        //         div: this.principal.getIdInternal(),
        //         type: this.inTipeBahan,
        //     }
        //     this.loadingService.loadingStart();
        //     this.masterBahanService.create(obj)
        //     .subscribe(
        //         (res: ResponseWrapper) =>
        //         this.onSaveSuccess(res, res.headers),
        //         (res: ResponseWrapper) => this.onError(res.json)
        //     );
        // }
    }

    private onSaveSuccess(data, headers) {

    }
    private onError(error) {

    }
    previousState() {
        this.router.navigate(['/target-produksi', { page: this.paramPage }]);
    }
    back() {
        this.router.navigate(['/target-produksi'])
    }
}
