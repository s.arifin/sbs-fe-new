import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RofoComponent } from './rofo.component';
import { RofoNewComponent } from './rofo-new.component';
import { RofoEditComponent } from './rofo-edit.component';
import { RofoEditNewAllComponent } from './rofo-edit-new-all.component';
import { TargetProduksiComponent } from './target-produksi.component';
import { TargetProduksiNewComponent } from './target-produksi-new.component';
import { TargetProduksiEditComponent } from './target-produksi-edit.component';
import { TargetProduksiEditAllComponent } from './target-produksi-edit-all.component';

@Injectable()
export class RofoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idrofo,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}
@Injectable()
export class TargetProduksiResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id_target,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const rofoRoute: Routes = [
    {
        path: 'rofo',
        component: RofoComponent,
        resolve: {
            'pagingParams': RofoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home1.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'create-new-rofo',
        component: RofoNewComponent,
        // resolve: {
        //     'pagingParams': RofoResolvePagingParams
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home1.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'edit-new-rofo-all',
        component: RofoEditNewAllComponent,
        // resolve: {
        //     'pagingParams': RofoResolvePagingParams
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home1.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rofo-edit/:route/:page/:id/edit',
        component: RofoEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home1.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'target-produksi',
        component: TargetProduksiComponent,
        resolve: {
            'pagingParams': TargetProduksiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home2.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'create-new-target',
        component: TargetProduksiNewComponent,
        // resolve: {
        //     'pagingParams': RofoResolvePagingParams
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home2.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'target-produksi-edit/:route/:page/:id/edit',
        component: TargetProduksiEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home2.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'edit-target-produksi-all',
        component: TargetProduksiEditAllComponent,
        // resolve: {
        //     'pagingParams': RofoResolvePagingParams
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home2.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
