import { BaseEntity } from './../../shared';

export class RofoModel implements BaseEntity {
    constructor(
        public id?: number,
        public idrofo?: any,
        public kd_group?: any,
        public kd_produk?: any,
        public rofo_awal?: string,
        public revisi1?: any,
        public revisi2?: any,
        public revisi3?: any,
        public revisi4?: any,
        public revisi5?: any,
        public bulan?: string,
        public tahun?: any,
        public total?: number,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public nm_produk?: string,
    ) {
    }
}

export class TargetProduksi implements BaseEntity {
    constructor(
        public id?: number,
        public id_target?: any,
        public kd_group?: any,
        public kd_produk?: any,
        public qty1?: any,
        public qty2?: any,
        public qty3?: any,
        public qty4?: any,
        public qty5?: any,
        public bulan?: string,
        public tahun?: any,
        public dtcreate?: Date,
        public dtmodified?: Date,
        public createby?: string,
        public modifiedby?: string,
        public nm_produk?: string,
    ) {
    }
}
