import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts';
import { RofoModel } from './rofo.model';
import { ItemProduct } from '../purchasing';
import { RofoService } from './rofo.service';

@Component({
    selector: 'jhi-rofo',
    templateUrl: './rofo.component.html'
})
export class RofoComponent implements OnInit, OnDestroy {

    currentAccount: any;
    purchaseOrders: RofoModel[];
    modalListHR: boolean;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;
    stat: any;
    newBarang: any;

    // field filter
    filDtFrom: Date;
    filDtThru: Date;
    dtfromstr: string;
    dtthrustr: string;
    filteredSuppliers: any[];
    kdSPL: string;
    isFilter: boolean;
    isFilter1: boolean;
    selectBulan: string;
    selectTahun: string;
    selectProduk: string;
    // field filter
    loginName: string;

    hideButton: boolean;
    idpp: any;
    filteredBarang: ItemProduct[];

    // Filter Kebutuhan PP
    inputNeeds: any;
    bulans: Array<object> = [
        { label: 'Pilih Bulan', value: '' },
        { label: 'Januari', value: '01' },
        { label: 'Februari', value: '02' },
        { label: 'Maret', value: '03' },
        { label: 'April', value: '04' },
        { label: 'Mei', value: '05' },
        { label: 'Juni', value: '06' },
        { label: 'Juli', value: '07' },
        { label: 'Agustus', value: '08' },
        { label: 'September', value: '09' },
        { label: 'Oktober', value: '10' },
        { label: 'November', value: '11' },
        { label: 'Desember', value: '12' },
    ];

    dtneed: string;
    non_rutin: boolean;

    tahuns: Array<object> = [
        // { label: 'Pilih', value: '' },
        // { label: '2023', value: '2023' },
        // { label: '2024', value: '2024' },
    ];
    curAccount: string;

    constructor(
        protected purchaseOrderService: RofoService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected modalService: NgbModal,
        protected loadingService: LoadingService,
        protected toaster: ToasterService,
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.isFilter = false;
        this.selectBulan = '';
        this.selectTahun = '';
        this.selectProduk = '';
        this.curAccount = this.principal.getUserLogin();
        const currentYear = new Date().getFullYear();
        for (let year = 2020; year <= currentYear; year++) {
            this.tahuns.push({ label: year.toString(), value: year });
        }
    }

    filter() {
        this.isFilter = true;
        this.currentSearch = '';
        this.loadAll()
    }

    reset() {
        this.isFilter = false;
        this.currentSearch = ''
        this.selectBulan = '';
        this.selectTahun = '';
        this.selectProduk = '';
        this.newBarang = '';
        this.loadAll()
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.isFilter === true) {
            if (this.selectBulan === '' || this.selectBulan === undefined) {
                alert('Pilih Bulan Terlebih Dahulu');
                this.loadingService.loadingStop()
            } else if (this.selectTahun === '' || this.selectTahun === undefined) {
                alert('Pilih Tahun Terlebih Dahulu');
                this.loadingService.loadingStop()
            } else {
                this.purchaseOrderService.queryFilter({
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort(),
                    filter: 'bulan: ' + this.selectBulan +
                        '|tahun: ' + this.selectTahun +
                        '|kd_produk: ' + this.selectProduk
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccessfilter(res.json, res.headers),
                    (reserr: ResponseWrapper) => this.onErrorSearch(reserr.json),
                );
                return;
            }
        } else if (this.isFilter === false) {
            if (this.currentSearch) {
                this.purchaseOrderService.querySearch({
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onErrorSearch(res.json)
                );
                return;
            } else {
                this.purchaseOrderService.query({
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (reserr: ResponseWrapper) => this.onError(reserr.json),
                );
            }
        }
    }

    protected onErrorSearch(error) {
        this.toasterService.showToaster('info', 'Data Proces', 'Data Not Found.')
        this.clear();
        this.loadingService.loadingStop();
        this.isFilter = false;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/rofo'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.isFilter = false;
        this.router.navigate(['/rofo', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isFilter = false;
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/rofo', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPurchaseOrders();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPurchaseOrders() {
        this.loadingService.loadingStart();
        this.eventSubscriber = this.eventManager.subscribe('rofoListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate === 'idrofo') {
            result.push('idrofo');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.purchaseOrders = data;
        this.loadingService.loadingStop();

    }

    protected onSuccessfilter(data, headers) {
        if (data.length > 0) {
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.purchaseOrders = data;
            this.loadingService.loadingStop();
        } else {
            alert('Data Tidak Ditemukan');
        }
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    filterBarangSingle(event) {
        const query = event.query;
        this.purchaseOrderService.getPoProductCode().then((newBarangs) => {
            this.filteredBarang = this.filterBarang(query, newBarangs);
        });
    }

    filterBarang(query, newBarangs: ItemProduct[]): any[] {
        const filBahan: any[] = [];
        for (let i = 0; i < newBarangs.length; i++) {
            const newBarang = newBarangs[i];
            if (newBarang.nmBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang);
            } else if (newBarang.kdBahan.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filBahan.push(newBarang)
            }
        }
        return filBahan;
    }

    public selectBarang(isSelect?: boolean): void {
        this.selectProduk = this.newBarang.kdBahan;
    }
}
