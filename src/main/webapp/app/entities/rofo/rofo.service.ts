import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { RofoModel, TargetProduksi } from './rofo.model';
import { MasterProduk } from '../report/report.model';

@Injectable()
export class RofoService {
    private resourceUrl = process.env.API_C_URL + '/api/rofo';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySearch(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search', options)
            .map((res: any) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filter1', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querySearch1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/search1', options)
            .map((res: any) => this.convertResponse(res));
    }

    query1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/index', options)
            .map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    protected convertItemFromServer(json: any): RofoModel {
        const entity: RofoModel = Object.assign(new RofoModel(), json);
        return entity;
    }

    getPoProductCode() {
        return this.http.get(this.resourceUrl + '/po-product-code').toPromise()
            .then((res) => <any[]>res.json())
            .then((data) => data);
    }

    getDataROFO(bulan: any, tahun: any): Observable<RofoModel> {
        return this.http.get(`${this.resourceUrl}/find/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getDataTarget(bulan: any, tahun: any): Observable<TargetProduksi> {
        return this.http.get(`${this.resourceUrl}/find1/${bulan}/${tahun}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getProduk(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-produk', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getProduk1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-produk1', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getProdukEdit(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-produk-edit', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getProdukEdit1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/get-target-edit', options)
            .map((res: Response) => this.convertResponse(res));
    }

    createROFO(purchasing: RofoModel[]): Observable<RofoModel> {
        const purchasingItems = this.convertItems(purchasing);
        return this.http.post(this.resourceUrl, { purchasingItems }).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    createROFO1(purchasing: TargetProduksi[]): Observable<TargetProduksi> {
        const purchasingItems = this.convertItems(purchasing);
        return this.http.post(this.resourceUrl + '/create', { purchasingItems }).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    updateROFO(purchasing: RofoModel[]): Observable<RofoModel> {
        const purchasingItems = this.convertItems(purchasing);
        const obj: any = {
            'purchasingItems': purchasingItems
        }
        const output: JSON = <JSON>obj;
        return this.http.put(this.resourceUrl + '/update-rofo-detail', output).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateROFO1(purchasing: TargetProduksi[]): Observable<TargetProduksi> {
        const purchasingItems = this.convertItems(purchasing);
        const obj: any = {
            'purchasingItems': purchasingItems
        }
        const output: JSON = <JSON>obj;
        return this.http.put(this.resourceUrl + '/update-target-produksi-detail', output).map((res: Response) => {
            const jsonResponse = res.json();
            // this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    private convertItems(items: RofoModel[]): RofoModel[] {
        const copy: RofoModel[] = Object.assign([], items);
        return copy;
    }

    find(id: any): Observable<RofoModel> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find1(id: any): Observable<RofoModel> {
        return this.http.get(`${this.resourceUrl}/${id}/find1`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(purchaseOrder: RofoModel): Observable<RofoModel> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(this.resourceUrl + '/update', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update1(purchaseOrder: TargetProduksi): Observable<TargetProduksi> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(this.resourceUrl + '/update1', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    protected convert(purchaseOrder: RofoModel): RofoModel {
        if (purchaseOrder === null) {
            return {};
        }
        // const copy: PurchaseOrder = Object.assign({}, purchaseOrder);
        const copy: RofoModel = JSON.parse(JSON.stringify(purchaseOrder));
        return copy;
    }

}
