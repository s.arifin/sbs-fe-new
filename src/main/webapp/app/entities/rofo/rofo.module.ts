import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MpmSharedModule } from '../../shared';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule,
    DataListModule,
} from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { RofoComponent } from './rofo.component';
import { RofoResolvePagingParams, TargetProduksiResolvePagingParams, rofoRoute } from './rofo.route';
import { RofoService } from './rofo.service';
import { RofoNewComponent } from './rofo-new.component';
import { RofoEditComponent } from './rofo-edit.component';
import { RofoEditNewAllComponent } from './rofo-edit-new-all.component';
import { TargetProduksiComponent } from './target-produksi.component';
import { TargetProduksiNewComponent } from './target-produksi-new.component';
import { TargetProduksiEditComponent } from './target-produksi-edit.component';
import { TargetProduksiEditAllComponent } from './target-produksi-edit-all.component';
const ENTITY_STATES = [
    ...rofoRoute
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
        DataListModule,
    ],
    declarations: [
        RofoComponent, RofoNewComponent, RofoEditComponent, RofoEditNewAllComponent, TargetProduksiComponent, TargetProduksiNewComponent,
        TargetProduksiEditComponent, TargetProduksiEditAllComponent

    ],
    entryComponents: [
        RofoComponent, RofoNewComponent, RofoEditComponent, RofoEditNewAllComponent, TargetProduksiComponent, TargetProduksiNewComponent,
        TargetProduksiEditComponent, TargetProduksiEditAllComponent

    ],
    providers: [
        RofoService,
        RofoResolvePagingParams,
        TargetProduksiResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmRofoModule { }
