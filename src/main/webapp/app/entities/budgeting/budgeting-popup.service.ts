import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Budgeting } from './budgeting.model';
import { BudgetingService } from './budgeting.service';

@Injectable()
export class BudgetingPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private budgetingService: BudgetingService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.budgetingService.find(id).subscribe((budgeting) => {
                    // if (budgeting.dtRequest) {
                    //     budgeting.dtRequest = {
                    //         year: budgeting.dtRequest.getFullYear(),
                    //         month: budgeting.dtRequest.getMonth() + 1,
                    //         day: budgeting.dtRequest.getDate()
                    //     };
                    // }
                    this.ngbModalRef = this.budgetingModalRef(component, budgeting);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.budgetingModalRef(component, new Budgeting());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    budgetingModalRef(component: Component, budgeting: Budgeting): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.budgeting = budgeting;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
