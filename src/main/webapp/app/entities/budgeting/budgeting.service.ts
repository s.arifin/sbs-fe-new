import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { Budgeting } from './budgeting.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Header } from 'primeng/primeng';
import { Subject } from 'rxjs/Subject';
import { MasterBahanIt } from '../purchase-order/index';
import { BudgetingItem } from './index';

@Injectable()
export class BudgetingService {
    protected itemValues: Budgeting[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl = process.env.API_C_URL + '/api/budgetings';
    protected resourceSearchUrl = process.env.API_C_URL + 'api/_search/budgetings';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create1(budgeting: Budgeting): Observable<Budgeting> {
        const copy = this.convert(budgeting);
        return this.http.post(this.resourceUrl + '/itemHW', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    executeProcess(params?: any, req?: any): Observable<BudgetingItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    getAllDropdown(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getalldropdown', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllDropdownSw(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/getalldropdownSw', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findBarang(id: any): Observable<MasterBahanIt> {
        return this.http.get(`${this.resourceUrl}/bahan/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    deleteitems(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/items/${id}`);
    }

    createNew(unitChecks: any): Observable<ResponseWrapper> {
        // const copy = this.convert(purchaseOrder);
        const copy = JSON.stringify(unitChecks);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceUrl}/create`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    create(budgeting: Budgeting): Observable<Budgeting> {
        const copy = this.convert(budgeting);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(budgeting: Budgeting): Observable<Budgeting> {
        const copy = this.convert(budgeting);
        return this.http.post(this.resourceUrl + '/update', copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Budgeting> {
        return this.http.get(`${this.resourceUrl}/${id}/find`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // find(id: number): Observable<Budgeting> {
    //     return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }

    queryFilterByC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    // private convertItemFromServer(entity: any) {
    //     entity.dtRequest = this.dateUtils
    //         .convertLocalDateFromServer(entity.dtRequest);
    // }

    protected convertItemFromServer(json: any): Budgeting {
        const entity: Budgeting = Object.assign(new Budgeting(), json);
        return entity;
    }

    // private convert(budgeting: Budgeting): Budgeting {
    //     const copy: Budgeting = Object.assign({}, budgeting);
    //     copy.dtRequest = this.dateUtils
    //         .convertLocalDateToServer(budgeting.dtRequest);
    //     return copy;
    // }
    protected convert(budgeting: Budgeting): Budgeting {
        if (budgeting === null || budgeting === {}) {
            return {};
        }
        // const copy: PurchaseOrder = Object.assign({}, purchaseOrder);
        const copy: Budgeting = JSON.parse(JSON.stringify(budgeting));
        return copy;
    }
}
