import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Budgeting } from './budgeting.model';
import { BudgetingPopupService } from './budgeting-popup.service';
import { BudgetingService } from './budgeting.service';

@Component({
    selector: 'jhi-budgeting-dialog',
    templateUrl: './budgeting-dialog.component.html'
})
export class BudgetingDialogComponent implements OnInit {

    budgeting: Budgeting;
    isSaving: boolean;
    dtRequestDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private budgetingService: BudgetingService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.budgeting.id !== undefined) {
            this.subscribeToSaveResponse(
                this.budgetingService.update(this.budgeting));
        } else {
            this.subscribeToSaveResponse(
                this.budgetingService.create(this.budgeting));
        }
    }

    private subscribeToSaveResponse(result: Observable<Budgeting>) {
        result.subscribe((res: Budgeting) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Budgeting) {
        this.eventManager.broadcast({ name: 'budgetingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-budgeting-popup',
    template: ''
})
export class BudgetingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private budgetingPopupService: BudgetingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.budgetingPopupService
                    .open(BudgetingDialogComponent as Component, params['id']);
            } else {
                this.budgetingPopupService
                    .open(BudgetingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
