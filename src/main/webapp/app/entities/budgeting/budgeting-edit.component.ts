import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

// import { PurchaseOrder } from './purchase-order.model';
// import { PurchaseOrderService } from './purchase-order.service';
import { Budgeting } from './budgeting.model';
import { BudgetingService } from './budgeting.service';

import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { BudgetingItemAsListComponent } from './budgeting-item-as-list.component'
@Component({
    selector: 'jhi-budgeting-edit',
    templateUrl: './budgeting-edit.component.html'
})
export class BudgetingEditComponent implements OnInit, OnDestroy {
    [x: string]: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    budgeting: Budgeting;
    isSaving: boolean;
    idRequest: any;
    paramPage: number;
    routeId: number;
    dtOrder: any;
    inputTipe: any;
    internal: any;

    tipe: Array<object> = [
        {label : 'Spare Parts', value : 'SP'},
        {label : 'Maintenace', value : 'MT'},
        {label : 'Service', value : 'SR'},
        {label : 'Fixed Asets', value : 'FA'},
        {label : 'Project', value : 'PR'}
    ];

    divitions: Array<object> = [
        {label : 'PPIC', value : 'PPIC'},
        {label : 'PROD', value : 'PROD'},
        {label : 'LPM', value : 'LPM'},
        {label : 'PURCHASING', value : 'PURCHASING'},
        {label : 'RND', value : 'RND'},
        {label : 'TEKNIK', value : 'TEKNIK'},
        {label : 'MARKETING', value : 'MARKETING'},
        {label : 'FNA', value : 'FNA'},
        {label : 'HRD', value : 'HRD'},
        {label : 'IT', value : 'IT'}
    ];

    sifats: Array<object> = [
        {label : 'Biasa', value : 'Biasa'},
        {label : 'Segera', value : 'Segera'},
        {label : 'Penting', value : 'Penting'}

    ];

    needs: Array<object> = [
        {label : 'Rutin', value : 'Rutin'},
        {label : 'Non Rutin', value : 'Non Rutin'}

    ];

    loc: Array<object> = [
        {label : 'Pusat', value : 'Pusat'},
        {label : 'Pabrik', value : 'Pabrik'}

    ];

    constructor(
        protected alertService: JhiAlertService,
        protected budgetingServce: BudgetingService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal,
        protected loadingService: LoadingService,
    ) {
        this.budgeting = new Budgeting();
        this.routeId = 0;
        this.readonly = true;
        this.inputTipe = '';
        this.inputLoc = '';
        this.inPcReqUdr5Th = 0;
        this.inPcReqNew = 0;
    }

    ngOnInit() {
        this.internal = this.principal.getIdInternal();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequest = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        // this.vendorService.query()
        //     .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.internalService.query()
        //     .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.billToService.query()
        //     .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.budgetingServce.find(this.idRequest).subscribe((budgeting) => {
            this.budgeting = budgeting;
            this.dtOrder = new Date(this.budgeting.dtOrder);
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['budgeting', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        this.loadingService.loadingStart();
        this.budgeting.dtOrder = this.dtOrder;
        if (this.budgeting.idrequest !== undefined) {
            this.subscribeToSaveResponse(
                this.budgetingServce.update(this.budgeting));
        } else {
            this.subscribeToSaveResponse(
                this.budgetingServce.create(this.budgeting));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Budgeting>) {
        result.subscribe((res: Budgeting) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Budgeting) {
        // this.print(result.idPurchaseOrder);
        this.eventManager.broadcast({ name: 'budgetingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Permintaan Pembelian saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    print(id: any) {
        const filter_data = 'idPruchaseOrder:' + id;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'Budgeting Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
