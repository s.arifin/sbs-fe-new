import { BaseEntity } from './../../shared';

export class Budgeting implements BaseEntity {
    constructor(
        public id?: number,
        public idrequest?: any,
        public requestNumber?: string,
        public dtRequest?: any,
        public dtOrder?: any,
        public reqType?: any,
        public div?: string,
        public description?: any,
        public dtCreated?: Date,
        public createdBy?: any,
        public idBudget?: any,
        public location?: any,
        public pcAstUdr5Th?: any,
        public lpAstUdr5Th?: any,
        public qtyAstPrntr?: any,
        public pcAstUpr5Th?: any,
        public lpAstUpr5Th?: any,
        public qtyPcReq?: any,
        public qtyLpReq?: any,
        public qtyPntrReq?: any,
        public pcReqUdr5Th?: any,
        public lpReqUdr5Th?: any,
        public idReqItem?: any,
        public kdBarang?: any,
        public nmBarang?: any,
        public qty?: any,
        public totalPrice?: any,
        public price?: any,
        public desc?: any,
        public type?: any,
    ) {
    }
}

export class BudgetingItem implements BaseEntity {
    constructor(
        public id?: any,
        public idRequest?: any,
        public idReqItem?: any,
        public kdBarang?: any,
        public nmBarang?: any,
        public qty?: any,
        public desc?: any,
        public totalPrice?: any,
        public price?: any,
        public pcReqUdr5Th?: any,
        public qtyPcReq?: any,
    ) {
    }
}

export class MasterBahanIt implements BaseEntity {
    constructor(
        public id?: number,
        public idMasBah?: any,
        public tipe?: any,
        public kdBahan?: string,
        public nmBahan?: string,
        public harga?: any,
        public ket?: any,
        public dtCreated?: any,
        public createdBy?: any,
    ) {

    }
}
