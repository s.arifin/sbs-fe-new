import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageHelper } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';

import { MpmSharedModule } from '../../shared';
import {
    BudgetingEditComponent,
    BudgetingService,
    BudgetingPopupService,
    BudgetingComponent,
    // BudgetingItNewComponent,
    // BudgetingItComponent,
    BudgetingDetailComponent,
    BudgetingDialogComponent,
    BudgetingPopupComponent,
    BudgetingDeletePopupComponent,
    BudgetingDeleteDialogComponent,
    budgetingRoute,
    budgetingPopupRoute,
    BudgetingResolvePagingParams,
    BudgetingItemAsListComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextareaModule,
         PaginatorModule,
         ConfirmDialogModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         TabViewModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import {BudgetingItNewComponent} from './budgeting-it-new.component'
import { MpmPurchaseOrderItemModule } from '../purchase-order-item/purchase-order-item.module';

const ENTITY_STATES = [
    ...budgetingRoute,
    ...budgetingPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        TabViewModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        SliderModule,
        RadioButtonModule,
        MpmPurchaseOrderItemModule
    ],
    exports: [
        BudgetingComponent,
        BudgetingItNewComponent,
        BudgetingEditComponent,
        BudgetingItemAsListComponent
        // BudgetingItComponent
        // BudgetingEditComponent
    ],
    declarations: [
        BudgetingComponent,
        BudgetingDetailComponent,
        BudgetingDialogComponent,
        BudgetingDeleteDialogComponent,
        BudgetingPopupComponent,
        BudgetingDeletePopupComponent,
        BudgetingItNewComponent,
        BudgetingEditComponent,
        BudgetingItemAsListComponent
        // BudgetingNewComponent,
        // BudgetingItComponent
    ],
    entryComponents: [
        BudgetingComponent,
        BudgetingDialogComponent,
        BudgetingPopupComponent,
        BudgetingDeleteDialogComponent,
        BudgetingDeletePopupComponent,
        BudgetingItNewComponent,
        BudgetingEditComponent,
        BudgetingItemAsListComponent
        // BudgetingNewComponent,
        // BudgetingItComponent
    ],
    providers: [
        BudgetingService,
        BudgetingPopupService,
        BudgetingResolvePagingParams,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmBudgetingModule {}
