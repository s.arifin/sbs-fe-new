import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
// import { BudgetingNewComponent } from './budgeting-new.component';
import { BudgetingComponent } from './budgeting.component';
// import { BudgetingItComponent } from './it-budgeting.component';
import { BudgetingDetailComponent } from './budgeting-detail.component';
import { BudgetingPopupComponent } from './budgeting-dialog.component';
import { BudgetingDeletePopupComponent } from './budgeting-delete-dialog.component';
import { BudgetingItNewComponent } from './budgeting-it-new.component';
import { BudgetingEditComponent } from './budgeting-edit.component';

@Injectable()
export class BudgetingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const budgetingRoute: Routes = [
    {
        path: 'budgeting',
        component: BudgetingComponent,
        resolve: {
            'pagingParams': BudgetingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'budgeting-it',
    //     component: BudgetingItComponent,
    //     resolve: {
    //         'pagingParams': BudgetingResolvePagingParams
    //     },
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.budgeting.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: 'budgeting/:id',
        component: BudgetingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'budgeting-new',
        component: BudgetingItNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const budgetingPopupRoute: Routes = [
    {
        path: 'budgeting-new',
        component: BudgetingItNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'budgeting/:id/edit',
        component: BudgetingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'budgeting/:route/:page/:id/edit',
        component: BudgetingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'budgeting/:id/delete',
        component: BudgetingDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.budgeting.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
