import { Component, OnInit, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// import { PurchaseOrder } from './purchase-ord er.model';
import { Budgeting, MasterBahanIt } from './budgeting.model';

// import { PurchaseOrderService } from './purchase-order.service';
import { BudgetingService } from './budgeting.service';

import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper, Principal } from '../../shared';
import { PurchaseOrderItemInPurchaseOrderAsListComponent } from '../purchase-order-item'
// import { CustomPurchaseOrderHeader, MasterBahan, CustomSupplyer, MasterBarang } from './index';
import { Header } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/index';
import { MasterBahan } from '../purchase-order/index';
@Component({
    selector: 'jhi-budgeting-it-new',
    templateUrl: './budgeting-it-new.component.html'
})
export class BudgetingItNewComponent implements OnInit, OnDestroy {
    commontUtilService: any;

    @Input() readonly = false;

    protected subscription: Subscription;
    budgeting: Budgeting;
    // header: CustomPurchaseOrderHeader;
    isSaving: boolean;
    idPurchaseOrder: any;
    paramPage: number;
    routeId: number;

    dtOrder: Date;
    inputPurchaseNumber: any;
    inputDtOrder: any;
    inputDivition: any;
    inputSifat: any;

    inputNeeds: any;
    inputAppPembelian: any;
    inputAppPabrik: any;
    inputAppDept: any;

    selectedSLNumber: string;
    selectedshipmentPackageId: string;
    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputSupplyerName: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: any;
    inputRemainingStock: any;
    inputRemainingPo: any;

    hasilSave: Budgeting;
    inputOfferNumber: any;

    internal: any;
    isOpen: Boolean = false;
    index: number;
    // supplier: CustomSupplyer;
    // Supplyer: CustomSupplyer[];
    selectedSupplier: any;
    listSupplier = [{label: 'Please Select or Insert', value: null}];

    // public barang: MasterBahan;
    // public Barang: MasterBahan[];
    public selectedBarang: any;
    public listBarang = [{label: 'Please Select or Insert', value: null}];

    // public kdbarang: MasterBahan;
    // public kdBarang: MasterBahan[];
    public selectedKdBarang: any;
    public listKdBarang = [{label: 'Please Select or Insert', value: null}];
    public listKdBarang1 = [{label: 'Please Select or Insert', value: null}];

    divitions: Array<object> = [
        {label : 'PPIC', value : 'PPIC'},
        {label : 'PROD', value : 'PROD'},
        {label : 'LPM', value : 'LPM'},
        {label : 'PURCHASING', value : 'PURCHASING'},
        {label : 'RND', value : 'RND'},
        {label : 'TEKNIK', value : 'TEKNIK'},
        {label : 'MARKETING', value : 'MARKETING'},
        {label : 'FNA', value : 'FNA'},
        {label : 'HRD', value : 'HRD'},
        {label : 'IT', value : 'IT'}
    ];

    tipe: Array<object> = [
        {label : 'Spare Parts', value : 'SP'},
        {label : 'Maintenace', value : 'MT'},
        {label : 'Service', value : 'SR'},
        {label : 'Fixed Asets', value : 'FA'},
        {label : 'Project', value : 'PR'}
    ];

    sifats: Array<object> = [
        {label : 'Biasa', value : 'Biasa'},
        {label : 'Segera', value : 'Segera'},
        {label : 'Penting', value : 'Penting'}

    ];

    loc: Array<object> = [
        {label : 'Pusat', value : 'Pusat'},
        {label : 'Pabrik', value : 'Pabrik'}

    ];

    newSuplier: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];
    pilihTipe: any;

    newKdBarang: any;
    newKdBarangs: any[];
    filteredKdBarang: any[];
    pilihKdBarang: any;

    unitChecks: Budgeting[];
    unitChecksCopy: Budgeting[];

    barangIT: MasterBahan;
    barangITs: MasterBahan[];
    inputLoc: any;
    dtRequest: any;
    inPcAstUdr5Th?: any;
    inLpAstUdr5Th?: any;
    inQtyAstPrntr?: any;
    inPcAstUpr5Th?: any;
    inLpAstUpr5Th?: any;
    inQtyPcReq?: any;
    inQtyLpReq?: any;
    inQtyPntrReq?: any;
    inPcReqUdr5Th?: any;
    inLpReqUdr5Th?: any;
    inPcReqNew?: any;
    inLpReqNew?: any;
    inKet?: any;
    inqtyTot?: any;

    kdBahanHw?: any;
    nmBahanHw?: any;
    hargaItemHw?: any;
    kdBahanSw?: any;
    nmBahanSw?: any;

    unitSw: Budgeting[];
    unitSwCopy: Budgeting[];
    itemSw?: any;
    qtySw?: any;
    priceSw?: any;
    totalPriceSw?: any;
    KetSw?: any;

    nsUnit: Budgeting[];
    nsUnitCopy: Budgeting[];
    nsItem?: any;
    nsQty?: any;
    nsPrice?: any;
    nsTotalPrice?: any;
    nsKet?: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected budgetingService: BudgetingService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        protected reportUtilService: ReportUtilService,
        private principal: Principal
    ) {
        this.budgeting = new Budgeting();
        this.routeId = 0;
        this.dtOrder = new Date();
        this.dtRequest = new Date();
        // this.barang = new MasterBahan();
        this.barangIT = new MasterBahan();
        this.isSelectSupplier = false;
        this.inputLoc = '';
        this.inPcReqUdr5Th = 0;
        this.inPcReqNew = 0;
    }
    protected resetMe() {
        this.selectedBarang = null;
    }

    public selectKdBarang(isSelect?: boolean): void {
        console.log('idbrng : ' + this.selectedKdBarang);
        this.barangIT.idMasBahan = this.selectedKdBarang;
        this.budgetingService.findBarang(this.selectedKdBarang)
        .subscribe(
            (res: MasterBahan) => {
                console.log('ini isi bahan = ', res.kdBahan);
                this.kdBahanHw = res.kdBahan;
                this.nmBahanHw = res.nmBahan;
                this.hargaItemHw = res.harga;
            },
            (res: MasterBahan) => {
                this.kdBahanHw = null;
                console.log('ini isi bahan eror = ', this.kdBahanHw);
            }
        )
    }

    public selectKdBarang1(isSelect?: boolean): void {
        console.log('idbrngSw : ' + this.itemSw);
        this.barangIT.idMasBahan = this.itemSw;
        this.budgetingService.findBarang(this.itemSw)
        .subscribe(
            (res: MasterBahan) => {
                console.log('ini isi bahan = ', res.kdBahan);
                this.kdBahanSw = res.kdBahan;
                this.nmBahanSw = res.nmBahan;
                this.priceSw = res.harga;
            },
            (res: MasterBahan) => {
                this.kdBahanSw = null;
                console.log('ini isi bahan eror = ', this.kdBahanHw);
            }
        )
    }

    onSuccessBarang(data, headers) {
        this.inputCodeName = data.kdBahan;
    }

    onErrorBarang(error) {
        this.inputCodeName = null;
        console.log('ini isi bahan eror = ', this.inputCodeName);
    }

    simpanTipe() {
        console.log('masuk ngga neehh ', this.inputLoc);
        this.pilihTipe = (this.inputLoc);
    }

    ngOnInit() {
        this.internal =  this.principal.getIdInternal();
        console.log('internal : ', this.internal);
        this.budgetingService.getAllDropdown({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.nmBahan,
                    value: element.idMasBahan });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.budgetingService.getAllDropdownSw({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang1.push({
                    label: element.nmBahan,
                    value: element.idMasBahan });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPurchaseOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.pilihTipe = this.inputLoc;
        this.unitChecks = Array<Budgeting>();
        this.unitSw = Array<Budgeting>();
        this.nsUnit = Array<Budgeting>();
        this.isSaving = false;
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        this.clearForm();
    }

    addData1(): void {
        const data = new Budgeting();
        data.idBudget = this.selectedshipmentPackageId;
        data.kdBarang = this.kdBahanHw;
        data.nmBarang = this.nmBahanHw;
        data.pcReqUdr5Th = this.inPcReqUdr5Th;
        data.qtyPcReq = this.inPcReqNew;
        this.inqtyTot = this.inPcReqUdr5Th + this.inPcReqNew;
        data.qty = this.inqtyTot;
        console.log('total qty: ', data.qty);
        data.price = this.hargaItemHw;
        data.totalPrice = data.qty * this.hargaItemHw;
        data.desc = this.inKet;
        this.unitChecks = [...this.unitChecks, data];
        this.clearDataInput();
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    addData2(): void {
        const data1 = new Budgeting();
        data1.idBudget = this.selectedshipmentPackageId;
        data1.kdBarang = this.kdBahanSw;
        data1.nmBarang = this.nmBahanSw;
        data1.qty = this.qtySw;
        console.log('total qty: ', data1.qty);
        data1.price = this.priceSw;
        data1.totalPrice = data1.qty * this.priceSw;
        data1.desc = this.KetSw;
        this.unitSw = [...this.unitSw, data1];
        this.clearDataInput1();
        if (this.unitSw.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    addData3(): void {
        const data2 = new Budgeting();
        data2.idBudget = this.selectedshipmentPackageId;
        data2.nmBarang = this.nsItem;
        data2.qty = this.nsQty;
        console.log('total qty: ', data2.qty);
        data2.price = this.nsPrice;
        data2.totalPrice = data2.qty * this.nsPrice;
        data2.desc = this.nsKet;
        this.nsUnit = [...this.nsUnit, data2];
        this.clearDataInput2();
        if (this.nsUnit.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    clearBack() {
        this.activeModal.dismiss('cancel');
        this.previousState();
    }

    clearDataInput(): void {
        this.selectedKdBarang = null;
        this.kdBahanHw = null;
        this.nmBahanHw = null;
        this.kdBahanSw = null;
        this.nmBahanSw = null;
        this.inPcReqUdr5Th = 0;
        this.inPcReqNew = 0;
        this.hargaItemHw = null;
        this.inqtyTot = null;
        this.hargaItemHw = null;
        this.inKet = null;
    }

    clearDataInput1(): void {
        this.selectedshipmentPackageId = null;
        this.kdBahanSw = null;
        this.nmBahanSw = null;
        this.qtySw = 0;
        this.priceSw = 0;
        this.KetSw = null;
    }

    clearDataInput2(): void {
        this.nsItem = null;
        this.nsQty = null;
        this.nsPrice = null;
        this.nsKet = null;
    }

    protected clearForm(): void {
        this.clearDataInput();
        this.clearDataInput1();
        this.clearDataInput2();
        this.selectedSLNumber = null;
        this.selectedshipmentPackageId = null;
        this.index = null;
    }

    addDataBtnDisable(): boolean {
        if ((this.selectedKdBarang !== '' &&
            this.selectedKdBarang !== undefined &&
            this.selectedKdBarang != null)) {

            return false;
        } else { return true; }
    }
    addDataBtnDisable2(): boolean {
        if ((this.itemSw !== '' &&
            this.itemSw !== undefined &&
            this.itemSw != null)) {

            return false;
        } else { return true; }
    }
    addDataBtnDisable3(): boolean {
        if ((this.nsItem !== '' &&
            this.nsItem !== undefined &&
            this.nsItem != null)) {

            return false;
        } else { return true; }
    }

    removeData(index: number): void {
        this.unitChecksCopy = Object.assign([], this.unitChecks);
        this.unitChecksCopy.splice(index, 1);
        this.unitChecks = new Array<Budgeting>();
        this.unitChecks = this.unitChecksCopy;
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    removeData1(index: number): void {
        this.unitSwCopy = Object.assign([], this.unitSw);
        this.unitSwCopy.splice(index, 1);
        this.unitSw = new Array<Budgeting>();
        this.unitSw = this.unitSwCopy;
        if (this.unitSw.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    removeData2(index: number): void {
        this.nsUnitCopy = Object.assign([], this.nsUnit);
        this.nsUnitCopy.splice(index, 1);
        this.nsUnit = new Array<Budgeting>();
        this.nsUnit = this.nsUnitCopy;
        if (this.nsUnit.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    load() {
        // this.filterBarangSingle(event);
        // this.filterKdBarangSingle(event);

    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['budgeting', { page: this.paramPage }]);
        }
    }
    // const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;

    public updateCalcs(isSelect?: boolean): void {
        const waktu = '00:00:00.000';
        this.inputDtOrder = this.dtOrder.getFullYear() + '-' + (this.dtOrder.getMonth() + 1) + '-' + this.dtOrder.getDate() + ' ' + waktu;

        console.log('masuk change date : ', this.inputDtOrder);
    }

    save() {
        this.inputDtOrder = this.dtOrder.getFullYear() + '-' + (this.dtOrder.getMonth() + 1) + '-' + this.dtOrder.getDate();
        console.log('data save date: ', this.dtOrder);
        const obj = {
            location: this.inputLoc,
            dtOrder: this.inputDtOrder,
            div: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            pcAstUdr5Th: this.inPcAstUdr5Th,
            pcAstUpr5Th: this.inPcAstUpr5Th,
            qtyAstPrntr: this.inQtyAstPrntr,
            lpAstUdr5Th: this.inLpAstUdr5Th,
            lpAstUpr5Th: this.inLpAstUpr5Th,
            data1: this.unitChecks,
            data2: this.unitSw,
            data3: this.nsUnit
        }
        console.log('data save obj: ', obj.dtOrder);
        if (this.inputLoc == null) {
            this.toaster.showToaster('info', 'Save', 'Please Select Location!');
        } else if (this.unitChecks == null) {
            this.toaster.showToaster('info', 'Save', 'Please Insert Detail Komputer!');
        } else {
            this.loadingService.loadingStart();
            this.budgetingService.createNew(obj).subscribe(
                (res: ResponseWrapper) =>
                this.onSaveSuccess(res, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
        }
    }

    // protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
    //     result.subscribe((res: PurchaseOrder) =>
    //         this.onSaveSuccess(res, headers), (res: Response) => this.onSaveError(res));
    // }

    print(id: any) {
        const user = this.principal.getUserLogin();

        const filter_data = 'idPruchaseOrder:' + id;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
        // this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PurchaseOrder/pdf', {filterData: filter_data});
    }

    private onSaveSuccess(data, headers) {
        this.eventManager.broadcast({ name: 'budgetingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Budget saved !');
        this.isSaving = false;
        // this.print(data[0].idPurchaseOrder);
        this.clear();
        this.previousState();
        this.loadingService.loadingStop();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'Budget Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

}
