import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { MasterBahan } from '../purchase-order/index';
// import { PurchaseOrderItemService } from './purchase-order-item.service';
import { BudgetingService } from './budgeting.service';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

// import { PurchaseOrderItemPopupService } from './purchase-order-item-popup.service';
// import { PurchaseOrderService, PurchaseOrder, CustomSupplyer, MasterBahan, MasterBarang } from '../purchase-order';
import { CommonUtilService } from '../../shared';
import { InternalService } from '../internal';
import { BudgetingItem, Budgeting, MasterBahanIt } from './budgeting.model';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    selector: 'jhi-budgeting-item-as-list',
    templateUrl: './budgeting-item-as-list.component.html'
})
export class BudgetingItemAsListComponent implements OnInit, OnDestroy, OnChanges {
    dtOrder: Date;
    @Input() idRequest: any;
    @Input() idReqItem: any;

    eventSubscriber: Subscription;
    paramPage: number;
    routeId: number;
    budgetingItem: BudgetingItem;
    isSaving: boolean;
    itemType: number;
    budgetings: Budgeting[];
    currentAccount: any;
    budgetingItems: BudgetingItem[];
    budgeting: Budgeting;
    error: any;
    success: any;
    subscription: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    newModal: boolean;
    newModal1: boolean;
    newModal2: boolean;
    routeData: any;
    currentSearch: string;
    isOpen: Boolean = false;
    index: number;

    inputCodeName: string;
    inputProdukName: string;
    inputQty: any;
    inputDtOffer: any;
    inputOfferNumber: any;
    inputSupplyerName: string;
    inputSupplyerPrice: any;
    inputNecessity: any;
    inputDtNecessity: any;
    inputRemainingStock: any;
    inputRemainingPo: any;

    unitChecks: Budgeting[];
    unitChecksCopy: Budgeting[];
    hasilSave: Budgeting;
    // supplier: CustomSupplyer;
    // Supplyer: CustomSupplyer[];
    selectedSupplier: any;
    listSupplier = [{label: 'Please Select or Insert', value: null}];

    public barang: MasterBahanIt;
    public Barangs: MasterBahanIt[];
    public selectedBarang: any;
    public listBarang = [{label: 'Please Select or Insert', value: null}];

    public kdbarang: MasterBahanIt;
    public kdBarangs: MasterBahanIt[];
    public selectedKdBarang: any;
    public listKdBarang = [{label: 'Please Select or Insert', value: null}];
    public listKdBarang1 = [{label: 'Please Select or Insert', value: null}];

    newSuplier: any;
    newSupliers: any[];
    filteredSuppliers: any[];
    isSelectSupplier: boolean;
    newBarang: any;
    newBarangs: any[];
    filteredBarang: any[];

    newKdBarang: any;
    newKdBarangs: any[];
    filteredKdBarang: any[];
    pilihKdBarang: any;
    internal: any;

    inputLoc: any;
    dtRequest: any;
    inPcAstUdr5Th?: any;
    inLpAstUdr5Th?: any;
    inQtyAstPrntr?: any;
    inPcAstUpr5Th?: any;
    inLpAstUpr5Th?: any;
    inQtyPcReq?: any;
    inQtyLpReq?: any;
    inQtyPntrReq?: any;
    inPcReqUdr5Th?: any;
    inLpReqUdr5Th?: any;
    inPcReqNew?: any;
    inLpReqNew?: any;
    inKet?: any;
    inqtyTot?: any;

    kdBahanHw?: any;
    nmBahanHw?: any;
    hargaItemHw?: any;
    kdBahanSw?: any;
    nmBahanSw?: any;

    unitSw: Budgeting[];
    unitSwCopy: Budgeting[];
    itemSw?: any;
    qtySw?: any;
    priceSw?: any;
    totalPriceSw?: any;
    KetSw?: any;

    nsUnit: Budgeting[];
    nsUnitCopy: Budgeting[];
    nsItem?: any;
    nsQty?: any;
    nsPrice?: any;
    nsTotalPrice?: any;
    nsKet?: any;

    barangIT: MasterBahan;
    barangITs: MasterBahan[];

    pilihTipe: any;
    tp: any;
    tothw: any;
    totsw: any;
    totns: any;

    constructor(
        protected budgetingService: BudgetingService,
        // protected purchaseOrderItemService: PurchaseOrderItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected loadingService: LoadingService,
        protected toasterService: ToasterService,

        protected route: ActivatedRoute,
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected toaster: ToasterService,
        protected commontUtilService: CommonUtilService,
        protected internalService: InternalService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idPurOrdItem';
        this.reverse = 'asc';
        this.budgeting = new Budgeting();
        this.newModal = false;
        this.newModal1 = false;
        this.newModal2 = false;
        this.budgetingItem = new BudgetingItem();
        this.dtOrder = new Date();
        this.barang = new MasterBahanIt();
        this.isSelectSupplier = false;
        this.kdbarang = new MasterBahanIt();
        this.inPcReqUdr5Th = 0;
        this.inPcReqNew = 0;
        this.tothw = 0;
        this.totsw = 0;
        this.totns = 0;
    }
    protected resetMe() {
        this.selectedBarang = null;
    }

    public selectKdBarang(isSelect?: boolean): void {
        console.log('idbrng : ' + this.selectedKdBarang);
        this.barang.idMasBah = this.selectedKdBarang;
        this.budgetingService.findBarang(this.selectedKdBarang)
        .subscribe(
            (res: MasterBahan) => {
                console.log('ini isi bahan = ', res.kdBahan);
                this.kdBahanHw = res.kdBahan;
                this.nmBahanHw = res.nmBahan;
                this.hargaItemHw = res.harga;
            },
            (res: MasterBahan) => {
                this.kdBahanHw = null;
                console.log('ini isi bahan eror = ', this.kdBahanHw);
            }
        )
    }

    selectKdBarang1(isSelect?: boolean): void {
        console.log('idbrngSw : ' + this.itemSw);
        this.barang.idMasBah = this.itemSw;
        this.budgetingService.findBarang(this.itemSw)
        .subscribe(
            (res: MasterBahan) => {
                console.log('ini isi bahan = ', res.kdBahan);
                this.kdBahanSw = res.kdBahan;
                this.nmBahanSw = res.nmBahan;
                this.priceSw = res.harga;
            },
            (res: MasterBahan) => {
                this.kdBahanSw = null;
                console.log('ini isi bahan eror = ', this.kdBahanHw);
            }
        )
    }

    addNewData(): void {
        this.newModal = true;
        this.budgeting = new Budgeting();
    }
    addNewData1(): void {
        this.newModal1 = true;
        this.budgeting = new Budgeting();
    }
    addNewData2(): void {
        this.newModal2 = true;
        this.budgeting = new Budgeting();
    }

    addDataBtnDisable(): boolean {
        if ((this.selectedKdBarang !== '' &&
            this.selectedKdBarang !== undefined &&
            this.selectedKdBarang != null)) {

            return false;
        } else { return true; }
    }
    addDataBtnDisable2(): boolean {
        if ((this.itemSw !== '' &&
            this.itemSw !== undefined &&
            this.itemSw != null)) {

            return false;
        } else { return true; }
    }
    addDataBtnDisable3(): boolean {
        if ((this.nsItem !== '' &&
            this.nsItem !== undefined &&
            this.nsItem != null)) {

            return false;
        } else { return true; }
    }

    removeData(index: number): void {
        this.unitChecksCopy = Object.assign([], this.unitChecks);
        this.unitChecksCopy.splice(index, 1);
        this.unitChecks = new Array<Budgeting>();
        this.unitChecks = this.unitChecksCopy;
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    removeData1(index: number): void {
        this.unitSwCopy = Object.assign([], this.unitSw);
        this.unitSwCopy.splice(index, 1);
        this.unitSw = new Array<Budgeting>();
        this.unitSw = this.unitSwCopy;
        if (this.unitSw.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    removeData2(index: number): void {
        this.nsUnitCopy = Object.assign([], this.nsUnit);
        this.nsUnitCopy.splice(index, 1);
        this.nsUnit = new Array<Budgeting>();
        this.nsUnit = this.nsUnitCopy;
        if (this.nsUnit.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    onSuccessBarang(data, headers) {
        this.inputCodeName = data.kdBahan;
    }

    onErrorBarang(error) {
        this.inputCodeName = null;
        console.log('ini isi bahan eror = ', this.inputCodeName);
    }

    simpanTipe() {
        console.log('masuk ngga neehh ', this.inputLoc);
        this.pilihTipe = (this.inputLoc);
    }

    calculate() {
        // for (let item of this.unitChecks) {
        //     this.tothw = this.tothw + item.totalPrice
        // }
        // console.log('totalhw :', this.tothw);
    }

    loadAll() {
        this.budgetingItem = new BudgetingItem();
        this.budgeting = new Budgeting();
        this.loadingService.loadingStart();
        this.budgetingService.queryFilterBy({
            query: 'idInternal:' + this.idRequest,
            ty: 'HW',
            idPurOrdItem: this.idReqItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
                this.tothw = 0;
                for (const item of res.json) {
                    this.tothw = this.tothw + item.totalPrice
                    console.log('totalhw :', this.tothw);
                }
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.budgetingService.queryFilterBy({
            query: 'idInternal:' + this.idRequest,
            ty: 'SW',
            idPurOrdItem: this.idReqItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess1(res.json, res.headers)
                this.totsw = 0;
                for (const item of res.json) {
                    this.totsw = this.totsw + item.totalPrice
                }
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.budgetingService.queryFilterBy({
            query: 'idInternal:' + this.idRequest,
            ty: 'NS',
            idPurOrdItem: this.idReqItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess2(res.json, res.headers)
                this.totns = 0;
                for (const item of res.json) {
                    this.totns = this.totns + item.totalPrice
                }
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.unitChecks = Array<Budgeting>();
        this.nsUnit = Array<Budgeting>();
        this.unitSw = Array<Budgeting>();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }
    clear() {
        this.page = 0;
        this.router.navigate(['/budgeting-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.activeModal.dismiss('cancel');
        this.loadAll();
    }

    ngOnInit() {

        this.internal =  this.principal.getIdInternal();
        console.log('internal : ', this.internal);
        this.budgetingService.getAllDropdown({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang.push({
                    label: element.nmBahan,
                    value: element.idMasBahan });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.budgetingService.getAllDropdownSw({
            page: 0,
            size: 10000,
            sort: ['kdBahan', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.newKdBarang = res.json;
            this.newKdBarang.forEach((element) => {
                this.listKdBarang1.push({
                    label: element.nmBahan,
                    value: element.idMasBahan });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            }
        );

        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequest = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.registerChangeInBillingItems();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.isSaving = false;
    }

    load() {
        // this.filterBarangSingle(event);
        // this.filterKdBarangSingle(event);

    }

    clearDataInput(): void {
        this.selectedKdBarang = null;
        this.kdBahanHw = null;
        this.nmBahanHw = null;
        this.kdBahanSw = null;
        this.nmBahanSw = null;
        this.inPcReqUdr5Th = 0;
        this.inPcReqNew = 0;
        this.hargaItemHw = null;
        this.inqtyTot = null;
        this.hargaItemHw = null;
        this.inKet = null;
        this.qtySw = 0;
        this.priceSw = 0;
        this.KetSw = null;
        this.nsItem = null;
        this.nsQty = null;
        this.nsPrice = null;
        this.nsKet = null;
    }

    clearDataInput1(): void {
        this.kdBahanSw = null;
        this.nmBahanSw = null;
        this.qtySw = 0;
        this.priceSw = 0;
        this.KetSw = null;
    }

    clearDataInput2(): void {
        this.nsItem = null;
        this.nsQty = null;
        this.nsPrice = null;
        this.nsKet = null;
    }

    protected clearForm(): void {
        this.clearDataInput();
        this.clearDataInput1();
        this.clearDataInput2();
        this.index = null;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idRequest']) {
            this.loadAll();
        }
        if (changes['idReqItem']) {
            this.loadAll();
        }
        // if (changes['idInventoryItem']) {
        //     this.loadAll();
        // }
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: BudgetingItem) {
        return item.idReqItem;
    }
    // protected registerChangeInBillingItems(): void {
    //     this.eventSubscriber = this.billingItemService.values.subscribe(
    //         (res) => {
    //             this.billingItems = res;
    //         }
    //     )
    // }
    registerChangeInBillingItems() {
        console.log('load data reqChance');
        this.eventSubscriber = this.eventManager.subscribe('BudgetingItemListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idReqItem') {
            result.push('idReqItem');
        }
        return result;
    }

    protected onSuccess1(data, headers) {
        if (data.count > 0) {
            this.totalItems = data.TotalData;
        } else {
            this.totalItems = 0;
        }
        this.queryCount = this.totalItems;
        this.budgetingItems = data;
        this.unitSw = data;
        this.loadingService.loadingStop();
        console.log('ini data internal', data);
    }

    protected onSuccess2(data, headers) {
        if (data.count > 0) {
            this.totalItems = data.TotalData;
        } else {
            this.totalItems = 0;
        }
        this.queryCount = this.totalItems;
        this.budgetingItems = data;
        this.nsUnit = data;
        this.loadingService.loadingStop();
        console.log('ini data internal', data);
    }

    protected onSuccess(data, headers) {
        if (data.count > 0) {
            this.totalItems = data.TotalData;
        } else {
            this.totalItems = 0;
        }
        this.queryCount = this.totalItems;
        this.budgetingItems = data;
        this.unitChecks = data;
        this.loadingService.loadingStop();
        console.log('ini data internal', data);
    }

    protected onError(error: any) {
        // this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'billingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.budgetingService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.budgetingService.update(event.data)
                .subscribe((res: BudgetingItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.budgetingService.create(event.data)
                .subscribe((res: BudgetingItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: BudgetingItem) {
        this.toasterService.showToaster('info', 'budgetingItem Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    // save() {
    //     this.newModal = false;
    //     this.loadingService.loadingStart();
    //     this.budgetingItem.idRequest = this.idRequest;
    //     this.budgetingItem.kdBarang = this.inputCodeName;
    //     this.isSaving = true;
    //     if (this.budgetingItem.kdBarang !== undefined && this.budgetingItem.nmBarang !== undefined) {
    //         if (this.budgetingItem.idReqItem !== undefined) {
    //             this.subscribeToSaveResponse(
    //                 this.budgetingService.update(this.budgetingItem));
    //         } else {
    //             this.subscribeToSaveResponse(
    //                 this.budgetingService.create(this.budgetingItem));
    //         }
    //     } else {
    //         this.toaster.showToaster('warning', 'Purchase Request Changed', 'Data Belum Lengkap.');
    //     }
    // }

    addData3() {
        this.loadingService.loadingStart();
        this.budgeting.idrequest = this.idRequest;
        this.budgeting.nmBarang = this.nsItem;
        this.budgeting.qty = this.nsQty;
        this.budgeting.price = this.nsPrice;
        this.budgeting.totalPrice = this.nsQty * this.nsPrice;
        this.budgeting.desc = this.nsKet;
        this.budgeting.type = 'NS'
        this.isSaving = true;
        if (this.budgeting.idrequest !== undefined) {
            this.subscribeToSaveResponse(
                this.budgetingService.create1(this.budgeting));
        } else {
            this.toaster.showToaster('warning', 'Budgeting Changed', 'Data Belum Lengkap.');
        }
    }
    addData2() {
        this.loadingService.loadingStart();
        this.budgeting.idrequest = this.idRequest;
        this.budgeting.kdBarang = this.kdBahanSw;
        this.budgeting.nmBarang = this.nmBahanSw;
        this.budgeting.qty = this.qtySw;
        this.budgeting.price = this.priceSw;
        this.budgeting.totalPrice = this.qtySw * this.priceSw;
        this.budgeting.desc = this.KetSw;
        this.budgeting.type = 'SW'
        this.isSaving = true;
        if (this.budgeting.idrequest !== undefined) {
            this.subscribeToSaveResponse(
                this.budgetingService.create1(this.budgeting));
        } else {
            this.toaster.showToaster('warning', 'Budgeting Changed', 'Data Belum Lengkap.');
        }
    }

    addData1() {
        this.loadingService.loadingStart();
        this.budgeting.idrequest = this.idRequest
        this.budgeting.kdBarang = this.kdBahanHw;
        this.budgeting.nmBarang = this.nmBahanHw;
        this.budgeting.pcReqUdr5Th = this.inPcReqUdr5Th;
        this.budgeting.qtyPcReq = this.inPcReqNew;
        this.inqtyTot = this.inPcReqUdr5Th + this.inPcReqNew;
        this.budgeting.qty = this.inqtyTot;
        this.budgeting.price = this.hargaItemHw;
        this.budgeting.totalPrice = this.budgeting.qty * this.hargaItemHw;
        this.budgeting.desc = this.inKet;
        this.budgeting.type = 'HW';
        this.isSaving = true;
        if (this.budgeting.idrequest !== undefined) {
            this.subscribeToSaveResponse(
                this.budgetingService.create1(this.budgeting));
        } else {
            this.toaster.showToaster('warning', 'Budgeting Changed', 'Data Belum Lengkap.');
        }
    }

    protected subscribeToSaveResponse(result: Observable<Budgeting>) {
        result.subscribe((res: Budgeting) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Budgeting) {
        this.loadingService.loadingStop();
        this.eventManager.broadcast({
            name: 'BudgetingItemListModification',
            content: 'OK'
        });
        this.toaster.showToaster('info', 'Save', 'Budgeting Item saved !');
        this.isSaving = false;
        this.budgetingItem = new Budgeting();
        this.activeModal.dismiss(result);
        this.isSelectSupplier = false;
        this.clearDataInput();
        this.newModal = false;
        this.newModal1 = false;
        this.newModal2 = false;
    }

    protected onSaveError() {
        this.loadingService.loadingStop();
        this.isSaving = false;
        this.isSelectSupplier = false;
    }

    trackBillingById(index: number, item: Budgeting) {
        return item.idrequest;
    }

    delete(id: any) {
        console.log('idreqitem: ', id);
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.budgetingService.deleteitems(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'BudgetingItemListModification',
                        content: 'Deleted an Budgeting Item'
                    });
                });
            }
        });
    }
}
