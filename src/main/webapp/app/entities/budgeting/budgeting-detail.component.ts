import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Budgeting } from './budgeting.model';
import { BudgetingService } from './budgeting.service';

@Component({
    selector: 'jhi-budgeting-detail',
    templateUrl: './budgeting-detail.component.html'
})
export class BudgetingDetailComponent implements OnInit, OnDestroy {

    budgeting: Budgeting;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private budgetingService: BudgetingService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBudgetings();
    }

    load(id) {
        this.budgetingService.find(id).subscribe((budgeting) => {
            this.budgeting = budgeting;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBudgetings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'budgetingListModification',
            (response) => this.load(this.budgeting.id)
        );
    }
}
