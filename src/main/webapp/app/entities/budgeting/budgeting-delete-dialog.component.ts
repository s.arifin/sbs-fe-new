import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Budgeting } from './budgeting.model';
import { BudgetingPopupService } from './budgeting-popup.service';
import { BudgetingService } from './budgeting.service';

@Component({
    selector: 'jhi-budgeting-delete-dialog',
    templateUrl: './budgeting-delete-dialog.component.html'
})
export class BudgetingDeleteDialogComponent {

    budgeting: Budgeting;

    constructor(
        private budgetingService: BudgetingService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.budgetingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'budgetingListModification',
                content: 'Deleted an budgeting'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-budgeting-delete-popup',
    template: ''
})
export class BudgetingDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private budgetingPopupService: BudgetingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.budgetingPopupService
                .open(BudgetingDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
