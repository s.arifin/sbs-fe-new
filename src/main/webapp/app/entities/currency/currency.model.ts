import { BaseEntity } from './../../shared';

export class Currency implements BaseEntity {
    constructor(
        public id?: number,
        public idPurchaseOrder?: number,
        public noPurchaseOrder?: string,
        public dtOrder?: any,
        public divition?: string,
    ) {
    }
}
