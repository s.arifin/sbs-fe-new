import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MasterDistributor } from './master-distributor.model';
import { MasterDistributorPopupService } from './master-distributor-popup.service';
import { MasterDistributorService } from './master-distributor.service';

@Component({
    selector: 'jhi-master-distributor-delete-dialog',
    templateUrl: './master-distributor-delete-dialog.component.html'
})
export class MasterDistributorDeleteDialogComponent {

    masterDistributor: MasterDistributor;

    constructor(
        private masterDistributorService: MasterDistributorService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.masterDistributorService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'masterDistributorListModification',
                content: 'Deleted an masterDistributor'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-master-distributor-delete-popup',
    template: ''
})
export class MasterDistributorDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private masterDistributorPopupService: MasterDistributorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.masterDistributorPopupService
                .open(MasterDistributorDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
