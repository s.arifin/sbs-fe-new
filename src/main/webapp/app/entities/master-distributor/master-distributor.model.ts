import { BaseEntity } from './../../shared';

export class MasterDistributor implements BaseEntity {
    constructor(
        public id?: number,
        public cust_code?: string,
        public cust_name?: string,
        public cust_nick?: string,
        public cust_addr?: string,
        public cust_addr2?: string,
        public cust_addr3?: string,
        public cust_send?: string,
        public cust_send2?: string,
        public cust_send3?: string,
        public cust_phone?: string,
        public cust_pic?: string,
        public cust_limit?: number,
    ) {
    }
}
