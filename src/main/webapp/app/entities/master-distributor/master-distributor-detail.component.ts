import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { MasterDistributor } from './master-distributor.model';
import { MasterDistributorService } from './master-distributor.service';

@Component({
    selector: 'jhi-master-distributor-detail',
    templateUrl: './master-distributor-detail.component.html'
})
export class MasterDistributorDetailComponent implements OnInit, OnDestroy {

    masterDistributor: MasterDistributor;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private masterDistributorService: MasterDistributorService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        // this.subscription = this.route.params.subscribe((params) => {
        //     this.load(params['id']);
        // });
        console.log('cek masuk detail distributtor ==');
        this.load();
        this.registerChangeInMasterDistributors();
    }

    // load(id) {
    //     this.masterDistributorService.find(id).subscribe((masterDistributor) => {
    //         this.masterDistributor = masterDistributor;
    //     });
    // }
    load() {
        this.masterDistributorService.findOne().subscribe((masterDistributor) => {
            this.masterDistributor = masterDistributor;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMasterDistributors() {
        this.eventSubscriber = this.eventManager.subscribe(
            'masterDistributorListModification',
            (response) => this.load() // this.load(this.masterDistributor.id)
        );
    }
    update() {
        this.masterDistributorService.update(this.masterDistributor)
        .subscribe(
            (res) => console.log('Data Berhasil Diupdate')
        );
    }
}
