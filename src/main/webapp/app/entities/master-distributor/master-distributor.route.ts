import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MasterDistributorComponent } from './master-distributor.component';
import { MasterDistributorDetailComponent } from './master-distributor-detail.component';
import { MasterDistributorPopupComponent } from './master-distributor-dialog.component';
import { MasterDistributorDeletePopupComponent } from './master-distributor-delete-dialog.component';

@Injectable()
export class MasterDistributorResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const masterDistributorRoute: Routes = [
    {
        path: 'master-distributor',
        component: MasterDistributorComponent,
        resolve: {
            'pagingParams': MasterDistributorResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterDistributor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'master-distributor/:id',
        component: MasterDistributorDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterDistributor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'master-distributor-one',
        component: MasterDistributorDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterDistributor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const masterDistributorPopupRoute: Routes = [
    {
        path: 'master-distributor-new',
        component: MasterDistributorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterDistributor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-distributor/:id/edit',
        component: MasterDistributorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterDistributor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-distributor/:id/delete',
        component: MasterDistributorDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterDistributor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
