import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MasterDistributor } from './master-distributor.model';
import { MasterDistributorPopupService } from './master-distributor-popup.service';
import { MasterDistributorService } from './master-distributor.service';

@Component({
    selector: 'jhi-master-distributor-dialog',
    templateUrl: './master-distributor-dialog.component.html'
})
export class MasterDistributorDialogComponent implements OnInit {

    masterDistributor: MasterDistributor;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private masterDistributorService: MasterDistributorService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.masterDistributor.id !== undefined) {
            this.subscribeToSaveResponse(
                this.masterDistributorService.update(this.masterDistributor));
        } else {
            this.subscribeToSaveResponse(
                this.masterDistributorService.create(this.masterDistributor));
        }
    }

    private subscribeToSaveResponse(result: Observable<MasterDistributor>) {
        result.subscribe((res: MasterDistributor) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: MasterDistributor) {
        this.eventManager.broadcast({ name: 'masterDistributorListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-master-distributor-popup',
    template: ''
})
export class MasterDistributorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private masterDistributorPopupService: MasterDistributorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.masterDistributorPopupService
                    .open(MasterDistributorDialogComponent as Component, params['id']);
            } else {
                this.masterDistributorPopupService
                    .open(MasterDistributorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
