import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
    DataTableModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    RadioButtonModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    InputTextareaModule,
    TabViewModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    InputMaskModule
} from 'primeng/primeng';

import { MpmSharedModule } from '../../shared';
import {
    MasterDistributorService,
    MasterDistributorPopupService,
    MasterDistributorComponent,
    MasterDistributorDetailComponent,
    MasterDistributorDialogComponent,
    MasterDistributorPopupComponent,
    MasterDistributorDeletePopupComponent,
    MasterDistributorDeleteDialogComponent,
    masterDistributorRoute,
    masterDistributorPopupRoute,
    MasterDistributorResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...masterDistributorRoute,
    ...masterDistributorPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        DataTableModule,
        ButtonModule,
        CalendarModule,
        ConfirmDialogModule,
        CurrencyMaskModule,
        AutoCompleteModule,
        RadioButtonModule,
        TooltipModule,
        CheckboxModule,
        DialogModule,
        PdfViewerModule,
        InputTextareaModule,
        TabViewModule,
        DropdownModule,
        FieldsetModule,
        AccordionModule,
        InputMaskModule,
    ],
    declarations: [
        MasterDistributorComponent,
        MasterDistributorDetailComponent,
        MasterDistributorDialogComponent,
        MasterDistributorDeleteDialogComponent,
        MasterDistributorPopupComponent,
        MasterDistributorDeletePopupComponent,
    ],
    entryComponents: [
        MasterDistributorComponent,
        MasterDistributorDialogComponent,
        MasterDistributorPopupComponent,
        MasterDistributorDeleteDialogComponent,
        MasterDistributorDeletePopupComponent,
    ],
    providers: [
        MasterDistributorService,
        MasterDistributorPopupService,
        MasterDistributorResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterDistributorModule {}
