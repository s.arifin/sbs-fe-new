import './vendor.ts';

import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { MpmSharedModule, UserRouteAccessService, Principal, ToasterService } from './shared';
import { MpmHomeModule } from './home/home.module';
import { MpmAdminModule } from './admin/admin.module';
import { MpmAccountModule } from './account/account.module';
import { MpmEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CommonModule } from '@angular/common';
import { GrowlModule, ConfirmationService, AccordionModule } from 'primeng/primeng';
import { CoreUIModule } from './dashboard';
import { LoadingBarHttpModule } from '@ngx-loading-bar/http';
// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    InfoModuleComponent,
    LoadingComponent,
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    LoadingService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

import { ReportUtilService } from './shared/report/report-util.service';
// nuse
import { MpmSharedEntityModule } from './entities/shared-entity.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        NgbModule.forRoot(),
        LoadingBarHttpModule,
        CoreUIModule,
        BrowserAnimationsModule,
        CommonModule,
        GrowlModule,
        MpmSharedModule,
        MpmHomeModule,
        MpmAdminModule,
        MpmAccountModule,
        MpmEntityModule,
        MpmSharedEntityModule,
        AccordionModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        LoadingComponent,
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
        InfoModuleComponent,
    ],
    providers: [
        LoadingService,
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService,
        ConfirmationService,
        ReportUtilService,
        ToasterService,
        // nuse
        Principal,
    ],
    bootstrap: [ JhiMainComponent ]
})
export class MpmAppModule {}
