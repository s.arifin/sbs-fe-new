// Active = 10
// Request Faktur = 31
// Receipt Faktur = 32
// Receipt Revised Faktur = 33
// Request Biro Jasa = 34
// Request Revised Biro Jasa = 35
// Receipt BPKB & STNK = 36
// Receipt Revised BPKB & STNK = 37

// Request Luar Wilayah = 38
// Receipt Luar Wilayah = 39

// BAST = 40
// Close / Complete = 17

export const REQUEST_FAKTUR = 31;
export const RECEIPT_FAKTUR = 32;
export const RECEIPT_REV_FAKTUR = 33;
export const REQUEST_BIRO_JASA = 34;
export const REQUEST_REV_BIRO_JASA = 35;
export const RECEIPT_BPKB_STNK = 36;
export const RECEIPT_REV_BPKB_STNK = 37;
export const REQUEST_LUAR = 38;
export const RECEIPT_LUAR = 39;
export const BAST = 40;
export const CANCEL_PENGAJUAN = 41;
export const CANCEL_FAKTUR = 42;
export const MULIA = 43;
export const NON_MULIA = 44;
export const REVISI_FAKTUR = 45;
export const RECEIVE = 46;
