/*
    ini adalah master constants untuk authority.

    untuk menggunakan nya di base.constants ada authority by segment jadi baiknya yang digunakan adalah yang di base constants
*/

export const ROLE_KACAB = 'ROLE_KACAB';
export const ROLE_SALESMAN = 'ROLE_SALESMAN';
export const ROLE_KORSAL = 'ROLE_KORSAL';
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_ADMINSALES = 'ROLE_ADMINSALES';
export const ROLE_AFC = 'ROLE_AFC';
export const ROLE_KABENG = 'ROLE_KABENG';
export const ROLE_USER = 'ROLE_USER';
