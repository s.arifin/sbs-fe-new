import { Pipe, PipeTransform } from '@angular/core';
import * as BaseConstants from '../constants/base.constants';

@Pipe({
    name : 'Message'
})
export class MessagePipe implements PipeTransform {
    transform(value: string, type: string): string {
        let result: string = value;
        if (type === 'motor') {
            if (value === null) {
                result = 'Please select motor';
            };
        } else if (type === 'color') {
            if (value === null) {
                result = 'Please select color';
            };
        } else if (type === 'empty') {
            if (value === null) {
                result = '-';
            }
        } else {
            result = 'error parse message';
        }

        return result;
    }
}

@Pipe({
    name : 'MessageApprovalCategory'
})
export class MessageApprovalCategoryPipe implements PipeTransform {
    transform(value: any): String {
        let result: string = null;
        if (value.currentStatus === BaseConstants.Status.STATUS_TERRITORIAL_VIOLATION) {
            result = 'Pelanggaran Wilayah';
        }

        if (result !== null) {
            if (value.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
                result += ' and Subsidi';
            }
        } else {
            if (value.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
                result = ' Subsidi';
            }
        }

        return result;
    }
}

@Pipe({
    name : 'MessageIfNull'
})
export class MessageIfNullPipe implements PipeTransform {
    transform(value?: any): String {
        if (value == null) {
            return 'Data not assign';
        }
        return value;
    }
}

@Pipe({
    name: 'ByteSize'
})
export class ByteSizePipe implements PipeTransform {
    transform(bytes?: any): string {
        const sizes: any = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) {
            return '0 Byte'
        };
        const i = Number(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
    }
}
