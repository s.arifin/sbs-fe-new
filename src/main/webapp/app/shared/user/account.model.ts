export class Account {
    constructor(
        public activated?: boolean,
        public authorities?: string[],
        public email?: string,
        public firstName?: string,
        public langKey?: string,
        public lastName?: string,
        public login?: string,
        public imageUrl?: string,
        public menus?: string[],
        public idInternal?: string,
        public group_users?: string,
    ) {
        this.authorities = new Array<string>();
    }
}
