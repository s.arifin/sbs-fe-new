import { Injectable } from '@angular/core';

@Injectable()
export class ConvertUtilService {
    public csvToArray(file: any, separator: string): Promise<any> {
        return new Promise<any>(
            (resolve, reject) => {
                const fileInput = file.target.files[0];
                const lines: any[] = [];
                const reader: FileReader = new FileReader();
                if (fileInput instanceof Blob) {
                    reader.readAsText(fileInput);

                    reader.onload = (e) => {
                        const csv: string = reader.result;
                        const allTextLines = csv.split(/\r?\n/);
                        if (allTextLines.length > 0) {
                            for (let i = 0; i < allTextLines.length; i++) {
                                // split content based on separator
                                console.log('allTextLines[i]', allTextLines[i]);
                                if (allTextLines[i] !== null && allTextLines[i] !== '') {
                                    const each = allTextLines[i].split(separator);
                                    const _arr: Array<string> = new Array<string>();
                                    console.log('eachhhh', each);
                                    for (let a = 0; a < each.length; a++) {
                                        const _each = each[a];
                                        _arr.push(_each);
                                    }

                                    lines.push(_arr);
                                }
                            }
                        }

                        resolve(lines);
                    }
                } else {
                    reject();
                }
            }
        )
    }
}
