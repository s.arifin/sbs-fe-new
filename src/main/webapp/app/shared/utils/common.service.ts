import { Injectable } from '@angular/core';
import { ToasterService} from '../../shared';
import * as _ from 'lodash';

export class ErrorBody {
    constructor(
        public type?: string,
        public title?: string,
        public status?: string,
        public detail?: string,
        public path?: string,
        public message?: string
    ) {
    }
}

@Injectable()
export class CommonUtilService {
    constructor(
        private toaster: ToasterService
    ) {

    }

    public getYearRangeForCalendar(): string {
        let now: Date;
        now = new Date();

        const currentYear = now.getFullYear();
        const maxYear = currentYear - 0;
        const minYear = currentYear - 2000;

        return minYear + ':' + maxYear;
    }

    public setYearRangeForCalendar(upperVar: number, lowerVar: number): string {
        let now: Date;
        now = new Date();

        const currentYear = now.getFullYear();
        const maxYear = currentYear - upperVar;
        const minYear = currentYear - lowerVar;

        return minYear + ':' + maxYear;
    }

    public showError(err: any): void {
        if (err !== undefined) {
            const obj: ErrorBody = JSON.parse(err._body);
            this.toaster.showToaster('error', 'Error', obj.detail);
        }
    }

    public buildTree(array, parent?, tree?): any[] {
        tree = typeof tree !== 'undefined' ? tree : [];
        parent = typeof parent !== 'undefined' ? parent : { id: null };

        const children: Array<any> = _.filter( array, function(child) { return child.parentId === parent.id; });

        if ( !_.isEmpty( children )) {
            if ( parent.id == null ) {
                tree = children;
            } else {
                parent['child'] = children;
            }

            children.forEach(
                (e) => {
                    this.buildTree( array, e);
                }
            )
        }

        return tree;
    }

    public findChild(data: Array<any>, idParent, rs?): any[] {
        rs = typeof rs !== 'undefined' ? rs : [];

        data.forEach(
            (e) => {
                if (e.parentId === idParent) {
                    rs.push(e);
                }

                if (!_.isEmpty(e.child)) {
                    this.findChild(e.child, idParent, rs);
                }
            }
        )

        return rs;
    }

    public flat(data: Array<any>, rs?): any[] {
        rs = typeof rs !== 'undefined' ? rs : [];
        data.forEach(
            (e) => {
                if (e.hasOwnProperty('child')) {
                    const _item = Object.assign({}, e);
                    delete _item['child'];

                    rs.push(_item);
                } else {
                    rs.push(e);
                }

                if (!_.isEmpty(e.child)) {
                    this.flat(e.child, rs);
                }
            }
        )

        return rs;
    }

    public getDataByParent(data, idParent) {
        const tree: any[] = this.buildTree(data);
        const filter: any[] = this.findChild(tree, idParent);
        return this.flat(filter);
    }
}
