import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { ResponseWrapper } from '../../shared';

@Injectable()
export class PasswordService {

    constructor(private http: Http) {}

    save(data: any): Observable<any> {
        return this.http.post(process.env.API_C_URL + '/api/account/change-password', data);
    }

    savePrivatePassword(data: any): Observable<any> {
        return this.http.post(process.env.API_C_URL + '/api/account/change-private-password', data);
    }
    cekPrivatePassword(data: any): Observable<any> {
        return this.http.get(process.env.API_C_URL + '/api/account/compare-private-password/' + data)
        .map(
            (res: Response) => {
                const jsonResponse = res.json();
                // this.convertItemFromServer(jsonResponse);
                return jsonResponse;
            });
    }
}
