import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../layouts';

import { Principal } from '../../shared';
import { PasswordService } from './password.service';

@Component({
    selector: 'jhi-private-password',
    templateUrl: './private-password.component.html'
})
export class PrivatePasswordComponent implements OnInit {
    doNotMatch: string;
    error: string;
    success: string;
    account: any;
    password: string;
    confirmPassword: string;

    user: any;

    constructor(
        private passwordService: PasswordService,
        private principal: Principal,
        private loadingService: LoadingService,
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.user = {};
    }

    changePassword() {
        this.loadingService.loadingStart();
        if (this.password !== this.confirmPassword) {
            this.error = null;
            this.success = null;
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.user.privatePassword = this.password
            this.passwordService.savePrivatePassword(this.user).subscribe(() => {
                this.error = null;
                this.success = 'OK';
                this.password = '';
                this.confirmPassword = '';
                this.loadingService.loadingStop();
            }, () => {
                this.success = null;
                this.error = 'ERROR';
                this.password = '';
                this.confirmPassword = '';
                this.loadingService.loadingStop();
            });
        }
    }
}
