import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd, RoutesRecognized } from '@angular/router';
import { Message } from 'primeng/primeng';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { JhiLanguageService, JhiEventManager } from 'ng-jhipster';
import { JhiLanguageHelper, Principal, StateStorageService, ToasterService, LoginService, LoginModalService } from '../../shared';
import { ProfileService } from '../profiles/profile.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Subscription } from 'rxjs';

@Component({
    selector: 'jhi-main',
    templateUrl: './main.component.html'
})
export class JhiMainComponent implements OnInit, AfterViewInit {
    objToaster: Message[] = [];
    inProduction: boolean;
    swaggerEnabled: boolean;
    link: string;
    internals: any;
    eventSubscriber: Subscription;
    isNavbarCollapsed: boolean;
    modalRef: NgbModalRef;

    constructor(
        private jhiLanguageHelper: JhiLanguageHelper,
        private router: Router,
        private $storageService: StateStorageService,
        private principal: Principal,
        private profileService: ProfileService,
        private toasterService: ToasterService,
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private loginModalService: LoginModalService,
    ) {
        this.isNavbarCollapsed = true;
    }

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = (routeSnapshot.data && routeSnapshot.data['pageTitle']) ? routeSnapshot.data['pageTitle'] : 'mpmApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    ngOnInit() {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.link = event.url;
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
                this.principal.identity().then((account) => {
                    if (account) {
                        this.internals = account.idInternal;
                    }
                });
            }
        });
        // this.profileService.getProfileInfo().subscribe((profileInfo) => {
        //     this.inProduction = profileInfo.inProduction;
        //     this.swaggerEnabled = profileInfo.swaggerEnabled;
        // });
        this.registerLogin();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
        this.modalRef = this.loginModalService.open();
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    ngAfterViewInit() {
        this.toasterService.toasterStatus.subscribe((val: Message) => {
            if (val) {
                this.objToaster.push(val);
                this.objToaster = [... this.objToaster];
            }
        });
    }

    public checkLink(link: string): boolean {
        let a: boolean;
        a = false;

        if (this.link !== undefined) {
            return this.link.toLowerCase().indexOf(link) > -1;
        }

        return a;
    }

    registerLogin() {
        this.eventSubscriber = this.eventManager.subscribe('authenticationSuccess',
            () => this.principal.identity().then((account) => { this.internals = account.idInternal; }));
    }
}
