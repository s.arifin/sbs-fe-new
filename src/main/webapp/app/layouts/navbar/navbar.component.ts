import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';

import { ProfileService } from '../profiles/profile.service';
import { JhiLanguageHelper, Principal, LoginModalService, LoginService } from '../../shared';

import { VERSION, DEBUG_INFO_ENABLED } from '../../app.constants';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        // 'navbar.scss'
    ]
})
export class NavbarComponent implements OnInit {

    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    notification: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    idInternal: string;
    idPlant: string;
    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        this.idInternal = '';
        this.idPlant = '';
    }

    ngOnInit() {
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });
        // this.profileService.getProfileInfo().subscribe((profileInfo) => {
        //     this.inProduction = profileInfo.inProduction;
        //     this.swaggerEnabled = profileInfo.swaggerEnabled;
        // });
        this.notification = [{
            id: 0,
            name: 'Document has been approved.'
        }, {
            id: 1,
            name: 'Sales report is ready.'
        }, {
            id: 2,
            name: 'Your have new message.'
        }, {
            id: 3,
            name: 'Your Info will be here.'
        }, {
            id: 4,
            name: 'Your document has been rejected.'
        }];
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
        this.idInternal = this.principal.getIdInternal();
        if (this.idInternal.includes('SKG')) {
            this.idPlant = 'SKG';
        } else if (this.idInternal.includes('SMK')) {
            this.idPlant = 'SMK';
        } else if (!this.idInternal.includes('SKG') && !this.idInternal.includes('SMK')) {
            this.idPlant = 'SBS';
        }
        console.log(this.idPlant);
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
        this.modalRef = this.loginModalService.open();
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
        if (document.body.classList.contains('open-navbar')) {
            document.body.classList.remove('open-navbar');
        } else {
            document.body.classList.add('open-navbar');
        }
    }

    hideMenu() {
        if (document.body.classList.contains('hide-menu-show')) {
            document.body.classList.remove('hide-menu-show');
        } else {
            document.body.classList.add('hide-menu-show');
        }
    }

    showNotification() {
        if (document.body.classList.contains('show-notification')) {
            document.body.classList.remove('show-notification');
        } else {
            document.body.classList.add('show-notification');
        }
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    getUserInternal() {
        return this.isAuthenticated() ? this.idInternal = this.principal.getIdInternal() : null;
    }
    backWO() {
        this.loginService.logout();
        window.location.href = 'http://localhost:8080/pus_live_new/auth/logus';
    }
}
