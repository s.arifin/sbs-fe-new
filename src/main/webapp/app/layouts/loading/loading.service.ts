import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable()
export class LoadingService {
    public loadingStart(): void {
        $('.loading-wrapper').fadeIn();
    }

    public loadingStop(): void {
        $('.loading-wrapper').fadeOut();
    }
}
