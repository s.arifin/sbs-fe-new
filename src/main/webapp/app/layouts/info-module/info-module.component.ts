import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'jhi-info-module',
    templateUrl: './info-module.component.html'
})

export class InfoModuleComponent implements OnChanges {

    @Input()
    public html: string;

    ngOnChanges(changes: SimpleChanges) {

    }
}
